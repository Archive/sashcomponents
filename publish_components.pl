#!/usr/bin/perl -w

use File::Copy;

if (scalar @ARGV != 1) {
  print <<usage;

This script copies all the relevant component files to a specified
destination directory, maintaining the directory structure. 

Run as:
$0 destination_directory

usage
  exit;
}

my $dest = shift @ARGV;
mkdir($dest);
foreach (qw(extensions locations)) {
  my $td = $dest."/".$_;
  mkdir($td);
  chdir($_);
  opendir("in", ".");
  foreach(grep {-d && ! /\./} readdir("in")) {
	next if /CVS/;
	my $dir = $_;
	mkdir($td."/".$dir);
	if (chdir($dir."/src")) {
		my @files = glob("*.tgz *.htm* *.wdf");
		foreach(@files) {
		  copy($_, $td."/".$dir);
		}
		chdir("../..");
	} else {
		print "Failed to install $dir!\n";
	}
  }
  chdir("..");
}
system("cp", "-r", "weblications", $dest);
chdir($dest."/weblications");
system("rm -rf `find . -path './*CVS'`");
