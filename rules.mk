#include this file after IDL_FILES is set to define these variables

GENERATED_INCLUDES = $(IDL_FILES:.idl=.h)
GENERATED_TYPELIBS = $(IDL_FILES:.idl=.xpt)
GENERATED_DOCS = $(IDL_FILES:.idl=.html)
EXTRA_DIST += $(IDL_FILES)
CLEANFILES += $(GENERATED_INCLUDES) $(GENERATED_TYPELIBS) $(GENERATED_DOCS)
$(GENERATED_INCLUDES): $(IDL_FILES)
$(GENERATED_TYPELIBS): $(IDL_FILES)
$(GENERATED_DOCS): $(IDL_FILES)

docs: $(GENERATED_DOCS)

## stuff to tar up the .so and .xpt files
# set WDF_FILE beforehand!
# set EXTRA_TARS if you want to tar up additional data files
noinst_DATA = $(WDF_FILE)
GENERATED_ZIPS = $(noinst_DATA:.wdf=.tgz)
XPT_FILE = $(noinst_DATA:.wdf=.xpt)

$(XPT_FILE): $(GENERATED_TYPELIBS)
	$(XPTLINK) $@ $^

$(WDF_FILE): $(GENERATED_ZIPS)
$(GENERATED_ZIPS): $(noinst_PROGRAMS) $(XPT_FILE) $(EXTRA_TARS)
	tar cfz $(GENERATED_ZIPS) $^
CLEANFILES += $(GENERATED_ZIPS) $(XPT_FILE)

*.cpp: $(GENERATED_TYPELIBS) $(GENERATED_INCLUDES) $(XPT_FILE)

install: 
	sash-install $(noinst_DATA) -f

