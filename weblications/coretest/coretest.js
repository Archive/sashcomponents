var gladefile = new Sash.Glade.GladeFile("coretest.glade");
Sash.Gtk = Sash.Glade.GtkExtension;

var Status = gladefile.getWidget("Status");

// Message window widgets
var MessageList = gladefile.getWidget("MessageList");
var IDEntry = gladefile.getWidget("IDEntry");
var MessageEntry = gladefile.getWidget("MessageEntry");
var Send = gladefile.getWidget("Send");
var ChannelLabel = gladefile.getWidget("ChannelLabel");

// Subscribed Channel widgets
var ChannelPublic = gladefile.getWidget("ChannelPublic");
var ChannelPrivate = gladefile.getWidget("ChannelPrivate");
var ChannelList = gladefile.getWidget("ChannelList");
var Unsubscribe = gladefile.getWidget("Unsubscribe");
Unsubscribe.enabled = false;

// Available Channel widgets
var AvailableList = gladefile.getWidget("AvailableList");
var AvailablePublic = gladefile.getWidget("AvailablePublic");
var AvailablePrivate = gladefile.getWidget("AvailablePrivate");
var CreateText = gladefile.getWidget("CreateText");
var AddChannel = gladefile.getWidget("AddChannel");

// Net widgets
var RequestHeaders = gladefile.getWidget("RequestHeaders");
var RequestParameters = gladefile.getWidget("RequestParameters");
var ResponseHeaders = gladefile.getWidget("ResponseHeaders");
var RequestData = gladefile.getWidget("RequestData");
var ResponseData = gladefile.getWidget("ResponseData");
var NetURL = gladefile.getWidget("NetURL");
var filename = gladefile.getWidget("filename");
NetURL.setText("http://www.thefigtrees.net/test/echo.cgi");

var CurrentChannel = null;

// ---------- General functions -----------------
function AppQuit() {
	Sash.Console.Quit();
}

function SwitchPage() {
	UpdateAll();
}

// ---------- Available Page callbacks -----------
function UpdateAvailable() {
	var availableChannels = Sash.Core.Channels.List(AvailablePublic.value);

	AvailableList.clear();
	for (var i = 0; i < availableChannels.length; i++)
		AvailableList.Append(availableChannels[i]);
}

function MakeChannelMessageFun(name, isPublic) {
	var funText = "var s = '(' + '" + (isPublic ? "Public" : "Private") +
		"' + ' channel ' + '" + name + ")' + ':\\n\\tID = ' + arguments[0].ID + '\\n'" +
		" + '\\tText = ' + arguments[0].Text + '\\n';" +
		"MessageList.insert(null, null, null, s, s.length);\n";

	return (new Function(funText));
}

function MakeChannelPresenceFun(name, isPublic, text) {
	var funText = "var s = '(' + '" + (isPublic ? "Public" : "Private") +
		"' + ' channel ' + '" + name + ")' + ': " + text + "\\n';\n" +
		"MessageList.insert(null, null, null, s, s.length);\n";

  	return (new Function(funText));
}

function OnAddChannel() {
	var name = CreateText.getText();
	var newChan = Sash.Core.Channels.Add(name, AvailablePublic.value);
	if (newChan) {
		UpdateAvailable();
		Status.setText("Subscribed to channel " + newChan.Name + ".");
		newChan.OnMessage = MakeChannelMessageFun(newChan.Name, newChan.Public);
		newChan.OnNewMember = MakeChannelPresenceFun(newChan.Name, newChan.Public, "New Member");
		newChan.OnMemberLeft = MakeChannelPresenceFun(newChan.Name, newChan.Public, "Member Left");
		CurrentChannel = newChan;
	}
}

function AvailableRowSelected(obj, row) {
	var availableChannels = Sash.Core.Channels.List(AvailablePublic.value);
	CreateText.setText(availableChannels[row]);
}
// -----------------------------------------------

// ---------- Subscribed Page callbacks -----------
function UpdateSubscribed() {
	var item, i = 0;

	ChannelList.clear();
	while ((item = Sash.Core.Channels.Item(i++, ChannelPublic.value))) {
		ChannelList.Append(item.Name);
	}
}

function SubscribedRowSelected(obj, row) {
	CurrentChannel = Sash.Core.Channels.Item(row, ChannelPublic.value);
	if (CurrentChannel) {
		Status.setText(((CurrentChannel.Public) ? "Public" : "Private") +
					   " channel " + CurrentChannel.Name + " selected.");
		Unsubscribe.enabled = true;
	}
}

function OnRemoveChannel() {
	print("Removing channel");
	if (CurrentChannel) {
		Sash.Core.Channels.Remove(CurrentChannel.Name, CurrentChannel.Public);
		CurrentChannel = null;
		Unsubscribe.enabled = false;
		UpdateSubscribed();
	}
}

// ----------------------------------------------

// ---------- Message Page callbacks ------------
function UpdateMessages() {
	if (CurrentChannel)
		ChannelLabel.setText(((CurrentChannel.Public) ? "Public" : "Private") +
							 ": " + CurrentChannel.Name);
	else
		ChannelLabel.setText("No channel selected");
}

function OnSend() {
	if (CurrentChannel) {
		var msg = new Sash.Core.Channels.Message();
		msg.ID = IDEntry.getText();
		msg.Text = MessageEntry.getText();
		CurrentChannel.SendMessage(msg);
		Status.setText("Send message on channel " + CurrentChannel.Name + ".");
	}
}

// ----------------------------------------------

function OnMessage(id, text) {
	var s = id + ":" + text + "\n";

	MessageList.insert(null, null, null, s, s.length);
}

function UpdateAll() {
	UpdateAvailable();
	UpdateSubscribed();
	UpdateMessages();
	Status.setText("");
}

var foo = Sash.Core.Cursor.X_CURSOR;
function OnCursorSwitch() {
	if (foo < 154) {
		Sash.Core.Cursor.SetStandard(foo);
		foo += 2;
		Status.setText("Button clicked! " + foo);
	} 
}

function default_cursor() {
		Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function get_mouse_pos() {
	var bob = Sash.Core.Cursor.GetPosition(null);
	Status.setText("Position is (" + bob[0] + "," + bob[1] + ")");
}

function get_mouse_x_pos() {
	Status.setText("x position is " + Sash.Core.Cursor.GetPosition(true));
}

function get_mouse_y_pos() {
	Status.setText("y position is " + Sash.Core.Cursor.GetPosition(false));
}

function move_cursor() {
	Sash.Core.Cursor.SetPosition(69, 69);
}

function webl_data_path() {
	Status.setText("Weblication data path is " + Sash.Core.WeblicationDataPath);
}

function start_mozilla() {
	Sash.Core.ExecDefaultWebBrowser("www.news.com");
}

function guid() {
	Status.setText(Sash.Core.GetNewGUID());
}

function sashversion() {
	Status.setText("Sash version is " + Sash.Core.GetSashVersion());
}

function sleep() {
	Sash.Core.Sleep(1000);
}

function test_exec_action() {
	// try execing this one again 
	Sash.Core.ExecAction("{00D174C8-C669-446E-BDEE-D30C4C28BC03}", "{24EC67E1-99A6-4A00-8EAF-7505089BBFB5}");
}

function test_webl_installed() {
	var a = Sash.Core.WeblicationIsInstalled("{00D174C8-C669-446E-BDEE-D30C4C28BC03}");
	var b = Sash.Core.WeblicationIsInstalled("{oogabooga}");
	if (a && !b) Status.setText("TEST PASSED!");
	else Status.setText("TEST FAILED!");
}

// ----------------- Process test ---------------------
var ProcessName = gladefile.getWidget("ProcessName");
var PidEntry = gladefile.getWidget("PidEntry");

function CreateProcess() {
	var name = ProcessName.getText();
	var pid = Sash.Core.Process.Create(name.split(' '));
	if (pid > 0)
		Status.setText(name + " executed with PID " + pid);
	else 
		Status.setText("Unable to execute " + name + "!");
}

function ProcessIsAlive() {
	var pid = PidEntry.getText();
	Status.setText(pid + " is " + 
				   (Sash.Core.Process.IsAlive(pid) ? "alive" : "NOT alive") +
				   ".");
}

function ProcessKill() {
	var pid = PidEntry.getText();
	if (Sash.Core.Process.Terminate(pid))
		Status.setText("Process " + pid + " terminated.");
	else
		Status.setText("Unable to terminate process " + pid + ".");
}

// ----------------- UI Test --------------------

function OKButton() {
	Status.setText("Invoking a message box, expecting " + Sash.Core.UI.IDOK);
	var b = Sash.Core.UI.MessageBox("This is a message", "this is the title", Sash.Core.UI.MB_OK);
	if (b != Sash.Core.UI.IDOK) Status.setText("Incorrect return value! " + b);
	
}	

function FunkyButton() {
	var b = Sash.Core.UI.MessageBox("This is a message", "this is the title", Sash.Core.UI.MB_YESNOCANCEL | Sash.Core.UI.MB_ICONINFORMATION | Sash.Core.UI.MB_DEFBUTTON2);
	
}	

function PromptForValue() {
	var b = Sash.Core.UI.PromptForValue("This is the title", "Gimme a string, baby!", "Like this one");
	Status.setText("Got string " + b);
}

function PromptChoice() {
	var a = Sash.Core.UI.PromptChoice("This is the title", "Please select one or more of the following choices:", ["these", "are", "the", "choices"], 2, true);
	Status.setText(a.toSource());
}

function os() {
	var o = Sash.Core.Platform.OS;
	Status.setText("OS is " + o);
}

function shell() {
	var o = Sash.Core.Platform.Shell;
	Status.setText("Shell is " + o);
}

// --------------------- Net Test --------------------------
var netConnection = null;

function getrespheaders() {
	ResponseHeaders.deleteText(0, -1);
	if (netConnection) {
		for (i = 0; i < netConnection.ResponseHeaders.Count; i++) {
		print (netConnection.ResponseHeaders.Name(i) + ": " + netConnection.ResponseHeaders.Value(i));
			ResponseHeaders.Append(netConnection.ResponseHeaders.Name(i) + ": " +
								   netConnection.ResponseHeaders.Value(i) + "\n");
		}
	}
}

function getreqheaders() {
	var headerstr = RequestHeaders.text;
	if (headerstr == "") return;

	var headerarr = headerstr.split ('\n');
	if (headerarr.length > 0){
		for (i in headerarr){
			if (headerarr[i] != ""){
				var entry = headerarr[i].split (':');
				if (entry[1].indexOf(' ') == 0){
					entry[1] = entry[1].substr(1);
				}
				print ("using header: (" + entry[0] + ", " + entry[1] + ")");
				netConnection.RequestHeaders.SetValue(entry[0], entry[1]);
			}
		}
	}
}

function getreqparameters() {
	print ("getreqparameters");
	var headerstr = RequestParameters.text;
	if (headerstr == "") return;

	var headerarr = headerstr.split ('\n');
	if (headerarr.length > 0){
		for (i in headerarr){
			if (headerarr[i] != ""){
				var entry = headerarr[i].split (':');
				if (entry[1].indexOf(' ') == 0){
					entry[1] = entry[1].substr(1);
				}
				print ("using parameter: (" + entry[0] + ", " + entry[1] + ")");
				netConnection.SetRequestParameter(entry[0], entry[1]);
			}
		}
	}
}

function on_abort() {
	Status.setText("URL transfer aborted!");
}

function on_complete() {
	Status.setText("URL transfer completed with status " + netConnection.Status);
	getrespheaders();
	ResponseData.text = "";
	ResponseData.Append(netConnection.Response);
	print (netConnection.ResponseAsString);
	print (netConnection.ResponseCode);
}

function on_progress(o, cur, total, timeElapsed) {
	var str = "URL progress " + cur + " of " + total + "(" + timeElapsed + " seconds).";
	Status.setText(str);
	printsys(str);
}

function on_start() {
	Status.setText("URL transfer started.");
}

function netSetup() {
	netConnection = new Sash.Core.Net.URLConnection(NetURL.getText());
	netConnection.RequestBody = RequestData.text;
	netConnection.Async  = gladefile.getWidget("use_async").value;
	if (netConnection.Async){
		print ("async transfer");
	} else {
		print ("sync transfer");
	}
	netConnection.OnAbort = on_abort;
	netConnection.OnComplete = on_complete;
	netConnection.OnProgress = on_progress;
	netConnection.OnStart = on_start;
	netConnection.ProgressEventFrequency = 2;
}

function netget() {
	netSetup();
	netConnection.ConnectionType = netConnection.GET;

	netConnection.SaveResponseToFile = gladefile.getWidget("use_file").value;
	if (netConnection.SaveResponseToFile){
		netConnection.SaveResponseTo = filename.getText();
		print ("saving file to: " +netConnection.SaveResponseTo);
	}
	getreqheaders();

	netConnection.Execute();
	
	if (netConnection.Async == false){
		 print ("status code: " + netConnection.Status);
		 print ("response code: " + netConnection.ResponseCode);
		 print ("ResponseAsString: " + netConnection.ResponseAsString);
	}
}

function netpost() {
	netSetup();
	netConnection.ConnectionType = netConnection.POST;

	var fromFile = gladefile.getWidget("use_file").value;
	if (fromFile){
		netConnection.RequestFile = filename.getText();
		print ("getting file from: " +netConnection.RequestFile);
	} else {
		print ("posting data: " +netConnection.RequestBody);
	}
	getreqheaders();
	getreqparameters();

	netConnection.Execute();
	if (netConnection.Async == false){
		 print ("status code: " + netConnection.Status);
		 print ("response code: " + netConnection.ResponseCode);
		 print ("ResponseAsString: " + netConnection.ResponseAsString);
	}
}
