var gladefile = new Sash.Glade.GladeFile("sashpad.glade");

var filename = "";

var statusbar = gladefile.getWidget("statusbar");
var text = gladefile.getWidget("text");

// disable save until something's actually there
var save = gladefile.getWidget("save_button");
disable_save();

function new_entered() { statusbar.setText("Create a new file"); }
function open_entered() { statusbar.setText("Open an existing text file"); }
function save_entered() { statusbar.setText("Save the current buffer"); }
function open_last_entered() { statusbar.setText("Open the last file you edited"); }
function about_entered() { statusbar.setText("About SashPad"); }
function quit_entered() { statusbar.setText("Quit SashPad"); }
function cut_clicked() { text.cutClipboard(); }
function paste_clicked() { text.pasteClipboard(); }
function copy_clicked() { text.copyClipboard(); }
function select_all_clicked() { text.selectRegion(0, -1); }
function read_only_clicked(button) { text.setEditable(! button.active); }
function clear() { statusbar.setText(""); }
function show_about() { gladefile.getWidget("about").showAll(); }

function clear_clicked() { 
	if (save_first() == false) return;
	text.selectRegion(0, -1);
	text.deleteSelection();
}

function word_wrap_clicked(button) { 
	text.setLineWrap(button.active);  // not WordWrap -- that's useless!
}

function set_last(f) {
	  Sash.Registry.SetWeblicationValue("", "last", f);
}

function on_text_changed() { 
	save.enabled = true; 
	text.signalDisconnect("changed");
}

function save_clicked() {
	  if (filename == "") {
	     save_as_clicked();
	  } else {
		  statusbar.setText("Unable to open file " + filename + "!");
		  try {
			  var file = new Sash.FileSystem.TextStream(filename, Sash.FileSystem.MODE_WRITE);
		  } catch(a) {
			  return;
		  }

		  file.Write(text.text);
		  file.Close();
		  statusbar.setText("File " + filename + " saved!");
		  set_last(filename);
		  disable_save();
	  }
}

function save_as_clicked() {
	  filename = Sash.Linux.FileDialog(filename);
	  if (filename != "")
	  	save_clicked();
}

function last_clicked() {
	var fname = Sash.Registry.QueryWeblicationValue("", "last");
    if (fname && fname != "")
       open_clicked(fname, fname);
}

function save_first() {
	if (save.enabled) {
		var but = Sash.Core.UI.MessageBox("The current file hasn't been saved. Save it first?", "Save current file?", 
			Sash.Core.UI.MB_YESNOCANCEL);
		if (but == Sash.Core.UI.IDCANCEL) return false;
		if (but == Sash.Core.UI.IDYES) {
			save_clicked();
		}
	} 
	return true;
}

function new_clicked() {
	if (save_first() == false) return;
	filename = "";
 	text.text = "";
	disable_save();
}

function open_clicked(widget, tmp_name) {
	if (save_first() == false) return;
	if (tmp_name == null || tmp_name == "")
		tmp_name = Sash.Linux.FileDialog("");

	if (tmp_name != "") {
		if (Sash.FileSystem.FolderExists(tmp_name)) {
			statusbar.setText("Cannot open a directory!");
			return;
		}
		statusbar.setText("Unable to open file " + tmp_name + "!");
		try {
			var file = new Sash.FileSystem.TextStream(tmp_name, Sash.FileSystem.MODE_WRITE);
		} catch (a) {
			  return;
		}
	    if (filename != "") set_last(filename);
		filename = tmp_name;

		text.text = file.ReadAll();
		file.Close();
		disable_save();
		gladefile.getWidget("SashPad").setTitle("Sashpad - " + filename);
	}
}

function disable_save() {
	save.enabled = false;
	text.signalConnect("changed", "on_text_changed", null);
}

function quit() {
	if (save_first() == false) return true;
	if (filename != "") set_last(filename);
	Sash.Console.Quit();
	return false;
}

var findstring, findpos;
function find() {
	var str = Sash.Core.UI.PromptForValue("Find", "Enter the search text:", "");
	if (str == "") return;
	findpos = text.getPoint();
	findstring = str;
	find_again();
}

function find_again() {
	if (findstring == "") return;
	var pos = text.text.indexOf(findstring, findpos);
	if (pos == -1 && findpos != 0) {
		pos = text.text.indexOf(findstring, 0);
	}
	if (pos == -1) {
		statusbar.setText("String '"+findstring+"' not found!");
		return;
	}

	findpos = pos+findstring.length;
	text.setPosition(pos);
	text.selectRegion(pos, findpos);	
}

function replace(a) {
	gladefile.getWidget("ReplaceWindow").visible = false;
	if (! a) return false;
	var sstring = new RegExp(gladefile.getWidget("search_string").getText(), 
			gladefile.getWidget("case").value ? "g" : "ig");

	var rstring = gladefile.getWidget("replace_string").getText();
	text.text = text.text.replace(sstring, rstring);
}

function replace_show() {
	gladefile.getWidget("ReplaceWindow").showAll();
}

