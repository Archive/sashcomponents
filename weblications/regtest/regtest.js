print ("Starting registry test\n");
var r = Sash.Registry;

var suc = 0;
function check(s) {
	if (!s) {
		print ("Test failed!\n");
		suc = -1000;
	} else {
		suc++;
	}	
}


print ( "Testing key creation and deletion:" );
// make sure key isn't there already
r.DeleteWeblicationKey("\\Andrew\\Wu");

// create key and verify it exists
check( r.CreateWeblicationKey("\\Andrew\\Wu") );

var j, keys, s;
keys =  r.EnumWeblicationKeys("\\Andrew");
for (j = 0; j < keys.length ; j++) {
	if (keys[j] == "Wu") break;
}
check (j != keys.length);

if (! r.DeleteWeblicationKey("\\Andrew\\Wu")) failed ("Delete key");

keys =  r.EnumWeblicationKeys("\\Andrew");
for (j = 0; j < keys.length ; j++) {
 	if (keys[j] == "Wu") break;
}
check (j == keys.length);

print ("Key create/delete unit test... OK\n");

print( "Testing SetValue() and QueryValue()" );
print("STRING set, ");
check( r.SetWeblicationValue("\\BigBoss\\name", "some_string", "aj") );

print( "ARRAY set");
check( r.SetWeblicationValue("\\BigBoss\\name", "ari", ["andrew", "john", "ooga"]) );

s = r.QueryWeblicationValue("\\BigBoss\\name", "some_string");
check(s == "aj");
print( "\tSTRING retrieved. ");
s = r.QueryWeblicationValue("\\BigBoss\\name", "ari");

check (s[0] == "andrew" && s[1] == "john" && s[2] == "ooga" && s.length == 3);
print( "ARRAY retrieved.");

print( "Set and query values unit test... OK");

check( r.SetValue("{70BCBE85-0567-4FFC-879E-CF31D15FC5B4}", "sadf", "asda", "asdfa") );
check( r.QueryValue("{70BCBE85-0567-4FFC-879E-CF31D15FC5B4}", "sadf", "asda") == "asdfa");
check( r.SetWeblicationValue("\\BigBoss\\name", "some_string", "aj") );


if (suc >= 0) {
	print("Success! " + suc + " tests completed.\n");
} else {
	print("Failed!");
}

Sash.Console.Quit();