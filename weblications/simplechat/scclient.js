var gladefile = new Sash.Glade.GladeFile("scclient.glade");
var statusbar = gladefile.getWidget("status");
Sash.Gtk = Sash.Glade.GtkExtension;
var textBox = gladefile.getWidget("messagebox");
var messageBox = gladefile.getWidget("message");
var notebook = gladefile.getWidget("notebook");

var client = new Sash.Comm.Sockets.ClientSocket();
var localIP = client.LocalIPAddress;
client.OnConnect = "on_connect";
client.OnReceived = "on_receive";
client.OnClosed = "on_closed";
client.OnError = "on_error";

notebook.setPage(1);

print("localIP:" + localIP);

function printStatus(s){
	statusbar.setText(s);
}

function on_connect_clicked(){
	if (client.Connected){
		printStatus ("Already connected!");
	} else {
		var ipaddr = gladefile.getWidget("ipaddress").getText();
		var port = gladefile.getWidget("port").getText();
		client.Connect(ipaddr, port, null, null);
	}
}

function on_disconnect_clicked(){
	if (client.Connected){
		client.Close(null);	
		printStatus ("Disconnected.");
	}else{
		printStatus ("Not connected.");
	}

}

function on_send_clicked(){
	if (client.Connected){
		var message = gladefile.getWidget("message").getText();

		var name = gladefile.getWidget("name").getText();
		if ((name == null)||(name == "")){
			name = "anon";
		}
		client.Send(name + ": " + message + "\n");	
		messageBox.setText("");
		printStatus("Message sent.");
	}	else{
		printStatus("Not connected.");
	}
	
}

function on_connect(socket){
	printStatus("Connected to server " + socket.RemoteHost + ":" + socket.RemotePort);
	notebook.setPage(0);
}

function on_receive(socket){
	var readto = socket.SearchPendingReceiveDat("!!!");
	if (readto == -1){
		var data = socket.Receive(null, null);
		textBox.Append(data);

	}
	else{
		var data = socket.Receive(true, readto);
		textBox.Append(data);

		var amtLeft = socket.PendingReceiveByteCount;

		var rest = socket.Receive(null, null);
		textBox.Append(rest);

    }
}

function on_closed(socket){
	printStatus("Connection closed.");
	textBox.Append("Disconnected from server!\n");
}

function on_error(socket){
	printStatus ("Error: " + socket.LastErrorDescription);
}

function on_close_clicked(){
	if (client.Connected){
		client.Close(null);
	}
	Sash.Console.Quit();
}

