var gladefile = new Sash.Glade.GladeFile("scserv.glade");
var statusbar = gladefile.getWidget("status");
Sash.Gtk = Sash.Glade.GtkExtension;

// create a new socket for listening.
var listen = new Sash.Comm.Sockets.ServerSocket();
listen.OnConnection = "on_connection";
listen.OnError = "on_error";

var users = new Array();
var count = 0;

function printStatus(s){
	statusbar.setText(s);
}

function on_receive(socket){
	var data = socket.Receive(null, null);
	printStatus("Data received/sent.");
	for (u in users){
		if ((u != 0) && (users[u] != null) && (users[u].Connected)){
			users[u].Send(data);
		}
	}
}

function on_error(socket){
	printStatus("Server socket error: " + socket.LastErrorDescription);
}

function on_client_error(socket){
	printStatus("client socket error: " + socket.LastErrorDescription);
}

function on_closed(socket){
	// figure out who left.
	var index;
	var u=0;

	users[socket.RemotePort] = null;
	
	for (u in users){
		if ((u != 0) && (users[u] != null)) users[u].Send("someone left!\n");
	}
	count --;
	printStatus(count + " users left.");
}

function on_start_clicked(){
	var port = gladefile.getWidget("port").getText();
	if (listen.Started){
		printStatus("Already started.");
	} else {
		if (listen.Start(port)){;
			count = 0;
			printStatus ("Started on port " + listen.ServerPort);
		}	else{
			printStatus ("Couldn't start.");
		}
	}
}

function on_stop_clicked(){
	if (listen.Started){
		listen.Stop();
		printStatus ("Stopped.");
	} else {
		printStatus ("Not started.");
	}
}

// set event handlers
function on_connection(socket){
	var connection1 = new Sash.Comm.Sockets.ClientSocket();	
	connection1.OnReceived = "on_receive";
	connection1.OnClosed = "on_closed";
	connection1.OnError = "on_client_error";

	socket.AcceptConnection(connection1);
	users[connection1.RemotePort] = connection1;
	
	for (u in users){
		if ((u != 0) && (users[u] != null) && (users[u].Connected))
			users[u].Send("someone new joined!\n");
	}
	count ++;
	printStatus(count + " users present.");
}

function on_close_clicked(){
	if (listen.Started){
		listen.Stop();
	}
	Sash.Console.Quit();
}
