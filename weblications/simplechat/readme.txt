Simple Client/Server Chat weblication

Contributor(s):
Wing Yung

Brief Description:
A very simple, limited client/server chat weblication that demonstrates how to use client and server sockets. 

The server is started on some port. A client that connects to that particular port receive messages from all other clients connected to that port.