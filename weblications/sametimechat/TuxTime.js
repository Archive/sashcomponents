var session= null;

var mainui= new Sash.Glade.GladeFile("ui.glade");
var searchEntry= mainui.getWidget("SearchEntry");
var buddyList= mainui.getWidget("BuddyList");
var statusBar= mainui.getWidget("StatusBar");

var loginui= new Sash.Glade.GladeFile("loginui.glade");
var loginWindow= loginui.getWidget("LoginWindow");
var serverEntry= loginui.getWidget("ServerEntry");
var usernameEntry= loginui.getWidget("UsernameEntry");
var passwordEntry= loginui.getWidget("PasswordEntry");

var server;
var username;
var usernick;
var password;

serverEntry.setText("spoonfed.genesis.adtech.internet.ibm.com");
usernameEntry.setText("atev1test");
passwordEntry.setText("atev1test");

loginWindow.hide();

var cxid= null;

function StatusMessage(msg) {
	if (cxid!= null)
		statusBar.pop(cxid);
	cxid= statusBar.getContextId(msg);
	statusBar.push(cxid, msg);
}

StatusMessage("Welcome to TuxTime");

var openChats= new Array(0);

function GetChatByUser(user) {
	for (var i= 0; i< openChats.length; ++i) {
		if (openChats[i].user== user)
			return openChats[i];
	}
	return null;
}

function RemoveChatByUser(user) {
	var i;
	if (openChats.length== 0)
		return null;
	for (i= 0; i< openChats.length; ++i) {
		if (openChats[i].user== user)
			break;
	}
	for (var j= i+ 1; j< openChats.length; ++j)
		openChats[j- 1]= openChats[j];
	--openChats.length;
	return null;
}

function OnChatWindowClosed(gwin, user) {
	print("Closing chat window");
	session.CloseChannel(user);
}

function OnChatEnterPressed(gtext, user) {	
	var txt= gtext.text;
	if (txt.length== 0)
		return;
	var lastc= txt[txt.length- 1];
	if (lastc!= '\n')
		return;
	session.SendMessage(user, txt);
	gtext.text= "";
	chatObj= GetChatByUser(user);
	chatObj.ui.getWidget("ChatDisplay").text+= "["+ username+ "] "+ txt;
}

function GetUIForUserWithOpen(user) {
	var chatObj= GetChatByUser(user);
	if (chatObj== null) {
		var chatui= new Sash.Glade.GladeFile("chatui.glade");
		chatObj= new Object();
		chatObj.user= user;
		chatObj.ui= chatui;
		Sash.GTK.signalConnect(chatui.getWidget("ChatEdit"), "changed", "OnChatEnterPressed", user);
		Sash.GTK.signalConnect(chatui.getWidget("ChatWindow"), "destroy", "OnChatWindowClosed", user);
		openChats[openChats.length]= chatObj;
	}
	return chatObj;
}

function OnChannelOpened(user, clientType) {
//	GetUIForUserWithOpen(user);
}

function OnChannelClosed(user, clientType) {
	RemoveChatByUser(user);
}

function OnMessage(user, msg, isEncrypted, clientType) {
	var chatObj= GetUIForUserWithOpen(user);
	chatObj.ui.getWidget("ChatDisplay").text+= "["+ user+ "] "+ msg+ "\n";
}

function OnData(user, data, dataType, dataSubType, isEncrypted, clientType) {
//	print("User: "+ user+ " sent us "+ data.length+ (isEncrypted ? "encrypted" : "non-encrypted")+
//		" bytes of type "+ dataType+ "."+ dataSubType);
}

function OnChatClicked() {
	if (session== null)
		return;
	var sel= buddyList.SelectedRows;
	if (sel== null || sel.length!= 1)
		return;
	var user= buddyList.GetText(sel[0], 1);
	var chatObj= GetChatByUser(user);
	if (chatObj!= null)
		return;
	session.OpenChannel(user, null);
	GetUIForUserWithOpen(user);
}

function OnChannelRequest(user, encryptionLevel) {
	print("Chat request by user: "+ user);
	return true;
}

function OnUpdateUser(user) {
	print("OnUpdateUser");
	var row= buddyList.rows;
	for (i= 0; i< buddyList.rows && row== buddyList.rows; ++i)
		if (buddyList.GetText(i, 1)== user.DisplayName)
			row= i;
	print(user.DisplayName+ " :status= "+ user.Status);
	var status= "Offline";
	switch (user.Status) {
		case Sash.STCommunity.STATUS_ACTIVE:
			status= "Active";
			break;
		case Sash.STCommunity.STATUS_AWAY:
			status= "Away";
			break;
		case Sash.STCommunity.STATUS_DISCONNECTED:
			status= "Disconnected";
			break;
		case Sash.STCommunity.STATUS_DND:
			status= "Do Not Disturb";
			break;
		case Sash.STCommunity.STATUS_NOT_USING:
			status= "Not active";
			break;
	}
	if (row== buddyList.rows)
		buddyList.Append([status, user.DisplayName]);
	else
		buddyList.setText(row, 0, status);
	StatusMessage(user.DisplayName+ " - "+ status);
}

function OnAddClicked() {
	if (session== null)
		return;
	var buddy= searchEntry.getText();
	session.AddUsers([ buddy ]);
}

function OnLogin(success, reason, displayName) {
	if (success) {
		StatusMessage("Logged in as "+ displayName);
		usernick= displayName;
	}
	else {
		StatusMessage("Cannot login to "+ server);
		session.Terminate();
		session= null;
	}
}

function OnLogout(reason) {
	print("OnLogout");
	buddyList.clear();
	StatusMessage("Disconnected from "+ server);
	session.Terminate();
	session= null;
}

function OnLoginClicked() {
	if (session!= null) {
		session.Terminate();
		session= null;
	}
	loginWindow.show();
}

function OnLoginOKClicked() {
	loginWindow.hide();
	server= serverEntry.getText();
	username= usernameEntry.getText();
	password= passwordEntry.getText();
	StatusMessage("Connecting as "+ username+ " on "+ server);
	session= new Sash.STCommunity.Session(server);
	session.OnLoginComplete= "OnLogin";
	session.OnDisconnect= "OnLogout";
	session.OnUpdateUser= "OnUpdateUser";
	session.OnChannelRequest= "OnChannelRequest";
	session.OnChannelOpened= "OnChannelOpened";
	session.OnChannelClosed= "OnChannelClosed";
	session.OnMessage= "OnMessage";
	session.OnData= "OnData";
	session.Login(username, password);
}

function OnLoginCancelClicked() {
	loginWindow.hide();
}

function OnLogoutClicked() {
	session.Disconnect();
}

function OnExitClicked() {
	if (session!= null)
		session.Terminate();
	Sash.Console.Quit();
}
