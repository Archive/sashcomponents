print ("Starting xml test\n");
var x = Sash.XML;

var suc = 0;
function check(s) {
	if (!s) {
		print ("Test failed!\n");
		suc = -1000;
	} else {
		suc++;
	}	
}

function printChildren(node){
	print ("Node name: " + node.name + ", Parent name: " + node.parent.name);
	var childnodes = node.getChildNodes("");
	var i=0;
	if (childnodes.length > 0){
		print ("has child(ren):");
		for (p in childnodes){
			printChildren(childnodes[p]);
		}
		print ("end of children");
	}	
}

function printSingleNodeInfo(node){
	print ("Node name: " + node.name + ", Parent name: " + node.parent.name);
	var children = node.getChildNodes("");
	var numattr = node.hasAttributes("");
	print (numattr + " attributes, " + children.length + " children");
}

// things to test:
// creating an XML document
print ("creating new XML document");
var doc = new x.Document();
doc.createRootNode("wingroot");
var rootnode = doc.rootNode;

var nodearr = new Array();
print ("trying to create new node");
var ajnode = doc.createNode("aj");
print ("created new node");
for (p in ajnode){
	print (p);
}
rootnode.appendChild(ajnode);
print ("appended new node");

ajnode = rootnode.getFirstChildNode("aj");
ajnode.setAttribute("lastname", "shankar");
ajnode.setAttribute("company", "IBM");

var johnnode = doc.createNode("john");
rootnode.appendChild(johnnode);
johnnode = rootnode.getFirstChildNode("john");
johnnode.setAttribute("lastname", "corwin");
johnnode.setAttribute("company", "IBM");

var wingnode = doc.createNode("wing");
rootnode.appendChild(wingnode);
wingnode = rootnode.getFirstChildNode("wing");
wingnode.setAttribute("lastname", "yung");
wingnode.setAttribute("company", "IBM");


print ("adding sisters");
var nodearr = new Array();
var mayanode = doc.createNode("maya");
nodearr.push(mayanode);
var meeranode = doc.createNode("meera");
nodearr.push(meeranode);
ajnode.appendChildNodes(nodearr);
print ("document string:" + doc.saveToString());

print ("examining node 'john'");
var jn = rootnode.getFirstChildNode("john");
var attributes = jn.getAttributeNames("");
if (attributes.length == 0){
	print ("no attributes")
} else {
	for (i in attributes){
		print (attributes[i] + ": " + jn.getAttribute(attributes[i]));
	}
}

print ("updating node 'john'");
jn.setAttribute ("likes", "bikes");
jn.setAttribute ("company", "SashXB");
print ("document string:" + doc.saveToString());

print ("removing attribute 'likes'");
print ("updating node 'john'");
jn.removeAttribute ("likes");
print ("document string:" + doc.saveToString());

print ("adding jordi...");
var jordinode = doc.createNode("jordi");
rootnode.appendChild(jordinode);
print ("document string:" + doc.saveToString());

print ("removing jordi...");
var numremoved = rootnode.removeChildNodes("jordi");
print (numremoved + " nodes removed");
print ("document string:" + doc.saveToString());

print ("***ITERATOR TEST***");
var wdfiter = new x.Iterator(doc.rootNode);
wdfiter.begin();
var currNode;
while (wdfiter.hasNext()){
	currNode = wdfiter.getNext();
	printSingleNodeInfo(currNode);

}

print ("***CHILD TEST***");
printChildren(rootnode);


// opening an existing XML document
/*

var wdfdoc = new x.Document();
wdfdoc.loadFromFile("xmltest.wdf");
print ("document string:" + wdfdoc.saveToString());

print ("***ITERATOR TEST***");
var wdfiter = new x.Iterator(wdfdoc.rootNode);
wdfiter.begin();
var currNode;
while (wdfiter.hasNext()){
	currNode = wdfiter.getNext();
	printSingleNodeInfo(currNode);

}
print ("***LOADFROMSTRING***");
var cloneddoc = new x.Document("cloned");
clonedoc.loadFromString(doc.saveToString());
print ("cloned document string:" + clonedoc.saveToString());

print ("***CHILD TEST***");
printChildren(wdfdoc.rootNode);
*/

Sash.Console.Quit();