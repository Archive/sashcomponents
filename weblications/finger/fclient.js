var gladefile = new Sash.Glade.GladeFile("fclient.glade");
var textBox = gladefile.getWidget("info");
var client = new Sash.Comm.Sockets.ClientSocket();
client.OnReceived = "on_receive";
client.OnConnect = "on_connect";
client.OnClosed = "on_closed";
client.OnError = "on_error";
var strarr;

function on_receive(socket){
	print ("in on_receive. trying to call socket.Receive()");
	for (p in socket){
		print(p);
	}
	print ("remote host: " + socket.RemoteHost);
	print ("bytes to be received: " + socket.PendingReceiveByteCount);
	if (socket.Connected){
		print ("socket connected");
	} else {
		print ("socket not connected");
	}

	var data = socket.Receive(null, null);
	print ("data received. trying to replace");

	data = data.replace(new RegExp("\r", "g"), "");	
	print ("updating text box");
	textBox.text += data;
}

function on_closed(socket){
	print ("connection closed");
}

function on_connect(socket){
	var user;
	if (strarr.length == 1){
		// assume it's just the hostname. just send \r\n
		socket.Send("\r\n");
	} else if (strarr.length ==2){
		// of the form blah@host
		user = strarr[0];
		print("looking for user " + user);
		socket.Send(user + "\r\n");
 	}
}

function on_go_clicked(){
	textBox.text = "";
	var name = gladefile.getWidget("query").getText();
	gladefile.getWidget("FingerWindow").setTitle("Finger Client - " + name);

	// parse query into two parts: x@y
	strarr = name.split("@");

	var host;

	if (strarr.length ==1){
		host = strarr[0];
	}
	else if (strarr.length == 2){
		host = strarr[1];
	}

	// get a connection.
	textBox.text += "Connecting to " + host + "...\n";
	client.Connect(host, 79, null, null);
}

function on_close_clicked(){
	Sash.Console.Quit();
}

