#!/usr/bin/perl
if ($ARGV[0] eq "--help") {
  print "$0: \n\tsash-installs all weblications one level underneath this directory\n";
  exit;
}

opendir(in, ".");
foreach(grep {-d && ! /\./} readdir(in)) {
  chdir($_);
  my (@names) = <*.wdf>;
  foreach (@names) {
	print "sash-install $_ -f\n";
	system("sash-install $_ -f");
  }
  chdir("..");
}
closedir(in);
