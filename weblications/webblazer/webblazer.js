var gladefile = new Sash.Glade.GladeFile("webblazer.glade");
var sbar = gladefile.getWidget("statusbar");
var urllist = gladefile.getWidget("urllist");
var bookmarks = gladefile.getWidget("bookmarks_menu_item_menu");
var back = gladefile.getWidget("back");
var forward = gladefile.getWidget("forward");
var browser = gladefile.getWidget("browser");
var window = gladefile.getWidget("WebBlazer");
var url = gladefile.getWidget("URL");
// disable navigation buttons until we've been somewhere
forward.enabled = false;
back.enabled = false;

browser.OnProgress = update_sbar;
browser.OnLocationChanged = set_buttons;
browser.OnTitleChanged = set_title;
browser.OnLoadEnd = done_loading;
browser.OnLinkMessageChanged = link_message;

function link_message() {
	sbar.setText(browser.getLinkMessage());
}

function done_loading() {
	url.setText(browser.getLocation());
	sbar.setText("Done loading!");
}

function update_sbar(a, b) {
	sbar.setText("Blazing to " + browser.getLocation() + " (" + a + " bytes read)"); 
}

function set_buttons() {
	forward.enabled = browser.canGoForward();
	back.enabled = browser.canGoBack();
}

function set_title() {
	window.setTitle("Sash WebBlazer - " + browser.getTitle());
}

function on_refresh_clicked() { browser.reload(0); sbar.setText("Reloading page"); }
function on_stop_clicked() { browser.stopLoad(); sbar.setText("Stopping page load"); }

var keys = Sash.Registry.EnumWeblicationValues("bookmarks");
var item3, i;
for (i = 0; i < keys.length; i++) {
 	item3 = new Sash.GTK.GtkMenuItem(keys[i]);
	item3.signalConnect("activate", "on_bookmark_clicked", 
		Sash.Registry.QueryWeblicationValue("bookmarks", keys[i]));
	item3.show();
	bookmarks.append(item3);
}

function set_as_home_page() {
	var location = browser.getLocation();
	Sash.Registry.SetWeblicationValue("", "home", location); 
	sbar.setText("Home page set to " + location); 
}

function on_bookmark_clicked(bookmark, page) {
	sbar.setText("Blazing to bookmarked page " + page);	
	browser.loadUrl(page);
}

function add_bookmark_clicked() {
	var nowurl = browser.getLocation();
	var is_there = Sash.Registry.QueryWeblicationValue("bookmarks", browser.getTitle());
	if (! is_there) {
		Sash.Registry.SetWeblicationValue("bookmarks", browser.getTitle(), nowurl);
		var item2 = new Sash.GTK.GtkMenuItem(browser.getTitle());
		item2.signalConnect("activate", "on_bookmark_clicked", nowurl);
		item2.show();
	  	bookmarks.append(item2);
		sbar.setText("Added bookmark " + browser.getTitle());
	} else {
		sbar.setText("Bookmark already exists!");
	}
}

function goto_url() { 
	sbar.setText("Blazing to " + url.getText()); 
	browser.loadUrl(url.getText());
}

function on_home_clicked() {
	var home = Sash.Registry.QueryWeblicationValue("", "home");
	if (home == "" || home == null) {
    	sbar.setText("Please set a home page first through the bookmarks menu.");
	} else {
		sbar.setText("Going home to " + home); 
	    browser.loadUrl(home);
	}
}

function on_forward_clicked() { 
	if (browser.canGoForward()) {
		sbar.setText("Going forward");
		browser.goForward(); 
	}
}

function on_back_clicked() { 
	if (browser.canGoBack()) {
		sbar.setText("Going back");
		browser.goBack(); 
	}
}

on_home_clicked();
