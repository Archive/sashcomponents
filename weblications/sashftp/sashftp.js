var gladefile = new Sash.Glade.GladeFile("sashftp.glade");
var window = gladefile.getWidget("ftp");
var help_window = gladefile.getWidget("help_window");
var open_connection_dialog = gladefile.getWidget("open_connection");
var status_text = gladefile.getWidget("status_text");
var local_list = gladefile.getWidget("local_list");
var remote_list = gladefile.getWidget("remote_list");
var local_dir = gladefile.getWidget("local_dir");
var remote_dir = gladefile.getWidget("remote_dir");
var transfer_list = gladefile.getWidget("transfer_list");
var bookmark_list = gladefile.getWidget("bookmark_list");
var local_sort = 0, remote_sort = 0, double_clicked = false;
var local_path = "", remote_path = "";
var remote_files, remote_dirs, reconnecting = false;
var host, port, username, password, connection_name;
var dont_confirm_delete, overwrite_by_default;
var list_dot_files, automatic_reconnect;
var connection = new Sash.FTP.Connection;
var curr_transfer_num = 0, my_receive_queue, my_send_queue;
init_connection(connection);
display_local(Sash.FileSystem.WeblicationDirectory);
var QUERY_YES = 0, QUERY_NO = 1, QUERY_YES_TO_ALL = 2, QUERY_NO_TO_ALL = 3;
var DIRECTORY = 2, SYMLINK_FILE = 1, SYMLINK_DIRECTORY = 3;
var NO_INFO = 0;
var forced_disconnect = false;

function init_connection(c) {
	c.OnLogin = "on_login";
	c.OnProgress = "on_progress";
	c.OnStatusMessage = "on_status_message";
	c.OnFileSendStart = "on_file_send_start";
	c.OnFileReceiveStart = "on_file_send_start";
	c.OnFileSent = "on_file_sent";
	c.OnFileReceived = "on_file_received";
	c.OnFileSendError = "on_file_send_error";
	c.OnFileReceiveError = "on_file_receive_error";
	c.OnFatalError = "on_fatal_error";
	c.OnTimeout = "on_timeout";
	c.OnConnectionTimeout = "on_connection_timeout";
}

function ConnectionFree(){
	 return (connection.TransferQueue.length == 0 && ! connection.IsTransferring && my_send_queue < 1 && my_receive_queue < 1) ;
}

function isSymLink(str){
	 return (str.indexOf('->') != -1);
}

function linkName(str){
	 if (isSymLink(str)) return (str.substring(0, str.indexOf(' ->')));
	 else return str;
}

function RemoteSymLinkIsDir(str){
	 var dirname = linkName(str);
	 var currdir = remote_dir.getText();
	 status ("Testing symlink: ");
	 if (connection.Cd(dirname)){
		  connection.Cd(currdir);
		  status ("'" + dirname + "' links to a directory.\n");
		  return true;
	 } else {
		  status ("'" + dirname + "' links to a file.\n");
		  return false;
	 }
}

function RemoveTrailingBackslash(str){
	 if (str.charAt(str.length - 1) == '/') return (str.substr(0, str.length-1));
	 else return str;
}

function ChangeRemoteDir (con, newdir, currdir){
	 currdir = RemoveTrailingBackslash(currdir);
	 newdir = RemoveTrailingBackslash(newdir);
	 var fulldir, dotdotdir, retval;
	 if ((newdir.charAt(0) == '/')){
		  retval = con.Cd(newdir);
		  remote_path = newdir;
	 } else {
		  if (currdir == "") currdir = remote_dir.getText();
		  if ((newdir == "../") || (newdir == "..")){
			   dotdotdir = Sash.FileSystem.GetParentFolderName(currdir);
			   retval = con.Cd(dotdotdir);
			   remote_path = dotdotdir;
		  } else {
			   retval = con.Cd(newdir);
			   remote_path = Sash.FileSystem.BuildPath(currdir, newdir);
		  }
	 }
	 remote_dir.setText(remote_path);
	 return retval;
}

function on_login(con, a) {
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
	 if (a) {
		  my_receive_queue = 0;
		  my_send_queue = 0;
		  con.IsASCII = false;
		  con.NotifyInterval_S = 1;
		  con.ShowDotFiles = list_dot_files;
		  
		  var lp = local_path;
		  var rp = remote_path;

		  if (lp == "" || ! reconnecting) 
			  lp = Sash.Registry.QueryWeblicationValue(connection_name, "local_dir");
		  if (rp == "" || ! reconnecting) 
			  rp = Sash.Registry.QueryWeblicationValue(connection_name, "remote_dir");
		  reconnecting = false;
		  if ((rp == null) ||(rp == "")) rp = con.CurrentDirectory;
		  ChangeRemoteDir(con, rp, "");
		  populate_and_display_remote();
		  display_local(lp);
		  window.setTitle("SashFTP - " + connection_name);
	 } else {
		  status("Login failed!\n");
		  con.Disconnect();
		  connection_name = "";
	 }
}

function on_status_message(a, b) {
	 status(b);
}


function RemoveChar(item,OldChar)
{
	 var retval="";
	 var itemchars = item.split("");
	 for(var i=0; i< itemchars.length; i++)
		  if (itemchars[i] != OldChar) retval += itemchars[i];
	 return retval;
}
 
function status(m) {
	 m = RemoveChar(m, '\r');
	 status_text.Append(m);
	 status_text.setPoint(status_text.getLength()-1);
}

function populate_and_display_remote() {
	 populate_remote();
	 display_remote();
}

function populate_remote() {
	 if (! connection.TimedOut){
		  remote_files = [];
		  remote_dirs = [];
		  set_remote_label(remote_path);
		  var files = connection.DetailedList(null);
		  var space = /\s+$/;
		  var reg = /^.+\b(\d+)\s+([A-Za-z]{3})\s+(\d+)\s+(\S+)\s+(.+)$/;
		  //		  var reg = /^.+\b(\d+)\s+([A-Za-z]{3})\s+(\d+)\s+(\S+)\s+(.+)\b(\s*)$/;

		  for (var i = 0; i < files.length; i++) { 
			   files[i] = files[i].replace(/\s+$/g, "");
			   var match = reg.exec(files[i]);
			   if (match) {
					var check_year = /^\d{4}/;
					if (! check_year.test(match[4])) {
						 var now = new Date();
						 match[4] += " " + now.getFullYear();
					}
					if (match[5] == ".." || match[5] == ".") continue;
					if (files[i].charAt(0) == 'd') {
						 remote_dirs.push([match[5], "[dir]", match[2] + " " + match[3] + " " + match[4]]);
					} else if (files[i].charAt(0) == 'l') {
						 remote_files.push([match[5], "[symlink]", match[2] + " " + match[3] + " " + match[4]]);
					} else {
						 remote_files.push([match[5], match[1], match[2] + " " + match[3] + " " + match[4]]);
					}
			   } 
		  }
	 }
}

function display_remote() {
	 remote_list.clear();
	 remote_list.Append(["../", "[dir]", ""]);
	 var func = (remote_sort < 2 ? remote_sort_by_name : (remote_sort < 4 ? remote_sort_by_size : remote_sort_by_date));
	 remote_dirs.sort(func);
	 for (var i = 0; i < remote_dirs.length; i++) {
		  remote_list.Append([remote_dirs[i][0] + "/", remote_dirs[i][1], remote_dirs[i][2]]);
	 }
	 remote_files.sort(func);
	 for (var i = 0; i < remote_files.length; i++) {
		  remote_list.Append([remote_files[i][0], remote_files[i][1], remote_files[i][2]]);
	 }
}

function display_local(dir) {
	 var func = (local_sort < 2 ? sort_by_name : (local_sort < 4 ? sort_by_size : sort_by_date));
	 if (!dir || dir == "") dir = local_path;
	 local_path = dir;
	 local_list.clear();
	 var folder = new Sash.FileSystem.Folder(dir);
	 set_local_label(folder.Path);

	 var subf = folder.SubFolders;
	 var subfolders = new Array();
	 for (var i = 0; i < subf.length; i++)
		  subfolders.push(subf[i]);
	 subfolders.sort(func);
	 local_list.Append(["../", "[dir]", ""]);
	
	 for (var i = 0; i < subfolders.length; i++) {
		  local_list.Append([subfolders[i].ShortName + "/", "[dir]", subfolders[i].DateLastModified]);
	 }

	 var f = folder.Files;
	 var files = new Array();
	 for (var j = 0; j < f.length; j++)
		  files.push(f[j]);
	 files.sort(func);
	 for (var i = 0; i < files.length; i++) {
		  local_list.Append([files[i].ShortName, files[i].Size + "", files[i].DateLastModified]);
	 }
}

function on_refresh_dirs_clicked() 
{
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);	 
	 var dir = local_dir.getText();
	 display_local(dir);
	 populate_and_display_remote();
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function on_quit_clicked() {
	 if (disconnect())
		  Sash.Console.Quit();
}

function mytest(x, y, z) {
	 var way = (z == 1 ? remote_sort : local_sort) % 2 ? -1 : 1;
	 if (x < y) return -1 * way;
	 if (y < x) return 1 * way;
	 return 0;
}

function list_button_press_event(a, num_clicks) {
	 double_clicked = num_clicks == 2;
}

function AllowCopyToRemote(filename, localsize, localdate,
						   remoteData){
	 var retval = QUERY_NO;
	 // figure out whether the file is on the remote side.

	 if (remoteData == NO_INFO) remoteData = find_in_remote(filename);
	 if (remoteData != null){
		  // If there is something on the remote side of the same name.
		  if (remoteData == DIRECTORY) status("Cannot overwrite remote directory '"+filename+"'\n");
 		  else if (remoteData == SYMLINK_DIRECTORY) status("Cannot overwrite remote symlink directory '"+filename+"'\n");
		  else {
			   if (overwrite_by_default) retval = QUERY_YES;
			   else retval = query_overwrite(filename, localsize, localdate, remoteData[0], remoteData[1], 1);
		  }
	 } else retval = QUERY_YES;
	 return retval;
}

function AllowCopyToLocal(filename, lfinfo, remotesize, remotedate){
	 var retval = QUERY_NO;
	 if (lfinfo == NO_INFO) {
		  var newfile = Sash.FileSystem.BuildPath(local_path, linkName(filename));
		  lfinfo = get_local_file_info(newfile);
	 }
	 if (lfinfo != null){
		  if (lfinfo == DIRECTORY) status("Cannot overwrite local directory '"+filename+"'\n");
		  else {
			   if (overwrite_by_default) retval = QUERY_YES;
			   else retval = query_overwrite(linkName(filename), lfinfo[0], lfinfo[1], remotesize, remotedate, 0);
		  }
	 } else retval = QUERY_YES;
	 return retval;
}

function local_list_select_row() {
	 if (! double_clicked) return; // only care about double clicks
	 double_clicked = false;

	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var list_data = get_data_from_selected(local_list, [0, 1, 2]);
	 if (list_data.length != 1) {
		  status(list_data.length + " items selected; not sure what to do!\n");
		  Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
		  return;
	 }
	 if (list_data[0][1] == "[dir]") {
		  if (list_data[0][0] == "../") display_local(Sash.FileSystem.GetParentFolderName(local_path));
		  else display_local(Sash.FileSystem.BuildPath(local_path, list_data[0][0]));
	 } else if (list_data[0][1] == "[symlink]") {
		  // You'll never get here.
		  print ("ERROR: LOCAL SYMLINKS ARE NOT DISPLAYED AS SYMLINKS.");
	 } else {
		  // figure out whether the file is on the remote side.
		  var actr = AllowCopyToRemote(list_data[0][0], list_data[0][1], list_data[0][2], NO_INFO);
		  if ((actr == QUERY_YES) || (actr == QUERY_YES_TO_ALL)){
			   add_to_transfer_list(list_data[0][0], list_data[0][1], 0);
			   if (! connection.TimedOut) connection.Put(Sash.FileSystem.BuildPath(local_path, list_data[0][0]), remote_path);
		  }
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function remote_list_select_row() {
	 if (! double_clicked) return; // only care about double clicks
	 double_clicked = false;
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var list_data = get_data_from_selected(remote_list, [0, 1, 2]);
	 if (list_data.length != 1) {
		  status(list_data.length + " items selected; not sure what to do!\n");
		  Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
		  return;
	 }
	 if (list_data[0][1] == "[symlink]") {
		  if (RemoteSymLinkIsDir(list_data[0][0])){
			   if (check_transfer()) {
					Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
					return;
			   }
			   if (ChangeRemoteDir(connection, linkName(list_data[0][0]), "")) populate_and_display_remote();
		  } else {
			   var actl = AllowCopyToLocal(linkName(list_data[0][0]), NO_INFO, list_data[0][1], list_data[0][2]);
			   if ((actl == QUERY_YES) || (actl == QUERY_YES_TO_ALL)){
					add_to_transfer_list(linkName(list_data[0][0]), list_data[0][1], 1);
					if (! connection.TimedOut) connection.Get(linkName(list_data[0][0]), local_path);
			   }
		  }
	 } else if (list_data[0][1] == "[dir]") {
		  if (check_transfer()) {
			   Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
			   return;
		  }
		  if (ChangeRemoteDir(connection, list_data[0][0], "")) populate_and_display_remote();
	 } else {
		  var actl = AllowCopyToLocal(list_data[0][0], NO_INFO, list_data[0][1], list_data[0][2]);
		  if ((actl == QUERY_YES) || (actl == QUERY_YES_TO_ALL)){
			   add_to_transfer_list(list_data[0][0], list_data[0][1], 1);
			   if (! connection.TimedOut) connection.Get(list_data[0][0], local_path);
		  }
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function sort_by_date(a, b) { return mytest(Date.parse(a.DateLastModified), Date.parse(b.DateLastModified)); }
function sort_by_name(a, b) { return mytest(a.ShortName, b.ShortName); }
function sort_by_size(a, b) { return mytest(a.Size, b.Size); }
function remote_sort_by_name(a, b) { return mytest(a[0], b[0], 1); }
function remote_sort_by_size(a, b) { return mytest(a[1]*1, b[1]*1, 1); }
function remote_sort_by_date(a, b) { return mytest(Date.parse(a[2]), Date.parse(b[2]), 1); }

function set_local_label(a) {local_dir.setText(a);}
function set_remote_label(a) {remote_dir.setText(a);}

function on_local_dir_activate() {
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var dir = local_dir.getText();
	 var old_path = local_path;
	 if (Sash.FileSystem.FolderExists(dir)) display_local(dir);
	 else {
		  status(dir + " is not a directory!\n");
		  local_path = old_path;
		  local_dir.setText(local_path);
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function on_remote_dir_activate() {
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 if (connection.LoggedIn && ! check_transfer()) {
		  var dir = remote_dir.getText();
		  var old_path = remote_path;
		  if (! ChangeRemoteDir(connection, dir, "")) {
			   status(dir + " is not a directory!\n");
			   remote_path = old_path;
			   remote_dir.setText(remote_path);
		  } else populate_and_display_remote();
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function on_local_list_click_column(a, b, c) {
	 local_sort = (local_sort % 2 ? 0 : 1);
	 local_sort += b*2;
	 display_local("");
}

function on_remote_list_click_column(a, b, c) {
	 remote_sort = (remote_sort % 2 ? 0 : 1);
	 remote_sort += b*2;
	 display_remote();
}

function on_help_clicked(){ help_window.showAll(); }
function on_help_ok_clicked() { help_window.visible = false; }

function on_save_dirs_clicked() {
	 if (connection.LoggedIn) {
		  Sash.Registry.SetWeblicationValue(connection_name, "local_dir", local_path);
		  Sash.Registry.SetWeblicationValue(connection_name, "remote_dir", connection.CurrentDirectory);
		  status("Current directories saved as defaults for this connection.\n");
	 } else status("Please establish a connection first!\n");
}

function open_connection_dialog_close() {
	 open_connection_dialog.visible = false;

	return true;
}

function open_connection(a) {
	 open_connection_dialog.visible = false;
	 host = gladefile.getWidget("host").getText();
	 port = gladefile.getWidget("port").getText();
	 username = gladefile.getWidget("username").getText();
	 password = gladefile.getWidget("password").getText();
	 dont_confirm_delete = gladefile.getWidget("dont_confirm_delete").value;
	 overwrite_by_default = gladefile.getWidget("overwrite_by_default").value;
	 list_dot_files = gladefile.getWidget("list_dot_files").value;
	 automatic_reconnect = gladefile.getWidget("automatic_reconnect").value;

	 if (gladefile.getWidget("save_connection").value) {
		  var key = username+"@"+host+":"+port;
		  Sash.Registry.SetWeblicationValue(key, "host", host);
		  Sash.Registry.SetWeblicationValue(key, "port", port);
		  Sash.Registry.SetWeblicationValue(key, "username", username);
		  Sash.Registry.SetWeblicationValue(key, "password", password);
		  Sash.Registry.SetWeblicationValue(key, "dont_confirm_delete", dont_confirm_delete);
		  Sash.Registry.SetWeblicationValue(key, "overwrite_by_default", overwrite_by_default);
		  Sash.Registry.SetWeblicationValue(key, "list_dot_files", list_dot_files);
		  Sash.Registry.SetWeblicationValue(key, "automatic_reconnect", automatic_reconnect);
	 }
	 open_connection_dialog.hide();
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);	 
	 connect(host, port, username, password);
}

function on_connect_clicked() {
	 // display the bookmarks
	 bookmark_list.clear();
	 var keys = Sash.Registry.EnumWeblicationKeys("");
	 for (var i = 0; i < keys.length; i++) {
		  bookmark_list.Append([keys[i]]);				
	 }
	 open_connection_dialog.showAll();
}

function connect(host, port, username, password) {
	 forced_disconnect = false;
	 if (connection.Connected && ! disconnect()) return;
	 if (!connection.Connect(host, port)) {
		  status("Couldn't connect to server.\n");
		  Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
		  on_connect_clicked();
		  return;	
	 }
	 if (username == "") { username = "anonymous"; if (password == "") password = "anon@anon.net" }
	 connection_name = username+"@"+host+":"+port;
	 connection.Login(username, password);
}

function on_delete_bookmark_clicked() {
	 var selected = bookmark_list.SelectedRows;
	 if (selected == null) return;
	 var key = bookmark_list.GetText(selected[0], 0);
	 bookmark_list.RemoveRow(selected[0]);
	 Sash.Registry.DeleteWeblicationKey(key);
}

function on_bookmark_list_button_press_event(a, num_clicks) {
	if (num_clicks == 2) {
		on_bookmark_list_select_row();
		open_connection(true);
	}
}

function on_bookmark_list_select_row() {
	 var selected = bookmark_list.SelectedRows;
	 if (selected == null) return;
	 var key = bookmark_list.GetText(selected[0], 0);

	 gladefile.getWidget("host").setText(Sash.Registry.QueryWeblicationValue(key, "host"));
	 gladefile.getWidget("port").setText(Sash.Registry.QueryWeblicationValue(key, "port"));
	 gladefile.getWidget("username").setText(Sash.Registry.QueryWeblicationValue(key, "username"));
	 gladefile.getWidget("password").setText(Sash.Registry.QueryWeblicationValue(key, "password"));
	 gladefile.getWidget("dont_confirm_delete").value = Sash.Registry.QueryWeblicationValue(key, "dont_confirm_delete");
	 gladefile.getWidget("overwrite_by_default").value = Sash.Registry.QueryWeblicationValue(key, "overwrite_by_default");
	 gladefile.getWidget("list_dot_files").value = Sash.Registry.QueryWeblicationValue(key, "list_dot_files");
	 gladefile.getWidget("automatic_reconnect").value = Sash.Registry.QueryWeblicationValue(key, "automatic_reconnect");

}

function disconnect() {
	 if (connection.Connected) {
		  var prompt = "Are you sure you want to disconnect from '"+connection_name+"'?";
		  var num = connection.TransferQueue.length;
		  if (num > 0) prompt += " There are " + num + " file transfers left.";
		  if (Sash.Linux.Dialog("Are you sure?", prompt, 
								["Button_Yes", "Button_No"], 0, 1) == 0) {
			   connection.Logout();
			   connection_name = "";
//			local_path = "";
			   remote_path = "";
			   clear_remote();
			   window.setTitle("SashFTP");
			   while (curr_transfer_num < transfer_list.rows) {
					transfer_list.setText(curr_transfer_num, 3, "Aborted!");
					curr_transfer_num++;
			   }
			   forced_disconnect = true;
			   return true;
		  } else
			   return false;
	 } else 
		  return true;
}

function clear_remote() {
	 set_remote_label("");
	 remote_list.clear();
}

function get_data_from_selected(listbox, cols) {
	 list = listbox.SelectedRows;
	 var retlist = [];
	 if (! list) return retlist;
	 list.sort();
	 for (var i = 0; i < list.length; i++) {
		  var data = [];
		  for (var j = 0; j < cols.length; j++) data.push(listbox.GetText(list[i], cols[j]));
		  retlist.push(data);
	 }
	 return retlist;
}

function add_to_transfer_list(name, size, isReceive) {
	 transfer_list.Append([name, isReceive ? "Receive":"Send", size, "Queued..."]);
	 transfer_list.moveto(transfer_list.rows-1, 0, 0, 0);
	 if (isReceive) my_receive_queue++;
	 else my_send_queue++;
}

function on_send_clicked() {
	 var response = QUERY_NO;
	 if (! connection.LoggedIn) {
		  status("Can't send files -- please establish a connection first!\n");
		  return;
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var files = get_data_from_selected(local_list, [0, 1, 2]);
	 var file_info = [];
	 for (var i = 0; i < files.length; i++) {
		  if (files[i][1] == "[dir]") {
			   if (files[i][0] == "../") continue;
			   if (connection.TransferQueue.length > 0 || connection.IsTransferring) {
					status("Cannot transfer directory structure '" + files[i][0] + "' until transfer queue is empty. Please try again then.\n");
			   } else {
					var substuff = new Object();
					enumerate_local(local_path, files[i][0], substuff);
					for (l in substuff) {
						 var newdir = Sash.FileSystem.BuildPath(remote_path, l);
						 connection.Mkdir(newdir);
						 if (! overwrite_by_default) {
							  status("Gathering directory and file information for '" + newdir + "'...\n");
							  connection.Cd(newdir);
							  populate_remote();
						 }
						 for (var k = 0; k < substuff[l].length; k++) {
							  file_info.push([Sash.FileSystem.BuildPath(l,substuff[l][k][0]), substuff[l][k][1], substuff[l][k][2], Sash.FileSystem.BuildPath(Sash.FileSystem.BuildPath(local_path, l), substuff[l][k][0]), find_in_remote(substuff[l][k][0])]);
						 }
					}
			   }	
			   if (connection.CurrentDirectory != remote_path) {
					connection.Cd(remote_path);
					populate_remote();
			   }
		  } else file_info.push([files[i][0], files[i][1], files[i][2], Sash.FileSystem.BuildPath(local_path,files[i][0]), find_in_remote(files[i][0])]);
		  
	 }
	 status("Performing file data analysis... \n");
	 file_info.sort(sort_by_overwrite);
	 status("Done.\n");
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);

	 for (var i = 0; ((i < file_info.length) 
					  && (response != QUERY_NO_TO_ALL)); i++) {
		  if (! overwrite_by_default){
			   if (response != QUERY_YES_TO_ALL){
					response = AllowCopyToRemote(file_info[i][0],
												 file_info[i][1],
												 file_info[i][2], 
												 file_info[i][4]);
					if ((response == QUERY_NO_TO_ALL) || (response == QUERY_NO)) continue;
			   }
		  }
		  add_to_transfer_list(file_info[i][0], file_info[i][1], 0);
		  if (! connection.TimedOut) connection.Put(file_info[i][3], Sash.FileSystem.BuildPath(remote_path, Sash.FileSystem.GetParentFolderName(file_info[i][0])));
	 }
	 if (ConnectionFree()) populate_and_display_remote();
}

function on_receive_clicked() {
	 var response = QUERY_NO;
	 if (! connection.LoggedIn) {
		  status("Can't receive files -- please establish a connection first!\n");
		  return;
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var files = get_data_from_selected(remote_list, [0, 1, 2]);
	 var file_info = [];
	 for (var i = 0; i < files.length; i++) {
		  if ((files[i][1] == "[dir]") || 
			  ((files[i][1] == "[symlink]") 
			   && (RemoteSymLinkIsDir(files[i][0])))){
			   if (files[i][0] == "../") continue;
			   if (connection.TransferQueue.length > 0 || connection.IsTransferring) 
					status("Cannot transfer directory structure '" + files[i][0] + "' until transfer queue is empty. Please try again then.\n");
			   else {
					var substuff = new Object();
					enumerate_remote(remote_path, files[i][0], substuff, 0);
					status("Gathering local file information... \n");
					for (l in substuff) {
						 var curr_remote_dir = Sash.FileSystem.BuildPath(remote_path, l);
						 var newdir = Sash.FileSystem.BuildPath(local_path, l);
						 var s = new Sash.FileSystem.Folder(newdir, true);
						 for (var k = 0; k < substuff[l].length; k++) {
							  var fname = substuff[l][k][0];
							  var fullp = Sash.FileSystem.BuildPath(curr_remote_dir, fname);
							  file_info.push([Sash.FileSystem.BuildPath(l, fname), substuff[l][k][1], substuff[l][k][2], fullp, get_local_file_info(Sash.FileSystem.BuildPath(newdir, fname))]);
						 }
						 if (ConnectionFree()) display_local("");
					}
					status("Done.\n");
					connection.Cd(remote_path);
					populate_remote();	
			   }	
		  } else {
			   file_info.push([linkName(files[i][0]), files[i][1], files[i][2], Sash.FileSystem.BuildPath(remote_path,linkName(files[i][0])), get_local_file_info(Sash.FileSystem.BuildPath(local_path, linkName(files[i][0])))]);
		  }
	 }
	 status("Performing file data analysis... \n");
	 file_info.sort(sort_by_overwrite);
	 status("Done.\n");
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);

	 for (var i = 0; ((i < file_info.length)
					  && (response != QUERY_NO_TO_ALL)); i++) {
		  if (! overwrite_by_default && response != QUERY_YES_TO_ALL) {
			   if (file_info[i][4] == 2) continue;
			   else if (file_info[i][4] != null){
					response = AllowCopyToLocal(linkName(file_info[i][0]), 
												file_info[i][4], 
												file_info[i][1], 
												file_info[i][2]);
					if ((response == QUERY_NO) || (response == QUERY_NO_TO_ALL)) continue;
			   }
		  }
		  add_to_transfer_list(file_info[i][0], file_info[i][1], 1);
		  if (! connection.TimedOut) connection.Get(file_info[i][3], Sash.FileSystem.BuildPath(local_path, Sash.FileSystem.GetParentFolderName(file_info[i][0])));
	 }
}

function get_local_file_info(p) {
	 p = linkName(p);
	 if (Sash.FileSystem.FolderExists(p)) return DIRECTORY;
	 if (Sash.FileSystem.FileExists(p)) {
		  var f = new Sash.FileSystem.File(p);
		  return [f.Size, f.DateLastModified];
	 }
	 return null;
}

function query_overwrite(name, local_size, local_date, remote_size, remote_date, to_remote) {
	 var prompt = "Overwrite the " + (to_remote ? "remote" : "local") + " file '"+name+"'?\n"
		  prompt += "\nLocal: " + local_size + " bytes, " + local_date;
	 prompt += "\nRemote: " + remote_size + " bytes, " + remote_date;
	 return Sash.Linux.Dialog("Overwrite file?", prompt,
							  ["Button_Yes", "Button_No", "Yes to All", "No to All"], 1, 1);
}

function sort_by_overwrite(a, b) {
	 if (! a[4] && b[4]) return -1;
	 if (! b[4] && a[4]) return 1;
	 return 0;
}

function find_in_remote(a) {
	 for (var l = 0; l < remote_dirs.length; l++)
		  if (a == remote_dirs[l][0]) { return DIRECTORY; }

	 for (var j = 0; j < remote_files.length; j++){
		  if ((remote_files[j][1] == "[symlink]") 
			  && (a == linkName(remote_files[j][0]))){
			   if (RemoteSymLinkIsDir(linkName(remote_files[j][0]))){
					return SYMLINK_DIRECTORY;
			   } else {
					return SYMLINK_FILE;
			   }
		  } 
		  if (a == linkName(remote_files[j][0])) return [remote_files[j][1], remote_files[j][2]]; 
	 }
	 return null;
}

function on_file_send_start(con, file) {
	 transfer_list.setText(curr_transfer_num, 3, "Starting transfer...");
}

function on_file_sent(con, file, amt, seconds) {
	 transfer_list.setText(curr_transfer_num, 3, "Finished! (" + (Math.round(amt/seconds/1000) + "") + " K/s)");
	 transfer_list.moveto(curr_transfer_num, 0, 0, 0);
	 curr_transfer_num++;
	 my_send_queue--;
	 if (ConnectionFree()) {
		  my_send_queue = 0;
		  populate_and_display_remote();
	 }
}

function on_file_received(con, file, amt, seconds) {
	 transfer_list.setText(curr_transfer_num, 3, "Finished! (" + (Math.round(amt/seconds/1000) + "") + " K/s)");
	 transfer_list.moveto(curr_transfer_num, 0, 0, 0);
	 curr_transfer_num++;
	 my_receive_queue--;
	 if (ConnectionFree()){
		  my_receive_queue = 0;
		  display_local("");
	 }
}

function on_file_receive_error(con, file, error) {
	 transfer_list.setText(curr_transfer_num, 3, "Error transmitting file: " + error);
	 transfer_list.moveto(curr_transfer_num, 0, 0, 0);
	 curr_transfer_num++;
	 my_receive_queue--;
}

function on_file_send_error(con, file, error) {
	 transfer_list.setText(curr_transfer_num, 3, "Error transmitting file: " + error);
	 transfer_list.moveto(curr_transfer_num, 0, 0, 0);
	 curr_transfer_num++;
	 my_send_queue--;
}

function on_progress(con, file, amt, seconds) {
	 var rate = amt/seconds;
	 var size = transfer_list.GetText(curr_transfer_num, 2) * 1;
	 var time_left = (size-amt)/rate;
	 transfer_list.setText(curr_transfer_num, 3, amt + " bytes transferred (" + (Math.round(rate/1000) + "") + " K/s; " + (Math.round(time_left) + "") + "s left)");
	 transfer_list.moveto(curr_transfer_num, 0, 0, 0);
}

function on_delete_local_clicked() {
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var stuff = get_data_from_selected(local_list, [0, 1]);
	 var disp = "", count = 0;
	 for (var i = 0; i < stuff.length; i++) {
		  if (stuff[i][0] != "../") {
			   disp += stuff[i][0] + "\n";
			   count++;
		  }
	 }
	 if (disp == "") {
		  status("Please make a selection first!\n");
		  Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
		  return;
	 }
	 if (dont_confirm_delete || 
		 Sash.Linux.Dialog("Are you sure?", 
						   "Are you sure you want to recursively delete the following " + count + " files and/or directories?\n\n" + disp,
						   ["Button_Yes", "Button_No"], 1, 1) == 0) {
		  for (var i = 0; i < stuff.length; i++) {
			   if (stuff[i][0] != "../") {
					if (stuff[i][1] == "[dir]") 
						 Sash.FileSystem.DeleteFolder(Sash.FileSystem.BuildPath(local_path, stuff[i][0]), true);
					else 
						 Sash.FileSystem.DeleteFile(Sash.FileSystem.BuildPath(local_path, stuff[i][0]), true);
			   }
		  }
		  display_local("");
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function on_rename_local_clicked() {
	 var stuff = get_data_from_selected(local_list, [0, 1]);
	 for (var i = 0; i < stuff.length; i++) {
		  if (stuff[i][0] == "../") continue;
		  var prm = "Rename the " + (stuff[i][1] == "[dir]" ? "directory" : "file") + " '" + stuff[i][0] + "' to :";
		  var fname = Sash.Core.UI.PromptForValue("Rename", prm, stuff[i][0]);
		  if (fname == "") return;
		  if (stuff[i][1] == "[dir]") 
			   Sash.FileSystem.MoveFolder(Sash.FileSystem.BuildPath(local_path, stuff[i][0]), Sash.FileSystem.BuildPath(local_path, fname));
		  else 
			   Sash.FileSystem.MoveFile(Sash.FileSystem.BuildPath(local_path, stuff[i][0]), Sash.FileSystem.BuildPath(local_path, fname));
		  
		  display_local("");
	 }
}

function on_create_local_clicked() {
	 var fname = Sash.Core.UI.PromptForValue("Create local directory", "Enter the name of the directory you wish to create:", "");
	 if (fname == "") return;
	 var fol = new Sash.FileSystem.Folder(Sash.FileSystem.BuildPath(local_path, fname), true);
	 display_local("");
}

function on_create_remote_clicked() {
	 if (check_transfer()) return;
	 var fname = Sash.Core.UI.PromptForValue("Create remote directory", "Enter the name of the directory you wish to create:", "");
	 if (fname == "") return;
	 connection.Mkdir(fname);
	 populate_and_display_remote();
}

function on_delete_remote_clicked() {
	 if (check_transfer()) return;
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
	 var stuff = get_data_from_selected(remote_list, [0, 1]);
	 var disp = "", count = 0;
	 var dir_files = [];
	 for (var i = 0; i < stuff.length; i++) {
		  if (stuff[i][0] == "../") continue;
		  if (stuff[i][1] == "[dir]") {
			   enumerate_remote(remote_path, stuff[i][0], dir_files, 1);	
			   connection.Cd(remote_path);
			   populate_remote();
		  }
		  disp += stuff[i][0] + "\n";
		  count++;
	 }
	 if (disp == "") {
		  status("Please make a selection first!\n");
		  Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
		  return;
	 }
	 if (dont_confirm_delete || Sash.Linux.Dialog("Are you sure?", 
												  "Are you sure you want to recursively delete the following " + count + " files and/or directories?\n\n" + disp,
												  ["Button_Yes", "Button_No"], 1, 1) == 0) {
		  var dir_sorted = [];
		  for (var i in dir_files) {
			   dir_sorted.push(i);
		  }
		  dir_sorted.sort();
		  dir_sorted.reverse();
		  for (var i in dir_sorted) {
			   for (var j in dir_files[dir_sorted[i]]) {
					connection.Del(Sash.FileSystem.BuildPath(dir_sorted[i], dir_files[dir_sorted[i]][j][0]));
			   }
			   status("Trying to remove remote directory " + dir_sorted[i] + "\n");

			   if (!connection.Rmdir(dir_sorted[i])) status("Could not remove remote dir " + dir_sorted[i] + "!" + "\n");
		  }
		  for (var i = 0; i < stuff.length; i++) {
			   if (stuff[i][0] == "../") continue;
			   if (stuff[i][1] == "[dir]") continue;

			   connection.Del(linkName(stuff[i][0]));
		  }
		  populate_and_display_remote();
	 }
	 Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.DEFAULT);
}

function on_rename_remote_clicked() {
	 if (check_transfer()) return;
	 var stuff = get_data_from_selected(remote_list, [0, 1]);
	 for (var i = 0; i < stuff.length; i++) {
		  if (stuff[i][1] == "[symlink]"){
			   status("You can't rename a symlink.\n");
			   continue;
		  }

		  if (stuff[i][0] == "../") continue;
		  var prm = "Rename the " + (stuff[i][1] == "[dir]" ? "directory" : "file") + " '" + stuff[i][0] + "' to :";
		  var fname = Sash.Core.UI.PromptForValue("Rename", prm, stuff[i][0]);
		  if (fname == "") return;
		  connection.Rename(stuff[i][0], fname);
		  populate_and_display_remote();
	 }
}

function check_transfer() {
	 if (! connection.IsTransferring) return false;
	 status("Please wait until the current transfer is finished before trying a remote operation.\n");
	 return true;
}

function enumerate_local(base_path, curr_path, data) {
	 var mypath = Sash.FileSystem.BuildPath(base_path, curr_path);
	 if (Sash.FileSystem.IsSymlink(mypath)) {
		  status("Ignoring '" + mypath + "': is a symlink\n");
		  return;
	 }
	 data[curr_path] = [];
	 if (! Sash.FileSystem.FolderExists(mypath)) return;
	 var f = new Sash.FileSystem.Folder(mypath);
	 var subfolders = f.SubFolders;
	 for (var i = 0; i < subfolders.length; i++) {
		  enumerate_local(base_path, Sash.FileSystem.BuildPath(curr_path, subfolders[i].Name), data);
	 }
	 var files = f.Files;
	 for (var i = 0; i < files.length; i++) {
		  data[curr_path].push([files[i].Name, files[i].Size + "", files[i].DateLastModified]);
	 }
}

function enumerate_remote(base_path, curr_path, data, isForDelete) {
	 curr_path = linkName(curr_path);
	 var mypath = Sash.FileSystem.BuildPath(base_path, curr_path);
	 data[curr_path] = [];
	 status("Gathering directory and file information for '" + mypath + "'..\n");

	 if (! ChangeRemoteDir(connection, curr_path, base_path)) {
		  ChangeRemoteDir(connection, base_path, "");
		  return;
	 } 

	 populate_remote();
	 var tempstr;
	 var my_dirs = [];
	 for (var j = 0; j < remote_files.length; j++) {
		  if (remote_files[j][1] == "[symlink]") {
			   if (RemoteSymLinkIsDir(remote_files[j][0])){
					if (! isForDelete){
						 tempstr = linkName(remote_files[j][0]);
						 my_dirs.push(tempstr);
					}
			   } else 
					data[curr_path].push([linkName(remote_files[j][0]), remote_files[j][1], remote_files[j][2]]);
		  } else 
			   data[curr_path].push([linkName(remote_files[j][0]), remote_files[j][1], remote_files[j][2]]);
	 }

	 for (var i = 0; i < remote_dirs.length; i++) { 
		  my_dirs.push(remote_dirs[i][0]);
	 }
	
	 for (var i = 0; i < my_dirs.length; i++) { 
		  ChangeRemoteDir(connection, base_path, "");
		  enumerate_remote(base_path, Sash.FileSystem.BuildPath(curr_path, my_dirs[i]), data, isForDelete);
	 }
	 ChangeRemoteDir(connection, base_path, "");
}

function Reconnect(){
	 if (automatic_reconnect){
		  Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
		  status("Attempting to reconnect to " + host + "...\n");
		  reconnecting = true;
		  connect(host, port, username, password);	
	 } else {
		  var prompt = "Disconnected. Do you want to reconnect to '"+connection_name+"'?";
		  var num = connection.TransferQueue.length;
		  if (num > 0) prompt += " There are " + num + " file transfers left.";
		  if (Sash.Linux.Dialog("Are you sure?", prompt, 
								["Button_Yes", "Button_No"], 0, 1) == 0) {
			   Sash.Core.Cursor.SetStandard(Sash.Core.Cursor.WAIT);
			   status("Attempting to reconnect...\n");
			   reconnecting = true;
			   connect(host, port, username, password);	
		  } else {
			   connection_name = "";
			   local_path = "";
			   remote_path = "";
			   clear_remote();
			   window.setTitle("SashFTP");
			   while (curr_transfer_num < transfer_list.rows) {
					transfer_list.setText(curr_transfer_num, 3, "Aborted!");
					curr_transfer_num++;
			   }
		  }
	 }
}

function on_stop_transfer_clicked()  {
	 if (connection.IsTransferring && Sash.Linux.Dialog("Are you sure?", 
														"Are you sure you want to stop the current transfer?",
														["Button_Yes", "Button_No"], 1, 1) == 0) {
		  connection.StopCurrentTransfer();
		  transfer_list.setText(curr_transfer_num, 3, "Aborted!");
		  if (curr_transfer_num < transfer_list.rows) curr_transfer_num++;
		  if (connection.TransferQueue.length == 0){
			   while (connection.IsTransferring){}
			   if (! connection.TimedOut){
					populate_and_display_remote();
			   }
		  }
	 }
}

function on_timeout(con) {
	 status("Disconnect forced by server (timeout).\n");
	 print ("Timed out.");
	 con.Disconnect();
	 if (! forced_disconnect) Reconnect();
}

function on_connection_timeout(con) {
	 status("Couldn't connect to server (timeout).\n");
	 print ("Timed out.");
	 con.Disconnect();
	 on_connect_clicked();
}

function on_fatal_error(con, message) {
	 status(message);
	 status("Disconnect forced by server.\n");
	 con.Disconnect();
	 clear_remote();
	 if (! forced_disconnect) Reconnect();
}

on_connect_clicked();
