/*

// can't destroy windows.

org tree
// get rid of subtrees
// clean up data...

// edit favorites
// edit john3
// apply
// close
// run john3
// try to customize

*/

var gladefile = new Sash.Glade.GladeFile("bpl.glade");

// Widgets
// Search
var SearchFieldOption = gladefile.getWidget("SearchFieldOption");
var SearchData = gladefile.getWidget("SearchData");
var SearchHistoryCombo = gladefile.getWidget("SearchHistoryCombo");
var FavoritesSearchOption = gladefile.getWidget("FavoritesSearchOption");
var FiltersSearchOption = gladefile.getWidget("FiltersSearchOption");
var Search = gladefile.getWidget("Search");
var Cancel = null;
var SearchButtonVBox = gladefile.getWidget("SearchButtonVBox");
var QuickListStatusLabel = gladefile.getWidget("QuickListStatusLabel");

// SearchResults
var SearchResultsCList = gladefile.getWidget("SearchResultsCList");
var SearchResultsWindow = gladefile.getWidget("SearchResultsWindow");

// Details
var DetailsVBox = gladefile.getWidget("DetailsVBox");
var DetailsCloseOption = gladefile.getWidget("DetailsCloseOption");
var DetailsGoToOption = gladefile.getWidget("DetailsGoToOption");

// OrgTree
var OrgTree = gladefile.getWidget("OrgTree");

var ManageASWindow = gladefile.getWidget("ManageASWindow");
var ManageASNameCList = gladefile.getWidget("ManageASNameCList");
var ManageASDescription = gladefile.getWidget("ManageASDescription");
var ManageASNew = gladefile.getWidget("ManageASNew");
var ManageASCopy = gladefile.getWidget("ManageASCopy");
var ManageASCopyAndEdit = gladefile.getWidget("ManageASCopyAndEdit");
var ManageASEdit = gladefile.getWidget("ManageASEdit");
var ManageASRemove = gladefile.getWidget("ManageASRemove");

var EditASWindow = gladefile.getWidget("EditASWindow"); 
var EditASAllRulesCList = gladefile.getWidget("EditASAllRulesCList");
var EditASSelectedRulesCList = gladefile.getWidget("EditASSelectedRulesCList");
var EditASSelect = gladefile.getWidget("EditASSelect"); 
var EditASUnselect = gladefile.getWidget("EditASUnselect"); 
var EditASMatchAll = gladefile.getWidget("EditASMatchAll"); 
var EditASMatchAny = gladefile.getWidget("EditASMatchAny"); 
var EditASEditRule = gladefile.getWidget("EditASEditRule");
var EditASName = gladefile.getWidget("EditASName");

var ASNameWindow = gladefile.getWidget("ASNameWindow");
var ASNameEntry = gladefile.getWidget("ASNameEntry");
var ASNameOperation = gladefile.getWidget("ASNameOperation");
var ASNameOldName = gladefile.getWidget("ASNameOldName");

// Edit Rule Screen
var EditRuleWindow = gladefile.getWidget("EditRuleWindow");
var EditRuleLabel = gladefile.getWidget("EditRuleLabel");
var EditRuleValuesCList = gladefile.getWidget("EditRuleValuesCList");
var EditRuleValueEntry = gladefile.getWidget("EditRuleValueEntry");
var EditRuleIndex = gladefile.getWidget("EditRuleIndex");
var EditRuleSave = gladefile.getWidget("EditRuleSave");
var EditRuleCancel = gladefile.getWidget("EditRuleCancel");
var EditRuleMatchAll = gladefile.getWidget("EditRuleMatchAll");
var EditRuleMatchAny = gladefile.getWidget("EditRuleMatchAny");

// BPL Options
var BPLOptionWindow = gladefile.getWidget("BPLOptionWindow");
var BPLOptionServerName = gladefile.getWidget("BPLOptionServerName");
var BPLOptionServerBaseDN = gladefile.getWidget("BPLOptionServerBaseDN");
var BPLOptionQLMaxResults = gladefile.getWidget("BPLOptionQLMaxResults");
var BPLOptionDisplayType = gladefile.getWidget("BPLOptionQLMaxResults");
var BPLOptionGroupMaxResults = gladefile.getWidget("BPLOptionGroupMaxResults");
var BPLOptionDisplayType = gladefile.getWidget("BPLOptionDisplayType");

var QLColumnWindow = gladefile.getWidget("QLColumnWindow");
var QLColumnsAllCList = gladefile.getWidget("QLColumnsAllCList");
var QLColumnsDisplayCList = gladefile.getWidget("QLColumnsDisplayCList");
var QLColumnWidth = gladefile.getWidget("QLColumnWidth");
var QLColumnWidthUpdate = gladefile.getWidget("QLColumnWidthUpdate");
var QLColumnMoveUp = gladefile.getWidget("QLColumnMoveUp");
var QLColumnMoveDown = gladefile.getWidget("QLColumnMoveDown");

// Export Results
var ExportDialog = gladefile.getWidget("ExportDialog");
var ExportFilename = gladefile.getWidget("ExportFilename");
var ExportDelimiter = gladefile.getWidget("ExportDelimiter");
var ExportNullText = gladefile.getWidget("ExportNullText");
var ExportAppend = gladefile.getWidget("ExportAppend");
var ExportOverwrite = gladefile.getWidget("ExportOverwrite");
var ExportIncludeTitles = gladefile.getWidget("ExportIncludeTitles");
var ExportFileSelect = gladefile.getWidget("ExportFileSelect");

// About Window
var AboutWindow = gladefile.getWidget("AboutWindow");
var AboutOK = gladefile.getWidget("AboutOK");

// Constants.
// Used to specify whether to prepend or append nodes to
// the organizational chart.
var tempcount = 0;
var ORGTREE_APPEND = tempcount++;
var ORGTREE_PREPEND = tempcount++;

// Used to specify which type of search to use.
tempcount = 0;
var SEARCH_TYPE_EQUALS = tempcount++;
var SEARCH_TYPE_CONTAINS = tempcount++;
var SEARCH_TYPE_STARTSWITH = tempcount++;
var SEARCH_TYPE_ENDSWITH = tempcount++;
var SEARCH_TYPE_ISNOT = tempcount++;

// Used to specify whether search criteria should be ANDed or 
// ORed together.
tempcount = 0;
var OPERATOR_AND = tempcount++;
var OPERATOR_OR = tempcount++;

// Used to index into an array holding rule information.
tempcount = 0;
var RULES_OPERATOR = tempcount++;
var RULES_DISPLAY_NAME = tempcount++;
var RULES_SEARCH_TYPE = tempcount++;
var RULES_VALUES = tempcount++;
var RULES_INDEX = tempcount++;

// Used to specify rule management options.
tempcount = 0;
var OPERATION_NEW = tempcount++;
var OPERATION_COPY = tempcount++;
var OPERATION_EDIT = tempcount++;
var OPERATION_COPY_AND_EDIT = tempcount++;
var OPERATION_RENAME = tempcount++;

// Used to specify file access type when exporting results.
tempcount = 0;
var EXPORT_FILE_APPEND = tempcount++;
var EXPORT_FILE_OVERWRITE = tempcount++;
var EXPORT_FILE_NONE = tempcount++;

// Used to specify the type of mouse cursor to use.
var MOUSE_WAIT = Sash.Core.Cursor.WAIT;
var MOUSE_DEFAULT = Sash.Core.Cursor.DEFAULT;

// Used to specify special fields in option menus.
var OPTION_CUSTOMIZE = -1;
var OPTION_TITLE = -3;

tempcount = 0;
var DETAIL_LABELS = tempcount++;
var DETAIL_TEXTENTRIES = tempcount++;

tempcount = 0;
var GTK_WIDGET_TEXTAREA = tempcount++;
var GTK_WIDGET_TEXTENTRY = tempcount++;
var GTK_WIDGET_LABEL = tempcount++;

// Used to specify the maximum number of people in a group (LDAP requires
// an upper bound on the number of search results to return).
var MAX_GROUP_SIZE = 100;

// key = bluepages attribute name,
// value = display name
// NOTE: currently the paired values are not tracked, so they must be
// added as single entries individually.
var AttributeNameMapping = new Object();
AttributeNameMapping["Callup Name"] = "callupname";
AttributeNameMapping["Name"] = "cn";
AttributeNameMapping["Last Name"] = "sn";
AttributeNameMapping["First Name"] = "givenname";
AttributeNameMapping["Country"] = "co";
AttributeNameMapping["Country Code"] = "employeecountrycode";
AttributeNameMapping["Department"] = "dept";
AttributeNameMapping["Directory"] = "directoryalias";
AttributeNameMapping["Division"] = "div";
AttributeNameMapping["Email"] = "emailaddress";
AttributeNameMapping["Country Code"] = "employeecountrycode";
AttributeNameMapping["Fax"] = "facsimiletelephonenumber";
AttributeNameMapping["Fax Tie Line"] = "facsimiletieline";
AttributeNameMapping["Is Manager"] = "ismanager";
AttributeNameMapping["Job Responsibility"] = "jobresponsibilities";
AttributeNameMapping["Mobile Phone"] = "mobile";
AttributeNameMapping["Notes Mail"] = "notesemail";
AttributeNameMapping["Office"] = "physicaldeliveryofficename";
AttributeNameMapping["Pager"] = "pager";
AttributeNameMapping["Pager ID"] = "pagerid";
AttributeNameMapping["Pager Type"] = "pagertype";
AttributeNameMapping["External Phone"] = "telephonenumber";
AttributeNameMapping["Tie Line"] = "tieline";
AttributeNameMapping["Work Location"] = "workloc";
AttributeNameMapping["Mail Drop"] = "internalmaildrop";
AttributeNameMapping["User ID"] = "primaryuserid";
AttributeNameMapping["UID"] = "uid";
AttributeNameMapping["Alternate User ID"] = "alternateuserid";
AttributeNameMapping["IBM Serial Number"] = "ibmserialnumber";
AttributeNameMapping["Serial Number"] = "serialnumber";
AttributeNameMapping["Employee Type"] = "employeetype";
AttributeNameMapping["Shift"] = "shift";
AttributeNameMapping["Building"] = "buildingname";
AttributeNameMapping["Floor"] = "floor";
AttributeNameMapping["Work Location DN"] = "worklocation";
AttributeNameMapping["Manager DN"] = "manager";
AttributeNameMapping["Manager Serial Number"] = "managerserialnumber";
AttributeNameMapping["Manager Country Code"] = "managercountrycode";
AttributeNameMapping["Secretary DN"] = "secretary";
AttributeNameMapping["Div/Dept DN"] = "divdept";
AttributeNameMapping["Phone/Tie-Line"] = ["telephonenumber", "tieline"];
AttributeNameMapping["Fax/Tie-Line"] = ["facsimiletelephonenumber", "facsimiletieline"];
AttributeNameMapping["Building/Floor"] = ["buildingname", "floor"];

var AttributeDisplayNameArray = [];
var AttributeActualNameArray = [];
for (var i in AttributeNameMapping){
	 var val = AttributeNameMapping[i];
	 if (typeof(val) == "string"){
		  AttributeDisplayNameArray.push(i);
		  AttributeActualNameArray.push(val);
	 }
}

// Name => field that specifies the DN, filter to use (I couldn't find a
// dummy filter that would just let you return all children), and
// an array of attributes to return.
var AdditionalDetailsMapping = new Object();

// NOTE: the filter (item at index 1) is obsolete.
AdditionalDetailsMapping["Business Address"] = ["Work Location DN",
											 "DUMMYWORK",
											 "address1",
											 "address2", "l",
											 "st", "postalcode"];
AdditionalDetailsMapping["Manager"] = ["Manager DN", "DUMMYMANAGER", "cn", "uid"];
AdditionalDetailsMapping["Secretary"] = ["Secretary DN", "DUMMYSECRETARY", "cn", "uid"];
AdditionalDetailsMapping["Div/Dept"] = ["Div/Dept DN", "DUMMYDEPT", "title"];

// For a quicklist search, the user gets to specify which field he wants 
// to search. Each field can correspond to a function, an array, or 
// a string.
//
// Given a field, first check AttributeSearchFunctionMapping to see whether'
// there is a function specified for a particular search. The function
// will generate a custom filter.
//
// If there is no function specified, then check AttributeFilterMapping
// to see whether an array is specified for a particular search. The
// array contains the fields to compare some single data value, resulting
// in a filter that looks like "((field1=value) || (field2=value) || ... )"
//
// If there is no array specified, then check AttributeNameMapping
// to see whether a string is specified for a particular search. The string
// corresponds to the field to compare the value to, so the filter will
// look like "(field=value)"
var AttributeSearchFunctionMapping = new Object();
AttributeSearchFunctionMapping["Notes Mail"] = FilterNotesMail;

var AttributeFilterMapping = new Object();
AttributeFilterMapping["Name"] = ["cn", "sn", "givenname", "callupname"];

var SearchableFields = ["Name",
						"Notes Mail",
						"Email",
						"User ID",
						"Alternate User ID",
						"Job Responsibility",
						"Department",
						"Tie Line",
						"External Phone",
						"Serial Number"];

var QuickListAttributes = [
	 "Name", 		
	 "Callup Name",
	 "Job Responsibility", 
	 "Is Manager", 
	 "Email", 
	 "Notes Mail", 
	 "External Phone", 
	 "Tie Line", 
	 "Fax", 
	 "Fax Tie Line", 
	 "Mobile Phone", 
	 "Pager", 
	 "Pager ID", 
	 "Pager Type", 
	 "Work Location",
	 "Country",	
	 "Country Code", 
	 "Mail Drop",
	 "Office", 
	 "Directory",
	 "Department",
	 "Division"
	 ];

var SearchableFields = ["Name",
						"Notes Mail",
						"Email",
						"User ID",
						"Alternate User ID",
						"Job Responsibility",
						"Department",
						"Tie Line",
						"External Phone",
						"Serial Number"];
var DetailHead = ["Name",
				  "Job Responsibility",
				  "Phone/Tie-Line",
				  "Fax/Tie-Line",
				  "Pager",
				  "Notes Mail",
				  "Email"];
//				  "SameTime"];
var DetailColumn1 = ["Business Address",
					 "Manager",
					 "Secretary",
					 "Backup",
					 "Work Location",
					 "Building/Floor",
					 "Office",
					 "Mail Drop"];
var DetailColumn2 = ["Alternate Phone",
					 "Mobile Phone",
					 "Is Manager",
					 "Employee Type",
					 "Country",
					 "Shift",
					 "Directory",
					 "Div/Dept"];

var WidgetNameMapping = new Object();
WidgetNameMapping["Name"] = "DetailName";
WidgetNameMapping["Job Responsibility"] = "DetailJobResponsibilities";
WidgetNameMapping["Phone/Tie-Line"] = "DetailPhone";
WidgetNameMapping["Fax/Tie-Line"] = "DetailFax";
WidgetNameMapping["Pager"] = "DetailPager";
WidgetNameMapping["Notes Mail"] = "DetailNotes";
WidgetNameMapping["Email"] = "DetailInternet";
WidgetNameMapping["SameTime"] = "DetailSameTime";
WidgetNameMapping["Business Address"] = "DetailBusinessAddress";
WidgetNameMapping["Manager"] = "DetailManager";
WidgetNameMapping["Secretary"] = "DetailSecretary";
WidgetNameMapping["Backup"] = "DetailBackup";
WidgetNameMapping["Work Location"] = "DetailWorkLocation";
WidgetNameMapping["Building/Floor"] = "DetailBuildingFloor";
WidgetNameMapping["Office"] = "DetailOffice";
WidgetNameMapping["Mail Drop"] = "DetailMailDrop";
WidgetNameMapping["Alternate Phone"] = "DetailAlternate";
WidgetNameMapping["Mobile Phone"] = "DetailMobile";
WidgetNameMapping["Is Manager"] = "DetailIsManager";
WidgetNameMapping["Employee Type"] = "DetailEmployeeType";
WidgetNameMapping["Country"] = "DetailCountry";
WidgetNameMapping["Shift"] = "DetailShift";
WidgetNameMapping["Directory"] = "DetailDirectory";
WidgetNameMapping["Div/Dept"] = "DetailDivDept";

var WidgetTypeMapping = new Object();
WidgetTypeMapping["Business Address"] = GTK_WIDGET_TEXTAREA;
WidgetTypeMapping["Div/Dept"] = GTK_WIDGET_TEXTAREA;

function GetWidgetNameMapping(name, detailstype){
	 if (detailstype == DETAIL_LABELS){
		  return WidgetNameMapping[name];
	 }else{
		  if (WidgetTypeMapping[name] == GTK_WIDGET_TEXTAREA){
			   return (WidgetNameMapping[name]);
		  } else {
			   return (WidgetNameMapping[name] + "TE");
		  }
	 }
}

var OrgTreeAttributes = ["Name", 
						 "UID",
						 "Manager DN", 
						 "Div/Dept DN",
						 "Is Manager",
						 "Department",
						 "IBM Serial Number",
						 "Serial Number",
						 "Country Code",
						 "Manager Serial Number",
						 "Manager Country Code"];

var AllFiltersCheckButtons = [];
var QLColumnsCheckButtons = [];

var DefaultBluePagesServerName = "bluepages.ibm.com";
var DefaultBluePagesBaseDN = "ou=bluepages,o=ibm.com";
var DefaultBluePagesMaxResults = 50;
var DefaultBluePagesGroupMaxResults = 100;
var DefaultBluePagesDisplayType = DETAIL_LABELS;

var DefaultExportFilename = "results.txt";
var DefaultExportDelimiter = ", ";
var DefaultExportNullText = "null";
var DefaultExportFilemode = EXPORT_FILE_APPEND;
var DefaultExportIncludeTitles = true;

var DefaultQLColumnWidths = new Object();
DefaultQLColumnWidths["Callup Name"] = 150;
DefaultQLColumnWidths["Country"] = 90;	
DefaultQLColumnWidths["Name"] = 150; 		
DefaultQLColumnWidths["Department"] = 90;
DefaultQLColumnWidths["Directory"] = 90;
DefaultQLColumnWidths["Division"] = 60;
DefaultQLColumnWidths["Email"] = 90; 
DefaultQLColumnWidths["Country Code"] = 90; 
DefaultQLColumnWidths["Fax"] = 115; 
DefaultQLColumnWidths["Fax Tie Line"] = 90; 
DefaultQLColumnWidths["Is Manager"] = 90; 
DefaultQLColumnWidths["Job Responsibility"] = 200; 
DefaultQLColumnWidths["Mobile Phone"] = 90; 
DefaultQLColumnWidths["Notes Mail"] = 90; 
DefaultQLColumnWidths["Pager"] = 115; 
DefaultQLColumnWidths["Pager ID"] = 90; 
DefaultQLColumnWidths["Pager Type"] = 90; 
DefaultQLColumnWidths["Office"] = 90; 
DefaultQLColumnWidths["External Phone"] = 115; 
DefaultQLColumnWidths["Tie Line"] = 90; 
DefaultQLColumnWidths["Work Location"] = 90;
DefaultQLColumnWidths["Mail Drop"] = 90;


// Global variables.
var QuickListSearchResultCount = 0;
var CurrentSearchField = 0;
var CurrentSortType = Sash.GTK.SORT_DESCENDING;

var clist_double_clicked = false;
var ctree_double_clicked = false;
var qlcolumnsall_double_clicked = false;
var qlcolumnssel_double_clicked = false;
var editasall_double_clicked = false;
var editassel_double_clicked = false;
var manageassel_double_clicked = false;

var ActiveFilter = null;
var CurrentTreeItemParams = null;
var ManageASType = "Favorites";

var LDAPConnection = null;
var SearchRequest;

// Save search history.
var SearchHistory = [];

var OrgTreeIdentifier = /^Managed by /;
var OrgTreeIDString = "Managed by ";

// Mapping from name to entry.
var CurrentOpenProfiles = new Object();
var ProfileOrdering = [];
var ProfileOrderingNames = [];
var CurrentProfileIndex = -1;
var NumProfiles = 0;

// Map ID numbers to the request.
var AdditionalDetailsOpenRequests = new Object();

// Cache the additional data.
// Map it so that if the baseDN's match, you can use the same data.
// (Same address, same secretary, etc.)
var AdditionalDetailsCache = new Object();

// Save manager information. 
// Map from UID to the entry containing information (such as
// Name, manager UID).
var ManagerCache = new Object();
var ManagerCycleCheck = new Object();
//var ManagerCache = new Sash.Linux.Collection();

// Correlate a manager's UID to the tree containing the people that he or she
// manages.
var ManagerNodeMapping = new Object();

// These should be saved to the registry.
var BluePagesServerName = null;
var BluePagesBaseDN = null;
var BluePagesMaxResults = -1;
var BluePagesGroupMaxResults = -1;
var BluePagesDisplayType = DETAIL_LABELS;

// Store in the registry as an array of strings.
// Store the column widths as well?
var QuickListColumns = null;
var DefaultQuickListColumns = ["Name", "Email", "External Phone"];

var QuickListUIDs = [];
var OrgTreeBaseSearch = null;

// For advanced searches.
var AdvancedSearchList = [];
AdvancedSearchList.push(["Name", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Last Name", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["First Name", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Email", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Notes Mail", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Tie Line", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["External Phone", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Job Responsibility", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Directory", SEARCH_TYPE_EQUALS]);
AdvancedSearchList.push(["IBM Serial Number", SEARCH_TYPE_CONTAINS]);
AdvancedSearchList.push(["Department", SEARCH_TYPE_EQUALS]);
AdvancedSearchList.push(["Country Code", SEARCH_TYPE_EQUALS]);
AdvancedSearchList.push(["Work Location", SEARCH_TYPE_EQUALS]);

// For managing rules.
var CurrentAdvancedSearch = new Object();

var DesiredDetailsDisplayType = DETAIL_LABELS;

/*******
 * Utility functions.
 *******/
function DummyAdvancedSearch (name){
	 var DAS = new Object();
	 DAS["name"] = ((name && (name != "")) ? name : "New favorite...");
	 DAS["rules"] = [];
	 DAS["operator"] = OPERATOR_AND;
	 return DAS;
}

/*******
 * Utility functions: Converting from display names to attribute names
 *******/
function ConvertAttributeNames(displaynames){
	 if (typeof(displaynames) == "string"){
		  return AttributeNameMapping[displaynames];
	 } else {
		  var namearr = [];
		  var i=0;
		  for (i=0; i<displaynames.length; i++){
			   namearr.push(AttributeNameMapping[displaynames[i]]);
		  }
		  return namearr;
	 }
}


/*******
 * Utility functions: Printing objects
 *******/
// Returns the contents of an array as a string.
function arrayContents(arr, delimiter, includeNulls){
	 var arrstr = "";
	 if (arr == null) return "" ;
	 if (arr.length == 0) return "" ;
	 for (i=0; i<arr.length; i++){
		  if (includeNulls || (arr[i] && (arr[i] != ""))){
			   arrstr += arr[i];
			   if (i != arr.length - 1) arrstr += delimiter;
		  }
	 }
	 return arrstr;
}

// Returns the contents of a collection as a string.
function collectionContents(collection){
	 var retstr = "";
	 var keys = collection.Keys;
	 retstr += ("The collection has " + keys.length + " elements.\n");
	 if (keys.length > 0){
		  for (k=0; k<keys.length; k++){
			   retstr += ("name: " + keys[k] + ", value: " + arrayContents(collection.Item(keys[k]), ", ", true) + "\n");
		  }
	 }
	 return retstr;
}

function printCache(cache){
	 for (p in cache){
		  print (p + ": " + (cache[p]));
	 }
}

/*******
 * Utility functions: Special filters construction and maintenance.
 *******/

/*******
 * Utility functions: Constructing filters
 *******/
// Construct a filter from a list of fields and a data string (which
// is the same for all of the different fields).
function AndFieldsSameData(fields, data){
	 return CombineFieldsSameData("&", fields, data);
}
function OrFieldsSameData(fields, data){
	 return CombineFieldsSameData("|", fields, data);
}
function CombineFieldsSameData(op, fields, data){
	 if (fields.length == 0) return "";
	 var retstr = "(" + op;
	 
	 var i=0;
	 for (i=0; i<fields.length; i++){
		  retstr += "(" + fields[i] + "=" + data + ")";
	 }
	 retstr += ")";
	 return retstr;
}

// Construct a filter from a list of fields and a data string (which
// is the same for all of the different field).
function AndFieldSameValues(field, searchtype, values){
	 return CombineFieldSameValues("&", field, searchtype, values);
}
function OrFieldSameValues(field, searchtype, values){
	 return CombineFieldSameValues("|", field, searchtype, values);
}
function CombineFieldSameValues(op, field, searchtype, values){
	 if (values.length == 0) return "";
	 var retstr = "";


	 if (values.length > 1) retstr = "(" + op;
	 
	 var i=0;
	 for (i=0; i<values.length; i++){
		  retstr += ConstructSingleGenericFilter(searchtype, 
												 field, values[i], true);
	 }

	 if (values.length > 1) retstr += ")";
	 return retstr;
}

function ConstructQLFilter(field, data){
	 if (data) data += '*';
	 var searchfunc = AttributeSearchFunctionMapping[field];
	 var filter = "";
	 if (searchfunc){
//		  print ("search function specified for field: " + field);
		  filter = searchfunc(data);
	 } else {
		  var fieldarr = AttributeFilterMapping[field];
		  if (fieldarr){
//			   print ("filter array specified for field: " + field);
			   filter = OrFieldsSameData(fieldarr, data);
		  } else {
//			   print ("no search function specified for field: " + field);		  
			   var attributeName = AttributeNameMapping[field];
			   if (typeof(attributeName) == "string"){
					filter = '(' + attributeName + '=' 
						 + ((data == null) ? '*' : data) 
						 + ')';
			   }
		  }
	 }
//	 if (filter == "") print ("EMPTY FILTER?");
	 return filter;
}

function ConstructSingleAttributeFilter(displayname, value){
	 var attributename = AttributeNameMapping[displayname];
	 return (attributename + "=" + value);
}

function ConstructSingleGenericFilter(searchtype, name, value, usemap){
	 var attributename = (usemap ? AttributeNameMapping[name] : name);
	 var retstr = "(" + name + "=";
	 
	 if (searchtype == SEARCH_TYPE_STARTSWITH){
		  retstr += value + "*)";
	 } else if (searchtype == SEARCH_TYPE_ENDSWITH){
		  retstr += '*' + value + ')';
	 } else if (searchtype == SEARCH_TYPE_CONTAINS){
		  retstr += '*' + value + "*)";
	 } else {
		  retstr += value + ')';
		  if (searchtype == SEARCH_TYPE_ISNOT){
			   retstr = "(! " + retstr + ')';
		  }
	 }
	 return retstr;
}

/*******
 * Utility functions: Advanced filters
 *******/
// elements will look like this:
// [displayname, searchtype, [values], operator]
function CreateCustomSearchFilter(filterArray, operator){
	 var retstr = null;
	 if (filterArray){
		  retstr = "(";
		  if (operator == OPERATOR_OR){
			   retstr += '|';
		  } else {
			   retstr += '&';
		  }
		  var i=0;
		  var attributename;
		  var currfilter;
		  for (i=0; i<filterArray.length; i++){
			   var attributename = AttributeNameMapping[filterArray[i][RULES_DISPLAY_NAME]];
//			   print ("attributename: " + attributename);
			   if (filterArray[i][RULES_OPERATOR] == OPERATOR_OR){
					currfilter = OrFieldSameValues(
						 attributename, 
						 filterArray[i][RULES_SEARCH_TYPE],
						 filterArray[i][RULES_VALUES]);
			   } else {
					currfilter = AndFieldSameValues(
						 attributename,
						 filterArray[i][RULES_SEARCH_TYPE],
						 filterArray[i][RULES_VALUES]);
			   }
			   retstr += (' ' + currfilter);
		  }
		  retstr += ')';
	 }
	 return retstr;
}

function ConstructRule(searchOption, index){
	 return [OPERATOR_AND, searchOption[0], searchOption[1], null, index];
}

function TranslateSearchType(type){
	 if (type == SEARCH_TYPE_EQUALS){
		  return "equals";
	 } else if (type == SEARCH_TYPE_CONTAINS){
		  return "contains";
	 }
}

function TranslateAdvancedSearchListOption(item){
	 var retstr = ""; 
	 retstr += item[0];
	 retstr += " " + TranslateSearchType(item[1]) + "...";
	 return retstr;
}

// [operator, displayname, searchtype, [values]]
function TranslateRule(rule, showvalues){
	 var retstr = ""; 
	 retstr += rule[RULES_DISPLAY_NAME];
	 retstr += " " + TranslateSearchType(rule[RULES_SEARCH_TYPE]);

	 var values = rule[RULES_VALUES];
	 
	 if (showvalues && values && (values.length > 0)){
		  var i=0;
		  var needcommas = (values.length > 2);
		  for (i=0; i<values.length; i++){
			   if (needcommas && (i != 0)) retstr += ",";
			   if ((i>0) && (i == values.length - 1)) 
					retstr += 
						 ((rule[RULES_OPERATOR] == OPERATOR_OR) ? " or" : " and");
			   retstr += ' ' + values[i];
		  }
	 } else {
		  retstr += "...";
	 }
	 
	 retstr += ".";
	 return retstr;
}

function TranslateAdvancedSearch(AdvancedSearch){
	 var rules = AdvancedSearch["rules"];
	 var retstr = "";
	 var op = AdvancedSearch["operator"];
	 if (rules){
		  var i=0;
		  for (i=0; i<rules.length; i++){
//			   print ("rule: " + TranslateRule(rules[i], true));
			   retstr += TranslateRule(rules[i], true);
			   
			   if (i != rules.length -1){
					retstr += (op == OPERATOR_AND) ? "\nAND\n" : "\nOR\n";
			   }
		  }
	 }
	 return retstr;
}

function GetRowForRule(index){
	 var i=0;
	 for (i=0; i<CurrentAdvancedSearch["rules"].length; i++){
		  if (index == CurrentAdvancedSearch["rules"][i][RULES_INDEX]){
			   return i;
		  }
	 }
	 return -1;
}

/*******
 * Utility functions: Advanced filters: Saving as and loading from XML.
 *******/

function EncodeAdvancedSearchXML(AdvancedSearch){
	 var rules = AdvancedSearch["rules"];

	 if ((rules == null) || (rules.length == 0)) return null;

	 var doc = new Sash.XML.Document();
	 doc.createRootNode("AdvancedSearch");
	 doc.rootNode.setAttribute("name", AdvancedSearch["name"]);
	 doc.rootNode.setAttribute("operator", AdvancedSearch["operator"]);

	 var rulenode, valuenode;
	 var values;

	 var i=0, j=0;
	 for (i=0; i<rules.length; i++){
		  values = rules[i][RULES_VALUES];

		  rulenode = doc.createNode("Rule");
		  rulenode.setAttribute("index", rules[i][RULES_INDEX]);
		  rulenode.setAttribute("operator", rules[i][RULES_OPERATOR]);

		  // WWW: For more complicated searches, might want to 
		  // allow for multiple display names.
		  rulenode.setAttribute("displayname", 
								rules[i][RULES_DISPLAY_NAME]);
		  rulenode.setAttribute("searchtype", 
								rules[i][RULES_SEARCH_TYPE]);
		  if ((values) && (values.length > 0)){
			   for (j=0; j<values.length; j++){
					valuenode = doc.createNode("Value");
					valuenode.text = values[j];
					rulenode.appendChild(valuenode);
			   }
		  }
		  doc.rootNode.appendChild(rulenode);
	 }
//	 print ("xml string: " + doc.saveToString());
	 return (doc.saveToString());
}

function DecodeAdvancedSearchXML(XMLAdvancedSearch){
	 AdvancedSearch = new Object();
	 var doc = new Sash.XML.Document();
	 doc.loadFromString(XMLAdvancedSearch);

	 AdvancedSearch["name"] = doc.rootNode.getAttribute("name");
	 AdvancedSearch["operator"] = doc.rootNode.getAttribute("operator");
	 AdvancedSearch["rules"] = [];

//	 print ("name: " + AdvancedSearch["name"]);
//	 print ("operator: " + AdvancedSearch["operator"]);

	 var rulenodearr = doc.rootNode.getChildNodes("Rule");
	 var valuenodearr;
	 var valuearr;
	 var currnode;
	 if ((rulenodearr) && (rulenodearr.length > 0)){
		  var i=0;
		  for (i=0; i<rulenodearr.length; i++){
			   currnode = rulenodearr[i];

			   valuenodearr = currnode.getChildNodes("Value");
			   valuearr = [];
			   var j=0;
			   for (j=0; j<valuenodearr.length; j++){
//					print ("pushing value: " + valuenodearr[j].text);
					valuearr.push(valuenodearr[j].text);
			   }

// 			   print ([currnode.getAttribute("operator"),
// 					currnode.getAttribute("displayname"),
// 					currnode.getAttribute("searchtype"),
// 					   currnode.getAttribute("index")]);

			   AdvancedSearch["rules"].push([
					currnode.getAttribute("operator"),
					currnode.getAttribute("displayname"),
					currnode.getAttribute("searchtype"),
					valuearr,
					currnode.getAttribute("index")]);
		  }

	 }
	 return AdvancedSearch;
}


/*******
 * Utility functions: Search functions
 *******/
function FilterNotesMail(data){
	 var dn = NotesIDToDN(data);
	 return ("(notesemail=" + dn + "*)");
}

/*******
 * Utility functions: LDAP connectivity and access.
 *******/
function ConnectToServer(){
	 Status ("Connecting to " + BluePagesServerName);
	 LDAPConnection = new Sash.LDAP.Connection(BluePagesServerName);
	 if (LDAPConnection)
		  LDAPConnection.SearchMaxResults = BluePagesMaxResults;
	 return LDAPConnection.Connect();
}
function DisconnectFromServer(){
	 if (LDAPConnection){
		  LDAPConnection.Disconnect();
	 }
}

function QuickListSearch(field, data, cols){
	 SearchResultsCList.clear();
	 QuickListUIDs = [];
	 var filter = ConstructQLFilter(field, data);
	 var personfilter = ConstructQLFilter("UID");

	 filter = "(& " + filter + ' ' + personfilter + ')';
	 ExecuteQLSearch(BluePagesBaseDN,
				   Sash.LDAP.SCOPE_SUBTREE,
				   filter,
				   ConvertAttributeNames(cols));
}

function QuickListFilteredSearch(field, data, AS, cols){
	 SearchResultsCList.clear();
	 QuickListUIDs = [];
	 var filter = ConstructQLFilter(field, data);
	 var personfilter = ConstructQLFilter("UID");
	 var ASfilter = CreateCustomSearchFilter(AS["rules"],
											 AS["operator"]);

	 filter = "(& " + filter + ' ' + personfilter + ' ' + ASfilter + ")";
//	 print ("generated filter: " + filter);
	 ExecuteQLSearch(BluePagesBaseDN,
				   Sash.LDAP.SCOPE_SUBTREE,
				   filter,
				   ConvertAttributeNames(cols));
}

function QuickListAdvancedSearch(advancedSearch, cols){
	 SearchResultsCList.clear();
	 QuickListUIDs = [];
	 var filter = CreateCustomSearchFilter(advancedSearch["rules"],
										   advancedSearch["operator"]);
	 var personfilter = ConstructQLFilter("UID");
	 filter = "(& " + filter + ' ' + personfilter + ')';

	 ExecuteQLSearch(BluePagesBaseDN,
				   Sash.LDAP.SCOPE_SUBTREE,
				   filter,
				   ConvertAttributeNames(cols));
}


function ManagerChainSearch(UID, cols){
	 OrgTreeBaseSearch = UID;
	 OrgTree.clearItems(0,1);

	 // Try to save the existing data?
	 ManagerCache = new Object();
	 
	 ExecuteManagerSearch(BluePagesBaseDN,
						  Sash.LDAP.SCOPE_SUBTREE,
						  ConstructSingleAttributeFilter("UID", UID),
						  ConvertAttributeNames(cols));
}

function DetailedProfileSearch(UID){
	 DetailsVBox.visible = true;

	 // Check the cache.
	 var entry = CurrentOpenProfiles[UID];
	 if (entry){
		  // Get All Data.
		  // Figure out the index.

		  var index = GetArrayNameIndex(ProfileOrdering, UID);

		  if ((index >=0) && (index < ProfileOrdering.length)){
			   item = ProfileOrdering[index];
			   ProfileOrdering = RemoveFromArray(ProfileOrdering, index);
			   ProfileOrdering.push(item);
			   
			   item = ProfileOrderingNames[index];
			   ProfileOrderingNames = RemoveFromArray(ProfileOrderingNames, index);
			   ProfileOrderingNames.push(item);

			   CurrentProfileIndex = ProfileOrderingNames.length -1;
			   
			   DisplaySimpleDetails(CurrentProfileIndex);
			   return;
		  }
	 } 

	 var ID = ExecuteDetailsSearch(BluePagesBaseDN, 
								   Sash.LDAP.SCOPE_SUBTREE,
								   ConstructSingleAttributeFilter("UID", UID),
								   AttributeActualNameArray);
}

// Given the entry and a specified fields that holds a distinguished name,
// Query the new DN for the fieldsDesired.
// data specifies what data you're looking for, like "Business Address"
// or "Manager".
function AdditionalDetailsSearch(entry, data){
//	 print ("AdditionalDetailsSearch for: " + data);
	 var ID = -1;
	 var dataarr = AdditionalDetailsMapping[data];
	 if (dataarr){
		  var baseDN = GetAttributes(entry, dataarr[0], true);
		  // WWW what to fill in if you can't find this info? (for example,
		  // if there isn't secretary information.
		  if ((baseDN) && (baseDN != "")){
			   // Check the cache.
			   var cachedarr = AdditionalDetailsCache[baseDN];
			   if (cachedarr){
					// If there's something in the cache, display 
					// it immediately.
// 					print ("There was something in the cache for: " + baseDN
// 						 + ", values: " + cachedarr);
					DisplayAdditionalDetails(data, cachedarr);
			   } else {
					// If there's nothing, then Execute the search.
					// Track the ID to make sure that you can match it
					// when it returns.
//					print ("Nothing in the cache for: " + baseDN);
					ID = ExecuteAdditionalDetailsSearch(baseDN, 
														Sash.LDAP.SCOPE_SUBTREE, 
														ConvertDNToFilter(baseDN), 
														dataarr.slice(2));
					if (ID != -1){
						 var name = GetSingleAttribute(entry, "Name", true);
						 AdditionalDetailsOpenRequests[ID] = [name, data];
					} else {
						 Status("Couldn't execute search.");
					}
			   }
		  }
	 }
	 return ID;
}

function RecursiveOrgTreeSearch(employeeDN){
//	 print ("RecursiveOrgTreeSearch");
	 // Given a name, find the manager.
	 ExecuteManagerSearch(employeeDN, Sash.LDAP.SCOPE_SUBTREE,
						  ConvertDNToFilter(employeeDN),
						  ConvertAttributeNames(OrgTreeAttributes));
}

function MinionsSearch(managerSN, managerCC, cols){
	 if ((managerSN)&&(managerCC)){
		  ExecuteMinionsSearch(BluePagesBaseDN,
							   Sash.LDAP.SCOPE_SUBTREE,
							   "(& (managerserialnumber=" + managerSN
							   + ") (managercountrycode=" + managerCC + "))",
							   ConvertAttributeNames(cols));
	 }
}

function ExecuteQLSearch(baseDN, scope, filter, cols){
// 	 print ("executing search (filter: " + filter 
// 			+ ", baseDN: " + baseDN + ", cols: " + arrayContents(cols, ", ", true)+ ")");

	 SearchRequest = new LDAPConnection.SearchRequest(baseDN,
													  Sash.LDAP.SCOPE_SUBTREE,
													  filter,
													  cols);
	 SearchRequest.OnSearchResult = OnQuickListResult;
	 SearchRequest.OnSearchComplete = OnQuickListComplete;
	 SearchRequest.OnError = onDefaultRequestError;

	 Sash.Core.Cursor.SetStandard(MOUSE_WAIT);
	 ChangeButtonToCancel();
	 FavoritesSearchOption.enabled = false;


	 if (LDAPConnection){
		  if (LDAPConnection.SearchMaxResults != BluePagesMaxResults){
			   LDAPConnection.SearchMaxResults = BluePagesMaxResults;
		  }
	 }
	 return (SearchRequest.Invoke());
}

function ExecuteDetailsSearch(baseDN, scope, filter, cols){
// 	 print ("executing search (filter: " + filter 
// 			+ ", baseDN: " + baseDN + ", cols: " + arrayContents(cols, ", ", true)+ ")");
	 SearchRequest = new LDAPConnection.SearchRequest(baseDN,
													  Sash.LDAP.SCOPE_SUBTREE,
													  filter,
													  cols);
	 SearchRequest.OnSearchResult = OnDetailsResult;
	 SearchRequest.OnSearchComplete = OnDetailsComplete;
	 SearchRequest.OnError = onDefaultRequestError;
	 Sash.Core.Cursor.SetStandard(MOUSE_WAIT);

	 if (LDAPConnection){
		  if (LDAPConnection.SearchMaxResults != BluePagesMaxResults){
			   LDAPConnection.SearchMaxResults = BluePagesMaxResults;
		  }
	 }
	 return (SearchRequest.Invoke());
}

function ExecuteManagerSearch(baseDN, scope, filter, cols){
// 	 print ("executing search (filter: " + filter 
// 			+ ", baseDN: " + baseDN + ", cols: " + arrayContents(cols, ", ", true)+ ")");

	 SearchRequest = new LDAPConnection.SearchRequest(baseDN,
													  Sash.LDAP.SCOPE_SUBTREE,
													  filter,
													  cols);
	 SearchRequest.OnSearchResult = OnManagerResult;
	 SearchRequest.OnSearchComplete = OnManagerComplete;
	 SearchRequest.OnError = onDefaultRequestError;
	 Sash.Core.Cursor.SetStandard(MOUSE_WAIT);

	 if (LDAPConnection){
		  if (LDAPConnection.SearchMaxResults != BluePagesMaxResults){
			   LDAPConnection.SearchMaxResults = BluePagesMaxResults;
		  }
	 }
	 return (SearchRequest.Invoke());
}

function ExecuteAdditionalDetailsSearch(baseDN, scope, filter, cols){
// 	 print ("executing search (filter: " + filter 
// 			+ ", baseDN: " + baseDN + ", cols: " + arrayContents(cols, ", ", true)+ ")");
	 SearchRequest = new LDAPConnection.SearchRequest(baseDN,
													  Sash.LDAP.SCOPE_SUBTREE,
													  filter,
													  cols);
	 SearchRequest.BaseDN = baseDN;
	 SearchRequest.OnSearchResult = OnAdditionalDetailsResult;
	 SearchRequest.OnSearchComplete = OnAdditionalDetailsComplete;
	 SearchRequest.OnError = onDefaultRequestError;
	 Sash.Core.Cursor.SetStandard(MOUSE_WAIT);

	 if (LDAPConnection){
		  if (LDAPConnection.SearchMaxResults != BluePagesMaxResults){
			   LDAPConnection.SearchMaxResults = BluePagesMaxResults;
		  }
	 }
	 return (SearchRequest.Invoke());
}

function ExecuteMinionsSearch(baseDN, scope, filter, cols){
// 	 print ("executing search (filter: " + filter 
// 			+ ", baseDN: " + baseDN + ", cols: " + arrayContents(cols, ", ", true)+ ")");
	 SearchRequest = new LDAPConnection.SearchRequest(baseDN,
													  Sash.LDAP.SCOPE_SUBTREE,
													  filter,
													  cols);
	 SearchRequest.BaseDN = baseDN;
	 SearchRequest.OnSearchResult = OnMinionsResult;
	 SearchRequest.OnSearchComplete = OnMinionsComplete;
	 SearchRequest.OnError = onDefaultRequestError;
	 Sash.Core.Cursor.SetStandard(MOUSE_WAIT);

	 if (LDAPConnection){
		  if (LDAPConnection.SearchMaxResults != BluePagesGroupMaxResults){
			   LDAPConnection.SearchMaxResults = BluePagesGroupMaxResults;
		  }
	 }
	 return (SearchRequest.Invoke());
}

/*******
 * Utility functions: callbacks
 *******/
function OnQuickListResult(searchRequest, entry){
	 QuickListSearchResultCount++;
	 Status(QuickListSearchResultCount + " results found.");
	 var values = GetAttributes(entry, QuickListColumns, true);
	 ConvertNullToEmptyString(values);

	 ConvertNotesMail(QuickListColumns, values);
	 if (values && (values.length > 0)){
		  QuickListUIDs.push(values[0]);
		  SearchResultsCList.Append(values.slice(1));
	 }
}

function OnQuickListComplete(searchRequest, entries, success, description){
	 Sash.Core.Cursor.SetStandard(MOUSE_DEFAULT);
	 ChangeButtonToSearch();
	 FavoritesSearchOption.enabled = true;

	 if (success){
		  Status (QuickListSearchResultCount + " results found. Search complete.");
	 } else {
		  Status (description);
	 }
	 QuickListSearchResultCount = 0;
}

function OnDetailsResult(searchRequest, entry){
	 // Cache this with the open profiles.
	 var UID = StripMultipleValue(GetSingleAttribute(entry, "UID", true));
	 var name = StripMultipleValue(GetSingleAttribute(entry, "Name", true));

	 CurrentOpenProfiles[UID] = entry;
	 ProfileOrdering.push(UID);
	 ProfileOrderingNames.push(name);
	 CurrentProfileIndex = NumProfiles;
	 NumProfiles++;
// 	 print ("Added entry to profiles for: " + UID
// 			+ ", index: " 
// 			+ CurrentProfileIndex + ", total profiles: " + NumProfiles);

	 DisplaySimpleDetails(CurrentProfileIndex);

}

function OnDetailsComplete(searchRequest, entries, success, description){
	 Sash.Core.Cursor.SetStandard(MOUSE_DEFAULT);
	 if (success){
		  Status ("Search completed successfully.");
	 } else {
		  Status (description);
	 }
}

function OnAdditionalDetailsResult(searchRequest, entry){
// 	 print( "Additional detail search result received. Contents: ");
// 	 print( collectionContents (entry));


	 // Figure out what kind of data it is.
	 // type = [name, data]
	 // name = person name
	 // data = type of data that you're getting ("Business Address" or
	 // "Manager", etc.)
	 var type = AdditionalDetailsOpenRequests[searchRequest.ID];

// 	 print ("Type: " + type[1]);

	 var dataarr = GetAttributes(entry,
								 AdditionalDetailsMapping[type[1]].slice(2));

	 // Add the data to the cache.
	 AdditionalDetailsCache[searchRequest.BaseDN] = dataarr;
// 	 print ("Cached entry under: " + searchRequest.BaseDN);

	 if (type){
		  // if it matches the current profile, then 
		  // display the data.
		  DisplayAdditionalDetails(type[1], dataarr);
	 }
}

function OnAdditionalDetailsComplete(searchRequest, entries, success, description){
	 Sash.Core.Cursor.SetStandard(MOUSE_DEFAULT);
	 if (success){
		  Status ("Search completed successfully.");
	 } else {
		  Status (description);
	 }
}

function OnManagerResult(searchRequest, entry){
	 var employeeUID = StripMultipleValue(GetSingleAttribute(entry, "UID", 
															 true));
	 var managerDN = StripMultipleValue(GetSingleAttribute(entry, "Manager DN",
														   true));
	 var managerUID = GetFirstQuotedValueFromDN(managerDN);

	 // Cache the mapping from person to manager
	 var arrvalue = GetAttributes(entry, OrgTreeAttributes, true);

	 if ((employeeUID == null) 
		 || (employeeUID == "")
		 || (employeeUID == managerUID)
		 || (ManagerCache[employeeUID])){
		  // If the manager uid is the same as the person uid
		  // you've reached the top. Call the OrgTreeDataComplete
		  // method.
		  // Also, if the data you're looking for has already been 
		  // cached, that means that the search has already been
		  // completed (This also lets you avoid cycles, which do
		  // exist in the hierarchy).
		  if (employeeUID == managerUID)
			   ManagerCache[employeeUID] = arrvalue;

		  OrgTreeDataComplete();
	 } else {
		  // If the manager name is not the same as the person name, 
		  // Call the next search.
//		  print ("Caching data for " + employeeUID);
		  ManagerCache[employeeUID] = arrvalue;
		  RecursiveOrgTreeSearch(managerDN);
	 }
}

function OnManagerComplete(searchRequest, entries, success, description){
	 Sash.Core.Cursor.SetStandard(MOUSE_DEFAULT);
	 if (success){
		  Status ("Search completed successfully.");
	 } else {
		  Status (description);
	 }
}

function OnMinionsResult(searchRequest, entry){
	 var employeeUID = StripMultipleValue(GetSingleAttribute(entry, "UID", 
															 true));
	 if (ManagerCache[employeeUID] == null){
		  // Cache the mapping from person to manager
		  var arrvalue = GetAttributes(entry, OrgTreeAttributes, true);
		  ManagerCache[employeeUID] = arrvalue;
//		  print ("Caching data for " + employeeUID);
		  
		  // Get the filter to find which manager this belongs to.
		  var filter = searchRequest.Filter;
//		  print ("minion returned for filter: " + filter + ", name: " + StripMultipleValue(GetSingleAttribute(entry, "Name", true)));
		  
		  // Map this back into the tree.
		  var manSNCC = GetAndConcatTwoValuesFromFilter(filter);
		  
//		  print ("looking for tree in ManagerNodeMapping for sncc: " + manSNCC);
		  var parentnode = ManagerNodeMapping[manSNCC];
		  var parenttree;
		  if (parentnode){
			   parenttree = parentnode.subtree;
			   if (parenttree == null){
//					print ("parent tree was null, create a new one\n");
					parentnode.subtree = new Sash.GTK.GtkTree();
			   }
		  }
		  CreateNode(arrvalue, parenttree, ORGTREE_APPEND);
	 } else {
		  // WWW may want to shuffle it.
//		  print ("result already in the tree, don't draw new node");
	 }
}


function OnMinionsComplete(searchRequest, entries, success, description){
	 Sash.Core.Cursor.SetStandard(MOUSE_DEFAULT);
	 if (success){
		  Status ("Search completed successfully.");
	 } else {
		  Status (description);
	 }
}



/*******
 * Utility functions: Data retrieval from entries
 *******/
function GetSingleAttribute(entry, AttributeDisplayName,
							useMap){
	 if (AttributeDisplayName == ""){
//		  print ("GetSingleAttribute: attribute display name is empty"); 
		  return null;
	 }
	 var attributename = 
		  (useMap ? 
		   AttributeNameMapping[AttributeDisplayName] : AttributeDisplayName);
	 if ((attributename == null) || (attributename == "")){
//		  print ("GetSingleAttribute: attribute actual name is empty"); 
		  return null;
	 }
	 var attributevalue;
	 if (typeof(attributename) == "string"){
//		  print ("GetSingleAttribute: name: " + attributename); 
		  attributevalue = entry.Item(attributename);
//		  print ("GetSingleAttribute: value: " + attributevalue); 
	 }else {
		  if (attributename.length == 2){
//			   print ("GetSingleAttribute: pair of values requested: ");
			   var val0 = entry.Item(attributename[0]);
			   var val1 = entry.Item(attributename[1]);
//			   print ("GetSingleAttribute: name1: " + attributename[0] + ", value1: " + val0);
//			   print ("GetSingleAttribute: name2: " + attributename[1] + ", value2: " + val1);

			   attributevalue = checkEmptyD(val0, "None") + " / " 
					+ checkEmptyD(val1, "None");
		  }
	 }
	 return attributevalue;
}

function GetAttributes(entry, AttributeDisplayNames, useMap){
	 if (entry == null) {
//		  print ("getSingleAttribute: entry is null"); 
		  return null;
	 }
	 if (AttributeDisplayNames == null){
//		  print ("getSingleAttribute: attribute name is null"); 
		  return null;
	 }
	 // For simple data, just look up the corresponding 
	 // attribute name in the AttributeNameMapping, then return 
	 // the data associated with that attribute from the item.
	 if (typeof(AttributeDisplayNames) == "string"){
		  return StripMultipleValue(GetSingleAttribute(entry, AttributeDisplayNames, useMap));
	 } else {
		  if (AttributeDisplayNames.length == 0){
//			   print ("getSingleAttribute: attribute array is empty"); 
			   return null;
		  }
		  var i=0;
		  var data = [];
		  for (i=0; i<AttributeDisplayNames.length; i++){
			   data.push(StripMultipleValue(
							  GetSingleAttribute(entry, 
												 AttributeDisplayNames[i],
												 useMap)));
		  }
		  return data;
	 }
	 return null;
}

// Sometimes the data will be stored in an array. Usually it will correspond
// to another array holding the property names.
function GetArrayNameIndex(namearray, name){
	 if (namearray == null) return -1;
	 if ((name == null) || (name == "")) return -1;
	 var i=0;
	 for (i=0; i<namearray.length; i++){
		  if (name == namearray[i]) return i;
	 }
	 return -1;
}

function GetArrayNameIndices(namearray, name){
	 if (namearray == null) return -1;
	 if ((name == null) || (name == "")) return -1;
	 var ret = [];
	 var i=0;
	 for (i=0; i<namearray.length; i++){
		  if (name == namearray[i]) ret.push(i);
	 }
	 return ret;
}

// WWW: this relies on ordering of the cached data, which depends on the 
// information found in AdditionalDetailsMapping.
function GetCurrentPersonUID(DNAttributeName){
	 // Use the current profile index to find the manager information.
	 var curr = CurrentOpenProfiles[ProfileOrdering[CurrentProfileIndex]];
	 if (curr){
		  // Get manager.
		  var DN = StripMultipleValue(GetSingleAttribute(curr, DNAttributeName, true));
		  if (DN){
//			   print ("dn: " + DN);
			   // look it up.
			   var cachedarr = AdditionalDetailsCache[DN];
//			   print ("cached arr: " + cachedarr);
			   if (cachedarr) return cachedarr[1];
		  }
	 }
	 return null;
}


function GetCurrentSecretaryUID(){}



/*******
 * Utility functions: OrgTree fun.
 *******/
function OrgTreeDataComplete(){
	 // Construct original list.
	 // replace this with the name of the current search.
	 
	 // Start with original person...
	 var OrgTreeList = [];
	 ConstructOrgTreeList(OrgTreeList, OrgTreeBaseSearch);
	 OrgTreeBaseSearch = "";

//	 print ("Constructed OrgTreeList:");
	 var i=0;
// 	 for (i=0; i<OrgTreeList.length; i++){
// 		  print (OrgTreeList[i]);
// 	 }

	 DisplayOrgTree(OrgTreeList);
}

function ConstructOrgTreeList(OrgTreeList, UID){
	 // Construct an array that holds arrays of data.
//	 print ("Trying to insert node for uid: " + UID);
	 ManagerCycleCheck = new Object();
	 RecursiveConstructOrgTreeList(OrgTreeList, UID);
}

function RecursiveConstructOrgTreeList(OrgTreeList, employeeUID){
	 // look up the name in the manager cache.
	 var employeedata = ManagerCache[employeeUID];
	 
	 if (ManagerCycleCheck[employeeUID]) return;
	 ManagerCycleCheck[employeeUID] = 1;

	 var index;
	 var uid, managerdn, dept;

//	 print ("employeedata: " + employeedata);
	 if (employeedata){
		  index = GetArrayNameIndex(OrgTreeAttributes, "UID");
		  if (index != -1)
			   uid = StripMultipleValue(employeedata[index]);

		  index = GetArrayNameIndex(OrgTreeAttributes, "Manager DN");
		  if (index != -1)
			   managerdn = StripMultipleValue(employeedata[index]);

		  // We are creating the array from the bottom up, which means that
		  // the current person we're looking at contains the dept code
		  // of the group that the next person manages.
		  index = GetArrayNameIndex(OrgTreeAttributes, "Department");
		  if ((OrgTreeList) && (OrgTreeList.length > 0)){
			   dept = StripMultipleValue(OrgTreeList[0][index]);
//			   print ("trying to add department info: " + dept);
			   employeedata.push(dept);
		  }

		  OrgTreeList.unshift(employeedata);

//		  print ("trying getuidfromdn on: " + managerdn);
		  var managerUID = GetFirstQuotedValueFromDN(managerdn);
		  if (uid == managerUID){
//			   print ("done updating ctree");
		  } else {
			   RecursiveConstructOrgTreeList(OrgTreeList, managerUID);
		  }
	 } else {
//		  print ("Error: no employee data in the cache\n");
	 }
}



/*******
 * Utility functions: Massaging Data
 *******/

function CutOut(str, substr){
	 var temp = "";
	 var temparr = str.split(substr);
	 
	 var i=0;
	 for (i=0; i<temparr.length; i++){
		  temp += temparr[i];
	 }
	 return temp;
}

// convert DN to valid notes email:
// CN=John W Rooney/OU=Somers/O=IBM@IBMUS 
// to John W Rooney/Somers/IBM@IBMUS 
function DNToNotesID(DN){
	 var temp = "";
	 temp = CutOut(DN, "CN=");
	 temp = CutOut(temp, "OU=");
	 temp = CutOut(temp, "O=");
	 return temp;
}

function NotesIDToDN(NotesID){
	 var temp = "";
	 var vals = NotesID.split('/');
	 if (vals.length > 3){
		  return null;
	 }

	 // If someone has already included these, the match should
	 // still work.
	 var cnmatch = /^CN=/i;
	 var oumatch = /^OU=/i;
	 var omatch = /^O=/i;

	 temp = (vals[0].match(cnmatch) ? "" : "CN=") + vals[0];
	 if ((vals.length > 1) && (vals[1] != "")){
		  temp += ('/' + (vals[1].match(oumatch) ? "" : "OU=") + vals[1]);
		  if ((vals.length > 2) && (vals[2] != "")){
			   temp += ('/' + (vals[2].match(omatch) ? "" : "O=") + vals[2]);
		  }
	 }
	 return temp;
}

function ConvertNotesMail(QLCols, values){
	 var indices = GetArrayNameIndices(QLCols, "Notes Mail");
	 var i=0;
	 for (i=0; i<indices.length; i++){
		  values[indices[i]] = DNToNotesID(values[indices[i]]);
	 }
}

function checkEmpty(val){
	 if ((val == null) || (val == "")){
		  return "None";
	 }
	 return val;
}

function checkEmptyD(val, def){
	 if ((val == null) || (val == "")){
		  return def;
	 }
	 return val;
}

function StripMultipleValue(value){
	 // Assume that if it's an object, it's either null 
	 // or an array.
	 if (value == null) return null;
	 if (value){
		  if (typeof(value) == "object"){
			   return value[0];
		  }
	 }
	 return value;
}

function ConvertNullToEmptyString(value){
	 if (value == null) return "";
	 if (typeof(value) == "object"){
		  var i;
		  for (i in value){
			   if (value[i] == null) value[i] = "";
		  }
	 }
}

function ConvertDNToFilter(filter){
//	 print ("filter: " + filter);
	 var retval = "";
	 // WWW: Not sure if this is the best way to do it.
	 // Scan for the second quote mark.
	 var firstq = filter.indexOf('"');
	 retval = filter.substr(0, firstq);
	 
//	 print ("up to firstq: " + retval);

	 var secondq = filter.indexOf('"', firstq+1);
	 retval += filter.substr(firstq+1, secondq - firstq-1);

//	 print ("up to secondq: " + retval);
	 return retval;
}

function GetFirstQuotedValueFromDN(employeeDN){
// 	 print ("GetFirstQuotedValueFromDN");
// 	 print ("employeeDN: " + employeeDN);
// 	 print ("typeof(edn): " + typeof(employeeDN));
	 
	 var firstq = employeeDN.indexOf('"');
	 var secondq = employeeDN.indexOf('"', firstq+1);
	 var retval = employeeDN.substr(firstq+1, secondq-firstq-1);
// 	 print ("uid value: " + retval);
	 return retval;
}

function GetValueFromFilter(filter){
	 var eqindex = filter.indexOf('=');
	 var retval = filter.substr(eqindex+1, filter.length-eqindex-1);
	 return retval;
}

function GetAndConcatTwoValuesFromFilter(filter){
	 var eqindex = filter.indexOf('=');
	 var parenindex = filter.indexOf(')', eqindex +1);
	 var retval1 = filter.substr(eqindex+1, parenindex-eqindex-1);

	 eqindex = filter.indexOf('=', parenindex + 1);
	 parenindex = filter.indexOf(')', eqindex + 1);
	 retval2 = filter.substr(eqindex+1, parenindex-eqindex-1);


	 return (retval1 + retval2);
}

function RemoveFromArray(arr, index){
	 if (arr == null) return null;
	 if (arr.length == 0) return null;
	 if (index < 0) return null;
	 if (index >= arr.length) return null;
	 
	 var item;
	 item = arr[index];
	 if (index != -1){
		  if (index == 0){
			   // it's the first element
			   arr.shift();
		  } else if (index == arr.length - 1){
			   // it's the last element that you're removing.
			   arr.pop();
		  } else {
			   // Remove it from the current advanced search.
			   var temp = arr.slice(0, index);
			   arr = temp.concat(arr.slice(index + 1));
		  }
	 }
	 return arr;
}

function RemoveRowsFromCList(clist, indices, min){
	 if (indices){
		  indices.sort();
		  var j=0;
		  
		  for (j=indices.length-1; j >= 0; j--){
			   if (clist.rows > min)
					clist.RemoveRow(indices[j]);
		  }
	 }


}

function ReplaceArrayItem(arr, olditem, newitem){
	 if (arr != null){
		  var i=0;
		  for (i=0; i<arr.length; i++){
			   if (arr[i] == olditem)
					arr[i] = newitem;
		  }
	 }
}

/*******
 * Utility functions: Manipulating GTK objects
 *******/

function setWidgetText(name, value){
	 var widgetname = GetWidgetNameMapping(name, BluePagesDisplayType);
	 if (widgetname == null) return;
	 var widget = gladefile.getWidget(widgetname);
	 widget.visible = true;
	 widget.show();
	 if (widget == null) return;

	 if (WidgetTypeMapping[name] == GTK_WIDGET_TEXTAREA){
		  widget.text = value;
		  widget.setPoint(0);
	 } else {
		  widget.setText(value);
	 }
}

function SetupOption(title, gtkOptionWidget,
					 options, code, special,
					 activateHandler){
//	 print ("setting up option: " + title + ", handler: " + activateHandler);
	 var menu = new Sash.GTK.GtkMenu();
	 menu.visible = true;

	 var item;
	 
	 if (title && (title != "")){
		  item = new Sash.GTK.GtkMenuItem(title);
		  item.signalConnect("activate", activateHandler,
							 [OPTION_TITLE, title]);

		  item.visible = true;
		  menu.append(item);
	 }

	 if (options){
		  var i=0;
		  for (i=0; i<options.length; i++){
			   item = new Sash.GTK.GtkMenuItem(options[i]);
			   item.signalConnect("activate", activateHandler,
								  [i, options[i]]);
			   item.visible = true;
			   menu.append(item);
		  }
	 }
	 
	 if (special && (special != "")){
		  item = new Sash.GTK.GtkMenuItem(special);
		  item.signalConnect("activate", activateHandler, 
							 [code, special]);
		  item.visible = true;
		  menu.append(item);
	 }

	 gtkOptionWidget.setMenu(menu);
}

function SetupSearchFieldOption(gtkOptionWidget, searchablefields,
								activateHandler){
	 SetupOption(null, gtkOptionWidget, 
				 searchablefields, null, null, activateHandler);
}

function SetupSearchFavoritesOption(gtkOptionWidget,
									favorites, code, special,
									activateHandler){
	 SetupOption("Favorites", gtkOptionWidget, 
				 favorites, code, special, activateHandler);
}

function SetupSearchFiltersOption(gtkOptionWidget,
								  filters,
								  code, special, activateHandler){
	 SetupOption("Filters", gtkOptionWidget, filters, code, special, 
				 activateHandler);
}

// Set up the combo fields.
function SetupFields(combo, list){
}

// Operates on global variables.
function CreateNewCList(size){
	 SearchResultsWindow.remove(SearchResultsCList);
	 SearchResultsCList = new Sash.GTK.GtkCList(size);
	 SearchResultsCList.signalConnect(
		  "button_press_event", 
		  "on_SearchResultsCList_button_press_event", 
		  null);
	 SearchResultsCList.signalConnect(
		  "select_row", 
		  "on_SearchResultsCList_select_row", 
		  null);
	 
	 SearchResultsWindow.add(SearchResultsCList);
}

// Set up the clist columns.
function SetupCList(clist, columns, 
					col_click_handler,
					col_resize_handler)
{
	 if (columns == null) return false;
	 if (columns.length == 0) return false;
 	 if (clist.columns != columns.length) return false;

	 var i=0;
	 for (i=0; i<columns.length; i++){
		  clist.setColumnTitle(i, columns[i]);
		  clist.setColumnWidth(i, 
							   checkEmptyD(
									LoadBluePagesOption("W:" + columns[i]),
									DefaultQLColumnWidths[columns[i]]));
	 }
	 clist.show();
	 clist.columnTitlesShow();
	 clist.signalConnect("click-column", col_click_handler, null);
	 clist.signalConnect("resize-column", col_resize_handler, null);
	 return true;
}

function SetupLabelWidgets(){
	 var i;
	 var widget;
	 for (i in WidgetNameMapping){
		  // Figure out which mode to display.
		  widget = gladefile.getWidget(GetWidgetNameMapping(i, DETAIL_LABELS));
		  if (widget){
			   if (WidgetTypeMapping[i] != GTK_WIDGET_TEXTAREA){
					widget.setLineWrap(true);
					widget.setJustify(Sash.GTK.JUSTIFY_LEFT);
			   }
		  }
	 }
}


function SetupDetailWidgets(){
	 if (DesiredDetailsDisplayType == BluePagesDisplayType) return;

//	 print ("setting up detail widgets...");

	 var i;
	 var showwidget;
	 var hidewidget;
	 for (i in WidgetNameMapping){
//		  print ("Setting up label widget for: " + i);

		  // Figure out which mode to display.
		  if (DesiredDetailsDisplayType != BluePagesDisplayType){
			   showwidget = gladefile.getWidget(GetWidgetNameMapping(i, DesiredDetailsDisplayType));
			   hidewidget = gladefile.getWidget(GetWidgetNameMapping(i, BluePagesDisplayType));
			   if (showwidget){
					showwidget.show();
					showwidget.visible = true;
			   }
			   if (hidewidget){
					hidewidget.hide();
					hidewidget.visible = false;
			   }
		  }
	 }

	 BluePagesDisplayType = DesiredDetailsDisplayType;
}

function SetupCheckButtonVboxAndArray(vbox, array, labels,
									  toggle_handler){
	 if (vbox == null) return null;
	 if (array == null) array = [];
	 if ((labels == null) && (labels.length > 0)) return null;

	 var i=0;
	 var checkboxWidget, checkboxLabel;
	 for (i=0; i<labels.length; i++){
//		  print ("label: " + labels[i]);
		  checkboxWidget = new Sash.GTK.GtkCheckButton();
		  checkboxLabel = new Sash.GTK.GtkLabel(labels[i]);
		  checkboxLabel.setJustify(Sash.GTK.JUSTIFY_LEFT);
		  checkboxLabel.visible = true;

		  checkboxWidget.add(checkboxLabel);
		  checkboxWidget.visible = true;

		  if (toggle_handler != "")
			   checkboxWidget.signalConnect("toggled", toggle_handler, i);
		  if (checkboxWidget){
			   vbox.add(checkboxWidget);
			   array.push(checkboxWidget);
		  }else{
			   print ("couldn't create checkboxWidget");
		  }
	 }
}

function SetupSearchFiltersDialog(){
	 var rulestext = [];
	 for (i=0; i<AdvancedSearchList.length; i++){
		  rulestext.push( 
			   TranslateAdvancedSearchListOption(AdvancedSearchList[i]));
	 }
	 SetupCheckButtonVboxAndArray(AllFiltersListVBox,
								  AllFiltersCheckButtons,
								  rulestext,
								  "on_CheckButton_toggled");
}

function UpdateASLists(type){
	 var favorites = LoadASNames(type);
	 ManageASNameCList.clear();
	 RepopulateCList(ManageASNameCList, favorites, true);
	 
	 var currname = CurrentAdvancedSearch["name"];
	 var index;
	 if (currname && (currname != "")){
		  index = GetArrayNameIndex(favorites, currname);
//		  print ("selecting row: " + index + ", name: " + currname);
		  ManageASNameCList.selectRow(index, 0);
	 }
	 var option;
	 if (type == "Favorites") option = FavoritesSearchOption;
	 else option = FiltersSearchOption;

	 SetupOption(type, option, favorites, 
				 OPTION_CUSTOMIZE, "Customize...",
				 "on_Search"+type+"_activate");
}

function UpdateFavoritesLists(){
	 UpdateASLists("Favorites");
}

function UpdateFiltersLists(){
	 UpdateASLists("Filters");
}

/*******
 * Utility functions: Displaying data
 *******/

function DisplaySimpleDetails(index){
	 if (index == -1) return 0;
	 if (index >= NumProfiles) return NumProfiles-1;

//	 print ("DisplaySimpleDetails on profile: " + index);
//	 print ("ProfileName: " + ProfileOrdering[index]);
	 var entry = CurrentOpenProfiles[ProfileOrdering[index]];
//	 print ("Cached entry: " + collectionContents(entry));

	 SetupDetailWidgets()

	 // Display the header.
	 var i=0;
	 var widget;
	 var widgetname;
	 var value;
	 for (i=0; i<DetailHead.length; i++){
		  widgetname = GetWidgetNameMapping(DetailHead[i], BluePagesDisplayType);
		  value = checkEmpty(GetAttributes(entry, DetailHead[i], true)); 
		  if (DetailHead[i] == "Notes Mail"){
			   value = DNToNotesID(value);
		  }
		  setWidgetText(DetailHead[i], value);
	 }

	 var j=0;
	 // Display the left column data.
	 for (j=0; j<DetailColumn1.length; j++){
		  value = checkEmpty(GetAttributes(entry, DetailColumn1[j], true)); 
		  setWidgetText(DetailColumn1[j], value);
	 }

	 var k=0;

	 // Display the left column data.
	 for (k=0; k<DetailColumn2.length; k++){
		  value = checkEmpty(GetAttributes(entry, DetailColumn2[k], true)); 
		  setWidgetText(DetailColumn2[k], value);
	 }

	 var ID;
	 // WWW: not sure whether to put this here or in OnDetailsComplete
	 // We put this here because at this point, we're guaranteed to have a
	 // profile, which means that we have all of the information necessary
	 // to fill in the rest of the profile.
	 // Get Business Address
	 AdditionalDetailsSearch(entry, "Business Address");
	 // Get Manager Name.
	 AdditionalDetailsSearch(entry, "Manager");
	 // Get Secretary Name.
	 AdditionalDetailsSearch(entry, "Secretary");
	 // Get Div-Dept.
	 AdditionalDetailsSearch(entry, "Div/Dept");

	 SetupOption("Go To...", DetailsGoToOption, ProfileOrderingNames,
				 null, null, "on_DetailsProfile_activate");

	 return index;
}

// data contains something like "Business Address" or "Manager".
function DisplayAdditionalDetails(type, typearr){
	 var value;
	 // Get the mapping from type to the array of attributes for that 
	 // type.
	 if (typearr){
		  if (type == "Business Address"){
			   value = arrayContents(typearr, "\n", false);
		  } else {
			   value = typearr[0];
		  }
		  setWidgetText(type, value);
	 }
}

function DisplayOrgTree(OrgTreeList){
	 RecursiveOrgTreeInsert(OrgTree, OrgTreeList);
}

// WWW start changing it here.
function RecursiveOrgTreeInsert(parenttree, RestOfList){
// 	 print ("calling recursiveorgtreeinsert on: " + parenttree);
// 	 print ("RecursiveOrgTreeInsert");
	 // look up the name in the manager cache.
	 var employeedata = RestOfList.shift();
	 if (employeedata){
		  var node = CreateNode(employeedata, parenttree, ORGTREE_PREPEND);
		  // Put the node into the parent tree.
		  RecursiveOrgTreeInsert(node.subtree, RestOfList);
	 } else {
//		  print ("No more employee data. Done!");
	 }
}

function CreateNode(employeedata, parenttree, AppendOrPrepend){
	 var nameindex, index;
	 var name, uid, managerdn, ismanager, managerSN, 
		  managerCC, SN, CC;

	 nameindex = GetArrayNameIndex(OrgTreeAttributes, "Name");
	 if (nameindex != -1)
		  name = StripMultipleValue(employeedata[nameindex]);

	 index = GetArrayNameIndex(OrgTreeAttributes, "UID");
	 if (index != -1)
		  uid = StripMultipleValue(employeedata[index]);

	 index = GetArrayNameIndex(OrgTreeAttributes, "Manager DN");
	 if (index != -1)
		  managerdn = StripMultipleValue(employeedata[index]);

	 index = GetArrayNameIndex(OrgTreeAttributes, "Is Manager");
	 if (index != -1)
		  ismanager = StripMultipleValue(employeedata[index]);

	 index = GetArrayNameIndex(OrgTreeAttributes, 
							   "Manager Serial Number");
	 if (index != -1)
		  managerSN = StripMultipleValue(employeedata[index]);

	 index = GetArrayNameIndex(OrgTreeAttributes, 
							   "Manager Country Code");
	 if (index != -1)
		  managerCC = StripMultipleValue(employeedata[index]);

	 index = GetArrayNameIndex(OrgTreeAttributes, 
							   "IBM Serial Number");
	 if (index != -1)
		  SN = StripMultipleValue(employeedata[index]);

	 index = GetArrayNameIndex(OrgTreeAttributes, 
							   "Country Code");
	 if (index != -1)
		  CC = StripMultipleValue(employeedata[index]);

//	 print ("creating new node with label: " + name);
	 var node = new Sash.GTK.GtkTreeItem(name);

	 if (node){
		  if (AppendOrPrepend == ORGTREE_PREPEND){
			   parenttree.prepend(node);
		  }else{
			   parenttree.append(node);
		  }
		  node.visible = true;

		  var signalArgs = new Sash.Linux.Collection();
		  signalArgs.Add("name", name);
		  signalArgs.Add("node", node);
		  signalArgs.Add("uid", uid);

		  node.signalConnect("select", "on_TreeItem_select", 
							 signalArgs);
		  node.signalConnect("deselect", "on_TreeItem_deselect", 
							 signalArgs);

		  // Create a new subtree for the node.
		  node.subtree = new Sash.GTK.GtkTree();
		  node.subtree.signalConnect("button_press_event",
									 "on_OrgTree_button_press_event", 
									 null);
		  if (ismanager == "Y"){
			   node.subtree.visible = true;
	
//			   print ("it's a manager.");
			   var minionsNode = new Sash.GTK.GtkTreeItem(OrgTreeIDString
														  + name);
			   minionsNode.visible = true;
// 			   minionsNode.subtree = new Sash.GTK.GtkTree();
// 			   minionsNode.subtree.visible = true;

			   var minionsSignalArgs = new Sash.Linux.Collection();
			   minionsSignalArgs.Add("name", OrgTreeIDString + name);
			   minionsSignalArgs.Add("managersn", SN);
			   minionsSignalArgs.Add("managercc", CC);
			   minionsSignalArgs.Add("managerdn", managerdn);

			   minionsSignalArgs.Add("parentnode", node);
			   minionsNode.signalConnect("select", "on_TreeItem_select", 
										 minionsSignalArgs);
			   minionsNode.signalConnect("deselect", "on_TreeItem_deselect", 
										 minionsSignalArgs);
			   node.subtree.append(minionsNode);
		  }
//		  node.expand();
	 } else {
//		  print ("returning null node");
	 }
	 return node;

}

function DisplayEditRuleWindow(rule){
	 EditRuleWindow.visible = true;
	 EditRuleWindow.raise();
	 EditRuleLabel.setText(TranslateRule(rule, false));
	 EditRuleIndex.setText(rule[RULES_INDEX]);
	 if (rule[RULES_OPERATOR] == OPERATOR_AND){
		  EditRuleMatchAll.value = true;
	 } else {
		  EditRuleMatchAny.value = true;
	 }
	 
	 // Clear out the existing text first.
	 EditRuleValuesCList.clear();

	 // Put the values into the clist.
	 var values = rule[RULES_VALUES];
	 if (values){
		  var i=0;
		  for (i=0; i<values.length; i++){
			   EditRuleValuesCList.Append([values[i]]);
		  }
	 }
}

function DisplayBluePagesOptions(){
	 BPLOptionWindow.visible = true;
	 BPLOptionWindow.raise();
	 
	 var name = checkEmptyD(LoadBluePagesOption("Server"),
							DefaultBluePagesServerName);
	 var baseDN = checkEmptyD(LoadBluePagesOption("BaseDN"),
							  DefaultBluePagesBaseDN);
	 var maxResults = checkEmptyD(LoadBluePagesOption("MaxResults"),
								  DefaultBluePagesMaxResults);
	 var groupMaxResults = checkEmptyD(LoadBluePagesOption("GroupMaxResults"),
								  DefaultBluePagesGroupMaxResults);
	 var displayType = checkEmptyD(LoadBluePagesOption("DisplayType"),
								   DefaultBluePagesDisplayType);


	 BPLOptionServerName.setText(name);
	 BPLOptionServerBaseDN.setText(baseDN);
	 BPLOptionQLMaxResults.setText(maxResults);
	 BPLOptionGroupMaxResults.setText(groupMaxResults);
	 BPLOptionDisplayType.value = (displayType == DETAIL_TEXTENTRIES);
}

function RedrawCList(cols, extraPrepend){
	 CreateNewCList(cols.length);
	 if (! SetupCList(SearchResultsCList, cols,
					  "on_SearchResultsCList_click_column",
					  "on_SearchResultsCList_resize_column")){
		  Status ("Column error. Add some columns or restart BPL.");
	 }

	 if (extraPrepend != "")
		  cols.unshift(extraPrepend);
}

function RepopulateCList(clist, data, is1Darr){
	 clist.clear();
	 if (data){
		  var i=0;
		  for (i=0; i<data.length; i++){
			   clist.Append(is1Darr ? [data[i]] : data);
		  }
	 }
}

function DisplaySingleAdvancedSearch(AS){
	 if (AS != null)
		  CurrentAdvancedSearch = AS;
	 
	 var name = CurrentAdvancedSearch["name"];
	 EditASName.setText(name);
	 
	 // Fill in the left list (all rules).
	 EditASWindow.visible = true;
	 EditASWindow.raise();
	 
	 var rulestext = [];
	 if (AdvancedSearchList){
		  var i=0;
		  for (i=0; i<AdvancedSearchList.length; i++){
			   rulestext.push( 
					TranslateAdvancedSearchListOption(AdvancedSearchList[i]));
		  }
		  RepopulateCList(EditASAllRulesCList, rulestext, true);
//		  print ("allrules: " + rulestext);
	 }
	 
	 rulestext = [];
	 if (CurrentAdvancedSearch["rules"]){
		  var j=0;
		  for (j=0; j<CurrentAdvancedSearch["rules"].length; j++){
			   rulestext.push(
					TranslateRule(CurrentAdvancedSearch["rules"][j], true));
		  }
		  RepopulateCList(EditASSelectedRulesCList, rulestext, true);
//		  print ("currentrules: " + rulestext);
	 }
	 var operator = CurrentAdvancedSearch["operator"];
	 EditASMatchAll.value = (operator == OPERATOR_AND);
}

function SetupCloseOption(){
	 var menu = new Sash.GTK.GtkMenu();
	 menu.visible = true;

	 var item;
	 item = new Sash.GTK.GtkMenuItem("Close");
	 item.signalConnect("activate", "on_DetailsClose_activate", null);
	 item.visible = true;
	 menu.append(item);

	 item = new Sash.GTK.GtkMenuItem("Close All");
	 item.signalConnect("activate", "on_DetailsCloseAll_activate", null);		  
	 item.visible = true;
	 menu.append(item);

	 DetailsCloseOption.setMenu(menu);
}

function UpdateSearchHistory(){

	 SearchHistoryCombo.setPopdownItems(SearchHistory);
}

/*******
 * Utility functions: Accessing the Registry
 *******/
function SaveQuickListColumns(qlc){
	 Sash.Registry.SetWeblicationValue("QuickList", "Columns", qlc);
}
function LoadQuickListColumns(){
	 return Sash.Registry.QueryWeblicationValue("QuickList", "Columns");
}

function SaveFullQuickListOrdering(){
	Sash.Registry.SetWeblicationValue("QuickList", "AllColumns", allqlc);
}

function LoadFullQuickListOrdering(allqlc){
	return Sash.Registry.QueryWeblicationValue("QuickList", "AllColumns");
}

function SaveBluePagesOption(name, value){
	 Sash.Registry.SetWeblicationValue("Option", name, value);
}
function LoadBluePagesOption(name){
	 return Sash.Registry.QueryWeblicationValue("Option", name);
}

function SaveFavorite(name, currentAdvancedSearch, overwrite){
	 return SaveAS("Favorites", name, currentAdvancedSearch, overwrite);
}

function SaveFilter(name, currentAdvancedSearch, overwrite){
	 return SaveAS("Filters", name, currentAdvancedSearch, overwrite);
}

function SaveAS(type, name, currentAdvancedSearch, overwrite){
	 if (overwrite){
		  var oldval = LoadASStr(type, name);
		  if (oldval == null){
			   // Need to add the name to the "Names".
			   var names = LoadASNames(type);
			   if (names == null) names = [];
			   names.push (name);
			   Sash.Registry.SetWeblicationValue(type, "Names", names);
		  }
		  
		  // Convert to xml.
		  var str = EncodeAdvancedSearchXML(currentAdvancedSearch);
		  Sash.Registry.SetWeblicationValue(type, "AS:"+name, str);
		  return true;
	 } else {
//		  print ("value already exists and no permission to overwrite.");
		  return false;
	 }
}
function RenameFavorite(oldname, newname){
	 return RenameAS("Favorites", oldname, newname);
}
function RenameFilter(oldname, newname){
	 return RenameAS("Filters", oldname, newname);
}

function RenameAS(type, oldname, newname){
	 var xmlstr = Sash.Registry.QueryWeblicationValue(type, "AS:"+oldname);
	 Sash.Registry.DeleteWeblicationValue(type, "AS:"+oldname);
	 Sash.Registry.SetWeblicationValue(type, "AS:"+newname, xmlstr);
	 
	 var favorites = LoadASNames(type);
	 ReplaceArrayItem(favorites, oldname, newname);
	 SaveASNames(type, favorites);
	 return favorites;
}

function LoadFavoriteNames(){
	 return LoadASNames("Favorites");
}
function LoadFilterNames(){
	 return LoadASNames("Filters");
}

function LoadASNames(type){
	 var names = Sash.Registry.QueryWeblicationValue(type, "Names");
	 return names;
}

function SaveFavoriteNames(names){
	 return SaveASNames("Favorites", names);
}
function SaveFilterNames(names){
	 return SaveASNames("Filters", names);
}

function SaveASNames(type, names){
	 var names = 
		  Sash.Registry.SetWeblicationValue(type, "Names", names);
}

function LoadFavoriteStr(name){
	 return LoadASStr("Favorites", name);
}
function LoadFilterStr(name){
	 return LoadASStr("Filters", name);
}

function LoadASStr(type, name){
	 var currentAdvancedSearch = 
		  Sash.Registry.QueryWeblicationValue(type, "AS:" + name);
	 return currentAdvancedSearch;
}


function LoadFavorite(name){
	 return LoadAS("Favorites", name);
}
function LoadFilter(name){
	 return LoadAS("Filters", name);
}
function LoadAS(type, name){
	 var str = LoadASStr(type, name);
	 if (str && (str != "")) return DecodeAdvancedSearchXML(str);
}

function FindFavoriteIndex(name){
	 return FindASIndex("Favorites", name);
}
function FindFilterIndex(name){
	 return FindASIndex("Filters", name);
}
function FindASIndex(type, name){
	 var favorites = LoadASNames(type);
	 if (favorites && (favorites.length > 0)){
		  var i=0;
		  for (i=0; i<favorites.length; i++){
			   if (favorites[i] == name) return i;
		  }
	 }
	 return -1;
}


function DeleteFavorite(name){
	 return DeleteAS("Favorites", name);
}
function DeleteFilter(name){
	 return DeleteAS("Filters", name);
}

function DeleteAS(type, name){
	 var index = FindASIndex(type, name);
	 var favorites = LoadASNames(type);
	 favorites = RemoveFromArray(favorites, index);
	 SaveASNames(type, favorites);
	 Sash.Registry.DeleteWeblicationValue(type, "AS:"+name);
	 return favorites;
}

/*******
 * Basic functionality
 *******/

// Search - QuickList - only need certain columns
// org chart - just need names, ids, and manager.

// getting manager:
// search / bluepages / employeeid = managerid

// follow manager chain all the way up to the top
// draw in dummy guys at each level
// click fills in rest of the group (everyone with the same manager)
// tree manipulation

// Filters - manage and apply filters
// save custom filters to the registry

// Display - get all columns, display in a table, maintain list of people 

//divdept/worklocation -

function onDefaultConnectionError(connection, desc){
	 RequestResults.Append("Connection error: " + desc);
}
function onDefaultRequestError(request, desc){
	 RequestResults.Append("Request error: " + desc);
}

/*******
 * Utility functions: Status messages
 *******/
function Status(message){
	 print ("STATUS: " + message);
	 QuickListStatusLabel.setText(message);
}

/*******
 * Utility functions: Sorting columns
 *******/
function test(val1, val2){
	 if (val1 > val2) return 1;
	 if (val1 == val2) return 0;
	 return -1;
}


/*******
 * Utility functions: Exporting results
 *******/
function exportResults(filename, filemode, showcols, delimiter, nulltext){
//	 print ("exportResults:");
	 var textstream = null;
	 if (Sash.FileSystem.FileExists(filename)){
		  if (filemode == EXPORT_FILE_APPEND){
			   print ("Appending to file: " + filename);
			   textstream = new Sash.FileSystem.TextStream(filename,
														   Sash.FileSystem.MODE_APPEND, true);
//			   print ("textstream opened");
		  } else {
			   if (filemode == EXPORT_FILE_OVERWRITE){
					print ("Overwriting file: " + filename);
					Sash.FileSystem.DeleteFile(filename, true);
					textstream = new Sash.FileSystem.TextStream(filename,
																Sash.FileSystem.MODE_WRITE, true);
			   } else {
					Status("The file exists, but it wasn't modified");
					// If the file exists and it's not append or overwrite.
					return false;
			   }
		  }
	 } else {
//		  print ("Writing to file: " + filename);
		  textstream = new Sash.FileSystem.TextStream(filename, 
													  Sash.FileSystem.MODE_WRITE, true);
	 }
	 
	 if (showcols){
		  // Grab the quicklist columns.
//		  print ("outputting columns");
		  var i=1;
		  for (i=1; i<QuickListColumns.length; i++){
			   textstream.Write(QuickListColumns[i]);
			   if (i != QuickListColumns.length - 1){
					textstream.Write(delimiter);
			   }
		  }
		  textstream.Write('\n');
	 }

	 var j=0;
	 var k=0;

//	 print ("outputting data");
	 for (j=0; j<SearchResultsCList.rows; j++){
		  for (k=0; k<QuickListColumns.length-1; k++){
			   textstream.Write(checkEmptyD(SearchResultsCList.GetText(j,k), 
											nulltext));;
			   if (k != QuickListColumns.length -2){
					textstream.Write(delimiter);
			   }
		  }
		  textstream.Write('\n');
	 }
	 textstream.Close();
	 Status ("Wrote results to " + filename);
}

function CheckFilename(name){
	 return true;
}

/*******
 * Utility functions: Test Code
 *******/
// Get the function's name, then call it.
/*
var funct = FunctionMapping["Callup Name"];
funct(null,null);
*/

/*******
 * Utility functions: Showing help
 *******/
function LoadUrl(a) {
	if (a == "") return;
	Sash.Core.ExecDefaultWebBrowser(a);
}

function ShowHelp() {
	LoadUrl(Sash.Core.WeblicationDataPath + "/help.html");
}

/*******
 * Utility functions: Search/Cancel button
 *******/

function ChangeButtonToCancel(){
	 if (Cancel == null){
		  SearchButtonVBox.remove(Search);
		  Search = null;
		  
		  Cancel = new Sash.GTK.GtkButton();
		  Cancel.visible = true;
		  var label = new Sash.GTK.GtkLabel("Cancel"); 
		  label.visible = true;
		  Cancel.add(label);
		  Cancel.signalConnect("clicked",
							   "on_Cancel_clicked",
							   null);
		  SearchButtonVBox.add(Cancel);
	 }
}

function ChangeButtonToSearch(){
	 if (Search == null){
		  SearchButtonVBox.remove(Cancel);
		  Cancel = null;
		  
		  Search = new Sash.GTK.GtkButton();
		  Search.visible = true;
		  var label = new Sash.GTK.GtkLabel("Search"); 
		  label.visible = true;
		  Search.add(label);
		  Search.signalConnect("clicked",
							   "on_Search_clicked",
							   null);
		  SearchButtonVBox.add(Search);
	 }

}


/*******
 * Signals: Menu Options
 *******/
function on_ldap_options1_activate(){
	DisplayBluePagesOptions();
}

function on_edit_columns1_activate(){
	 var cols = checkEmptyD(LoadFullQuickListOrdering(),
							QuickListAttributes);

//	 print ("cols: " + cols);
	 // populate the columns.
	 var i=0;
	 var currentQLC = checkEmptyD(LoadQuickListColumns(),
								  DefaultQuickListColumns);
	 QLColumnsAllCList.clear();
	 QLColumnsDisplayCList.clear();
	 var index;
	 for (i=0; i<cols.length; i++){
		  index = GetArrayNameIndex(currentQLC, cols[i]);
		  QLColumnsAllCList.Append([cols[i]]);
	 }
	 for (i=0; i<currentQLC.length; i++){
		  if (currentQLC[i] != "UID")
			   QLColumnsDisplayCList.Append([currentQLC[i]]);
	 }
	 QLColumnWindow.visible = true;
	 QLColumnWindow.raise();
}

function on_toggle_details_display1_activate(){
	 if (BluePagesDisplayType == DETAIL_TEXTENTRIES)
		  DesiredDetailsDisplayType = DETAIL_LABELS;
	 else
		  DesiredDetailsDisplayType = DETAIL_TEXTENTRIES;

	 if (DetailsVBox.visible)
		  DisplaySimpleDetails(CurrentProfileIndex);
}

function on_results1_activate(){
	 ExportFilename.setText(DefaultExportFilename);
	 ExportDelimiter.setText(DefaultExportDelimiter);
	 ExportNullText.setText(DefaultExportNullText);
	 ExportAppend.value = (DefaultExportFilemode == EXPORT_FILE_APPEND);
	 ExportOverwrite.value = (DefaultExportFilemode == EXPORT_FILE_OVERWRITE);
	 ExportIncludeTitles.value = DefaultExportIncludeTitles;
	 
	 
	 ExportDialog.visible = true;
	 
}

function on_contents1_activate(){
	 ShowHelp();
}
						  

function on_about1_activate(){
	 AboutWindow.visible = true;
}

/*******
 * Signals: Main BPL Window
 *******/

function on_Search_clicked(){
	 var field = SearchableFields[CurrentSearchField];
	 var data = SearchData.getText();


	 if ((data == null) || (data == "")){
		  Sash.Core.UI.MessageBox("Please specify a query.", "Error",
								  Sash.Core.UI.MB_OK);
	 } else {
		  SearchHistory.unshift(data);
		  UpdateSearchHistory();

		  // Get the filter AS.
		  if (ActiveFilter){
//		  print ("Active filter: " + ActiveFilter[0] + ", " + ActiveFilter[1]);
			   var AS = LoadAS("Filters", ActiveFilter[1]);
			   if (AS){
					QuickListFilteredSearch(field, data, AS, QuickListColumns);
					return;
			   }
		  }
		  QuickListSearch(field, data, QuickListColumns);
	 }
}

function on_Cancel_clicked(){
	 if (SearchRequest.Cancel()){
		  ChangeButtonToSearch();
		  Sash.Core.Cursor.SetStandard(MOUSE_DEFAULT);
		  FavoritesSearchOption.enabled = true;

		  Status("Request cancelled.");
		  LDAPConnection.Disconnect();
		  ConnectToServer();
	 } else {
		  
	 }

}

function on_SearchFavorites_activate(menuitem, indexname){
//	 print ("on_SearchFavorites_activate");
	 if (indexname[0] == OPTION_CUSTOMIZE){
		  ManageASType = "Favorites";
		  ManageASDescription.text = ("");
		  ManageASWindow.setTitle("Manage Search Favorites");
		  ManageASWindow.visible = true;
		  ManageASWindow.raise();
		  UpdateASLists(ManageASType);
	 } else {
		  if (indexname[0] != OPTION_TITLE){
			   var str = LoadFavoriteStr(indexname[1]);
			   var advancedSearch = DecodeAdvancedSearchXML(str);
			   QuickListAdvancedSearch(advancedSearch, QuickListColumns);
		  }
	 }
}

function on_SearchFilters_activate(menuitem, indexname){
	 if (indexname[0] == OPTION_CUSTOMIZE){
		  ActiveFilter = null;
		  ManageASType = "Filters";
		  ManageASDescription.text = ("");
		  ManageASWindow.setTitle("Manage Search Filters");
		  ManageASWindow.visible = true;
		  ManageASWindow.raise();
		  UpdateASLists(ManageASType);
	 } else {
		  if (indexname[0] == OPTION_TITLE)
			   ActiveFilter = null;
		  else 
			   ActiveFilter = indexname;
	 }
}

function on_Searchable_activate(menuitem, indexname){
	 // Record which field was selected.
	 CurrentSearchField = indexname[0];
}


function on_SearchResultsCList_button_press_event(a, num_clicks){
// 	 print ("search button press event: " + num_clicks);
	 clist_double_clicked = (num_clicks == 2);
}

function on_SearchResultsCList_select_row(clist, row){
//	 print("clicked clist, row: " + row);
	 if (! clist_double_clicked) return;
	 clist_double_clicked = false;
//	 print("double clicked clist, row: " + row);
	 
	 if (row >= 0){
		  DetailedProfileSearch(QuickListUIDs[row]);
	 }
}

function on_SearchResultsCList_click_column(dummy, col){
	 if (CurrentSortType == Sash.GTK.SORT_ASCENDING){
		  CurrentSortType = Sash.GTK.SORT_DESCENDING;
	 }else{
		  CurrentSortType = Sash.GTK.SORT_ASCENDING;
	 }
	 SearchResultsCList.setSortType(CurrentSortType);
	 SearchResultsCList.setSortColumn(col);
	 SearchResultsCList.sort();
}

function on_SearchResultsCList_resize_column(dummy, col, width){
// 	 print ("resized column: " + col + ", " + width);
	 var colname = SearchResultsCList.getColumnTitle(col);
	 SaveBluePagesOption("W:" + colname, width);
}


function on_DoDetails_clicked(){
//	 print ("*****DETAILS*****")
	 // Get the appropriate row.
	 var row = SearchResultsCList.focus_row;
//	 print ("row selected: " + row);

	 if (row >= 0){
		  var UID = QuickListUIDs[row];
		  DetailedProfileSearch(UID);
	 }
}
	 

function on_DoOrgTree_clicked(){
//	 print ("*****ORGTREE*****")
	 // Get the appropriate row.
	 var row = SearchResultsCList.focus_row;
//	 print ("row selected: " + row);

	 if (row >= 0){
		  var UID = QuickListUIDs[row];
		  ManagerChainSearch(UID, OrgTreeAttributes);
	 }
}

function on_DoPreviousProfile_clicked(){
	 CurrentProfileIndex = DisplaySimpleDetails(--CurrentProfileIndex);
}

function on_DoNextProfile_clicked(){
	 CurrentProfileIndex = DisplaySimpleDetails(++CurrentProfileIndex);
}

function on_OrgTree_button_press_event(a, num_clicks){
//	 print ("button pressed: " + num_clicks);
	 ctree_double_clicked = (num_clicks == 2);
}

function on_TreeItem_select(treeitem, params){
	 // If it is a normal person and it was a doubleclick, 
	 // then do a detailed search on that person.
	 // WWW: Remove dependency on this.
	 CurrentTreeItemParams = params;
	 var label = params.Item("name");

	 if (ctree_double_clicked){
//		  print ("double clicked");
		  ctree_double_clicked = false;
//		  print ("ctree_double_clicked");
		  // Make sure that it's not a minions node.
		  if (!label.match(OrgTreeIdentifier)){
			   var UID = params.Item("uid");
			   if (UID)
					DetailedProfileSearch(UID);
		  }
	 } else {
//		  print ("not double clicked");
		  // If it is a manager's department, 
		  // then do a search for the whole department.
		  if (label.match(OrgTreeIdentifier)){
			   var node = params.Item("parentnode");
			   var managername = label.substr(OrgTreeIDString.length);
//			   print ("manager name: " + managername);

			   // Remove minions node.
			   treeitem.visible = false;

			   var managerSN = params.Item("managersn");
			   var managerCC = params.Item("managercc");

//			   print ("adding to managernodemapping: " + managerSN);
			   ManagerNodeMapping[managerSN+managerCC] = node;

			   // execute a search on people who have that manager.
			   MinionsSearch(managerSN, managerCC, OrgTreeAttributes);
		  }
	 }
}

function on_TreeItem_deselect(treeitem, params){
	 CurrentTreeItemParams = null;
}

function on_DoOrgDetails_clicked(){
	 if (CurrentTreeItemParams){
		  var label = CurrentTreeItemParams.Item("name");

		  // Make sure that it's not a minions node.
		  if (!label.match(OrgTreeIdentifier)){
			   var UID = CurrentTreeItemParams.Item("uid");
			   if (UID)
					DetailedProfileSearch(UID);
		  }
	 }
}

function on_DoOrgOrgTree_clicked(){
	 if (CurrentTreeItemParams){
		  var label = CurrentTreeItemParams.Item("name");
		  // Make sure that it's not a minions node.
		  if (!label.match(OrgTreeIdentifier)){
			   var UID = CurrentTreeItemParams.Item("uid");
			   if (UID)
					ManagerChainSearch(UID, OrgTreeAttributes);
		  }
	 }
}

/*******
 * Signals: Details
 *******/
function on_DoManagerOrg_clicked(){
	 var UID = GetCurrentPersonUID("Manager DN");
	 if (UID){
		  ManagerChainSearch(UID, OrgTreeAttributes);
	 }
}

function on_DoManagerDetails_clicked(){
	 var UID = GetCurrentPersonUID("Manager DN");
	 if (UID){
		  DetailedProfileSearch(UID);
	 }
}

function on_DoSecretaryOrg_clicked(){
	 var UID = GetCurrentPersonUID("Secretary DN");
	 if (UID){
		  ManagerChainSearch(UID, OrgTreeAttributes);
	 }
}

function on_DoSecretaryDetails_clicked(){
	 var UID = GetCurrentPersonUID("Secretary DN");
	 if (UID){
		  DetailedProfileSearch(UID);
	 }
}

function on_DetailsClose_activate(){
	 // Remove it from the object.
	 var index = CurrentProfileIndex;

//	 print ("index: " + index);

	 if ((index >=0) && (index < ProfileOrdering.length)){
		  var item;
		  if (index == ProfileOrdering.length - 1){
			   CurrentProfileIndex--;
		  }

		  item = ProfileOrdering[index];
		  ProfileOrdering = RemoveFromArray(ProfileOrdering, index);

		  ProfileOrderingNames = RemoveFromArray(ProfileOrderingNames, index);

		  CurrentOpenProfiles[item] = null;
		  NumProfiles--;

		  if (NumProfiles == 0)
			   DetailsVBox.visible = false;
		  else
			   DisplaySimpleDetails(CurrentProfileIndex);
	 }
}

function on_DetailsCloseAll_activate(){
	 CurrentOpenProfiles = new Object();
	 ProfileOrdering = [];
	 ProfileOrderingNames = [];
	 CurrentProfileIndex = -1;
	 NumProfiles = 0;
	 DetailsVBox.visible = false;
}

function on_DetailsProfile_activate(menuitem, indexname){
	 // Switch to the appropriate profile.
	 var index = indexname[0];
	 if ((index >=0) && (index < ProfileOrdering.length)){
		  CurrentProfileIndex = index;
		  DisplaySimpleDetails(index);
	 }
}

/*******
 * Signals: Edit Rule (EditRuleWindow)
 *******/
function on_EditRuleAdd_clicked(){
	 var text = EditRuleValueEntry.getText();
	 EditRuleValueEntry.setText("");
	 if (text && (text != "")){
		  EditRuleValuesCList.Append([text]);
	 }
}

function on_EditRuleRemove_clicked(){
	 var selected = EditRuleValuesCList.SelectedRows;
	 RemoveRowsFromCList(EditRuleValuesCList, selected, 0);
}

function on_EditRuleSave_clicked(){
	 var index = EditRuleIndex.getText();
	 var row = GetRowForRule(index);
	 var currrule = CurrentAdvancedSearch["rules"][row];
	 currrule[RULES_OPERATOR] = (EditRuleMatchAll.value ? OPERATOR_AND : OPERATOR_OR);

	 var values = [];
	 var i=0;
	 for (i=0; i<EditRuleValuesCList.rows; i++){
		  values.push(EditRuleValuesCList.GetText(i, 0));
	 }

//	 print ("rule values to save: " + values);
	 currrule[RULES_VALUES] = values;
	 DisplaySingleAdvancedSearch(CurrentAdvancedSearch);
	 EditRuleWindow.visible = false;
}

function on_EditRuleCancel_clicked(){
	 // Don't save anything.
	 // WWW clear out the window?
	 EditRuleWindow.visible = false;
}

/*******
 * Signals: BPL Options
 *******/

function on_BPLOptionClearHistory_clicked(){
	 SearchHistory = [];
	 UpdateSearchHistory();
}

function on_BPLOptionUpdate_clicked(){
	 SaveBluePagesOption("Server", BPLOptionServerName.getText());
	 SaveBluePagesOption("BaseDN", BPLOptionServerBaseDN.getText());

	 var mr = BPLOptionQLMaxResults.getText();
	 var gmr = BPLOptionGroupMaxResults.getText();

	 SaveBluePagesOption("MaxResults", mr);
	 SaveBluePagesOption("GroupMaxResults", gmr);

	 var val = BPLOptionDisplayType.value;
	 var type = (val ? DETAIL_TEXTENTRIES : DETAIL_LABELS);
	 SaveBluePagesOption("DisplayType", type);

	 BluePagesMaxResults = mr;
	 BluePagesGroupMaxResults = gmr;
	 DesiredDetailsDisplayType = type;
	 
	 if (DetailsVBox.visible)
		  DisplaySimpleDetails(CurrentProfileIndex);

	 BPLOptionWindow.visible = false;
}

function on_BPLOptionCancel_clicked(){
//	 print ("cancel clicked");
	 BPLOptionWindow.visible = false;
}


/*******
 * Signals: Quick list column selection
 *******/
function on_QLColumnsAllCList_button_press_event(a, num_clicks, b){
	 qlcolumnsall_double_clicked = (num_clicks == 2);
}

function on_QLColumnsAllCList_select_row(){
//	 print ("column all clist select row");
	 if (qlcolumnsall_double_clicked){
		  on_QLColumnShow_clicked();
	 }
}

function on_QLColumnsAllCList_unselect_row(){
//	 print ("column all clist unselect row");
}

// My own function to handle changing numbers of selected rows.
function on_QLColumnsDisplayCList_selection_change(){
	 var selected = QLColumnsDisplayCList.SelectedRows;
	 var text;
	 if (selected && (selected.length == 1)){
		  text = QLColumnsDisplayCList.GetText(selected[0], 0);
		  QLColumnMoveUp.enabled = true;
		  QLColumnMoveDown.enabled = true;
		  QLColumnWidth.enabled = true;
		  QLColumnWidthUpdate.enabled = true;

		  QLColumnWidth.setText(
			   checkEmptyD(
					LoadBluePagesOption(
						 "W:" + text),
					DefaultQLColumnWidths[text]));
	 }else{
		  QLColumnMoveUp.enabled = false;
		  QLColumnMoveDown.enabled = false;
		  QLColumnWidth.enabled = false;
		  QLColumnWidthUpdate.enabled = false;
	 }
}

function on_QLColumnsDisplayCList_button_press_event(a, num_clicks){
//	 print ("qlcolsel button press: " + num_clicks);
	 qlcolumnssel_double_clicked = (num_clicks == 2);
}

function on_QLColumnsDisplayCList_select_row(){
//	 print ("column display clist select row");
	 on_QLColumnsDisplayCList_selection_change();

	 if (qlcolumnssel_double_clicked){
		  on_QLColumnHide_clicked();
	 }
}

function on_QLColumnsDisplayCList_unselect_row(){
//	 print ("column display clist unselect row");
	 on_QLColumnsDisplayCList_selection_change();
}

function on_QLColumnShow_clicked(){
	 var selected = QLColumnsAllCList.SelectedRows;
	 var text;
	 for (p in selected){
//		  print ("selected: " + p);
		  text = QLColumnsAllCList.GetText(selected[p], 0);
		  if (text != "") QLColumnsDisplayCList.Append([text]);
	 }

//	 QLColumnsAllCList.unselectAll();
}

function on_QLColumnHide_clicked(){
	 var selected = QLColumnsDisplayCList.SelectedRows;
	 RemoveRowsFromCList(QLColumnsDisplayCList, selected, 1);
}

function on_QLColumnMoveUp_clicked(){
	 var selected = QLColumnsDisplayCList.SelectedRows;
	 var numselected = selected.length;
	 if (numselected != 1){
//		  print ("can't move 0 or multiple selections");
		  return;
	 }

	 // If it's already on top.
	 if (selected[0] == 0) return;
	 QLColumnsDisplayCList.swapRows(selected[0], selected[0]-1);
}

function on_QLColumnMoveDown_clicked(){
	 var selected = QLColumnsDisplayCList.SelectedRows;
	 var numselected = selected.length;
	 if (numselected != 1){
//		  print ("can't move 0 or multiple selections");
		  return;
	 }
	 // If it's already on top.
	 if (selected[0] == QLColumnsDisplayCList.rows - 1) return;
	 QLColumnsDisplayCList.swapRows(selected[0], selected[0]+1);

}

function on_QLColumnWidthUpdate_clicked(){
	 var selected = QLColumnsDisplayCList.SelectedRows;
	 var numselected = selected.length;
	 if (numselected != 1){
//		  print ("can't move 0 or multiple selections");
		  return;
	 }
	 SaveBluePagesOption("W:" + QLColumnsDisplayCList.GetText(selected[0], 0),
						 QLColumnWidth.getText());
}

function on_QLColumnSave_clicked(){
	 var numrows = QLColumnsDisplayCList.rows;
	 var arr = [];

	 var i=0;
	 for (i=0; i<numrows; i++){
		  arr.push(QLColumnsDisplayCList.GetText(i, 0));
	 }
	 QuickListColumns = arr;
	 SaveQuickListColumns(arr);
	 RedrawCList(QuickListColumns, "UID");
	 QLColumnWindow.visible = false;
}

function on_QLColumnCancel_clicked(){
	 QLColumnWindow.visible = false;
}



/*******
 * Signals: Manage Advanced Search
 *******/

function on_ManageASNameCList_button_press_event(a, num_clicks){
	 manageassel_double_clicked = (num_clicks == 2);
}

function on_ManageASNameCList_select_row(clist, row){
	 var name = ManageASNameCList.GetText(row, 0);
	 var fav = LoadAS(ManageASType, name);
	 if (fav){
		  ManageASCopy.enabled = true;
		  ManageASEdit.enabled = true;
		  ManageASRemove.enabled = true;
		  var str = TranslateAdvancedSearch(fav);
//		  print ("str: " + str);
		  ManageASDescription.text = (str);
		  
		  if (manageassel_double_clicked){
			   manageassel_double_clicked = false;
			   on_ManageASEdit_clicked();
		  }

	 }

}
function on_ManageASNameCList_unselect_row(clist,row){
	 ManageASCopy.enabled = false;
	 ManageASEdit.enabled = false;
	 ManageASRemove.enabled = false;
	 ManageASDescription.text = ("");
}

function on_ManageASExecute_clicked(){
	 var selected = ManageASNameCList.SelectedRows;
	 var row = (selected.length == 1 ? selected[0] : -1);
	 if (row != -1){
		  row = selected[0];
		  var name = ManageASNameCList.GetText(row, 0);	
		  var fav = LoadAS(ManageASType, name);
		  QuickListAdvancedSearch(fav, QuickListColumns);
	 }
}

function on_ManageASClose_clicked(){
	 ManageASWindow.visible = false;
}

// WWW this doesn't work.
function on_delete_event(delwindow, event){
//	 print ("on_delete_event");
	 delwindow.hide();
	 return true;
}

/*******
 * Signals: Closing windows.
 *******/
function on_BPLOptionWindow_delete_event(){
	 BPLOptionWindow.visible = false;
	 return true;
}

function on_QLColumnWindow_delete_event(){
	 QLColumnWindow.visible = false;
	 return true;
}

function on_EditASWindow_delete_event(){
	 EditASWindow.visible = false;
	 return true;
}
function on_ManageASWindow_delete_event(){
	 ManageASWindow.visible = false;
	 return true;
}

function on_ASNameWindow_delete_event(){
	 ASNameWindow.visible = false;
	 return true;
}

function on_EditRuleWindow_delete_event(){
	 EditRuleWindow.visible = false;
	 return true;
}
function on_AboutWindow_delete_event(){
	 AboutWindow.visible = false;
	 return true;
}
function on_ExportDialog_delete_event(){
	 ExportDialog.visible = false;
	 return true;
}

function on_ExportFileSelect_delete_event(){
	 ExportFileSelect.visible = false;
	 return true;
}

/*******
 * Signals: Manage Advanced Searches (ManageASWindow)
 *******/

function on_ManageASNew_clicked(){
	 ASNameOperation.setText(OPERATION_NEW);
	 ASNameWindow.visible = true;
	 ASNameEntry.setText("");
	 ASNameWindow.raise();
}

function on_ManageASCopy_clicked(){
	 var selected = ManageASNameCList.SelectedRows;
	 var row = (selected.length == 1 ? selected[0] : -1);
	 if (row != -1){
		  var name = ManageASNameCList.GetText(row, 0);
		  ASNameOldName.setText(name);
		  ASNameOperation.setText(OPERATION_COPY);
		  ASNameWindow.visible = true;
		  ASNameWindow.raise();
	 }
}

function on_ManageASEdit_clicked(){
	 var selected = ManageASNameCList.SelectedRows;
	 var row = (selected.length == 1 ? selected[0] : -1);
	 if (row != -1){
		  row = selected[0];
		  var name = ManageASNameCList.GetText(row, 0);	
		  var fav = LoadAS(ManageASType, name);
		  if (fav)
			   DisplaySingleAdvancedSearch(fav);
	 }
}

function on_ManageASCopyAndEdit_clicked(){
	 var selected = ManageASNameCList.SelectedRows;
	 var row = (selected.length == 1 ? selected[0] : -1);
	 if (row != -1){
		  var name = ManageASNameCList.GetText(row, 0);
		  ASNameOldName.setText(name);
		  ASNameOperation.setText(OPERATION_COPY_AND_EDIT);
		  ASNameWindow.visible = true;
		  ASNameWindow.raise();
	 }
}

function on_ManageASRename_clicked(){
	 var selected = ManageASNameCList.SelectedRows;
	 var row = (selected.length == 1 ? selected[0] : -1);
	 if (row != -1){
		  var name = ManageASNameCList.GetText(row, 0);
		  ASNameOldName.setText(name);
		  ASNameOperation.setText(OPERATION_RENAME);
		  ASNameWindow.visible = true;
		  ASNameWindow.raise();
	 }
}

function on_ManageASRemove_clicked(){
	 var selected = ManageASNameCList.SelectedRows;
	 var row = (selected.length == 1 ? selected[0] : -1);
	 if (row != -1){
		  var name = ManageASNameCList.GetText(row, 0);
		  var favorites = DeleteAS(ManageASType, name);
		  ManageASDescription.text = "";
		  RepopulateCList(ManageASNameCList, favorites, true);
		  if (row == favorites.length)
			   ManageASNameCList.selectRow(row -1, 0);
		  else 
			   ManageASNameCList.selectRow(row, 0);
	 }
}

/*******
 * Signals: Edit Advanced Search Name window (ASNameWindow)
 *******/
function on_ASNameOK_clicked(){
	 var name = ASNameEntry.getText();
//	 print ("name: " + name);
	 var operation = ASNameOperation.getText();
//	 print ("op: " + operation);
	 var oldname = ASNameOldName.getText();
//	 print ("oldname: " + oldname);
	 var newAS;

	 if (name == ""){
		  Sash.Core.UI.MessageBox("Please enter a name.", "Error", 
								  Sash.Core.UI.MB_OK);
	 } else {
		  var fav = LoadAS(ManageASType, name);
		  if (fav != null){
			   Sash.Core.UI.MessageBox(
					"A search by that name already exists. Please enter something different.", 
					"Error", 
					Sash.Core.UI.MB_OK);
			   ASNameEntry.selectRegion(0, name.length);
		  } else {
			   if (operation == OPERATION_NEW){
					newAS = DummyAdvancedSearch(name);
					DisplaySingleAdvancedSearch(newAS);
			   } else if (operation == OPERATION_COPY){
					// Copy the old rule to a rule of a new name.
					var oldfav = LoadAS(ManageASType, oldname);
					newAS = oldfav;
					newAS["name"] = name;
					SaveAS(ManageASType, name, newAS, true);
					CurrentAdvancedSearch = newAS;
			   } else if (operation == OPERATION_COPY_AND_EDIT){
					// Copy the old rule to a rule of a new name.
					var oldfav = LoadAS(ManageASType, oldname);
					newAS = oldfav;
					newAS["name"] = name;
					DisplaySingleAdvancedSearch(newAS);
			   } else if (operation == OPERATION_RENAME){
					var fav = LoadAS(ManageASType, oldname);
					CurrentAdvancedSearch = fav;
					if (oldname != name)
						 RenameAS(ManageASType, oldname, name);
			   }
			   ASNameWindow.visible = false;
			   UpdateASLists(ManageASType);
		  }
	 }
}

function on_ASNameCancel_clicked(){
	 ASNameWindow.visible = false;
}

/*******
 * Signals: Edit Advanced Search window (EditASWindow)
 *******/
function on_EditASAllRulesCList_button_press_event(a, num_clicks){
	 editasall_double_clicked = (num_clicks == 2);
}

function on_EditASAllRulesCList_select_row(){
	 if (editasall_double_clicked){
		  on_EditASSelect_clicked();
	 } 

}

function on_EditASAllRulesCList_unselect_row(){}

function on_EditASSelectedRulesCList_button_press_event(a, num_clicks){
	 editassel_double_clicked = (num_clicks == 2);
}
function on_EditASSelectedRulesCList_select_row(){
	 on_EditASSelectedRulesCList_selection_change();
	 if (editassel_double_clicked){
		  on_EditASEditRule_clicked();
	 }
}

function on_EditASSelectedRulesCList_unselect_row(){
	 on_EditASSelectedRulesCList_selection_change();
}

// My own function to handle changing numbers of selected rows.
function on_EditASSelectedRulesCList_selection_change(){
	 var selected = EditASSelectedRulesCList.SelectedRows;
	 var text;
	 if (selected && (selected.length == 1)){
		  EditASEditRule.enabled = true;
	 }else{
		  EditASEditRule.enabled = false;
	 }
}

function on_EditASSelect_clicked(){
	 var row = EditASAllRulesCList.focus_row;
	 if (row != -1){
		  var newrule = ConstructRule(AdvancedSearchList[row], row);
		  CurrentAdvancedSearch["rules"].push(newrule);
		  DisplaySingleAdvancedSearch();
	 }
}

function on_EditASUnselect_clicked(){
	 var row = EditASSelectedRulesCList.focus_row;
	 if (row != -1){
		  var selected = EditASSelectedRulesCList.SelectedRows;
		  var text;
		  var i=0;
		  var remove = [];
		  
		  if (selected){
			   for (i=0; i < selected.length; i++){
//					print (i + ": " + selected[i]);
					text = EditASSelectedRulesCList.GetText(selected[i], 0);
					remove.push(selected[i]);
			   }
		  }

		  remove.sort();
//		  print ("remove after sort: " + remove);
		  var j=0;
		  for (j=remove.length-1; j >= 0; j--){
//			   print ("removing row: " + remove[j]);
			   EditASSelectedRulesCList.RemoveRow(remove[j]);
			   CurrentAdvancedSearch["rules"] = RemoveFromArray(CurrentAdvancedSearch["rules"], remove[j]);
		  }
	 }
}

function on_EditASEditRule_clicked(){
	 var row = EditASSelectedRulesCList.focus_row;
	 var selected = EditASSelectedRulesCList.SelectedRows;
	 if (selected.length != 1){
//		  print ("Select exactly 1 rule to edit.");
	 } else {
		  DisplayEditRuleWindow(CurrentAdvancedSearch["rules"][selected[0]]);
	 }
}

function on_EditASApply_clicked(){
	 var asname = EditASName.getText();
	 var op = EditASMatchAll.value ? OPERATOR_AND : OPERATOR_OR;
	 CurrentAdvancedSearch["operator"] = op;
	 if (asname && (asname != "")){
		  SaveAS(ManageASType, asname, CurrentAdvancedSearch, true);
	 }
	 EditASWindow.visible = false;
	 UpdateASLists(ManageASType);
}

function on_EditASExecute_clicked(){
	 var op = EditASMatchAll.value ? OPERATOR_AND : OPERATOR_OR;
	 CurrentAdvancedSearch["operator"] = op;
	 QuickListAdvancedSearch(CurrentAdvancedSearch, QuickListColumns);
}

function on_EditASClose_clicked(){
	 EditASWindow.visible = false;
}

/*******
 * Signals: Export Results
 *******/

function on_ExportFileOK_clicked(){
	 var fn = ExportFileSelect.getFilename();
	 ExportFilename.setText(fn);
	 ExportFileSelect.visible = false;
}

function on_ExportFileCancel_clicked(){
	 ExportFileSelect.visible = false;
}
function on_ExportChooseFile_clicked(){
	 ExportFileSelect.visible = true;
}

function on_ExportExport_clicked(){
	 var filename = checkEmptyD(ExportFilename.getText(),
								DefaultExportFilename);
	 var delimiter = checkEmptyD(ExportDelimiter.getText(),
								 DefaultExportDelimiter);
	 var nulltext = checkEmptyD(ExportNullText.getText(),
								DefaultExportNullText);
	 var filemode = (ExportAppend.value ? EXPORT_FILE_APPEND
					 : EXPORT_FILE_OVERWRITE);
	 var includetitles = ExportIncludeTitles.value;
	 exportResults(filename, filemode, includetitles,
				   delimiter, nulltext);
}

function on_ExportCancel_clicked(){
	 ExportDialog.visible = false;
}


/*******
 * Signals: About Window
 *******/
function on_AboutOK_clicked(){
	 AboutWindow.visible = false;
}

/*******
 * Startup Code
 *******/

// Get the QuickList columns.
var BluePagesServerName = null;
var BluePagesBaseDN = null;
var BluePagesMaxResults = -1;
var BluePagesGroupMaxResults = -1;
var BluePagesDisplayType = DETAIL_LABELS;

BluePagesServerName = checkEmptyD(LoadBluePagesOption("Server"),
								 DefaultBluePagesServerName);
BluePagesBaseDN = checkEmptyD(LoadBluePagesOption("BaseDN"),
							 DefaultBluePagesBaseDN);
BluePagesMaxResults = checkEmptyD(LoadBluePagesOption("MaxResults"),
								 DefaultBluePagesMaxResults);
BluePagesGroupMaxResults = checkEmptyD(LoadBluePagesOption("GroupMaxResults"),
								 DefaultBluePagesGroupMaxResults);

// This hack will guarantee that 
// BluePagesDisplayType != DesiredDetailsDisplayType
// the first time SetupDetailWidgets is run, which will ensure
// that the widgets are set up correctly the first time.
print ("Setting up widgets...");
DesiredDetailsDisplayType = checkEmptyD(LoadBluePagesOption("DisplayType"),
								 DETAIL_LABELS);
BluePagesDisplayType = 1 - DesiredDetailsDisplayType;

QuickListColumns = LoadQuickListColumns();
if (QuickListColumns == null){
	 QuickListColumns = DefaultQuickListColumns;
}

RedrawCList(QuickListColumns, "UID");

SetupLabelWidgets();
SetupDetailWidgets();

//SetupSearchFiltersDialog();
SetupSearchFieldOption(SearchFieldOption, SearchableFields, 
					   "on_Searchable_activate");
var favorites = LoadFavoriteNames();
SetupSearchFavoritesOption(FavoritesSearchOption, favorites, 
						   OPTION_CUSTOMIZE, "Customize...",
						   "on_SearchFavorites_activate");
var filters = LoadFilterNames();
SetupSearchFiltersOption(FiltersSearchOption, filters, 
						   OPTION_CUSTOMIZE, "Customize...",
						   "on_SearchFilters_activate");
SetupCloseOption();

print ("Connecting to server " + BluePagesServerName + "...");
if (ConnectToServer()){
	 Status("Ready for search.");
} else {
	 Status("Error connecting to server.");
}

