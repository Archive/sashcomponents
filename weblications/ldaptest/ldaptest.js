var gladefile = new Sash.Glade.GladeFile("ldaptest.glade");

// Login widgets
var Server = gladefile.getWidget("Server");
var Username = gladefile.getWidget("Username");
var Password = gladefile.getWidget("Password");
var Status = gladefile.getWidget("Status");

// Search Request widgets
var SearchBaseDN = gladefile.getWidget("SearchBaseDN");
var SearchScope = gladefile.getWidget("SearchScope");
var SearchFilter = gladefile.getWidget("SearchFilter");
var SearchAttributes = gladefile.getWidget("SearchAttributes");

// Compare Request widgets
var CompareDN = gladefile.getWidget("CompareDN");
var CompareAttribute = gladefile.getWidget("CompareAttribute");
var CompareValue = gladefile.getWidget("CompareValue");

// Add Entry widgets
var AddEntryDN = gladefile.getWidget("AddEntryDN");
var AddEntryAttributes = gladefile.getWidget("AddEntryAttributes");
var AddEntryAttributeName = gladefile.getWidget("AddEntryAttributeName");
var AddEntryAttributeValues = gladefile.getWidget("AddEntryAttributeValues");

// Delete Entry widget
var DeleteEntryDN = gladefile.getWidget("DeleteEntryDN");

// Rename Entry Widget
var RenameEntryDN = gladefile.getWidget("RenameEntryDN");
var RenameEntryNewRDN = gladefile.getWidget("RenameEntryNewRDN");
var RenameEntryNewParent = gladefile.getWidget("RenameEntryNewParent");
var RenameEntryDeleteRDN = gladefile.getWidget("RenameEntryDeleteRDN");

var EntryNotebook = gladefile.getWidget("EntryNotebook");

// Edit Attributes
var EditAttributeDN = gladefile.getWidget("EditAttributeDN");
var EditAttributeAttributes = gladefile.getWidget("EditAttributeAttributes");
var EditAttributeName = gladefile.getWidget("EditAttributeName");
var EditAttributeValues = gladefile.getWidget("EditAttributeValues");
var EditAttributeType = gladefile.getWidget("EditAttributeType");

var ADD_ATTRIBUTE = 0, DELETE_ATTRIBUTE =1, REPLACE_ATTRIBUTE = 2;

// Output Widget
var RequestResults = gladefile.getWidget("RequestResults");

// Global variables.
var LDAPConnection = null;
var searchRequest = null;
var compareRequest = null;
var entryRequest = null;
var editAttributeRequest = null;

var DataCache = new Object();

// Initial data.
Server.setText("bluepages.ibm.com");
SearchBaseDN.setText("ou=bluepages,o=ibm.com");
SearchFilter.setText("sn=Corwin");
SearchAttributes.text = "*";

var editAttributeCollection = new Sash.Linux.Collection();
var addEntryCollection = new Sash.Linux.Collection();
addEntryCollection.Add("sn", ["Wang"]);
addEntryCollection.Add("cn", ["Grant Wang"]);
addEntryCollection.Add("objectClass", ["top", "person", "organizationalPerson", "inetOrgPerson"]);

// Utility functions.
function arrayContents(arr){
	 var arrstr = "";
	 if (arr == null) return "null array" ;
	 if (arr.length == 0) return "empty array" ;


	 for (i=0; i<arr.length; i++){
		  arrstr += arr[i];
		  if (i != arr.length - 1) arrstr += ", ";
	 }
	 return arrstr;
}
function collectionInfo(collection){
	 var retstr = "";
	 var keys = collection.Keys;
	 retstr += ("The collection has " + keys.length + " elements.\n");
	 if (keys.length > 0){
/*
		  printsys("normal key list:");
		  for (i=0; i<keys.length; i++){
			   printsys ("key: " + keys[i]);
		  }

		  printsys("trying to access values during iteration:");
		  for (i=0; i<keys.length; i++){
			   printsys ("key: " + keys[i]);
			   values = collection.Item(keys[0]);
		  }

		  printsys("normal key list:");
		  for (j=0; j<keys.length; j++){
			   printsys ("key: " + keys[j]);
		  }
*/
		  for (k=0; k<keys.length; k++){
//			   printsys("current key (" + k + "): " + keys[k]);
			   retstr += ("name: " + keys[k] + ", value: " + arrayContents(collection.Item(keys[k])) + "\n");
//			   printsys("finished with key (" + k + ")");
		  }
//		  printsys("k: " + k);
	 }
	 return retstr;
}

function printDataCache(){
	 for (p in DataCache){
		  print (p + ": " + (collectionInfo(DataCache[p])));
	 }
}

function getArrayFromTextArea(EditAttributeValues){

}

function onDefaultConnectionError(connection, desc){
	 RequestResults.Append("Connection error: " + desc);
}
function onDefaultRequestError(request, desc){
	 RequestResults.Append("Request error: " + desc);
}




function on_Connect_clicked(){
	if (LDAPConnection == null){
		var server = Server.getText();
		var username = Username.getText();
		var password = Password.getText();
	
		// Constructor does the correct thing even if username and
		// password are empty.
		print ("Trying to create new LDAP connection.");
		LDAPConnection = new Sash.LDAP.Connection (server, username, password);
		
		if (LDAPConnection){
			 LDAPConnection.OnError = onDefaultConnectionError;
			 if (LDAPConnection.Connect()) 
				  Status.setText ("New connection established.");
			 else
				  Status.setText ("Couldn't connect to server.");
		}else{
			 Status.setText ("Connection not created.");
			 LDAPConnection = null;
		}
	} else {
		 Status.setText ("LDAP connection exists.");
	}
}

function on_Disconnect_clicked(){
	 if (LDAPConnection == null){
		  Status.setText ("Not currently connected.\n");
	 } else {
		  LDAPConnection.Disconnect();
		  LDAPConnection = null;
		  Status.setText ("Disconnected from server.\n");
	 }
}

// Search Test
function on_SearchInvoke_clicked(){
	 RequestResults.text = "";
	if (LDAPConnection){
		var baseDN = SearchBaseDN.getText();
		var scopestr = SearchScope.getText();
		var scope;
		if (scopestr == "Base") {
			 scope = Sash.LDAP.SCOPE_BASE;
		} else if (scopestr == "One Level") {
			 scope = Sash.LDAP.SCOPE_ONELEVEL;
		} else if (scopestr == "Subtree") {
			 scope = Sash.LDAP.SCOPE_SUBTREE;
		}
		var filter = SearchFilter.getText();
		var attributesArr = SearchAttributes.text.split('\n');
		
		print ("Trying to create new LDAP search request.");
		// might want to re-use the old one.
		searchRequest = new LDAPConnection.SearchRequest(baseDN, scope, filter, attributesArr);

		if (searchRequest){
			 print ("New LDAP search request created.");
			 searchRequest.OnSearchResult = onSearchResult;
			 searchRequest.OnSearchComplete = onSearchComplete;
			 searchRequest.OnError = onDefaultRequestError;
			 
			 print ("Invoking search request.");
			 searchRequest.Invoke();
		} else {
			 print ("Search request not created.");
		}
	}
}

function cancel(request){
	 var retstr = "";
	 if (request){
		  if (request.Cancel()){
			   retstr += "\n*** Request cancelled. ***";
		  } else {
			   retstr += "\n*** Request was not cancelled. ***\n";
		  }
		  return retstr;
	 }
}

function on_SearchCancel_clicked(){
	 RequestResults.Append(cancel(searchRequest));
}

function onSearchResult(searchRequest, entry){
	 var newtext = "Search result (ID: " + searchRequest.ID + ") received. Contents: ";
	 newtext += collectionInfo (entry);
	 RequestResults.Append(newtext);

	 // GGG cache this.
	 var key = searchRequest.BaseDN + ":" + searchRequest.Filter + ":" 
		  + searchRequest.Attributes;
	 print ("caching data for key: " + key);
	 DataCache[key] = entry;
	 
	 print ("current data cache:");
	 printDataCache();
}

function onSearchComplete(searchRequest, entries, success, description){
	 var newtext = "";
	 if (success){
		  newtext += "Search (ID: " + searchRequest.ID + ") completed successfully. (See console)";
		  print ("Search completed: " + entries.length + " entries returned.");
		  if (entries.length > 0){
			   for (i=0; i<entries.length; i++){
					print ("Entry " + i);
					print (collectionInfo(entries[i]));
			   }
		  }
	 }else{
		  newtext += "Search completed unsuccessfully. \n";
		  newtext += ("Description: " + description);
	 }
	 RequestResults.Append(newtext);
}

// Compare Test
function on_CompareInvoke_clicked(){
	 RequestResults.text = "";
	if (LDAPConnection){
		var DN = CompareDN.getText();
		var attribute = CompareAttribute.getText();
		var value = CompareValue.getText();

		print ("Trying to create new LDAP compare request.");
		compareRequest = new LDAPConnection.CompareRequest(DN, attribute, value);

		if (compareRequest){
			 print ("New LDAP compare request created.");
			 compareRequest.OnCompareComplete = onCompareComplete;
			 compareRequest.OnError = onDefaultRequestError;
			 
			 print ("Invoking compare request.");
			 compareRequest.Invoke();
		} else {
			 print ("Compare request not created.");
		}
	} else {

	}

}

function on_CompareCancel_clicked(){
	 RequestResults.Append(cancel(compareRequest));
}

function onCompareComplete(compareRequest, result, success, description){
	 var newtext = "";
	 if (success){
		  if (result)
			   newtext += "Compare completed successfully (true).";
		  else
			   newtext += "Compare completed successfully (false).";

	 }else{
		  newtext += "Compare completed unsuccessfully. \n";
		  newtext += ("Description: " + description);
	 }
	 RequestResults.text = newtext;
}

// Add Test
function on_AddEntryAttributes_select_row(dummy, row){
	 printsys("row clicked: " + row);
	 var i=0;
	 var keys = addEntryCollection.Keys;
	 AddEntryAttributeName.setText(keys[row]);
	 AddEntryAttributeValues.text = arrayToString(addEntryCollection.Item(keys[row]), "\n");
	 
}

function on_AddEntryAddAttribute_clicked(){
	var attrname = AddEntryAttributeName.getText();
	printsys("attrname: " + attrname);
	var attrvalues = AddEntryAttributeValues.text.split('\n');
	if (attrname != ""){
		 addEntryCollection.Add(attrname, attrvalues);
	}
	redraw_clist(AddEntryAttributes, addEntryCollection);
	AddEntryAttributeName.setText("");
	AddEntryAttributeValues.text = "";
}

function on_AddEntryDeleteAttribute_clicked(){
	var attrname = AddEntryAttributeName.getText();
	printsys("attrname: " + attrname);
	if (attrname != ""){
		 addEntryCollection.Remove(attrname);
	}
	redraw_clist(AddEntryAttributes, addEntryCollection);
	AddEntryAttributeName.setText("");
	AddEntryAttributeValues.text = "";
}

function on_AddEntryUpdateAttribute_clicked(){
	 var attrname = AddEntryAttributeName.getText();
	 if (attrname != ""){
		  keys = addEntryCollection.Keys;
		  if (keys){
			   var i;
			   for (i=0; i<keys.length; i++){
					if (keys[i] == attrname){
						 on_AddEntryAddAttribute_clicked();
						 return;
					}
			   }
		  }
	 }
}

function on_AddEntryDeleteAll_clicked(){
	 printsys("deleteall");
	 addEntryCollection.RemoveAll();
	 redraw_clist(AddEntryAttributes, addEntryCollection);
	 AddEntryAttributeName.setText("");
	 AddEntryAttributeValues.text = "";
}

function onAddEntryComplete(request, entry, success, desc){
	 var newtext = "";
	 if (success){
		  newtext += "Added entry succesfully: \n";
		  newtext += collectionInfo(entry);
	 }else{
		  newtext += "Couldn't add entry. \n";
		  newtext += ("Description: " + desc);
	 }
	 RequestResults.text = newtext;
}

// Delete Test
function onDeleteEntryComplete(request, success, desc){
	 var newtext = "";
	 if (success){
		  newtext += "Deleted entry succesfully";
	 }else{
		  newtext += "Couldn't delete entry. \n";
		  newtext += ("Description: " + desc);
	 }
	 RequestResults.text = newtext;
}

// Rename Test
function onRenameEntryComplete(request, success, desc){
	 var newtext = "";
	 if (success){
		  newtext += "Replaced entry succesfully";
	 }else{
		  newtext += "Couldn't replace entry. \n";
		  newtext += ("Description: " + desc);
	 }
	 RequestResults.text = newtext;
}

function on_EntryInvoke_clicked(){
	 var page = EntryNotebook.getCurrentPage();
	 printsys("page of notebook: " + page);
	 if (page == 0) {
		  printsys("Adding an entry");
		  if (LDAPConnection){
			   var DN = AddEntryDN.getText();
			   entryRequest = new LDAPConnection.AddEntryRequest(DN, addEntryCollection);
			   if (entryRequest){
					entryRequest.OnAddEntryComplete = onAddEntryComplete;
					entryRequest.OnError = onDefaultRequestError;
			   }
		  }
	 } else if (page == 1){
		  printsys("Deleting an entry");
		  if (LDAPConnection){
			   var DN = DeleteEntryDN.getText();
			   entryRequest = new LDAPConnection.DeleteEntryRequest(DN);
			   if (entryRequest){
					entryRequest.OnDeleteEntryComplete 
						 = onDeleteEntryComplete;
					entryRequest.OnError = onDefaultRequestError;
			   }
		  }
	 } else if (page ==2 ){
		  printsys("Renaming an entry");
		  if (LDAPConnection){
			   var DN = RenameEntryDN.getText();
			   var NewRDN = RenameEntryNewRDN.getText();
			   var NewParent = RenameEntryNewParent.getText();
			   var deleteRDN = RenameEntryDeleteRDN.value;
			   entryRequest = new LDAPConnection.RenameEntryRequest(DN, NewRDN, NewParent, deleteRDN);
			   if (entryRequest){
					entryRequest.OnRenameEntryComplete 
						 = onRenameEntryComplete;
					entryRequest.OnError = onDefaultRequestError;
			   }
		  }
	 }
	 if (entryRequest)
		  entryRequest.Invoke();

}

function on_EntryCancel_clicked(){
	 RequestResults.Append(cancel(entryRequest));
}



// AddAttribute
// DeleteAttribute
// ReplaceAttribute

function arrayToString(arr, delimiter){
	 if ((delimiter == null) || (delimiter == ""))
		  delimiter = ", ";

	 if (arr == null) return "";
	 
	 var retstr = "";
	 for (i=0; i<arr.length; i++){
		  retstr += arr[i];
		  if (i != arr.length - 1)
			   retstr += delimiter;
	 }
	 return retstr;
}

function redraw_clist(attributeclist, attributecollection){
	 attributeclist.clear();
	 if ((attributeclist == null) || (attributecollection == null))
		  return false;
	 
	 var keys = attributecollection.Keys;
	 var currItem;
	 var i=0;

	 for (i=0; i<keys.length; i++){
		  currItem = attributecollection.Item(keys[i]);
		  attributeclist.Append([keys[i], arrayToString(currItem)]);
	 }

}

function on_EditAttributeAttributes_select_row(dummy, row){
	 printsys("row clicked: " + row);
	 var i=0;
	 var keys = editAttributeCollection.Keys;
	 EditAttributeName.setText(keys[row]);
	 EditAttributeValues.text = arrayToString(editAttributeCollection.Item(keys[row]), "\n");
	 
}

function on_EditAttributeAddAttribute_clicked(){
	var attrname = EditAttributeName.getText();
	printsys("attrname: " + attrname);
	var attrvalues = EditAttributeValues.text.split('\n');
	if (attrname != ""){
		 editAttributeCollection.Add(attrname, attrvalues);
	}
	redraw_clist(EditAttributeAttributes, editAttributeCollection);
	EditAttributeName.setText("");
	EditAttributeValues.text = "";
}

function on_EditAttributeDeleteAttribute_clicked(){
	var attrname = EditAttributeName.getText();
	printsys("attrname: " + attrname);
	if (attrname != ""){
		 editAttributeCollection.Remove(attrname);
	}
	redraw_clist(EditAttributeAttributes, editAttributeCollection);
	EditAttributeName.setText("");
	EditAttributeValues.text = "";
}

function on_EditAttributeUpdateAttribute_clicked(){
	 var attrname = EditAttributeName.getText();
	 if (attrname != ""){
		  keys = editAttributeCollection.Keys;
		  if (keys){
			   var i;
			   for (i=0; i<keys.length; i++){
					if (keys[i] == attrname){
						 on_EditAttributeAddAttribute_clicked();
						 return;
					}
			   }
		  }
	 }
}

function on_EditAttributeDeleteAll_clicked(){
	 printsys("deleteall");
	 editAttributeCollection.RemoveAll();
	 redraw_clist(EditAttributeAttributes, editAttributeCollection);
	 EditAttributeName.setText("");
	 EditAttributeValues.text = "";
}


function EditType(){
	 if (EditAttributeType.getText() == "Add"){
		  printsys("Edit Type: Add");
		  return ADD_ATTRIBUTE;
	 }else if (EditAttributeType.getText() == "Delete"){
		  printsys("Edit Type: Delete");
		 return DELETE_ATTRIBUTE;
	 }else{		  
		  printsys("Edit Type: Replace");
		 return REPLACE_ATTRIBUTE;
	 }
}

function onAddAttributeComplete(request, success, description){
	 var newtext = "";
	 if (success){
		  newtext += "Added attribute succesfully";
	 }else{
		  newtext += "Couldn't add attribute. \n";
		  newtext += ("Description: " + description);
	 }
	 RequestResults.text = newtext;
}

function onDeleteAttributeComplete(request, success, description){
	 var newtext = "";
	 if (success){
		  newtext += "Deleted attribute succesfully";
	 }else{
		  newtext += "Couldn't delete attribute. \n";
		  newtext += ("Description: " + description);
	 }
	 RequestResults.text = newtext;

}

function onReplaceAttributeComplete(request, success, description){
	 var newtext = "";
	 if (success){
		  newtext += "Replaced attribute succesfully";
	 }else{
		  newtext += "Couldn't replace attribute. \n";
		  newtext += ("Description: " + description);
	 }
	 RequestResults.text = newtext;


}

function on_AttributeInvoke_clicked(){
	 if (LDAPConnection){
		  var DN = EditAttributeDN.getText();
		  if (EditAttributeType.getText() == "Add"){
			   printsys("Edit Type: Add");
			   editAttributeRequest 
					= new LDAPConnection.AddAttributeRequest(DN, 
															 editAttributeCollection);
			   editAttributeRequest.OnAddAttributeComplete = onAddAttributeComplete;
			   
		  }else if (EditAttributeType.getText() == "Delete"){
			   printsys("Edit Type: Delete");
			   editAttributeRequest 
					= new LDAPConnection.DeleteAttributeRequest(DN, 
																editAttributeCollection.Keys);
			   editAttributeRequest.OnDeleteAttributeComplete = onDeleteAttributeComplete;
		  }else{		  
			   printsys("Edit Type: Replace");
			   editAttributeRequest 
					= new LDAPConnection.ReplaceAttributeRequest(DN, 
																 editAttributeCollection);
			   editAttributeRequest.OnReplaceAttributeComplete = onReplaceAttributeComplete;
		  }

		  if (editAttributeRequest){
			   editAttributeRequest.Invoke();
		  }
	 } else {

	 }
}



redraw_clist(AddEntryAttributes, addEntryCollection);
