#include ./$(DEPTH)/config/autoconf.mk

##### standard includes. anything used by all Makefile.am's should be here

SASHINCLUDES = \
	-Wall \
	-DG_LOG_DOMAIN=\"$(PACKAGE)\" \
	-DGNOMELOCALEDIR=\""$(datadir)/locale"\" \
	-DTRACING \
	-DOSTYPE=\"$(uname -s)$(uname -r)\" \
	-DOJI \
	-DXP_UNIX \
	-DXP_UNIX \
	-DMOZILLA_CLIENT \
	-DIBMBIDI \
	-fexceptions \
	-fno-rtti \
	-fPIC \
	-g \
	-I$(SASHXB_INCLUDE_DIR) \
	-I$(top_srcdir)/include \
	$(GNOME_INCLUDEDIR) \
	-I$(MOZILLA_INCLUDE_DIR)/string \
	-I$(MOZILLA_INCLUDE_DIR)/browser \
	-I$(MOZILLA_INCLUDE_DIR)/js \
	-I$(MOZILLA_INCLUDE_DIR)/necko \
	-I$(MOZILLA_INCLUDE_DIR)/xpcom \
	-I$(MOZILLA_INCLUDE_DIR)/layout \
	-I$(MOZILLA_INCLUDE_DIR)/xpconnect \
	-I$(MOZILLA_INCLUDE_DIR)/embed_base \
	-I$(MOZILLA_INCLUDE_DIR)/widget \
	-I$(MOZILLA_INCLUDE_DIR)/dom \
	-I$(MOZILLA_INCLUDE_DIR)/gfx \
	-I$(MOZILLA_INCLUDE_DIR)/nspr \
	-I$(MOZILLA_INCLUDE_DIR)/gtkembedmoz \
	-I$(MOZILLA_INCLUDE_DIR)/docshell \
	-I$(MOZILLA_INCLUDE_DIR)/pref \
	-I$(MOZILLA_INCLUDE_DIR)/caps \
	-I$(MOZILLA_INCLUDE_DIR)/webbrwsr \
	-I$(MOZILLA_INCLUDE_DIR)/content \
	-I/usr/include/nspr \
	`gnome-config libglade --cflags` \
	-D_REENTRANT # for gthreads

MOZILLA_LIBS = \
	-L$(MOZILLA_LIB_DIR) \
	-L$(MOZILLA_LIB_DIR)/components \
	-lgkgfx \
	-lgtkembedmoz \
	-lgtksuperwin \
	-lgtkxtbin \
	-ljsj \
	-lmozjs \
	-lnspr4 \
	-lplc4 \
	-lplds4 \
	-lxpcom \
	-lxpistub

######### XPIDL macros. used by extensions, or anything that uses XPCOM ####

IDL = $(MOZILLA_UTIL_BIN_DIR)/xpidl
XPTLINK = $(MOZILLA_UTIL_BIN_DIR)/xpt_link

# you can replace IDLFLAGS or redefine it below if you need
# more idl include dirs.
IDLFLAGS = \
	-w \
	-I $(MOZILLA_IDL_DIR) \
	-I $(SASHXB_IDL_DIR) \
	-I $(top_srcdir)/include \
	-I .

%.h: %.idl
	$(IDL) $(IDLFLAGS) -m header -o `basename $@ .h` $<

%.xpt: %.idl
	$(IDL) $(IDLFLAGS) -m typelib -o `basename $@ .xpt` $<

%.html: %.idl
	$(IDL) $(IDLFLAGS) -m doc -o `basename $@ .html` $<

componentsdir = $(pkglibdir)/components

idldir = $(datadir)/idl/$(PACKAGE)

includedir = $(prefix)/include
pkgincludedir = $(includedir)/$(PACKAGE)

IDL_FILES =

skeldir = $(pkgdatadir)/sash-skel

defaultextsdir = $(pkgdatadir)/default-exts

