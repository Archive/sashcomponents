
mozinclude=$1
target=$2

test -d $2/include || mkdir $2/include

for f in $1/*.h
	do ln -sf $f $2/include
done


if [ -d $1/obsolete ] 
then

if [ ! -d $2/include/obsolete ]
then mkdir $2/include/obsolete
fi

for f in $1/obsolete/*.h
	do ln -sf $f $2/include/obsolete
done

fi

