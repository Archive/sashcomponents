
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _PANELAPP_H
#define _PANELAPP_H

#include <nsICategoryManager.h>

#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIPanelApp.h"
#include "sashIJSEmbed.h"
#include "sashIGtkBrowser.h"
#include "sashIGtkObjectFactory.h"

//56b54830-84ad-40ac-9531-b50547d34353
#define PANELAPP_CID {0x56b54830, 0x84ad, 0x40ac, {0x95, 0x31, 0xb5, 0x05, 0x47, 0xd3, 0x43, 0x53}}
NS_DEFINE_CID(kPanelAppCID, PANELAPP_CID);

#define PANELAPP_CONTRACT_ID "@gnome.org/SashXB/PanelApp;1"

typedef struct _GtkWidget GtkWidget;
class SashGtkBrowser;

class PanelApp : public sashIPanelApp, public sashILocation
{
public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIEXTENSION;
  NS_DECL_SASHILOCATION;
  NS_DECL_SASHIPANELAPP;

  PanelApp();
  virtual ~PanelApp();
  /* additional members */

private:
  GtkWidget *m_applet;
  sashIGtkBrowser *m_brows;
  sashIActionRuntime * m_act;
  string m_url, m_Tooltip, m_OnSizeChange, m_OnOrientationChange;
  int m_length, m_thickness;
  PanelOrientType m_Orientation;
  bool m_gtkpanel;
  string m_jsfile;
  sashIJSEmbed * m_js;

  string ParseMenus(const char* name);
  NS_IMETHODIMP InitGTK(sashIActionRuntime * act, JSContext * cx, 
						JSObject * obj, const char * webGuid);
	
  void Resize();

  static void ResizeApp(AppletWidget* aw, int height, void* gp);
  static void ReorientApp(GtkWidget* aw, PanelOrientType o, gpointer gp);
  static void Callback(AppletWidget* w, gpointer data);

  string FixURL(const string & url);

  nsCOMPtr<sashIExtension> m_gtk_ext;
  nsCOMPtr<sashIGtkObjectFactory> m_fact;

  JSContext * GetJSContext();
};

#endif
