
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):

*****************************************************************/

#include <gnome.h>
#include <string>
#include <applet-widget.h>
#include "sash_constants.h"

#include <jsapi.h>
#include "url_tools.h"
#include "PanelApp.h"
#include "extensiontools.h"
#include "debugmsg.h"
#include "sashIActionRuntime.h"
#include "sashVariantUtils.h"
#include "GtkObjectFactory.h"
#include "sashGtkExtension.h"
#include "sashGtkWidget.h"
#include "sashIXPXMLDocument.h"
#include "sashIXPXMLNode.h"
#include "sash_error.h"

struct AppInfo { PanelApp* p; string func; };

SASH_LOCATION_IMPL(PanelApp, sashIPanelApp, 
				   PANELAPP, "PanelApp", "a simple panel location");
char* v[] = {"sash-runtime"};

PanelApp::PanelApp() : m_applet(NULL)
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(panelapp, "Creating panelapp location\n");
  m_gtkpanel = false;
  m_brows = NULL;
  m_js = NULL;
  m_gtk_ext = NULL;
  m_fact = NULL;
}

PanelApp::~PanelApp()
{
  DEBUGMSG(panelapp, "calling destructor\n");
}


NS_IMETHODIMP 
PanelApp::Cleanup(){
	 return NS_OK;
}

JSContext * PanelApp::GetJSContext() {
  JSContext * cx = NULL;

  if (m_gtkpanel) {
	assert(m_js);
	m_js->GetScriptContext(&cx);
  }
  else {
	assert(m_brows);
	nsIScriptContext* c = NULL;
	if (NS_SUCCEEDED(m_brows->GetScriptContext(&c)))
	  cx = (JSContext *) c->GetNativeContext();
  }

  return cx;
}

NS_IMETHODIMP PanelApp::GetPanelWidget(sashIGtkWidget ** widget) {
  if (m_gtkpanel) {
	assert(m_fact);
	return m_fact->GetXPObject(GTK_OBJECT(m_applet), 
							   (sashIGtkObject **) widget);
  } else {
	*widget = NULL;
	return NS_OK;
  }
}

NS_IMETHODIMP PanelApp::InitGTK(sashIActionRuntime * act, JSContext * cx, 
								JSObject * obj, const char * webGuid) 
{
  nsCID cid = SASHGTKEXTENSION_CID;
  nsIID iid = SASHIGTKEXTENSION_IID;

  nsresult rv = 
		nsComponentManager::CreateInstance(cid, nsnull, iid,
										   (void **) getter_AddRefs(m_gtk_ext));
  assert(rv == NS_OK);	

  m_gtk_ext->Initialize(act, "", webGuid, cx, obj);

  nsCOMPtr<sashIGtkExtension> gtk_ext(do_QueryInterface(m_gtk_ext));
  rv = gtk_ext->GetFactory(getter_AddRefs(m_fact));

  return rv;
}

NS_IMETHODIMP 
PanelApp::Initialize(sashIActionRuntime * act,
		   const char *registrationXML, const char *webGuid,
		   JSContext *cx, JSObject *obj)
{
  DEBUGMSG(panelapp, "Starting initialization with context %p...\n", cx);
  m_act = act;

  DEBUGMSG(panelapp, "initializing with registration:\n %s\n", registrationXML);

  nsCOMPtr<sashIXPXMLDocument> xml = do_CreateInstance((nsCID)XPXMLDOCUMENT_CID);
  PRBool res;
  xml->LoadFromString(registrationXML, &res);
  if (registrationXML && res) {
	   nsCOMPtr<sashIXPXMLNode> root;
	   xml->GetRootNode(getter_AddRefs(root));
	   if (root) {
			nsCOMPtr<sashIXPXMLNode> htmlNode, lengthNode;

			root->GetFirstChildNode("htmlfile", getter_AddRefs(htmlNode));
			if (htmlNode)
				 XP_GET_STRING(htmlNode->GetText, m_url);

			if (m_url == "") {
				 // Try getting a JS file
				 nsCOMPtr<sashIXPXMLNode> jsNode;
				 root->GetFirstChildNode("jsfile", getter_AddRefs(jsNode));
				 if (jsNode) {
					  XP_GET_STRING(htmlNode->GetText, m_url);
					  NewSashJSEmbed(&m_js);
					  DEBUGMSG(panelapp, "Switching to gtk panel\n");
					  m_js->StartJSInterpreter();
					  m_gtkpanel = true;
					  nsresult rv = InitGTK(act, cx, obj, webGuid);
					  assert(NS_SUCCEEDED(rv));
				 }
				 else
					  OutputError("PanelApp weblications must specify either an HTML file or a JS file");
			} 
			else {
				 NewSashGtkBrowser(&m_brows, act);
				 m_brows->SetEnableSashContext(true);
			}

			root->GetFirstChildNode("length", getter_AddRefs(lengthNode));
			string length_str;
			if (lengthNode) {
				 XP_GET_STRING(lengthNode->GetText, length_str);
				 m_length = atoi(length_str.c_str());
			}

	   }
  }
  return NS_OK;
}

/* void run (in string url); */
NS_IMETHODIMP 
PanelApp::Run()
{
  DEBUGMSG(panelapp, "Running\n");
  GtkWidget* h = gtk_handle_box_new();
  gtk_widget_show(h);
//  applet_widget_init("browserApplet","0.1",1,v,NULL,0,NULL);
  m_applet = applet_widget_new("browserApplet");
  if (!m_applet) OutputError("Could not intialize applet!");
  gtk_widget_realize(m_applet);

  GtkWidget * b_widget = NULL;

  if (!m_gtkpanel) {
	m_brows->LoadURL(FixURL(m_url).c_str());

	m_brows->GetWidget(&b_widget);

	gtk_widget_show(b_widget);
  }

  gtk_signal_connect(GTK_OBJECT(m_applet),
					 "delete_event",
					 GTK_SIGNAL_FUNC(gtk_main_quit),
					 NULL);
  gtk_signal_connect(GTK_OBJECT(m_applet),"change_orient",
					 GTK_SIGNAL_FUNC(PanelApp::ReorientApp),
					 this);
#ifdef HAVE_PANEL_PIXEL_SIZE
  gtk_signal_connect(GTK_OBJECT(m_applet),
					 "change_pixel_size",
					 GTK_SIGNAL_FUNC(PanelApp::ResizeApp),
					 this);
#endif

  if (!m_gtkpanel)
	gtk_container_add(GTK_CONTAINER(h), b_widget);

  applet_widget_add(APPLET_WIDGET(m_applet), h);
  m_thickness = applet_widget_get_panel_pixel_size(APPLET_WIDGET(m_applet));

  gtk_widget_show_all(m_applet);

  if (m_gtkpanel) {
	/* Evaluate the Javascript file */
	DEBUGMSG(panelapp, "Evaling javascript file %s\n", m_jsfile.c_str());
	if (m_js->EvalScriptFile(m_jsfile.c_str()) != NS_OK) {
	  DEBUGMSG(panelapp, "Error evaling javascript file\n");
	  return NS_COMFALSE;
	}
  }

  DEBUGMSG(panel, "About to call applet_widget_gtk_main\n");
  applet_widget_gtk_main();
  return NS_OK;
}

void PanelApp::ReorientApp(GtkWidget* aw, PanelOrientType o, gpointer gp) {
	 PanelApp* pa = (PanelApp*) gp;
	 DEBUGMSG(panelapp, "Resizing button for new panel orientation %d...\n", 
			  o);

	 pa->m_Orientation = o;
	 pa->Resize();
	 // call the user's custom callback, if there is one
	 if (pa->m_OnOrientationChange != "") {
		  int out = (pa->m_Orientation == ORIENT_UP ? 0 : 
					 pa->m_Orientation == ORIENT_DOWN ? 1 : 
					 pa->m_Orientation == ORIENT_LEFT ? 2 : 3);
		  
		  JSContext * j = pa->GetJSContext();
		  if (j == NULL) return;
		  vector<nsIVariant*> v;
		  nsIVariant* v1;
		  if (NewVariant(&v1) != NS_OK) return;
		  VariantSetNumber(v1, out);
		  v.push_back(v1);
		  DEBUGMSG(panelapp, "About to call user reorient function\n");
		  CallEvent(pa->m_act, j, pa->m_OnOrientationChange, v);
	 }	   
	 
}

void PanelApp::Resize() {
  if (m_gtkpanel)
	return;
  DEBUGMSG(panelapp, "Resizing app to %d by %d...\n", 
	   m_length, m_thickness);

  assert(m_brows);

  GtkWidget * browser;
  m_brows->GetWidget(&browser);
  switch (m_Orientation) {
  case ORIENT_UP:
  case ORIENT_DOWN:
	   gtk_widget_set_usize(GTK_WIDGET(browser), m_length, m_thickness);
	   break;
  case ORIENT_LEFT:
  case ORIENT_RIGHT:
	   gtk_widget_set_usize(GTK_WIDGET(browser), m_thickness, m_length);
	   break;
  default: break;
	   
  }
  gtk_widget_show_all(m_applet);

}

void PanelApp::ResizeApp(AppletWidget* aw, int thickness, void* gp) {
  DEBUGMSG(panelapp, "Resizing button for new panel size %d...\n", 
	   thickness);
  PanelApp* pa = (PanelApp*) gp;
  pa->m_thickness = thickness;
  pa->Resize();

  // call the user's custom callback, if there is one
  if (pa->m_OnSizeChange != "") {

	JSContext * j = pa->GetJSContext();
	if (j == NULL) return;
	vector<nsIVariant*> v;
	nsIVariant* v1;
	if (NewVariant(&v1) != NS_OK) return;
	VariantSetNumber(v1, thickness);
	v.push_back(v1);
	DEBUGMSG(panelapp, "About to call user resize function\n");
	CallEvent(pa->m_act, j, pa->m_OnSizeChange, v);
  }	   
}

/*
  If the argument passed is just a filename, make it absolute 
  and add a protocol.
*/
string PanelApp::FixURL(const string & url) {
  // return anything with a protocol
  if (URLHasProtocol(url) && url.length() == 0) {
    return url;
  }
  
  string ret;

  // Is it relative?
  if (url[0] != '/') {
    char *dir;
    m_act->GetDataDirectory(&dir);
    ret = ((string("file://") + dir) + "/") + url;
	nsMemory::Free(dir);
  }
  else
    ret = "file://" + url;
  DEBUGMSG(panelapp, "Fixed url %s to be %s\n", url.c_str(), ret.c_str());
  return ret;
}

NS_IMETHODIMP
PanelApp::ProcessEvent()
{
  return NS_OK;
}


/* readonly attribute string About; */
NS_IMETHODIMP PanelApp::GetAboutIcon(char * *aAbout)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_ABOUT, aAbout);
	 return NS_OK;
}

/* readonly attribute string New; */
NS_IMETHODIMP PanelApp::GetNewIcon(char * *aNew)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_NEW, aNew);
	 return NS_OK;
}

/* readonly attribute string Save; */
NS_IMETHODIMP PanelApp::GetSaveIcon(char * *aSave)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_SAVE, aSave);
	 return NS_OK;
}

/* readonly attribute string SaveAs; */
NS_IMETHODIMP PanelApp::GetSaveAsIcon(char * *aSaveAs)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_SAVE_AS, aSaveAs);
	 return NS_OK;
}

/* readonly attribute string Open; */
NS_IMETHODIMP PanelApp::GetOpenIcon(char * *aOpen)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_OPEN, aOpen);
	 return NS_OK;
}

/* readonly attribute string Close; */
NS_IMETHODIMP PanelApp::GetCloseIcon(char * *aClose)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_CLOSE, aClose);
	 return NS_OK;
}

/* readonly attribute string Quit; */
NS_IMETHODIMP PanelApp::GetQuitIcon(char * *aQuit)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_QUIT, aQuit);
	 return NS_OK;
}

/* readonly attribute string Cut; */
NS_IMETHODIMP PanelApp::GetCutIcon(char * *aCut)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_CUT, aCut);
	 return NS_OK;
}

/* readonly attribute string Copy; */
NS_IMETHODIMP PanelApp::GetCopyIcon(char * *aCopy)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_COPY, aCopy);
	 return NS_OK;
}

/* readonly attribute string Paste; */
NS_IMETHODIMP PanelApp::GetPasteIcon(char * *aPaste)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_PASTE, aPaste);
	 return NS_OK;
}

/* readonly attribute string Properties; */
NS_IMETHODIMP PanelApp::GetPropertiesIcon(char * *aProperties)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_PROP, aProperties);
	 return NS_OK;
}

/* readonly attribute string Preferences; */
NS_IMETHODIMP PanelApp::GetPreferencesIcon(char * *aPreferences)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_PREF, aPreferences);
	 return NS_OK;
}

/* readonly attribute string Stop; */
NS_IMETHODIMP PanelApp::GetStopIcon(char * *aStop)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_STOP, aStop);
	 return NS_OK;
}

/* readonly attribute string Refresh; */
NS_IMETHODIMP PanelApp::GetRefreshIcon(char * *aRefresh)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_REFRESH, aRefresh);
	 return NS_OK;
}

/* readonly attribute string Back; */
NS_IMETHODIMP PanelApp::GetBackIcon(char * *aBack)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_BACK, aBack);
	 return NS_OK;
}

/* readonly attribute string Forward; */
NS_IMETHODIMP PanelApp::GetForwardIcon(char * *aForward)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_FORWARD, aForward);
	 return NS_OK;
}

/* readonly attribute string Home; */
NS_IMETHODIMP PanelApp::GetHomeIcon(char * *aHome)
{
	 XP_COPY_STRING(GNOME_STOCK_MENU_HOME, aHome);
	 return NS_OK;
}

/* void SetTooltip (in string tt); */
NS_IMETHODIMP PanelApp::GetTooltip(char **tt)
{
	 XP_COPY_STRING(m_Tooltip.c_str(), tt);
	 return NS_OK;

}

/* void SetTooltip (in string tt); */
NS_IMETHODIMP PanelApp::SetTooltip(const char *tt)
{
	 m_Tooltip = tt;
	 DEBUGMSG(panelapp, "Setting tooltip to %s\n", tt);
	 applet_widget_set_tooltip(APPLET_WIDGET(m_applet), tt);
	 GtkWidget* b_widget;
	 m_brows->GetWidget(&b_widget);
	 applet_widget_set_widget_tooltip(APPLET_WIDGET(m_applet), b_widget, tt);
	 return NS_OK;
}

string PanelApp::ParseMenus(const char* name) {
	 string s = name;
	 unsigned int pos = 0, n = 0;
	 if (s[0] == '/') pos++;
	 while ((n = s.find("/", pos)) != string::npos) {
		  string t = s.substr(0, n);
		  string u = s.substr(pos, n-pos);
		  applet_widget_register_callback_dir(APPLET_WIDGET(m_applet), 
											  t.c_str(), 
											  _(u.c_str()));
		  pos = n + 1;
	 }
	 return (pos < s.length() ? s.substr(pos) : "");
}

/* void AddMenuItem (in string name, in string function); */
NS_IMETHODIMP PanelApp::AddMenuItem(const char *name, const char *function)
{
	 AppInfo* a = new AppInfo;
	 a->p = this;
	 a->func = function;
	 string blazaa = ParseMenus(name);
	 applet_widget_register_callback(APPLET_WIDGET(m_applet), 
									 strdup(name), 
									 _(strdup(blazaa.c_str())), 
									 PanelApp::Callback, 
									 (void*) a);
	 return NS_OK;
}

/* void AddStockMenuItem (in string name, in string stock_name, in string function); */
NS_IMETHODIMP PanelApp::AddStockMenuItem(const char *name, const char *stock_name, const char *function)
{
	 AppInfo* a = new AppInfo;
	 a->p = this;
	 a->func = function;
	 string blazaa = ParseMenus(name);
	 applet_widget_register_stock_callback(APPLET_WIDGET(m_applet), 
										   strdup(name), 
										   strdup(stock_name),
										   _(strdup(blazaa.c_str())), 
										   PanelApp::Callback, 
										   (void*) a);
	 return NS_OK;
}

/* void RemoveMenuItem (in string name); */
NS_IMETHODIMP PanelApp::RemoveMenuItem(const char *name)
{
	 applet_widget_unregister_callback(APPLET_WIDGET(m_applet), name);
	 return NS_OK;
}

/* void OnSizeChange (in string function); */
NS_IMETHODIMP PanelApp::SetOnSizeChange(const char *function) {
	 m_OnSizeChange = function;
	 return NS_OK;
}

NS_IMETHODIMP PanelApp::GetOnSizeChange(char **function) {
	 XP_COPY_STRING(m_OnSizeChange.c_str(), function);
	 return NS_OK;
}


NS_IMETHODIMP PanelApp::SetOnOrientationChange(const char *function) {
	 m_OnOrientationChange = function;
	 return NS_OK;
}

NS_IMETHODIMP PanelApp::GetOnOrientationChange(char **function) {
   XP_COPY_STRING(m_OnOrientationChange.c_str(), function);
	 return NS_OK;
}


/* attribute PRInt16 width; */
NS_IMETHODIMP PanelApp::GetSize(PRInt16 *aWidth)
{
	 *aWidth = m_thickness;
	 return NS_OK;
}

/* attribute PRInt16 width; */
NS_IMETHODIMP PanelApp::GetLength(PRInt16 *aWidth)
{
	 *aWidth = m_length;
	 return NS_OK;
}

NS_IMETHODIMP PanelApp::SetLength(PRInt16 aWidth)
{
	 int space = applet_widget_get_free_space(APPLET_WIDGET(m_applet));
  m_length = (aWidth > space ? space : aWidth);

  if (!m_gtkpanel) {
	   Resize();
  }

  return NS_OK;
}

/* void QuitApp (); */
NS_IMETHODIMP PanelApp::Quit() {
	 DEBUGMSG(panelapp, "Shutting down...\n");
	 m_act->Cleanup();
  applet_widget_remove(APPLET_WIDGET(m_applet));
  applet_widget_gtk_main_quit();

	 return NS_OK;
}

void PanelApp::Callback(AppletWidget* w, gpointer data) {
	 AppInfo* a = (AppInfo*) data;
	 DEBUGMSG(panelapp, "Callback for %s\n", a->func.c_str());
	 
	 JSContext * cx = a->p->GetJSContext();
	 if (cx == NULL) return; // no context; sorry
	 vector<nsIVariant*> v;
	 CallEvent(a->p->m_act, cx, a->func, v);
	 DEBUGMSG(panelapp, "Finished callback for %s\n", a->func.c_str());
}
