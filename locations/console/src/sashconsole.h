
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHCONSOLE_H
#define SASHCONSOLE_H

#include "sashIConsole.h"
#include "sashILocation.h"
#include "sashIJSEmbed.h"
#include <string>

//BD4E07E6-FEDF-4249-9C3E-FEB528566608
#define SASHCONSOLE_CID {0xBD4E07E6, 0xFEDF, 0x4249, {0x9C, 0x3E, 0xFE, 0xB5, 0x28, 0x56, 0x66, 0x08}}

#define SASHCONSOLE_CONTRACT_ID "@gnome.org/SashXB/SashConsole;1"

NS_DEFINE_CID(kSashConsoleCID, SASHCONSOLE_CID);

class SashConsole : public sashIConsole,
					public sashILocation
{
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHICONSOLE;
  NS_DECL_SASHIEXTENSION;
  NS_DECL_SASHILOCATION;
  
  SashConsole();
  virtual ~SashConsole();

 private:
  sashIActionRuntime *m_act;
  string m_jsfile;
  sashIJSEmbed * m_js;
};


#endif // SASHCONSOLE_H
