/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham, John Corwin

SashConsole implementation file.

*****************************************************************/

#include <jsapi.h>
#include "sashconsole.h"
#include "debugmsg.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sash_constants.h"
#include "sashIXPXMLDocument.h"
#include "sashIXPXMLNode.h"
#include <unistd.h>
#include "sashVariantUtils.h"

SASH_LOCATION_IMPL(SashConsole, sashIConsole, 
				   SASHCONSOLE, "Console", "Console location");

SashConsole::SashConsole() 
{
  NS_INIT_ISUPPORTS();
  NewSashJSEmbed(&m_js);
  m_js->StartJSInterpreter();

  DEBUGMSG(sashconsole, "Got object factory\n");
}

SashConsole::~SashConsole()
{

}

NS_IMETHODIMP 
SashConsole::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP
SashConsole::Initialize(sashIActionRuntime *act,
		     const char *xml,
		     const char *instanceGuid, 
		     JSContext *context, JSObject *object)
{
  DEBUGMSG(sashconsole, "Initializing. xml = %s\n", xml);

  string xmlfile;

  // SashConsole will use a raw javascript interpreter, not an entire mozilla
  m_act = act;
  DEBUGMSG(sashconsole, "About to call createsashobjectnative\n");
  
  JSContext * jsContext;
  m_js->GetScriptContext(&jsContext);
  m_act->CreateSashObjectNative(jsContext);
  DEBUGMSG(sashconsole, "done calling createsashobjectnative\n");

  nsCOMPtr<sashIXPXMLDocument> consoleXML = do_CreateInstance((nsCID)XPXMLDOCUMENT_CID);
  if (!consoleXML)
	   OutputError("Unable to create the XML parser\n");
  PRBool res;
  consoleXML->LoadFromString(xml, &res);
  if (!res)
	   OutputError("Unable to parse XML registration\n");

  nsCOMPtr<sashIXPXMLNode> root;
  consoleXML->GetRootNode(getter_AddRefs(root));
  if (!root)
	   OutputError("Unable to parse XML registration\n");

  nsCOMPtr<sashIXPXMLNode> jsfileNode;
  root->GetFirstChildNode("jsfile", getter_AddRefs(jsfileNode));
  if (jsfileNode) {
	   XP_GET_STRING(jsfileNode->GetText, m_jsfile);

	   DEBUGMSG(sashconsole, "Opening jsfile %s\n", m_jsfile.c_str());
	   if (m_jsfile != "")
			return NS_OK;
  }
  DEBUGMSG(sashconsole, "jsfile not specified\n");
  return NS_COMFALSE;
}

NS_IMETHODIMP
SashConsole::Run()
{
  DEBUGMSG(sashconsole, "Running\n");

  /* Evaluate the Javascript file */
  DEBUGMSG(sashconsole, "Evaling javascript file %s\n", m_jsfile.c_str());
  if (m_js->EvalScriptFile(m_jsfile.c_str()) != NS_OK) {
    DEBUGMSG(sashconsole, "Error evaling javascript file\n");
    return NS_COMFALSE;
  }
  DEBUGMSG(sashconsole, "Hitting event loop\n");
  
  m_act->EventLoop();
  return NS_OK;
}

NS_IMETHODIMP
SashConsole::ProcessEvent()
{
  return NS_OK;
}

NS_IMETHODIMP
SashConsole::Quit()
{
	 DEBUGMSG(sashconsole, "Quitting console\n");
	 m_act->Cleanup();
	 _exit(0);
	 DEBUGMSG(sashconsole, "This should never be called\n");
}

NS_IMETHODIMP SashConsole::Readline(char ** ret) {
  char buf[1024];

  fgets(buf, sizeof(buf), stdin);
  XP_COPY_STRING(buf, ret);
  return NS_OK;
}
