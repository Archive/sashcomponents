#!/usr/bin/perl -w
my $pass = "", %arg = ( -m => undef, -c => undef, -i => undef );
my @bads;

foreach(@ARGV) {
  if ($_ eq "--help") {
	print "$0: [-m] [-i] [-c] [additional autogen.sh flags]\nMakes and installs locations.\n\t-m: make only\n\t-i: install only.\n\t-c: make clean\nDefault is to make and install.\n";
	exit;
  } elsif (exists $arg{$_}) {
	$arg{$_} = 1;
  } else {
	$pass .= $_ . " ";
  }
}
$arg{-m} = $arg{-i} = 1 unless scalar grep {$_} values %arg;

opendir("in", ".");
foreach(grep {-d && ! /\./} readdir("in")) {
  chdir($_);
  unless (-e "autogen.sh") {
	chdir("..");
	next;
  }
  if ($arg{"-c"}) {
	system("make clean") if (-e "Makefile");
  } elsif ($arg{"-m"}) {
	my $td;
	($td = system("./autogen.sh $pass")) if ($pass ne "" || ! -e "Makefile");
	($td = system("make".($arg{-i} ? " install" : ""))) unless $td;
	push @bads, $_ if $td;
  } else {
	# install only
	if (chdir("src")) {
	  my (@names) = <*.wdf>;
	  foreach (@names) {
		print "sash-install $_ -f\n";
		  if (system("sash-install $_ -f")) {
			push @bads, $_;
		  }
	  }
	  chdir("..");
	}
  }
  chdir("..");
}

closedir("in");

print "\n***************************************************\n";
if (scalar @bads) {
  print "The following components failed to make or install:\n";
  foreach(@bads) {print "\t",$_,"\n";}
} else {
  print "All locations completed successfully!\n"
}
print "***************************************************\n";
