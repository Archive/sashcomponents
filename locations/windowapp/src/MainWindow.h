
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H

#include "sashIWinApp.h"
#include "sashIGtkBrowser.h"
#include "extensiontools.h"

#define MAINWINDOW_CID {0x9c820fe0, 0x61eb, 0x4703, {0x8f, 0x5c, \
                       0xbf, 0x53, 0xcc, 0x5c, 0x12, 0x48}}

NS_DEFINE_CID(kSashMainWindowCID, MAINWINDOW_CID);

#define MAINWINDOW_CONTRACT_ID "@gnome.org/SashXB/WinApp/MainWindow;1"

typedef struct _GtkWidget GtkWidget;
class SashGtkBrowser;

class MainWindow : public sashIMainWindow
{
public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIMAINWINDOW;

  MainWindow();
  virtual ~MainWindow();
  /* additional members */

  string FixURL(const string & url);
  bool onClose();
  void onFocusChange(bool in);
  void onResizeMove(int width, int height, int x, int y) {
	m_actual_width = width;
	m_actual_height = height;
	m_x = x;
	m_y = y;
  }
	
private:
  string m_onActivate;
  string m_onClose;
  string m_onMaximize;
  string m_onMinimize;
  string m_onRestore;
  string m_onSaveModified;
  string m_onSetForeGround;

  bool m_alwaysOnTop;
  bool m_isVisible;
  bool m_resizable;
  bool m_statusBarVisible;

  int m_actual_width, m_actual_height;
  int m_uWidth, m_uHeight;
  int m_x, m_y;

  GtkWidget * m_app;
  GtkWidget * m_menuBar;
  GtkWidget * m_browserWidget;
  GtkWidget * m_status;
  sashIGtkBrowser * m_brows;
  sashIActionRuntime * m_act;

  nsIScriptContext * m_cx;
  sashIWinApp * m_winApp;
};

#endif
