
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142

    USA

*****************************************************************

Contributor(s): John Corwin

module for winapp
******************************************************************/
#include "extensiontools.h"
#include "WinApp.h"
#include "MainWindow.h"

NS_DECL_CLASSINFO(WinApp);
NS_DECL_CLASSINFO(MainWindow);
NS_GENERIC_FACTORY_CONSTRUCTOR(WinApp);
NS_GENERIC_FACTORY_CONSTRUCTOR(MainWindow);

static nsModuleComponentInfo components[] = {
  MODULE_COMPONENT_ENTRY(WinApp, WINAPP, "WinApp Location"),
  MODULE_COMPONENT_ENTRY(MainWindow, MAINWINDOW, "MainWindow object")
};

NS_IMPL_NSGETMODULE(WinApp, components);
