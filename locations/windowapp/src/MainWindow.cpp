/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#include <gnome.h>
#include <string>
#include <gtkmozembed.h>

#include <stdlib.h>

#include "MainWindow.h"
#include "extensiontools.h"
#include "url_tools.h"
#include "sash_constants.h"
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS1_CI(MainWindow, sashIMainWindow);

MainWindow::MainWindow()
{
  NS_INIT_ISUPPORTS();
  m_uWidth = m_uHeight = 0;
  m_x = m_y = 0;
  m_actual_width = m_actual_height = 0;
  m_cx = NULL;
  DEBUGMSG(mainwindow, "Creating mainwin\n");
}

MainWindow::~MainWindow()
{
  DEBUGMSG(mainwindow, "calling destructor\n");
}

/*
  If the argument passed is just a filename, make it absolute 
  and add a protocol.
*/
string MainWindow::FixURL(const string & url)
{
  // return anything with a protocol
  if (URLHasProtocol(url)) {
    DEBUGMSG(winapp, "Not fixing url %s\n", url.c_str());
    return url;
  }

  // Assume it's a file
  if (url.length() == 0)
    return url;
  
  string ret;

  // Is it relative?
  if (url[0] != '/') {
    char *dir;
    m_act->GetDataDirectory(&dir);
    ret = ((string("file://") + dir) + "/") + url;
	nsMemory::Free(dir);
  }
  else
    ret = "file://" + url;
  DEBUGMSG(winapp, "Fixed url %s to be %s\n", url.c_str(), ret.c_str());
  return ret;
}

gint app_visibility_callback(GtkWidget * widget, GdkEventVisibility * event, void * data) {
  DEBUGMSG(winapp, "visiblity event\n");
  return false;
}

gint app_focus_callback(GtkWidget * widget, GdkEventFocus * event, void * data) {
  MainWindow * mainwin = (MainWindow *) data;

  mainwin->onFocusChange(event->in);
  DEBUGMSG(winapp, "focus %s\n", event->in ? "in" : "out");
  return false;
}

gint app_size_alloc_callback(GtkWidget * widget, GtkAllocation * size, void * data) {
  MainWindow * mainwin = (MainWindow *) data;

  DEBUGMSG(winapp, "Window resized to %d x %d\n", size->width, size->height);
  mainwin->onResizeMove(size->width, size->height, size->x, size->y);
  return false;
}

gint app_close_callback(GtkWidget * widget, GdkEvent * event, void * data) {
  MainWindow * mainwin = (MainWindow *) data;
  bool shouldClose;

  DEBUGMSG(winapp, "Close callback\n");
  shouldClose = mainwin->onClose();
  DEBUGMSG(winapp, "%s closing window\n", shouldClose ? "" : "not");

  if (shouldClose) {
	gtk_main_quit();
	return false;
  }
  else
	return true;
}

void MainWindow::onFocusChange(bool in) {
  nsresult rval = m_brows->GetScriptContext(&m_cx);
  assert(NS_SUCCEEDED(rval));
  // the context may be null (say, during page loads)... so don't invoke the callback
  // if it is!
  if (m_cx && m_onActivate != "") {
	   cout << m_onActivate << endl;
	nsIVariant * v;
	NewVariant(&v);
	VariantSetBoolean(v, in);
	vector<nsIVariant *> args;
	args.push_back(v);
	CallEvent(m_act, m_cx, m_onActivate, args);
  }
}

bool MainWindow::onClose() {
  bool shouldClose = true;
  vector<nsIVariant *> args;

  nsresult rval = m_brows->GetScriptContext(&m_cx);
  assert(NS_SUCCEEDED(rval));
  if (m_onClose != "" && m_cx != NULL) {
	nsCOMPtr<nsIVariant> v;
	
	DEBUGMSG(winapp, "close callback -- calling function %s\n", m_onClose.c_str());
	v = getter_AddRefs(CallEventWithResult(m_act, m_cx, m_onClose, args));
	DEBUGMSG(winapp, "returning from close callback\n");

	shouldClose = VariantGetBoolean(v, true);
  }

  if (m_cx != NULL && shouldClose && m_onSaveModified != "") {
	CallEvent(m_act, m_cx, m_onSaveModified, args);
  }
  return shouldClose;
}

NS_IMETHODIMP MainWindow::Initialize(sashIWinApp * winapp, sashIActionRuntime * act,
									 int width, int height, const char * url)
{
  m_act = act;
  m_winApp = winapp;

  NewSashGtkBrowser(&m_brows, m_act);
  m_brows->SetEnableSashContext(true);

  m_app = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_hide_all(m_app);

  gtk_window_set_default_size(GTK_WINDOW(m_app), width, height);
  gtk_widget_add_events(m_app, GDK_ALL_EVENTS_MASK);

  GtkWidget * vbox;

  vbox = gtk_vbox_new(false, 0);
  gtk_container_add(GTK_CONTAINER(m_app), vbox);

  m_menuBar = gtk_menu_bar_new();

  GtkWidget * m_browserWidget;
  m_brows->GetWidget(&m_browserWidget);

  m_status = gtk_label_new("");

  gtk_box_pack_start(GTK_BOX(vbox), m_menuBar, false, false, 0);
  gtk_box_pack_start(GTK_BOX(vbox), m_browserWidget, true, true, 0);
  gtk_box_pack_start(GTK_BOX(vbox), m_status, false, false, 0);

  m_brows->LoadURL(FixURL(url).c_str());

  // register the main window's callbacks
  gtk_signal_connect(GTK_OBJECT(m_app), "delete-event",
					 GTK_SIGNAL_FUNC(app_close_callback), this);
  gtk_signal_connect(GTK_OBJECT(m_app), "visibility-notify-event",
					 GTK_SIGNAL_FUNC(app_visibility_callback), this);
  gtk_signal_connect(GTK_OBJECT(m_app), "focus-in-event",
					 GTK_SIGNAL_FUNC(app_focus_callback), this);
  gtk_signal_connect(GTK_OBJECT(m_app), "focus-out-event",
					 GTK_SIGNAL_FUNC(app_focus_callback), this);
  gtk_signal_connect(GTK_OBJECT(m_app), "size-allocate",
					 GTK_SIGNAL_FUNC(app_size_alloc_callback), this);

  gtk_widget_show_all(m_app);

  DEBUGMSG(winapp, "Main window created\n");

  return NS_OK;
}

/* readonly attribute string OnActivate; */
NS_IMETHODIMP MainWindow::GetOnActivate(char * *aOnActivate)
{
  XP_COPY_STRING(m_onActivate.c_str(), aOnActivate);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnActivate(const char * aOnActivate)
{
  m_onActivate = aOnActivate;
  return NS_OK;
}

/* readonly attribute string OnClose; */
NS_IMETHODIMP MainWindow::GetOnClose(char * *aOnClose)
{
  XP_COPY_STRING(m_onClose.c_str(), aOnClose);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnClose(const char * aOnClose)
{
  m_onClose = aOnClose;
  return NS_OK;
}

/* readonly attribute string OnMaximize; */
NS_IMETHODIMP MainWindow::GetOnMaximize(char * *aOnMaximize)
{
  XP_COPY_STRING(m_onMaximize.c_str(), aOnMaximize);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnMaximize(const char * aOnMaximize)
{
  m_onMaximize = aOnMaximize;
  return NS_OK;
}

/* readonly attribute string OnMinimize; */
NS_IMETHODIMP MainWindow::GetOnMinimize(char * *aOnMinimize)
{
  XP_COPY_STRING(m_onMinimize.c_str(), aOnMinimize);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnMinimize(const char * aOnMinimize)
{
  m_onMinimize = aOnMinimize;
  return NS_OK;
}

/* readonly attribute string OnRestore; */
NS_IMETHODIMP MainWindow::GetOnRestore(char * *aOnRestore)
{
  XP_COPY_STRING(m_onRestore.c_str(), aOnRestore);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnRestore(const char * aOnRestore)
{
  m_onRestore = aOnRestore;
  return NS_OK;
}

/* readonly attribute string OnSaveModified; */
NS_IMETHODIMP MainWindow::GetOnSaveModified(char * *aOnSaveModified)
{
  XP_COPY_STRING(m_onSaveModified.c_str(), aOnSaveModified);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnSaveModified(const char * aOnSaveModified)
{
  m_onSaveModified = aOnSaveModified;
  return NS_OK;
}

/* readonly attribute string OnSetForeGround; */
NS_IMETHODIMP MainWindow::GetOnSetForeGround(char * *aOnSetForeGround)
{
  XP_COPY_STRING(m_onSetForeGround.c_str(), aOnSetForeGround);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetOnSetForeGround(const char * aOnSetForeGround)
{
  m_onSetForeGround = aOnSetForeGround;
  return NS_OK;
}

/* void Close (); */
NS_IMETHODIMP MainWindow::Close()
{
  m_act->Cleanup();

  _exit(0);

  // should never get to this point
  return NS_ERROR_FAILURE;
}

/* void SetForegroundWindow (); */
NS_IMETHODIMP MainWindow::SetForegroundWindow()
{
  gdk_window_raise(m_app->window);
  return NS_OK;
}

/* void SetIcon (in string location); */
NS_IMETHODIMP MainWindow::SetIcon(const char *location)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRBool AlwaysOnTop; */
NS_IMETHODIMP MainWindow::GetAlwaysOnTop(PRBool *aAlwaysOnTop)
{
  *aAlwaysOnTop = m_alwaysOnTop;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetAlwaysOnTop(PRBool aAlwaysOnTop)
{
  m_alwaysOnTop = aAlwaysOnTop;

  // make the window always on top
  return NS_OK;
}

/* attribute PRBool CaptionVisible; */
NS_IMETHODIMP MainWindow::GetCaptionVisible(PRBool *aCaptionVisible)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP MainWindow::SetCaptionVisible(PRBool aCaptionVisible)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRBool ClientEdge; */
NS_IMETHODIMP MainWindow::GetClientEdge(PRBool *aClientEdge)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP MainWindow::SetClientEdge(PRBool aClientEdge)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute string DataFileName; */
NS_IMETHODIMP MainWindow::GetDataFileName(char * *aDataFileName)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRInt32 Height; */
NS_IMETHODIMP MainWindow::GetHeight(PRInt32 *aHeight)
{
  *aHeight = m_actual_height;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetHeight(PRInt32 aHeight)
{
  DEBUGMSG(winapp, "Settting height to %d\n", aHeight);
  m_actual_height = aHeight;

  gtk_widget_hide(m_app);
  gtk_window_set_default_size(GTK_WINDOW(m_app), -1, m_actual_height);
  gtk_widget_show(m_app);

  return NS_OK;
}

/* attribute PRInt32 Left; */
NS_IMETHODIMP MainWindow::GetLeft(PRInt32 *aLeft)
{
  *aLeft = m_x;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetLeft(PRInt32 aLeft)
{
  m_x = aLeft;
  gdk_window_move(m_app->window, m_x, m_y);

  return NS_OK;
}

/* attribute PRBool MaximizeBoxEnabled; */
NS_IMETHODIMP MainWindow::GetMaximizeBoxEnabled(PRBool *aMaximizeBoxEnabled)
{
  *aMaximizeBoxEnabled = true;
  return NS_OK;
}
NS_IMETHODIMP MainWindow::SetMaximizeBoxEnabled(PRBool aMaximizeBoxEnabled)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRBool Maximized; */
NS_IMETHODIMP MainWindow::GetMaximized(PRBool *aMaximized)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP MainWindow::SetMaximized(PRBool aMaximized)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRInt32 MinHeight; */
NS_IMETHODIMP MainWindow::GetMinHeight(PRInt32 *aMinHeight)
{
  *aMinHeight = m_uHeight;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetMinHeight(PRInt32 aMinHeight)
{
  m_uHeight = aMinHeight;
  gtk_widget_set_usize(m_app, m_uWidth, m_uHeight);
  return NS_OK;
}

/* attribute PRBool Minimized; */
NS_IMETHODIMP MainWindow::GetMinimized(PRBool *aMinimized)
{
  *aMinimized = false;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetMinimized(PRBool aMinimized)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRInt32 MinWidth; */
NS_IMETHODIMP MainWindow::GetMinWidth(PRInt32 *aMinWidth)
{
  *aMinWidth = m_uWidth;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetMinWidth(PRInt32 aMinWidth)
{
  m_uWidth = aMinWidth;
  gtk_widget_set_usize(m_app, m_uWidth, m_uHeight);
  return NS_OK;
}

/* attribute PRBool Resizable; */
NS_IMETHODIMP MainWindow::GetResizable(PRBool *aResizable)
{
  *aResizable = m_resizable;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetResizable(PRBool aResizable)
{
  DEBUGMSG(winapp, "Setting window resizable flag to %s\n",
		   aResizable ? "true" : "false");

  m_resizable = aResizable;

  /*  Setting the window policy in GTK seems to break all 
	  other window resizing ops.

	  gtk_widget_set_usize(m_app, m_uWidth, m_uHeight);
	  gtk_window_set_policy(GTK_WINDOW(m_app), false,
	  m_resizable, false);
  */
  return NS_OK;
}

/* attribute string StatusText; */
NS_IMETHODIMP MainWindow::GetStatusText(char * *aStatusText)
{
  XP_COPY_STRING(GTK_LABEL(m_status)->label, aStatusText);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetStatusText(const char * aStatusText)
{
  gtk_label_set_text(GTK_LABEL(m_status), aStatusText);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::GetStatusVisible(PRBool * vis) {
  *vis = m_statusBarVisible;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetStatusVisible(PRBool vis) {
  m_statusBarVisible = vis;
  if (m_statusBarVisible)
	gtk_widget_show(m_status);
  else
	gtk_widget_hide(m_status);
  return NS_OK;
}

/* attribute string TitleText; */
NS_IMETHODIMP MainWindow::GetTitleText(char * *aTitleText)
{
  XP_COPY_STRING(GTK_WINDOW(m_app)->title, aTitleText);
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetTitleText(const char * aTitleText)
{
  if (aTitleText)
	gtk_window_set_title(GTK_WINDOW(m_app), aTitleText);
  else
	gtk_window_set_title(GTK_WINDOW(m_app), "");
  return NS_OK;
}

/* attribute PRInt32 Top; */
NS_IMETHODIMP MainWindow::GetTop(PRInt32 *aTop)
{
  *aTop = m_y;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetTop(PRInt32 aTop)
{
  m_y = aTop;
  gdk_window_move(m_app->window, m_x, m_y);

  return NS_OK;
}

/* attribute PRBool Visible; */
NS_IMETHODIMP MainWindow::GetVisible(PRBool *aVisible)
{
  *aVisible = m_isVisible;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetVisible(PRBool aVisible)
{
  m_isVisible = aVisible;
  if (m_isVisible)
	gtk_widget_show(m_app);
  else
	gtk_widget_hide(m_app);
  return NS_OK;
}

/* attribute PRInt32 Height; */
NS_IMETHODIMP MainWindow::GetWidth(PRInt32 *aWidth)
{
  *aWidth = m_actual_width;
  return NS_OK;
}

NS_IMETHODIMP MainWindow::SetWidth(PRInt32 aWidth)
{
  DEBUGMSG(winapp, "Setting width to %d\n", aWidth);
  m_actual_width = aWidth;
  gtk_widget_hide(m_app);
  gtk_window_set_default_size(GTK_WINDOW(m_app), m_actual_width, -1);
  gtk_widget_show(m_app);
  return NS_OK;
}
