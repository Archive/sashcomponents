
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <gnome.h>
#include <string>
#include <gtkmozembed.h>

#include <stdlib.h>

#include "WinApp.h"
#include "MainWindow.h"
#include "extensiontools.h"
#include "url_tools.h"
#include "sash_constants.h"
#include "sash_error.h"
#include "sashIXPXMLDocument.h"
#include "sashIXPXMLNode.h"
#include "sashVariantUtils.h"

SASH_LOCATION_IMPL_NO_NSGET(WinApp, sashIWinApp, "WindowApp");

static const int DEFAULT_WIDTH = 750;
static const int DEFAULT_HEIGHT = 500;

WinApp::WinApp() : m_width(0), m_height(0)
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(winapp, "Creating winapp\n");
  m_mainWin = NULL;
}

WinApp::~WinApp()
{
  DEBUGMSG(winapp, "calling destructor\n");
}

NS_IMETHODIMP 
WinApp::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP 
WinApp::Initialize(sashIActionRuntime * act,
		   const char *registrationXML, const char *webGuid,
		   JSContext *cx, JSObject *obj)
{
  m_act = act;

  DEBUGMSG(winapp, "initializing with registration:\n %s\n", 
		   registrationXML);

  string width_str, height_str;

  nsCOMPtr<sashIXPXMLDocument> xml = do_CreateInstance((nsCID)XPXMLDOCUMENT_CID);
  PRBool res;
  xml->LoadFromString(registrationXML, &res);
  if (registrationXML && res) {
	   nsCOMPtr<sashIXPXMLNode> root;
	   xml->GetRootNode(getter_AddRefs(root));
	   if (root) {
			nsCOMPtr<sashIXPXMLNode> htmlNode, widthNode, heightNode;

			root->GetFirstChildNode("htmlfile", getter_AddRefs(htmlNode));
			if (htmlNode)
				 XP_GET_STRING(htmlNode->GetText, m_url);

			root->GetFirstChildNode("width", getter_AddRefs(widthNode));
			if (widthNode)
				 XP_GET_STRING(widthNode->GetText, width_str);

			root->GetFirstChildNode("height", getter_AddRefs(heightNode));
			if (heightNode)
				 XP_GET_STRING(heightNode->GetText, height_str);

	   }
  }
  
  if (m_url == "") {
	   OutputError("No HTML file specified in registration string!\n");
	   return NS_ERROR_ILLEGAL_VALUE;
  }

  m_width = strtol(width_str.c_str(), NULL, 10);
  m_height = strtol(height_str.c_str(), NULL, 10);

  if (m_width == 0)
    m_width = DEFAULT_WIDTH;
  if (m_height == 0)
    m_height = DEFAULT_HEIGHT;

  return NS_OK;
}

/* void run (in string url); */
NS_IMETHODIMP 
WinApp::Run()
{
  nsresult retval = nsComponentManager::CreateInstance(kSashMainWindowCID, NULL, 
													   NS_GET_IID(sashIMainWindow),
													   (void **) &m_mainWin);
  assert(NS_SUCCEEDED(retval));
  m_mainWin->Initialize(this, m_act, m_width, m_height, m_url.c_str());

  gtk_main();
  return NS_OK;
}

/*
  If the argument passed is just a filename, make it absolute 
  and add a protocol.
*/
string
WinApp::FixURL(const string & url)
{
  // return anything with a protocol
  if (URLHasProtocol(url)) {
    DEBUGMSG(winapp, "Not fixing url %s\n", url.c_str());
    return url;
  }

  // Assume it's a file
  if (url.length() == 0)
    return url;
  
  string ret;

  // Is it relative?
  if (url[0] != '/') {
    char *dir;
    m_act->GetDataDirectory(&dir);
    ret = ((string("file://") + dir) + "/") + url;
	nsMemory::Free(dir);
  }
  else
    ret = "file://" + url;
  DEBUGMSG(winapp, "Fixed url %s to be %s\n", url.c_str(), ret.c_str());
  return ret;
}

NS_IMETHODIMP
WinApp::ProcessEvent()
{
  return NS_OK;
}

NS_IMETHODIMP
WinApp::Quit()
{
	 m_act->Cleanup(); 
	 gtk_main_quit();
	 _exit(0);
	 return NS_OK;
}

NS_IMETHODIMP WinApp::GetMainWindow(sashIMainWindow ** win) {
  NS_ADDREF(m_mainWin);
  *win = m_mainWin;
  DEBUGMSG(winapp, "Returning Main Window\n");
  return NS_OK;
}

NS_IMETHODIMP WinApp::GetIsMDIMode(PRBool * isMDI) {
  *isMDI = false;
  return NS_OK;
}

NS_IMETHODIMP WinApp::GetArgs(char ** args) {
  XP_COPY_STRING("", args);
  return NS_OK;
}
