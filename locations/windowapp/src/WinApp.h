/**************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef _WINAPP_H
#define _WINAPP_H

#include <nsICategoryManager.h>

#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIWinApp.h"

//90EF60F4-0FBC-11D4-A7DC-0004AC60599E
#define WINAPP_CID {0x90EF60F4, 0x0FBC, 0x11D4, {0xA7, 0xDC, 0x00, 0x04, 0xAC, 0x60, 0x59, 0x9E}}

NS_DEFINE_CID(kWinAppCID, WINAPP_CID);

#define WINAPP_CONTRACT_ID "@gnome.org/SashXB/WinApp;1"

typedef struct _GtkWidget GtkWidget;
class SashGtkBrowser;

class WinApp : public sashIWinApp, public sashILocation
{
public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIEXTENSION;
  NS_DECL_SASHILOCATION;
  NS_DECL_SASHIWINAPP;

  WinApp();
  virtual ~WinApp();
  /* additional members */
	
private:
  sashIRuntime * m_rt;
  sashIActionRuntime * m_act;
  string m_url;
  int m_width, m_height;

  sashIMainWindow * m_mainWin;
  string FixURL(const string & url);
};

#endif
