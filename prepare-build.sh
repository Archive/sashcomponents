#!/bin/sh
# A script to make symlinks to various build files in each extension/location
# directory. Called in the autogen.sh scripts

# We don't want to have to update these in several places when we change them
for target in macros rules.mk global.mk; do
  test -L $target || ln -s `pwd`/../../$target $target
done

# Make a pseudo-copy of the sashmacros directory
if [ ! -d sashmacros ]; then
  mkdir sashmacros
fi

# Symlink every file in the original sashmacros directory here
for target in `ls ../../sashmacros`; do
  if [ ! -L sashmacros/$target ]; then
    ln -s `pwd`/../../sashmacros/$target sashmacros
  fi
done

