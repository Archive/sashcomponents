/* USE THIS TEMPLATE FOR IMPLEMENTING THE EXTENSION */
package org.mozilla.xpcom;

public class javaSqlISashExtensionImpl implements javaSqlISashExtension, sashIExtension2 {

	private sashIActionRuntime m_act;
	private int contextID;
	private javaSqlIDriverManager driverMan;
	private sashIGenericScriptableConstructor m_arrayConstructor;
	private sashIGenericScriptableConstructor m_blobConstructor;
	private sashIGenericScriptableConstructor m_clobConstructor;
	private sashIGenericScriptableConstructor m_dateConstructor;
	private sashIGenericScriptableConstructor m_timeConstructor;
	private sashIGenericScriptableConstructor m_timestampConstructor;

	public Object queryInterface(IID ignored) {
		return this;
	}

	public javaSqlISashExtensionImpl() {
		m_arrayConstructor= null;
		m_blobConstructor= null;
		m_clobConstructor= null;
		m_dateConstructor= null;
		m_timeConstructor= null;
		m_timestampConstructor= null;
	}

	public sashIGenericScriptableConstructor getArray() {
		try {
			if (m_arrayConstructor== null) {
				m_arrayConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_arrayConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/jdbc/Array;1",
					javaSqlIArray.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_arrayConstructor;
	}

	public sashIGenericScriptableConstructor getBlob() {
		try {
			if (m_blobConstructor== null) {
				m_blobConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_blobConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/jdbc/Blob;1",
					javaSqlIBlob.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_blobConstructor;
	}

	public sashIGenericScriptableConstructor getClob() {
		try {
			if (m_clobConstructor== null) {
				m_clobConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_clobConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/jdbc/Clob;1",
					javaSqlIClob.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_clobConstructor;
	}

	public sashIGenericScriptableConstructor getDate() {
		try {
			if (m_dateConstructor== null) {
				m_dateConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_dateConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/jdbc/Date;1",
					javaSqlIDate.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_dateConstructor;
	}

	public sashIGenericScriptableConstructor getTime() {
		try {
			if (m_timeConstructor== null) {
				m_timeConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_timeConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/jdbc/Time;1",
					javaSqlITime.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_timeConstructor;
	}

	public sashIGenericScriptableConstructor getTimestamp() {
		try {
			if (m_timestampConstructor== null) {
				m_timestampConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_timestampConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/jdbc/Timestamp;1",
					javaSqlITimestamp.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_timestampConstructor;
	}


	public String getSashName() {
		return "JDBC";
	}

	public void initialize(sashIActionRuntime act, String registrationXml, String webGuid, int contextID) {
		m_act= act;
		this.contextID= contextID;
	}

	public void cleanup() {
	}

	public void processEvent() {
	}

	public boolean loadDriverClass(java.lang.String driverClassname) {
		try {
			Class.forName(driverClassname);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

    public javaSqlIDriverManager getDriverManager() {
    	if (driverMan== null)
    		driverMan= (javaSqlIDriverManager) new javaSqlIDriverManagerImpl();
    	return driverMan;
    }
}
