#!/bin/sh
XPIDL=/usr/lib/mozilla-1.0.0/xpidl
XPTLINK=/usr/lib/mozilla-1.0.0/xpt_link
JCXPIDL=`sash-config --dir bin`'/jcxpidl'
MOZIDL=/usr/share/idl/mozilla-1.0.0
SASHIDL=`sash-config --dir idl`
JCJARS1=`sash-config --dir lib`'/components/javaconnect.jar'
JCJARS2=`sash-config --dir lib`'/components/imported.jar'
JCJARS="$JCJARS1:$JCJARS2"
JAVAC=`which javac`
JAR=`which jar`
SWINGLER=`sash-config --dir bin`'/Swingler.pl'

if test ! -x $XPIDL ; then
    echo "Failed to locate xpidl. Check whether you have it (as part of your Mozilla distribution) and edit make_jdbc.sh"
    exit 1
fi
if test ! -x $XPTLINK ; then
    echo "Failed to locate xpt_link. Check whether you have it (as part of your Mozilla distribution) and edit make_jdbc.sh"
    exit 1
fi
if test ! -x $JCXPIDL ; then
    echo "Failed to locate jcxpidl. It is a standart part of the SashXB distribution. Check whether SashXB's bin directory is in your path"
    exit 2
fi
if test ! -d $MOZIDL ; then
    echo "Failed to locate Mozilla idl files. Check whether you have them (as part of your Mozilla distribution) and edit make_jdbc.sh"
    exit 3
fi
if test ! -d $SASHIDL ; then
    echo "Failed to locate SashXB idl files. Check whether you have them / see if SashXB's bin directory is in your path"
    exit 4
fi
if test ! -e $JCJARS1 ; then
    echo "Failed to locate javaconnect.jar. It comes with the Java-enabled distribution of SashXB"
    exit 5
fi
if test ! -e $JCJARS2 ; then
    echo "Failed to locate imported.jar. It comes with the Java-enabled distribution of SashXB"
    exit 6
fi
if test ! -x $JAVAC ; then
    echo "Failed to locate javac - the Java compiler. Check whether you have a Java SDK"
    exit 7
fi
if test ! -x $JAR ; then
    echo "Failed to locate jar - The Java archive tool. Check whether you have a Java SDK"
    exit 8
fi
if test ! -x $SWINGLER ; then
    echo "Failed to locate Swingler.pl. It comes with the Java-enabled distribution of SashXB"
    exit 8
fi

echo Exporting package java.sql
$SWINGLER -r -i -j java.sql.ResultSet java.sql.DriverManager
echo Creating typelibs
for ff in *.idl; do $JCXPIDL -m jc -I $MOZIDL -I $SASHIDL $ff ; done
for ff in *.idl; do $XPIDL -m typelib -I $MOZIDL -I $SASHIDL $ff ; done
echo Compiling
$JAVAC -classpath $JCJARS -d . *.java
echo Creating javaSql.jcm.jar
$JAR cf javaSql.jcm.jar org/mozilla/xpcom/*.class
echo Creating javaSql.xpt
$XPTLINK javaSql.xpt *.xpt
sash-install javaSql.wdf -f
