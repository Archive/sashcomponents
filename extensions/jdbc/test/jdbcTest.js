print("\nStarting test\n");

var connection= Sash.JDBC.driverManager.getConnection(
	'jdbc:mysql://localhost.localdomain/testdb1', 'testuser', 'password'
);

var statement= connection.createStatement();

var rs= statement.executeQuery(
	'SELECT LastName, FirstName FROM People WHERE (Age< 40)'
);

print("People under 40: ");
while (rs.next()) {
	print(rs.getString('FirstName')+ " "+ rs.getString('LastName'));
}
print();

rs= statement.executeQuery(
	'SELECT FirstName, Age FROM People'
);

print("All people: ");
while (rs.next()) {
	print(rs.getString('FirstName')+ " "+ rs.getInt('Age'));
}

Sash.Console.Quit();

