
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation of the FTP Connection object

******************************************************************/

#include "sashFTPConnection.h"

#include "sash_constants.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIGenericConstructor.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "extensiontools.h"
#include <unistd.h>
#include "secman.h"
#include "sashnet.h"
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include "streamBuffer.h"
#include "ftpcodes.h"
#include "sashFTP.h"

NS_IMPL_ISUPPORTS2_CI(sashFTPConnection, sashIFTPConnection, sashIConstructor);

sashFTPConnection::sashFTPConnection()
{
	 DEBUGMSG(ftp, "ftp connection constructor\n");
	 NS_INIT_ISUPPORTS();

	 m_normalTimeout = DEFAULT_NORMAL_TIMEOUT;
	 m_connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
	 m_connected = false;
	 m_timedOut = false;
	 m_loggedIn = false;
	 m_loginReady = false;
	 m_stopTransfer = false;
	 m_isPassive = true;
	 m_currTransfer = false;
	 m_showDotFiles = false;

	 m_mainSockFD = -1;
	 m_dataSockFD = -1;

	 m_dataType = TYPE_ASCII;

	 m_notifyIntervalK = -1;
	 m_notifyIntervalS = -1;
}

sashFTPConnection::~sashFTPConnection()
{
	 pthread_mutex_destroy(&m_connectionLock);
	 pthread_mutex_destroy(&m_transferLock);
	 pthread_mutex_destroy(&m_idleLock);
	 pthread_cond_destroy(&m_idleCond);
	 pthread_mutex_destroy(&m_stopLock);
	 pthread_cond_destroy(&m_stopCond);
	 if (m_ftpResponses) delete m_ftpResponses;
}

NS_IMETHODIMP
sashFTPConnection::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
									    PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	 DEBUGMSG(ftp, "initializing new ftp connection object\n");
	 m_cx = cx;
	 m_ext = ext;
	 m_act = actionRuntime;

	 pthread_mutex_init(&m_connectionLock, NULL);
	 pthread_mutex_init(&m_transferLock, NULL);
	 pthread_mutex_init(&m_idleLock, NULL);
	 pthread_cond_init(&m_idleCond, NULL);
	 pthread_mutex_init(&m_stopLock, NULL);
	 pthread_cond_init(&m_stopCond, NULL);

	 m_ftpResponses = new streamBuffer();

	 *ret = true;
	 start();
	 return NS_OK;
}

void sashFTPConnection::reinit(){
	 // check to see if there's a current transfer?

	 lockConnection();
	 m_connected = false;
	 m_loggedIn = false;
	 m_loginReady = false;
	 m_timedOut = false;
	 m_stopTransfer = false;
	 m_isPassive = true;
	 m_currTransfer = false;

	 m_dataType = TYPE_ASCII;
	 if (m_mainSockFD != -1) {
		  close (m_mainSockFD);
		  m_mainSockFD = -1;
	 }
	 if (m_dataSockFD != -1){
		  close (m_dataSockFD);
		  m_dataSockFD = -1;
	 }
	 // clear the event queue?
	 m_ftpResponses->clear();
	 unlockConnection();

	 // clear the transfer queue.
	 lockTransferQueue();
	 m_tQueue.clear();
	 unlockTransferQueue();
}

static void * start_run(void *manager){
	 DEBUGMSG(ftp, "about to run...\n");
	 assert (manager);
	 sashFTPConnection *c = (sashFTPConnection *) manager;
	 c->run();
	 return NULL;
}

bool sashFTPConnection::start(){
	 DEBUGMSG(ftp, "starting ftp connection thread...\n");

	 pthread_attr_t attr;
	 pthread_attr_init(&attr);

	 lockConnection();
	 int res = pthread_create(&m_transferFiles, &attr, 
							  start_run, (void *) this);  
	 bool retval = m_running = (res == 0);
	 unlockConnection();
	 
	 return retval;
}

void sashFTPConnection::waitForSignal()
{
	 DEBUGMSG(ftp, "sashFTPConnection: waitForSignal\n");
	 pthread_mutex_lock(&m_idleLock);
	 pthread_cond_wait(&m_idleCond, &m_idleLock);
	 pthread_mutex_unlock(&m_idleLock);
}

void sashFTPConnection::sendSignal(){
	 DEBUGMSG(ftp, "sashFTPConnection: sending signal\n");
	 pthread_mutex_lock(&m_idleLock);
	 pthread_cond_signal(&m_idleCond);
	 pthread_mutex_unlock(&m_idleLock);
}

void sashFTPConnection::waitForStop()
{
	 DEBUGMSG(ftp, "sashFTPConnection: waitForStop\n");
	 pthread_mutex_lock(&m_stopLock);
	 pthread_cond_wait(&m_stopCond, &m_stopLock);
	 pthread_mutex_unlock(&m_stopLock);
}

void sashFTPConnection::stoppedSignal(){
	 DEBUGMSG(ftp, "sashFTPConnection: sending stopped signal\n");
	 pthread_mutex_lock(&m_stopLock);
	 pthread_cond_signal(&m_stopCond);
	 pthread_mutex_unlock(&m_stopLock);
}

void sashFTPConnection::run()
{
	 tRequest *currTR;
	 m_running = true;

	 while (m_running){
		  DEBUGMSG(ftp, "waiting for connect\n");
		  while (! isConnected()){
			   waitForSignal();
		  }
		  DEBUGMSG(ftp, "waiting for login\n");
		  while (isConnected() && (!isLoggedIn()) && ! (isLoginReady())){
			   waitForSignal();
		  }
		  if (isConnected() && ! isLoggedIn() && isLoginReady()){
			   DEBUGMSG(ftp, "trying to log in user: %s\n", 
						m_username.c_str());
			   FTPCommandSuccess("user", m_username.c_str());
			   setLoggedIn(FTPCommandSuccess("pass", m_password.c_str()));
			   setLoginReady(false);
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onLogin, (sashIFTPConnection *)this, isLoggedIn());
		  }
		  DEBUGMSG(ftp, "waiting for more files..\n");
		  while (isConnected() && isLoggedIn() && emptyTransferQueue()){
			   waitForSignal();
		  }
		  if (isConnected() && isLoggedIn() && ! emptyTransferQueue()){
			   // when a file is supposed to be sent or received, wake up,
			   // send files.
			   while ((! emptyTransferQueue()) && (! isTimedOut())){
					lockTransferQueue();
					currTR = m_tQueue.front();
					m_tQueue.pop_front();
					unlockTransferQueue();
						 
					// want to wait until the transfer has been stopped
					// so that ftp commands don't get mixed up.
					while (shouldStopTransfer()){
						 waitForStop();
					}
						 
					if (currTR->isGet){
						 DEBUGMSG(ftp, 
								  "trying to get: file %s (new name: %s)\n",
								  currTR->filename.c_str(),
								  currTR->newfilename.c_str());
						 getSingleFile(currTR->filename,
									   currTR->newfilename);
					} else {
						 DEBUGMSG(ftp, 
								  "trying to put: file %s (new name: %s)\n",
								  currTR->filename.c_str(),
								  currTR->newfilename.c_str());
						 putSingleFile(currTR->filename,
									   currTR->newfilename);
					}
						 
					if (currTR) delete (currTR);
			   }
		  }
	 }
}

int FileSize(const string& fname) {
	 struct stat s;
	 if (stat(fname.c_str(), &s) == -1) {
		  return -1;
	 }
	 
	 return s.st_size;
}

/* readonly attribute PRBool Connected; */
NS_IMETHODIMP sashFTPConnection::GetConnected(PRBool *aConnected)
{
	 *aConnected = isConnected();
	 return NS_OK;
}

bool sashFTPConnection::isConnected(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_connected;
	 if (lock) unlockConnection();
	 return retval;
}

void sashFTPConnection::setConnected(bool val, bool lock){
	 if (lock) lockConnection();
	 m_connected = val;
	 if (lock) unlockConnection();
}

bool sashFTPConnection::isTimedOut(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_timedOut;
	 if (lock) unlockConnection();
	 return retval;
}
void sashFTPConnection::setTimedOut(bool val, bool lock){
	 if (lock) lockConnection();
	 m_timedOut = val;
	 if (lock) unlockConnection();
}



/* readonly attribute boolean TimedOut; */
NS_IMETHODIMP sashFTPConnection::GetTimedOut(PRBool *aTimedOut)
{
	 *aTimedOut = m_timedOut;
	 return NS_OK;
}

/* readonly attribute PRBool LoggedIn; */
NS_IMETHODIMP sashFTPConnection::GetLoggedIn(PRBool *aLoggedIn)
{
	 *aLoggedIn = isLoggedIn();
	 return NS_OK;
}

bool sashFTPConnection::isLoggedIn(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_loggedIn;
	 if (lock) unlockConnection();
	 return retval;
}

void sashFTPConnection::setLoggedIn(bool val, bool lock){
	 if (lock) lockConnection();
	 m_loggedIn = val;
	 if (lock) unlockConnection();
}

bool sashFTPConnection::isLoginReady(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_loginReady;
	 if (lock) unlockConnection();
	 return retval;
}

void sashFTPConnection::setLoginReady(bool val, bool lock){
	 if (lock) lockConnection();
	 m_loginReady = val;
	 if (lock) unlockConnection();
}

bool sashFTPConnection::ready(bool lock){
	 if (lock) lockConnection();
	 bool retval = m_loggedIn && m_connected && (! m_currTransfer);
	 if (lock) unlockConnection();
	 return retval;

}

/* PRBool Connect (in string remoteAddr, in PRInt32 remotePort, in nsIVariant localPort); */
NS_IMETHODIMP sashFTPConnection::Connect(const char *remoteAddr, nsIVariant *remotePort, PRBool *_retval)
{
	 if (! isConnected()){
		  bool connected;
		  lockConnection();

		  assert(remoteAddr);
		  assert(remotePort);
		  m_remotePort = (VariantIsEmpty(remotePort)) ? 
			   DEFAULT_FTP_PORT : (int)(VariantGetNumber(remotePort));
		  
		  // set up remote address
		  setRemoteAddr((struct sockaddr_in *)&m_remoteAddr, remoteAddr, 
						m_remotePort);

		  string status;
		  if ((m_mainSockFD = openConnection(SOCK_STREAM,&m_remoteAddr))==-1){
			   DEBUGMSG(ftp, "couldn't open connection\n");
			   m_connected = false;
			   *_retval = false;
		  } else {
			   DEBUGMSG(ftp, "opened connection\n");
			   m_connected = true;
			   int responseCode = receiveFTPResponse(m_mainSockFD, status,
													 false);
			   *_retval = processFTPResponseCode(responseCode);
		  }
		  connected = m_connected;
		  unlockConnection();

		  if (connected){
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);
		  }

		  DEBUGMSG(ftp, "connection opened, sending signal...");
		  sendSignal();
	 } else {
		  DEBUGMSG(ftp, "already connected...\n");
		  *_retval = true;
	 }
	 return NS_OK;
}

/* void Disconnect (); */
NS_IMETHODIMP sashFTPConnection::Disconnect()
{
	 DEBUGMSG(ftp, "disconnecting...\n");
	 reinit();
	 DEBUGMSG(ftp, "Disconnect send signal\n");
	 sendSignal();
	 return NS_OK;
}


/* void Login (in string username, in string password); */
NS_IMETHODIMP sashFTPConnection::Login(const char *username, 
									   const char *password)
{
	 DEBUGMSG(ftp, "logging in for user: %s\n", username);
	 if (isConnected() && (! isLoggedIn())){
		  lockConnection();
		  m_username = username;
		  m_password = password;
		  m_loginReady = true;
		  unlockConnection();

		  DEBUGMSG(ftp, "updated username and password (%s), sending signal\n",
				   username);
		  
		  sendSignal();
	 } else if (! isConnected()){
		  DEBUGMSG(ftp, "not connected...\n");
	 } else if (isLoggedIn()){
		  DEBUGMSG(ftp, "already logged in...\n");
	 } else {
		  DEBUGMSG(ftp, "hmmm...\n");
	 }
	 return NS_OK;
}

/* PRBool Logout (); */
NS_IMETHODIMP sashFTPConnection::Logout(PRBool *_retval)
{
	 if (isConnected() && isLoggedIn()){
		  DEBUGMSG(ftp,"logging out...\n");

		  if (isCurrTransfer()){
			   DEBUGMSG(ftp, "file currently being transferred...\n");

			   // clear the transfer queue.
			   lockTransferQueue();
			   m_tQueue.clear();
			   unlockTransferQueue();

			   // stop the current transfer
			   setStopTransfer(true);

			   // wait for the transfer to stop
			   while (shouldStopTransfer()){
					waitForStop();			   
			   }
		  }
		  *_retval = FTPCommandSuccess("quit", NULL);
		  reinit();

		  // wake up the thread so that it goes back to waiting for
		  // a login instead of waiting for files.
		  DEBUGMSG(ftp, "trying to wake up main thread...\n");
		  sendSignal();
	 } else {
		  DEBUGMSG(ftp,"not connected or not logged in...\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* attribute long NormalTimeout; */
NS_IMETHODIMP sashFTPConnection::GetNormalTimeout(PRInt32 *aNormalTimeout)
{
	 *aNormalTimeout = m_normalTimeout;
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetNormalTimeout(PRInt32 aNormalTimeout)
{
	 m_normalTimeout = aNormalTimeout;
	 return NS_OK;
}

/* attribute long ConnectionTimeout; */
NS_IMETHODIMP sashFTPConnection::GetConnectionTimeout(PRInt32 *aConnectionTimeout)
{
	 *aConnectionTimeout = m_connectionTimeout;
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetConnectionTimeout(PRInt32 aConnectionTimeout)
{
	 m_connectionTimeout = aConnectionTimeout;
	 return NS_OK;
}

/* readonly attribute string currentDirectory; */
NS_IMETHODIMP sashFTPConnection::GetCurrentDirectory(char * *aCurrentDirectory)
{
	 string substr;
	 if (ready()){
		  // can't use FTPCommandSuccess here because we need to save
		  // the return string so that we can grab the current working
		  // directory out of it.
		  sendNormalFTPCommand (m_mainSockFD, "pwd", NULL);
		  string status;
		  int responseCode = receiveFTPResponse(m_mainSockFD, status);

		  if (processFTPResponseCode(responseCode)){
			   int leftQuote = status.find('"');
			   int rightQuote = status.find('"', leftQuote+1);

			   if ((leftQuote == -1) || (rightQuote == -1)){
					DEBUGMSG(ftp, "return from pwd not well-formed!\n");
			   } else {
					substr = status.substr(leftQuote+1, 
											rightQuote-leftQuote -1);
					DEBUGMSG(ftp, "pwd: %s\n", substr.c_str());
			   }
		  } else {
			   DEBUGMSG(ftp, "No action: couldn't process response code.\n");
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
	 }
	 XP_COPY_STRING(substr.c_str(), aCurrentDirectory);
	 return NS_OK;
}

/* attribute PRInt32 NotifyInterval_K; */
NS_IMETHODIMP sashFTPConnection::GetNotifyInterval_K(PRInt32 *aNotifyInterval_K)
{
	 lockConnection();
	 *aNotifyInterval_K = m_notifyIntervalK;
	 unlockConnection();
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetNotifyInterval_K(PRInt32 aNotifyInterval_K)
{
	 lockConnection();
	 m_notifyIntervalK = aNotifyInterval_K;
	 unlockConnection();
	 return NS_OK;
}

/* attribute PRInt32 NotifyInterval_S; */
NS_IMETHODIMP sashFTPConnection::GetNotifyInterval_S(PRInt32 *aNotifyInterval_S)
{
	 lockConnection();
	 *aNotifyInterval_S = m_notifyIntervalS;
	 unlockConnection();
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetNotifyInterval_S(PRInt32 aNotifyInterval_S)
{
	 lockConnection();
	 m_notifyIntervalS = aNotifyInterval_S;
	 unlockConnection();
	 return NS_OK;
}

/* attribute PRBool isASCII; */
NS_IMETHODIMP sashFTPConnection::GetIsASCII(PRBool *aIsASCII)
{
	 lockConnection();
	 if (m_dataType == TYPE_ASCII){
		  *aIsASCII = true;
	 } else if (m_dataType == TYPE_BINARY){
		  *aIsASCII = false;
	 }
	 unlockConnection();
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetIsASCII(PRBool aIsASCII)
{
	 lockConnection();
	 int dataType = m_dataType;
	 unlockConnection();

	 if (ready()){
		  if ((aIsASCII && (dataType == TYPE_ASCII)) 
			  || (! aIsASCII && (dataType == TYPE_BINARY))) {
			   // do nothing.
			   DEBUGMSG(ftp, 
						"Data type is already %s, no need to change it.\n",
						(aIsASCII ? "ASCII" : "binary"));
		  } else {
			   DEBUGMSG(ftp, "Changing type to %s.\n",
						(aIsASCII ? "ASCII" : "binary"));

			   const char *c = (aIsASCII ? "a" : "i");
	
			   if(FTPCommandSuccess("type", c)){
					lockConnection();
					m_dataType = (aIsASCII ? TYPE_ASCII : TYPE_BINARY);
					unlockConnection();
			   }
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
	 }
	 return NS_OK;
}

/* attribute boolean showDotFiles; */
NS_IMETHODIMP sashFTPConnection::GetShowDotFiles(PRBool *aShowDotFiles)
{
	 *aShowDotFiles = m_showDotFiles;
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetShowDotFiles(PRBool aShowDotFiles)
{
	 DEBUGMSG(ftp, "setting showdotfiles to %s\n",
			  (aShowDotFiles ? "true" : "false"));
	 m_showDotFiles = aShowDotFiles;
	 return NS_OK;
}



/* readonly attribute PRBool isPassive; */
NS_IMETHODIMP sashFTPConnection::GetIsPassive(PRBool *aIsPassive)
{
	 *aIsPassive = isPassive();
	 return NS_OK;
}

bool sashFTPConnection::isPassive(){
	 lockConnection();
	 bool retval = m_isPassive;
	 unlockConnection();
	 return retval;
}

/* attribute string id; */
NS_IMETHODIMP sashFTPConnection::GetId(char * *aId)
{
	 XP_COPY_STRING(m_id.c_str(), aId);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetId(const char * aId)
{
	 m_id = aId;
	 return NS_OK;
}

/* PRInt32 get (in nsIVariant fileNames, in nsIVariant lFileOption); */
NS_IMETHODIMP sashFTPConnection::Get(nsIVariant *fileNames, nsIVariant *lFileOption, PRInt32 *_retval)
{
	 *_retval = 0;
	 if (isConnected() && isLoggedIn()){
		  tRequest *tr;
		  string currFileName;

		  // check to see whether it's a single file.
		  if (VariantIsString(fileNames)){
			   tr = new tRequest;
			   string fname = VariantGetString (fileNames);
			   createRequest(tr, true, fname, lFileOption);
			   DEBUGMSG(ftp, 
						"adding file %s (new name: %s) to transfer queue\n",
						tr->filename.c_str(),
						tr->newfilename.c_str());
			   lockTransferQueue();
			   m_tQueue.push_back(tr);
			   unlockTransferQueue();
			   (*_retval) ++;
		  } else if (VariantIsArray(fileNames)){
			   vector <string> strvec;
			   VariantGetArray(fileNames, strvec);

			   for (unsigned int i=0; 
					i < strvec.size(); i++){
					tr = new tRequest;
					createRequest(tr, true, strvec[i], lFileOption);
					DEBUGMSG(ftp, 
							 "adding file %s (new name: %s) to the transfer queue\n",
							 tr->filename.c_str(),
							 tr->newfilename.c_str());
					lockTransferQueue();
					m_tQueue.push_back(tr);
					unlockTransferQueue();
					(*_retval) ++;
			   }	
		  }

		  // if files were added, then signal.
		  if (*_retval > 0){
			   sendSignal();
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in.\n");
	 }
	 return NS_OK;
}

// Creates a transfer request, which specifies the old and new filenames.
// If the optional filenameopt nsIVariant is given, then it is interpreted
// to be the new path for the file.
//
// example:
// if filename = "/home/sash/temp.txt" 
// and filenameopt = "/home/sash2" (or "/home/sash2/")
// then 
// req->filename = "/home/sash/temp.txt" 
// and req->newfilename = "/home/sash2/temp.txt"
int sashFTPConnection::createRequest(tRequest *req, 
									 bool isGet,
									 const string &filename, 
									 nsIVariant *filenameopt){
	 DEBUGMSG(ftp, "createRequest: begin\n");
	 string baseFileName;
	 string targetPathName;
	 int index;

	 req->isGet = isGet;
	 req->filename = filename;
	 
	 if (! VariantIsEmpty(filenameopt)){
		  if (VariantIsString(filenameopt)){
			   index = req->filename.rfind("/");

			   if (index != -1){
					baseFileName = req->filename.substr(index+1);
			   } else {
					baseFileName = req->filename;
			   }

			   targetPathName = VariantGetString(filenameopt);
			   if ((targetPathName.size()-1) == targetPathName.rfind("/"))
			   {
					req->newfilename = targetPathName + baseFileName;
			   } else {
					req->newfilename = targetPathName + "/" + baseFileName;
			   }
		  }
	 } else {
		  req->newfilename = req->filename;
	 }

	 string & local_file = req->isGet ? req->newfilename : req->filename;
	 DEBUGMSG(ftp, "Checking FS access on %s\n", local_file.c_str());
	 m_act->AssertFSAccess(local_file.c_str());

	 return 1;
}

int sashFTPConnection::getSingleFile(const string& rfileName,
									 const string& lfileName){
	 DEBUGMSG(ftp, "getsinglefile: %s\n", rfileName.c_str());
	 string s;
//	 m_act->AssertFSAccess(lfileName.c_str());

	 setCurrTransfer(true, rfileName);
	 setCurrTransferGet(true);

	 // open a new file to write into.
	 FILE* filefd = fopen(lfileName.c_str(), "w+");
  
	 if (! filefd){
		  DEBUGMSG(ftp, "error opening the file %s!: %s\n",
				   lfileName.c_str(), strerror(errno));
		  s = "Error opening the file " + lfileName + "\n";
		  setCurrTransfer(false, "");
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, s);
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onFileReceiveError, (sashIFTPConnection *)this, rfileName, s);
		  return -1;
	 }
	 DEBUGMSG(ftp, "file %s created for writing\n", lfileName.c_str());

	 // set up a data connection.
	 int dataSockFD = getDataSockFD();
	 DEBUGMSG(ftp, "data sock fd: %d\n", dataSockFD);
	 
	 if (dataSockFD == -1){
		  DEBUGMSG(ftp, "couldn't open data connection\n");
		  fclose(filefd);
		  s = "Error opening data connection";
		  setCurrTransfer(false, "");
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, s);
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onFileReceiveError, (sashIFTPConnection *)this, rfileName, s);
		  return -1;
	 }

	 // don't use FTPCommandSuccess because you want to use the error 
	 // message (if there is one).
	 sendNormalFTPCommand(m_mainSockFD, "retr", rfileName.c_str());

	 // receive the start transfer message (150)
	 string status;
	 int responseCode = receiveFTPResponse(m_mainSockFD, status);

	 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
		  m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);

	 if (! processFTPResponseCode(responseCode)){
		  DEBUGMSG(ftp, "error trying to get file.\n");
		  if (filefd) fclose(filefd);
		  if (m_dataSockFD != -1) close(m_dataSockFD);
		  setCurrTransfer(false, "");
		  if (responseCode > 0)
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onFileReceiveError, (sashIFTPConnection *)this, 
					rfileName, status);
		  return -1;
	 }

	 DEBUGMSG(ftp, "receiving into file...\n");
	 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
		  m_cx, m_onFileReceiveStart, (sashIFTPConnection *)this, rfileName);

	 int totalTime;
	 int received = receiveIntoFile(m_dataSockFD, filefd, rfileName, 
									&totalTime);

	 DEBUGMSG(ftp, "received %d bytes for file\n", received);
	 fclose(filefd);

	 int retval;

	 // if there was an error, the error message was sent already.
	 // return from this method.
	 if (received == -1) {
		  close(m_dataSockFD);

		  responseCode = receiveFTPResponse(m_mainSockFD, status);
		  setCurrTransfer(false, "");
		  if (responseCode > 0)
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);
		  retval = -1;
	 } else if (received == -2) {
		  // is there some way to guarantee that the 'abor' is received 
		  // before this connection is terminated? i can't confirm this, 
		  // but i think that if the connection is closed before 'abor' has
		  // been processed, it generates a different kind of error
		  // (something like broken pipe instead of connection reset). 
		  // this sometimes leads to the main connection behind disconnected as
		  // well.

		  // if the file transfer was aborted, then send an abort command.
		  // this will generate an extra ftp response in addition to the
		  // one associated with the end of the file transfer.

		  // NOTE: we don't use the FTPCommandSuccess method here
		  // because 'abor' is not like other commands - the response does
		  // not come right away, rather it comes after the response that
		  // refers to the transfer status.
		  DEBUGMSG(ftp, "sending 'abor' command\n");
		  sendNormalFTPCommand(m_mainSockFD, "abor", NULL);

		  close(m_dataSockFD);

		  DEBUGMSG(ftp, "receiving response. should be something about connection reset\n");
		  if (receiveFTPResponse(m_mainSockFD, status) == -2){
			   DEBUGMSG(ftp, "timed out after 'abor'\n");
			   // if it times out, don't do anything.
			   setCurrTransfer(false, "");

		  } else {
			   // if it doesn't time out, read the messages.
			   setCurrTransfer(false, "");

			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);

			   DEBUGMSG(ftp, "receiving response. should be something about abort success\n");
			   responseCode = receiveFTPResponse(m_mainSockFD, status);
			   if (responseCode == -2){
					DEBUGMSG(ftp, "timed out while waiting for abor response\n");
			   } else {
					((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
						 m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);
			   }
		  }

		  retval = -1;
	 } else {
		  close(m_dataSockFD);

		  // transfer (should have) completed succesfully.
		  // receive the transfer complete message (226)
		  DEBUGMSG(ftp, 
				   "receiving response. should be something about success\n");
		  responseCode = receiveFTPResponse(m_mainSockFD, status);
		  setCurrTransfer(false, "");
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);

		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onFileReceived, (sashIFTPConnection *)this,
			   rfileName, 
			   (double) received,
			   (double) totalTime);

		  retval =  
			   (processFTPResponseCode(responseCode) ? 1 : -1);
	 }

	 // just in case something was stopped but the file transfer still
	 // finished.
	 setStopTransfer(false);
	 stoppedSignal();

	 return retval;
}

int sashFTPConnection::receiveIntoFile(int dataSockFD, 
									   FILE* filefd, 
									   const string& rfileName,
									   int *totalTime){
	 assert (dataSockFD != -1);
	 assert (filefd);

	 char buf[RECEIVE_BUFSIZE+1];
	 int startTime = time(NULL);

	 // receive until connection closed
	 int totalAmtReceived =0;
	 int amtReceived=1;
	 int blockNum = 1;
	 int blockNumS = 1;
	 int secondsElapsed = 0;

	 while ((amtReceived > 0) && (! shouldStopTransfer())){
		  amtReceived = recv(dataSockFD, buf, RECEIVE_BUFSIZE, 0);

		  if (amtReceived > 0){
			   fwrite(buf, sizeof(char), amtReceived, filefd);

			   totalAmtReceived += amtReceived;

			   secondsElapsed = time(NULL) - startTime;
			   
			   // check the progress.
			   if (m_notifyIntervalK > 0){
					if (totalAmtReceived >=
						blockNum * m_notifyIntervalK * 1024){
						 blockNum ++;
						 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
							  m_cx, m_onProgress, (sashIFTPConnection *)this,
							  rfileName, 
							  (double)totalAmtReceived,
							  (double)secondsElapsed);
					}
			   } 
			   if (m_notifyIntervalS > 0){
					if (secondsElapsed >= blockNumS * m_notifyIntervalS){
						 blockNumS ++;
						 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
							  m_cx, m_onProgress, (sashIFTPConnection *)this,
							  rfileName,
							  (double)totalAmtReceived,
							  (double)secondsElapsed);
					}
			   }
		  } else if (amtReceived == -1){
			   DEBUGMSG(ftp, "error receiving file: %s\n", strerror(errno));

			   string s = "Recv socket error.";
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onFileReceiveError, (sashIFTPConnection *)this,
					rfileName, s);

			   return -1;
		  }
	 }

	 *totalTime = time(NULL) - startTime;

	 if (shouldStopTransfer()){
		  DEBUGMSG(ftp, "getSingleFile stopped!\n");
		  return -2;
	 } else {
// 		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
// 			   m_cx, m_onFileReceived, (sashIFTPConnection *)this,
// 			   rfileName, 
// 			   (double) totalAmtReceived,
// 			   (double) totalTime);
	 }
	 return totalAmtReceived;
}

/* PRInt32 put (in nsIVariant fileNames, in nsIVariant rFileOption); */
NS_IMETHODIMP sashFTPConnection::Put(nsIVariant *fileNames, nsIVariant *rFileOption, PRInt32 *_retval)
{
	 *_retval = 0;
	 if (isConnected() && isLoggedIn()){
		  tRequest *tr;
		  string currFileName;

		  // check to see whether it's a single file.
		  assert (fileNames);
		  if (VariantIsString(fileNames)){
			   tr = new tRequest;
			   string fname = VariantGetString(fileNames);
			   createRequest(tr, false, fname, rFileOption);
			   DEBUGMSG(ftp, 
						"adding file %s (new name: %s) to transfer queue\n",
						tr->filename.c_str(),
						tr->newfilename.c_str());
			   lockTransferQueue();
			   m_tQueue.push_back(tr);
			   unlockTransferQueue();
			   (*_retval) ++;
		  } else if (VariantIsArray(fileNames)){
			   DEBUGMSG(ftp, "adding %d files to the transfer queue...\n",
						VariantGetArrayLength(fileNames));
			   
			   vector <string> strvec;
			   VariantGetArray(fileNames, strvec);

			   for (unsigned int i=0; 
					i < strvec.size(); i++){
					tr = new tRequest;
					createRequest(tr, false, strvec[i], rFileOption);
					DEBUGMSG(ftp, 
							 "adding file %s (new name: %s) to the transfer queue\n",
							 tr->filename.c_str(),
							 tr->newfilename.c_str());
					lockTransferQueue();
					m_tQueue.push_back(tr);
					unlockTransferQueue();
					(*_retval) ++;
			   }
		  }

		  // if files were added, then signal.
		  if (*_retval > 0){
			   sendSignal();
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in.\n");
	 }

	 return NS_OK;
}

int sashFTPConnection::putSingleFile(const string& lfileName,
									 const string& rfileName){
	 DEBUGMSG(ftp, "putsinglefile: %s\n", lfileName.c_str());
	 string s;
//	 m_act->AssertFSAccess(lfileName.c_str());
	 setCurrTransfer(true, lfileName);
	 setCurrTransferGet(false);

	 // make sure we can open the file for reading.
	 FILE* filefd = fopen(lfileName.c_str(), "r");
	 if (! filefd){
		  DEBUGMSG(ftp, "couldn't open file %s: %s\n", lfileName.c_str(), strerror(errno));
		  s = "Error opening the file " + lfileName + "\n";
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onStatusMessage, (sashIFTPConnection *)this, s);
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onFileSendError, (sashIFTPConnection *)this, 
					lfileName, s);

		  setCurrTransfer(false, "");
		  return -1;
	 }
	 DEBUGMSG(ftp, "putSingleFile: local file %s opened, fd%d\n",
			  lfileName.c_str(), (int)filefd);

	 // set up a data connection.
	 int dataSockFD = m_dataSockFD = getDataSockFD();
	 if (dataSockFD == -1){
		  DEBUGMSG(ftp, "couldn't open data connection\n");
		  fclose(filefd);		  

		  s = "Error opening data connection.";
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onFileSendError, (sashIFTPConnection *)this, 
			   lfileName, s);

		  setCurrTransfer(false, "");
		  return -1;
	 }

	 // don't use FTPCommandSuccess because you want to use the error 
	 // message (if there is one).
	 sendNormalFTPCommand(m_mainSockFD, "stor", rfileName.c_str());

	 // receive the start transfer message (150)
	 string status;
	 int responseCode = receiveFTPResponse(m_mainSockFD, status);
	 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
		  m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);

	 if (! processFTPResponseCode(responseCode)){	
		  DEBUGMSG(ftp, "error trying to put file.\n");
		  if (filefd) fclose(filefd);
		  if (m_dataSockFD != -1) close(m_dataSockFD);
		  if (responseCode > 0)
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onFileSendError, (sashIFTPConnection *)this, 
					lfileName, status);

		  setCurrTransfer(false, "");
		  return -1;
	 }

	 DEBUGMSG(ftp, "queueing onfilesendstart event.\n");
	 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
		  m_cx, m_onFileSendStart, (sashIFTPConnection *)this, lfileName);
	 
	 int totalTime;
	 int amtSent = sendFromFile(m_dataSockFD, filefd, lfileName, &totalTime);

	 DEBUGMSG(ftp, "done sending; closing file now.\n");
	 fclose(filefd);
	 close(m_dataSockFD);

	 // receive the transfer complete message (226)
	 responseCode = receiveFTPResponse(m_mainSockFD, status);
	 if (responseCode > 0)
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);

	 setCurrTransfer(false, "");

	 int retval;
	 // some sort of error.
	 if (amtSent == -1){
		  retval = -1;
	 } else if (amtSent == -2){
		  // transfer was stopped.
		  setStopTransfer(false);
		  stoppedSignal();
		  retval = -1;
	 } else {
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onFileSent, (sashIFTPConnection *)this,
			   lfileName, 
			   (double) amtSent,
			   (double) totalTime);
		  retval = 
			   (processFTPResponseCode(responseCode) ? 1 : -1);
	 }
	 return retval;
}
int sashFTPConnection::sendFromFile(int dataSockFD, FILE* filefd, 
									const string& lfileName,
									int *totalTime){
	 assert (dataSockFD != -1);
	 assert (filefd);

	 int startTime = time(NULL);

	 // read from file, write to data connection.
	 int filesize = FileSize(lfileName);
	 int amtRead = 0;
	 int amtLeft = filesize;
	 int blockNum = 1;
	 int blockNumS = 1;
	 int totalAmtSent = 0;
	 int secondsElapsed = 0;
	 char databuf[SEND_BUFSIZE];

	 while ((amtLeft != 0) && (! shouldStopTransfer())){
		  amtRead = fread(databuf, sizeof(char), SEND_BUFSIZE, filefd);

		  assert (dataSockFD != -1);
		  if (fullSend(dataSockFD, amtRead, databuf) != -1){
			   totalAmtSent += amtRead;

			   secondsElapsed = time(NULL) - startTime;
			   // check the progress
			   if (m_notifyIntervalK > 0){
					if (totalAmtSent >= blockNum * m_notifyIntervalK * 1024){
						 blockNum ++;
						 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
							  m_cx, m_onProgress, (sashIFTPConnection *)this,
							  lfileName, 
							  (double)totalAmtSent,
							  (double)secondsElapsed);
					}
			   }
			   if (m_notifyIntervalS > 0){
					if (secondsElapsed >= blockNumS * m_notifyIntervalS){
						 blockNumS ++;
						 ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
							  m_cx, m_onProgress, (sashIFTPConnection *)this,
							  lfileName, 
							  (double)totalAmtSent,
							  (double)secondsElapsed);
					}
			   }
			   amtLeft -= amtRead;
		  } else {
			   DEBUGMSG(ftp, "error sending file: %s\n", strerror(errno));

			   string s = "Send socket error.";
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onFileSendError, (sashIFTPConnection *)this,
					lfileName, s);
			   return -1;
		  }
	 }
	 *totalTime = time(NULL) - startTime;

	 if (shouldStopTransfer()){
		  DEBUGMSG(ftp, "putSingleFile stopped!\n");
		  return -2;
	 } else {
// 		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
// 			   m_cx, m_onFileSent, (sashIFTPConnection *)this,
// 			   lfileName, 
// 			   (double) totalAmtSent,
// 			   (double) totalTime);
	 }
	 return totalAmtSent;
}

/* PRBool RemoveFromQueue (in nsIVariant file); */
NS_IMETHODIMP sashFTPConnection::RemoveFromQueue(nsIVariant *file, PRBool *_retval)
{
	 assert (file);

	 DEBUGMSG(ftp, "RemoveFromQueue\n");
	 bool elementFound = false;
	 lockTransferQueue();

	 if (VariantIsString(file)){
		  string filename = VariantGetString(file);
		  // iterate through the transfer queue, find the one that matches.
		  deque<tRequest *>::iterator a = m_tQueue.begin();
		  deque<tRequest *>::iterator b = m_tQueue.end();

		  while (a != b){
			   if (((*a)->filename) == filename){
					DEBUGMSG(ftp, "removing matched file: %s\n", 
							 filename.c_str());
					
					// remove a from the transfer queue.
					delete *a;
					m_tQueue.erase(a);
					elementFound = true;
					break;
			   }
			   ++a;
		  }
	 } else if (VariantIsNumber(file)){
		  int queueIndex = (int) VariantGetNumber(file);

		  if (queueIndex < (int) m_tQueue.size()){
			   DEBUGMSG(ftp, "erasing element %d\n", queueIndex);
			   deque<tRequest *>::iterator a = m_tQueue.begin();
			   a += queueIndex;
			   delete *a;
			   m_tQueue.erase(a);
			   elementFound = true;
		  }
	 } else {
		  // not a string or a num.
		  DEBUGMSG(ftp, 
				   "Can only pass a num or a string to RemoveFromQueue\n");
		  unlockTransferQueue();
		  return NS_ERROR_FAILURE;
	 }
	 unlockTransferQueue();
	 *_retval = elementFound;
	 return NS_OK;
}

/* readonly attribute nsIVariant transferQueue; */
NS_IMETHODIMP sashFTPConnection::GetTransferQueue(nsIVariant * *aTransferQueue)
{
	 DEBUGMSG(ftp, "GetTransferQueue...\n");

	 if (NewVariant(aTransferQueue) != NS_OK){
		  return NS_ERROR_FAILURE;
	 }

	 lockTransferQueue();
	 vector <string> strvec;
	 deque <tRequest*>::iterator ai = m_tQueue.begin(),
		  bi = m_tQueue.end();
	 while (ai != bi){
		  strvec.push_back((*ai)->filename);
		  ++ai;
	 }
	 DEBUGMSG(ftp, "%d items on the transferqueue\n", 
			  strvec.size());
	 VariantSetArray (*aTransferQueue, strvec);
	 unlockTransferQueue();

	 return NS_OK;
}

/* readonly attribute PRBool isTransferring; */
NS_IMETHODIMP sashFTPConnection::GetIsTransferring(PRBool *aIsTransferring)
{
	 *aIsTransferring = isCurrTransfer();
	 return NS_OK;
}


bool sashFTPConnection::isCurrTransfer(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_currTransfer;
	 if (lock) unlockConnection();
	 return retval;
}

void sashFTPConnection::setCurrTransfer(bool val, const string& currfilename,
										bool lock){
	 if (lock) lockConnection();
	 m_currTransfer = val;
	 m_currTransferFile = currfilename;
	 if (lock) unlockConnection();
}

bool sashFTPConnection::isCurrTransferGet(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_currTransferIsGet;
	 if (lock) unlockConnection();
	 return retval;
}

void sashFTPConnection::setCurrTransferGet(bool val, bool lock){
	 if (lock) lockConnection();
	 m_currTransferIsGet = val;
	 if (lock) unlockConnection();
}

bool sashFTPConnection::shouldStopTransfer(bool lock){
	 bool retval;
	 if (lock) lockConnection();
	 retval = m_stopTransfer;
	 if (lock) unlockConnection();
	 return retval;
}

void sashFTPConnection::setStopTransfer(bool val, bool lock){
	 if (lock) lockConnection();
	 m_stopTransfer = val;
	 if (lock) unlockConnection();
}

/* PRBool cd (in string directory); */
NS_IMETHODIMP sashFTPConnection::Cd(const char *directory, PRBool *_retval)
{
	 if (ready()){
		  *_retval = FTPCommandSuccess("cwd", directory);
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* nsIVariant list (in nsIVariant dirName); */
NS_IMETHODIMP sashFTPConnection::List(nsIVariant *dirName, 
									  nsIVariant **_retval)
{
	 if (ready()){
		  if(! genericList("nlst", dirName, _retval)){
			   return NS_ERROR_FAILURE;
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* nsIVariant detailedlist (in nsIVariant dirName); */
NS_IMETHODIMP sashFTPConnection::DetailedList(nsIVariant *dirName, nsIVariant **_retval)
{
	 if (ready()){
		  if(! genericList("list", dirName, _retval)){
			   return NS_ERROR_FAILURE;
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

bool sashFTPConnection::genericList(const char *dirCommand,
									nsIVariant *dirName, 
									nsIVariant **_retval){

	 int dataSockFD = getDataSockFD();
	 string status, s;

	 if (dataSockFD == -1){
		  DEBUGMSG(ftp, "error opening data connection!\n");
		  s = "Error opening data connection.\n";
		  nullVariant(_retval);
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, s);
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onDefaultError, (sashIFTPConnection *)this, s);
		  return true;
	 }

	 streamBuffer sb;

	 const char *dirargs = NULL;
	 if ((! m_showDotFiles)){
		  DEBUGMSG(ftp, "show dot files off\n");
		  if (! VariantIsEmpty(dirName)){
			   dirargs = (VariantGetString(dirName).c_str());
		  } 
	 } else {
		  DEBUGMSG(ftp, "show dot files on\n");
		  if (! VariantIsEmpty(dirName)){
			   dirargs = ("-a " + VariantGetString(dirName)).c_str();
		  } else {
			   dirargs = "-a";
		  }
	 }

	 if (FTPCommandSuccess(dirCommand, dirargs)){
		  int received = receiveAllFromConnection(m_dataSockFD, &sb);
		  close (m_dataSockFD);

		  DEBUGMSG(ftp, "received %d bytes: \n", received);

		  if (received == -1){
			   s = "Error receiving on data connection.\n";
			   nullVariant(_retval);
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onStatusMessage, (sashIFTPConnection *)this, s);
			   ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx, m_onDefaultError, (sashIFTPConnection *)this, s);
			   return true;
		  }

		  if (! streamBufferToStringArray(&sb, _retval)){
			   // this means that a NewSashVariant call failed.
			   return false;
		  }

		  receiveFTPResponse(m_mainSockFD, status);
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);
	 } else {
		  // if it returns 550, then there are no files. 
		  // close the data connection
		  close (m_dataSockFD);
		  vector <string> strvec;
		  NewVariant (_retval);
		  VariantSetArray(*_retval, strvec);
	 }

	 return true;
}

/* PRBool rename (in string oldFileName, in string newFileName); */
NS_IMETHODIMP sashFTPConnection::Rename(const char *oldFileName, const char *newFileName, PRBool *_retval)
{
	 if (ready()){
		  if (! FTPCommandSuccess("rnfr", oldFileName)){
			   *_retval = false;
		  } else {
			   *_retval = FTPCommandSuccess("rnto", newFileName);
		  }
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* PRBool delete (in string removeFileName); */
NS_IMETHODIMP sashFTPConnection::Del(const char *removeFileName, PRBool *_retval)
{
	 if (ready()){
		  *_retval = FTPCommandSuccess("dele", removeFileName);
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* PRBool mkdir (in string remoteDir); */
NS_IMETHODIMP sashFTPConnection::Mkdir(const char *remoteDir, PRBool *_retval)
{
	 if (isConnected() && isLoggedIn()){
		  *_retval = FTPCommandSuccess("mkd", remoteDir);
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* PRBool rmdir (in string remoteDir); */
NS_IMETHODIMP sashFTPConnection::Rmdir(const char *remoteDir, PRBool *_retval)
{
	 if (ready()){
		  *_retval = FTPCommandSuccess("rmd", remoteDir);
	 } else {
		  DEBUGMSG(ftp, "No action: not connected or not logged in or file transfer in progress.\n");
		  *_retval = false;
	 }
	 return NS_OK;
}

/* PRBool StopCurrentTransfer (); */
NS_IMETHODIMP sashFTPConnection::StopCurrentTransfer(PRBool *_retval)
{
	 if (isCurrTransfer()) {
		  setStopTransfer(true);
		  while (shouldStopTransfer()){
			   waitForStop();
		  }
		  *_retval = true;
	 } else {
		  *_retval = false;
	 }
	 return NS_OK;
}

/* attribute string OnFileSendStart; */
NS_IMETHODIMP sashFTPConnection::GetOnFileSendStart(char * *aOnFileSendStart)
{
	 XP_COPY_STRING(m_onFileSendStart.c_str(), aOnFileSendStart);
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetOnFileSendStart(const char * aOnFileSendStart)
{
	 m_onFileSendStart = aOnFileSendStart;
	 return NS_OK;

}

/* attribute string OnFileSendStart; */
NS_IMETHODIMP sashFTPConnection::GetOnFileReceiveStart(char * *aOnFileReceiveStart)
{
	 XP_COPY_STRING(m_onFileReceiveStart.c_str(), aOnFileReceiveStart);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnFileReceiveStart(const char * aOnFileReceiveStart)
{
	 m_onFileReceiveStart = aOnFileReceiveStart;
	 return NS_OK;

}

/* attribute string OnFileSent; */
NS_IMETHODIMP sashFTPConnection::GetOnFileSent(char * *aOnFileSent)
{
	 XP_COPY_STRING(m_onFileSent.c_str(), aOnFileSent);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnFileSent(const char * aOnFileSent)
{
	 m_onFileSent = aOnFileSent;
	 return NS_OK;
}

/* attribute string OnFileReceived; */
NS_IMETHODIMP sashFTPConnection::GetOnFileReceived(char * *aOnFileReceived)
{
	 XP_COPY_STRING(m_onFileReceived.c_str(), aOnFileReceived);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnFileReceived(const char * aOnFileReceived)
{
	 m_onFileReceived = aOnFileReceived;
	 return NS_OK;
}

/* attribute string OnFileSendError; */
NS_IMETHODIMP sashFTPConnection::GetOnFileSendError(char * *aOnFileSendError)
{
	 XP_COPY_STRING(m_onFileSendError.c_str(), aOnFileSendError);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnFileSendError(const char * aOnFileSendError)
{
	 m_onFileSendError = aOnFileSendError;
	 return NS_OK;
}

/* attribute string OnFileReceiveError; */
NS_IMETHODIMP sashFTPConnection::GetOnFileReceiveError(char * *aOnFileReceiveError)
{
	 XP_COPY_STRING(m_onFileReceiveError.c_str(), aOnFileReceiveError);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnFileReceiveError(const char * aOnFileReceiveError)
{
	 m_onFileReceiveError = aOnFileReceiveError;
	 return NS_OK;
}

/* attribute string OnDefaultError; */
NS_IMETHODIMP sashFTPConnection::GetOnDefaultError(char * *aOnDefaultError)
{
	 XP_COPY_STRING(m_onDefaultError.c_str(), aOnDefaultError);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnDefaultError(const char * aOnDefaultError)
{
	 m_onDefaultError = aOnDefaultError;
	 return NS_OK;
}

/* attribute string OnFatalError; */
NS_IMETHODIMP sashFTPConnection::GetOnFatalError(char * *aOnFatalError)
{
	 XP_COPY_STRING(m_onFatalError.c_str(), aOnFatalError);
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetOnFatalError(const char * aOnFatalError)
{
	 m_onFatalError = aOnFatalError;
	 return NS_OK;
}

/* attribute string OnProgress; */
NS_IMETHODIMP sashFTPConnection::GetOnProgress(char * *aOnProgress)
{
	 XP_COPY_STRING(m_onProgress.c_str(), aOnProgress);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnProgress(const char * aOnProgress)
{
	 m_onProgress = aOnProgress;
	 return NS_OK;
}

/* attribute string OnStatusMessage; */
NS_IMETHODIMP sashFTPConnection::GetOnStatusMessage(char * *aOnStatusMessage)
{
	 XP_COPY_STRING(m_onStatusMessage.c_str(), aOnStatusMessage);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnStatusMessage(const char * aOnStatusMessage)
{
	 m_onStatusMessage = aOnStatusMessage;
	 return NS_OK;
}

/* attribute string OnLogin; */
NS_IMETHODIMP sashFTPConnection::GetOnLogin(char * *aOnLogin)
{
	 XP_COPY_STRING(m_onLogin.c_str(), aOnLogin);
	 return NS_OK;
}
NS_IMETHODIMP sashFTPConnection::SetOnLogin(const char * aOnLogin)
{
	 m_onLogin = aOnLogin;
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::GetOnTimeout(char * *aOnTimeOut)
{
	 XP_COPY_STRING(m_onTimeout.c_str(), aOnTimeOut);
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetOnTimeout(const char * aOnTimeOut)
{
	 m_onTimeout = aOnTimeOut;
	 return NS_OK;

}

NS_IMETHODIMP sashFTPConnection::GetOnConnectionTimeout(char * *aOnTimeOut)
{
	 XP_COPY_STRING(m_onConnectionTimeout.c_str(), aOnTimeOut);
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetOnConnectionTimeout(const char * aOnTimeOut)
{
	 m_onConnectionTimeout = aOnTimeOut;
	 return NS_OK;

}

// only returns false if NewVariant calls are unsuccessful.
bool sashFTPConnection::streamBufferToStringArray(streamBuffer *sb,
												  nsIVariant **v){
	 assert (sb);

	 vector <string> flisting;

	 // create a new array.
	 if (NewVariant (v) != NS_OK){
		  return false;
	 }

	 // read through, line by line.
	 // each line after that contains a filename.
	 int indexOfNL = sb->findInBuffer('\n');
	 char currline[LIST_BUFSIZE + 1];

	 while(indexOfNL != -1){
		  assert (indexOfNL + 1 < LIST_BUFSIZE);
		  sb->getData(indexOfNL + 1, currline);
		  currline[indexOfNL] = '\0';
		  DEBUGMSG(ftp, "currstr: %s\n", currline);
		  flisting.push_back(currline);
		  indexOfNL = sb->findInBuffer('\n');
	 }
	 
	 VariantSetArray(*v, flisting);
	 return true;
}

bool sashFTPConnection::nullVariant(nsIVariant **v){
	 // create a new array.
	 if (NewVariant (v) != NS_OK){
		  return false;
	 }

	 VariantSetEmpty(*v);
	 return true;
}
 
bool sashFTPConnection::emptyTransferQueue(bool lock){
	 bool retval;
	 if (lock) lockTransferQueue();
	 retval = m_tQueue.empty();
	 if (lock) unlockTransferQueue();
	 return retval;
}

bool sashFTPConnection::FTPCommandSuccess(
					   const char *ftpCommand,
					   const char *argstr){
	 string status;
	 sendNormalFTPCommand (m_mainSockFD, ftpCommand, argstr);
	 int responseCode = receiveFTPResponse(m_mainSockFD, status);
	 if (responseCode > 0)
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onStatusMessage, (sashIFTPConnection *)this, status);
	 return (processFTPResponseCode(responseCode));
}


int sashFTPConnection::sendNormalFTPCommand (int sockfd, 
											 const char *ftpCommand, 
											 const char *argstr){
	 assert (sockfd != -1);
	 assert (ftpCommand);

	 char *buf;
	 if ((argstr == NULL) || (argstr[0] == '\0')){
		  int len = strlen(ftpCommand);
		  buf = new char [len + 2];
		  memcpy (buf, ftpCommand, len);
		  buf[len] = '\n';

		  buf[len+1] = '\0';

	 } else{
		  buf = new char [strlen(ftpCommand) + strlen(argstr) + 3];
		  sprintf(buf, "%s %s\n", ftpCommand, argstr);
	 }

	 if (strcmp(ftpCommand, "pass") != 0)
		  DEBUGMSG(ftp, "sending command: %s\n", buf);

	 assert (sockfd != -1);
	 if (fullSend(sockfd, strlen(buf), buf) == -1){
		  delete [] buf;
		  return -1;
	 }
	 
	 delete [] buf;

	 return 1;
}

int sashFTPConnection::receiveFTPResponse (int sockfd, string& buff, 
										   bool isNormalCommand){
	 int timeoutsec;
	 if (isNormalCommand) timeoutsec = m_normalTimeout;
	 else timeoutsec = m_connectionTimeout;

	 assert (sockfd != -1);

	 // it's possible that there's already a full response in here. 
	 // receive until '\n' for single line messages
	 // use select to wait until there's data
	 // keep receiving while there's still data
	 // if there's a multi-line message, or you haven't read a newline
	 // yet, then you still need to receive more data

	 int indexOfNL;

	 char buf[LINE_BUFSIZE + 1];
	 char returnCode[4];
	 char * currline;

	 streamBuffer holdbuf;

	 int received;
	 int previousRCode = -1;
	 bool isDone = false;

	 fd_set readfds;
	 FD_ZERO(&readfds);	 

	 struct timeval timeout;
	 timeout.tv_sec = timeoutsec;
	 timeout.tv_usec = 0;

	 while (! isDone){
		  indexOfNL = m_ftpResponses->findInBuffer('\n');

		  // receive data until you get a newline.
		  while (indexOfNL == -1){
			   // don't wait for too long. right now the timeout
			   // is only being used to abort transfers.
			   if (timeoutsec > 0){
					FD_ZERO(&readfds);	 
					FD_SET(sockfd, &readfds);
					timeout.tv_sec = timeoutsec;
					timeout.tv_usec = 0;
					
					DEBUGMSG(ftp, "select, timeout: %d sec...\n",
							 (int)timeout.tv_sec);
					if (select(sockfd+1, &readfds, NULL, NULL, 
							   &timeout) == -1){
						 perror("select");
						 return -1;
					} 
					DEBUGMSG(ftp, "done with select...\n");
					if (! FD_ISSET(sockfd, &readfds)){
						 DEBUGMSG(ftp,
								  "timed out waiting for a response (%d sec), calling event\n", timeoutsec);
						 m_timedOut = true;
						 if (isNormalCommand){
							  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
								   m_cx, m_onTimeout, (sashIFTPConnection *)this);
						 } else {
							  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
								   m_cx, m_onConnectionTimeout, (sashIFTPConnection *)this);

						 }
						 
						 return -2;
					} else {
						 DEBUGMSG(ftp, "select: (sockfd: %d)", sockfd);
						 for (int j=0; j<= sockfd; j++){
							  if (FD_ISSET(j, &readfds)){
								   DEBUGMSG(ftp, "%d, ", j);
							  }
						 }
						 DEBUGMSG(ftp, "\n");
					}
			   }


			   received = recv(sockfd, buf, LINE_BUFSIZE, 0);
			   buf[received] = '\0';
			   if (received > 0){
					m_ftpResponses->appendData(received, buf);
					DEBUGMSG(ftp, "fd%d: %d bytes received: %s\n",
							 sockfd, received, buf);
			   } else if (received == 0){
					DEBUGMSG(ftp, "0 bytes received..\n");
					DEBUGMSG(ftp, "other side closed connection\n");
					return -1;
			   } else if (received == -1){
					DEBUGMSG(ftp, "error receiving ftp response: %s\n", strerror(errno));
					return -1;
			   }
			   indexOfNL = m_ftpResponses->findInBuffer('\n');
		  }

		  // keep receiving until you've got at least one newline.
		  // at that point, process all whole lines, one at a time.
		  while (indexOfNL != -1){
			   DEBUGMSG(ftp, "size allocated for currline: %d\n",
						indexOfNL + 2);
			   currline = new char [indexOfNL + 2];
			   assert (currline);

			   m_ftpResponses->getData(indexOfNL + 1, currline);
			   currline[indexOfNL + 1] = '\0';
			   holdbuf.appendData(indexOfNL + 1, currline);
			   
			   DEBUGMSG(ftp, "currline: %s\n", currline);

			   // with the current line, there are several possibilities.
			   // the line could be a one-line response
			   // the line could be the beginning of a multi-line response
			   // the line could be in the middle of a multi-line response
			   // the line could be at the end of a multi-line response

			   // not the first line of a multi-line response
			   if (previousRCode != -1){
					if ((returnCode[0] == currline[0]) 
						&& (returnCode[1] == currline[1]) 
						&& (returnCode[2] == currline[2]) 
						&& (currline[3] == ' ')){
						 // this is the last line of the multi-line response
						 // all done.
						 DEBUGMSG(net, "last line of multiline responses\n");
						 indexOfNL = -1;
						 isDone = true;

					} else {
						 // this isn't the last line of the multi-line response
						 // ignore it and keep going
						 DEBUGMSG(net, "not the last line of multiline respones\n");
						 indexOfNL = m_ftpResponses->findInBuffer('\n');
					}
			   } else {
					// this is the first line
					memcpy(returnCode, currline, 3);
					returnCode[3] = '\0';

					// if it's a multi-line response, you want it to 
					// get more data.
					if (currline[3] == '-'){
						 DEBUGMSG(net, "first line of multi-line response\n");
						 indexOfNL = m_ftpResponses->findInBuffer('\n');
						 previousRCode = atoi(returnCode);
					} else {
						 assert(currline[3] == ' ');
						 DEBUGMSG(net, "single-line response, %s\n",
								  returnCode);
						 indexOfNL = -1;
						 isDone = true;
					}
			   }
			   delete [] currline;
		  }
	 }

	 // get everything out of holdbuf into a string
	 holdbuf.getString(-1, buff);

	 int retval = atoi(returnCode);

	 return retval;
}

int sashFTPConnection::parsePASVResponse(const string& str, 
										 struct sockaddr_in *dataAddr){
	 // match all the way to the '(';
	 int leftparen = str.find('(');
	 int rightparen = str.find(')');
	 if ((leftparen == -1) || (rightparen == -1)) {
		  DEBUGMSG(net, "PASV response not well formed\n");
		  return -1;
	 }
	 string substr = str.substr(leftparen, rightparen-leftparen+1);
	 DEBUGMSG(net, "substr: %s\n", substr.c_str());
	 const char * buf = substr.c_str();
	 char buf2[LINE_BUFSIZE];

	 int a,b,c,d,e,f;
	 sscanf((char*)buf, "(%d, %d, %d, %d, %d, %d)", &a,&b,&c,&d,&e,&f);

	 sprintf(buf2, "%d.%d.%d.%d", a,b,c,d);

	 DEBUGMSG(net, "address: %s, port %d, %d\n", buf2,
			  e, f);
	 
	 if (dataAddr) setRemoteAddr(dataAddr, buf2, 256 * e + f);

	 return 1;
}

bool sashFTPConnection::processFTPResponseCode(int responseCode){
	 if (responseCode == -1){
		  // Transfer error somewhere. shut down everything?
		  DEBUGMSG(ftp, "socket error. reinitializing connection.\n");
		  reinit();
		  string s = "Fatal error. Connection Reset.";
		  ((sashFTP *)m_ext)->m_eventQueue->addGenericEvent(
			   m_cx, m_onFatalError, (sashIFTPConnection *)this, s);
		  return false;
	 } else if (responseCode == -2){
		  DEBUGMSG(ftp, "No response: timed out.\n");
		  return false;
	 }

	 bool retval = true;

	 int firstDigit = (int) responseCode / 100;
	 int secondDigit = (int) (responseCode - firstDigit * 100) / 10;
//	 int thirdDigit = (int) (responseCode - firstDigit * 100 - secondDigit * 10);


	 switch (firstDigit){
	 case FIRST_PRELIM_POSITIVE:
		  DEBUGMSG(ftp, "prelim positive\n");
		  break;
	 case FIRST_COMPLETE_POSITIVE:
		  DEBUGMSG(ftp, "complete positive\n");
		  break;
	 case FIRST_INTERMEDIATE_POSITIVE:
		  DEBUGMSG(ftp, "intermediate positive\n");
		  break;
	 case FIRST_TRANSIENT_NEGATIVE:
	 case FIRST_PERMANENT_NEGATIVE:
		  retval = false;
		  // if there's some sort of error, 
		  switch (secondDigit){
		  case SECOND_SYNTAX:
			   DEBUGMSG(ftp, "syntax error\n");
			   break;
		  case SECOND_INFORMATION:
			   break;

		  case SECOND_CONNECTION:
			   DEBUGMSG(ftp, "connection error\n");
			   break;
		  case SECOND_AUTHENTICATION:
			   DEBUGMSG(ftp, "authentication error\n");
			   break;
		  case SECOND_FILESYSTEM:
			   DEBUGMSG(ftp, "filesystem error\n");
			   break;
		  }
		  break;
	 }
	 return retval;
}

/*
	 DEBUGMSG(ftp, "test return code stuff\n");
	 string *str;
	 if (processFTPResponseCode(226, &str)){
		  DEBUGMSG(ftp, "successful!\n");
	 }else{
		  DEBUGMSG(ftp,"unsuccessful: %s\n", str->c_str());
	 }

	 if (processFTPResponseCode(530, &str)){
		  DEBUGMSG(ftp, "successful!\n");
	 }else{
		  DEBUGMSG(ftp,"unsuccessful: %s\n", str->c_str());
	 }
*/

// Only PASV works.
int sashFTPConnection::getDataSockFD(){
	 if (isPassive()){
		  // can't use FTPCommandSuccess here because we need to save
		  // the return string so that we can grab the data port number

		  DEBUGMSG(ftp, "getdatasockfd pasv\n");
		  sendNormalFTPCommand(m_mainSockFD, "pasv", NULL);
		  string status;
		  int responseCode = receiveFTPResponse(m_mainSockFD, status);

		  if (! processFTPResponseCode(responseCode)){
			   DEBUGMSG(ftp, "pasv response error\n");
			   return -1;
		  }

		  lockConnection();
		  parsePASVResponse(status, &m_dataAddr);
		  int dataSockFD = m_dataSockFD = openConnection(SOCK_STREAM, 
														 &m_dataAddr);
		  unlockConnection();
		  return dataSockFD;
	 } else {
		  // non-passive: use PORT instead.
		  return -1;
	 }
}

// unused.
int sashFTPConnection::formPORTCommand(sockaddr_in *dataAddr){
	 DEBUGMSG(ftp, "port: %d\n", ntohs(dataAddr->sin_port));
	 return -1;
}
