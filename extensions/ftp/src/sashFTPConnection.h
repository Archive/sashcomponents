
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the FTP Connection object 
******************************************************************/
#ifndef SASHFTPCONNECTION_H
#define SASHFTPCONNECTION_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIFTPConnection.h"
#include "sashIExtension.h"
#include "sashIConstructor.h"
#include "sashISecurityManager.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string>
#include <deque>
#include <stdio.h>
#include "debugmsg.h"
#include "streamBuffer.h"
#include "sashVariantUtils.h"
#include "sashFTP.h"
#include "event.h"

// (11835ff0-a9b9-464a-b580-8e3910f96004)
#define SASHFTPCONNECTION_CID {0x11835ff0, 0xa9b9, 0x464a, {0xb5, 0x80, 0x8e, 0x39, 0x10, 0xf9, 0x60, 0x04}}

NS_DEFINE_CID(ksashFTPConnectionCID, SASHFTPCONNECTION_CID);

#define SASHFTPCONNECTION_CONTRACT_ID "@gnome.org/SashXB/FTP/sashftpconnection;1"

#define DEFAULT_NORMAL_TIMEOUT 5
#define DEFAULT_CONNECTION_TIMEOUT 15


typedef struct{
	 bool isGet;
	 string filename;
	 string newfilename;
} tRequest;

class sashFTPConnection : public sashIFTPConnection,
						  public sashIConstructor
{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHICONSTRUCTOR;
	 NS_DECL_SASHIFTPCONNECTION;

	 sashFTPConnection();
	 virtual ~sashFTPConnection();

	 bool start();
	 void run();
	 bool m_running;

protected:
	 void reinit();

	 void waitForSignal();
	 void sendSignal();

	 void waitForStop();
	 void stoppedSignal();

	 pthread_mutex_t m_connectionLock;
	 pthread_t m_transferFiles;

	 pthread_mutex_t m_idleLock; 
	 pthread_cond_t m_idleCond;

	 pthread_mutex_t m_stopLock; 
	 pthread_cond_t m_stopCond;

	 pthread_mutex_t m_transferLock; 
	 deque<tRequest*> m_tQueue;

	 void lockConnection(){
//		  DEBUGMSG(ftp, "locking connection...\n");
		  pthread_mutex_lock(&m_connectionLock);
//		  DEBUGMSG(ftp, "locked connection...\n");
	 }
	 void unlockConnection(){
//		  DEBUGMSG(ftp, "unlocking connection...\n");
		  pthread_mutex_unlock(&m_connectionLock);
//		  DEBUGMSG(ftp, "unlocked connection...\n");
	 }

	 void lockTransferQueue(){
//		  DEBUGMSG(ftp, "locking tranfer queue...\n");
		  pthread_mutex_lock(&m_transferLock);
//		  DEBUGMSG(ftp, "locked transfer queue...\n");
	 }
	 void unlockTransferQueue(){
//		  DEBUGMSG(ftp, "unlocking transfer queue...\n");
		  pthread_mutex_unlock(&m_transferLock);
//		  DEBUGMSG(ftp, "unlocked transfer queue...\n");
	 }

	 bool ready(bool lock = true);

	 bool isConnected(bool lock = true);
	 void setConnected(bool val, bool lock = true);

	 bool isTimedOut(bool lock = true);
	 void setTimedOut(bool val, bool lock = true);

	 bool isLoggedIn(bool lock = true);
	 void setLoggedIn(bool val, bool lock = true);

	 bool isLoginReady(bool lock = true);
	 void setLoginReady(bool val, bool lock = true);

	 bool isCurrTransfer(bool lock = true);
	 void setCurrTransfer(bool val, const string& currfilename,
						  bool lock = true);

	 bool isCurrTransferGet(bool lock = true);
	 void setCurrTransferGet(bool val, bool lock = true);

	 bool shouldStopTransfer(bool lock = true);
	 void setStopTransfer(bool val, bool lock = true);

	 bool emptyTransferQueue(bool lock = true);

	 JSContext *m_cx;
	 sashIExtension *m_ext;
	 sashIActionRuntime *m_act;

	 bool m_connected;
	 bool m_timedOut;
	 bool m_loggedIn;
	 bool m_loginReady;

	 bool m_currTransfer;
	 string m_currTransferFile;
	 bool m_currTransferIsGet;
	 bool m_stopTransfer;

	 int m_remotePort;
	 struct sockaddr_in m_remoteAddr;

	 int m_mainSockFD;
	 struct sockaddr_in m_dataAddr;
	 int m_dataSockFD;

	 int m_notifyIntervalK;
	 int m_notifyIntervalS;
	 int m_dataType;
	 bool m_showDotFiles;

	 string m_id;

	 int m_normalTimeout;
	 int m_connectionTimeout;

	 // sending commands and receiving responses.
	 // for now, only support passive FTP.
	 streamBuffer *m_ftpResponses;
	 bool FTPCommandSuccess(const char *ftpCommand,
							const char *argstr);

	 int sendNormalFTPCommand (int sockfd, 
							   const char *ftpCommand, 
							   const char *argstr);
	 int receiveFTPResponse (int sockfd, string& buff, bool isNormalCommand = true);
	 bool processFTPResponseCode(int responseCode);
	 int parsePASVResponse(const string& str, 
						   struct sockaddr_in *dataAddr);
	 
	 // passive/active data port
	 bool m_isPassive;
	 bool isPassive();
	 int getDataSockFD();
	 int formPORTCommand(sockaddr_in *m_dataAddr);

	 // login.
	 std::string m_username;
	 std::string m_password;

	 // get and put.
	 int getSingleFile(const string& rfileName, const string& lfileName);
	 int putSingleFile(const string& lfileName, const string& rfileName);
	 int receiveIntoFile(int dataSockFD, 
						 FILE* filefd,
						 const string& rfileName,
						 int *totalTime);
	 int sendFromFile(int dataSockFD, 
					  FILE* filefd,
					  const string& lfileName,
					  int *totalTime);
	 // file transfer requests
	 int createRequest(tRequest *req, 
					   bool isGet,
					   const string &filename, 
					   nsIVariant *filenameopt);
	 
	 // list commands and javascript array constructors.
	 bool genericList(const char *dirCommand,
					  nsIVariant *dirName, 
					  nsIVariant **_retval);
	 
	 bool streamBufferToStringArray(streamBuffer *sb,
									nsIVariant **v);
	 bool nullVariant(nsIVariant **v);
	 
	 // callbacks
	 string m_onFileSendStart;
	 string m_onFileReceiveStart;
	 string m_onFileSent;
	 string m_onFileReceived;
	 string m_onFileSendError;
	 string m_onFileReceiveError;
	 string m_onDefaultError;
	 string m_onProgress;
	 string m_onStatusMessage;
	 string m_onLogin;
	 string m_onFatalError;
	 string m_onTimeout;
	 string m_onConnectionTimeout;
};

#endif
