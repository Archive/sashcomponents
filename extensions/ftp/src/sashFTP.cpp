
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation of the FTP extension
******************************************************************/

#include "sashFTP.h"
#include "sashFTPConnection.h"

#include "sash_constants.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIGenericConstructor.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include <unistd.h>
#include "secman.h"

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>

SASH_EXTENSION_IMPL_NO_NSGET(sashFTP, sashIFTP, "FTP");

sashFTP::sashFTP() : m_FTPConnectionConstructor(NULL)
{
	 NS_INIT_ISUPPORTS();
	 DEBUGMSG(ftp, "sashFTP: Creating FTP extension\n");
}

sashFTP::~sashFTP()
{
	 NS_IF_RELEASE(m_FTPConnectionConstructor);
	 if (m_eventQueue) delete m_eventQueue;
}

NS_IMETHODIMP
sashFTP::Initialize(sashIActionRuntime *act, 
			   const char *registrationXml, 
			   const char *webGuid,
			   JSContext *cx, JSObject *obj)
{

	 DEBUGMSG(ftp, "sashFTP::Initialize\n");

	 act->GetSecurityManager(&m_secMan);
	 m_cx = cx;
	 m_rt = act;

	 NewSashConstructor(m_rt, this, SASHFTPCONNECTION_CONTRACT_ID, 
						SASHIFTPCONNECTION_IID_STR, &m_FTPConnectionConstructor);

	 m_eventQueue = new eventQueue(act, this);
	 return NS_OK;
}

NS_IMETHODIMP
sashFTP::Cleanup(){
	 DEBUGMSG(ftp, "sashFTP:: cleaning up FTP\n");
	 return NS_OK;
}

NS_IMETHODIMP
sashFTP::ProcessEvent()
{
	 m_eventQueue->callAllEvents();
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor Connection; */
NS_IMETHODIMP sashFTP::GetConnection(sashIGenericConstructor * *aConnection)
{
	 DEBUGMSG(ftp, "getting new ftp connection\n");

	 assert (m_FTPConnectionConstructor);
 	 AssertSecurity(m_secMan, ToUpper(ksashFTPCID.ToString()), 1, true);
	 NS_ADDREF(m_FTPConnectionConstructor);
	 *aConnection = m_FTPConnectionConstructor;
	 return NS_OK;
}

/* attribute string onError; */
NS_IMETHODIMP sashFTP::GetOnError(char * *aOnError)
{
	 XP_COPY_STRING(m_onError.c_str(), aOnError);
	 return NS_OK;
}
NS_IMETHODIMP sashFTP::SetOnError(const char * aOnError)
{
	 m_onError = aOnError;
	 return NS_OK;
}
