
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for sashnet: simple socket calls

******************************************************************/

#include "sashnet.h"
#include <strstream>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include "debugmsg.h"
#include "streamBuffer.h"
#include <errno.h>

void setRemoteAddr(struct sockaddr_in *remoteAddr,
				   const char *remoteHost, unsigned short remotePort){
	 struct hostent *h;
	 ostrstream o;
	 if ((h=gethostbyname(remoteHost)) == NULL){
		  o << '\0';
	 }
	 else{
		  if (h->h_addr != NULL){
			   o << inet_ntoa(*((struct in_addr *)h->h_addr)) << '\0';
		  } else {
			   // not sure if this is covered by the first if condition.
			   o << '\0';
		  }
	 }
	 
	 DEBUGMSG(net, "setting remote address: %s, port %d\n", 
			  o.str(), remotePort);

	 remoteAddr->sin_family = AF_INET;
	 remoteAddr->sin_port = htons(remotePort);
	 remoteAddr->sin_addr.s_addr = inet_addr(o.str());
	 memset(&(remoteAddr->sin_zero), '\0', sizeof(remoteAddr->sin_zero));
}

bool bindSocket(int sockfd, int port){
	 struct sockaddr_in my_addr;
	 my_addr.sin_family = AF_INET;
	 my_addr.sin_port = htons(port);
	 my_addr.sin_addr.s_addr = INADDR_ANY;
	 memset(&(my_addr.sin_zero), '\0', sizeof(my_addr.sin_zero));
	 
	 if (bind(sockfd, (struct sockaddr *)&my_addr, 
			  sizeof(struct sockaddr)) == -1){
		  perror("bind");
		  return false;
	 }
	 return true;
}

int fullSend(int sockfd, int size, char * data){
	 int amtLeft = size;
	 int amtSent = 0;
	 while (amtLeft > 0){
		  amtSent = send(sockfd, data, size, 0);
		  if (amtSent == -1) {
			   perror("send");
			   return -1;
		  } else {
			   amtLeft -= amtSent;
		  }
	 }
	 return size; 
}

int openConnection(int sockType, struct sockaddr_in *remoteAddr){
	 // create a new socket
	 int sockfd = socket (AF_INET, sockType, 0);
	 DEBUGMSG(net, "socket call successful (fd%d).\n", sockfd);

	 // connect to the new port
	 if (connect(sockfd, 
				 (struct sockaddr *)remoteAddr, 
				 sizeof(struct sockaddr)) == -1){
		  DEBUGMSG(net, "error connecting to port %d: %s\n", ntohs(remoteAddr->sin_port), strerror(errno));
		  return -1;
	 }
	 DEBUGMSG(net, "connect call successful.\n");
	 return sockfd;
}

int receiveAllFromConnection(int dataSockFD, streamBuffer *strbuf){
	 char buf[RECEIVE_BUFSIZE+1];

	 // receive until connection closed
	 int totalAmtReceived =0;
	 int amtReceived=1;
	 while(amtReceived > 0){
		  amtReceived = recv(dataSockFD, buf, RECEIVE_BUFSIZE, 0);

		  if (amtReceived > 0){
			   strbuf->appendData(amtReceived, buf);
			   totalAmtReceived += amtReceived;
		  } 
	 }

	 if (amtReceived == -1) {
		  perror("recv");
		  return -1;
	 }
	 return totalAmtReceived;
}

int bindLocal(int sockType, int port, sockaddr_in *myAddr){
	 int sockfd;
	 if ((sockfd = socket(AF_INET, sockType, 0)) == -1){
		  perror("socket");
		  return -1;
	 }
	 
	 myAddr->sin_family = AF_INET;
	 myAddr->sin_port = htons(port);
	 myAddr->sin_addr.s_addr = INADDR_ANY;
	 memset(&(myAddr->sin_zero), '\0', sizeof(myAddr->sin_zero));
	 
	 if (bind(sockfd, (struct sockaddr *)myAddr, 
			  sizeof(struct sockaddr)) == -1){
		  perror("bind");
		  return -1;
	 }

	 DEBUGMSG(net, "bound port: %d\n", ntohs(myAddr->sin_port));
	 return sockfd;
}

