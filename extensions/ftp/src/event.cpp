
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation for the event class

******************************************************************/

#include "event.h"
#include <deque>

eventQueue::eventQueue(sashIActionRuntime *act, sashIExtension *ext){
	 pthread_mutex_init(&m_queueLock, NULL);
	 m_rt = act;
	 m_ext = ext;
	 m_acceptNewEvents = true;
}
eventQueue::~eventQueue(){
	 pthread_mutex_destroy(&m_queueLock);
}

genericEvent *eventQueue::nextEvent(bool lock){
	 if (lock) lockEventQueue();
	 genericEvent *e = m_queue.front();
	 m_queue.pop_front();
	 if (lock) unlockEventQueue();
	 return e;
}

void eventQueue::callNextEvent (bool lock){
	 genericEvent *e = nextEvent(lock);

	 assert(e);
	 assert(e->cx);
	 assert(! e->callback.empty());

	 DEBUGMSG(ftp, "calling event %s (%d args) with cx %p...\n", e->callback.c_str(),
			  e->args.size(), e->cx);
	 CallEvent(m_rt, e->cx, 
			   e->callback.c_str(),
			   (e->args));

	 DEBUGMSG(ftp, "success\n");
	 delete (e);
}

bool eventQueue::emptyEventQueue(bool lock){
	 if (lock) lockEventQueue();
	 bool retval = m_queue.empty();
	 if (lock) unlockEventQueue();
	 return retval;
}

void eventQueue::addEvent(genericEvent *e, bool lock){
	 if (m_acceptNewEvents) {
		  DEBUGMSG(ftp, "trying to addEvent %s...\n",
				   e->callback.c_str());
		  if (lock) lockEventQueue();
		  m_queue.push_back(e);
		  DEBUGMSG(ftp, "%d events on the queue\n", m_queue.size());
		  if (lock) unlockEventQueue();
	 }
	 m_rt->ProcessEvent(m_ext);
}

void eventQueue::callAllEvents (bool lock){
	 while (! emptyEventQueue(lock)){
		  callNextEvent(lock);
	 }
}

bool eventQueue::addGenericEvent(JSContext *cx,
								 const string& callback,
								 const nsISupports *obj){
	 bool retval = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;

		  deque <argElement> args;

		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *) obj;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   retval = true;
		  } else {
			   delete (e);
		  }
	 }
	 return retval;
}

bool eventQueue::addLastGenericEvent(JSContext *cx,
								 const string& callback,
								 const nsISupports *obj){
	 bool retval = false;
	 m_acceptNewEvents = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;

		  deque <argElement> args;

		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *) obj;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   retval = true;
		  } else {
			   delete (e);
		  }
	 }
	 return retval;
}

bool eventQueue::addGenericEvent(JSContext *cx,
										  const string& callback,
										  const nsISupports *obj,
										  const string& message){
	 bool retval = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;

		  deque <argElement> args;

		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *) obj;
		  args.push_back(ae);

		  ae.type = TYPE_STRING;
		  ae.arg.s = message;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   retval = true;
		  } else {
			   delete (e);
		  }
	 }
	 return retval;
}

bool eventQueue::addGenericEvent(JSContext *cx,
								 const string& callback,
								 const nsISupports *obj,
								 const string& message1,
								 const string& message2){
	 bool retval = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;

		  deque <argElement> args;

		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *) obj;
		  args.push_back(ae);

		  ae.type = TYPE_STRING;
		  ae.arg.s = message1;
		  args.push_back(ae);

		  ae.type = TYPE_STRING;
		  ae.arg.s = message2;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   retval = true;
		  } else {
			   delete (e);
		  }
	 }
	 return false;
}

bool eventQueue::addGenericEvent(JSContext *cx,
							const string& callback,
							const nsISupports *obj,
							bool bval){
	 bool retval = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;
		  deque <argElement> args;

		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *)obj;
		  args.push_back(ae);

		  ae.type = TYPE_BOOL;
		  ae.arg.b = bval;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   retval = true;
		  } else {
			   delete (e);
		  }
	 }
	 return retval;
}

bool eventQueue::addGenericEvent(JSContext *cx,
								 const string& callback,
								 const nsISupports *obj,
								 const string& message,
								 double numval){
	 bool retval = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;
		  deque <argElement> args;
		  
		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *) obj;
		  args.push_back(ae);

		  ae.type = TYPE_STRING;
		  ae.arg.s = message;
		  args.push_back(ae);

		  ae.type = TYPE_NUM;
		  ae.arg.d = numval;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   retval = true;
		  } else {
			   delete (e);
		  }
	 }
	 return retval;
}

bool eventQueue::addGenericEvent(JSContext *cx,
								 const string& callback,
								 const nsISupports *obj,
								 const string& message,
								 double numval,
								 double numval2){
	 bool retval = false;
	 if (callback != ""){
		  genericEvent *e = new genericEvent;
		  deque <argElement> args;
		  
		  argElement ae;
		  ae.type = TYPE_OBJ;
		  ae.arg.o = (void *) obj;
		  args.push_back(ae);

		  ae.type = TYPE_STRING;
		  ae.arg.s = message;
		  args.push_back(ae);

		  ae.type = TYPE_NUM;
		  ae.arg.d = numval;
		  args.push_back(ae);

		  ae.type = TYPE_NUM;
		  ae.arg.d = numval2;
		  args.push_back(ae);

		  if(createGenericEvent(e, cx, callback, args)){
			   addEvent(e);
			   return true;
		  } else {
			   delete (e);
		  }
	 }
	 return retval;
}

bool eventQueue::createGenericEvent
(genericEvent *event, JSContext *cx, const string& callback, 
deque <argElement>& args){

	 if (callback != "") {
		  event->cx = cx;
		  event->callback = callback;
		  nsIVariant *tempVariant;

		  deque <argElement>::iterator a = args.begin(),
			   b = args.end();

		  while (a != b){
			   const argElement& ae = *a;


			   if (NewVariant(&tempVariant) != NS_OK) {
					DEBUGMSG(ftp, "couldn't create a new variant!\n");
					return false;
			   }
			   
			   switch(ae.type){
			   case TYPE_STRING:
					VariantSetString(tempVariant, ae.arg.s);
					event->args.push_back(tempVariant);
					break;

			   case TYPE_NUM:
					VariantSetNumber(tempVariant, ae.arg.d);
					event->args.push_back(tempVariant);
					break;

			   case TYPE_BOOL:
					VariantSetBoolean(tempVariant, ae.arg.b);
					event->args.push_back(tempVariant);
					break;

			   case TYPE_OBJ:
					VariantSetInterface(tempVariant, 
										(nsISupports *)(ae.arg.o));
					event->args.push_back(tempVariant);
					break;

			   }
			   ++a;
		  }
		  return true;
	 }
	 return false;
}
