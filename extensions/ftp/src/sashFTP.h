
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the FTP object 
******************************************************************/
#ifndef SASHFTP_H
#define SASHFTP_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIFTP.h"
#include "sashIExtension.h"
#include "sashISecurityManager.h"
#include "nsIXPConnect.h"
#include <string>
#include <deque>
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include "event.h"

class sashIGenericConstructor;
// (542AE96F-53C2-40E1-8839-DF8050EA2C71)
#define SASHFTP_CID {0x542ae96f, 0x53c2, 0x40e1, {0x88, 0x39, 0xdf, 0x80, 0x50, 0xea, 0x2c, 0x71}}

NS_DEFINE_CID(ksashFTPCID, SASHFTP_CID);

#define SASHFTP_CONTRACT_ID "@gnome.org/SashXB/FTP;1"

class sashFTP : public sashIFTP,
				public sashIExtension
{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHIEXTENSION;
	 NS_DECL_SASHIFTP;

	 sashFTP();
	 virtual ~sashFTP();

protected:
	 sashIGenericConstructor *m_FTPConnectionConstructor;
	 JSContext *m_cx;
	 sashISecurityManager * m_secMan;

	 string m_onError;
	 sashIActionRuntime *m_rt;

	 eventQueue *m_eventQueue;

	 friend class sashFTPConnection;
};

#endif
