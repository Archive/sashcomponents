
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the stream buffer object

*** this should be the file as the one for the communications/sockets extension

******************************************************************/
#include <stdlib.h>
#include "streamBuffer.h"
#include "assert.h"
#include "debugmsg.h"
streamBufferItem::streamBufferItem(const unsigned int num_bytes, const char* buf) {
	 assert(num_bytes > 0);
	 m_size = num_bytes;
	 m_data = new char[m_size+1];
	 memcpy(m_data, buf, m_size);
	 m_data[m_size] = '\0';
	 m_currpos = 0;
}

// reads from the current position in the item
void streamBufferItem::ReadData(const unsigned int num_bytes, char* buf) {
	 if (num_bytes > m_size - m_currpos) return;
	 memcpy(buf, &(m_data[m_currpos]), num_bytes);
	 m_currpos += num_bytes;
}

int streamBufferItem::FindChar(char c) {
	 if (m_currpos >= m_size) return -1;
	 char* a;
	 if ((a = strchr(&(m_data[m_currpos]), c)) != NULL)
		  return a - &(m_data[m_currpos]);

	 return -1;
}

streamBuffer::streamBuffer() {
	 m_totalSize = 0;
	 pthread_mutex_init(&m_buffer_lock, NULL);
}

streamBuffer::~streamBuffer() {
	 clear();
	 pthread_mutex_destroy(&m_buffer_lock);
}

bool streamBuffer::appendData(int size, char *buf, bool lock) {
	 bool retval = true;

	 if (lock) pthread_mutex_lock(&m_buffer_lock);

	 if (size <= 0){ 
		  retval = false;
	 } else {
		  streamBufferItem *sbi = new streamBufferItem(size, buf);
		  if (sbi == NULL){
			   retval = false;
		  } else {
			   m_bufferItems.push_back(sbi);
			   m_totalSize += sbi->Size();
		  }
	 }

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

bool streamBuffer::prependData(int size,char *buf){
	 pthread_mutex_lock(&m_buffer_lock);
	 bool retval = true;
	 if (size <= 0) {
		  retval = false;
	 } else {
		  streamBufferItem *sbi = new streamBufferItem(size, buf);
		  if (sbi == NULL){
			   retval = false;
		  }else{
			   m_bufferItems.push_front(sbi);
			   m_totalSize += sbi->Size();
		  }
	 }
	 pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}


int streamBuffer::getString(int size, string& str, bool lock){
	 if (lock) pthread_mutex_lock(&m_buffer_lock);

	 int dataSize = size;
	 if (size == -1) dataSize = m_totalSize;
	 if (size > m_totalSize) dataSize = m_totalSize;
	 
	 char *buf = new char[dataSize+1];
	 getData(dataSize, buf, false);
	 buf[dataSize] = '\0';
	 
	 str.reserve(dataSize);
	 str = buf;
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 delete [] buf;
	 return dataSize;
}

int streamBuffer::getData(int size, char *buf, bool lock){
	 if (! buf) return 0;
	 
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 DEBUGMSG(streambuffer, "Get data: asking for %d bytes of %d total\n", size, m_totalSize);	 

	 unsigned int amtLeft = (size >= 0 && size <= m_totalSize ? size : m_totalSize);

	 int amtRead = 0;

	 while (! m_bufferItems.empty() && amtLeft > 0) {
		  streamBufferItem* sbi = m_bufferItems[0];
		  if (sbi->BytesLeft() > amtLeft) {
			   sbi->ReadData(amtLeft, &buf[amtRead]);
			   m_totalSize -= amtLeft;
			   amtLeft = 0;
			   amtRead = size;
		  } else {
			   unsigned int thisone = sbi->BytesLeft();
			   sbi->ReadData(thisone, &buf[amtRead]);
			   amtRead += thisone;
			   amtLeft -= thisone;
			   delete sbi;
			   m_totalSize -= thisone;
			   m_bufferItems.pop_front();
		  }
	 }

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return amtRead;
}

int streamBuffer::findInBuffer(const string& str, int startpos){
	 pthread_mutex_lock(&m_buffer_lock);
	 int index = -1;

	 // lump it all into one.
	 if (m_bufferItems.size() > 1 && flatten() == -1) {
		  // error.
	 }
	 
	 if (! m_bufferItems.empty()) {
		  // read the data as a string.
		  streamBufferItem *sbi = m_bufferItems.front();
		  
		  if (sbi){
			   const char* a;
			   if ((a = strstr(sbi->m_data, str.c_str())) != NULL) 
					index = a - sbi->m_data;
		  }
	 }
	 pthread_mutex_unlock(&m_buffer_lock);
	 return index;
}

int streamBuffer::findInBuffer(char c, int startpos){
	 pthread_mutex_lock(&m_buffer_lock);	 
	 int offset = 0;
	 bool there = false;

	 if (! m_bufferItems.empty()) {
		  deque<streamBufferItem*>::iterator ai = m_bufferItems.begin(), bi = m_bufferItems.end();
		  while (ai != bi) {
			   int found = (*ai)->FindChar(c);
			   if (found >= 0) {
					offset += found;
					there = true;
					break;
			   }
			   offset += (*ai)->BytesLeft();
			   ++ai;
		  }
	 }
	 pthread_mutex_unlock(&m_buffer_lock);
	 return (there ? offset : -1);
}

int streamBuffer::flatten(){
	 if (m_totalSize == 0){
		  return 0;
	 }

// 	 assert(checkTotalSize(false));
	 int size = m_totalSize;
	 char * buf = new char[size];

	 if (buf == NULL)
		  return -1;
	 DEBUGMSG(streambuffer, "Flattening; asking for %d bytes\n", size);
	 getData(size, buf, false);
	 DEBUGMSG(streambuffer, "Flattening; size is now %d\n", m_totalSize);
	 // this is a really lazy way to do it.
	 // ***there are faster ways to do this.
	 appendData(size, buf, false);
	 DEBUGMSG(streambuffer, "Flattening; size is now %d\n", m_totalSize);	 
// 	 assert(checkTotalSize(false));
	 assert (size==m_totalSize);
	 assert (m_bufferItems.size() == 1);
	 delete [] buf;
	 return size;
}

bool streamBuffer::empty(bool lock){
	 bool retval;
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 retval = m_bufferItems.empty();
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

int streamBuffer::getSize(bool lock){		 
	 int size;
	 if (lock)pthread_mutex_lock(&m_buffer_lock);
	 size = m_totalSize;
	 if (lock)pthread_mutex_unlock(&m_buffer_lock);
	 return size;
}

bool streamBuffer::checkTotalSize(bool lock){
	 bool retval = true;
	 if (lock) pthread_mutex_lock(&m_buffer_lock);

	 int totalSize=0;
	 deque<streamBufferItem *>::iterator a = m_bufferItems.begin(), 
		  b = m_bufferItems.end();

	 while (a != b){
		  totalSize += (*a)->BytesLeft();
		  ++a;
	 }
	 if (totalSize != m_totalSize){
		  retval = false;
	 }
	 DEBUGMSG(streambuffer, "Checking total size: %d bytes\n", totalSize);

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

void streamBuffer::printStreamBuffer(bool lock){
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 if (m_bufferItems.empty()){
		  printf("empty stream buffer\n");
	 }else{
		  printf("total bytes: %d\n", m_totalSize);
	 
		  deque<streamBufferItem *>::iterator a = m_bufferItems.begin(), 
			   b = m_bufferItems.end();

		  while (a != b){
			   (*a)->Print();
			   ++a;
		  }
	 }
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
}

void streamBuffer::clear(bool lock){
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 while (! m_bufferItems.empty()){
		  streamBufferItem *sbi = m_bufferItems.front();
		  m_bufferItems.pop_front();
		  if (sbi != NULL){ 
			   delete sbi;
		  }
	 }
	 m_totalSize = 0;
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
}
