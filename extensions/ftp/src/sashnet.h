
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for sashnet: simple socket calls

******************************************************************/

#ifndef SASHNET_H
#define SASHNET_H

#include "streamBuffer.h"

const int RECEIVE_BUFSIZE = 2048;
const int SEND_BUFSIZE = 1024;
const int LINE_BUFSIZE = 128;
const int LIST_BUFSIZE = 256;

void setRemoteAddr(struct sockaddr_in *remoteAddr,
				   const char *remoteHost, unsigned short remotePort);
bool bindSocket(int sockfd, int port);
int openConnection(int sockType, struct sockaddr_in *remoteAddr);
int fullSend(int sockfd, int size, char * data);
int receiveAllFromConnection(int dataSockFD, streamBuffer *sb);
int bindLocal(int sockType, int port, sockaddr_in *myAddr);

#endif
