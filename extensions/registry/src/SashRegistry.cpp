
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, AJ Shankar, Stefan Atev

Implementation of the Sash Registry class

*****************************************************************/

#include "SashRegistry.h"
#include <nsMemory.h>
#include "sashtools.h"
#include <string>
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "nsIVariant.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIXPRegistry.h"
#include "sashIXPFileSystem.h"
#include "security.h"
#include "secman.h"
#include "sash_constants.h"

using namespace std;

SASH_EXTENSION_IMPL(SashRegistry, sashIRegistry, SASHREGISTRY,
		   "Registry", "Registry extension");


NS_IMETHODIMP
SashRegistry::ProcessEvent()
{
	return NS_OK;
}

SashRegistry::SashRegistry() : m_LastModified(0) {
  NS_INIT_ISUPPORTS();
  DEBUGMSG(sashregistry, "Sash registry constructor\n");
  /* member initializers and constructor code */
}

SashRegistry::~SashRegistry() {
}

NS_IMETHODIMP 
SashRegistry::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP SashRegistry::Initialize(sashIActionRuntime *act,
		     const char *xml,
		     const char *instanceGuid, 
		     JSContext *context, JSObject *object) {
  DEBUGMSG(sashregistry, "Initializing. xml = %s\n", xml);
  // right now we ignore any xml

  // Create a new registry.
  m_pRegistry = do_CreateInstance("@gnome.org/SashXB/XPRegistry;1");
  if (!m_pRegistry)
	   OutputError("SashRegistry: Unable to create the XPRegistry\n");
  m_pFileSystem = do_GetService("@gnome.org/SashXB/XPFileSystem;1");
  if (!m_pFileSystem)
	   OutputError("SashRegistry: Unable to create the FileSystem\n");

  act->GetSecurityManager(&m_pSecurityManager);

  XP_GET_STRING(act->GetGuid, m_WeblicationGuid);
  DEBUGMSG(sashregistry, "Guid is %s\n", m_WeblicationGuid.c_str());
  XP_GET_STRING(act->GetSashDirectory, m_SashDir);
  DEBUGMSG(sashregistry, "sash dir is %s\n", m_SashDir.c_str());

  // ASSIGN THIS LATER.
//	 string fileloc = WeblicationDirectory(m_SashDir, g) +  "/" + RegistryFileName(g);

//  act->GetRegistry(&m_pWeblicationRegistry);


  return NS_OK;
}

void SashRegistry::WriteToDisk() {
	 if (m_pRegistry) {
		  m_pRegistry->WriteToDisk(m_Filename.c_str());
		  PRInt32 last2;
		  m_pFileSystem->FileLastModified(m_Filename.c_str(), &last2);
		  m_LastModified = last2;
	 }
}

bool SashRegistry::OpenRegistry(const string& guid) {
	 // some overlap of code here, but it's important to keep things clear...
	 string g = ToUpper(guid);

	 DEBUGMSG(sashregistry, "OpenRegistry %s\n", guid.c_str());

	 // trying to access a registry we already have open
	 if (g == m_CurrGuid) {
		  PRInt32 last;
		  m_pFileSystem->FileLastModified(m_Filename.c_str(), &last);
		  // if it's been modified since we last opened or saved it, open it again
		  if (last > m_LastModified) {
			   PRBool val;
			   m_pRegistry->OpenFile(m_Filename.c_str(), &val);
			   if (! val)
					return false;
			   m_LastModified = last;
		  }
	 } else {
		  if (g == m_WeblicationGuid) {
			   // if we're trying to access the weblication's registry, just open it
			   AssertSecurity(m_pSecurityManager, GSS_REGISTRY_ACCESS, (float) RSS_LOCAL);
		  } else {
			   // trying to open any old registry
			   AssertSecurity(m_pSecurityManager, GSS_REGISTRY_ACCESS, (float) RSS_GLOBAL);
		  }
		  m_Filename = WeblicationDirectory(m_SashDir, g) + "/" + RegistryFileName(g);
		  PRBool exists = false;
		  m_pFileSystem->FileExists(m_Filename.c_str(), &exists);
		  if (!exists) {
			   return false;
		  }
		  PRBool val;
		  m_pRegistry->OpenFile(m_Filename.c_str(), &val);
		  if (! val)
			   return false;
		  
		  PRInt32 last2;
		  m_pFileSystem->FileLastModified(m_Filename.c_str(), &last2);
		  m_LastModified = last2;
		  m_CurrGuid = g;
	 }
	 return true;
}

/* PRBool CreateKey (in string mainKey, in string key); */
NS_IMETHODIMP SashRegistry::CreateKey(const char *mainKey, const char *key, PRBool *_retval) { 
  *_retval = PR_FALSE;
  nsresult ret = NS_OK;

  DEBUGMSG(sashregistry, "creating key\n");
  if (OpenRegistry(mainKey)) {
//    *_retval = (PRBool) m_pRegistry->CreateKey(key);
	   ret = m_pRegistry->CreateKey(key, _retval);
	   assert(NS_SUCCEEDED(ret));
	   if (NS_SUCCEEDED(ret) && (*_retval))
			WriteToDisk();
  }
  DEBUGMSG(sashregistry, "created key\n");
  return ret;
}

/* PRBool CreateWeblicationKey (in string key); */
NS_IMETHODIMP SashRegistry::CreateWeblicationKey(const char *key, PRBool *_retval) { 
	 return CreateKey(m_WeblicationGuid.c_str(), key, _retval);
}

/* PRBool DeleteKey (in string mainKey, in string key); */
NS_IMETHODIMP SashRegistry::DeleteKey(const char *mainKey, const char *key, PRBool *_retval) { 
  *_retval = PR_FALSE;
  nsresult ret = NS_OK;

  if (OpenRegistry(mainKey)) {
	   ret = m_pRegistry->DeleteKey(key, true, _retval);
	   if (NS_SUCCEEDED(ret) && (*_retval))
			WriteToDisk();
  }
  return ret;

}

/* PRBool DeleteWeblicationKey (in string subkey); */
NS_IMETHODIMP SashRegistry::DeleteWeblicationKey(const char *subkey, PRBool *_retval) {
	 return DeleteKey(m_WeblicationGuid.c_str(), subkey, _retval);
}

/* PRBool DeleteValue (in string mainKey, in string subkey, in string dataName); */
NS_IMETHODIMP SashRegistry::DeleteValue(const char *mainKey, const char *subkey, const char *dataName, PRBool *_retval) { 
  *_retval = PR_FALSE;
  nsresult ret = NS_OK;

  if (OpenRegistry(mainKey)) {
//     *_retval = (PRBool) m_pRegistry->DeleteValue(subkey, dataName);
	   ret = m_pRegistry->DeleteValue(subkey, dataName,
									  _retval);
	   if (NS_SUCCEEDED(ret) && (*_retval))
			WriteToDisk();
  }
  return NS_OK;
}

/* PRBool DeleteWeblicationValue (in string subkey, in string dataName); */
NS_IMETHODIMP SashRegistry::DeleteWeblicationValue(const char *subkey, const char *dataName, PRBool *_retval) { 
	 return DeleteValue(m_WeblicationGuid.c_str(), subkey, dataName, _retval);
}
 
/* nsIVariant QueryValue (in string mainKey, in string subKey, in string dataName); */
NS_IMETHODIMP SashRegistry::QueryValue(const char *mainKey, const char *subkey, const char *dataName, nsIVariant **_retval) {
	 nsresult ret = NS_OK;
	 if (OpenRegistry(mainKey)) {
		  ret = m_pRegistry->QueryValue(subkey, dataName,
										_retval);
	 } 
	 return ret;	 
}

/* nsIVariant QueryWeblicationValue (in string subKey, in string dadaName); */
NS_IMETHODIMP SashRegistry::QueryWeblicationValue(const char *subkey, const char *dataName, nsIVariant **_retval) {
	 return QueryValue(m_WeblicationGuid.c_str(), subkey, dataName, _retval);
}

/* PRBool SetValue (in string mainKey, in string subKey, in string dataName, in nsIVariant value); */
NS_IMETHODIMP SashRegistry::SetValue(const char *mainKey, const char *subKey, const char *dataName, nsIVariant *value, PRBool *_retval) {
	 *_retval = PR_TRUE;

	 nsresult ret = NS_OK;
	 if (OpenRegistry(mainKey)) {
		  ret = m_pRegistry->SetValue(subKey, dataName,
									  value);
		  if (NS_SUCCEEDED(ret))
			   WriteToDisk();
	 }
	 return ret;
}

/* PRBool SetWeblicationValue (in string subKey, in string dataName, in nsIVariant value); */
NS_IMETHODIMP SashRegistry::SetWeblicationValue(const char *subkey, const char *dataName, nsIVariant *value, PRBool *_retval) {
	 return SetValue(m_WeblicationGuid.c_str(), subkey, dataName, value, _retval);
}

/* nsIVariant EnumKeys (in string mainKey, in string subKey); */
NS_IMETHODIMP SashRegistry::EnumKeys(const char *mainKey, const char *subkey, nsIVariant **_retval) {
	 nsresult ret = NS_OK;

	 DEBUGMSG(sashregistry, "EnumKeys: mainkey = %s\n", mainKey);
	 
	 if (OpenRegistry(mainKey)) {
		  ret = m_pRegistry->EnumKeys(subkey, _retval);
	 } else {
		  DEBUGMSG(sashregistry, "Unable to open main key %s\n", mainKey);
	 }
	 return ret;
}

/* nsIVariant EnumWeblicationKeys (in string subKey); */
NS_IMETHODIMP SashRegistry::EnumWeblicationKeys(const char *subkey, nsIVariant **_retval) {
	 return EnumKeys(m_WeblicationGuid.c_str(), subkey, _retval);
}

/* nsIVariant EnumValues (in string mainKey, in string subKey); */
NS_IMETHODIMP SashRegistry::EnumValues(const char *mainKey, const char *subkey, nsIVariant **_retval) {
	 nsresult ret = NS_OK;
	 if (OpenRegistry(mainKey)) {
		  ret = m_pRegistry->EnumValues(subkey, _retval);
	 }
	 return ret;;
}
	

/* nsIVariant EnumWeblicationValues (in string subKey); */
NS_IMETHODIMP SashRegistry::EnumWeblicationValues(const char *subkey, nsIVariant **_retval) {
	 return EnumValues(m_WeblicationGuid.c_str(), subkey, _retval);
}
