
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, AJ Shankar, Stefan Atev

Implementation of the Sash Registry class

*****************************************************************/

#ifndef NSSASHCOMMON_REGISTRY_H_DEF
#define NSSASHCOMMON_REGISTRY_H_DEF

#include "sashIRegistry.h"
#include "sashIXPRegistry.h"
#include "sashIXPFileSystem.h"
#include "sashIExtension.h"
#include "sashISecurityManager.h"
#include <string>

#define SASHREGISTRY_CID {0x3E3A9D75, 0xA147, 0x46BF, {0x87, 0x0A, 0x58, 0xEC, 0x27, 0xA4, 0xB4, 0x2E}}

NS_DEFINE_CID(kSashRegistryCID, SASHREGISTRY_CID);

#define SASHREGISTRY_CONTRACT_ID "@gnome.org/SashXB/SashRegistry;1"

class SashRegistry : public sashIRegistry, public sashIExtension {
  public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHIEXTENSION;
	 NS_DECL_SASHIREGISTRY;

    SashRegistry();
    virtual ~SashRegistry();
  protected:
	nsCOMPtr<sashIXPRegistry> m_pRegistry;
	nsCOMPtr<sashIXPFileSystem> m_pFileSystem;
	sashISecurityManager *m_pSecurityManager;
	int m_LastModified;
    string m_CurrGuid, m_WeblicationGuid, m_SashDir, m_Filename;

	void WriteToDisk();
    bool OpenRegistry(const string & guid);
    
};

#endif
