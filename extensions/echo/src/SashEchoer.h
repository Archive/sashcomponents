#ifndef SASHECHOER_H
#define SASHECHOER_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashIEcho.h"
#include <string>

#define SASHECHOER_CID {0x51696a30, 0x3e59, 0x46ca, {0xad, 0x7d, 0xcc, 0x20, 0x89, 0x7b, \
0xe6, 0xf3}}

NS_DEFINE_CID(ksashEchoerCID, SASHECHOER_CID);

#define SASHECHOER_CONTRACT_ID "@gnome.org/SashXB/echo/echoer;1"

class SashEchoer : public sashIEchoer, 
                   public sashIConstructor {
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHIECHOER

  SashEchoer();
  virtual ~SashEchoer();
protected:
  std::string m_Prefix;
  PRBool m_Reverse;
  PRUint32 m_NumTimes;

  JSContext* m_pContext;
  sashISecurityManager* m_pSecurityManager;
};

#endif //SASHECHOER_H


