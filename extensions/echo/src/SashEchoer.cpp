#include "SashEchoer.h"
#include "SashEcho.h"
#include "sashIExtension.h"
#include "sashVariantUtils.h"
#include "extensiontools.h"
#include <algorithm>

// Note that we have slightly different macros here, since sashIEchoer
// is not an extension. 
NS_IMPL_ISUPPORTS2_CI(SashEchoer, sashIEchoer, sashIConstructor);

SashEchoer::SashEchoer() : m_Reverse(false), m_NumTimes(1) {
   NS_INIT_ISUPPORTS();
}

SashEchoer::~SashEchoer() {
}

NS_IMETHODIMP SashEchoer::InitializeNewObject(sashIActionRuntime* act, 
                                   sashIExtension* ext, JSContext* cx, 
                                   PRUint32 argc, nsIVariant **argv, PRBool *ret) {
   // check to see if the arguments are valid
   if ((argc != 0 && argc != 3) || (argc == 3 && (! VariantIsString(argv[0]) ||
        ! VariantIsNumber(argv[1]) || ! VariantIsBoolean(argv[2])))) {
      *ret = false;
      return NS_OK;
   }
   if (argc == 3) {
      m_Prefix = VariantGetString(argv[0]);
      m_NumTimes = (PRUint32) VariantGetNumber(argv[1]);
      m_Reverse = VariantGetBoolean(argv[2]);
      DEBUGMSG(echo, "Creating echoer with params %s, %d, %d\n", 
               m_Prefix.c_str(), m_NumTimes, m_Reverse);
   }

   // we need to keep a pointer to the security manager around for Print
   act->GetSecurityManager(&m_pSecurityManager);

   // save the context, in case we need to call events
   m_pContext = cx;

   // make sure to set the return value to true on success,
   *ret = true;

   return NS_OK;
}


NS_IMETHODIMP SashEchoer::GetPrefix(char **aPrefix) {
   XP_COPY_STRING(m_Prefix.c_str(), aPrefix);
   return NS_OK;
}

NS_IMETHODIMP SashEchoer::SetPrefix(const char *aPrefix) {
   m_Prefix = aPrefix;
   return NS_OK;
}

NS_IMETHODIMP SashEchoer::GetReverse(PRBool* aReverse) {
   *aReverse = m_Reverse;
   return NS_OK;
}

NS_IMETHODIMP SashEchoer::SetReverse(PRBool aReverse) {
   m_Reverse = aReverse;
   return NS_OK;
}

NS_IMETHODIMP SashEchoer::GetNumTimes(PRUint32* aNumTimes) {
   *aNumTimes = m_NumTimes;
   return NS_OK;
}

NS_IMETHODIMP SashEchoer::SetNumTimes(PRUint32 aNumTimes) {
   m_NumTimes = aNumTimes;
   return NS_OK;
}

// we should really extern this from SashEcho but for ease of implementation
// we'll do it this way
const SecuritySetting NUM_TIMES_SETTING_ID = 1;

NS_IMETHODIMP SashEchoer::Print(const char* toPrint) {
   // since it's still the Sash.Echo extension, we use that GUID
   AssertSecurity(m_pSecurityManager, ToUpper(kSashEchoCID.ToString()), 
                  NUM_TIMES_SETTING_ID, (float) m_NumTimes);

   string p(toPrint);

   if (m_Reverse)
      reverse(p.begin(), p.end()); // STL algo

   for (PRUint32 i = 0 ; i < m_NumTimes ; i++) 
      cout << m_Prefix << p << endl;

   return NS_OK;
}


