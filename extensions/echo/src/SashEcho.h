#ifndef SASHECHO_H
#define SASHECHO_H

#include "nsID.h"
#include "sashIEcho.h"
#include "sashIExtension.h"
#include "secman.h"
#include <string>

//{2ab05691-ba8c-4766-9931-bac841b0d39c}
#define SASHECHO_CID \
  {0x2ab05691, 0xba8c, 0x4766, \
    {0x99, 0x31, 0xba, 0xc8, 0x41, 0xb0, 0xd3, 0x9c}}

NS_DEFINE_CID(kSashEchoCID, SASHECHO_CID);

#define SASHECHO_CONTRACT_ID "@gnome.org/SashXB/echo;1"

class sashIGenericConstructor;

class SashEcho : public sashIEcho,
                 public sashIExtension {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHIECHO

  SashEcho();
  virtual ~SashEcho();
  
private:
  // store the Prefix here
  std::string m_Prefix;
  std::string m_OnReallyLongEcho;

  // the runtime security manager -- we'll need this later
  sashISecurityManager* m_pSecurityManager;

  // the JSContext, needed for callbacks
  JSContext* m_pContext;

  // also needed for callbacks
  sashIActionRuntime* m_pActionRuntime;

  sashIGenericConstructor *m_pEchoerConstructor;

};

#endif // SASHECHO_H


