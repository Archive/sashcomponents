// include all of your implementation header files here
#include "SashEcho.h"
#include "SashEchoer.h"
#include "extensiontools.h"

// we need one each of these for each object we're exposing -- 
// including the base extension itself
NS_DECL_CLASSINFO(SashEcho);
NS_DECL_CLASSINFO(SashEchoer);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashEcho);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashEchoer);

static nsModuleComponentInfo components[] = {
   MODULE_COMPONENT_ENTRY(SashEcho, SASHECHO, "A simple example extension"),
   MODULE_COMPONENT_ENTRY(SashEchoer, SASHECHOER, "Echoer object")
};

NS_IMPL_NSGETMODULE(SashEcho, components)


