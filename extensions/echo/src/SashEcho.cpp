#include <string>
#include <vector>
#include <iostream>
#include "xpcomtools.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "security.h"
#include "sash_constants.h"
#include "sashIActionRuntime.h"
#include "SashEcho.h"
#include "sashIGenericConstructor.h"
#include "SashEchoer.h" // since we need its CID

SASH_EXTENSION_IMPL_NO_NSGET(SashEcho, sashIEcho, "Echo");

SashEcho::SashEcho() {
  NS_INIT_ISUPPORTS();
}

SashEcho::~SashEcho() {
}

NS_IMETHODIMP SashEcho::Initialize(sashIActionRuntime *act,
                                   const char *xml,
                                   const char *instanceGuid, 
                                   JSContext *context, 
                                   JSObject *object) {
 /*
    This function is called by the runtime once the extension is created. 
    The first argument is the global runtime. The extension might want
    to call methods on this or store a pointer to it for later. The next 
    argument is the XML string with which the extension is being registered. 
    The structure of this XML is up to the extension-writer (you), and the 
    values will be filled in by the weblication author. The third is the guid
    of the currently running weblication.

    The JSContext is necessary to provide callbacks to the weblication. The 
    JSObject is in fact the JavaScript representation of your extension; 
    you might want this as well when invoking callbacks.

    We'll simply extract the security manager, and save the JSContext.
  */
  act->GetSecurityManager(&m_pSecurityManager);
  m_pActionRuntime = act;
  NewSashConstructor(act, this, SASHECHOER_CONTRACT_ID, SASHIECHOER_IID_STR,
                     &m_pEchoerConstructor);


  m_pContext = context;
  return NS_OK;
}

NS_IMETHODIMP SashEcho::ProcessEvent() {
  /*
    This function will be called periodically by the runtime, so we can 
    perform any idle loop actions required by the extension. Specifically, 
    if your extension has multiple threads, the JavaScript engine requires 
    that you invoke callbacks on it from its own thread. So to call an event 
    from another thread, save the callback info in an accessible structure, 
    wait for the runtime to call ProcessEvent from the main thread, and invoke
    the callback from here.
    
    Since Echo does none of this, we don't need to do anything here.
  */
  return NS_OK;
}

NS_IMETHODIMP SashEcho::Cleanup() {
  /*
    The runtime calls Cleanup when the weblication is about to exit. Do any
    shutdown procedures here. One example is using Cleanup to close any open 
    file and socket descriptors. 
  */
  return NS_OK;
}

NS_IMETHODIMP SashEcho::GetPrefix(char **aPrefix) {
   // we use a macro defined in SashXB/extensiontools/xpcomtools.h that 
   // allocates memory for the string and copies it
   XP_COPY_STRING(m_Prefix.c_str(), aPrefix);
   return NS_OK;
}

NS_IMETHODIMP SashEcho::SetPrefix(const char *aPrefix) {
   // just store the prefix for future reference
   // we don't own aPrefix, so we don't free it
   m_Prefix = aPrefix;
   return NS_OK;
}

NS_IMETHODIMP SashEcho::Print(const char *toPrint) {
   if (strlen(toPrint) > 32) {
      // if the user has defined a callback...
      if (! m_OnReallyLongEcho.empty()) {
         // time to invoke our event callback
         // first, we construct its arguments, in this case the echo string
         vector<nsIVariant*> args;
         nsCOMPtr<nsIVariant> echo_string;
         NewVariant(getter_AddRefs(echo_string));
         VariantSetString(echo_string, toPrint);
         args.push_back(echo_string);
         // we're careful about properly reference-counting the result
         // (normally, with just CallEvent, we wouldn't have to worry)
         nsCOMPtr<nsIVariant> res = getter_AddRefs(
             CallEventWithResult(m_pActionRuntime, m_pContext, 
                                 m_OnReallyLongEcho, args));
         // if the function doesn't return true, we're outta here
         if (VariantGetBoolean(res) == false) {
            return NS_OK;
         }
      }
   }
   cout << m_Prefix << toPrint << endl;
   return NS_OK;
}


NS_IMETHODIMP SashEcho::WhatType(nsIVariant* my_variant, char** _retval) {
   if (VariantIsBoolean(my_variant))
      XP_COPY_STRING("boolean", _retval);
   else if (VariantIsString(my_variant))
      XP_COPY_STRING("string", _retval);
   else if (VariantIsNumber(my_variant))
      XP_COPY_STRING("number", _retval);
   else 
      XP_COPY_STRING("one of many other types we could check", _retval);

   return NS_OK;
}


#include <numeric>

NS_IMETHODIMP SashEcho::Sum(nsIVariant* array_of_nums, double* _retval) {
   *_retval = 0.;
   if (VariantIsArray(array_of_nums)) {
      vector<double> nums;
      VariantGetArray(array_of_nums, nums);
      // use an STL algorithm to do the work for us
      *_retval = accumulate(nums.begin(), nums.end(), 0.);
   }
   return NS_OK;
}


#include <stdlib.h>
#include <time.h>

// PRInt16 is the native type that corresponds to IDL's "short"
NS_IMETHODIMP SashEcho::Random(PRInt16 int_or_string, nsIVariant** _retval) {
   srand(time(NULL));
   // make sure to create a new variant first
   NewVariant(_retval);
   if (int_or_string == 0) {
      VariantSetInteger(*_retval, rand());
   } else {
      string t;
      int i = 0;
      do { t += (char) (rand() % ('Z' - 'A') + 'A'); } while (++i < 6);
      VariantSetString(*_retval, t);
   }
   return NS_OK;
}


#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

NS_IMETHODIMP SashEcho::SystemEcho(const char* toPrint) {
   // we want to ensure that the user is letting the currently running
   // weblication spawn other processes
   AssertSecurity(m_pSecurityManager, GSS_PROCESS_SPAWNING, true);
   
   // If we've made it here then we must have received security clearance.
   // it's probably in /bin/echo, but we're not sure, so we'll use the even
   // more dangerous execlp
   int pid = fork();
   if (pid == 0) 
      execlp("echo", "echo", toPrint, NULL);
   else 
      waitpid(pid, NULL, 0);

   return NS_OK;
}


// must be the same as the ID we set in the wdf
const SecuritySetting NUM_TIMES_SETTING_ID = 1;

NS_IMETHODIMP SashEcho::EchoNTimes(const char* toPrint, PRUint32 n) {
   // this time, we pass along our own GUID, the ID of the custom setting we 
   // want to check, and the number we're checking
   AssertSecurity(m_pSecurityManager, ToUpper(kSashEchoCID.ToString()), 
                  NUM_TIMES_SETTING_ID, (float) n);

   for (unsigned int i = 0 ; i < n ; i++)
      cout << m_Prefix << toPrint << endl;
   return NS_OK;
}


NS_IMETHODIMP SashEcho::GetOnReallyLongEcho(char **aString) {
   XP_COPY_STRING(m_OnReallyLongEcho.c_str(), aString);
   return NS_OK;
}

NS_IMETHODIMP SashEcho::SetOnReallyLongEcho(const char *aString) {
   m_OnReallyLongEcho = aString;
   return NS_OK;
}


NS_IMETHODIMP SashEcho::GetEchoer(sashIGenericConstructor **ret) {
  NS_ADDREF(m_pEchoerConstructor);
  *ret = m_pEchoerConstructor;
  return NS_OK;
}


