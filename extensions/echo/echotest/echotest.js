// sample js code that uses echo

// prints out "Hello!"
Sash.Echo.Print("Hello!");

Sash.Echo.Prefix = "TestEcho: ";
// prints out "TestEcho: Goodbye!"
Sash.Echo.Print("Goodbye!");

// prints out "TestEcho: TestEcho: "
Sash.Echo.Print(Sash.Echo.Prefix);

printsys("Should be string: " + Sash.Echo.WhatType("oogabooga"));
printsys("Should be boolean: " + Sash.Echo.WhatType(true));
printsys("Should be number: " + Sash.Echo.WhatType(42));
printsys("Should be something else: " + Sash.Echo.WhatType(Sash.Echo));

printsys("total is " + Sash.Echo.Sum([1, 5.3, 23]));  // should be 29.3

var a_num = Sash.Echo.Random(0);
var a_string = Sash.Echo.Random(1);
printsys(typeof(a_num)+" " + a_num + " | " + typeof(a_string) + " " + a_string);

Sash.Echo.SystemEcho("testing SystemEcho");

Sash.Echo.EchoNTimes("Trying 2", 2);
Sash.Echo.EchoNTimes("Trying 12", 12);

// we can actually do this:
// Sash.Echo.OnReallyLongEcho = long_echo;
// but that passes the entire function body through, which is less efficient
// so we'll stick with quotes:
Sash.Echo.OnReallyLongEcho = "long_echo";

// we know that this function receives the string as an argument only because
// it is documented in the IDL
function long_echo(str) {
   var t = Sash.Core.UI;
   return (t.MessageBox("Are you sure you want to echo the following " + 
                        str.length + "-character string?\n" + str, 
                        "Are you sure?", t.MB_YESNO | t.MB_ICONINFORMATION) 
             == t.IDYES);
}

var a;
while((a = Sash.Core.UI.PromptForValue("Echo String", 
         "Please enter a string to echo:", "")) != "") {
   Sash.Echo.Print(a);
}

var echoers = new Array();
echoers.push(new Sash.Echo.Echoer("Prefix1 ", 1, false));
echoers.push(new Sash.Echo.Echoer("Prefix2 ", 5, true));
echoers.push(new Sash.Echo.Echoer("Prefix3 ", 3, false));
echoers.push(new Sash.Echo.Echoer("Prefix4 ", 1, true));

while((a = Sash.Core.UI.PromptForValue("Echo String", 
         "Please enter a string to echo:", "")) != "") {
   for (var p in echoers) { echoers[p].Print(a); }
}



Sash.Console.Quit();


