#include <unistd.h>

#include "xpcomtools.h"
#include "extensiontools.h"
#include "security.h"
#include "sash_constants.h"
#include "sashIActionRuntime.h"
#include "sashIGenericConstructor.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"

#include "sashmySQL.h"
#include "sashmySQLConnection.h"
#include "sashmySQLResult.h"

SASH_EXTENSION_IMPL_NO_NSGET(sashmySQL, sashImySQL, "mySQL");

sashmySQL::sashmySQL() {
  NS_INIT_ISUPPORTS();
}

sashmySQL::~sashmySQL() {
}

NS_IMETHODIMP sashmySQL::Initialize(sashIActionRuntime *_rt,
                                    const char *xml,
                                    const char *instGuid,
                                    JSContext *_cx,
                                    JSObject *_obj)
{
  rt = _rt;
  jscx = _cx;
  rt->GetSecurityManager(&secman);
  NewSashConstructor(rt, 
                     this, 
                     SASHMYSQLCONNECTION_CONTRACT_ID,
                     SASHIMYSQLCONNECTION_IID_STR,
                     &con);
                     
  pthread_mutex_init(&readyLock, 0);
  
  return NS_OK;
} 

NS_IMETHODIMP sashmySQL::ProcessEvent() {
//TODO can we do something meaningful when the programmer hasn't set the
//  callback?
  pthread_mutex_lock(&readyLock);
  while (!ready.empty()) {
    sashmySQLConnection *c = ready.front();
    ready.pop();

    vector<nsIVariant*> args;
    nsCOMPtr<nsIVariant> conVar;
    NewVariant(getter_AddRefs(conVar));
    VariantSetInterface(conVar,
                        (sashImySQLConnection*)c,
                        NS_GET_IID(sashImySQLConnection));
    args.push_back(conVar);

    switch (c->op) {
      case sashmySQLConnection::CONNECT: {
        DEBUGMSG(mysqlext, "Found con with complete connect, calling\n");
        c->op = sashmySQLConnection::NONE;
        CallEventWithResult(rt, jscx, c->onConnectDone, args);
        break;
      }
      case sashmySQLConnection::QUERY: {
        DEBUGMSG(mysqlext, "Found con with complete query, calling\n");
        c->op = sashmySQLConnection::NONE;
        CallEventWithResult(rt, jscx, c->onQueryDone, args);
        break;
      }
      case sashmySQLConnection::STORE: {
        DEBUGMSG(mysqlext, "Found con with complete store, calling\n");
        nsCOMPtr<nsIVariant> resVar;
        NewVariant(getter_AddRefs(resVar));
        VariantSetInterface(resVar,
                            (sashImySQLResult*)c->reqData,
                            NS_GET_IID(sashImySQLResult));
        args.push_back(resVar);
        c->op = sashmySQLConnection::NONE;
        CallEventWithResult(rt, jscx, c->onStoreDone, args);
        break;
      }
    }
  }
  pthread_mutex_unlock(&readyLock);
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::Cleanup() {
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetConnection(sashIGenericConstructor **c) {
  NS_ADDREF(con);
  *c = con;
  DEBUGMSG(mysqlext, "Constructed mySQLConnection object\n");
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetDECIMAL(FieldType *t) {
  *t = FIELD_TYPE_DECIMAL;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetTINY(FieldType *t) {
  *t = FIELD_TYPE_TINY;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetSHORT(FieldType *t) {
  *t = FIELD_TYPE_SHORT;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetLONG(FieldType *t) {
  *t = FIELD_TYPE_LONG;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetFLOAT(FieldType *t) {
  *t = FIELD_TYPE_FLOAT;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetDOUBLE(FieldType *t) {
  *t = FIELD_TYPE_DOUBLE;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetNULL(FieldType *t) {
  *t = FIELD_TYPE_NULL;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetTIMESTAMP(FieldType *t) {
  *t = FIELD_TYPE_TIMESTAMP;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetLONGLONG(FieldType *t) {
  *t = FIELD_TYPE_LONGLONG;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetINT24(FieldType *t) {
  *t = FIELD_TYPE_INT24;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetDATE(FieldType *t) {
  *t = FIELD_TYPE_DATE;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetTIME(FieldType *t) {
  *t = FIELD_TYPE_TIME;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetDATETIME(FieldType *t) {
  *t = FIELD_TYPE_DATETIME;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetYEAR(FieldType *t) {
  *t = FIELD_TYPE_YEAR;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetNEWDATE(FieldType *t) {
  *t = FIELD_TYPE_NEWDATE;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetENUM(FieldType *t) {
  *t = FIELD_TYPE_ENUM;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetSET(FieldType *t) {
  *t = FIELD_TYPE_SET;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetTINY_BLOB(FieldType *t) {
  *t = FIELD_TYPE_TINY_BLOB;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetMEDIUM_BLOB(FieldType *t) {
  *t = FIELD_TYPE_MEDIUM_BLOB;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetLONG_BLOB(FieldType *t) {
  *t = FIELD_TYPE_LONG_BLOB;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetBLOB(FieldType *t) {
  *t = FIELD_TYPE_BLOB;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetVAR_STRING(FieldType *t) {
  *t = FIELD_TYPE_VAR_STRING;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::GetSTRING(FieldType *t) {
  *t = FIELD_TYPE_STRING;
  return NS_OK;
}

NS_IMETHODIMP sashmySQL::IsNum(const FieldType t, PRBool *res) {
  if ((t == FIELD_TYPE_DECIMAL) ||
      (t == FIELD_TYPE_TINY) ||
      (t == FIELD_TYPE_SHORT) ||
      (t == FIELD_TYPE_LONG) ||
      (t == FIELD_TYPE_FLOAT) ||
      (t == FIELD_TYPE_DOUBLE) ||
      (t == FIELD_TYPE_LONGLONG) ||
      (t == FIELD_TYPE_INT24))
    *res = true;
  else
    *res = false;

  return NS_OK;    
}

NS_IMETHODIMP sashmySQL::GetMaster(sashmySQL **master) { 
  *master = this;
  return NS_OK;
}

