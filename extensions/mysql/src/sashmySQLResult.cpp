#include "extensiontools.h"
#include "debugmsg.h"
#include "sashVariantUtils.h"
#include "sashmySQLResult.h"
#include "sashmySQLField.h"

NS_IMPL_ISUPPORTS2_CI(sashmySQLResult,
                      sashImySQLResult,
                      sashIConstructor);
                      
sashmySQLResult::sashmySQLResult() : result(0), 
                                     nFields(0),
                                     nRows(0),
                                     rowsAffected(0),  
                                     curRow(0),
                                     error(false)
{
  NS_INIT_ISUPPORTS();
}

sashmySQLResult::~sashmySQLResult() {
  if (result)
    mysql_free_result(result);
}

NS_IMETHODIMP sashmySQLResult::InitializeNewObject(sashIActionRuntime *_rt,
                                                   sashIExtension *_ext,
                                                   JSContext *_cx,
                                                   PRUint32 argc,
                                                   nsIVariant **argv,
                                                   PRBool *ret)
{
  rt = _rt;
  ext = _ext;
  rt->GetSecurityManager(&secman);
  jscx = _cx;
  *ret = true;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetError(PRBool *res) {
  *res = error;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetRowsAffected(PRUint32 *n) {
  *n = rowsAffected;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetNumFields(PRUint32 *n) {
  *n = nFields;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetNumRows(PRUint32 *n) {
  *n = nRows;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetFields(nsIVariant **ret) {
  *ret = 0;
  if (rowsAffected) {
    DEBUGMSG(mysql, "Hey, you can't get fields, there's no data!\n");
    return NS_OK;
  }
  if (error) {
    DEBUGMSG(mysql, "Hey, your query failed and there's an error\n");
    return NS_OK;
  }
  
  NewVariant(ret);
  vector<nsISupports*> fieldObjs;
  MYSQL_FIELD *fields = mysql_fetch_fields(result);
  for (unsigned int i = 0; i < nFields; i++) {
    nsresult err;
    nsCOMPtr<sashImySQLField> r = 
      do_CreateInstance(SASHMYSQLFIELD_CONTRACT_ID, &err);
    if (err != NS_OK)
      DEBUGMSG(mysql, "Ack, CI of Field object failed!\n");
    nsCOMPtr<sashIConstructor> r_con = do_QueryInterface(r);

    PRBool rv;
    r_con->InitializeNewObject(rt, ext, jscx, 0, 0, &rv);
    if (!rv) 
      DEBUGMSG(mysql, "Ack, initialization of Field object failed!\n");
    r->Field(&fields[i]);
    
    NS_ADDREF((sashImySQLField*)r);
    fieldObjs.push_back((nsISupports*)r);
  }
  
  VariantSetArray(*ret, fieldObjs);
  DEBUGMSG(mysql, "Sending back %d field objects\n", nFields);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetData(nsIVariant **ret) {
//TODO make an empty variant
  *ret = 0;
  if (rowsAffected || error) {
    DEBUGMSG(mysql, "Error or no data\n");
    return NS_OK;
  }
  
  mysql_data_seek(result, 0);
  MYSQL_ROW row;
  vector<nsIVariant*> data;
  while ( (row = mysql_fetch_row(result)) ) {
//TODO fold this code with duplicate from below   
    vector<string> rowVals;
    for (unsigned int i; i < nFields; i++) {
      if (row[i]) rowVals.push_back(row[i]);
      else rowVals.push_back("NULL");
    }
    nsIVariant *r;
    NewVariant(&r);
    VariantSetArray(r, rowVals);
    data.push_back(r);
  }
  
  NewVariant(ret);
  VariantSetArray(*ret, data);
  
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetNextRow(nsIVariant **ret) {
  *ret = 0;
  if (curRow == nRows) {
    DEBUGMSG(mysql, "End of dataset (%d)\n", curRow);
    return NS_OK;
  }
  
  return GetRow(curRow++, ret);
}

NS_IMETHODIMP sashmySQLResult::GetCurrentRow(PRUint32 *n) {
  *n = curRow;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::SetCurrentRow(PRUint32 n) {
  if (n >= nRows) {
    DEBUGMSG(mysql, "Invalid row %d (>= %d)\n", (int)n, 
             (int)mysql_num_rows(result));
    return NS_OK;
  }
  curRow = n;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::GetRow(PRUint32 n, nsIVariant **ret) {
  *ret = 0;
  if (rowsAffected) {
    DEBUGMSG(mysql, "Hey, you can't get a row, there's no data!\n");
    return NS_OK;
  }
  if (error) {
    DEBUGMSG(mysql, "Hey, your query failed and there's an error\n");
    return NS_OK;
  }
  if (n >= nRows) {
    DEBUGMSG(mysql, "Invalid row request %d (>= %d)\n", (int)n, 
             (int)mysql_num_rows(result));
    return NS_OK;
  }
  
  MYSQL_ROW row;
  mysql_data_seek(result, n);
  if ( !(row = mysql_fetch_row(result)) ) {
    DEBUGMSG(mysql, "Null row, or error\n");
    return NS_OK;
  }
  
  NewVariant(ret);
  vector<string> rowVals;
  for (unsigned int i = 0; i < nFields; i++) {
    if (row[i]) rowVals.push_back(row[i]);
    else rowVals.push_back("NULL");
  }
  VariantSetArray(*ret, rowVals);
  DEBUGMSG(mysql, "Sending back row %d, %d fields\n", n, nFields);
  
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::Result(MYSQL_RES *r) {
  result = r;
  nFields = mysql_num_fields(result);
  nRows = mysql_num_rows(result);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::SetRowsAffected(PRUint32 n) {
  rowsAffected = n;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLResult::SetError() {
  error = true;
  return NS_OK;
}
