#include "sashmySQL.h"
#include "sashmySQLConnection.h"
#include "sashmySQLResult.h"
#include "sashmySQLField.h"
#include "extensiontools.h"

NS_DECL_CLASSINFO(sashmySQL);
NS_DECL_CLASSINFO(sashmySQLConnection);
NS_DECL_CLASSINFO(sashmySQLResult);
NS_DECL_CLASSINFO(sashmySQLField);

NS_GENERIC_FACTORY_CONSTRUCTOR(sashmySQL);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashmySQLConnection);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashmySQLResult);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashmySQLField);

static nsModuleComponentInfo components[] = {
  MODULE_COMPONENT_ENTRY(sashmySQL, SASHMYSQL, "mySQL database extension"),
  MODULE_COMPONENT_ENTRY(sashmySQLConnection, 
                         SASHMYSQLCONNECTION,
                         "mySQL connection object"),
  MODULE_COMPONENT_ENTRY(sashmySQLResult,
                         SASHMYSQLRESULT,
                         "mySQL query result object"),
  MODULE_COMPONENT_ENTRY(sashmySQLField,
                         SASHMYSQLFIELD,
                         "mySQL field object")
};

NS_IMPL_NSGETMODULE(sashmySQL, components)


