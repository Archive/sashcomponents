#ifndef SASHMYSQLRESULT_H
#define SASHMYSQLRESULT_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashImySQL.h"
#include <string>
#include <mysql/mysql.h>

#define SASHMYSQLRESULT_CID {0x2b270a4f, 0xa9ba, 0x473e, \
  {0x9b, 0x26, 0xec, 0xfc, 0x9c, 0x04, 0x55, 0x3a}}
  
NS_DEFINE_CID(kSashmySQLResultCID, SASHMYSQLRESULT_CID);

#define SASHMYSQLRESULT_CONTRACT_ID \
  "@gnome.org/SashXB/mysql/mysqlresult;1"
  
class sashmySQLResult : public sashImySQLResult, public sashIConstructor {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHIMYSQLRESULT
  
  sashmySQLResult();
  virtual ~sashmySQLResult();
  
private:
  MYSQL_RES *result;
  sashIActionRuntime *rt;
  sashIExtension *ext;
  sashISecurityManager *secman; 
  JSContext *jscx;
  unsigned int nFields;
  unsigned int nRows;
  unsigned int rowsAffected;
  unsigned int curRow;
  PRBool error;
};    


#endif
