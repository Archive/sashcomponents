#ifndef SASHMYSQLFIELD_H
#define SASHMYSQLFIELD_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashImySQL.h"
#include <string>
#include <mysql/mysql.h>

//{925c73b8-9851-48bb-9fed-7615bc30d1d9}
#define SASHMYSQLFIELD_CID {0x925c73b8, 0x9851, 0x48bb, \
  {0x9f, 0xed, 0x76, 0x15, 0xbc, 0x30, 0xd1, 0xd9}}
  
NS_DEFINE_CID(ksashmySQLFieldCID, SASHMYSQLFIELD_CID);

#define SASHMYSQLFIELD_CONTRACT_ID \
  "@gnome.org/SashXB/mysql/mysqlfield;1"
  
class sashmySQLField : public sashImySQLField, public sashIConstructor {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHIMYSQLFIELD  

  sashmySQLField();
  virtual ~sashmySQLField();
  
private:
  MYSQL_FIELD *field;
  sashISecurityManager *secman;
  JSContext *jscx;
};

#endif


