#ifndef SASHMYSQLCONNECTION_H
#define SASHMYSQLCONNECTION_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashImySQL.h"
#include <string>
#include <mysql/mysql.h>

#define SASHMYSQLCONNECTION_CID {0x281dfec9, 0xce37, 0x4e5d, \
  {0xab, 0x89, 0x3a, 0x7b, 0xe1, 0x86, 0xb1, 0x2b}}
  
NS_DEFINE_CID(kSashmySQLConnectionCID, SASHMYSQLCONNECTION_CID);
  
#define SASHMYSQLCONNECTION_CONTRACT_ID \
  "@gnome.org/SashXB/mysql/mysqlconnection;1"
  
class sashmySQLConnection : public sashImySQLConnection, 
                            public sashIConstructor 
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHIMYSQLCONNECTION
  
  sashmySQLConnection();
  virtual ~sashmySQLConnection();
  
  enum operation {CONNECT, QUERY, STORE, NONE};
  
  struct sem {
    pthread_mutex_t lock;
    pthread_cond_t ready;
    unsigned int val;
    sem(int _v = 0) : val(_v) {
      pthread_mutex_init(&lock, 0);
      pthread_cond_init(&ready, 0);
    }
    ~sem() {}
    void P() {
      pthread_mutex_lock(&lock);
      if (val == 0) 
        pthread_cond_wait(&ready, &lock);
      val--;
      pthread_mutex_unlock(&lock);
    }
    void V() {
      pthread_mutex_lock(&lock);
      val++;
      pthread_cond_signal(&ready);
      pthread_mutex_unlock(&lock);
    }
  };
  
  struct queryReq {
    string q;
    queryReq(const char *_q) {
      q = _q;
    }
  };
  
  struct conReq {
    string host;
    string user;
    string passwd;
    string db;
    conReq(const char *h, const char *u, const char *p, const char *d) {
      host = h;
      user = u;
      passwd = p;
      db = d;
    }
  };
  
private:
  MYSQL mysql;
  pthread_t serverID; 
  sem request;
  operation op;
  void *reqData;
  class sashmySQL *master;
  PRBool error;
  PRBool open;
  
  string onConnectDone;
  string onQueryDone;
  string onStoreDone;
  
  sashIActionRuntime *rt;
  sashIExtension *ext;
  sashISecurityManager *secman; 
  JSContext *jscx;

  static void *serverThread(void *);
  
  friend class sashmySQL;
};  


#endif
