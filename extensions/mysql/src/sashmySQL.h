#ifndef SASHMYSQL_H
#define SASHMYQSL_H

#include "nsID.h"
#include "sashImySQL.h"
#include "sashIExtension.h"
#include "secman.h"
#include <string>
#include <queue>
#include <pthread.h>

//{ec3fd98b-f646-401f-b020-32bd97e692ca}
#define SASHMYSQL_CID {0xec3fd98b, 0xf646, 0x401f, \
  {0xb0, 0x20, 0x32, 0xbd, 0x97, 0xe6, 0x92, 0xca}}

NS_DEFINE_CID(kSashmySQLCID, SASHMYSQL_CID);
  
#define SASHMYSQL_CONTRACT_ID "@gnome.org/SashXB/mysql;1"

class sashmySQL : public sashImySQL, public sashIExtension {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHIMYSQL
  
  sashmySQL();
  virtual ~sashmySQL();
  
private:
  queue<class sashmySQLConnection*> ready; //waiting for "done" notification
  pthread_mutex_t readyLock;
  
  sashIActionRuntime *rt;
  sashISecurityManager *secman; 
  JSContext *jscx;
  sashIGenericConstructor *con;  
  
  friend class sashmySQLConnection;
};

#endif

