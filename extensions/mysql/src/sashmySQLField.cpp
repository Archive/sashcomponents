#include "sashmySQLField.h"
#include "extensiontools.h"
#include "debugmsg.h"
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS2_CI(sashmySQLField, sashImySQLField, sashIConstructor);

sashmySQLField::sashmySQLField() : field(0) {
  NS_INIT_ISUPPORTS();
}

sashmySQLField::~sashmySQLField() {
}

NS_IMETHODIMP sashmySQLField::InitializeNewObject(sashIActionRuntime *rt,
                                                  sashIExtension *ext,
                                                  JSContext *_cx,
                                                  PRUint32 argc,
                                                  nsIVariant **argv,
                                                  PRBool *ret)
{
  rt->GetSecurityManager(&secman);
  jscx = _cx;
  *ret = true;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLField::GetName(char **n) {
  XP_COPY_STRING(field->name, n);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLField::GetTable(char **t) {
  XP_COPY_STRING(field->table, t);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLField::GetDefaultValue(char **d) {
  XP_COPY_STRING(field->def, d);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLField::GetType(FieldType *t) {
  *t = field->type;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLField::GetLength(PRUint32 *l) {
  *l = field->length;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLField::Field(MYSQL_FIELD *f) {
  field = f;
  return NS_OK;
}


