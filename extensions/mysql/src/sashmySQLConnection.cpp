#include "sashmySQL.h"
#include "sashmySQLConnection.h"
#include "sashmySQLResult.h"
#include "extensiontools.h"
#include "debugmsg.h"

NS_IMPL_ISUPPORTS2_CI(sashmySQLConnection, 
                      sashImySQLConnection, 
                      sashIConstructor);

sashmySQLConnection::sashmySQLConnection() {
  NS_INIT_ISUPPORTS();
}

sashmySQLConnection::~sashmySQLConnection() {
  if (open)
    mysql_close(&mysql);
}

NS_IMETHODIMP sashmySQLConnection::InitializeNewObject(sashIActionRuntime *_rt,
                                                       sashIExtension *_ext,
                                                       JSContext *_cx,
                                                       PRUint32 argc,
                                                       nsIVariant **argv,
                                                       PRBool *ret)
{
  rt = _rt;
  ext = _ext;
  rt->GetSecurityManager(&secman);
  jscx = _cx;
  *ret = true;
  
  mysql_init(&mysql);
  nsCOMPtr<sashImySQL> m = do_QueryInterface(ext);
  m->GetMaster(&master);
  op = NONE;
  pthread_create(&serverID, 0, serverThread, this);
  
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::GetOnConnectDone(char **p) {
  XP_COPY_STRING(onConnectDone.c_str(), p);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::SetOnConnectDone(const char *p) {
  onConnectDone = p;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::GetOnQueryDone(char **p) {
  XP_COPY_STRING(onQueryDone.c_str(), p);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::SetOnQueryDone(const char *p) {
  onQueryDone = p;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::GetOnStoreDone(char **p) {
  XP_COPY_STRING(onStoreDone.c_str(), p);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::SetOnStoreDone(const char *p) {
  onStoreDone = p;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::GetError(PRBool *e) {
  if (error) {
    DEBUGMSG(mysql, "Yup, there's an error!\n");
  } else {
    DEBUGMSG(mysql, "Nope, no error, everything's fine...\n");
  }
  *e = error;
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::GetErrorMsg(char **m) {
  if (!error) {
    DEBUGMSG(mysql, "No error, so no message!\n");
    XP_COPY_STRING("No error", m);
  } else {
    DEBUGMSG(mysql, "The secret message is: %s\n", mysql_error(&mysql));
    XP_COPY_STRING(mysql_error(&mysql), m);
  }
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::GetErrorNumber(PRUint32 *n) {
  *n = mysql_errno(&mysql);
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::Quote(const char *s, char **qs) {
  if (!open) 
    DEBUGMSG(mysql, "Oooh, will this work w/o a connection open?\n");
  *qs = (char*)nsMemory::Alloc(2*strlen(s) + 1);
  mysql_real_escape_string(&mysql, *qs, s, strlen(s));
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::Connect(const char *host,
                                           const char *user,
                                           const char *passwd,
                                           const char *db,
                                           PRBool *res)
{
  if (op != NONE) {
    DEBUGMSG(mysql, "Connect requested but connection already busy!\n");
    *res = false;
    return NS_OK;
  }
  if (open) {
    DEBUGMSG(mysql, "Closing old connectoin\n");
    mysql_close(&mysql);
  }
  
  *res = true;
  op = CONNECT;
  DEBUGMSG(mysql, "Requesting connect %s:%s@%s:%s, V\n",user,passwd,host,db);
  (conReq*)reqData = new conReq(host, user, passwd, db);
  request.V();
  
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::Disconnect() {
  if (open) {
    DEBUGMSG(mysql, "Closing connection\n");
    mysql_close(&mysql);
  }
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::Query(const char *q, PRBool *res) {
  if (op != NONE) {
    DEBUGMSG(mysql, "Query requested but connection already busy!\n");
    *res = false;
    return NS_OK;
  }
  if (!open) {
    DEBUGMSG(mysql, "Query requested but connection not open!\n");
    *res = false;
    return NS_OK;
  }
  
  *res = true;
  op = QUERY;
  DEBUGMSG(mysql, "Requesting query \"%s\", op=QUERY, setting reqData, V\n",q);
  (queryReq*)reqData = new queryReq(q);
  request.V();
  
  return NS_OK;
}

NS_IMETHODIMP sashmySQLConnection::Store(PRBool *res) {
  if (op != NONE) {
    DEBUGMSG(mysql, "Store requested but connection already busy\n");
    *res = false;
    return NS_OK;
  }
  if (!open) {
    DEBUGMSG(mysql, "Store requested but connection not open!\n");
    *res = false;
    return NS_OK;
  }
  
  *res = true;
  op = STORE;
  DEBUGMSG(mysql, "Requesting store, V\n");
  request.V();
  
  return NS_OK;
}

void *sashmySQLConnection::serverThread(void *data) {
  sashmySQLConnection *con = (sashmySQLConnection*)data;
  while (1) {
    DEBUGMSG(mysqld, "Server: waiting for request\n");
    con->request.P();
    DEBUGMSG(mysqld, "Woke up!...");
    switch (con->op) {
      
      case CONNECT: {
        conReq *cr = (conReq*)con->reqData;
        DEBUGMSG(mysqld, "Sending connect %s:%s@%s:%s...", cr->user.c_str(), 
                 cr->passwd.c_str(), cr->host.c_str(), cr->db.c_str());
        if (!mysql_real_connect(&con->mysql,
                                cr->host.c_str(),
                                cr->user.c_str(),
                                cr->passwd.c_str(),
                                cr->db.c_str(), 0, 0, 0))
        {  
          DEBUGMSG(mysqld, "failed\n");
          DEBUGMSG(mysqld, "Error: %s\n", mysql_error(&con->mysql));
          con->error = true;
        } else {
          DEBUGMSG(mysqld, "succeeded\n");
          con->error = false;
        }
        delete (conReq*)con->reqData;
        pthread_mutex_lock(&con->master->readyLock);
        con->master->ready.push(con);
        pthread_mutex_unlock(&con->master->readyLock);
        DEBUGMSG(mysqld, "Put connection on ready list, called ProcessEvent\n");
        con->open = true;
        con->rt->ProcessEvent(con->ext);
        break;
      }

      case QUERY: {
        DEBUGMSG(mysqld, "Sending query \"%s\"...", 
                 ((queryReq*)con->reqData)->q.c_str());
        if (mysql_query(&con->mysql, ((queryReq*)con->reqData)->q.c_str())) {
          DEBUGMSG(mysqld, "failed\n");
          DEBUGMSG(mysqld, "Error: %s\n", mysql_error(&con->mysql));
          con->error = true;
        } else {
          DEBUGMSG(mysqld, "succeeded\n");
          con->error = false;
        }
        delete (queryReq*)con->reqData;
        pthread_mutex_lock(&con->master->readyLock);
        con->master->ready.push(con);
        pthread_mutex_unlock(&con->master->readyLock);
        DEBUGMSG(mysqld, "Put connection on ready list, called ProcessEvent\n");
        con->error = false;
        con->rt->ProcessEvent(con->ext);
        break;
      }
      
      case STORE: {
        con->error = false;
        DEBUGMSG(mysqld, "Storing result...");
        MYSQL_RES *result = mysql_store_result(&con->mysql);
        if (!result) {
          if (!mysql_field_count(&con->mysql)) {
            DEBUGMSG(mysqld, "succeeded\n");
            DEBUGMSG(mysqld, "Query affected %d rows\n", 
                     (int)mysql_affected_rows(&con->mysql));
          } else {
            DEBUGMSG(mysqld, "failed\n");
            DEBUGMSG(mysqld, "Query error %s\n", mysql_error(&con->mysql));
            con->error = true;
          }
        } 
        
        nsresult err;
        DEBUGMSG(mysqld, "succeeded\n");
        DEBUGMSG(mysqld, "do_CreateInstance(sashmySQLResult)...");
        nsCOMPtr<sashImySQLResult> sqlresult = 
          do_CreateInstance(SASHMYSQLRESULT_CONTRACT_ID, &err);
        con->reqData = (void*)sqlresult;
        NS_ADDREF((sashImySQLResult*)con->reqData);

        if (err == NS_OK) {
          DEBUGMSG(mysqld, "succeeded\n");
          PRBool ret;
          nsCOMPtr<sashIConstructor> r_con = 
            do_QueryInterface((sashImySQLResult*)con->reqData);
          r_con->InitializeNewObject(con->rt, con->ext, con->jscx, 0, 0, &ret);
          if (ret) {
            if (result) {
              sqlresult->Result(result);
              DEBUGMSG(mysqld, "Passed sashmySQLResult result object...\n");
            } else if (!con->error) {
              sqlresult->SetRowsAffected(mysql_affected_rows(&con->mysql));
              DEBUGMSG(mysqld, "Set sashmySQLResult rows affected %d\n",
                mysql_affected_rows(&con->mysql));
            } else {
              sqlresult->SetError();
              DEBUGMSG(mysqld, "Set sashmySQLResult to error\n");
            }
          } else {
            DEBUGMSG(mysqld, "sashmySQLResult::InitializeNewObject:\n");
            DEBUGMSG(mysqld, "    call failed on query object. What do I do?!");
          }
        } else 
          DEBUGMSG(mysqld, "failed\n");
          
        pthread_mutex_lock(&con->master->readyLock);
        con->master->ready.push(con);
        pthread_mutex_unlock(&con->master->readyLock);
        DEBUGMSG(mysqld, "Put connection on ready list, called ProcessEvent\n");
        con->rt->ProcessEvent(con->ext);
        break;
      }
      
    }
  }
}
