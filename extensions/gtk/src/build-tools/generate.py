'''
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Connects scmexr to the XPCOM-specific classes
'''

import scmexpr
import sys
import os

from state import *

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

class XPCOMParser(scmexpr.Parser):
    def __init__(self, input):
        scmexpr.Parser.__init__(self, input)
        
    def define_enum(self, name, *values):
        Enum(name, values)

    def define_flags(self, name, *values):
        Flags(name, values)

    def define_object(self, name, parent, *args):
        Object(name, parent[0], args)

    def define_func(self, name, retType, args):
        Function(name, retType, args)

    def define_boxed(self, name, *args):
        Boxed(name, args)

    def define_object_extern(self, name, *args):
        print "Define_extern", name

    def include(self, filename):
        if filename[0] != '/':
            afile = os.path.join(os.path.dirname(self.filename),
                                 filename)
        else:
            afile = filename
        if not os.path.exists(afile):
            afile = filename
        fp = open(afile)
        self.startParsing(scmexpr.parse(fp))


def finalize(generator):
    '''After everything has been read in, call this to finalize
    the initialization and write out the code'''
    finish()
    map(generator.genClass, boxedmap.values())
    map(generator.genClass, objmap.values())
    generator.genExtension(objmap.values())
    generator.genLast()
