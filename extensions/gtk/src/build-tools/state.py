'''
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Stores the information extracted from the defs files before the
code is generated.
'''

import sys
import string
import re

# a map from short names (e.g. gtk_label) to objects
shortmap = {}

# a map from names (GtkLabel) to objects
objmap = {}

# a map from function names (gtk_label_set_text) to function objects
funcmap = {}

# a map from enum names to enum objects
enummap = {}

# a map from  boxed names (GdkCursor) to boxed objects
boxedmap = {}

#objects which are not widgets
nonwidgets = {}

#things xpidl won't let me use
keywords = ('string', 'fixed')

#if this is your parent, just use nsISupports
ignoreParents = ('nil', )
    
class Attribute:
    '''An object which represents an Object field'''
    def __init__(self, type, name, object):
        self.type = type
        self.name = name

        # the name xpidl turns this into for C++
        self.capsname = string.capitalize(name)
        
        self.obj = object

accessorRe = re.compile('[gs]et_(.*)')

class Object:
    '''An object which represents a GTK class'''
    
    def __init__(self, name, parent, *args):
        self.name = name
        self.parent = parent
        self.args = args
        self.methods = []
        self.constructors = []
        self._setLowName()
        objmap[name] = self
        shortmap[self.lowname] = self
        self.setFields(args)

    def __repr__(self):
        return "<Object %s>" % self.name

    def setFields(self, args):
        fields = ()
        for arg in args:
            if len(arg) == 0: continue
            if arg[0][0] == "fields":
                fields = arg[0][1:]
                break
        self.fields = []
        for field in fields:
            self.fields.append(Attribute(field[0], field[1], self))

    def finalize(self):
        'do what has to be done after all objects are created'
        self._setParent()

    def pruneObject(self):
        "Do object removal after finalize (yeah, yeah, I know)"
        if 'GtkWidget' not in self.getAncestors():
            nonwidgets[self.name] = self


    def _hasSimilarAttribute(self, method):
        '''Returns whether an attribute exists which
           would conflict with this method'''
        m = accessorRe.match(method.shortname)
        if not m: return 0
        possibleAttr = m.group(1)
        for field in self.fields:
            if field.name == possibleAttr:
                return 1
        return 0
        
    def pruneFunctions(self):
        "Remove functions which were already defined in superclasses"
        newMethods = []
        for method in self.methods:
            if self._hasSimilarAttribute(method):
                sys.stderr.write("Method %s in object %s conflicts with an attribute, skipping method\n"
                                 % (method.shortname, self.name))
            elif self.hasSuper(method.shortname):
                sys.stderr.write("Object %s already has a method called %s in ancestor\n"
                                 % (self.name, method.shortname))
            else:
                newMethods.append(method)
        self.methods = newMethods

    def hasSuper(self, shortname):
        '''Return whether the given function already
        exists in in an ancestor'''
        for obj in self.getAncestors():
            for method in obj.methods:
                if method.shortname == shortname: return 1
        return 0

    def getAncestors(self):
        '''Return a list of the ancestors of this object, in reverse order'''
        if not self.parentObj: return []
        ret = self.parentObj.getAncestors()
        ret.append(self.parentObj)
        return ret

    def getDict(self):
        dict = getattr(self, 'dict', None)
        if not dict:
            dict = {'name' : self.name,
                    'exposedname' : self.name,
                    'capsname' : string.upper(self.name),
                    'castname' : string.upper(self.lowname),
                    'classname' : 'sash' + self.name,
                    'lowname' : self.lowname,
                    'parent' : self.parent,
                    'parentclass' : 'sash' + self.parent,
                    'interface' : 'sashI' + self.name,
                    'interfacecaps' : string.upper('sashI' + self.name),
                    'parentinterface' : 'sashI' + self.parent,
                    'methoddecls' : ''}
            if self.parent in ignoreParents:
                dict['parentinterface'] = 'nsISupports'
            self.dict = dict
        return self.dict

    def _setParent(self):
        '''find and register the parent object; this
        cannot be done until the objects have all been created'''
        if self.parent in ignoreParents:
            self.parentObj = None
        else:
            self.parentObj = objmap.get(self.parent, None)
            if self.parentObj == None:
                print "Could not find parent for", self.name

    def _setLowName(self):
        """set the expanded version of the name, like gtk_widget.
        this is not pretty. It is now even less pretty"""
        name = [string.lower(self.name[0])]
        lastcaps = 1 # whether the last letter was capital
        beforelastcaps = 0 #whether the letter before that was
        for pos in range(1, len(self.name)):
            temp = lastcaps
            letter = self.name[pos]
            if letter in string.lowercase:
                name.append(letter)
                lastcaps = 0
            else:
                if not lastcaps and not beforelastcaps:
                    name.append('_')
                name.append(string.lower(letter))
                lastcaps = 1
            beforelastcaps = temp
        self.lowname = string.join(name, '')

    def addMethod(self, func):
        '''Add the function as a method on this object'''
        if not func.isConstructor:
            self.methods.append(func)

    def __str__(self):
        ret = 'Object:\n  ' + self.name + '\n'
        if self.methods:
            ret = ret + 'Methods:\n'
            for func in self.methods:
                ret = ret + '  ' + str(func) + '\n'
        return ret

class Boxed(Object):
    '''A class which represents a GTK boxed type, whatever that is...'''
    def __init__(self, name, args):
        Object.__init__(self, name, 'nil', ())
        boxedmap[name] = self
        shortmap[self.lowname] = self
        del objmap[name]
        if len(args) > 0:
            self.incref = args[0]
            self.decref = args[1]
        else:
            self.incref = ''
            self.decref = ''

    def addMethod(self, method):
        pass
    
constructre = re.compile('(.*)_new')

class Function:
    """A class which represents a GTK function"""
    def __init__(self, name, retType, args):
        funcmap[name] = self
        self.name = name

        self.isConstructor = constructre.search(name)

        #TODO - deal with null-ok
        if type(retType) == type(()):
            retType = retType[0]
        self.retType = retType

        self.args = []
        for arg in args: 
            argtype, argname = arg[:2]
            if argname in keywords:
                argname = argname + '_x'
            self.args.append([argtype, argname])

    def finalize(self):
        'do what has to be done after all objects and functions are created'
        self._findObject()
        self._setShortName()
        self._setInterCaps()

    def _findObject(self):
        'find which object this is a method of'
        self.object = None

        #constructors attach to the object they create
        if self.isConstructor:

            # constructors return a GtkWidget, which is useless here
            # so we need to compare the short names (like gtk_label) to
            # the constructor (gtk_label_new)

            shortname = constructre.search(self.name).group(1)
            if shortmap.has_key(shortname):
                self.object = shortmap[shortname]
            self.object.constructors.append(self)
            
            return # don't attach it as a regular method

        #methods attach to the object which is their first argument
        if len(self.args) == 0:
            return
        arg1type = self.args[0][0]
        if objmap.has_key(arg1type):
            objmap[arg1type].addMethod(self)
            # a circular reference; we'll live
            self.object = objmap[arg1type]

    def __str__(self):
        return self.retType + ' ' + self.name + '(' + self._argString() + ')'

    def _argString(self):
        args = []
        for type, name in self.args:
            args.append(type + ' ' + name)
        return string.join(args, ', ')

    def _setShortName(self):
        "Set the function name without the class part in front of it"
        if self.object:
            self.shortname = string.replace(self.name,
                                            self.object.lowname + '_', '', 1)
        else:
            self.shortname = ''
            
    def _setInterCaps(self):
        "Set what the function name will be in IDL and in C++"
        newname = []
        lastUnderscore = 0
        for c in self.shortname:
            if c == '_':
                lastUnderscore = 1
                continue
            if lastUnderscore:
                newname.append(string.upper(c))
            else:
                newname.append(c)
            lastUnderscore = 0
        newname = string.join(newname, '')
        self.idlname = newname
        self.cname = string.upper(newname[:1]) + newname[1:]

gtkre = re.compile('GTK_(.*)')

class Enum:
    '''A class which represents a GTK enum'''

    def __init__(self, name, values):
        enummap[name] = self
        self.name = name
        self.values = {}
        for pos in range(len(values)):
            if len(values[pos]) != 2:
                print "Could not parse enum", values[pos]
                continue
            key = values[pos][0]
            value = values[pos][1]
            self.values[key] = value
        self._setShortNames()

    def _setShortNames(self, dict = {}):
        "Strip any GTK_'s from the beginning of the constants. Leave GDK_"
        self.shortnames = {}
        for value in self.values.values():
            m = gtkre.match(value)
            if m:
                self.shortnames[m.group(1)] = value
            else:
                self.shortnames[value] = value

class Flags(Enum):
    pass
        
def finishObjects():
    for obj in objmap.values(): obj.finalize()
    for obj in boxedmap.values(): obj.finalize()

def finishFunctions():
    for func in funcmap.values(): func.finalize()

def prune():
    "Prune functions which are defined in superclasses"
    for name, obj in objmap.items():
        obj.pruneFunctions()
        obj.pruneObject()

def finish():
    '''finish up after the objects have been created'''
    finishObjects()
    finishFunctions()
    prune()
