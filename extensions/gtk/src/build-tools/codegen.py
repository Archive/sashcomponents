'''
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Contains functions which will actually output code for the classes
'''


import state
import os
import string
import uuid
import sys
import re

commentre = re.compile('^#.*')

# converts the types from the defs file to types for IDL
IDLtypes = {'string' : 'string',
            'none' : 'void',
            'static_string' : 'string',
            'uint' : 'PRUint32',
            'int' : 'PRInt32',
            'bool' : 'PRBool',
            'float' : 'float'}

# converts the types from the defs file to types for C++
ctypes = IDLtypes.copy()
ctypes['string'] = 'const char *'
ctypes['static_string'] = 'const char *'

# converts the types from the normall C++ types
# to what they should be if they're _retVal
ctypereturn = ctypes.copy()
ctypereturn['string'] = ctypereturn['static_string'] = 'char *'

factorycall = '''
nsresult createrv = WrapObject(GTK_OBJECT(c_retVal), (void **) _retVal);
if (!NS_SUCCEEDED(createrv))
  return createrv;
'''

staticstringconvert = '''*_retVal = XP_STRDUP(*_retVal);'''

stringconvert = '''
char *andrew_tmp = XP_STRDUP(*_retVal);
g_free(*_retVal);
*_retVal = andrew_tmp;
'''

isupportsInherit = "SASH_IMPL_ISUPPORTS_INHERITED_CI2(%(classname)s, sash%(parent)s, %(interface)s, sashIConstructor);"

isupportsNoInherit = "NS_IMPL_ISUPPORTS2_CI(%(classname)s, %(interface)s, sashIConstructor);"

constructorInherit = "// NS_INIT_ISUPPORTS() done in superclass"
constructorNoInherit = "NS_INIT_ISUPPORTS();"

inheritDict = {'implisupports' : isupportsInherit,
               'constructor' : constructorInherit,
               'declisupports' : 'NS_DECL_ISUPPORTS_INHERITED'}
noInheritDict = {'implisupports' : isupportsNoInherit,
               'constructor' : constructorNoInherit,
                 'declisupports' : 'NS_DECL_ISUPPORTS'}

def getCType(deftype, typedict = ctypes):
    '''Convert a type used in the defs file to one appropriate for C++'''
    if typedict.has_key(deftype):
        return typedict[deftype]
    elif state.objmap.has_key(deftype) or state.boxedmap.has_key(deftype):
        return 'sashI' + deftype + ' *'
    elif state.enummap.has_key(deftype):
        return 'PRInt32'
    else: raise ValueError, "Unknown type " + deftype
    
def indentCode(codestr):
    ret = []
    for line in string.split(codestr, '\n'):
        ret.append('  ' + line)
    return string.join(ret, '\n')

class Implementor:
    '''Creates the function bodies'''

    def __init__(self, object, method):
        self.object = object
        self.method = method
        self.setArgs()

    def setArgs(self):
        self.cargs = []
        for argtype, argname in self.method.args:
            isgtk = 0
            isenum = 0
            isboxed = 0
            
            # things like int can be left alone
            if argtype in ctypes.keys():
                ctype = argtype
                cname = argname

            elif self.isEnum(argtype):
                ctype = 'PRInt32'
                cname = argname
                isenum = 1
            
            #but widgets will need to be extracted and stored in temps
            elif self.isObject(argtype):
                ctype = 'GtkObject *'
                cname = 'raw_' + argname
                isgtk = 1

            elif self.isBoxed(argtype):
                ctype = argtype + ' *'
                cname = 'raw_' + argname
                isboxed = 1

            else:
                raise ValueError, "Unknown type " + argtype
            
            arg = {'ctype' : ctype,
                   'cname' : cname,
                   'isgtk' : isgtk,
                   'isenum' : isenum,
                   'isboxed' : isboxed,
                   'argtype' : argtype,
                   'argname' : argname}
            self.cargs.append(arg)
            

    def isEnum(self, ctype):
        return state.enummap.has_key(ctype)
        
    def isObject(self, ctype):
        return state.objmap.has_key(ctype)

    def isWidget(self, ctype):
        return self.isObject(ctype) and not state.nonwidgets.has_key(ctype)

    def isBoxed(self, ctype):
        return state.boxedmap.has_key(ctype)
    
    def isWrapped(self, ctype):
        '''Return whether the object is wrapped in XPCOM.
        ctype is the type from the defs file'''
        return state.objmap.has_key(ctype) or state.boxedmap.has_key(ctype)
        
    def storeReturn(self):
        '''Return text to store the return value if necessary'''
        if self.method.retType == 'none': return ''
        if self.isWidget(self.method.retType):
            return 'GtkObject * c_retVal = '
        elif self.isObject(self.method.retType) or self.isBoxed(self.method.retType):
            return '%s * c_retVal = ' % self.method.retType
        elif self.isEnum(self.method.retType):
            return '*_retVal = (PRInt32) '
        else:
            return '*_retVal = '

    def convertReturn(self):
        "Convert the return value before exiting if necessary"
        if self.isObject(self.method.retType):
            return factorycall
        elif self.method.retType == 'static_string':
            return staticstringconvert
        elif self.method.retType == 'string':
            return stringconvert
        else:
            return ''

    def getResult(self):
        "return the actual C function call"
        ret = []
        ret.append(self.method.name)
        ret.append('(')
        ret.append(self.argList())
        ret.append(');')
        return string.join(ret, '')

    def declareTemps(self):
        "Declare tempororary values"
        ret = []
        #skip first argument, the object
        
        for arg in self.cargs[1:]:
            argtype = arg['argtype']

            # Boxed types will have their own specialized GetObject call
            if self.isObject(argtype) or self.isBoxed(argtype):
                ret.append(arg['ctype'] + ' ' + arg['cname'] + ';')
                line = '''if (%(argname)s)
  %(argname)s->GetObject(&%(cname)s);
else
  %(cname)s = NULL;'''
                ret.append(line % arg)
                
        return string.join(ret, '\n')
    
    def argList(self):
        "Return the argument list"
        ret = []

        #the first argument is the object
        obj = state.objmap[self.cargs[0]['argtype']]
        ret.append('%s(m_object)' % obj.getDict()['castname'])
        
        for dict in self.cargs[1:]:
            if dict['isgtk']:
                obj = state.objmap[dict['argtype']]
                dict['castcall'] = string.upper(obj.lowname)
                ret.append('%(castcall)s(%(cname)s)' % dict)
            elif dict['isenum']:
                ret.append('(%(argtype)s)(%(cname)s)' % dict)
            else:
                ret.append(dict['cname'])
        return string.join(ret, ', ')
            
    def __str__(self):
        ret = []
        ret.append(self.declareTemps())
        ret.append(self.storeReturn() + self.getResult())
        ret.append(self.convertReturn())
        ret.append("return NS_OK;")
        return indentCode(string.join(ret, '\n'))


class AttributeImplementor(Implementor):
    '''Creates the function bodies for attribute accessors'''
    def __init__(self, object, type, name):
        self.object = object
        self.type = type
        self.name = name

    def declareTemps(self):
        return ''

    def declareSetTemps(self):
        return ''

    def storeReturn(self):
        return '*_retVal = '

    def storeSetReturn(self):
        return self._objectCast() + '->' + self.name + ' = '

    def _objectCast(self):
        return '%s(m_object)' % self.object.getDict()['castname']
    
    def getResult(self):
        return self._objectCast() + '->' + self.name + ';'

    def getSetResult(self):
        return '_inVal;'

    def convertReturn(self):
        return ''

    def convertSetReturn(self):
        return ''

    def getterCode(self):
        return str(self)

    def setterCode(self):
        ret = []
        ret.append(self.declareSetTemps())
        ret.append(self.storeSetReturn() + self.getSetResult())
        ret.append(self.convertSetReturn())
        ret.append("return NS_OK;")
        return indentCode(string.join(ret, '\n'))

settempdecl = '''
GtkObject *tempobj;
_inVal->GetObject(&tempobj);
'''

class WidgetAttributeImplementor(AttributeImplementor):
    def __init__(self, object, type, name):
        assert(self.isObject(type))
        AttributeImplementor.__init__(self, object, type, name)

    def declareSetTemps(self):
        return settempdecl

    def storeReturn(self):
        return self.type + " *c_retVal = "

    def getSetResult(self):
        obj = state.objmap[self.type]
        return '%s(tempobj);' % obj.getDict()['castname']

    def convertReturn(self):
        return factorycall

class BoxedAttributeImplementor(AttributeImplementor):
    def __init__(self, object, type, name):
        assert(self.isBoxed(type))
        AttributeImplementor.__init__(self, object, type, name)

    def declareTemps(self):
        return 'return NS_ERROR_NOT_IMPLEMENTED;'

    def declareSetTemps(self):
        return 'return NS_ERROR_NOT_IMPLEMENTED;'

    def storeReturn(self):
        return getCType(self.type) + ' *c_retVal = '

    def storeSetReturn(self):
        return ''

def AttributeImplementorFactory(object, type, name):
    if state.objmap.has_key(type):
        return WidgetAttributeImplementor(object, type, name)
    elif state.boxedmap.has_key(type):
        return BoxedAttributeImplementor(object, type, name)
    else:
        return AttributeImplementor(object, type, name)

def GeneratorFactory(object):
    if state.objmap.has_key(object.name):
        return ObjectGenerator(object)
    elif state.boxedmap.has_key(object.name):
        return BoxedGenerator(object)
    else:
        raise ValueError

class EmptyConstructor:
    def __init__(self):
        pass

    def _getArgNums(self):
        return 0

    def _getInitArgs(self):
        return ''

    def _getCCall(self):
        return ''

    def getDict(self):
        return {'argnums' : self._getArgNums(),
                'initargs' : self._getInitArgs(),
                'ccall' : self._getCCall()}


argDecl = '''
  nsIVariant *cur_var;
'''

convertVariant ='''
  %(ctype)s %(cname)s;
  cur_var = xp_argv[%(cur_pos)s];
  {
    %(ret_type)s sash_temp_ret;
    if (!VariantIs%(cur_type)s(cur_var)) {
        *xp_ret = false;
        return NS_OK;
    }
	sash_temp_ret = VariantGet%(variant_get)s(cur_var);
    %(cname)s = %(convert)s;
  }
'''

nocast = ('string', )
class ConstructorGen(EmptyConstructor):
    ret_type =  {'PRUint32' : 'double',
                 'PRInt32' : 'double',
                 'PRBool' : 'bool',
                 'float' : 'double',
                 'const char *' : 'string'}
    cur_type = {'PRUint32' : 'Number',
                'PRInt32' : 'Number',
                'PRBool' : 'Boolean',
                'float' : 'Number',
                'const char *' : 'String'}
    variant_get = {'PRUint32' : 'Number',
                   'PRInt32' : 'Number',
                   'PRBool' : 'Boolean',
                   'float' : 'Number',
                   'const char *' : 'String'}
    convert = {'PRUint32' : '(PRUint32) (sash_temp_ret + 0.5)',
               'PRInt32' : '(PRInt32) (sash_temp_ret + 0.5)',
               'PRBool' : '(PRBool) sash_temp_ret',
               'float' : '(float) sash_temp_ret',
               'const char *' : 'sash_temp_ret.c_str()'}

    # TODO: Free strings which get converted
    
    def __init__(self, const):
        self.const = const

    def _getArgNums(self):
        return len(self.const.args)

    def _getInitArgs(self):
        lines = []
        first = 1
        for pos in range(len(self.const.args)):
            argType, argName = self.const.args[pos]
            if pos == 0:
                lines.append(argDecl)
            cType = getCType(argType)
            if cType not in self.ret_type.keys():
                raise NotImplementedError, "Unknown type %s" % cType
            dict = {'ctype' : cType,
                    'cname' : argName,
                    'cur_pos' : pos,
                    'ret_type' : self.ret_type[cType],
                    'cur_type' : self.cur_type[cType],
                    'variant_get' : self.variant_get[cType],
                    'convert' :  self.convert[cType]}
            lines.append(convertVariant % dict)
        return string.join(lines, '\n')

    def _getCCall(self):
        args = []
        for argType, argName in self.const.args:
            cast = ''
            if argType not in nocast:
                cast = '(%s)' % argType
            args.append('%s %s' % (cast, argName))
        args = string.join(args, ',')

        if state.objmap.has_key(self.const.object.name):
            cast = 'GtkObject*'
        else:
            cast = self.const.object.name + '*'
        return 'm_object = (%s) %s(%s);' % (cast, self.const.name, args)

class ObjectGenerator:

    def __init__(self, obj):
        self.obj = obj

        # store objects which we use as arguments or return types
        # so we can include their headers 
        self.undefined = []

    def getConstructor(self):
        if len(self.obj.constructors):
            return ConstructorGen(self.obj.constructors[0])
        else:
            return EmptyConstructor()

    def getTypedefs(self):
        "Return typedefs for the IDL"
        return ''

    def getPrivateDecl(self):
        "Return any private declarations for the wrapper object"
        return ''
    
    def getIDLType(self, deftype):
        '''Convert a type used in the defs file to one appropriate for IDL'''
        if IDLtypes.has_key(deftype):
            return IDLtypes[deftype]
        elif state.objmap.has_key(deftype) or state.boxedmap.has_key(deftype):
            ret = 'sashI' + deftype
            if ret not in self.undefined:
                self.undefined.append(ret)
        elif state.enummap.has_key(deftype):
            ret = 'PRInt32'
        else:
            raise ValueError, "Unknown type " + deftype
        return ret

    def _IDLArgs(self, method):
        '''Return a the declaration of the arguments for the idl'''
        ret = []

        #skip the first argument, which is the object
        for argtype, argname in method.args[1:]:
            prefix = 'in'
            ret.append(prefix + ' ' + self.getIDLType(argtype) + ' ' + argname)
        return string.join(ret, ', ')
    
    def _cArgs(self, method):
        '''Return a the declaration of the arguments for the cpp file'''
        ret = []
        #skip the first argument, which is the object
        for argtype, argname in method.args[1:]:
            ret.append(getCType(argtype) + ' ' + argname)

        # put the return type as the last argument
        if method.retType != "none":
            ret.append(getCType(method.retType, ctypereturn) + '*' + '_retVal')
        return string.join(ret, ', ')
    
    def getIDL(self):
        '''Return the IDL declarations for the object methods'''
        methodDeclarations = []
        
        # generate attributes
        for field in self.obj.fields:
            decl = ['   attribute', self.getIDLType(field.type),
                    field.name, ';']
            decl = string.join(decl)
            methodDeclarations.append(decl)

        # generate methods
        for method in self.obj.methods:
            decl = ['  ', self.getIDLType(method.retType),
                    method.idlname,
                    '(', self._IDLArgs(method), ');']
            decl = string.join(decl)
            methodDeclarations.append(decl)
        return string.join(methodDeclarations, '\n')

    def _getCDecl(self, method):
        '''Common C declaration stuff'''
        decl = [method.cname, '(', self._cArgs(method), ')']
        return string.join(decl, '')
    
    def _getAttrDecl(self, getOrSet, name, type):
        '''Return the functionname and argument list for an attribute'''
        decl = [getOrSet + name, '(', getCType(type)]
        if getOrSet == "Get":
            decl.append(' *_retVal')
        else:
            decl.append(' _inVal')
        decl.append(')')
        return string.join(decl, '')
    
    def _implDec(self, decl, implementation):
        "return the first part of the implementation, declaring it"
        impl = ['NS_IMETHODIMP']
        impl.append(self.obj.getDict()['classname'] + '::' + decl)
        impl.append('{')
        impl.append(implementation)
        impl.append('}')
        return string.join(impl, '\n')

    def getCImpl(self):
        '''Return the C++ implementations of the functions'''
        methodImplementations = []

        for field in self.obj.fields:
            implementor = AttributeImplementorFactory(self.obj,
                                                      field.type,
                                                      field.name)

            # produce the code for the getter
            decl = self._getAttrDecl('Get', field.capsname, field.type)
            impl = self._implDec(decl, implementor.getterCode())
            methodImplementations.append(impl)

            # produce the code for the setter
            decl = self._getAttrDecl('Set', field.capsname, field.type)
            impl = self._implDec(decl, implementor.setterCode())
            methodImplementations.append(impl)

        for method in self.obj.methods:
            decl = self._getCDecl(method)
            code = str(Implementor(self.obj, method))
            impl = self._implDec(decl, code)
            methodImplementations.append(impl)

        return string.join(methodImplementations, '\n\n')

#TODO: Reference counting functions
boxedimpl = """
NS_IMETHODIMP
%(classname)s::GetObject(%(name)s **_retVal)
{
  *_retVal = m_object;
  return NS_OK;
}

NS_IMETHODIMP
%(classname)s::SetObject(%(name)s *inval)
{
  m_object = inval;
  return NS_OK;
}
"""

getter_impl = '''
NS_IMETHODIMP
sashGtkExtension::Get%(exposedname)s(sashIGenericConstructor **ret)
{
  if(m_%(exposedname)s == NULL) {
    NewSashConstructor(m_rt, this, SASH%(capsname)s_CONTRACT_ID, %(interfacecaps)s_IID_STR, &m_%(exposedname)s);
  }
  *ret = m_%(exposedname)s;
  NS_ADDREF(*ret);
  return NS_OK;
}'''

const_getter = '''
NS_IMETHODIMP
sashGtkExtension::Get%s(PRInt16 *ret)
{
  if (!ret) return NS_ERROR_NULL_POINTER;
  *ret = %s;
  return NS_OK;
}'''

destroy_impl = '''NS_IF_RELEASE(m_%(exposedname)s);'''

class BoxedGenerator(ObjectGenerator):
    def getTypedefs(self):
        return "[ptr] native native%(name)s(%(name)s);" % self.obj.getDict()
    
    def getIDL(self):
        return '  [noscript] attribute native%(name)s object;' % self.obj.getDict()
    
    def getCImpl(self):
        return boxedimpl % self.obj.getDict()

    def getPrivateDecl(self):
        "Return any private declarations for the wrapper object"
        return '%(name)s *m_object;\nnsCOMPtr<sashIActionRuntime> m_rt;\nsashIExtension *m_ext;\nJSContext *m_cx;' % self.obj.getDict()

class CodeGenerator:
    ignores = []

    def __init__(self, indir, outdir):
        self.indir = indir
        self.outdir = outdir
        self._setIgnores()

        self.idltemplate = self._readFile('template.idl')
        self.cpptemplate = self._readFile('template.cpp')
        self.headertemplate = self._readFile('template.h')
        self.moduletemplate = self._readFile('templatemodule.cpp')
        self.factorytemplate = self._readFile('factorytemplate.h')
        self.mktemplate = self._readFile('template.mk')
        self.extensiontemplatecpp = self._readFile('templateextension.cpp')
        self.extensiontemplateh = self._readFile('templateextension.h')
        self.extensiontemplateidl = self._readFile('templateextension.idl')
        
        self.objects = [] # objects which have been generated
        self.skipped = [] # objects which were skipped

    def _readFile(self, relname):
        '''Return the string data from the specified file. The relative path is
        turned into an absolute path in the input directory'''
        return open(os.path.join(self.indir, relname)).read()
    
    def _setIgnores(self):
        '''Load a file which lists classes not to generate'''
        self.ignores = []
        filename = os.path.join(self.indir, 'ignores')
        try:
            f = open(filename)
        except IOError:
            sys.stderr.write('Could not open ignores file "%s"\n' % filename)
            return
        for line in f.readlines():
            if not commentre.match(line):
                self.ignores.append(string.strip(line))
                
    def _includeString(self, prefix, outfile):

        '''Look at an optional file which gives additional information
        to be inserted in the autogenerated code. Return an empty
        string if it is not found. outfile is the name of the file it
        will be included in prepend "include_" to get the filename
        which will be included'''
        
        try:
            return self._readFile(prefix + outfile)
        except:
            return ''

    def genLast(self):
        """Generate everything that's not an object's source code"""
        os.chdir(self.outdir)
        try:
            self._genModule()
            self._genFactory()
            self._genMake()
        finally:
            os.chdir('..')
        
    def _genModule(self):
        '''Generate the code needed to make this a module'''
        print 'Generating module ...',
        dict = {'includes' : self._moduleIncludes(),
                'factories' : self._moduleFactories(),
                'components' : self._moduleComponents()}
        filename = 'sashGtkModule.cpp'
        f = open(filename, 'w')
        f.write(self.moduletemplate % dict)
        f.close()
        print 'Done'

    def _genFactory(self):
        '''Generate the file to be included in the factory'''
        print 'Generating factory include ...',
        dict = {'includes' : self._moduleIncludes(),
                'classinfos' : self._classInfos()}
        filename = 'GtkObjectFactorySupport.h'
        f = open(filename, 'w')
        f.write(self.factorytemplate % dict)
        f.close()
        print 'Done'

    def _genMake(self):
        '''Generate the file which will be included in the Makefile.am'''

        filename = 'sashgtk.mk'
        print 'Generating %s ...' % filename,
        dict = {'idlfiles' : self._idlFiles(),
                'sources' : self._sources()}
        f = open(filename, 'w')
        f.write(self.mktemplate % dict)
        f.close()
        print 'Done'

    def _idlFiles(self):
        idlfiles = map(lambda x: '\t%(interface)s.idl' % x.getDict(),
                       self.objects)
        return string.join(idlfiles, ' \\\n')

    def _sources(self):
        sources = map(lambda x: '\t%(classname)s.h %(classname)s.cpp' % x.getDict(),
                      self.objects)
        return string.join(sources, ' \\\n')
    
    def _moduleIncludes(self):
        includes = map(lambda x: '#include "%(classname)s.h"' % x.getDict(), self.objects)
        return string.join(includes, '\n')

    def _moduleComponents(self):
        components = map(lambda x: '  MODULE_COMPONENT_ENTRY(%(classname)s, SASH%(capsname)s, "%(name)s")' % x.getDict(),
                         self.objects)
        return string.join(components, ',\n')

    def _moduleFactories(self):
        factories = map(lambda x: 'NS_GENERIC_FACTORY_CONSTRUCTOR(%(classname)s)\nNS_DECL_CLASSINFO(%(classname)s)' % x.getDict(),
                        self.objects)
        return string.join(factories, '\n')

    def _classInfos(self):
        def info(obj):
            "return a ClassInfo struct for an object"
            dict = obj.getDict()
            return '{"%(name)s", SASH%(capsname)s_CONTRACT_ID, %(interfacecaps)s_IID}' % dict #, %(lowname)s_get_type()}' % dict
        
        classinfos = map(info, self.objects)
        return string.join(classinfos, ',\n')

    def genExtension(self, objects):
        '''Generate the extension files'''
        os.chdir(self.outdir)
        try:
            objects = filter(lambda x, ignores=self.ignores:
                             x.name not in ignores, objects)
            self._genExtIDL(objects)
            self._genExtCPP(objects)
            self._genExtHeader(objects)
        finally:
            os.chdir('..')

    def _genExtIDL(self, objects):
        f = open('sashIGtkExtension.idl','w')
        data = self.extensiontemplateidl

        line = '  readonly attribute sashIGenericConstructor %(exposedname)s;'
        lines = map(lambda x, line=line: line % x.getDict(), objects)
        attributes = string.join(lines, '\n')

        dict = {'attributes' : attributes,
                'enums' : self._genConstants()}
        f.write(data % dict)
        f.close()

    def _genExtHeader(self, objects):
        f = open('sashGtkExtension.h','w')
        data = self.extensiontemplateh

        line = 'sashIGenericConstructor *m_%(exposedname)s;'
        lines = map(lambda x, line=line: line % x.getDict(), objects)
        members = string.join(lines, '\n')

        dict = {'attribute_members' : members}
        f.write(data % dict)
        f.close()

    def _genExtCPP(self, objects):
        f = open('sashGtkExtension.cpp', 'w')
        data = self.extensiontemplatecpp

        line = '   m_%(exposedname)s(NULL)'
        lines = map(lambda x, line=line: line % x.getDict(), objects)
        inits = string.join(lines, ',\n')

        lines = map(lambda x, line=getter_impl: line % x.getDict(), objects)
        getters = string.join(lines, '\n')

        line = 'NS_METHOD_GETTER(sashGtkExtension::Get%s, PRInt16, %s)'
        lines = []
        for enum in state.enummap.values():
            for short, long in enum.shortnames.items():
                lines.append(const_getter % (short, long))
        enum_getters = string.join(lines, '\n')
                    

        line = '#include "sash%(name)s.h"'
        lines = map(lambda x, line=line: line % x.getDict(), objects)
        includes = string.join(lines, '\n')

        lines  = map(lambda x, line=destroy_impl: line % x.getDict(), objects)
        destructors = string.join(lines, '\n')
        
        dict = {'attribute_init' : inits,
                'attribute_getters' : getters,
                'attribute_destroy' : destructors,
                'includes' : includes,
                'enum_getters' : enum_getters}
        f.write(data % dict)
        f.close()
        
    def _genConstants(self, dict = {}):
        "return a list of the constants declared in the toplevel object"
        lines = []
        for enum in state.enummap.values():
            for name in enum.shortnames.keys():
                line = '  readonly attribute short %s;' % name
                lines.append(line)
        return string.join(lines, '\n')

    def genClass(self, object):
        '''Generate all the code needed for a given object to
        be wrapped into XPCOM'''

        if object.name in self.ignores:
            print 'Skipping', object.name
            self.skipped.append(object)
            return

        self.objects.append(object)

        print 'Generating', object.name,'...',

        os.chdir(self.outdir)
        objgen = GeneratorFactory(object)
        try:
            self._genIDL(object, objgen)
            self._genCPP(object, objgen)
            self._genHeader(object, objgen)
        finally:
            os.chdir('..')
            
        print 'done'


    def updateDict(self, object, dict):
        "Update the dictionary for the inheritenc-specific values"
        if dict['parent'] == 'nil':
            for key, value in noInheritDict.items():
                dict[key] = value % dict
        else:
            for key, value in inheritDict.items():
                dict[key] = value % dict
            
    def _genIDL(self, object, objgen):
        '''Generate the IDL file'''

        dict = object.getDict()
        filename = dict['interface'] + '.idl'

        dict['uuid'] = uuid.getUUID()
        dict['methods'] = objgen.getIDL()
        dict['typedefs'] = objgen.getTypedefs()
        dict['extra'] = self._includeString('include_', filename)
        interfaces = map(lambda x: 'interface %s;' % x,
                                    objgen.undefined)
        dict['arginterfaces'] = string.join(interfaces, '\n')

        f = open(filename, 'w')
        f.write(self.idltemplate % dict)
        f.close()


    def _genCPP(self, object, objgen):
        '''Generate the C++ implementation source file'''
        dict = object.getDict()
        filename = 'sash' + dict['name'] + '.cpp'

        dict['methodimpls'] = objgen.getCImpl()
        dict['extra'] = self._includeString('include_', filename)
        undefined = map(lambda x: '#include <%s.h>' % x,
                        objgen.undefined)
        dict['includes'] = string.join(undefined, '\n')
        self.updateDict(object, dict)
        
        myconst = objgen.getConstructor()
        try:
            dict.update(myconst.getDict())
        except NotImplementedError, e:
            dict.update(EmptyConstructor().getDict())
            print "Constructor not implmented:", e
        
        f = open(filename, 'w')
        f.write(self.cpptemplate % dict)
        f.close()

    noForward = ()
    def getForwards(self, object, dict):
        ret = []
        ancestors = object.getAncestors()
        for obj in ancestors:
            if obj.name in self.noForward: continue
            dict['curclass'] = string.upper(obj.name)
            ret.append('  NS_FORWARD_SASHI%(curclass)s(%(parentclass)s::)'
                       % dict)
        return string.join(ret, '\n')
    
    def _genHeader(self, object, objgen):
        '''Generate the C++ header file'''
        dict = object.getDict()

        filename = 'sash' + dict['name'] + '.h'

        dict['capsparent'] = string.upper(dict['parent'])
        dict['uuid'] = uuid.getUUID()
        dict['biguuid'] = uuid.conv(dict['uuid'])
        dict['private'] = objgen.getPrivateDecl()
        dict['extra'] = self._includeString('include_', filename)
        undefined = map(lambda x: 'class %s;' % x,
                        objgen.undefined)
        dict['declares'] = string.join(undefined, '\n')

        dict['forward'] = self.getForwards(object, dict)
        self.updateDict(object, dict)
            
        if dict['parent'] not in state.ignoreParents:
            dict['parentinclude'] = '#include "%(parentclass)s.h"' % dict
            dict['inherit'] = ", public " + dict['parentclass']
        else:
            dict['inherit'] = dict['parentinclude'] = dict['forward'] = ''

        f = open(filename, 'w')
        f.write(self.headertemplate % dict)
        f.close()
