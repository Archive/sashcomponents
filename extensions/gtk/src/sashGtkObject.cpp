
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashGtkObject.h"
#include "extensiontools.h"
#include "sashIGtkExtension.h"
#include "sashIExtension.h"

#include <nsIGenericFactory.h>
#include <sashIGtkObjectFactory.h>
#include <gtk/gtk.h>
#include <assert.h>

NS_IMPL_ISUPPORTS1_CI(sashGtkObject, sashIGtkObject);

sashGtkObject::sashGtkObject()
  : m_object(NULL)
{
  NS_INIT_ISUPPORTS();
}

sashGtkObject::~sashGtkObject()
{
  if (m_object)
    gtk_object_unref(m_object);
}

NS_IMETHODIMP
sashGtkObject::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension *ext,
								   JSContext *cx, PRUint32 argc, nsIVariant **args, 
								   PRBool *ret)
{
  // abstract class
  *ret = false;
  return NS_OK;
}

NS_IMETHODIMP
sashGtkObject::SetObject(GtkObject *object)
{
  if (m_object)
    gtk_object_unref(m_object);
  gtk_object_ref(object);
  m_object = object;
  return NS_OK;
}

NS_IMETHODIMP
sashGtkObject::GetObject(GtkObject **object)
{
  assert(object != NULL);
  *object = m_object;
  return NS_OK;  
}


NS_IMETHODIMP
sashGtkObject::SetExtension(sashIExtension *ext)
{
	m_ext = ext;
	return NS_OK;
}

NS_IMETHODIMP
sashGtkObject::SetContext(JSContext *cx)
{
	m_cx = cx;
	return NS_OK;
}

NS_IMETHODIMP
sashGtkObject::SetActionRuntime(sashIActionRuntime * act)
{
	 m_rt = act;
	 return NS_OK;
}

NS_IMETHODIMP
sashGtkObject::SignalConnect(const char *signalName,
							 const char *handlerName,
							 nsIVariant * user_data)
{
  nsresult rv;
  nsCOMPtr<sashIGtkExtension> gtkext(do_QueryInterface(m_ext, &rv));
  assert(rv == NS_OK);

  if (user_data)
	   NS_ADDREF(user_data);
  return gtkext->SignalConnect(this, signalName, handlerName, user_data);
}

NS_IMETHODIMP
sashGtkObject::SignalDisconnect(const char *signalName) {
  nsresult rv;
  nsCOMPtr<sashIGtkExtension> gtkext(do_QueryInterface(m_ext, &rv));
  assert(rv == NS_OK);
  return gtkext->SignalDisconnect(this, signalName);
}

NS_IMETHODIMP
sashGtkObject::WrapObject(GtkObject *obj, void **ret)
{
  nsresult rv;
  nsCOMPtr<sashIGtkExtension> gtkext(do_QueryInterface(m_ext, &rv));
  assert(rv == NS_OK);

	nsCOMPtr<sashIGtkObjectFactory> objectfactory;

	rv = gtkext->GetFactory(getter_AddRefs(objectfactory));
	if (rv != NS_OK) {
		DEBUGMSG(gtk, "Could not get object factory\n");
		return rv;
	}

	rv = objectfactory->GetXPObjectQI(GTK_OBJECT(obj),
									  (void **) ret);
	if (rv != NS_OK) {
		DEBUGMSG(gtk, "Error in GetXPObjectQI");
		return rv;
	}
	return NS_OK;
}

/* attribute nsIVariant userData; */
NS_IMETHODIMP sashGtkObject::GetUserData(nsIVariant * *aUserData)
{
	if(!m_object)
	{
		*aUserData = NULL;
		return NS_OK;
	}
	
	*aUserData = (nsIVariant *) gtk_object_get_user_data(m_object);
	return NS_OK;
}
NS_IMETHODIMP sashGtkObject::SetUserData(nsIVariant * aUserData)
{
	if(!m_object) return NS_OK;

	gtk_object_set_user_data(m_object, (void *) aUserData);
	return NS_OK;	
}

