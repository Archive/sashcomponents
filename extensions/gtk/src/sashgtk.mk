SASHGTK_IDL_FILES = \
	sashIGdkCursor.idl \
	sashIGtkAccelGroup.idl \
	sashIGdkEvent.idl \
	sashIGtkStyle.idl \
	sashIGdkWindow.idl \
	sashIGtkSelectionData.idl \
	sashIGdkDrawable.idl \
	sashIGdkFont.idl \
	sashIGdkColormap.idl \
	sashIGdkColor.idl \
	sashIGdkGC.idl \
	sashIGdkDragContext.idl \
	sashIGdkBitmap.idl \
	sashIGtkCTreeNode.idl \
	sashIGdkPixmap.idl \
	sashIGtkViewport.idl \
	sashIGtkEntry.idl \
	sashIGtkMenuBar.idl \
	sashIGtkVScale.idl \
	sashIGtkHandleBox.idl \
	sashIGtkAlignment.idl \
	sashIGtkScrolledWindow.idl \
	sashIGtkCheckMenuItem.idl \
	sashIGtkToggleButton.idl \
	sashIGtkToolbar.idl \
	sashIGtkVScrollbar.idl \
	sashIGtkData.idl \
	sashIGtkAspectFrame.idl \
	sashIGtkCTree.idl \
	sashIGtkCheckButton.idl \
	sashIGtkList.idl \
	sashIGtkFontSelection.idl \
	sashIGtkMenuItem.idl \
	sashIGtkMenu.idl \
	sashIGtkDrawingArea.idl \
	sashIGtkColorSelection.idl \
	sashIGtkVRuler.idl \
	sashIGtkAdjustment.idl \
	sashIGtkItem.idl \
	sashIGtkCurve.idl \
	sashIGtkMozEmbed.idl \
	sashIGtkCList.idl \
	sashIGtkScale.idl \
	sashIGtkHSeparator.idl \
	sashIGtkHScale.idl \
	sashIGtkBox.idl \
	sashIGtkVButtonBox.idl \
	sashIGtkAccelLabel.idl \
	sashIGtkPixmap.idl \
	sashIGtkVSeparator.idl \
	sashIGtkCalendar.idl \
	sashIGtkText.idl \
	sashIGtkListItem.idl \
	sashIGtkMenuShell.idl \
	sashIGtkHPaned.idl \
	sashIGtkOptionMenu.idl \
	sashIGtkTipsQuery.idl \
	sashIGtkGammaCurve.idl \
	sashIGtkTreeItem.idl \
	sashIGtkFixed.idl \
	sashIGtkButton.idl \
	sashIGtkProgressBar.idl \
	sashIGtkVBox.idl \
	sashIGtkMisc.idl \
	sashIGtkPaned.idl \
	sashIGtkContainer.idl \
	sashIGtkHScrollbar.idl \
	sashIGtkPlug.idl \
	sashIGtkSpinButton.idl \
	sashIGtkItemFactory.idl \
	sashIGtkInputDialog.idl \
	sashIGtkTree.idl \
	sashIGtkTearoffMenuItem.idl \
	sashIGtkHBox.idl \
	sashIGtkArrow.idl \
	sashIGtkCombo.idl \
	sashIGtkHButtonBox.idl \
	sashIGtkDialog.idl \
	sashIGtkRadioButton.idl \
	sashIGtkColorSelectionDialog.idl \
	sashIGtkRange.idl \
	sashIGtkHRuler.idl \
	sashIGtkPreview.idl \
	sashIGtkTable.idl \
	sashIGtkButtonBox.idl \
	sashIGtkVPaned.idl \
	sashIGtkFileSelection.idl \
	sashIGtkLayout.idl \
	sashIGtkFontSelectionDialog.idl \
	sashIGtkProgress.idl \
	sashIGtkRadioMenuItem.idl \
	sashIGtkFrame.idl \
	sashIGtkStatusbar.idl \
	sashIGtkBin.idl \
	sashIGtkRuler.idl \
	sashIGtkTooltips.idl \
	sashIGtkNotebook.idl \
	sashIGtkSeparator.idl \
	sashIGtkPacker.idl \
	sashIGtkScrollbar.idl \
	sashIGtkLabel.idl \
	sashIGtkEventBox.idl \
	sashIGtkEditable.idl \
	sashIGtkWindow.idl

SASHGTK_SOURCES = \
	sashGdkCursor.h sashGdkCursor.cpp \
	sashGtkAccelGroup.h sashGtkAccelGroup.cpp \
	sashGdkEvent.h sashGdkEvent.cpp \
	sashGtkStyle.h sashGtkStyle.cpp \
	sashGdkWindow.h sashGdkWindow.cpp \
	sashGtkSelectionData.h sashGtkSelectionData.cpp \
	sashGdkDrawable.h sashGdkDrawable.cpp \
	sashGdkFont.h sashGdkFont.cpp \
	sashGdkColormap.h sashGdkColormap.cpp \
	sashGdkColor.h sashGdkColor.cpp \
	sashGdkGC.h sashGdkGC.cpp \
	sashGdkDragContext.h sashGdkDragContext.cpp \
	sashGdkBitmap.h sashGdkBitmap.cpp \
	sashGtkCTreeNode.h sashGtkCTreeNode.cpp \
	sashGdkPixmap.h sashGdkPixmap.cpp \
	sashGtkViewport.h sashGtkViewport.cpp \
	sashGtkEntry.h sashGtkEntry.cpp \
	sashGtkMenuBar.h sashGtkMenuBar.cpp \
	sashGtkVScale.h sashGtkVScale.cpp \
	sashGtkHandleBox.h sashGtkHandleBox.cpp \
	sashGtkAlignment.h sashGtkAlignment.cpp \
	sashGtkScrolledWindow.h sashGtkScrolledWindow.cpp \
	sashGtkCheckMenuItem.h sashGtkCheckMenuItem.cpp \
	sashGtkToggleButton.h sashGtkToggleButton.cpp \
	sashGtkToolbar.h sashGtkToolbar.cpp \
	sashGtkVScrollbar.h sashGtkVScrollbar.cpp \
	sashGtkData.h sashGtkData.cpp \
	sashGtkAspectFrame.h sashGtkAspectFrame.cpp \
	sashGtkCTree.h sashGtkCTree.cpp \
	sashGtkCheckButton.h sashGtkCheckButton.cpp \
	sashGtkList.h sashGtkList.cpp \
	sashGtkFontSelection.h sashGtkFontSelection.cpp \
	sashGtkMenuItem.h sashGtkMenuItem.cpp \
	sashGtkMenu.h sashGtkMenu.cpp \
	sashGtkDrawingArea.h sashGtkDrawingArea.cpp \
	sashGtkColorSelection.h sashGtkColorSelection.cpp \
	sashGtkVRuler.h sashGtkVRuler.cpp \
	sashGtkAdjustment.h sashGtkAdjustment.cpp \
	sashGtkItem.h sashGtkItem.cpp \
	sashGtkCurve.h sashGtkCurve.cpp \
	sashGtkMozEmbed.h sashGtkMozEmbed.cpp \
	sashGtkCList.h sashGtkCList.cpp \
	sashGtkScale.h sashGtkScale.cpp \
	sashGtkHSeparator.h sashGtkHSeparator.cpp \
	sashGtkHScale.h sashGtkHScale.cpp \
	sashGtkBox.h sashGtkBox.cpp \
	sashGtkVButtonBox.h sashGtkVButtonBox.cpp \
	sashGtkAccelLabel.h sashGtkAccelLabel.cpp \
	sashGtkPixmap.h sashGtkPixmap.cpp \
	sashGtkVSeparator.h sashGtkVSeparator.cpp \
	sashGtkCalendar.h sashGtkCalendar.cpp \
	sashGtkText.h sashGtkText.cpp \
	sashGtkListItem.h sashGtkListItem.cpp \
	sashGtkMenuShell.h sashGtkMenuShell.cpp \
	sashGtkHPaned.h sashGtkHPaned.cpp \
	sashGtkOptionMenu.h sashGtkOptionMenu.cpp \
	sashGtkTipsQuery.h sashGtkTipsQuery.cpp \
	sashGtkGammaCurve.h sashGtkGammaCurve.cpp \
	sashGtkTreeItem.h sashGtkTreeItem.cpp \
	sashGtkFixed.h sashGtkFixed.cpp \
	sashGtkButton.h sashGtkButton.cpp \
	sashGtkProgressBar.h sashGtkProgressBar.cpp \
	sashGtkVBox.h sashGtkVBox.cpp \
	sashGtkMisc.h sashGtkMisc.cpp \
	sashGtkPaned.h sashGtkPaned.cpp \
	sashGtkContainer.h sashGtkContainer.cpp \
	sashGtkHScrollbar.h sashGtkHScrollbar.cpp \
	sashGtkPlug.h sashGtkPlug.cpp \
	sashGtkSpinButton.h sashGtkSpinButton.cpp \
	sashGtkItemFactory.h sashGtkItemFactory.cpp \
	sashGtkInputDialog.h sashGtkInputDialog.cpp \
	sashGtkTree.h sashGtkTree.cpp \
	sashGtkTearoffMenuItem.h sashGtkTearoffMenuItem.cpp \
	sashGtkHBox.h sashGtkHBox.cpp \
	sashGtkArrow.h sashGtkArrow.cpp \
	sashGtkCombo.h sashGtkCombo.cpp \
	sashGtkHButtonBox.h sashGtkHButtonBox.cpp \
	sashGtkDialog.h sashGtkDialog.cpp \
	sashGtkRadioButton.h sashGtkRadioButton.cpp \
	sashGtkColorSelectionDialog.h sashGtkColorSelectionDialog.cpp \
	sashGtkRange.h sashGtkRange.cpp \
	sashGtkHRuler.h sashGtkHRuler.cpp \
	sashGtkPreview.h sashGtkPreview.cpp \
	sashGtkTable.h sashGtkTable.cpp \
	sashGtkButtonBox.h sashGtkButtonBox.cpp \
	sashGtkVPaned.h sashGtkVPaned.cpp \
	sashGtkFileSelection.h sashGtkFileSelection.cpp \
	sashGtkLayout.h sashGtkLayout.cpp \
	sashGtkFontSelectionDialog.h sashGtkFontSelectionDialog.cpp \
	sashGtkProgress.h sashGtkProgress.cpp \
	sashGtkRadioMenuItem.h sashGtkRadioMenuItem.cpp \
	sashGtkFrame.h sashGtkFrame.cpp \
	sashGtkStatusbar.h sashGtkStatusbar.cpp \
	sashGtkBin.h sashGtkBin.cpp \
	sashGtkRuler.h sashGtkRuler.cpp \
	sashGtkTooltips.h sashGtkTooltips.cpp \
	sashGtkNotebook.h sashGtkNotebook.cpp \
	sashGtkSeparator.h sashGtkSeparator.cpp \
	sashGtkPacker.h sashGtkPacker.cpp \
	sashGtkScrollbar.h sashGtkScrollbar.cpp \
	sashGtkLabel.h sashGtkLabel.cpp \
	sashGtkEventBox.h sashGtkEventBox.cpp \
	sashGtkEditable.h sashGtkEditable.cpp \
	sashGtkWindow.h sashGtkWindow.cpp

