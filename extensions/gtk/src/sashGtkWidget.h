
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHGTKWIDGET_H
#define SASHGTKWIDGET_H

#include "sashIGtkWidget.h"
#include "sashGtkObject.h"
#include "sashIConstructor.h"

//f0d45389-6d67-40ba-b6b5-2343141a63d9
#define SASHGTKWIDGET_CID {0xf0d45389, 0x6d67, 0x40ba, {0xb6, 0xb5, 0x23, 0x43, 0x14, 0x1a, 0x63, 0xd9}}

#define SASHGTKWIDGET_CONTRACT_ID "@gnome.org/SashXB/gtk/GtkWidget;1"

class sashGtkWidget : public sashIGtkWidget, 
		      public sashGtkObject
{
 public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHICONSTRUCTOR;
  NS_DECL_SASHIGTKWIDGET;

  sashGtkWidget();
  virtual ~sashGtkWidget();

  NS_FORWARD_SASHIGTKOBJECT(sashGtkObject::)
};

#endif // SASHGTKWIDGET_H
