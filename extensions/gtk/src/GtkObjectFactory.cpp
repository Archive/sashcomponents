
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "GtkObjectFactory.h"
#include "sashGtkWidget.h"
#include <jsapi.h>
#include <nsCOMPtr.h>
#include <nsIXPConnect.h>
#include <nsIServiceManager.h>
#include <gtk/gtk.h>
#include "debugmsg.h"
#include "GtkObjectFactorySupport.h"

NS_IMPL_ISUPPORTS1(GtkObjectFactory, sashIGtkObjectFactory);

GtkObjectFactory::GtkObjectFactory() :
	m_ext(NULL)
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(gtk, "Creating object factory\n");
  m_hash = g_hash_table_new(g_str_hash, g_str_equal);
  m_rt = NULL;
  int count = sizeof(classinfos) / sizeof(ClassInfo);
  for (int k = 0; k < count; k++) {
	   g_hash_table_insert(m_hash, g_strdup(classinfos[k].name), new int(k));
  }
}

static gboolean
entry_destroy(gpointer key, gpointer value, gpointer user_data)
{
  g_free((char *) key);
  delete (int *) value;
  return TRUE;
}

GtkObjectFactory::~GtkObjectFactory()
{
  DEBUGMSG(gtk, "Destroying object factory\n");
  g_hash_table_foreach_remove(m_hash, &entry_destroy, NULL);
  g_hash_table_destroy(m_hash);
}

NS_IMETHODIMP
GtkObjectFactory::SetExtension(sashIExtension *ext)
{
	DEBUGMSG(gtk, "Setting extension for GtkObjectFactory\n");
	m_ext = ext;
	return NS_OK;
}

NS_IMETHODIMP
GtkObjectFactory::SetContext(JSContext *cx)
{
	DEBUGMSG(gtk, "Setting context for GtkObjectFactory\n");
	m_cx = cx;
	return NS_OK;
}

int
GtkObjectFactory::FindObjectPos(GtkObject *w)
{
  gchar * klassname = gtk_type_name(GTK_OBJECT_TYPE(w));
  gpointer res = g_hash_table_lookup(m_hash, klassname);
  if (res == NULL) {
	   // attempt to wrapper as a regular GTK widget
	   if (GTK_IS_WIDGET(w))
			res = g_hash_table_lookup(m_hash, gtk_type_name(GTK_TYPE_WIDGET));
	   if (res == NULL)
			return -1;
  }
  return *(int*) res;
}


NS_IMETHODIMP
GtkObjectFactory::GetXPObject(GtkObject *w, sashIGtkObject ** obj)
{
  static const nsIID uuid = SASHIGTKOBJECT_IID;

  assert(obj != NULL);
  *obj = NULL;
  
  int pos = FindObjectPos(w);
  if (pos == -1) {
    DEBUGMSG(gtk, "Object not wrapped\n");
    return NS_COMFALSE;
  }

  nsIID * iid;
  nsresult res = GetIID(w, &iid);
  if (res != NS_OK)
    return res;
  
  sashIGtkObject *temp;
  res = nsComponentManager::CreateInstance(classinfos[pos].contractid, 
					   nsnull, *iid, (void **) &temp);
  if (res != NS_OK)
  {
	   DEBUGMSG(gtk, "Could not create object instance");
	   return res;
  }

  res = temp->QueryInterface(uuid, (void **)obj);
  NS_IF_RELEASE(temp);
  if (res != NS_OK)
  {
	   DEBUGMSG(gtk, "Could not get object interface");
	   return res;
  }

	DEBUGMSG(gtk, "Setting extension\n");
	/*
		Object factory created separately. Should have been gotten
		as an attribute of the GTK extension.
	*/
	assert(m_ext != NULL); 
	assert(m_rt != NULL);
	(*obj)->SetExtension(m_ext);
	(*obj)->SetContext(m_cx);
	(*obj)->SetActionRuntime(m_rt);
  res = (*obj)->SetObject(w);
  return res;
}

NS_IMETHODIMP
GtkObjectFactory::GetXPObjectQI(GtkObject *object, void **_retVal)
{
  nsCOMPtr<sashIGtkObject> xpobject;
  nsresult rv = GetXPObject(object, getter_AddRefs(xpobject));
  if (rv != NS_OK)
    return rv;

  nsIID * iid;
  rv = GetIID(object, &iid);
  if (rv != NS_OK)
    return rv;

  rv = xpobject->QueryInterface(*iid, _retVal);
  return rv;
}


NS_IMETHODIMP
GtkObjectFactory::GetJSObject(GtkObject *w, JSContext *cx, JSObject **ret)
{
  nsresult rv;
  nsCOMPtr<nsIXPConnect> xpc = do_GetService(nsIXPConnect::GetCID(), &rv);
  assert(rv == NS_OK);
  
  nsCOMPtr<sashIGtkObject> xpobj;
  DEBUGMSG(gtk, "Getting object\n");
  rv = GetXPObject(w, getter_AddRefs(xpobj));
  if (xpobj == NULL)
  {
	   // we have not wrappered this particular widget -- return null to JS
	   *ret = JSVAL_NULL;
	   return NS_OK;
  }
  assert(rv == NS_OK);

  nsIID * iid;
  nsresult res = GetIID(w, &iid);
  assert(res == NS_OK);
  
  DEBUGMSG(gtk, "Wrapping object\n");
  return m_rt->WrapSashObject(*iid, xpobj, cx, ret);
}

NS_IMETHODIMP
GtkObjectFactory::GetIID(GtkObject *w, nsIID **ret)
{
  int pos = FindObjectPos(w);
  if (pos == -1) {
    DEBUGMSG(gtk, "Could not find object");
    return NS_COMFALSE;
  }
  
  *ret = &classinfos[pos].iid;
  return NS_OK;
}

NS_IMETHODIMP GtkObjectFactory::SetRuntime(sashIActionRuntime * rt)
{
	 m_rt = rt;
	 return NS_OK;
}
