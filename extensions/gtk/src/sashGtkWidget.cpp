
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashGtkWidget.h"
#include "extensiontools.h"
#include <nsIGenericFactory.h>
#include <gtk/gtk.h>

SASH_IMPL_ISUPPORTS_INHERITED_CI1(sashGtkWidget, sashGtkObject, sashIGtkWidget);

sashGtkWidget::sashGtkWidget()
{
}

sashGtkWidget::~sashGtkWidget()
{
}

NS_IMETHODIMP
sashGtkWidget::Show()
{
  gtk_widget_show(GTK_WIDGET(m_object));
  return NS_OK;
}

NS_IMETHODIMP
sashGtkWidget::ShowAll()
{
  gtk_widget_show_all(GTK_WIDGET(m_object));
  return NS_OK;
}

NS_IMETHODIMP
sashGtkWidget::Hide()
{
  gtk_widget_hide(GTK_WIDGET(m_object));
  return NS_OK;
}

NS_IMETHODIMP
sashGtkWidget::Destroy()
{
  gtk_widget_destroy(GTK_WIDGET(m_object));
  return NS_OK;
}

NS_IMETHODIMP
sashGtkWidget::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension *ext,
								   JSContext *cx, PRUint32 argc, nsIVariant **args, 
								   PRBool *ret)
{
  // abstract class
  *ret = false;
  return NS_OK;
}

NS_IMETHODIMP sashGtkWidget::GetEnabled(PRBool * enabled) {
	 *enabled = (GTK_WIDGET_STATE(GTK_WIDGET(m_object)) & GTK_STATE_INSENSITIVE) == 0;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWidget::SetEnabled(PRBool enabled) {
	 gtk_widget_set_sensitive(GTK_WIDGET(m_object), enabled);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWidget::GetVisible(PRBool * visible) {
	 *visible = GTK_WIDGET_VISIBLE(GTK_WIDGET(m_object));
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWidget::SetVisible(PRBool visible) {
	 if (visible)
		  gtk_widget_show(GTK_WIDGET(m_object));
	 else
		  gtk_widget_hide(GTK_WIDGET(m_object));
	 return NS_OK;
}

