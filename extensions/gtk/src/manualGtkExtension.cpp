/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Parts of the sashGtkExtension object that are written by hand
To ease the incremental compilation pain

*****************************************************************/

#include "sashGtkExtension.h"
#include "extensiontools.h"
#include "sashIGenericConstructor.h"
#include "sashIActionRuntime.h"
#include "GtkObjectFactory.h"
#include "sashIGtkObject.h"
#include "sashVariantUtils.h"
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gmodule.h>
#include <unistd.h>

/*
	Signal connection in the Gtk extension is a little indirect.
	I'll assume you know how to get to SignalConnectNative. There are
	a few ways to do this.

	This function takes a GtkObject, the name of the signal, the name of
	the handler, and some user data, if any.

	If the handler name has a pair of parentheses in it, it will connect
	the signal to a static C function which will, when the signal is emitted,
	go into sashGtkExtension::EvaluateJavascript.

	If the handler name begins with gtk_, we use gmodule to find the
	symbol for that function in the executable and just connect the
	signal to that function.

	Otherwise, we connect to a Javascript function that has the correct
	signature (same as it would be for C, but without the type
	information, of course). This happens by connecting the signal to
	the C function JSCallbackMarshal, which jumps into
	sashGtkExtension::Callback.
	
	- Andrew Chatham
*/

NS_IMETHODIMP
sashGtkExtension::ProcessEvent()
{
	// this should be taken care of by the main loop anyway
  return NS_OK;
}

NS_IMETHODIMP
sashGtkExtension::Cleanup()
{
  return NS_OK;
}

NS_IMETHODIMP
sashGtkExtension::Initialize(sashIActionRuntime *act,
							 const char *registrationXml, 
							 const char *webGuid,
							 JSContext *cx, JSObject *obj)
{
	 DEBUGMSG(gtk, "Initializing gtk extension with guid\n");
	 m_jsContext = cx;
	 m_rt = act;

  nsCID cid = GTKOBJECTFACTORY_CID;
  nsIID iid = SASHIGTKOBJECTFACTORY_IID;
  
  nsresult rv = 
    nsComponentManager::CreateInstance(cid, nsnull, iid,
									   (void **) getter_AddRefs(m_fact));
  assert(rv == NS_OK);
	assert(m_fact != NULL);
  m_fact->SetRuntime(m_rt);

	nsCOMPtr<sashIExtension> me(do_QueryInterface((sashIGtkExtension *)this));
	m_fact->SetExtension(me);
	m_fact->SetContext(cx);

  return NS_OK;
}

static void
sash_quit(GtkWidget *widget, GtkCallbackInfo *cb)
{
  gtk_main_quit();
  _exit(1);
}

static void
destroyCallbackInfo(GtkCallbackInfo *cb)
{
  delete cb;
}

static void
destroyEvaluateInfo(JSEvaluateInfo *info)
{
	delete info;
}

// Callback for evaluating a JS string
static void
JSEvaluateMarshal(GtkObject *o, gpointer data, guint argc, GtkArg *args)
{
	JSEvaluateInfo *info = (JSEvaluateInfo *) data;
	info->ext->EvaluateJavascript(info);
}

// Callback for jumping into a JS function
static void
JSCallbackMarshal(GtkObject *o, gpointer data, guint argc, GtkArg *args)
{
  GtkCallbackInfo *cb = (GtkCallbackInfo *) data;
  cb->ext->Callback(cb, o, argc, args);
}

gboolean sashGtkExtension::JSDeleteCallback(GtkWidget *widget,
											GdkEvent *event,
											gpointer data) 
{
	 GtkCallbackInfo *cb = (GtkCallbackInfo *) data;
	 vector<nsIVariant *> args;
	 nsCOMPtr<nsIVariant> res = getter_AddRefs(CallEventWithResult(cb->ext->m_rt, cb->ext->m_jsContext, 
																   cb->handler_name, args));
	 return VariantGetBoolean(res);
}

NS_IMETHODIMP
sashGtkExtension::SignalDisconnect(sashIGtkObject *obj, 
								const char *signalName) {
  GtkObject *gtkobj;
  obj->GetObject(&gtkobj);
  guint i = (guint) gtk_object_get_data(gtkobj, (string("sash-handler-") + signalName).c_str()); 
  DEBUGMSG(gtk, "disconnecting %d (%s) for gtkobj %p\n", i, signalName, gtkobj);
  gtk_signal_disconnect(GTK_OBJECT(gtkobj), i);
  return NS_OK;
}

NS_IMETHODIMP
sashGtkExtension::SignalConnect(sashIGtkObject *obj, 
								const char *signalName,
								const char *handlerName,
								nsIVariant * user_data)
{
  GtkObject *gtkobj;
  obj->GetObject(&gtkobj);
  return SignalConnectNative(gtkobj, signalName, handlerName, user_data);
}

// This code is heavily influenced by libglade's glade-xml.c
// Note that this assumes gtk is linked into the executable.
// We should probably check this library instead of the executable,
// but that's kind of hard
static gpointer
getFuncPtr(const char *handlerName)
{
	static GModule *allsymbols = NULL;
	gpointer func = NULL;
	if (!g_module_supported()) {
		g_warning("gmodule not supported. Could not get handler %s\n",  
							handlerName);
		return NULL;
	}
	if (!allsymbols)
		allsymbols = g_module_open(NULL, G_MODULE_BIND_LAZY);
	if (!allsymbols) {
		g_error("Could not open executable for symbol reading");
	}
	g_module_symbol(allsymbols, handlerName, &func);
	if (!func)
		g_warning("Could not find native handler %s\n", handlerName);
	return func;
}

// For connecting to native handlers, like gtk_widget_show, etc.
static guint
connect_native(GtkObject *obj, const char *signalName, const char *handlerName)
{
	 DEBUGMSG(gtk, "Connecting signal %s to native handler %s\n", signalName, handlerName);
	 gpointer func = getFuncPtr(handlerName);
	 if (func)
		  return gtk_signal_connect(obj, signalName, GTK_SIGNAL_FUNC(func), NULL);
	 else
		  fprintf(stderr, "Could not connect to native handler %s\n", handlerName);
	 return 0;
}

NS_IMETHODIMP
sashGtkExtension::SignalConnectNative(GtkObject *gtkobj, 
									  const char *signalName,
									  const char *handlerName, 
									  nsIVariant * user_data)
{
	/*
	  TODO: Connect things like gtk_widget_destroy, gtk_widget_hide, etc.
	  to the actual GTK functions. Glade does this by using gmodule to 
	  find the functions in the executable. Should we do that? We perhaps
	  shouldn't assume anything about the executable.
	  Or keep a table of function names to function pointers?
	*/
	NS_ADDREF(user_data);
	guint i = 0; 
	// If it has parentheses, then assume it's an explicit JS call and do that
	if (strchr(handlerName, '(') != NULL && strchr(handlerName, ')')) {
		 i = gtk_signal_connect_full(gtkobj, signalName, 
									NULL, JSEvaluateMarshal,
									 (gpointer) CreateEvaluateInfo(handlerName),
									 (GtkDestroyNotify) destroyEvaluateInfo, 
									 false, false);
	} else if (strcmp(handlerName,"gtk_main_quit") == 0) {
		 i = gtk_signal_connect_full(gtkobj, signalName,
									 GTK_SIGNAL_FUNC(sash_quit), NULL,
									 (gpointer) CreateCallbackInfo(handlerName, signalName, user_data),
									 (GtkDestroyNotify) destroyCallbackInfo,
									 false, false);
	} else if (strstr(handlerName, "gtk_")) {
		 i = connect_native(gtkobj, signalName, handlerName);
	} else if (strcmp(signalName, "delete_event") == 0) {
		 i = gtk_signal_connect_full(gtkobj, signalName, 
									 GTK_SIGNAL_FUNC(JSDeleteCallback), NULL,
									 (gpointer) CreateCallbackInfo(handlerName, signalName, user_data),
									 (GtkDestroyNotify) destroyCallbackInfo,
									 false, false);
	} else {
		 i = gtk_signal_connect_full(gtkobj, signalName, 
									 NULL, JSCallbackMarshal,
									 (gpointer) CreateCallbackInfo(handlerName, signalName, user_data),
									 (GtkDestroyNotify) destroyCallbackInfo,
									 false, false);
	}

	DEBUGMSG(gtk, "set signal handler %d (%s) for obj %p to %s\n", i, signalName, gtkobj, handlerName);
	gtk_object_set_data(gtkobj, (string("sash-handler-") + signalName).c_str(), (void*) i); 
	
	return NS_OK;
}

/*
   Perform the callback specified in cb on object obj with arguments args.
   Will call the JS function specified in cb->handler_name.
*/
void
sashGtkExtension::Callback(GtkCallbackInfo *cb, GtkObject *obj, 
						   guint argc, GtkArg *args)
{
	 jsval js_rval;
	 int num_js_args = argc + 2; 
	 bool ret;
	 DEBUGMSG(gtk, "Calling back on object %p, with handler %s and %d args\n", 
			  obj, cb->handler_name, argc);
	 if (m_jsContext)
	 {
		  jsval *argv = new jsval[num_js_args]; // first argument (object) not counted
		  
		  argv[0] = Obj2JSVal(obj);
		  
		  // special hack for double click
		  if (! strcmp(cb->signal_name, "button_press_event")) {
			   // extra special hack to take care of other events so that double click makes sense
			   assert(GTK_FUNDAMENTAL_TYPE(args[0].type) == GTK_TYPE_BOXED);
			   GdkEventButton* event = (GdkEventButton*) GTK_VALUE_BOXED(args[0]);
			   if (event->type == GDK_2BUTTON_PRESS) {
					argv[1] = INT_TO_JSVAL(2);
			   } else if (event->type == GDK_3BUTTON_PRESS) {
					argv[1] = INT_TO_JSVAL(3);
			   } else {
					argv[1] = INT_TO_JSVAL(1);
			   }
		  } else {
			   for (unsigned int k = 0; k < argc; k++) {
					bool err = false;
			   
					// k+1 so we start past the first argument (the object)
					argv[k + 1] = Arg2JSVal(&args[k], &err);
					if (err) {
						 fprintf(stderr, "Error converting argument. Callback %s not called\n",
								 cb->handler_name);
						 delete [] argv;
						 return;
					}
			   }
		  }
		  if (cb->user_data)
			   m_rt->VariantGetJSVal(cb->user_data, m_jsContext, argv[argc + 1]);
		  else
			   num_js_args--;

		  DEBUGMSG(gtk, "Calling JS_CallFunctionName on %s\n", cb->handler_name);

		  if (JS_GetProperty(m_jsContext, JS_GetGlobalObject(m_jsContext), 
							 cb->handler_name, &js_rval) == JS_TRUE) {
			   ret = JS_CallFunctionName(m_jsContext, JS_GetGlobalObject(m_jsContext),
										 cb->handler_name, num_js_args, argv, &js_rval);
		  }
		  delete [] argv;		  
	 }
}

// Evaluate the javascript stored in info->jsjstring
void
sashGtkExtension::EvaluateJavascript(JSEvaluateInfo *info)
{
	jsval out;
	DEBUGMSG(gtk, "Evaluating javascript code %s\n", 
					 info->jsstring.c_str());

	JS_EvaluateScript(m_jsContext, JS_GetGlobalObject(m_jsContext),
										info->jsstring.c_str(), info->jsstring.length(),
										"builtin", 1, &out);

	DEBUGMSG(gtk, "Evaluated\n");
}


/**
   Convert a GtkArg to a jsval. If the conversion could not be made, err is set
*/
jsval
sashGtkExtension::Arg2JSVal(GtkArg *arg, bool *err)
{
  switch (GTK_FUNDAMENTAL_TYPE(arg->type)) {
  case GTK_TYPE_INVALID:
  case GTK_TYPE_NONE:
    return JSVAL_NULL;

  case GTK_TYPE_CHAR:
    return INT_TO_JSVAL(GTK_VALUE_CHAR(*arg));

  case GTK_TYPE_UCHAR:
    return INT_TO_JSVAL(GTK_VALUE_UCHAR(*arg));

  case GTK_TYPE_BOOL:
    return BOOLEAN_TO_JSVAL(GTK_VALUE_BOOL(*arg));

  case GTK_TYPE_ENUM:
    return INT_TO_JSVAL(GTK_VALUE_ENUM(*arg));
    
  case GTK_TYPE_FLAGS:
    return INT_TO_JSVAL(GTK_VALUE_FLAGS(*arg));

  case GTK_TYPE_INT:
    return INT_TO_JSVAL(GTK_VALUE_INT(*arg));

  case GTK_TYPE_UINT:
    return INT_TO_JSVAL(GTK_VALUE_UINT(*arg));
    
  case GTK_TYPE_LONG:
    return INT_TO_JSVAL(GTK_VALUE_LONG(*arg));
  case GTK_TYPE_ULONG:
    return INT_TO_JSVAL(GTK_VALUE_ULONG(*arg));

  case GTK_TYPE_FLOAT:
  {
	   jsval v;
	JS_NewNumberValue(m_jsContext, GTK_VALUE_FLOAT(*arg), &v);
	return v;
  }
  case GTK_TYPE_DOUBLE:
	{   jsval v;
	JS_NewNumberValue(m_jsContext, GTK_VALUE_DOUBLE(*arg), &v);
	return v;
	}
  case GTK_TYPE_STRING:
    // strings in callbacks are not to be freed
    if (GTK_VALUE_STRING(*arg) == NULL)
      return JSVAL_NULL;
    else
      return STRING_TO_JSVAL(JS_NewStringCopyZ(m_jsContext, 
					       GTK_VALUE_STRING(*arg)));

  case GTK_TYPE_OBJECT:
    return Obj2JSVal(GTK_VALUE_OBJECT(*arg));

  default:
	return JSVAL_NULL;
	//    fprintf(stderr, "Unhandled type %d\n", arg->type);
  }

  *err = 1;
  return 0;
}

jsval
sashGtkExtension::Obj2JSVal(GtkObject *obj)
{
    JSObject *sashobject;
    nsresult rv = m_fact->GetJSObject(obj, m_jsContext, &sashobject);
    assert(rv == NS_OK);
    DEBUGMSG(gtk, "Have sashobject %p\n", sashobject);
    return OBJECT_TO_JSVAL(sashobject);
}

GtkCallbackInfo *
sashGtkExtension::CreateCallbackInfo(const gchar *handler_name, const gchar* signal_name, nsIVariant * user_data)
{
  GtkCallbackInfo * cb = new GtkCallbackInfo();

  cb->handler_name = handler_name;
  cb->signal_name = signal_name;
  cb->ext = this;
  cb->user_data = user_data;

  return cb;
}

JSEvaluateInfo *
sashGtkExtension::CreateEvaluateInfo(const gchar *jsstring)
{
	JSEvaluateInfo * info = new JSEvaluateInfo();
	
	info->jsstring = jsstring;
	info->ext = this;

	return info;
}

NS_IMETHODIMP
sashGtkExtension::GetFactory(sashIGtkObjectFactory **fact)
{
	DEBUGMSG(gtk, "Getting factory\n");
	assert(fact != NULL);
	assert(m_fact != NULL);

	*fact = m_fact;
	NS_ADDREF(*fact);

	return NS_OK;
}

NS_IMETHODIMP sashGtkExtension::GetScreenWidth(PRInt32* _retval) {
	 *_retval = gdk_screen_width();
	 return NS_OK;
}

NS_IMETHODIMP sashGtkExtension::GetScreenHeight(PRInt32* _retval) {
	 *_retval = gdk_screen_height();
	 return NS_OK;
}
