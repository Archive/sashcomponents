
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef GTKOBJECTFACTORY_H
#define GTKOBJECTFACTORY_H

#include "nsIID.h"
#include "nsError.h"
#include "sashIGtkObjectFactory.h"
#include "sashIActionRuntime.h"
#include <glib.h>

//ec2f0b46-4d68-4fff-9f11-aa620c8c31bb
#define GTKOBJECTFACTORY_CID {0xec2f0b46, 0x4d68, 0x4fff, {0x9f, 0x11, 0xaa, 0x62, 0x0c, 0x8c, 0x31, 0xbb}}

#define GTKOBJECTFACTORY_CONTRACT_ID "@gnome.org/SashXB/gtk/GtkObjectFactory;1"

class JSObject;
class JSContext;
class nsISupports;
class sashIGtkObject;
class sashIExtension;

/**
   A factory to turn an actual GtkObject into its XPCOM wrapper, 
   the IID of that wrapper's interface, 
   or a JSObject which corresponds to that object
*/
class GtkObjectFactory : public sashIGtkObjectFactory
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIGTKOBJECTFACTORY

  GtkObjectFactory();
  virtual ~GtkObjectFactory();
 private:

	sashIExtension *m_ext; // should be the Gtk extension
	JSContext* m_cx;
  /**
     Return the position of the object's type 
     in the classinfo array, -1 if not found
  */
  int FindObjectPos(GtkObject *w);

  GHashTable *m_hash;
  sashIActionRuntime * m_rt;
};

#endif // GTKOBJECTFACTORY_H
