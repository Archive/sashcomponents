
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHGTKOBJECT_H
#define SASHGTKOBJECT_H

#include "sashIGtkObject.h"
#include "sashIActionRuntime.h"
#include "sashIConstructor.h"

//3985ac27-1405-468b-b893-7a9d51909c2a
#define SASHGTKOBJECT_CID {0x3985ac27, 0x1405, 0x468b, {0xb8, 0x93, 0x7a, 0x9d, 0x51, 0x90, 0x9c, 0x2a}}

#define SASHGTKOBJECT_CONTRACT_ID "@gnome.org/SashXB/gtk/GtkObject;1"

class sashIExtension;

class sashGtkObject : public sashIGtkObject,
		      public sashIConstructor

{
 public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIGTKOBJECT;
  NS_DECL_SASHICONSTRUCTOR;

  sashGtkObject();
  virtual ~sashGtkObject();

 protected:
  GtkObject *m_object;
  sashIExtension *m_ext;
  nsCOMPtr<sashIActionRuntime> m_rt;
  JSContext *m_cx;
  nsresult WrapObject(GtkObject *obj, void **ret);
};

#endif // SASHGTKOBJECT_H
