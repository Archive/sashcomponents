#include "sashVariantUtils.h"
#include "FillItemText.h"
#include "sashGtkCTreeNode.h"
#include "debugmsg.h"

NS_DEFINE_CID(ksashGtkCTreeNodeCID, SASHGTKCTREENODE_CID);

NS_IMETHODIMP sashGtkCTree::InsertNode(sashIGtkCTreeNode *parent, 
                                       sashIGtkCTreeNode *sibling,
                                       nsIVariant *i,
                                       PRUint8 spacing,
                                       PRBool is_leaf,
                                       PRBool expanded,
                                       sashIGtkCTreeNode **ret)
{
  vector<string> strings;
  VariantGetArray(i, strings);

  char **items;
  int nCols = GTK_CLIST(m_object)->columns;
  items = (char**)alloca(sizeof(char**) * nCols);
  FillItemText(items, strings, nCols);
  
  GtkCTreeNode *gtkNode, *p, *s;
  if (parent) parent->GetObject(&p);
  else p = 0;
  if (sibling) sibling->GetObject(&s);
  else s = 0;
  DEBUGMSG(gtk, "Calling gtk_ctree_insert node...\n");
  gtkNode = gtk_ctree_insert_node(GTK_CTREE(m_object), p, s, items, spacing, 
                                  0, 0, 0, 0, is_leaf, expanded);

  *ret = 0;                                  
  
  sashIConstructor *nodeCon;
  DEBUGMSG(gtk, "Creating sashIGtkCTreeNode...");
  nsComponentManager::CreateInstance(ksashGtkCTreeNodeCID, nsnull,
    (nsIID)SASHICONSTRUCTOR_IID, (void**)&nodeCon);
  DEBUGMSG(gtk, "QI to sashIGtkCTreeNode...\n")
  nsCOMPtr<sashIGtkCTreeNode> node = do_QueryInterface(nodeCon);
  PRBool rv;
  DEBUGMSG(gtk, "initializing...");
  nodeCon->InitializeNewObject(m_rt, m_ext, m_cx, 0, 0, &rv);
  DEBUGMSG(gtk, "SetObject...");
  node->SetObject(gtkNode);
  DEBUGMSG(gtk, "done\n");

/*
 *   DEBUGMSG(gtk, "Creating sashIGtkCTreeNode...");
 *   nsCOMPtr<sashIGtkCTreeNode> node = 
 *     do_CreateInstance(SASHGTKCTREENODE_CONTRACT_ID);
 *   DEBUGMSG(gtk, "(0x%x) QI to sashIConstructor...\n", 
 *            (unsigned)(void*)(sashIGtkCTreeNode*)node);
 *   nsCOMPtr<sashIConstructor> nodeCon = do_QueryInterface(node);
 *   PRBool rv;
 *   DEBUGMSG(gtk, "SetObject...");
 *   node->SetObject(gtkNode);
 *   DEBUGMSG(gtk, "initializing...");
 *   nodeCon->InitializeNewObject(m_rt, m_ext, m_cx, 0, 0, &rv);
 *   DEBUGMSG(gtk, "done\n");
 */

  *ret = (sashIGtkCTreeNode*)node;
  NS_ADDREF(*ret);
  DEBUGMSG(gtk, "ADDREF'd, all done\n");

  return NS_OK;
}

/*
 * NS_IMETHDIMP sashGtkCTree::RemoveNode(sashIGtkCTreeNode *n) {
 *   gtk_ctree_remove_node(GTK_CTREE(m_object), n->GetObject());
 *   return NS_OK;
 * }
 */ 
