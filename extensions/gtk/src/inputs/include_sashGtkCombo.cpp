#include "sashVariantUtils.h"
#include <stdlib.h>

NS_IMETHODIMP sashGtkCombo::SetPopdownItems(nsIVariant * itemsArray) {
	 vector<string> strings;
	 VariantGetArray(itemsArray, strings);

	 GList *items = NULL;   
	 vector<string>::iterator ai = strings.begin(),
		  bi = strings.end();
	 while (ai != bi){
		  items = g_list_append (items, strdup((*ai).c_str()));
		  ++ai;
	 } 
	 gtk_combo_set_popdown_strings (GTK_COMBO (m_object), items);

	 return NS_OK;
}

