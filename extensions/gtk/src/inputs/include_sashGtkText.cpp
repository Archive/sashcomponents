NS_IMETHODIMP sashGtkText::GetText(char ** text)
{
	 XP_COPY_STRING(gtk_editable_get_chars(GTK_EDITABLE(m_object), 0, -1), text);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkText::SetText(const char * text)
{
	 gtk_editable_delete_text(GTK_EDITABLE(m_object), 0, -1);
	 gtk_text_insert(GTK_TEXT(m_object), NULL, NULL, NULL, text, strlen(text));
	 return NS_OK;
}


NS_IMETHODIMP sashGtkText::Append(const char * text)
{
	 gtk_text_set_point(GTK_TEXT(m_object), gtk_text_get_length(GTK_TEXT(m_object)));
	 gtk_text_insert(GTK_TEXT(m_object), NULL, NULL, NULL, text, strlen(text));
	 return NS_OK;
}
