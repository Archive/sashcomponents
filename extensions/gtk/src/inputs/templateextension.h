
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHGTKEXTENSION_H
#define SASHGTKEXTENSION_H

#include <vector>
#include <gtk/gtk.h>
#include <jsapi.h>
#include <nsCOMPtr.h>
#include <string>

#include "sashIGtkObjectFactory.h"
#include "sashIGtkExtension.h"
#include "sashIExtension.h"

class sashIGenericConstructor;

//08D7F20A-3A9D-402C-86E6-20B53E37A87B
#define SASHGTKEXTENSION_CID {0x08D7F20A, 0x3A9D, 0x402C, {0x86, 0xE6, 0x20, 0xB5, 0x3E, 0x37, 0xA8, 0x7B}}

#define SASHGTKEXTENSION_CONTRACT_ID "@gnome.org/SashXB/GTK;1"

NS_DEFINE_CID(ksashGtkExtensionCID, SASHGTKEXTENSION_CID);


class sashGtkExtension;

/**
   Information which will be passed to the gtk callback function
*/
class GtkCallbackInfo 
{
 public:
  const gchar *handler_name, *signal_name;
  sashGtkExtension *ext;
  nsIVariant * user_data;
};

class JSEvaluateInfo
{
 public:
	string jsstring;
	sashGtkExtension *ext;
};

class sashGtkExtension : public sashIGtkExtension,
												 public sashIExtension
{
 public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIEXTENSION;
  NS_DECL_SASHIGTKEXTENSION;
  
  sashGtkExtension();
  virtual ~sashGtkExtension();

  /**
     Eventually signal connection is passed back to the object. 
     Executes the actual javascript callback function.
  */
  void Callback(GtkCallbackInfo *cb, GtkObject *obj,
		guint argc, GtkArg *args);

  static gboolean JSDeleteCallback(GtkWidget *widget,
								   GdkEvent *event,
								   gpointer data);

	/**
		 Evaluates a javascript string
	*/
	void EvaluateJavascript(JSEvaluateInfo *info);



 private:
  jsval Obj2JSVal(GtkObject *obj);
  jsval Arg2JSVal(GtkArg *arg, bool *err);


  GtkCallbackInfo *CreateCallbackInfo(const char *handlerName, const char *signalname, 
									  nsIVariant * user_data);
	JSEvaluateInfo *CreateEvaluateInfo(const char *evaluateString);

  JSContext *m_jsContext;
  nsCOMPtr<sashIGtkObjectFactory> m_fact;
  sashIActionRuntime * m_rt;

%(attribute_members)s
};

#endif // SASHGTKEXTENSION_H
