private:

std::string m_OnProgress;
std::string m_OnLocationChanged;
std::string m_OnLoadBegin;
std::string m_OnLoadEnd;
std::string m_OnTitleChanged;
std::string m_OnLinkMessageChanged;

// need it as a string so it will be auto-initialized
std::string m_HaveRegistered;
