
NS_IMETHODIMP
sashGtkTreeItem::GetSubtree(sashIGtkWidget * *_retVal)
{
  
  GtkWidget *c_retVal = GTK_TREE_ITEM(m_object)->subtree;
  
  nsresult createrv = WrapObject(GTK_OBJECT(c_retVal), (void **) _retVal);
  if (!NS_SUCCEEDED(createrv))
    return createrv;
  
  return NS_OK;
}


NS_IMETHODIMP
sashGtkTreeItem::SetSubtree(sashIGtkWidget * subtree)
{
  GtkObject * raw_subtree;
  if (subtree)
    subtree->GetObject(&raw_subtree);
  else
    raw_subtree = NULL;
  gtk_tree_item_set_subtree(GTK_TREE_ITEM(m_object), GTK_WIDGET(raw_subtree));
  
  return NS_OK;
}
