#include "sashVariantUtils.h"
#include <stdlib.h>

void FillItemText(char ** item_text, vector<string> & strings, int numColumns) {
  int i;

  // initialize each entry
  for (i = 0; i < numColumns; i++)
	item_text[i] = NULL;

  // assign the entries with the supplied string list
  for (i = 0; i < (int)strings.size() && i < numColumns; i++) {
    item_text[i] = (char*)malloc(strings[i].size()+1);
    strncpy(item_text[i], strings[i].c_str(), strings[i].size());
	item_text[i][strings[i].size()] = '\0';
    //item_text[i] = (char *) strings[i].c_str();
  }
}

NS_IMETHODIMP sashGtkCList::Append(nsIVariant * items, PRInt32 * ret) {
  vector<string> strings;
  VariantGetArray(items, strings);
  char ** item_text;
  int numColumns;

  numColumns = GTK_CLIST(m_object)->columns;
  item_text = (char **) alloca(sizeof(char **) * numColumns);
  
  FillItemText(item_text, strings, numColumns);

  *ret = gtk_clist_append(GTK_CLIST(m_object), item_text);

  return NS_OK;
}

NS_IMETHODIMP sashGtkCList::Insert(PRInt32 pos, nsIVariant * items) {
  vector<string> strings;
  VariantGetArray(items, strings);
  char ** item_text;
  int numColumns;

  numColumns = GTK_CLIST(m_object)->columns;
  item_text = (char **) alloca(sizeof(char **) * numColumns);
  
  FillItemText(item_text, strings, numColumns);

  gtk_clist_insert(GTK_CLIST(m_object), pos, item_text);

  return NS_OK;
}

NS_IMETHODIMP sashGtkCList::Prepend(nsIVariant * items, PRInt32 * ret) {
  vector<string> strings;
  VariantGetArray(items, strings);
  char ** item_text;
  int numColumns;

  numColumns = GTK_CLIST(m_object)->columns;
  item_text = (char **) alloca(sizeof(char **) * numColumns);
  
  FillItemText(item_text, strings, numColumns);

  *ret = gtk_clist_prepend(GTK_CLIST(m_object), item_text);

  return NS_OK;
}

NS_IMETHODIMP sashGtkCList::RemoveRow(PRInt32 pos) {
	 gtk_clist_remove(GTK_CLIST(m_object), pos);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkCList::GetText(PRInt32 row, PRInt32 col, char** rval) {
	 char* a;
	 if (gtk_clist_get_text(GTK_CLIST(m_object), row, col, &a) == 0) {
		  XP_COPY_STRING("", rval);
		  return NS_OK;
	 } else {
		  XP_COPY_STRING(a, rval);
		  return NS_OK;
	 }
}

NS_IMETHODIMP sashGtkCList::GetSelectedRows(nsIVariant** _retval) {
	 NewVariant(_retval);
	 GList* dl = GTK_CLIST(m_object)->selection;
	 if (dl == NULL) {
		  VariantSetEmpty(*_retval);
	 } else {
	   vector<int> items;
	   while (dl) {
		 items.push_back(GPOINTER_TO_INT(dl->data));
		 dl = dl->next;
	   }
	   VariantSetArray(*_retval, items);
	 }

	 return NS_OK;
}

NS_IMETHODIMP sashGtkCList::GetColumns(PRInt32 * _retval) {
	 *_retval = GTK_CLIST(m_object)->columns;
	 return NS_OK;
}
