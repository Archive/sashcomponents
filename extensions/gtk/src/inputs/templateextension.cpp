#include "GtkObjectFactory.h"
#include "sashGtkExtension.h"
#include "extensiontools.h"
#include "sashIGenericConstructor.h"
#include "sashIActionRuntime.h"
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkprivate.h>

%(includes)s

SASH_EXTENSION_IMPL_NO_NSGET(sashGtkExtension, sashIGtkExtension, "GTK");

sashGtkExtension::sashGtkExtension() :
%(attribute_init)s
{
  NS_INIT_ISUPPORTS();
}

sashGtkExtension::~sashGtkExtension()
{
%(attribute_destroy)s
}

%(attribute_getters)s

%(enum_getters)s
