
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef FACTORYINCLUDE_H
#define FACTORYINCLUDE_H

// autogenerate file to be included in the factory cpp file; 
// defines all the per-object stuff

#include <string>
#include <nsIID.h>
#include <gtk/gtk.h>
#include "sashGtkWidget.h"

%(includes)s

typedef struct ClassInfo {
  const char *name;
  const char *contractid;
  nsIID iid;
  //  GtkType type;
} ClassInfo;

ClassInfo classinfos[] = {
	{"GtkWidget", SASHGTKWIDGET_CONTRACT_ID, SASHIGTKWIDGET_IID},
   %(classinfos)s
};

#endif // FACTORYINCLUDE_H
