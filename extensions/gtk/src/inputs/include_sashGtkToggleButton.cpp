
NS_IMETHODIMP sashGtkToggleButton::GetValue(PRBool * val)
{
	 *val = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_object));
	 return NS_OK;
}

NS_IMETHODIMP sashGtkToggleButton::SetValue(PRBool val)
{
	 gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m_object), val);
	 return NS_OK;
}
