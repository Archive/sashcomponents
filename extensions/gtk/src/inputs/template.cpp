/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

A generated source file for the GTK Object %(classname)s

*****************************************************************/

#include "%(classname)s.h"
#include <nsIGenericFactory.h>
#include <nsCOMPtr.h>
#include "debugmsg.h"
#include "GtkObjectFactory.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"

%(includes)s

%(extra)s

%(implisupports)s

%(classname)s::%(classname)s()
{
  %(constructor)s
}

%(classname)s::~%(classname)s()
{ 
}

NS_IMETHODIMP
%(classname)s::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension *ext, JSContext *cx, PRUint32 argc, nsIVariant **xp_argv, PRBool *xp_ret)
{
  m_ext = ext;
  m_cx = cx;
  m_object = NULL;
  m_rt = actionRuntime;
  if (argc != %(argnums)s) {
    *xp_ret = false;
    return NS_OK;
  }

  %(initargs)s
  %(ccall)s
  *xp_ret = (m_object != NULL);
  return NS_OK;
}

%(methodimpls)s
