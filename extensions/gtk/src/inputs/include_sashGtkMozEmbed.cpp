#include "gtkmozembed.h"
#include <string>
#include <vector>
#include "sashVariantUtils.h"
#include "extensiontools.h"

void OnMozEmbedProgress(GtkMozEmbed *embed, gint cur, gint max, gpointer data) {
	 sashGtkMozEmbed* gme = (sashGtkMozEmbed*) data;
	 gme->ProgressCallback(cur, max);
}

void OnMozEmbedLocationChanged(GtkMozEmbed *embed, gpointer data) {
	 sashGtkMozEmbed* gme = (sashGtkMozEmbed*) data;
	 gme->LocationChangedCallback();
}

void OnMozEmbedLoadBegin(GtkMozEmbed *embed, gpointer data) {
	 sashGtkMozEmbed* gme = (sashGtkMozEmbed*) data;
	 gme->LoadBeginCallback();
}

void OnMozEmbedLoadEnd(GtkMozEmbed *embed, gpointer data) {
	 sashGtkMozEmbed* gme = (sashGtkMozEmbed*) data;
	 gme->LoadEndCallback();
}

void OnMozEmbedTitleChanged(GtkMozEmbed *embed, gpointer data) {
	 sashGtkMozEmbed* gme = (sashGtkMozEmbed*) data;
	 gme->TitleChangedCallback();
}

void OnMozEmbedLinkMessageChanged(GtkMozEmbed *embed, gpointer data) {
	 sashGtkMozEmbed* gme = (sashGtkMozEmbed*) data;
	 gme->LinkMessageChangedCallback();
}

NS_IMETHODIMP sashGtkMozEmbed::RegisterSignals() {
	 if (! m_HaveRegistered.empty()) return NS_OK;
	 m_HaveRegistered = "done";
	 gtk_signal_connect(GTK_OBJECT(m_object),
						"progress",
						GTK_SIGNAL_FUNC(OnMozEmbedProgress),
						this);
	 gtk_signal_connect(GTK_OBJECT(m_object),
						"location",
						GTK_SIGNAL_FUNC(OnMozEmbedLocationChanged),
						this);
	 gtk_signal_connect(GTK_OBJECT(m_object),
						"title",
						GTK_SIGNAL_FUNC(OnMozEmbedTitleChanged),
						this);
	 gtk_signal_connect(GTK_OBJECT(m_object),
						"net_start",
						GTK_SIGNAL_FUNC(OnMozEmbedLoadBegin),
						this);
	 gtk_signal_connect(GTK_OBJECT(m_object),
						"net_stop",
						GTK_SIGNAL_FUNC(OnMozEmbedLoadEnd),
						this);
	 gtk_signal_connect(GTK_OBJECT(m_object),
						"link_message",
						GTK_SIGNAL_FUNC(OnMozEmbedLinkMessageChanged),
						this);	 
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::SetOnProgress(const char* signal) {
	 RegisterSignals();
	 m_OnProgress = signal;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::GetOnProgress(char **function) {
	 XP_COPY_STRING(m_OnProgress.c_str(), function);
	 return NS_OK;	 
}

NS_IMETHODIMP sashGtkMozEmbed::SetOnLocationChanged(const char* signal) {
	 RegisterSignals();
	 m_OnLocationChanged = signal;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::GetOnLocationChanged(char **function) {
	 XP_COPY_STRING(m_OnLocationChanged.c_str(), function);
	 return NS_OK;	 
}

NS_IMETHODIMP sashGtkMozEmbed::SetOnTitleChanged(const char* signal) {
	 RegisterSignals();
	 m_OnTitleChanged = signal;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::GetOnTitleChanged(char **function) {
	 XP_COPY_STRING(m_OnTitleChanged.c_str(), function);
	 return NS_OK;	 
}

NS_IMETHODIMP sashGtkMozEmbed::SetOnLoadBegin(const char* signal) {
	 RegisterSignals();
	 m_OnLoadBegin = signal;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::GetOnLoadBegin(char **function) {
	 XP_COPY_STRING(m_OnLoadBegin.c_str(), function);
	 return NS_OK;	 
}

NS_IMETHODIMP sashGtkMozEmbed::SetOnLoadEnd(const char* signal) {
	 RegisterSignals();
	 m_OnLoadEnd = signal;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::GetOnLoadEnd(char **function) {
	 XP_COPY_STRING(m_OnLoadEnd.c_str(), function);
	 return NS_OK;	 
}

NS_IMETHODIMP sashGtkMozEmbed::SetOnLinkMessageChanged(const char* signal) {
	 RegisterSignals();
	 m_OnLinkMessageChanged = signal;
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::GetOnLinkMessageChanged(char **function) {
	 XP_COPY_STRING(m_OnLinkMessageChanged.c_str(), function);
	 return NS_OK;	 
}

NS_IMETHODIMP sashGtkMozEmbed::ProgressCallback(PRInt32 cur, PRInt32 max) {
	 if (m_OnProgress.empty()) return NS_OK;
	 vector<nsIVariant*> v;
	 nsIVariant* v1;
	 if (NewVariant(&v1) != NS_OK) return NS_ERROR_FAILURE;
	 VariantSetNumber(v1, cur);
	 v.push_back(v1);
	 if (NewVariant(&v1) != NS_OK) return NS_ERROR_FAILURE;
	 VariantSetNumber(v1, max);
	 v.push_back(v1);

	 CallEvent(m_rt, m_cx, m_OnProgress, v);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::LocationChangedCallback() {
	 if (m_OnLocationChanged.empty()) return NS_OK;
	 vector<nsIVariant*> v;
	 CallEvent(m_rt, m_cx, m_OnLocationChanged, v);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::TitleChangedCallback() {
	 if (m_OnTitleChanged.empty()) return NS_OK;
	 vector<nsIVariant*> v;
	 CallEvent(m_rt, m_cx, m_OnTitleChanged, v);
	 return NS_OK;
}


NS_IMETHODIMP sashGtkMozEmbed::LoadBeginCallback() {
	 if (m_OnLoadBegin.empty()) return NS_OK;
	 vector<nsIVariant*> v;
	 CallEvent(m_rt, m_cx, m_OnLoadBegin, v);
	 return NS_OK;
}


NS_IMETHODIMP sashGtkMozEmbed::LoadEndCallback() {
	 if (m_OnLoadEnd.empty()) return NS_OK;
	 vector<nsIVariant*> v;
	 CallEvent(m_rt, m_cx, m_OnLoadEnd, v);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkMozEmbed::LinkMessageChangedCallback() {
	 if (m_OnLinkMessageChanged.empty()) return NS_OK;
	 vector<nsIVariant*> v;
	 CallEvent(m_rt, m_cx, m_OnLinkMessageChanged, v);
	 return NS_OK;
}
