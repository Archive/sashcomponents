NS_IMETHODIMP sashGtkWindow::Raise() {
	 gdk_window_raise(GTK_WIDGET(m_object)->window);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWindow::Lower() {
	 gdk_window_lower(GTK_WIDGET(m_object)->window);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWindow::Move(PRInt32 x, PRInt32 y) {
	 gdk_window_move(GTK_WIDGET(m_object)->window, x, y);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWindow::Resize(PRInt32 width, PRInt32 height) {
	 gdk_window_resize(GTK_WIDGET(m_object)->window, width, height);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWindow::Move_resize(PRInt32 x, PRInt32 y, PRInt32 width, PRInt32 height) {
	 gdk_window_move_resize(GTK_WIDGET(m_object)->window, x, y, width, height);
	 return NS_OK;
}


NS_IMETHODIMP sashGtkWindow::GetIsVisible(PRBool * res) {
	 *res = gdk_window_is_visible(GTK_WIDGET(m_object)->window);
	 return NS_OK;
}

NS_IMETHODIMP sashGtkWindow::GetIsViewable(PRBool * res) {
	 *res = gdk_window_is_viewable(GTK_WIDGET(m_object)->window);
	 return NS_OK;
}
