#include "extensiontools.h"
#include "GtkObjectFactory.h"
#include "sashGtkExtension.h"
%(includes)s

%(factories)s

NS_GENERIC_FACTORY_CONSTRUCTOR(GtkObjectFactory);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashGtkObject);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashGtkWidget);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashGtkExtension);

NS_DECL_CLASSINFO(sashGtkExtension);
NS_DECL_CLASSINFO(sashGtkObject);
NS_DECL_CLASSINFO(sashGtkWidget);

static nsModuleComponentInfo components [] = {
  MODULE_COMPONENT_ENTRY(sashGtkExtension, SASHGTKEXTENSION, "GTK Extension"),
  {"GtkObjectFactory", GTKOBJECTFACTORY_CID, 
   GTKOBJECTFACTORY_CONTRACT_ID, GtkObjectFactoryConstructor},
  MODULE_COMPONENT_ENTRY(sashGtkObject, SASHGTKOBJECT, "GtkObject"),
  MODULE_COMPONENT_ENTRY(sashGtkWidget, SASHGTKWIDGET, "GtkWidget"),
  %(components)s
};

NS_IMPL_NSGETMODULE(Gtk, components)
