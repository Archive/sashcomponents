
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASH%(capsname)s_H
#define SASH%(capsname)s_H

#include "sashIConstructor.h"
#include "%(interface)s.h"
#include <string>
#include <vector>

%(parentinclude)s

%(declares)s

//%(uuid)s
#define SASH%(capsname)s_CID %(biguuid)s

#define SASH%(capsname)s_CONTRACT_ID "@gnome.org/SashXB/gtk/%(name)s;1"

class %(classname)s : public %(interface)s %(inherit)s
{
 public:
  %(declisupports)s
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHI%(capsname)s

  %(classname)s();
  virtual ~%(classname)s();

%(forward)s

%(extra)s

private:
  %(private)s
};

#endif // SASH%(capsname)s_H
