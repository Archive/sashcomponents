#!/bin/sh
XPIDL=/usr/lib/mozilla-1.0.0/xpidl
XPTLINK=/usr/lib/mozilla-1.0.0/xpt_link
JCXPIDL=`sash-config --dir bin`'/jcxpidl'
MOZIDL=/usr/share/idl/mozilla-1.0.0
SASHIDL=`sash-config --dir idl`
JCJARS1=`sash-config --dir lib`'/components/javaconnect.jar'
JCJARS2=`sash-config --dir lib`'/components/imported.jar'
JCJARS="$JCJARS1:$JCJARS2"
JAVAC=`which javac`
JAR=`which jar`
ST_TOOLKIT_JAR=toolkit/STComm25.jar

if test ! -x $XPIDL ; then
    echo "Failed to locate xpidl. Check whether you have it (as part of your Mozilla distribution) and edit make_jdbc.sh"
    exit 1
fi
if test ! -x $XPTLINK ; then
    echo "Failed to locate xpt_link. Check whether you have it (as part of your Mozilla distribution) and edit make_jdbc.sh"
    exit 1
fi
if test ! -x $JCXPIDL ; then
    echo "Failed to locate jcxpidl. It is a standart part of the SashXB distribution. Check whether SashXB's bin directory is in your path"
    exit 2
fi
if test ! -d $MOZIDL ; then
    echo "Failed to locate Mozilla idl files. Check whether you have them (as part of your Mozilla distribution) and edit make_jdbc.sh"
    exit 3
fi
if test ! -d $SASHIDL ; then
    echo "Failed to locate SashXB idl files. Check whether you have them / see if SashXB's bin directory is in your path"
    exit 4
fi
if test ! -e $JCJARS1 ; then
    echo "Failed to locate javaconnect.jar. It comes with the Java-enabled distribution of SashXB"
    exit 5
fi
if test ! -e $JCJARS2 ; then
    echo "Failed to locate imported.jar. It comes with the Java-enabled distribution of SashXB"
    exit 6
fi
if test ! -e $ST_TOOLKIT_JAR ; then
    echo "Failed to locate Sametime Toolkit. You must obtain it from Lotus Software"
    exit 9
fi
if test ! -x $JAVAC ; then
    echo "Failed to locate javac - the Java compiler. Check whether you have a Java SDK"
    exit 7
fi
if test ! -x $JAR ; then
    echo "Failed to locate jar - The Java archive tool. Check whether you have a Java SDK"
    exit 8
fi


for ff in *.idl; do $JCXPIDL -m jc -I $SASHIDL -I $MOZIDL $ff ; done
for ff in *.idl; do $XPIDL -m typelib -I $SASHIDL -I $MOZIDL $ff ; done
$XPTLINK sashSametime.xpt *.xpt
$JAVAC -d . -classpath $JCJARS sashISametime*.java
$JAR cf sashSametime.jcm.jar org/mozilla/xpcom/*.class

$JAVAC -d . -classpath "$JCJARS:sashSametime.jcm.jar:$ST_TOOLKIT_JAR" PendingTask.java sashSametime*Impl.java
$JAR uf sashSametime.jcm.jar org/sash/stcommunity/*.class

sash-install sashSametime.wdf -f
