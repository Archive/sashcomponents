/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.sash.stcommunity;

import org.mozilla.xpcom.*;
import java.util.*;
import com.lotus.sametime.core.comparch.*;
import com.lotus.sametime.core.types.*;
import com.lotus.sametime.core.constants.*;
import com.lotus.sametime.im.*;
import com.lotus.sametime.awareness.*;

public class sashSametimeUserImpl implements sashISametimeUser {

	public static final int USTATE_NONE= 1;
	public static final int USTATE_RESOLVING= 2;
	public static final int USTATE_INVALID= 3;
	public static final int USTATE_RESOLVED= 4;
	public static final int USTATE_RESOLVED_PROXY= 5;

	private String lookupName;
	private STUser userObj;
	private int state;
	private LinkedList tasks;
	private Im channel;
	
	public sashSametimeUserImpl() {
		state= USTATE_NONE;
		lookupName= null;
		userObj= null;
		tasks= new LinkedList();
		channel= null;
	}
	public sashSametimeUserImpl(String ln) {
		state= USTATE_NONE;
		lookupName= ln;
		userObj= null;
		tasks= new LinkedList();
		channel= null;
	}
	public void setLookupName(String ln) {
		lookupName= ln;
	}
	public void setUserObj(STUser stuser) {
		userObj= stuser;
	}
	public STUser getUserObj() {
		return userObj;
	}
	public int getState() {
		return state;
	}
	public void setState(int nstate) {
		state= nstate;
	}
	
	public Object queryInterface(IID ignored) {
		return null;
	}
    public Object getDescription() {
    	if (userObj== null || !(userObj instanceof STWatchedUser))
    		return null;
    	return ((STWatchedUser) userObj).getStatus().getStatusDescription();
    }
    public Object getStatus() {
    	if (userObj== null || !(userObj instanceof STWatchedUser))
    		return null;
    	int status= ((STWatchedUser) userObj).getStatus().getStatusType();
    	// for compatibility with the Win version
    	switch (status) {
			case STUserStatus.ST_USER_STATUS_ACTIVE:
			case STUserStatus.ST_USER_STATUS_AWAY:
			case STUserStatus.ST_USER_STATUS_OFFLINE:
			case STUserStatus.ST_USER_STATUS_DND:
			case STUserStatus.ST_USER_STATUS_NOT_USING: 
			case STUserStatus.ST_USER_STATUS_UNKNOWN:
				break;
			case STUserStatus.ST_USER_STATUS_ACTIVE_MOBILE:
			case STUserStatus.ST_USER_STATUS_MOBILE:
			case -1:
			default:
				status= STUserStatus.ST_USER_STATUS_OFFLINE;
		}
		return new Integer(status);
	}
    public String getDisplayName() {
    	if (userObj== null)
    		return "";
    	return userObj.getDisplayName();
    }
    public String getID() {
    	if (userObj== null)
    		return "";
    	return userObj.getId().getId();
    }
    public String getLookupName() {
    	if (lookupName== null)
    		return "";
    	return lookupName;
    }
    public void enqueueTask(PendingTask task) {
    	tasks.add(task);
    }
    public PendingTask dequeueTask() {
    	if (tasks.size()== 0)
    		return null;
    	PendingTask result= (PendingTask) tasks.getFirst();
    	tasks.removeFirst();
    	return result;
    }
    public Im getChannel() {
    	return channel;
    }
    public void setChannel(Im chan) {
    	channel= chan;
    }
    public LinkedList getTasks() {
    	return tasks;
    }
    public void setTasks(LinkedList t) {
    	tasks.addAll(t);
    }
}
