/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "nsISupports.idl"
#include "nsIVariant.idl"

interface sashIGenericScriptableConstructor;

[scriptable, uuid(5a7cb9b2-6d20-491b-8460-a911e8f0d10e)]    
interface sashISametimeCommunity: nsISupports {

	readonly attribute long ENC_LEVEL_ALL;
	readonly attribute long ENC_LEVEL_DONT_CARE;
	readonly attribute long ENC_LEVEL_NONE;
	readonly attribute long ENC_LEVEL_RC2_128;
	readonly attribute long ENC_LEVEL_RC2_40;
	
	readonly attribute long STATUS_ACTIVE;
	readonly attribute long STATUS_AWAY;
	readonly attribute long STATUS_DISCONNECTED;
	readonly attribute long STATUS_DND;
	readonly attribute long STATUS_NOT_USING;
	
	// Session(in string server)
	readonly attribute sashIGenericScriptableConstructor Session;
	
};
