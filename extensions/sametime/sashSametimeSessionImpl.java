/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.sash.stcommunity;

import org.mozilla.xpcom.*;

import com.lotus.sametime.core.comparch.*;
import com.lotus.sametime.core.types.*;
import com.lotus.sametime.core.constants.*;
import com.lotus.sametime.community.*;
import com.lotus.sametime.lookup.*;
import com.lotus.sametime.awareness.*;
import com.lotus.sametime.directory.*;
import com.lotus.sametime.im.*;
import com.lotus.sametime.places.*;
import com.lotus.sametime.post.*;
import com.lotus.sametime.storage.*;

import java.util.*;

public class sashSametimeSessionImpl implements
	sashISametimeSession, sashIScriptableConstructor,
	LoginListener,
	ResolveListener,
	AwarenessServiceListener,
	ImServiceListener,
	StatusListener,
	ImListener {

	// Sash related fields
	private int contextID;
	private sashIActionRuntime m_act;
	private sashIGenericScriptableConstructor m_chatConstructor;
	private sashIExtension2 m_parentExt;

	// Services
    private String server;
    private STSession session;
    private CommunityService community;
    private LookupService lookup;

    private AwarenessService awareness;
    private InstantMessagingService messaging;
    private PlacesService places;
    private PostService post;
    private StorageService storage;
    private Resolver resolver;
    

	private STUserStatus myStatus;
	private WatchList watchList;

    // Maps
    private HashMap userMap= null;
    private LinkedList pendingWatches= null;
    
	// Status flags
	private static final int SSTATE_NONE= 1;
	private static final int SSTATE_LOGGING_IN= 2;
	private static final int SSTATE_LOGGED_IN= 3;
	private static final int SSTATE_LOGGING_OUT= 4;
	private int state;
    
	private static final int ACT_ADD_USER= 1;
	private static final int ACT_REMOVE_USER= 2;
	private static final int ACT_ADD_USER_TO_PRIVACY_LIST= 3;
	
	/******************************************************************
	* LoginListener 
	******************************************************************/
	public void loggedIn(LoginEvent event) {
    	lookup= (LookupService) session.getCompApi(LookupService.COMP_NAME);
    	awareness= (AwarenessService) session.getCompApi(AwarenessService.COMP_NAME);
    	messaging= (InstantMessagingService) session.getCompApi(InstantMessagingService.COMP_NAME);

		resolver= lookup.createResolver(
			false, // all mathces
			true, // non-exhaustive
			true, // return resolved users
			false
		);

		resolver.addResolveListener(this);
		awareness.addAwarenessServiceListener(this);
		messaging.addImServiceListener(this);
		messaging.registerImType(ImTypes.IM_TYPE_CHAT);

		state= SSTATE_LOGGED_IN;

		// Notify OnLoginComplete event handler
		Object args[]= new Object[3];
		args[0]= new Boolean(true); // success= true
		args[1]= "Congrats";
		args[2]= ((STUser) event.getLogin().getMyUserInstance()).getDisplayName();
		m_act.callEventByID(propOnLoginComplete, contextID, args, new Object[1]);
	}
	public void loggedOut(LoginEvent event) {
		if (state== SSTATE_LOGGING_IN) {
			// failed to log in
			state= SSTATE_NONE;
			
			// Notify OnLoginComplete event handler
			Object args[]= new Object[3];
			args[0]= new Boolean(false); // success= false
			args[1]= "Reason #"+ event.getReason()+ ";";
			args[2]= "";
			m_act.callEventByID(propOnLoginComplete, contextID, args, new Object[1]);
		}
		else {
			resolver.removeResolveListener(this);
			awareness.removeAwarenessServiceListener(this);
			messaging.removeImServiceListener(this);
	
			state= SSTATE_NONE;

			// Notify OnDisconnected event handler
			Object args[]= new Object[1];
			args[0]= "Reason #"+ event.getReason()+ ";";
			m_act.callEventByID(propOnDisconnect, contextID, args, new Object[1]);
		}
	}

	/******************************************************************
	* ResolveListener
	******************************************************************/
	public void resolved(ResolveEvent event) {
		STUser userObj= (STUser) event.getResolved();
		String ln= event.getName();
		sashSametimeUserImpl user= getUser(ln);
		if (user== null) {
			user= new sashSametimeUserImpl(ln);
			userMap.put(ln, user);
		}
		if (userObj.getId().getId().equals(ln)) {
			user.setUserObj(userObj);
			user.setState(sashSametimeUserImpl.USTATE_RESOLVED);
			watchUser(user);
		    processPendingTasks(user);
		}
		else {
			user.setUserObj(userObj);
			user.setState(sashSametimeUserImpl.USTATE_RESOLVED_PROXY);
			sashSametimeUserImpl realUser= new sashSametimeUserImpl(userObj.getId().getId());
			realUser.setUserObj(userObj);
			realUser.setState(sashSametimeUserImpl.USTATE_RESOLVED);
			realUser.setTasks(user.getTasks());
			userMap.put(user.getID(), realUser);
			watchUser(realUser);
		    processPendingTasks(realUser);
		}
	}
	public void resolveConflict(ResolveEvent event) {
		String ln= event.getName();
		System.err.println("Resolving conflict for name: "+ ln);
		userMap.remove(ln);
	}
	public void resolveFailed(ResolveEvent event) {
		String ln= event.getName();
		System.err.println("Error resolving name: "+ ln);
		userMap.remove(ln);
	}

	/******************************************************************
	* AwarenessServiceListener
	******************************************************************/
	public void serviceAvailable(AwarenessServiceEvent event) {
		watchList= awareness.createWatchList();
		watchList.addStatusListener(this);
		while (pendingWatches.size()> 0) {
			sashSametimeUserImpl user= (sashSametimeUserImpl) pendingWatches.getFirst();
			pendingWatches.removeFirst();
			watchList.addItem(user.getUserObj());
		}
	}
	public void serviceUnavailable(AwarenessServiceEvent event) {
		watchList.removeStatusListener(this);
		watchList.close();
	}

	/******************************************************************
	* StatusListener
	******************************************************************/
	public void groupCleared(StatusEvent event) {
		System.err.println("to do: handle groupCleared");
	}
	public void userStatusChanged(StatusEvent event) {
		STWatchedUser changedList[]= event.getWatchedUsers();
		for (int i= 0; i< changedList.length; ++i) {
			sashSametimeUserImpl changedUser= getUser(changedList[i].getId().getId());
			if (changedUser!= null) {
				changedUser.setUserObj(changedList[i]);
			    processPendingTasks(changedUser);
			}

			// Notify OnUpdateUser event handler
			Object args[]= new Object[1];
			args[0]= changedUser;
			m_act.callEventByID(propOnUpdateUser, contextID, args, new Object[1]);
		}
	}
	/******************************************************************
	* ImServiceListener
	******************************************************************/
	public void imReceived(ImEvent evt) {
		STUser partner= evt.getIm().getPartner();
		sashSametimeUserImpl user= getUser(partner.getId().getId());
		if (user== null) {
			// Notify OnChannelRequest event handler
			Object args[]= new Object[2];
			args[0]= partner.getId().getId();
			args[1]= new Integer((int) evt.getIm().getEncLevel().getValue());
			Object result[]= new Object[1];
			m_act.callEventByID(propOnChannelRequest, contextID, args, result);
			if (result[0]== null || !(result[0] instanceof Boolean) ||
				!((Boolean) result[0]).booleanValue()) {
			
				// user refused to accept im
				evt.getIm().close(STError.ST_ABORT);
				return;
			}
			user= new sashSametimeUserImpl();
			user.setUserObj(partner);
			user.setChannel(evt.getIm());
			user.setState(sashSametimeUserImpl.USTATE_RESOLVED);
			userMap.put(user.getID(), user);

			// Notify OnChannelOpened event handler
			args[0]= user.getID();
			args[1]= new Integer((int) community.getLoginType());
			m_act.callEventByID(propOnChannelOpened, contextID, args, new Object[1]);
			user.getChannel().addImListener(this);
		}
		else {
			if (user.getChannel()!= null)
				user.getChannel().removeImListener(this);
			user.setChannel(evt.getIm());
			evt.getIm().addImListener(this);
		}
	}

	/******************************************************************
	* ImListener
	******************************************************************/
	public void dataReceived(ImEvent evt) {
		STUser partner= evt.getIm().getPartner();
		sashSametimeUserImpl user= getUser(partner.getId().getId());
		if (user== null || !(user.getState()== sashSametimeUserImpl.USTATE_RESOLVED))
			return;
		Object args[]= new Object[6];
		args[0]= user.getID();
		byte data[]= evt.getData();
		if (data== null)
			data= new byte[0];
		args[1]= data; // byte[]
		args[2]= new Integer(evt.getDataType());
		args[3]= new Integer(evt.getDataSubType());
		args[4]= new Boolean(evt.isEncrypted());
		args[5]= new Integer((int) community.getLoginType());
		m_act.callEventByID(propOnData, contextID, args, new Object[1]);
	}
	
	public void imClosed(ImEvent evt) {
		STUser partner= evt.getIm().getPartner();
		sashSametimeUserImpl user= getUser(partner.getId().getId());
		if (user== null)
			return;
		if (user.getChannel()!= null)
			user.getChannel().removeImListener(this);
		user.setChannel(null);
		Object args[]= new Object[1];
		args[0]= user.getID();
		m_act.callEventByID(propOnChannelClosed, contextID, args, new Object[1]);
	}

	public void imOpened(ImEvent evt) {
		String partnerName= evt.getIm().getPartner().getId().getId();
		sashSametimeUserImpl user= getUser(partnerName);
	    processPendingTasks(user);

		// Notify OnChannelOpened event handler
		Object args[]= new Object[2];
		args[0]= user.getID();
		args[1]= new Integer((int) community.getLoginType());
		m_act.callEventByID(propOnChannelOpened, contextID, args, new Object[1]);
	}

	public void openImFailed(ImEvent evt) {
		// silently fail to open channel and discard all pending tasks
		String partnerName= evt.getIm().getPartner().getId().getId();
		sashSametimeUserImpl user= getUser(partnerName);
		if (user!= null)
			user.setTasks(new LinkedList());
	}
	
	public void textReceived(ImEvent evt) {
		STUser partner= evt.getIm().getPartner();
		sashSametimeUserImpl user= getUser(partner.getId().getId());
		if (user== null || !(user.getState()== sashSametimeUserImpl.USTATE_RESOLVED)) {
			return;
		}
		Object args[]= new Object[4];
		args[0]= user.getID();
		args[1]= evt.getText();
		args[2]= new Boolean(evt.isEncrypted());
		args[3]= new Integer((int) community.getLoginType());
		m_act.callEventByID(propOnMessage, contextID, args, new Object[1]);
	}

	/******************************************************************
	* Private utility functions
	******************************************************************/
    private sashSametimeUserImpl getUser(String user) {
    	sashSametimeUserImpl result= (sashSametimeUserImpl) userMap.get(user);
    	if (result== null)
    		return null;
    	if (result.getState()== sashSametimeUserImpl.USTATE_RESOLVED_PROXY) {
    		result= (sashSametimeUserImpl) userMap.get(result.getID());
    		if (result== null) // parent was deleted, delete this user too
    			userMap.remove(user);
    	}
    	return result;
    }
	private void addUser(String user, PendingTask task) {
    	if (user== null)
    		return;
		if (userMap.containsKey(user)) {
			sashSametimeUserImpl luser= getUser(user);
			if (luser!= null && awareness.isServiceAvailable())
				watchList.addItem(luser.getUserObj());
			return;
		}
		sashSametimeUserImpl userObj= new sashSametimeUserImpl(user);
		userObj.setState(sashSametimeUserImpl.USTATE_RESOLVING);
		if (task!= null)
			userObj.enqueueTask(task);
		userMap.put(user, userObj);
		resolver.resolve(user);
	}
	private void addUser(String user) {
		addUser(user, null);
	}
	private void removeUser(String user) {
	}
	private void addUserToPrivacyList(String user) {
	}
	private void actOnUser(String user, int action) {
		switch (action) {
			case ACT_ADD_USER:
				addUser(user);
				break;
			case ACT_REMOVE_USER:
				removeUser(user);
				break;
			case ACT_ADD_USER_TO_PRIVACY_LIST:
				// has no immediate effect
				addUserToPrivacyList(user);
				break;
		}
		return;
	}
    private void forAllUsers(Object users, int action) {
    	if (users instanceof String) {
    		actOnUser((String) users, action);
    		return;
    	}
    	if (users instanceof String[]) {
    		String []__users= (String[]) users;
	    	for (int i= 0; i< __users.length; ++i)
	    		actOnUser(__users[i], action);
    		return;
    	}
    	if (users instanceof sashISametimeUser[]) {
    		sashISametimeUser []__users= (sashISametimeUser[]) users;
	    	for (int i= 0; i< __users.length; ++i)
	    		actOnUser(__users[i].getID(), action);
    		return;
    	}
    	if (users instanceof Object[]) {
    		Object []__users= (Object[]) users;
	    	for (int i= 0; i< __users.length; ++i) {
	    		if (__users[i] instanceof String)
		    		actOnUser((String) __users[i], action);
	    		if (__users[i] instanceof sashISametimeUser) {
		    		actOnUser(((sashISametimeUser) __users[i]).getID(), action);
		    	}
		    }
    		return;
    	}
    }
	private void watchUser(sashSametimeUserImpl user) {
		if (state!= SSTATE_LOGGED_IN || !awareness.isServiceAvailable()) {
			pendingWatches.add(user);
			return;
		}
		watchList.addItem(user.getUserObj());
	}

	/******************************************************************
	* sashISametimeSession methods
	******************************************************************/
    public void login(String user, String password) {
    	if (state!= SSTATE_NONE)
    		return;
    	state= SSTATE_LOGGING_IN;
    	community.loginByPassword(
    		server,
    		user,
    		password
    	);
    }
    public void disconnect() {
    	if (state!= SSTATE_LOGGED_IN && state!= SSTATE_LOGGING_IN)
    		return;
		state= SSTATE_LOGGING_OUT;
    	community.logout();
    	try {
	    	// sleep for 10ms
	    	Thread.sleep(10);
	    } catch (Exception ex) {
	    }
    }

    public void addUsers(Object users) {
    	if (state!= SSTATE_LOGGED_IN)
    		return;
    	forAllUsers(users, ACT_ADD_USER);
    }

    public void closeChannel(String user) {
    	if (state!= SSTATE_LOGGED_IN)
    		return;
    	sashSametimeUserImpl luser= getUser(user);
    	if (luser== null || luser.getState()!= sashSametimeUserImpl.USTATE_RESOLVED)
	    	return;
	    if (luser.getChannel()!= null) {
		    luser.getChannel().close(STError.ST_ABORT);
		}
	    luser.setChannel(null);
    }

    /* nsIVariant GetEncryptionLevel (in wstring user); */
    public Object getEncryptionLevel(String user) {
    	// return null or Integer(ENC_LEVEL);
    	return null;
    }

    public boolean isChannelOpen(String user) {
    	sashSametimeUserImpl luser= getUser(user);
    	if (luser== null || luser.getState()!= sashSametimeUserImpl.USTATE_RESOLVED)
	    	return false;
	    return luser.getChannel()!= null;
    }
    
    public void openChannel(String user, Object encryptionLevel) {
    	sashSametimeUserImpl luser= getUser(user);
    	if (luser== null) {
    		Object[] args= new Object[1];
    		args[0]= encryptionLevel;
    		addUser(user, new PendingTask(PendingTask.PTASK_OPEN_CHANNEL, args));
    		return;
    	}
    	if (luser.getState()!= sashSametimeUserImpl.USTATE_RESOLVED) {
    		Object[] args= new Object[1];
    		args[0]= encryptionLevel;
    		luser.enqueueTask(new PendingTask(PendingTask.PTASK_OPEN_CHANNEL, args));
    		return;
    	}
		internalOpenChannel(luser, encryptionLevel);
    }
    
    private void internalOpenChannel(sashSametimeUserImpl user, Object encryptionLevel) {
    	sashSametimeUserImpl luser= getUser(user.getID());
    	if (luser== null || luser.getState()!= sashSametimeUserImpl.USTATE_RESOLVED)
    		return;
    	Im currentIm= luser.getChannel();
    	EncLevel enc= EncLevel.ENC_LEVEL_DONT_CARE;
    	if (encryptionLevel!= null && (encryptionLevel instanceof Integer)) {
    		System.err.println("To do: support different encryption levels");
    		// to do: convert int to EncLevel
    	}
    	if (currentIm== null) { // need to open new channel
    		currentIm= messaging.createIm(luser.getUserObj(), enc, ImTypes.IM_TYPE_CHAT);
    		currentIm.addImListener(this);
    		luser.setChannel(currentIm);
    		currentIm.open();
    	}
    	else {
    		if (!currentIm.isOpen())
    			currentIm.open();
    	}
    }

    public void removeUsers(Object users) {
    	if (state!= SSTATE_LOGGED_IN)
    		return;
    	forAllUsers(users, ACT_REMOVE_USER);
    }

    public void sendMessage(String user, String text) {
    	sashSametimeUserImpl luser= getUser(user);
    	if (luser== null) {
    		Object[] args1= new Object[1];
    		args1[0]= new Integer(getENC_LEVEL_ALL());
    		addUser(user, new PendingTask(PendingTask.PTASK_OPEN_CHANNEL, args1));
    		Object[] args2= new Object[1];
    		args2[0]= text;
    		luser= getUser(user);
    		luser.enqueueTask(new PendingTask(PendingTask.PTASK_SEND_MESSAGE, args2));
    		return;
    	}
    	if (luser.getState()!= sashSametimeUserImpl.USTATE_RESOLVED
    		|| luser.getChannel()== null || !luser.getChannel().isOpen()) {
    		if (luser.getChannel()== null)
	    		openChannel(user, null);
			Object[] args2= new Object[1];
			args2[0]= text;
			luser.enqueueTask(new PendingTask(PendingTask.PTASK_SEND_MESSAGE, args2));
			return;
		}
		internalSendMessage(luser, text);
    }
    
    private void internalSendMessage(sashSametimeUserImpl user, String text) {
    	sashSametimeUserImpl luser= getUser(user.getID());
    	Im currentIm= luser.getChannel();
    	if (currentIm== null) {
    		return;
    	}
    	currentIm.sendText(true, text);
	}

    public void setPrivacyList(Object users, boolean excluding) {
    }

    public void setStatus(int status, String message) {
    }

    public void responding(String user, Object starting) {
    }

    public void sendData(String user, String data, int type, int subType) {
    }

    public void terminate() {
    	System.err.println("Terminating session");
		community.logout();
    	session.stop();
    	session.unloadSession();
    }

	public Object queryInterface(IID ignored) {
		return null;
	}
	
    sashSametimeSessionImpl() {
    	session= null;
    	server= null;
    }
    
    private void processPendingTasks(sashSametimeUserImpl user) {
    	if (user.getState()!= sashSametimeUserImpl.USTATE_RESOLVED)
    		return;
    	LinkedList tasks= user.getTasks();
    	user.setTasks(new LinkedList());
    	while (tasks.size()> 0) {
    		PendingTask pt= (PendingTask) tasks.getFirst();
    		switch (pt.getAction()) {
    			case PendingTask.PTASK_OPEN_CHANNEL:
    				openChannel(user.getID(), pt.getArgs()[0]);
    				break;
    			case PendingTask.PTASK_CLOSE_CHANNEL:
    				closeChannel(user.getID());
    				break;
    			case PendingTask.PTASK_RESPONDING:
    				if (user.getChannel()!= null && user.getChannel().isOpen())
	    				responding(user.getID(), pt.getArgs()[0]);
	    			else
	    				user.enqueueTask(pt);
	    			break;
    			case PendingTask.PTASK_SEND_DATA:
    				if (user.getChannel()!= null && user.getChannel().isOpen())
	    				sendData(
	    					user.getID(), 
	    					(String) pt.getArgs()[0],
	    					((Integer) pt.getArgs()[1]).intValue(),
	    					((Integer) pt.getArgs()[2]).intValue()
	    				);
	    			else
	    				user.enqueueTask(pt);
	    			break;
    			case PendingTask.PTASK_SEND_MESSAGE:
    				if (user.getChannel()!= null && user.getChannel().isOpen())
	    				sendMessage(user.getID(), (String) pt.getArgs()[0]);
	    			else
	    				user.enqueueTask(pt);
	    			break;
	    	}
	    	tasks.removeFirst();
	    }
    }
	
	public sashIGenericScriptableConstructor getChat() {
		try {
			if (m_chatConstructor== null) {
				m_chatConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance(
						"@gnome.org/SashXB/sashGenericConstructor;1"
					);
				m_chatConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@adtech.ibm.com/STCommunity/Chat;1",
					sashISametimeChat.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_chatConstructor;
	}

	// satisfy the sashIScriptableConstructor requirements
    public boolean initializeNewObject(
    	sashIActionRuntime actionRuntime,
    	sashIExtension2 parentExtension,
    	int contextID, Object[] args) {

    	m_act= actionRuntime;
    	this.contextID= contextID;
    	m_parentExt= parentExtension;
    	
    	if (args.length!= 1 || ! (args[0] instanceof String)) {
    		System.err.println("Session constructor expects 1 string (server name)");
    		return false; // fail to initialize
    	}
    	
    	// argument expected is server name
    	server= (String) args[0];
    	try {
    		// If this is not unique we don't deserve a session
    		Random sessionid= new Random();
    		String sessionstr= ""+ this+ m_act+ m_parentExt+ sessionid.nextLong()+ "-"+ sessionid.nextLong();
    		session= new STSession(sessionstr);
	    	session.loadSemanticComponents();
 	   	
	    	community= (CommunityService) session.getCompApi(CommunityService.COMP_NAME);

 	   	session.start();

	    	community.addLoginListener(this);
		
			userMap= new HashMap();
			pendingWatches= new LinkedList();
	    	
	    } catch (DuplicateObjectException ex) {
	    	System.err.println("Session already open");
	    	return false; // fail to instantiate Session
	    }
	    state= SSTATE_NONE;
	    return true; // object instantiated and initialized
    }

	/******************************************************************
	* Constants
	******************************************************************/
    public int getENC_LEVEL_ALL() { return EncLevel.ENC_LEVEL_ALL.getValue(); }
    public int getENC_LEVEL_DONT_CARE() { return EncLevel.ENC_LEVEL_DONT_CARE.getValue(); }
    public int getENC_LEVEL_NONE() { return EncLevel.ENC_LEVEL_NONE.getValue(); }
    public int getENC_LEVEL_RC2_128() { return EncLevel.ENC_LEVEL_RC2_128.getValue(); }
    public int getENC_LEVEL_RC2_40() { return EncLevel.ENC_LEVEL_RC2_40.getValue(); }
    public int getSTATUS_ACTIVE() { return STUserStatus.ST_USER_STATUS_ACTIVE; }
    public int getSTATUS_AWAY() { return STUserStatus.ST_USER_STATUS_AWAY; }
    public int getSTATUS_DISCONNECTED() { return STUserStatus.ST_USER_STATUS_OFFLINE; }
    public int getSTATUS_DND() { return STUserStatus.ST_USER_STATUS_DND; }
    public int getSTATUS_NOT_USING() { return STUserStatus.ST_USER_STATUS_NOT_USING; }

	/******************************************************************
	* Events
	******************************************************************/
	private String propOnChannelClosed;
    public String getOnChannelClosed() { return propOnChannelClosed; }
    public void setOnChannelClosed(String value) { propOnChannelClosed= value; }

	private String propOnChannelOpened;
    public String getOnChannelOpened() { return propOnChannelOpened; }
    public void setOnChannelOpened(String value) { propOnChannelOpened= value; }

	private String propOnChannelRequest;
    public String getOnChannelRequest() { return propOnChannelRequest; }
    public void setOnChannelRequest(String value) { propOnChannelRequest= value; }

	private String propOnChatInvitation;
    public String getOnChatInvitation() { return propOnChatInvitation; }
    public void setOnChatInvitation(String value) { propOnChatInvitation= value; }

	private String propOnData;
    public String getOnData() { return propOnData; }
    public void setOnData(String value) { propOnData= value; }

	private String propOnDisconnect;
    public String getOnDisconnect() { return propOnDisconnect; }
    public void setOnDisconnect(String value) { propOnDisconnect= value; }

	private String propOnError;
    public String getOnError() { return propOnError; }
    public void setOnError(String value) { propOnError= value; }

	private String propOnLoginComplete;
    public String getOnLoginComplete() { return propOnLoginComplete; }
    public void setOnLoginComplete(String value) { propOnLoginComplete= value; }

	private String propOnMessage;
    public String getOnMessage() { return propOnMessage; }
    public void setOnMessage(String value) { propOnMessage= value; }

	private String propOnResponding;
    public String getOnResponding() { return propOnResponding; }
    public void setOnResponding(String value) { propOnResponding= value; }

	private String propOnUpdateStatus;
    public String getOnUpdateStatus() { return propOnUpdateStatus; }
    public void setOnUpdateStatus(String value) { propOnUpdateStatus= value; }

	private String propOnUpdateUser;
    public String getOnUpdateUser() { return propOnUpdateUser; }
    public void setOnUpdateUser(String value) { propOnUpdateUser= value; }
}
