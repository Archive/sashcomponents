/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.sash.stcommunity;

import org.mozilla.xpcom.*;
import com.lotus.sametime.core.types.*;
import com.lotus.sametime.core.constants.*;

public class sashSametimeCommunityImpl implements sashISametimeCommunity, sashIExtension2 {

	private sashIActionRuntime m_act;
	private int contextID;
	private sashIGenericScriptableConstructor m_sessionConstructor;
	
	public Object queryInterface(IID ignored) {
		return null;
	}
	
	public sashSametimeCommunityImpl() {
		m_sessionConstructor= null;
	}

	public sashIGenericScriptableConstructor getSession() {
		try {
			if (m_sessionConstructor== null) {
				m_sessionConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance(
						"@gnome.org/SashXB/sashGenericConstructor;1"
					);
				m_sessionConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@adtech.ibm.com/STCommunity/Session;1",
					sashISametimeSession.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_sessionConstructor;
	}

    public String getSashName() {
    	return "STCommunity";
    }

    public void initialize(
    	sashIActionRuntime act,
    	String registrationXml,
    	String webGuid,
    	int contextID) {
    	
    	m_act= act;
    	this.contextID= contextID;
    }

    public void cleanup() {
    }
    
    public void processEvent() {
	}

	/******************************************************************
	* Constants
	******************************************************************/
    public int getENC_LEVEL_ALL() { return EncLevel.ENC_LEVEL_ALL.getValue(); }
    public int getENC_LEVEL_DONT_CARE() { return EncLevel.ENC_LEVEL_DONT_CARE.getValue(); }
    public int getENC_LEVEL_NONE() { return EncLevel.ENC_LEVEL_NONE.getValue(); }
    public int getENC_LEVEL_RC2_128() { return EncLevel.ENC_LEVEL_RC2_128.getValue(); }
    public int getENC_LEVEL_RC2_40() { return EncLevel.ENC_LEVEL_RC2_40.getValue(); }
    public int getSTATUS_ACTIVE() { return STUserStatus.ST_USER_STATUS_ACTIVE; }
    public int getSTATUS_AWAY() { return STUserStatus.ST_USER_STATUS_AWAY; }
    public int getSTATUS_DISCONNECTED() { return STUserStatus.ST_USER_STATUS_OFFLINE; }
    public int getSTATUS_DND() { return STUserStatus.ST_USER_STATUS_DND; }
    public int getSTATUS_NOT_USING() { return STUserStatus.ST_USER_STATUS_NOT_USING; }
}
