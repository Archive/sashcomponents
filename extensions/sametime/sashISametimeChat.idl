/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "nsISupports.idl"
#include "nsIVariant.idl"

[scriptable, uuid(75949dff-f680-4766-8fa7-04cd6d86a6fa)]    
interface sashISametimeChat: nsISupports {

	attribute string OnEntered;
	attribute string OnLeft;
	attribute string OnMessage;
	attribute string OnResponding;
	attribute string OnUserLeft;
	attribute string OnUserEntered;
	
	void Enter(
	);
	
	void Invite(
		in nsIVariant invitees,
		in wstring topic,
		in wstring message,
		in boolean autoInvite
	);
	
	void Leave(
	);
	
	void Responding(
		in boolean started
	);
	
	void SendMessage(
		in wstring text
	);
	
	readonly attribute long EncryptionLevel;
	
	attribute boolean ExplicitlyRestrictUsers;
	
	readonly attribute wstring Name;
	
	// sashISametimeSession
	readonly attribute nsIVariant Session;
	
	// array of sashISametimeUser
	readonly attribute nsIVariant Users;
	
};
