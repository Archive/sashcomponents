/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.sash.stcommunity;

public class PendingTask {
	
	private int action;
	private Object[] args;
	
	public static final int PTASK_OPEN_CHANNEL= 1;
	public static final int PTASK_CLOSE_CHANNEL= 2;
	public static final int PTASK_RESPONDING= 3;
	public static final int PTASK_SEND_DATA= 4;
	public static final int PTASK_SEND_MESSAGE= 5;
	
	public PendingTask(int act, Object[] task_args) {
		action= act;
		args= task_args;
	}
	
	public int getAction() {
		return action;
	}
	public Object[] getArgs() {
		return args;
	}
}
