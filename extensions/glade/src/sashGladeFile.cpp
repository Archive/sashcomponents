
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashGladeFile.h"
#include "nsCRT.h"

#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "sash_constants.h"
#include "GtkObjectFactory.h"
#include "sashGtkWidget.h"
#include "sashGtkExtension.h"
#include "sashIGtkBrowser.h"
#include <unistd.h>
#include "sashGlade.h"
#include <iostream>

extern sashGlade * GetRegisteredGladeExt();

/* Implementation file */
NS_IMPL_ISUPPORTS2_CI(sashGladeFile, sashIGladeFile, sashIConstructor)

void SignalConnect(const gchar *handler_name, 
									 GtkObject *object, 
									 const gchar *signal_name, 
									 const gchar *signal_data, 
									 GtkObject *connect_object, 
									 gboolean after, 
									 gpointer user_data)
{
  sashGladeFile * gladefile = (sashGladeFile *) user_data;
  DEBUGMSG(glade, "Connecting handler %s, signal %s, data %s\n",
					 handler_name, signal_name, signal_data);
  gladefile->Connect(object, signal_name, signal_data, handler_name);
}


sashGladeFile::sashGladeFile()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */	
}

sashGladeFile::~sashGladeFile()
{
  /* destructor code */
}

NS_IMETHODIMP sashGladeFile::InitializeNewObject(sashIActionRuntime * actionRuntime, 
												 sashIExtension *parentExtension, 
												 JSContext * cx, 
												 PRUint32 argc, nsIVariant ** argv, 
												 PRBool *_retval)
{
	if(argc != 1)
		{
		*_retval = false;
		return NS_OK;
	}
	
	if (! VariantIsString(argv[0])){
		*_retval = false;
		return NS_OK;
	}

	sashIGtkObjectFactory *fact;
	sashIExtension *gtkext;
	nsCOMPtr<sashIGlade> gladeext = do_QueryInterface(parentExtension);
	gladeext->GetGtkFactory(&fact);
	gladeext->GetGtkExtension(&gtkext);
	m_fact = fact;
	m_gtk_ext = gtkext;
	NS_IF_RELEASE(fact);
	NS_IF_RELEASE(gtkext);
	gladeext = NULL;

	string gladefile = VariantGetString(argv[0]);

  DEBUGMSG(glade, "Loading file %s\n", gladefile.c_str());
  actionRuntime->AssertFSAccess(gladefile.c_str());
  /* load the interface */
  m_xml = glade_xml_new(gladefile.c_str(), NULL);
  
  /* connect the signals in the interface */
  glade_xml_signal_autoconnect_full(m_xml, SignalConnect, this);
  DEBUGMSG(glade, "Done connecting\n");

	*_retval = true;
	return NS_OK;
}

void sashGladeFile::Connect(GtkObject *obj,  
							const char *signalName, 
							const char *signalData,
							const char *handlerName)
{
  // TODO: store this
  nsCOMPtr<sashIGtkExtension> gtk_ext(do_QueryInterface(m_gtk_ext));

  nsIVariant * js_data;
  NewVariant(&js_data);
  if (signalData)
	   VariantSetString(js_data, signalData);
  gtk_ext->SignalConnectNative(obj, signalName, handlerName, js_data);
  NS_IF_RELEASE(js_data);
}

NS_IMETHODIMP sashGladeFile::GetWidget(const char *name, sashIGtkWidget **retVal)
{
  assert(m_xml != NULL);
  DEBUGMSG(glade, "Getting widget %s ...", name);
  GtkWidget * widget = glade_xml_get_widget(m_xml, name);
  if (widget == NULL) {
    DEBUGMSG(glade, "failed\n");
    return NS_COMFALSE;
  }
  DEBUGMSG(glade, "success\n");
  nsresult rv = m_fact->GetXPObject(GTK_OBJECT(widget), 
				    (sashIGtkObject **) retVal);
  assert(rv == NS_OK);
  DEBUGMSG(glade, "Got XP Object\n");
  return NS_OK;
}


// The following code allows the use of the syntax gladeFile.widget
// to get widgets from the glade file object

/*
#define XPC_MAP_CLASSNAME sashGladeFile
#define XPC_MAP_QUOTED_CLASSNAME "sashGladeFile"
#define XPC_MAP_WANT_GETPROPERTY
#define XPC_MAP_FLAGS 0
#include "xpc_map_end.h"

NS_IMETHODIMP sashGladeFile::GetProperty(nsIXPConnectWrappedNative *wrapper, 
										 JSContext * cx, 
										 JSObject * obj, 
										 jsval id, 
										 jsval * vp, 
										 PRBool *_retval) 
{
  sashIGtkWidget * w;

  *vp = JSVAL_NULL;
  JSString * jstring = JS_ValueToString(cx, id);
  if (jstring) {
	char * widgetName = JS_GetStringBytes(jstring);
	if (GetWidget(widgetName, &w) == NS_OK) {

	  JSObject *jsobj;
	  jsobj = RuntimeTools::WrapSashObject(NS_GET_IID(sashIGtkWidget), w, cx);
	  
	  *vp= OBJECT_TO_JSVAL(jsobj);
	}
  }

  *_retval = true;
  return NS_OK;
}
*/
