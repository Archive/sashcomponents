
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

#include <gnome.h>
#include <jsapi.h>
#include <nsIGenericFactory.h>
#include "sashGlade.h"
#include "debugmsg.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sash_constants.h"
#include "GtkObjectFactory.h"
#include "sashGtkWidget.h"
#include "sashGtkExtension.h"
#include <unistd.h>

#include "sashGladeFile.h"
#include "sashIGenericConstructor.h"
#include "nsIGenericFactory.h"
#include "sashIGtkBrowser.h"


// XXX --- this is an ugly hack to support mozembed through the custom widget in Glade
extern "C" {
#define new libgladesucks1
#define class libgladesucks2
#include <glade/glade.h>
#include <glade/glade-build.h>
#undef new
#undef class
}


static sashGlade * g_GladeExt = NULL;

void RegisterGladeExt(sashGlade * GladeExt)
{
	 g_GladeExt = GladeExt;
}

sashGlade * GetRegisteredGladeExt()
{
	 if (g_GladeExt == NULL)
	 {
		  OutputError("ERROR: Glade: No Glade extension registered for custom widget construction\n");
	 }
	 return g_GladeExt;
}

GtkWidget * 
sash_moz_embed_new(GladeXML * xml, GladeWidgetInfo * info)
{
	 sashGlade * gladeExt = GetRegisteredGladeExt();
	 sashIGtkBrowser * browser;
	 
	 NewSashGtkBrowser(&browser, gladeExt->getActionRuntime());
  
	 DEBUGMSG(glade, "Creating new sash browser %s\n", info->name);

	 string url = "about:blank";
	 bool enableSashContext = false;

	 GList * p = info->attributes;
	 while (p) {
		  GladeAttribute * attr = (GladeAttribute *) p->data;
		  DEBUGMSG(glade, "MozEmbed attribute %s: %s\n", attr->name, attr->value);
		  if (strcmp(attr->name, "string1") == 0) {
			   url = attr->value;
		  }
		  else if (strcmp(attr->name, "int1") == 0) {
			   enableSashContext = (atoi(attr->value) != 0);
		  }
		  p = p->next;
	 }

	 browser->LoadURL(url.c_str());
	 browser->SetEnableSashContext(enableSashContext);
	 GtkWidget * mozWidget;
	 browser->GetWidget(&mozWidget);
//	 gtk_widget_realize(mozWidget);
//	 gtk_widget_show(mozWidget);

	 return mozWidget;
}


// End ugly hack
//--------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                           Library interface functions
//-----------------------------------------------------------------------------

SASH_EXTENSION_IMPL_NO_NSGET(sashGlade, sashIGlade, "Glade");

sashGlade::sashGlade() 
{
  NS_INIT_ISUPPORTS();
	
  nsCID cid = SASHGTKEXTENSION_CID;
  nsIID iid = SASHIEXTENSION_IID;

  nsresult rv = 
		nsComponentManager::CreateInstance(cid, nsnull, iid,
										   (void **) getter_AddRefs(m_gtk_ext));
  assert(rv == NS_OK);	

}

sashGlade::~sashGlade()
{
	NS_IF_RELEASE(m_Constructor);
}

NS_IMETHODIMP
sashGlade::Initialize(
		     sashIActionRuntime *act,
		     const char *xml,
		     const char *instanceGuid, 
		     JSContext *context, JSObject *object)
{
	static GladeWidgetBuildData widgets[] = {
		{"Custom", sash_moz_embed_new, NULL},
		{NULL, NULL, NULL}};
	static bool initialized = false;

	if (!initialized) {
		DEBUGMSG(glade, "Initializing glade\n");
		glade_gnome_init();
		glade_register_widgets(widgets);
		initialized = true;
	}
  
	DEBUGMSG(glade, "Registering widgets\n");

	DEBUGMSG(glade, "Initializing. xml = %s\n", xml);
	m_act = act;

	NewSashConstructor(m_act, this, SASHGLADEFILE_CONTRACT_ID,
					   SASHIGLADEFILE_IID_STR, &m_Constructor);
	
	RegisterGladeExt(this);

	nsresult rv = m_gtk_ext->Initialize(act, "", instanceGuid, context, object);

	nsCOMPtr<sashIGtkExtension> gtk_ext(do_QueryInterface(m_gtk_ext));
	rv = gtk_ext->GetFactory(getter_AddRefs(m_fact));
	if (NS_FAILED(rv))
		return rv;

	return rv;
}
  
NS_IMETHODIMP
sashGlade::ProcessEvent()
{
  return NS_OK;
}

NS_IMETHODIMP
sashGlade::Cleanup()
{
  return NS_OK;
}

/* readonly attribute sashIGenericConstructor GladeFile; */
NS_IMETHODIMP sashGlade::GetGladeFile(sashIGenericConstructor * *aGladeFile)
{
	*aGladeFile = m_Constructor;
	NS_IF_ADDREF(*aGladeFile);
	return NS_OK;
}

/* [noscript] readonly attribute gtkobj GtkFactory; */
NS_IMETHODIMP sashGlade::GetGtkFactory(sashIGtkObjectFactory * *aGtkFactory)
{
	*aGtkFactory = m_fact.get();
	NS_IF_ADDREF(*aGtkFactory);
	return NS_OK;
}

/* [noscript] readonly attribute gtkext GtkExtension; */
NS_IMETHODIMP sashGlade::GetGtkExtension(sashIExtension * *aGtkExtension)
{
	*aGtkExtension = m_gtk_ext.get();
	NS_IF_ADDREF(*aGtkExtension);
	return NS_OK;
}

sashIActionRuntime *
sashGlade::getActionRuntime()
{
	return m_act;
}

/* void MessageDialog (in string message); */
NS_IMETHODIMP sashGlade::MessageDialog(const char *message)
{    
  GtkWidget * dialog = gnome_message_box_new(_(message),GNOME_MESSAGE_BOX_INFO,
                                               _("OK"),NULL);
  gnome_dialog_run_and_close(GNOME_DIALOG(dialog));
  return NS_OK;
}

/* void YesNoDialog (in string message, out PRBool result); */
NS_IMETHODIMP sashGlade::YesNoDialog(const char *message, PRBool *result)
{  
  GtkWidget * dialog = gnome_message_box_new(_(message),GNOME_MESSAGE_BOX_INFO,
                                               _("Yes"),_("No"),NULL);
  *result = (gnome_dialog_run_and_close(GNOME_DIALOG(dialog)) == 0);
  return NS_OK;
}
/* void FileDialog (in string filename, out string ret_file); */
NS_IMETHODIMP sashGlade::FileDialog(const char *filename, char **ret_file)
{
  int result;
  char* file;
  GtkWidget * dialog = gnome_dialog_new(_(filename),("OK"),_("Cancel"),NULL);
  GtkWidget * gnomeEntry = gnome_file_entry_new(filename,"Browse...");

  gtk_widget_show(gnomeEntry);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),gnomeEntry,true,true,0);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog), true);
  result = (gnome_dialog_run(GNOME_DIALOG(dialog)) == 0);
  DEBUGMSG(glade, "result of gnome file dialog is %d\n", result);
  if (result) {
    file = gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(gnomeEntry),FALSE);
	if (file) {
		 *ret_file = XP_STRDUP(file);
		 g_free(file);
	} else {
		 *ret_file = XP_STRDUP("");
	}
	
  } else {
    *ret_file = XP_STRDUP("");
  }
  gtk_widget_destroy(dialog);

  return NS_OK;
}

NS_IMETHODIMP
sashGlade::SignalDisconnect(sashIGtkObject *obj, 
							const char *signalName) {
	nsCOMPtr<sashIGtkExtension> gtk_ext(do_QueryInterface(m_gtk_ext));
	return gtk_ext->SignalDisconnect(obj, signalName);
}

NS_GENERIC_FACTORY_CONSTRUCTOR(sashGlade);
NS_DECL_CLASSINFO(sashGlade);

NS_GENERIC_FACTORY_CONSTRUCTOR(sashGladeFile);
NS_DECL_CLASSINFO(sashGladeFile);


static nsModuleComponentInfo components[] = {
	MODULE_COMPONENT_ENTRY(sashGlade, SASHGLADE, "Glade Extension"),
	MODULE_COMPONENT_ENTRY(sashGladeFile, SASHGLADEFILE, "Glade File object")
};

NS_IMPL_NSGETMODULE(sashGlade, components);
