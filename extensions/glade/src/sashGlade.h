
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHGLADE_H
#define SASHGLADE_H

#include "sashIGlade.h"
#include "sashIExtension.h"
#include "sashIJSEmbed.h"
#include "sashIGtkObjectFactory.h"
#include <nsCOMPtr.h>
#include <glade/glade.h>
#include <gnome.h>
#include <vector>
#include <string>

typedef struct _GtkWidget GtkWidget;
class sashIRuntime;
class sashIActionRuntime;

class sashGlade;


//DEB96E42-83E3-4406-8C09-8A2C5E92C042
#define SASHGLADE_CID {0xDEB96E42, 0x83E3, 0x4406, {0x8C, 0x09, 0x8A, 0x2C, 0x5E, 0x92, 0xC0, 0x42}}
NS_DEFINE_CID(ksashGladeCID, SASHGLADE_CID);

#define SASHGLADE_CONTRACT_ID "@gnome.org/SashWDE/sashGlade;1"

class sashGlade : public sashIGlade
{
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIGLADE;
  NS_DECL_SASHIEXTENSION;
  
  sashGlade();
  virtual ~sashGlade();

	sashIActionRuntime *getActionRuntime();

 private:
  sashIActionRuntime *m_act;
	string m_dataDir;
	sashIGenericConstructor *m_Constructor;
	
  nsCOMPtr<sashIGtkObjectFactory> m_fact;
  nsCOMPtr<sashIExtension> m_gtk_ext;	
};


#endif
