
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SASHGLADEFILE_H
#define _SASHGLADEFILE_H

#include "sashIGlade.h"
#include "nsIXPCScriptable.h"
#include <gnome.h>
#include <glade/glade.h>

//062D7C7A-630B-4456-AF9B-F7FFF3AEC2ED
#define SASHGLADEFILE_CID {0x062D7C7A, 0x630B, 0x4456, {0xAF, 0x9B, 0xF7, 0xFF, 0xF3, 0xAE, 0xC2, 0xED}}
NS_DEFINE_CID(ksashGladeFileCID, SASHGLADEFILE_CID);

#define SASHGLADEFILE_CONTRACT_ID "@gnome.org/SashWDE/sashGladeFile;1"

/* Header file */
class sashGladeFile : public sashIGladeFile
{
public:
    NS_DECL_ISUPPORTS
	NS_DECL_SASHIGLADEFILE
	NS_DECL_SASHICONSTRUCTOR

  sashGladeFile();
  virtual ~sashGladeFile();

  void Connect(GtkObject *obj, const char *signalName, 
			   const char * signalData, const char *handlerName);
	
private:
	GladeXML *m_xml;
  nsCOMPtr<sashIGtkObjectFactory> m_fact;
  nsCOMPtr<sashIExtension> m_gtk_ext;		
};

#endif
