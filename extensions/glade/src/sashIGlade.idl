
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

GladeApp abstract interface

*****************************************************************/

#include "nsISupports.idl"
#include "nsISupportsPrimitives.idl"
#include "nsIPref.idl"
#include "sashIExtension.idl"
#include "sashIConstructor.idl"

%{C++
#include "sashIConstructor.h"
#include "sashIGenericConstructor.h"
#include "sashIGtkObjectFactory.h"
%}

interface sashIGtkWidget;
interface sashIGenericConstructor;
interface sashIGtkObject;

[scriptable, uuid(DEC96EC1-E460-47AD-A3A3-299EF4C0C388)]
interface sashIGladeFile : sashIConstructor
{
  sashIGtkWidget getWidget(in string name);
};

[ptr] native gtkobj(sashIGtkObjectFactory);
[ptr] native gtkext(sashIExtension);

[scriptable, uuid(1D562EA5-173C-479E-88F4-F9EF9AC8ECCF)]
interface sashIGlade : sashIExtension
{
	readonly attribute sashIGenericConstructor GladeFile;
	[noscript] readonly attribute gtkobj GtkFactory;
	[noscript] readonly attribute gtkext GtkExtension;
	void signalDisconnect(in sashIGtkObject o, in string signal);

	void MessageDialog(in string message);
	boolean YesNoDialog(in string message);
	string FileDialog(in string filename);
};
