
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142

    USA

*****************************************************************

Contributor(s): John Corwin

module for the SashFileSystem
******************************************************************/

#include "sashFile.h"
#include "sashFileSystem.h"
#include "sashFolder.h"
#include "sashTextStream.h"
#include "extensiontools.h"

NS_DECL_CLASSINFO(sashFileSystem);
NS_DECL_CLASSINFO(sashFile);
NS_DECL_CLASSINFO(sashTextStream);
NS_DECL_CLASSINFO(sashFolder);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashFileSystem);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashFile);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashTextStream);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashFolder);

static nsModuleComponentInfo components[] = {
	 MODULE_COMPONENT_ENTRY(sashFileSystem, SASHFILESYSTEM, "FileSystem extension"),
	 MODULE_COMPONENT_ENTRY(sashFile, SASHFILE, "sashFile"),
	 MODULE_COMPONENT_ENTRY(sashTextStream, SASHTEXTSTREAM, "sashTextStream"),
	 MODULE_COMPONENT_ENTRY(sashFolder, SASHFOLDER, "sashFolder")
};

NS_IMPL_NSGETMODULE(sashFileSystem, components)
