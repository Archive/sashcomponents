
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#include "sashFolder.h"
#include "extensiontools.h"
#include "nsCRT.h"
#include "sashIExtension.h"
#include "sashFileCommon.h"
#include "sashVariantUtils.h"

#include <unistd.h>

NS_IMPL_ISUPPORTS2_CI(sashFolder, sashIFolder, sashIConstructor);

sashFolder::sashFolder()
{
	DEBUGMSG(folder, "Making folder\n");
	 NS_INIT_ISUPPORTS();
}

sashFolder::~sashFolder()
{
}

NS_IMETHODIMP
sashFolder::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
								PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	DEBUGMSG(folder, "initializing\n");
	 if (argc < 1 || argc > 2) {
		  printf("ERROR: sashFolder constructor requires one or two arguments\n");
		  *ret = false;
		  return NS_OK;
	 }

	 if (!ext)
	 {
		  cout << "ERROR: sashFolder: extension pointer is NULL" << endl;
		  *ret = false;
		  return NS_ERROR_NULL_POINTER;
	 }

	 nsCOMPtr<sashIFileSystem> fsExt = do_QueryInterface(ext);
	 if (!fsExt)
	 {
		  cout << "ERROR: Folder objects must be created by the filesystem" << endl;
		  *ret = false;
		  return NS_ERROR_FAILURE;
	 }

	 string name = VariantGetString(argv[0], "/");
	 if (name[name.length()-1] == '/') name.resize(name.length()-1);
	 sashFileCommon::InitFileCommon(actionRuntime, cx, fsExt, name.c_str());

	 bool create = (argc == 2 ? VariantGetBoolean(argv[1], false) : false);

	 if (create) {
		  DEBUGMSG(folder, "Creating folder\n");
		  
		  PRBool success;
		  m_fs->CreateFolder(m_path.c_str(), &success);
	 }

	 *ret = true;
	 return NS_OK;
}

/* void Copy (in string dest, in nsIVariant overwrite); */
NS_IMETHODIMP sashFolder::Copy(const char *dest, nsIVariant *overwrite)
{
	 
	 PRBool ow = VariantGetBoolean(overwrite);
	 PRBool success;
	 if (ow)
		  m_fs->CopyFolder(m_path.c_str(), dest, &success);
	 
	 return NS_OK;
}

/* void Delete (in nsIVariant force); */
NS_IMETHODIMP sashFolder::Delete(nsIVariant *force)
{
	 PRBool frc = VariantGetBoolean(force);
	 PRBool success;
	 if (frc)
		  m_fs->DeleteFolder(m_path.c_str(), &success);
	 return NS_OK;
}

/* void Move (in string dest); */
NS_IMETHODIMP sashFolder::Move(const char *dest)
{
	 PRBool success;
	 m_fs->MoveFolder(m_path.c_str(), dest, &success);
	 return NS_OK;
}

/* readonly attribute boolean IsFile; */
NS_IMETHODIMP sashFolder::GetIsFile(PRBool *aIsFile)
{
	 *aIsFile = false;
	 return NS_OK;
}

/* readonly attribute boolean IsFolder; */
NS_IMETHODIMP sashFolder::GetIsFolder(PRBool *aIsFolder)
{
	 *aIsFolder = true;
    return NS_OK;
}

/* readonly attribute PRInt32 Size; */
NS_IMETHODIMP sashFolder::GetSize(PRInt32 *aSize)
{
	 m_fs->FolderSize(m_path.c_str(), aSize);
	 return NS_OK;
}

/* readonly attribute string Type; */
NS_IMETHODIMP sashFolder::GetType(char * *aType)
{
	 XP_COPY_STRING("folder", aType);
	 return NS_OK;
}

void sashFolder::ReturnSubFiles(const vector<string> & files, nsCID cid, nsIVariant ** aFiles) {
	 sashIConstructor * file;
	 nsIVariant * pathVariant;
	 PRBool ret;

	 NewVariant(aFiles);

	 vector<nsISupports *> fileObjs;

	 NewVariant(&pathVariant);
	 DEBUGMSG(filesystem, "Folder has %d files\n", files.size());
	 for (unsigned int i = 0; i < files.size(); i++)
	 {
		  DEBUGMSG(filesystem, "creating file #%d [%s]\n", i, files[i].c_str());
		  nsComponentManager::CreateInstance(cid, nsnull, 
											 (nsIID)SASHICONSTRUCTOR_IID, (void **) &file);
		  
		  VariantSetString(pathVariant, (m_path + "/" + files[i]).c_str());
		  nsCOMPtr<sashIExtension> ext = do_QueryInterface(m_fsExt);
		  file->InitializeNewObject(m_act, ext, m_cx, 1, &pathVariant, &ret);
		  fileObjs.push_back((nsISupports*) file);
	 }
	 
	 DEBUGMSG(filesystem, "setting variant array\n");
	 VariantSetArray(*aFiles, fileObjs);
	 
	 NS_RELEASE(pathVariant);
}

/* readonly attribute boolean IsRootFolder; */
NS_IMETHODIMP sashFolder::GetIsRootFolder(PRBool *aIsRootFolder)
{
	 *aIsRootFolder = (m_path == "/");
    return NS_OK;
}

/* readonly attribute sashIFileCollection Files; */
NS_IMETHODIMP sashFolder::GetFiles(nsIVariant * *aFiles)
{
  if (!aFiles)
	return NS_ERROR_NULL_POINTER;

  nsCOMPtr<nsIVariant> temp;
  m_fs->EnumFiles(m_path.c_str(), getter_AddRefs(temp));
  
  vector<string> vec;
  VariantGetArray(temp, vec);
  ReturnSubFiles(vec, ksashFileCID, aFiles);
  
  return NS_OK;
}

/* readonly attribute sashIFileCollection SubFolders; */
NS_IMETHODIMP sashFolder::GetSubFolders(nsIVariant * *aSubFolders)
{
  if (!aSubFolders)
	return NS_ERROR_NULL_POINTER;

  nsCOMPtr<nsIVariant> temp;
  m_fs->EnumDirs(m_path.c_str(), getter_AddRefs(temp));
  vector<string> vec;
  VariantGetArray(temp, vec);

  ReturnSubFiles(vec, ksashFolderCID, aSubFolders);

  return NS_OK;
}

