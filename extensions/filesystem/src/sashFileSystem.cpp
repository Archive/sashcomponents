/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Andrew Chatham

Implementation of the filesystem extension

*****************************************************************/

#include "sash_constants.h"
#include "sashFileSystem.h"
#include "sashFile.h"
#include "sashFolder.h"
#include "sashTextStream.h"
#include "sashIGenericConstructor.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "sashIXPFileSystem.h"
#include "debugmsg.h"
#include "sashFile.h"
#include "nsMemory.h"
#include "extensiontools.h"
#include <unistd.h>
#include "secman.h"
#include "sash_error.h"

SASH_EXTENSION_IMPL_NO_NSGET(sashFileSystem, sashIFileSystem, 
		    "FileSystem");

sashFileSystem::sashFileSystem() 
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(filesystem, "Creating filesystem extension\n");
}

sashFileSystem::~sashFileSystem()
{
  NS_RELEASE(m_fileConstructor);
  NS_RELEASE(m_textStreamConstructor);
  NS_RELEASE(m_folderConstructor);
}

NS_IMETHODIMP 
sashFileSystem::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP 
sashFileSystem::ProcessEvent() {
  return NS_OK;
}

NS_IMETHODIMP
sashFileSystem::GetFile(sashIGenericConstructor **ret)
{
  NS_ADDREF(m_fileConstructor);
	 *ret = m_fileConstructor;
	 return NS_OK;
}

NS_IMETHODIMP
sashFileSystem::GetTextStream(sashIGenericConstructor **ret)
{
  NS_ADDREF(m_textStreamConstructor);
  *ret = m_textStreamConstructor;
  return NS_OK;
}

NS_IMETHODIMP
sashFileSystem::GetFolder(sashIGenericConstructor **ret)
{
  NS_ADDREF(m_folderConstructor);
	 *ret = m_folderConstructor;
	 return NS_OK;
}

/* string BuildPath (in string Path, in string Name); */
NS_IMETHODIMP sashFileSystem::BuildPath(const char *Path, const char *Name, char **_retval)
{
	 if (!_retval)
		  return NS_ERROR_NULL_POINTER;
	 string s;
	 if (Path) {
		  s = Path;
		  if (s[s.length()-1] != '/') 
			   s += '/';
	 }
	 if (Name)
		  s += Name;

	 XP_COPY_STRING(s.c_str(), _retval);
	 return NS_OK;
}

/* void CopyFile (in string source, in string dest, in nsIVariant overwrite); */
NS_IMETHODIMP sashFileSystem::CopyFile(const char *source, const char *dest, nsIVariant *overwrite) {
	 PRBool val;
	 return (m_pFS->CopyFile(source, dest, VariantGetBoolean(overwrite),
							 &val));
}

/* void CopyFolder (in string source, in string dest, in nsIVariant overwrite); */
NS_IMETHODIMP sashFileSystem::CopyFolder(const char *source, const char *dest, nsIVariant *overwrite)
{
	 PRBool exists;
	 nsresult res = m_pFS->FolderExists(dest, &exists);
	 if (! NS_SUCCEEDED(res)) return res;
	 
	 if ((! exists) || VariantGetBoolean(overwrite)){
		  PRBool ret;
		  m_pFS->CopyFolder(source, dest, &ret);
	 }
	 return NS_OK;
}

/* void DeleteFile (in string path, in nsIVariant force); */
NS_IMETHODIMP sashFileSystem::DeleteFile(const char *path, nsIVariant *force)
{
	 PRBool ret;
	 if (VariantGetBoolean(force))
		  return (m_pFS->DeleteFile(path, &ret));
	 
	 return NS_OK;
}

/* void DeleteFolder (in string path, in nsIVariant force); */
NS_IMETHODIMP sashFileSystem::DeleteFolder(const char *path, nsIVariant *force)
{
	 PRBool ret;
	 if (VariantGetBoolean(force))
		  return (m_pFS->DeleteFolder(path, &ret));
	 
	 return NS_OK;
}

/* PRBool FileExists (in string path); */
NS_IMETHODIMP sashFileSystem::FileExists(const char *path, PRBool *_retval)
{
	 return (m_pFS->FileExists(path, _retval));
}

/* PRBool FolderExists (in string path); */
NS_IMETHODIMP sashFileSystem::FolderExists(const char *path, PRBool *_retval)
{
	 return (m_pFS->FolderExists(path, _retval));
}

/* string GetAbsolutePath (in string relPath); */
NS_IMETHODIMP sashFileSystem::GetAbsolutePathName(const char *relPath, char **_retval)
{
	 if (relPath[0] == '/') {
		  XP_COPY_STRING(relPath, _retval);
		  return NS_OK;
	 }
	 else {
		  char dir[PATH_MAX];
		  string absPath;
		
		  getcwd(dir, PATH_MAX);
		  absPath = string(dir) + "/" + string(relPath);
		  XP_COPY_STRING(absPath.c_str(), _retval);
		  return NS_OK;
  	 }
}

/* string GetBaseName (in string path); */
NS_IMETHODIMP sashFileSystem::GetBaseName(const char *path, char **_retval)
{
	 string pathStr = path, baseName;
	 int start, end;
	 
	 start = pathStr.rfind('/') + 1; 
	 end = pathStr.rfind('.');
	 if (end < start)
		  end = pathStr.size();
	 baseName = string(pathStr, start, end - start);

	 XP_COPY_STRING(baseName.c_str(), _retval);
	 return NS_OK;
}

/* string GetExtensionName (in string path); */
NS_IMETHODIMP sashFileSystem::GetExtensionName(const char *path, char **_retval)
{
	 string pathStr = path;
	 int dot = pathStr.rfind('.') + 1;
	 string ext;
	 if (dot == 0)
		  ext = "";
	 else
		  ext = string(pathStr, dot, pathStr.size() - dot);

	 XP_COPY_STRING(ext.c_str(), _retval);
	 return NS_OK;
}

/* string GetFileName (in string path); */
NS_IMETHODIMP sashFileSystem::GetFileName(const char *path, char **_retval)
{
	 string pathStr = path, fileName;
	 int start;

	 start = pathStr.rfind('/') + 1; 
	 fileName = string(pathStr, start, pathStr.size() - start);

	 XP_COPY_STRING(fileName.c_str(), _retval);
	 return NS_OK;
}

/* string GetParentFolderName (in string path); */
NS_IMETHODIMP sashFileSystem::GetParentFolderName(const char *path, char **_retval)
{
	 string pathStr = path;
	 if ((pathStr == "/") || (pathStr == "")) {
		  XP_COPY_STRING(pathStr.c_str(), _retval);
		  return NS_OK;
	 }

	 if (pathStr[pathStr.length()-1] == '/') pathStr.resize(pathStr.length()-1);
	 int end = pathStr.rfind('/') + 1;
	 string parentFolder = string(path, 0, end);
	 
	 XP_COPY_STRING(parentFolder.c_str(), _retval);
	 return NS_OK;
}

/* string GetTempName (); */
NS_IMETHODIMP sashFileSystem::GetTempName(char **_retval)
{
	 return (m_pFS->GetTempName(_retval));
}

/* void MoveFile (in string source, in string dest); */
NS_IMETHODIMP sashFileSystem::MoveFile(const char *source, const char *dest)
{
	 PRBool ret;
	 return (m_pFS->MoveFile(source, dest, &ret));
}

/* void MoveFolder (in string source, in string dest); */
NS_IMETHODIMP sashFileSystem::MoveFolder(const char *source, const char *dest)
{
	 PRBool ret;
	 return (m_pFS->MoveFolder(source, dest, &ret));
}

/* void MoveFolder (in string source, in string dest); */
NS_IMETHODIMP sashFileSystem::IsSymlink(const char *path, PRBool *_retval)
{
	 return (m_pFS->IsSymlink(path, _retval));
}

/* void initialize (in sashIRuntime rt, in string registrationXml, in string webGuid); */
NS_IMETHODIMP
sashFileSystem::Initialize(sashIActionRuntime *act, 
			   const char *registrationXml, 
			   const char *webGuid,
			   JSContext *cx, JSObject *obj)
{
	 XP_GET_STRING(act->GetDataDirectory, m_weblicationDir);
	 act->GetSecurityManager(&m_secMan);
	 m_pActionRuntime = act;

	 NewSashConstructor(act, this, SASHFILE_CONTRACT_ID, SASHIFILE_IID_STR, &m_fileConstructor);
	 NewSashConstructor(act, this, SASHTEXTSTREAM_CONTRACT_ID, SASHITEXTSTREAM_IID_STR, &m_textStreamConstructor);
	 NewSashConstructor(act, this, SASHFOLDER_CONTRACT_ID, SASHIFOLDER_IID_STR, &m_folderConstructor);

	 m_pFS = do_GetService("@gnome.org/SashXB/XPFileSystem;1");
	 if (m_pFS == NULL) OutputError("Could not get filesystem service!");

	 return NS_OK;
}

NS_IMETHODIMP sashFileSystem::GetWeblicationDirectory(char ** dir)
{
	 XP_COPY_STRING(m_weblicationDir.c_str(), dir);
	 return NS_OK;
}

NS_IMETHODIMP sashFileSystem::AssertFSAccess(const char * file)
{
	 m_pActionRuntime->AssertFSAccess(file);
	 return NS_OK;
}
