/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Andrew Chatham

Header file for the filesystem extension

*****************************************************************/

#ifndef SASHFILESYSTEM_H
#define SASHFILESYSTEM_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIFileSystem.h"
#include "sashIXPFileSystem.h"
#include "sashIExtension.h"
#include "sashISecurityManager.h"

#include <string>

class FileSystem;
class sashIGenericConstructor;
// {7B9943C4-54C0-4F96-A454-71120854ECAE}
#define SASHFILESYSTEM_CID \
{0x7B9943C4, 0x54C0, 0x4F96, {0xA4, 0x54, 0x71, 0x12, 0x08, 0x54, 0xEC, 0xAE}}
    
NS_DEFINE_CID(ksashFileSystemCID, SASHFILESYSTEM_CID);

#define SASHFILESYSTEM_CONTRACT_ID "@gnome.org/SashXB/filesystem;1"

class sashFileSystem : public sashIFileSystem,
		       public sashIExtension
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHIFILESYSTEM
  
  sashFileSystem();
  virtual ~sashFileSystem();

 private:

  string m_weblicationDir;
  sashIGenericConstructor *m_fileConstructor, *m_textStreamConstructor, *m_folderConstructor;
  sashISecurityManager * m_secMan;
  nsCOMPtr<sashIXPFileSystem> m_pFS;
  sashIActionRuntime* m_pActionRuntime;
};

#endif
