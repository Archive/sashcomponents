
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef SASHTEXTSTREAM_H
#define SASHTEXTSTREAM_H

#include "nsID.h"
#include "sashITextStream.h"
#include "sashIFileSystem.h"
#include "sashIConstructor.h"
#include "sashIXPFileSystem.h"
#include <string>
//3993d34f-8942-40a6-8e5a-d882237dfac2
#define SASHTEXTSTREAM_CID {0X3993D34F, 0X8942, 0X40A6, {0X8E, 0X5A, 0XD8, 0X82, 0X23, 0X7D, 0XFA, 0XC2}}

NS_DEFINE_CID(ksashTextStreamCID, SASHTEXTSTREAM_CID);
#define SASHTEXTSTREAM_CONTRACT_ID "@gnome.org/SashXB/filesystem/textstream;1"

class sashTextStream : public sashITextStream, 
		 public sashIConstructor
{
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHICONSTRUCTOR;
  NS_DECL_SASHITEXTSTREAM;

  sashTextStream();
  virtual ~sashTextStream();

 private:
  FILE * m_file;

  FILE * OpenFile(const string& name, int mode, bool create);

  nsCOMPtr<sashIFileSystem> m_fsExt;
  nsCOMPtr<sashIXPFileSystem> m_fs;
};

#endif //SASHTEXTSTREAM_H
