
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef SASHFOLDER_H
#define SASHFOLDER_H

#include "nsID.h"
#include "nsIVariant.h"
#include "sashFile.h"
#include "sashIFolder.h"
#include "sashIConstructor.h"
#include "sashIFileSystem.h"
#include "sashIXPFileSystem.h"
#include "sashFileCommon.h"
#include <vector>
#include <string>

#define SASHFOLDER_CID {0x95f7f2a2, 0x37e9, 0x467a, {0x93, 0xf7, 0xa4, 0x74, 0xd2, 0x84, 0x84, 0xd0}}
NS_DEFINE_CID(ksashFolderCID, SASHFOLDER_CID);

#define SASHFOLDER_CONTRACT_ID "@gnome.org/SashXB/filesystem/folder;1"

class sashFolder : public sashIFolder,
				   public sashIConstructor,
				   public sashFileCommon
{
public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHICONSTRUCTOR;
  NS_DECL_SASHIFILE;
  NS_DECL_SASHIFOLDER;

  NS_FORWARD_SASHIFILECOMMON(sashFileCommon::);

  sashFolder();
  virtual ~sashFolder();

private:
  void ReturnSubFiles(const vector<string> & files, nsCID cid, nsIVariant ** aFiles);
};

#endif //SASHFOLDER_H
