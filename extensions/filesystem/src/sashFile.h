
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef SASHFILE_H
#define SASHFILE_H

#include "nsID.h"
#include "sashIFile.h"
#include "sashIConstructor.h"
#include "sashIFileSystem.h"
#include "sashIXPFileSystem.h"
#include "sashFileCommon.h"
#include <string>

#define SASHFILE_CID {0x5F7997C1, 0x1C65, 0x4894, {0x9B, 0xB2, 0x10, 0xA7, 0x81, 0x28, 0xF2, 0xB0}}

NS_DEFINE_CID(ksashFileCID, SASHFILE_CID);

#define SASHFILE_CONTRACT_ID "@gnome.org/SashXB/filesystem/file;1"

class sashFile : public sashFileCommon,
				 public sashIFile, 
				 public sashIConstructor
{
public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHICONSTRUCTOR;
  NS_DECL_SASHIFILE;

  NS_FORWARD_SASHIFILECOMMON(sashFileCommon::)

  sashFile();
  virtual ~sashFile();
};

#endif //SASHFILE_H
