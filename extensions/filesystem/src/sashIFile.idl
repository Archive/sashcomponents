
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham, John Corwin

interface file for the File object
******************************************************************/

#include "nsISupports.idl"
#include "nsIVariant.idl"
#include "sashIFileSystem.idl"
#include "sashIActionRuntime.idl"
#include "sashIExtension.idl"

interface sashIFolder;

[scriptable, uuid(6d1722ed-e42a-4b23-b01b-631f2b8e0f4c)]
interface sashIFileCommon : nsISupports
{
// ----------- Properties common to files and folders --------------
	 readonly attribute string DateCreated;
	 readonly attribute string DateLastAccessed;
	 readonly attribute string DateLastModified;
	 readonly attribute string Drive;
	 attribute string Name;
	 readonly attribute sashIFolder ParentFolder;
	 readonly attribute string Path;
	 readonly attribute string ShortName;
	 readonly attribute string ShortPath;
	 readonly attribute boolean IsLocal;
};

[scriptable, uuid(CA1F9926-933E-4764-B062-84EACDFAF524)]
interface sashIFile : sashIFileCommon
{
// ----------- Sash File methods -----------------
	 void Copy(in string dest, in nsIVariant overwrite);
	 void Delete(in nsIVariant force);
	 void Move(in string dest);

// ----------- Sash File properties --------------
	 // readonly attribute sashIFileAttributes Attributes
	 readonly attribute string Type;
	 readonly attribute boolean IsFile;
	 readonly attribute boolean IsFolder;
	 readonly attribute long Size;
};

