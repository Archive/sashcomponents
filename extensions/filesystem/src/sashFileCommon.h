/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef SASHFILECOMMON_H
#define SASHFILECOMMON_H

#include "sashIFile.h"
#include "sashIFolder.h"
#include "sashFileSystem.h"
#include <string>

class sashFileCommon
{
public:
	 NS_DECL_SASHIFILECOMMON

	 virtual ~sashFileCommon() {}
	 void InitFileCommon(sashIActionRuntime * act, JSContext * cx,
						 sashIFileSystem * fs, const char * filename);

protected:
	 nsCOMPtr<sashIFileSystem> m_fsExt;
	 nsCOMPtr<sashIXPFileSystem> m_fs;
	 string m_path;
	 string m_name;
	 JSContext * m_cx;
	 nsCOMPtr<sashIActionRuntime> m_act;
};

#endif //SASHFILECOMMON_H
