
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#include "sashFileSystem.h"
#include "sashFile.h"
#include "sashTextStream.h"
#include "extensiontools.h"
#include "sashIExtension.h"
#include "nsCRT.h"
#include "sashFileCommon.h"
#include "sashVariantUtils.h"

#include <stdio.h>
#include <unistd.h>

NS_IMPL_ISUPPORTS2_CI(sashTextStream, sashITextStream, sashIConstructor);

const int MaxLineSize = 8192;

inline int FileSize(FILE * f)
{
	 int cur, end;

	 cur = ftell(f);
	 fseek(f, 0, SEEK_END);
	 end = ftell(f);
	 fseek(f, cur, SEEK_SET);
	 
	 return end;
}

inline int FileRemaining(FILE * f)
{
	 int cur, end;

	 cur = ftell(f);
	 fseek(f, 0, SEEK_END);
	 end = ftell(f);
	 fseek(f, cur, SEEK_SET);
	 
	 return end - cur;
}

sashTextStream::sashTextStream()
{
	 NS_INIT_ISUPPORTS();
	 m_file = NULL;
}

sashTextStream::~sashTextStream()
{
	 if (m_file)
	 {
		  fclose(m_file);
		  m_file = NULL;
	 }
}

FILE * sashTextStream::OpenFile(const string& name, int mode, bool create)
{
	 char* modeStr = "r";

	 PRBool exists;
	 m_fs->FileExists(name.c_str(), &exists);

	 if (create && !exists) 
		  modeStr = "w+";
	 else if (create)
		  modeStr = "r+";
	 else if (mode == sashFileSystem::MODE_APPEND){
		  modeStr = "r+";
//		  modeStr = "a+";
	 }

	 FILE *temp = fopen(name.c_str(), modeStr);
	 
	 if (mode == sashFileSystem::MODE_APPEND)
		  if (temp)
			   fseek(temp, 0, SEEK_END);

	 return temp;
}

NS_IMETHODIMP
sashTextStream::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
									PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	 DEBUGMSG(filesystem, "sashTextStream::InitializeNewObject\n");
	 string fileName;
	 int fileMode = sashFileSystem::MODE_READ;
	 bool fileCreate = true;

	 m_fs = do_GetService("@gnome.org/SashXB/XPFileSystem;1");
	 if (m_fs == NULL) 
		  OutputError("Could not get filesystem service!");

	 m_fsExt = do_QueryInterface(ext);
	 if (!m_fsExt) {
		  OutputError("ERROR: File objects must be created by the filesystem");;
		  *ret = false;
		  return NS_ERROR_FAILURE;
	 }

	 if (argc < 1) {
		  printf("ERROR: sashTextStream constructor requires at least one argument\n");
		  return NS_ERROR_UNEXPECTED;
	 }

	 if (VariantIsString(argv[0])) 	 {
		  fileName = VariantGetString(argv[0], "");
		  DEBUGMSG(filesystem, "filename: %s\n", fileName.c_str());
	 } else if (VariantIsInterface(argv[0])) {
		  nsCOMPtr<nsISupports> unknIf = VariantGetInterface(argv[0]);
		  if (!unknIf) {
			   printf("ERROR: TextStream constructor: could not get nsISupports interface from first parameter\n");
			   return NS_ERROR_UNEXPECTED;
		  }
		  nsCOMPtr<sashIFile> fileIf = do_QueryInterface(unknIf);
		  if (!fileIf) {
			   printf("ERROR: TextStream constructor: first argument to constructor is not a sashIFile\n");
			   return NS_ERROR_UNEXPECTED;
		  }
		  XP_GET_STRING(fileIf->GetName, fileName);
	 }
	 else {
		  printf("ERROR: TextStream constructor: first argument is of unknown type\n");
		  return NS_ERROR_UNEXPECTED;
	 }

	 if (argc >= 2)
	 {
		  // Get the requested file mode
		  fileMode = (int) (VariantGetNumber(argv[1], sashFileSystem::MODE_READ) + 0.5);
		  if (argc >= 3)
		  {
			   // Get the file creation flag
			   fileCreate = VariantGetBoolean(argv[2]);
		  }
	 }
	 
	 DEBUGMSG(filesystem, "Testing access for %s\n", fileName.c_str());
	 // Check the access rights for this file
	 m_fsExt->AssertFSAccess(fileName.c_str());

	 // open the actual file
	 DEBUGMSG(filesystem, "trying to open file\n");
	 m_file = OpenFile(fileName, fileMode, fileCreate);
	 if (!m_file) {
		  DEBUGMSG(filesystem, "ERROR: sashTextStream was unable to open file %s with mode %d\n", 
				 fileName.c_str(), fileMode);
		  return NS_ERROR_ILLEGAL_VALUE;
	 }
	 
	 *ret = true;
	 return NS_OK;
}

/* void Close (); */
NS_IMETHODIMP sashTextStream::Close()
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 fclose(m_file);
	 m_file = NULL;
	 return NS_OK;
}

/* string Read (in PRInt32 count); */
NS_IMETHODIMP sashTextStream::Read(PRInt32 count, char **_retval)
{
	 char * buf;
	 int size; 

	 if (!m_file || count < 0)
		  return NS_ERROR_UNEXPECTED;

	 // magic memory allocation
	 buf = (char *) alloca(count + 1);

	 size = fread(buf, 1, count, m_file);
	 buf[size] = '\0'; // null terminate the input
	 
	 XP_COPY_STRING(buf, _retval);
	 return NS_OK;
}

/* string ReadAll (); */
NS_IMETHODIMP sashTextStream::ReadAll(char **_retval)
{
	 int size;
	 char * buf;

	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;
	 
	 size = FileRemaining(m_file);

	 buf = (char *) alloca(size + 1);
	 fread(buf, 1, size, m_file);
	 buf[size] = '\0';

	 XP_COPY_STRING(buf, _retval);
	 return NS_OK;
}

/* string ReadLine (); */
NS_IMETHODIMP sashTextStream::ReadLine(char **_retval)
{
	 char buf[MaxLineSize];

	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 fgets(buf, MaxLineSize, m_file);
	 XP_COPY_STRING(buf, _retval);
	 return NS_OK;
}

/* void Skip (in PRInt32 count); */
NS_IMETHODIMP sashTextStream::Skip(PRInt32 count)
{
	 int remain;
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;
	 
	 remain = FileRemaining(m_file);
	 if (count > remain)
		  fseek(m_file, 0, SEEK_END);
	 else
		  fseek(m_file, count, SEEK_CUR);

    return NS_OK;
}

/* void SkipLine (); */
NS_IMETHODIMP sashTextStream::SkipLine()
{
	 char * line;

	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 ReadLine(&line);
	 nsMemory::Free(line);

	 return NS_OK;
}

/* void Write (in string text); */
NS_IMETHODIMP sashTextStream::Write(const char *text)
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;
	 if (!text)
		  return NS_ERROR_NULL_POINTER;

	 fputs(text, m_file);

	 return NS_OK;
}

/* void WriteBlankLines (in PRInt32 count); */
NS_IMETHODIMP sashTextStream::WriteBlankLines(PRInt32 count)
{
	 if (!m_file || count < 0)
		  return NS_ERROR_UNEXPECTED;

	 for (int i = 0; i < count; i++)
		  fputs("\n", m_file);

	 return NS_OK;
}

/* void WriteLine (in string text); */
NS_IMETHODIMP sashTextStream::WriteLine(const char *text)
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;
	 if (!text)
		  return NS_ERROR_NULL_POINTER;

	 fputs(text, m_file);
	 fputs("\n", m_file);

	 return NS_OK;
}

/* readonly attribute boolean AtEndOfLine; */
NS_IMETHODIMP sashTextStream::GetAtEndOfLine(PRBool *aAtEndOfLine)
{	 
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 char c = getc(m_file);
	 *aAtEndOfLine = (c == '\n');
	 ungetc(c, m_file);
	 
	 return NS_OK;
}

/* readonly attribute boolean AtEndOfStream; */
NS_IMETHODIMP sashTextStream::GetAtEndOfStream(PRBool *aAtEndOfStream)
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 *aAtEndOfStream = (FileRemaining(m_file) == 0);
	 return NS_OK;
}

/* readonly attribute PRInt32 CharSize; */
NS_IMETHODIMP sashTextStream::GetCharSize(PRInt32 *aCharSize)
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;
	 
	 *aCharSize = FileSize(m_file);

	 return NS_OK;
}

/* attribute PRInt32 Position; */
NS_IMETHODIMP sashTextStream::GetPosition(PRInt32 *aPosition)
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 *aPosition = ftell(m_file);
	 
	 return NS_OK;
}
NS_IMETHODIMP sashTextStream::SetPosition(PRInt32 aPosition)
{
	 if (!m_file)
		  return NS_ERROR_UNEXPECTED;

	 fseek(m_file, aPosition, SEEK_SET);

	 return NS_OK;
}
