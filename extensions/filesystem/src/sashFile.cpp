
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#include "sashFile.h"
#include "extensiontools.h"
#include "nsCRT.h"
#include "sashIExtension.h"
#include "sashFileCommon.h"
#include "sashVariantUtils.h"
#include "sash_error.h"
#include <unistd.h>

NS_IMPL_ISUPPORTS2_CI(sashFile, sashIFile, sashIConstructor);

sashFile::sashFile()
{
	 NS_INIT_ISUPPORTS();
}

sashFile::~sashFile()
{
}


NS_IMETHODIMP
sashFile::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
							  PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	 DEBUGMSG(sashfs, "sashfile::initializenewobject\n");
	 if (argc != 1) {
		  OutputError("ERROR: sashFile constructor requires one argument\n");
		  *ret = false;
		  return NS_OK;
	 }
	 nsCOMPtr<sashIFileSystem> fsExt = do_QueryInterface(ext);
	 if (!fsExt)
	 {
		  OutputError("ERROR: File objects must be created by the filesystem");
		  *ret = false;
		  return NS_ERROR_FAILURE;
	 }

	 string name = VariantGetString(argv[0], "");
	 if (name[name.length()-1] == '/') name.resize(name.length()-1);
	 sashFileCommon::InitFileCommon(actionRuntime, cx, fsExt, name.c_str());

	 *ret = true;
	 DEBUGMSG(sashfs, "sashfile::initializenewobject end\n");
	 return NS_OK;
}

/* void Copy (in string dest, in nsIVariant overwrite); */
NS_IMETHODIMP sashFile::Copy(const char *dest, nsIVariant *overwrite)
{
	 if (!dest)
	 {
		  cout << "Error: sashFile::Copy: input string is NULL" << endl;
		  return NS_ERROR_NULL_POINTER;
	 }
	 cout << "Copying file " << m_path << " to " << dest << endl;
	 
	 PRBool success;
	 m_fs->CopyFile(m_path.c_str(), dest, 
					 VariantGetBoolean(overwrite), &success);
	 return NS_OK;
}

/* void Delete (in nsIVariant force); */
NS_IMETHODIMP sashFile::Delete(nsIVariant *force)
{
	 PRBool success;
	 m_fs->DeleteFile(m_path.c_str(), &success);

	 return NS_OK;
}

/* void Move (in string dest); */
NS_IMETHODIMP sashFile::Move(const char *dest)
{
	 PRBool success;
	 m_fs->MoveFile(m_path.c_str(), dest, &success);
	 return NS_OK;
}

/* readonly attribute boolean IsFile; */
NS_IMETHODIMP sashFile::GetIsFile(PRBool *aIsFile)
{
	 *aIsFile = true;
	 return NS_OK;
}

/* readonly attribute boolean IsFolder; */
NS_IMETHODIMP sashFile::GetIsFolder(PRBool *aIsFolder)
{
	 *aIsFolder = false;
    return NS_OK;
}

/* readonly attribute PRInt32 Size; */
NS_IMETHODIMP sashFile::GetSize(PRInt32 *aSize)
{
	 m_fs->FileSize(m_path.c_str(), aSize);
	 return NS_OK;
}

/* readonly attribute string Type; */
NS_IMETHODIMP sashFile::GetType(char * *aType)
{
	 XP_COPY_STRING("file", aType);
	 return NS_OK;
}
