#include "sashFileCommon.h"
#include <nsIServiceManagerUtils.h>
#include "xpcomtools.h"
#include "sashVariantUtils.h"
#include "sashFolder.h"

//NS_IMPL_ISUPPORTS1_CI(sashFileCommon, sashIFileCommon);

void sashFileCommon::InitFileCommon(sashIActionRuntime * act, JSContext * cx, 
									sashIFileSystem * fs, const char * fileName) 
{
	 m_act = act;
	 m_cx = cx;
	 m_fsExt = fs;
	 m_fs = do_GetService("@gnome.org/SashXB/XPFileSystem;1");
	 if (m_fs == NULL) 
		  OutputError("Could not get filesystem service!");

	 string name = fileName;

	 // if not absolute, relative to the weblication directory
	 if (name[0] != '/') {
		  string webldir;
		  XP_GET_STRING(m_fsExt->GetWeblicationDirectory, webldir);
		  m_path = webldir + "/" + name;
		  m_name = name;
	 } else {
		  int pos = name.rfind('/');
		  m_name = name.substr(pos+1);
		  m_path = name;
	 }

	 // Check the access rights for this file/folder
	 m_fsExt->AssertFSAccess(m_path.c_str());
}

/* readonly attribute string DateCreated; */ 
NS_IMETHODIMP sashFileCommon::GetDateCreated(char * *aDateCreated) 
{ 
	 /* Strangely, the unix FS does not seem to keep track of the creation date */ 
	 /* return the last access date */ 
	 return GetDateLastAccessed(aDateCreated); 
} 

/* readonly attribute string DateLastAccessed; */ 
NS_IMETHODIMP sashFileCommon::GetDateLastAccessed(char * *aDateLastAccessed) 
{ 
	 PRInt32 time32; 
	 m_fs->FileLastAccessed(m_path.c_str(), &time32); 
	 time_t time = (time_t)time32; 
	 struct tm * btime = localtime(&time); 

	 XP_COPY_STRING(asctime(btime), aDateLastAccessed); 
     return NS_OK; 
} 

/* readonly attribute string DateLastModified; */ 
NS_IMETHODIMP sashFileCommon::GetDateLastModified(char * *aDateLastModified) 
{ 
	 PRInt32 time32; 
	 m_fs->FileLastModified(m_path.c_str(), &time32); 
	 time_t time = (time_t)time32; 
	 struct tm * btime = localtime(&time); 
 
	 XP_COPY_STRING(asctime(btime), aDateLastModified); 
     return NS_OK; 
} 
 
/* attribute string Name; */ 
NS_IMETHODIMP sashFileCommon::GetName(char * *aName) 
{ 
	 XP_COPY_STRING(m_name.c_str(), aName); 
     return NS_OK; 
}
 
NS_IMETHODIMP sashFileCommon::SetName(const char * aName) 
{ 
     string newname = m_path.substr(0, m_path.rfind('/')) + '/' + aName; 
	 PRBool fileexists, folderexists; 
	 PRBool success; 

	 m_fs->FileExists(newname.c_str(), &fileexists); 
	 m_fs->FolderExists(newname.c_str(), &folderexists); 
	 if (fileexists || folderexists) 
		  return NS_OK; 
	 m_fs->FileExists(m_path.c_str(), &fileexists); 
	 m_fs->FolderExists(m_path.c_str(), &folderexists); 
	 if (folderexists) { 
		  m_fs->MoveFolder(m_path.c_str(), newname.c_str(), &success); 
	 } else if (fileexists) { 
		  m_fs->MoveFile(m_path.c_str(), newname.c_str(), &success); 
	 } 
	 m_name = aName; 
	 m_path = newname; 
	 return NS_OK; 
} 
 
/* readonly attribute string ParentFolder; */ 
NS_IMETHODIMP sashFileCommon::GetParentFolder(sashIFolder ** parentFolder) 
{	  
	 nsIVariant * pathVariant; 
	 NewVariant(&pathVariant); 
	 string parent = m_path.substr(0, m_path.rfind('/')); 
     VariantSetString(pathVariant, parent); 
	 PRBool ret;

	 nsComponentManager::CreateInstance(ksashFolderCID, nsnull, 
										(nsIID)SASHIFILE_IID, (void **) parentFolder); 
	 nsCOMPtr<sashIConstructor> folder = do_QueryInterface(*parentFolder);
	 nsCOMPtr<sashIExtension> ext = do_QueryInterface(m_fsExt);
	 folder->InitializeNewObject(m_act, ext, m_cx, 1, &pathVariant, &ret); 

	 return NS_OK; 
}
 
/* readonly attribute string Path; */ 
NS_IMETHODIMP sashFileCommon::GetPath(char * *aPath) 
{ 
	 XP_COPY_STRING(m_path.c_str(), aPath); 
	 return NS_OK; 
} 
 
/* readonly attribute string ShortName; */ 
NS_IMETHODIMP sashFileCommon::GetShortName(char * *aShortName) 
{ 
	   return GetName(aShortName); 
} 
 
/* readonly attribute string ShortPath; */ 
NS_IMETHODIMP sashFileCommon::GetShortPath(char * *aShortPath) 
{ 
	 return GetPath(aShortPath); 
} 
 
NS_IMETHODIMP sashFileCommon::GetDrive(char ** drive) 
{ 
	 XP_COPY_STRING("/", drive);
	 return NS_OK; 
} 

NS_IMETHODIMP sashFileCommon::GetIsLocal(PRBool * isLocal) 
{ 
    string localDir; 
    XP_GET_STRING(m_fsExt->GetWeblicationDirectory, localDir); 
	m_fs->IsLocal(m_path.c_str(), localDir.c_str(), isLocal); 
    return NS_OK; 
} 
