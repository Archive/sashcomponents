/* USE THIS TEMPLATE FOR IMPLEMENTING THE EXTENSION */
package org.mozilla.xpcom;

public class sashGoogleExtensionImpl implements sashIGoogle, sashIExtension2 {

	private sashIActionRuntime m_act;
	private int contextID;
	private sashIGenericScriptableConstructor m_searchConstructor;
	private sashIGenericScriptableConstructor m_searchResultConstructor;
	private sashIGenericScriptableConstructor m_searchCategoryConstructor;
	private sashIGenericScriptableConstructor m_searchResultElementConstructor;

	public Object queryInterface(IID ignored) {
		return this;
	}

	public sashGoogleExtensionImpl() {
		m_searchConstructor= null;
		m_searchResultConstructor= null;
		m_searchCategoryConstructor= null;
		m_searchResultElementConstructor= null;
	}

	public sashIGenericScriptableConstructor getSearch() {
		try {
			if (m_searchConstructor== null) {
				m_searchConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_searchConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/google/Search;1",
					comGoogleSoapSearchIGoogleSearch.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_searchConstructor;
	}

	public sashIGenericScriptableConstructor getSearchResult() {
		try {
			if (m_searchResultConstructor== null) {
				m_searchResultConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_searchResultConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/google/SearchResult;1",
					comGoogleSoapSearchIGoogleSearchResult.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_searchResultConstructor;
	}

	public sashIGenericScriptableConstructor getSearchCategory() {
		try {
			if (m_searchCategoryConstructor== null) {
				m_searchCategoryConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_searchCategoryConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/google/SearchCategory;1",
					comGoogleSoapSearchIGoogleSearchDirectoryCategory.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_searchCategoryConstructor;
	}

	public sashIGenericScriptableConstructor getSearchResultElement() {
		try {
			if (m_searchResultElementConstructor== null) {
				m_searchResultElementConstructor= (sashIGenericScriptableConstructor)
					jcComponentManager.createInstance("@gnome.org/SashXB/sashGenericConstructor;1");
				m_searchResultElementConstructor.setScriptableConstructorProperties(
					m_act, (sashIExtension2) this,
					"@gnome.org/SashXB/google/SearchResultElement;1",
					comGoogleSoapSearchIGoogleSearchResultElement.IID.getIDStr()
				);
			}
		} catch (Exception ex) {
		}
		return m_searchResultElementConstructor;
	}

	public String getSashName() {
		return "Google";
	}

	public void initialize(sashIActionRuntime act, String registrationXml, String webGuid, int contextID) {
		m_act= act;
		this.contextID= contextID;
	}

	public void cleanup() {
	}

	public void processEvent() {
	}

}
