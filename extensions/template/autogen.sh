#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

# TODO: replace 'template' with the name of your extension
PKG_NAME="SashXB-template"

. ../../prepare-build.sh
. $srcdir/macros/autogen.sh
