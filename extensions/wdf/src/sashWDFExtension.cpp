/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):
    Stefan Atev (atevstef@luther.edu)

*****************************************************************/

#include "sashIGenericConstructor.h"
#include "sashWDFExtension.h"
#include "extensiontools.h"

using namespace std;

SASH_EXTENSION_IMPL_NO_NSGET(sashWDFExtension, sashIWDFExtension, "WDF");

NS_IMPL_ISUPPORTS2_CI(sashWDFDocument, sashIXPWDF, sashIConstructor)
NS_IMPL_ISUPPORTS2_CI(sashWDFActionItem, sashIXPWDFAction, sashIConstructor)
NS_IMPL_ISUPPORTS2_CI(sashWDFDependencyItem, sashIXPWDFDependency, sashIConstructor)
NS_IMPL_ISUPPORTS2_CI(sashWDFInstallScreenItem, sashIXPWDFInstallScreen, sashIConstructor)
NS_IMPL_ISUPPORTS2_CI(sashWDFFileItem, sashIXPWDFFile, sashIConstructor)

sashWDFExtension::sashWDFExtension() :
  m_act(NULL),
  DocumentConstructor(NULL),
  ActionItemConstructor(NULL),
  DependencyItemConstructor(NULL),
  InstallScreenItemConstructor(NULL),
  FileItemConstructor(NULL)
{
  NS_INIT_ISUPPORTS();
}

sashWDFExtension::~sashWDFExtension() {
  NS_IF_RELEASE(DocumentConstructor);
  NS_IF_RELEASE(ActionItemConstructor);
  NS_IF_RELEASE(DependencyItemConstructor);
  NS_IF_RELEASE(InstallScreenItemConstructor);
  NS_IF_RELEASE(FileItemConstructor);
  NS_IF_RELEASE(m_act);
}

NS_IMETHODIMP sashWDFExtension::ProcessEvent() {
  return NS_OK;
}
NS_IMETHODIMP sashWDFExtension::Cleanup() {
  return NS_OK;
}

NS_IMETHODIMP sashWDFExtension::Initialize(
  sashIActionRuntime * act,
  const char *registrationXml, const char *webGuid,
  JSContext *cx, JSObject *obj)
{
  m_act= act;
  NS_IF_ADDREF(m_act);
  char *str= NULL;
  if (NS_FAILED(act->GetDataDirectory(&str)))
    return NS_OK;
  if (str) {
    base_dir= str;
    nsCRT::free(str);
  }
  else
    base_dir= "";
  if ((base_dir.length()> 0) && (base_dir[base_dir.length()- 1]!= '/'))
    base_dir+= "/";
  NewSashConstructor(m_act, this, SASHWDFDOCUMENT_CONTRACT_ID, SASHIXPWDF_IID_STR, &DocumentConstructor);
  NewSashConstructor(m_act, this, SASHWDFACTIONITEM_CONTRACT_ID, SASHIXPWDFACTION_IID_STR, &ActionItemConstructor);
  NewSashConstructor(m_act, this, SASHWDFDEPENDENCYITEM_CONTRACT_ID, SASHIXPWDFDEPENDENCY_IID_STR, &DependencyItemConstructor);
  NewSashConstructor(m_act, this, SASHWDFINSTALLSCREENITEM_CONTRACT_ID, SASHIXPWDFINSTALLSCREEN_IID_STR, &InstallScreenItemConstructor);
  NewSashConstructor(m_act, this, SASHWDFFILEITEM_CONTRACT_ID, SASHIXPWDFFILE_IID_STR, &FileItemConstructor);
  return NS_OK;
}

NS_IMETHODIMP sashWDFExtension::GetDocument(sashIGenericConstructor **ret) {
  *ret= DocumentConstructor;
  NS_IF_ADDREF(*ret);
  return NS_OK;
}
NS_IMETHODIMP sashWDFExtension::GetActionItem(sashIGenericConstructor **ret) {
  *ret= ActionItemConstructor;
  NS_IF_ADDREF(*ret);
  return NS_OK;
}
NS_IMETHODIMP sashWDFExtension::GetDependencyItem(sashIGenericConstructor **ret) {
  *ret= DependencyItemConstructor;
  NS_IF_ADDREF(*ret);
  return NS_OK;
}
NS_IMETHODIMP sashWDFExtension::GetInstallScreenItem(sashIGenericConstructor **ret) {
  *ret= InstallScreenItemConstructor;
  NS_IF_ADDREF(*ret);
  return NS_OK;
}
NS_IMETHODIMP sashWDFExtension::GetFileItem(sashIGenericConstructor **ret) {
  *ret= FileItemConstructor;
  NS_IF_ADDREF(*ret);
  return NS_OK;
}

NS_IMETHODIMP sashWDFExtension::GetBasePath(char **_retval) {
  XP_COPY_STRING(base_dir, _retval);
  return NS_OK;
}

/* "CONSTANTS" */
#define WDF_IMPL_GET_CONST(name, val) \
NS_IMETHODIMP sashWDFExtension::Get ## name (PRInt32 *ret) \
{ *ret= val; return NS_OK; }

WDF_IMPL_GET_CONST(SET_WEBLICATION, 0)
WDF_IMPL_GET_CONST(SET_EXTENSION, 1)
WDF_IMPL_GET_CONST(SET_LOCATION, 2)
WDF_IMPL_GET_CONST(SET_NUM_VALUES, 3)
WDF_IMPL_GET_CONST(SET_UNDEFINED, -1)
WDF_IMPL_GET_CONST(IST_TEXT, 0)
WDF_IMPL_GET_CONST(IST_LICENSE, 1)
WDF_IMPL_GET_CONST(IST_CUSTOM, 2)
WDF_IMPL_GET_CONST(IST_POSTSECURITY, 3)
WDF_IMPL_GET_CONST(IST_FAILURE, 4)
WDF_IMPL_GET_CONST(IST_SECURITY, 5)
WDF_IMPL_GET_CONST(IST_SUCCESS, 6)
WDF_IMPL_GET_CONST(IST_SCRIPT, 7)
WDF_IMPL_GET_CONST(IST_NUM_VALUES, 8)
WDF_IMPL_GET_CONST(IST_UNDEFINED, -1)
WDF_IMPL_GET_CONST(FLT_WEBLICATION, 0)
WDF_IMPL_GET_CONST(FLT_CACHE, 1)
WDF_IMPL_GET_CONST(FLT_WEBLICATIONPATH, 2)
WDF_IMPL_GET_CONST(FLT_SHAREDLOCATION, 3)
WDF_IMPL_GET_CONST(FLT_SERVER, 4)
WDF_IMPL_GET_CONST(FLT_NUM_VALUES, 5)
WDF_IMPL_GET_CONST(FLT_UNDEFINED, -1)

/* INITIALIZE WRAPPER */

#define IMPL_WDF_INITIALIZE(obj, objinit) \
obj::obj() { NS_INIT_ISUPPORTS(); } \
obj::~obj() { } \
NS_IMETHODIMP obj::InitializeNewObject( \
  sashIActionRuntime * actionRuntime, \
  sashIExtension *parent, \
  JSContext *cx, PRUint32 argc, \
  nsIVariant **argv, PRBool *_retval) \
{ \
  m_rt = actionRuntime; \
  if (argc!= 0) return NS_OK; \
  nsCOMPtr<sashIWDFExtension> dad = do_QueryInterface(parent); \
  if (!dad) OutputError("WDF object must be created by the WDF extension!\n"); \
  XP_GET_STRING(dad->GetBasePath, m_base_dir); \
  m_f= objinit(); \
  *_retval= true; \
  return NS_OK; \
}

IMPL_WDF_INITIALIZE(sashWDFDocument, NewSashXPWDF)
IMPL_WDF_INITIALIZE(sashWDFActionItem, NewSashXPWDFAction)
IMPL_WDF_INITIALIZE(sashWDFDependencyItem, NewSashXPWDFDependency)
IMPL_WDF_INITIALIZE(sashWDFInstallScreenItem, NewSashXPWDFInstallScreen)
IMPL_WDF_INITIALIZE(sashWDFFileItem, NewSashXPWDFFile)

// Module part

NS_DECL_CLASSINFO(sashWDFExtension);
NS_DECL_CLASSINFO(sashWDFDocument);
NS_DECL_CLASSINFO(sashWDFActionItem);
NS_DECL_CLASSINFO(sashWDFDependencyItem);
NS_DECL_CLASSINFO(sashWDFInstallScreenItem);
NS_DECL_CLASSINFO(sashWDFFileItem);

NS_GENERIC_FACTORY_CONSTRUCTOR(sashWDFExtension);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashWDFDocument);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashWDFActionItem);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashWDFDependencyItem);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashWDFInstallScreenItem);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashWDFFileItem);

static nsModuleComponentInfo sashWDFExtension_components[] = {
  MODULE_COMPONENT_ENTRY(sashWDFExtension, SASHWDFEXTENSION, "WDF toplevel object"),
  MODULE_COMPONENT_ENTRY(sashWDFDocument, SASHWDFDOCUMENT, "WDF document"),
  MODULE_COMPONENT_ENTRY(sashWDFActionItem, SASHWDFACTIONITEM, "WDF action"),
  MODULE_COMPONENT_ENTRY(sashWDFDependencyItem, SASHWDFDEPENDENCYITEM, "WDF dependency"),
  MODULE_COMPONENT_ENTRY(sashWDFInstallScreenItem, SASHWDFINSTALLSCREENITEM, "WDF install screen"),
  MODULE_COMPONENT_ENTRY(sashWDFFileItem, SASHWDFFILEITEM, "WDF file")
};
  
NS_IMPL_NSGETMODULE(sashWDFExtension, sashWDFExtension_components);
