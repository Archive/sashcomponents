#ifndef SASHWDFEXTENSION_H
#define SASHWDFEXTENSION_H

/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):
    Stefan Atev (atevstef@luther.edu)

*****************************************************************/

#include "sashIWDFExtension.h"
#include "sashIXPWDFStructs.h"
#include "sashIXPWDF.h"
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include <string>

// CID: 4aec3403-c424-4bad-81f4-72704ea365a8
#define SASHWDFEXTENSION_CONTRACT_ID "@gnome.org/SashXB/sashWDFExtension;1"
#define SASHWDFEXTENSION_CID {0x4aec3403, 0xc424, 0x4bad, {0x81, 0xf4, 0x72, 0x70, 0x4e, 0xa3, 0x65, 0xa8}}
NS_DEFINE_CID(ksashWDFExtensionCID, SASHWDFEXTENSION_CID);

class sashWDFExtension: public sashIWDFExtension, public sashIExtension {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHIEXTENSION
    NS_DECL_SASHIWDFEXTENSION

    sashWDFExtension();
    virtual ~sashWDFExtension();
    
  private:
  
    sashIActionRuntime *m_act;
    std::string base_dir;
    sashIGenericConstructor *DocumentConstructor;
    sashIGenericConstructor *ActionItemConstructor;
    sashIGenericConstructor *DependencyItemConstructor;
    sashIGenericConstructor *InstallScreenItemConstructor;
    sashIGenericConstructor *FileItemConstructor;
    
};

/* FORWARDERS */

class XPWDFForward: public sashIXPWDF {
  public:
    NS_FORWARD_SAFE_SASHIXPWDF(m_f)
    XPWDFForward() : m_f(0) {
    }
    virtual ~XPWDFForward() {
    }
  protected:
    sashIXPWDF *m_f;
};

class XPWDFActionForward: public sashIXPWDFAction {
  public:
    NS_FORWARD_SAFE_SASHIXPWDFACTION(m_f)
    XPWDFActionForward() : m_f(0) {
    }
    virtual ~XPWDFActionForward() {
    }
  protected:
    sashIXPWDFAction *m_f;
};

class XPWDFDependencyForward: public sashIXPWDFDependency {
  public:
    NS_FORWARD_SAFE_SASHIXPWDFDEPENDENCY(m_f)
    XPWDFDependencyForward() : m_f(0) {
    }
    virtual ~XPWDFDependencyForward() {
    }
  protected:
    sashIXPWDFDependency *m_f;
};

class XPWDFInstallScreenForward: public sashIXPWDFInstallScreen {
  public:
    NS_FORWARD_SAFE_SASHIXPWDFINSTALLSCREEN(m_f)
    XPWDFInstallScreenForward() : m_f(0) {
    }
    virtual ~XPWDFInstallScreenForward() {
    }
  protected:
    sashIXPWDFInstallScreen *m_f;
};

class XPWDFFileForward: public sashIXPWDFFile {
  public:
    NS_FORWARD_SAFE_SASHIXPWDFFILE(m_f)
    XPWDFFileForward() : m_f(0) {
    }
    virtual ~XPWDFFileForward() {
    }
  protected:
    sashIXPWDFFile *m_f;
};

/* WRAPPERS */

// CID: 720ea0b5-50cf-4343-9631-3903a1ca097b
#define SASHWDFDOCUMENT_CONTRACT_ID "@gnome.org/SashXB/sashWDFDocumentItem;1"
#define SASHWDFDOCUMENT_CID {0x720ea0b5, 0x50cf, 0x4343, {0x96, 0x31, 0x39, 0x03, 0xa1, 0xca, 0x09, 0x7b}}
NS_DEFINE_CID(ksashWDFDocumentCID, SASHWDFDOCUMENT_CID);

class sashWDFDocument: public XPWDFForward, public sashIConstructor {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHICONSTRUCTOR

    sashWDFDocument();
    virtual ~sashWDFDocument();

  private:
  
    sashIActionRuntime *m_rt;
    std::string m_base_dir;
};

// CID: 3e3513bc-b3b3-4bcd-97b6-481505657124
#define SASHWDFACTIONITEM_CONTRACT_ID "@gnome.org/SashXB/sashWDFActionItem;1"
#define SASHWDFACTIONITEM_CID {0x3e3513bc, 0xb3b3, 0x4bcd, {0x97, 0xb6, 0x48, 0x15, 0x05, 0x65, 0x71, 0x24}}
NS_DEFINE_CID(ksashWDFActionItemCID, SASHWDFACTIONITEM_CID);

class sashWDFActionItem: public XPWDFActionForward, public sashIConstructor {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHICONSTRUCTOR

    sashWDFActionItem();
    virtual ~sashWDFActionItem();

  private:
  
    sashIActionRuntime *m_rt;
    std::string m_base_dir;
};

// CID: cafa5e7d-55ef-481a-8111-b7a6f196a85d
#define SASHWDFDEPENDENCYITEM_CONTRACT_ID "@gnome.org/SashXB/sashWDFDependencyItem;1"
#define SASHWDFDEPENDENCYITEM_CID {0xcafa5e7d, 0x55ef, 0x481a, {0x81, 0x11, 0xb7, 0xa6, 0xf1, 0x96, 0xa8, 0x5d}}
NS_DEFINE_CID(ksashWDFDependencyItemCID, SASHWDFDEPENDENCYITEM_CID);

class sashWDFDependencyItem: public XPWDFDependencyForward, public sashIConstructor {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHICONSTRUCTOR

    sashWDFDependencyItem();
    virtual ~sashWDFDependencyItem();

  private:
  
    sashIActionRuntime *m_rt;
    std::string m_base_dir;
};

// CID: 7c08e4cd-bb4d-4a6e-91f3-69caed4ba249
#define SASHWDFINSTALLSCREENITEM_CONTRACT_ID "@gnome.org/SashXB/sashWDFInstallScreenItem;1"
#define SASHWDFINSTALLSCREENITEM_CID {0x7c08e4cd, 0xbb4d, 0x4a6e, {0x91, 0xf3, 0x69, 0xca, 0xed, 0x4b, 0xa2, 0x49}}
NS_DEFINE_CID(ksashWDFInstallScreenItemCID, SASHWDFINSTALLSCREENITEM_CID);

class sashWDFInstallScreenItem: public XPWDFInstallScreenForward, public sashIConstructor {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHICONSTRUCTOR

    sashWDFInstallScreenItem();
    virtual ~sashWDFInstallScreenItem();

  private:
  
    sashIActionRuntime *m_rt;
    std::string m_base_dir;
};

// CID: 1ad02983-19e9-48a3-9f06-0632bb383753
#define SASHWDFFILEITEM_CONTRACT_ID "@gnome.org/SashXB/sashWDFFileItem;1"
#define SASHWDFFILEITEM_CID {0x1ad02983, 0x19e9, 0x48a3, {0x9f, 0x06, 0x06, 0x32, 0xbb, 0x38, 0x37, 0x53}}
NS_DEFINE_CID(ksashWDFFileItemCID, SASHWDFFILEITEM_CID);

class sashWDFFileItem: public XPWDFFileForward, public sashIConstructor {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHICONSTRUCTOR

    sashWDFFileItem();
    virtual ~sashWDFFileItem();

  private:
  
    sashIActionRuntime *m_rt;
    std::string m_base_dir;
};

#endif
