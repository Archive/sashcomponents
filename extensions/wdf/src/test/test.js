print ("Testing WDF Extension\n");

function file2str(f) {
  return "    .precache= "+ f.precache+
    "\n    .location= "+ f.location+
    "\n    .filename= "+ f.filename;
}

function act2str(a) {
  return "    .id= "+ a.id+
    "\n    .locationid= "+ a.locationid+
    "\n    .name= "+ a.name+
    "\n    .registration= "+ a.registration;
}

function inst2str(a) {
  return "    .type= "+ a.type+
    "\n    .title= "+ a.title+
    "\n    .pageFile= "+ a.pageFile+
    "\n    .pageLocation= "+ a.pageLocation+
    "\n    .imageFile= "+ a.imageFile+
    "\n    .imageLocation= "+ a.imageLocation+
    "\n    .textFile= "+ a.textFile+
    "\n    .textLocation= "+ a.textLocation+
    "\n    .next= "+ a.next+
    "\n    .back= "+ a.back;
}

var wd= new Sash.WDF.Document();
var fname= "test.wdf";

print(".OpenAmbiguousFile(\"" +fname+ "\")= " + wd.OpenAmbiguousFile(fname));
print(".version= "+ wd.version);
print("  .weblication= "+ wd.version.weblication);
print("  .sashMinimum= "+ wd.version.sashMinimum);
print("  .sashRecommended= "+ wd.version.sashRecommended);
print("  .mozillaMinimum= "+ wd.version.mozillaMinimum);
print("  .mozillaRecommended= "+ wd.version.mozillaRecommended);

var pl= wd.platforms;
print(".platforms");
print("  .length= "+ pl.length);
for (i in pl)
  print("  ["+ i+ "]= " + pl[i]);

var fl= wd.files;
print(".files");
print("  .length= "+ fl.length);
for (i in fl)
  print("  ["+ i+ "]= " + fl[i]+ "\n"+ file2str(fl[i]));

var al= wd.actions;
print(".actions");
print("  .length= "+ al.length);
for (i in al)
  print("  ["+ i+ "]= " + al[i]+ "\n"+ act2str(al[i]));
  
var il= wd.installScreens;
print(".installScreens");
print("  .length= "+ il.length);
for (i in il)
  print("  ["+ i+ "]= " + il[i]+ "\n"+ inst2str(il[i]));
  
Sash.Console.Quit();