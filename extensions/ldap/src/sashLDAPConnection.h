
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHLDAPCONNECTION_H
#define SASHLDAPCONNECTION_H

#include "sashILDAP.h"
#include "sashLDAPService.h"
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include "sashIConstructor.h"
#include <string>
#include <vector>

#define SASHLDAPCONNECTION_CONTRACT_ID "@gnome.org/SashXB/sashLDAPConnection;1"
#define SASHLDAPCONNECTION_CID {0x98c010aa, 0x3d02, 0x4eac, {0x9f, 0x26, 0x7e, 0x50, 0x7a, 0x79, 0xb9, 0xa3}}
NS_DEFINE_CID(ksashLDAPConnectionCID, SASHLDAPCONNECTION_CID);

typedef vector<sashLDAPMessage> messages_vector;

class sashLDAPConnection: public sashILDAPConnection, public sashIConstructor, 
						  public sashINativeLDAPConnection, public sashIExtension
{
  public:
    NS_DECL_ISUPPORTS
	NS_DECL_SASHICONSTRUCTOR
    NS_DECL_SASHILDAPCONNECTION
	NS_DECL_SASHINATIVELDAPCONNECTION
    NS_DECL_SASHIEXTENSION

    sashLDAPConnection();
    virtual ~sashLDAPConnection();

  protected:
	void ParseServer(const string & server, string & host, int & port);
	
	void attributesToCollection(const vector<sashLDAPAttribute> &attributes,
								sashICollection *collection);

	bool CallSearchCompleteEvent(sashLDAPMessage &message);
	bool CallSearchResultEvent(sashLDAPMessage &message);
	bool CallCompareCompleteEvent(sashLDAPMessage &message);
	bool CallAddEntryCompleteEvent(sashLDAPMessage &message);
	bool CallDeleteEntryCompleteEvent(sashLDAPMessage &message);
	bool CallRenameEntryCompleteEvent(sashLDAPMessage &message);
	bool CallModifyAttributeCompleteEvent(sashLDAPMessage &message);

    nsCOMPtr<sashIActionRuntime> m_rt;
	JSContext *m_cx;
	nsCOMPtr<sashIExtension> m_ext;

	string m_host;
	int m_port;
	
	int m_searchMaxTime;
	int m_searchMaxResults;
	int m_aliasDerefOption;

	pthread_mutex_t m_openRequestsLock;
	void LockOpenRequests() { pthread_mutex_lock(&m_openRequestsLock); }
	void UnlockOpenRequests() { pthread_mutex_unlock(&m_openRequestsLock); }
	void addRequest(int key, sashILDAPRequest *value);
	sashILDAPRequest *removeRequest(int key);
	void clearRequests();

	hash_map<int, sashILDAPRequest*> m_openRequests;

	string m_user;
	string m_password;

	nsCOMPtr<sashILDAPErrorCallback> m_onError;

	LDAP * m_ldap;

	nsCOMPtr<sashIGenericConstructor> m_searchConstructor;
	nsCOMPtr<sashIGenericConstructor> m_compareConstructor;
	nsCOMPtr<sashIGenericConstructor> m_addEntryConstructor;
	nsCOMPtr<sashIGenericConstructor> m_renameEntryConstructor;
	nsCOMPtr<sashIGenericConstructor> m_deleteEntryConstructor;
	nsCOMPtr<sashIGenericConstructor> m_addAttributeConstructor;
	nsCOMPtr<sashIGenericConstructor> m_replaceAttributeConstructor;
	nsCOMPtr<sashIGenericConstructor> m_deleteAttributeConstructor;

};

#endif
