
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):

*****************************************************************/

#include "sashIGenericConstructor.h"
#include "sashICollection.h"
#include "sashLDAPConnection.h"
#include "extensiontools.h"
#include "sashLDAPRequest.h"
#include "sashVariantUtils.h"
#include <ldap.h>

using namespace std;

NS_IMPL_ISUPPORTS4_CI(sashLDAPConnection, sashILDAPConnection, 
					  sashIConstructor, 
					  sashINativeLDAPConnection,
					  sashIExtension);

sashLDAPConnection::sashLDAPConnection() :
	 m_searchMaxTime(-1) ,
	 m_searchMaxResults(-1),
	 m_aliasDerefOption(LDAP_DEREF_ALWAYS)
{
  NS_INIT_ISUPPORTS();
  pthread_mutex_init(&m_openRequestsLock, 0);
  m_ldap = NULL;
}

sashLDAPConnection::~sashLDAPConnection() 
{
	 pthread_mutex_destroy(&m_openRequestsLock);
}

NS_IMETHODIMP
sashLDAPConnection::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP
sashLDAPConnection::ProcessEvent()
{
	 return NS_OK;
}

NS_IMETHODIMP
sashLDAPConnection::GetSashName(char **name)
{
	 XP_COPY_STRING("LDAPConnection", name);
	 return NS_OK;
}

NS_IMETHODIMP
sashLDAPConnection::Initialize(sashIActionRuntime *act, 
			   const char *registrationXml, 
			   const char *webGuid,
			   JSContext *cx, JSObject *obj)
{
	 return NS_OK;
}



void sashLDAPConnection::ParseServer(const string & server, string & host, int & port) {
	 int pos = server.rfind(':');
	 if (pos >= 0) {
		  host = server.substr(0, pos);
		  port = atoi(server.substr(pos, server.length()).c_str());
	 }
	 else {
		  host = server;
		  port = LDAP_PORT;
	 }
}

NS_IMETHODIMP sashLDAPConnection::InitializeNewObject(sashIActionRuntime * actionRuntime, 
													  sashIExtension * ext, 
													  JSContext * cx, PRUint32 argc,
													  nsIVariant **argv, PRBool *ret)
{
	 DEBUGMSG(ldap, "sashLDAPConnection::InitializeNewObject()\n");
	 m_rt = actionRuntime;
	 m_cx = cx;
	 m_ext = ext;
	 if (argc == 0) {
		  *ret = false;
	 }
	 else if (argc >= 1) {
		  *ret = true;
		  ParseServer(VariantGetString(argv[0]), m_host, m_port);
		  if (argc >= 2) {
			   m_user = VariantGetString(argv[1]);
			   if (argc >= 3) {
					m_password = VariantGetString(argv[2]);
			   }
		  }

		  NewSashConstructor(m_rt, this, SASHLDAPSEARCHREQUEST_CONTRACT_ID,
							 SASHILDAPSEARCHREQUEST_IID_STR,
							 getter_AddRefs(m_searchConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPCOMPAREREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_compareConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPADDENTRYREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_addEntryConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPRENAMEENTRYREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_renameEntryConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPDELETEENTRYREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_deleteEntryConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPADDATTRIBUTEREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_addAttributeConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPREPLACEATTRIBUTEREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_replaceAttributeConstructor));
		  NewSashConstructor(m_rt, this, SASHLDAPDELETEATTRIBUTEREQUEST_CONTRACT_ID,
							 SASHILDAPREQUEST_IID_STR,
							 getter_AddRefs(m_deleteAttributeConstructor));

	 }
	 return NS_OK;
}

/* attribute long SearchMaxTime; */
NS_IMETHODIMP sashLDAPConnection::GetSearchMaxTime(PRInt32 *aSearchMaxTime)
{
	 *aSearchMaxTime = m_searchMaxTime;
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPConnection::SetSearchMaxTime(PRInt32 aSearchMaxTime)
{
	 m_searchMaxTime = aSearchMaxTime;
	 return NS_OK;
}

/* attribute long SearchMaxResults; */
NS_IMETHODIMP sashLDAPConnection::GetSearchMaxResults(PRInt32 *aSearchMaxResults)
{
	 *aSearchMaxResults = m_searchMaxResults;
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPConnection::SetSearchMaxResults(PRInt32 aSearchMaxResults)
{
	 m_searchMaxResults = aSearchMaxResults;

	 if (m_ldap){
		  if (m_searchMaxResults != -1){
			   sashLDAPService * service = GetSashLDAPService();
			   DEBUGMSG(ldap, "Setting search max results to %d.\n",
						m_searchMaxResults);
			   service->SetSearchMaxResults(m_ldap, &m_searchMaxResults);
		  }
	 }
	 return NS_OK;
}

/* attribute long AliasDerefOption; */
NS_IMETHODIMP sashLDAPConnection::GetAliasDerefOption(PRInt32 *aAliasDerefOption)
{
	 *aAliasDerefOption = m_aliasDerefOption;
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPConnection::SetAliasDerefOption(PRInt32 aAliasDerefOption)
{
	 if ((m_aliasDerefOption != LDAP_DEREF_ALWAYS)
		 && (m_aliasDerefOption != LDAP_DEREF_NEVER)
		 && (m_aliasDerefOption != LDAP_DEREF_SEARCHING)
		 && (m_aliasDerefOption != LDAP_DEREF_FINDING))
		  return NS_OK;
	 
	 m_aliasDerefOption = aAliasDerefOption;
	 return NS_OK;
}

/* boolean Connect (); */
NS_IMETHODIMP sashLDAPConnection::Connect(PRBool *_retval)
{
	 *_retval = false;
	 sashLDAPService * service = GetSashLDAPService();
	 m_ldap = service->ConnectToServer(m_host, m_port, 
									   m_user, m_password);
	 
	 if (m_ldap){
		  DEBUGMSG(ldap, "Connected to server successfully.\n");
		  
		  nsCOMPtr<sashILDAP> ldap = do_QueryInterface(m_ext);
		  if (ldap) {
			   ldap->RegisterConnection(this);
			   
			   // Set the connection options.
			   if (m_searchMaxTime != -1){
					DEBUGMSG(ldap, "Setting search max time to %d.\n",
							 m_searchMaxTime);
					service->SetSearchMaxTime(m_ldap, &m_searchMaxTime);
			   }
			   
			   if (m_searchMaxResults != -1){
					DEBUGMSG(ldap, "Setting search max results to %d.\n",
							 m_searchMaxResults);
					service->SetSearchMaxResults(m_ldap, &m_searchMaxResults);
			   }

			   DEBUGMSG(ldap, "Setting alias deref option.\n");
			   service->SetAliasDerefOption(m_ldap, &m_aliasDerefOption);

			   DEBUGMSG(ldap, "Setting referal option.\n");
			   service->SetReferralOption(m_ldap, false);

			   *_retval = true;
		  }
		  else {
			   service->DisconnectFromServer(m_ldap);
			   OutputError("The LDAP extension does not support the sashILDAP interface!");
		  }		  
	 } else {
		  DEBUGMSG(ldap, "Couldn't connect to server.\n");
	 }
	 return NS_OK;
}

// WWW memory management???
void sashLDAPConnection::addRequest(int key, sashILDAPRequest *value){
	 DEBUGMSG(ldap, "sashLDAPConnection::addRequest()\n");
	 LockOpenRequests();
	 m_openRequests[key] = value;
	 UnlockOpenRequests();
	 DEBUGMSG(ldap, "sashLDAPConnection::addRequest() done\n");
}

sashILDAPRequest *sashLDAPConnection::removeRequest(int key){
	 DEBUGMSG(ldap, "sashLDAPConnection::removeRequest()\n");
	 sashILDAPRequest *retval;
	 LockOpenRequests();
	 DEBUGMSG(ldap, "openrequests size before: %d\n", 
			  m_openRequests.size());
	 retval = m_openRequests[key];
	 m_openRequests.erase(key);
	 DEBUGMSG(ldap, "openrequests size after: %d\n", 
			  m_openRequests.size());

	 UnlockOpenRequests();
	 DEBUGMSG(ldap, "sashLDAPConnection::removeRequest done()\n");
	 return retval;
}

void sashLDAPConnection::clearRequests(){
	 LockOpenRequests();
	 m_openRequests.clear();
	 UnlockOpenRequests();
}

/* [noscript] void AddOpenRequest (in nativeLDAP ldap, in long messageID, in sashILDAPRequest request); */
NS_IMETHODIMP sashLDAPConnection::AddOpenRequest(PRInt32 messageID, sashILDAPRequest *request)
{
	 DEBUGMSG(ldap, "sashLDAPConnection::AddOpenRequest() for msgid: %d\n",
			  messageID);
	 addRequest(messageID, request);
	 DEBUGMSG(ldap, "sashLDAPConnection::AddOpenRequest() done\n");
	 return NS_OK;
}

/* [noscript] void RemoveOpenRequest (in nativeLDAP ldap, in long messageID); */
NS_IMETHODIMP sashLDAPConnection::RemoveOpenRequest(PRInt32 messageID)
{
	 DEBUGMSG(ldap, "sashLDAPConnection::RemoveOpenRequest() for msgid: %d\n",
			  messageID);
	 removeRequest(messageID);
	 DEBUGMSG(ldap, "sashLDAPConnection::RemoveOpenRequest done()\n");
	 return NS_OK;
}

void sashLDAPConnection::attributesToCollection(const vector<sashLDAPAttribute> &attributes,
												sashICollection *collection){
	 DEBUGMSG(ldap, "sashLDAPConnection::attributesToCollection\n");
	 nsIVariant *valueVar;
	 vector<sashLDAPAttribute>::const_iterator a = attributes.begin(),
		  b = attributes.end();
	 while (a != b){
		  NewVariant(&valueVar);
		  VariantSetArray(valueVar, (*a).values);
		  DEBUGMSG(ldap, "adding to collection: %s, %d values, first value: %s\n",
				   (*a).name.c_str(), (*a).values.size(),
				   ((*a).values.size()>0) ? (*a).values[0].c_str() : "no values");
		  collection->Add((*a).name.c_str(), valueVar);
		  ++a;
	 }
}

bool sashLDAPConnection::CallSearchCompleteEvent(sashLDAPMessage &message){
	 DEBUGMSG(ldap, "sashLDAPConnection::CallSearchCompleteEvent\n");
	 sashILDAPRequest *currRequest = removeRequest(message.msgID);

	 if (! currRequest) return false;

	 nsCOMPtr<sashILDAPSearchRequest> searchRequest
		  = do_QueryInterface(currRequest);
	 if (! searchRequest) return false;

	 nsIVariant *entryArrayVar;
	 NewVariant (&entryArrayVar);
	 bool success;
	 string desc = "";

	 if (success = (message.msgResult == LDAP_SUCCESS)){
		  DEBUGMSG(ldap, "Search Complete was successful.\n");
		  vector<nsISupports *> collections;
		  sashICollection *collection;

		  // iterate through the message entries. create an array of
		  // all search results that have been returned (each result
		  // corresponds to one of the elements of the message.entries
		  // vector.
		  // WWW not sure if this is possible.
		  DEBUGMSG(ldap, "trying to convert this message into an array of collections...\n");

		  message.printLDAPMessage();
		  vector<sashLDAPEntry>::iterator a = message.entries.begin(),
			   b = (message).entries.end();
		  while (a != b){
			   NewSashCollection(&collection);
			   attributesToCollection((*a).attributes, collection);
			   NS_ADDREF(collection);
			   collections.push_back(collection);
			   ++a;
		  }
		  VariantSetArray(entryArrayVar, collections);
	 } else {
		  VariantSetEmpty(entryArrayVar);
		  desc = ldap_err2string(message.msgResult);
	 }
	 
	 nsCOMPtr<sashILDAPSearchCompleteCallback> callback;
	 searchRequest->GetOnSearchComplete(getter_AddRefs(callback));
	 callback->Callback(searchRequest, entryArrayVar, success, desc.c_str());
	 return true;
}

bool sashLDAPConnection::CallSearchResultEvent(sashLDAPMessage &message){
	 DEBUGMSG(ldap, "sashLDAPConnection::CallSearchResultEvent\n");
	 sashILDAPRequest *currRequest;
	 LockOpenRequests();
	 currRequest = m_openRequests[message.msgID];
	 UnlockOpenRequests();

	 if (! currRequest) {DEBUGMSG(ldap, "couldn't look up request for id %d\n", message.msgID); return false;}
	 
	 nsCOMPtr<sashILDAPSearchRequest> searchRequest
		  = do_QueryInterface(currRequest);
	 if (! searchRequest) {DEBUGMSG(ldap, "couldn't get sashILDAPSearchRequest out of request\n");return false;}

	 // Assuming that if there is a search result to return,
	 // there will be at least one entry.
	 if (message.entries.size() == 0) {DEBUGMSG(ldap, "0 entries in the message\n");return false;}

	 sashICollection *collection;
	 NewSashCollection(&collection);
	 attributesToCollection(message.entries[0].attributes,
							collection);
	 
	 nsCOMPtr<sashILDAPSearchResultCallback> callback;
	 searchRequest->GetOnSearchResult(getter_AddRefs(callback));
	 callback->Callback(searchRequest, collection);

	 return true;
}

bool sashLDAPConnection::CallCompareCompleteEvent(sashLDAPMessage &message){
	 DEBUGMSG(ldap, "sashLDAPConnection::CallCompareCompleteEvent\n");
	 sashILDAPRequest *currRequest = removeRequest(message.msgID);
	 if (! currRequest) return false;
	 
	 nsCOMPtr<sashILDAPCompareRequest> compareRequest
		  = do_QueryInterface(currRequest);
	 if (! compareRequest) return false;
	 
	 bool compareResult = false, success = true;
	 string desc = "";

	 if (message.msgResult == LDAP_COMPARE_TRUE){
		  DEBUGMSG(ldap, "result matched true\n");
	 }else if (message.msgResult == LDAP_COMPARE_FALSE){
		  DEBUGMSG(ldap, "result matched false\n");
	 }else{
		  DEBUGMSG(ldap, "result wasn't true or false!\n");
		  desc = ldap_err2string(message.msgResult);
		  DEBUGMSG(ldap, "err: %s\n", desc.c_str());
		  success = false;
	 }
	 compareResult = (message.msgResult == LDAP_COMPARE_TRUE);

	 nsCOMPtr<sashILDAPCompareCompleteCallback> callback;
	 compareRequest->GetOnCompareComplete(getter_AddRefs(callback));
	 callback->Callback(compareRequest, compareResult, success, desc.c_str());

	 return true;
}

bool sashLDAPConnection::CallAddEntryCompleteEvent(sashLDAPMessage &message){
	 sashILDAPRequest *currRequest = removeRequest(message.msgID);
	 if (! currRequest) return false;
	 
	 nsCOMPtr<sashILDAPAddEntryRequest> addEntryRequest
		  = do_QueryInterface(currRequest);
	 if (! addEntryRequest) return false;

	 sashICollection *collection;
	 NewSashCollection(&collection);
	 bool success;
	 string desc = "";

	 if (success = (message.msgResult == LDAP_SUCCESS)){
		  // Assume that there will only be one entry returned.
		  if (message.entries.size() >0)
			   attributesToCollection(message.entries[0].attributes,
									  collection);

	 } else {
		  desc = ldap_err2string(message.msgResult);
	 }
	 nsCOMPtr<sashILDAPAddEntryCompleteCallback> callback;
	 addEntryRequest->GetOnAddEntryComplete(getter_AddRefs(callback));
	 callback->Callback(addEntryRequest, collection, success, desc.c_str());

	 return true;
}


bool sashLDAPConnection::CallDeleteEntryCompleteEvent(sashLDAPMessage &message){
	 sashILDAPRequest *currRequest = removeRequest(message.msgID);
	 if (! currRequest) return false;
	 
	 nsCOMPtr<sashILDAPDeleteEntryRequest> deleteEntryRequest
		  = do_QueryInterface(currRequest);
	 if (! deleteEntryRequest) return false;
	 
	 bool success = (message.msgResult == LDAP_SUCCESS);
	 string desc = "";
	 
	 if (! success)
		  desc = ldap_err2string(message.msgResult);
	 nsCOMPtr<sashILDAPOperationCompleteCallback> callback;
	 deleteEntryRequest->GetOnDeleteEntryComplete(getter_AddRefs(callback));
	 callback->Callback(currRequest, success, desc.c_str());

	 return true;
}

bool sashLDAPConnection::CallRenameEntryCompleteEvent(sashLDAPMessage &message){
	 sashILDAPRequest *currRequest = removeRequest(message.msgID);
	 if (! currRequest) return false;
	 
	 nsCOMPtr<sashILDAPRenameEntryRequest> renameEntryRequest
		  = do_QueryInterface(currRequest);
	 if (! renameEntryRequest) return false;

	 bool success = (message.msgResult == LDAP_SUCCESS);
	 string desc = "";

	 if (! success)
		  desc = ldap_err2string(message.msgResult);
	 nsCOMPtr<sashILDAPOperationCompleteCallback> callback;
	 renameEntryRequest->GetOnRenameEntryComplete(getter_AddRefs(callback));
	 callback->Callback(currRequest, success, desc.c_str());

	 return true;
}

bool sashLDAPConnection::CallModifyAttributeCompleteEvent(sashLDAPMessage &message){
	 sashILDAPRequest *currRequest = removeRequest(message.msgID);
	 if (! currRequest) return false;

	 bool success;
	 string desc = "";
//	 char *callback;

	 nsCOMPtr<sashILDAPOperationCompleteCallback> callback;

	 if (! (success = (message.msgResult == LDAP_SUCCESS)))
		  desc = ldap_err2string(message.msgResult);
		  
	 nsCOMPtr<sashILDAPAddAttributeRequest> addAttributeRequest;
	 nsCOMPtr<sashILDAPDeleteAttributeRequest> deleteAttributeRequest;
	 nsCOMPtr<sashILDAPReplaceAttributeRequest> replaceAttributeRequest;

	 addAttributeRequest = do_QueryInterface(currRequest);
	 if (addAttributeRequest){
		  addAttributeRequest->GetOnAddAttributeComplete(getter_AddRefs(callback));
	 } else {
		  deleteAttributeRequest = do_QueryInterface(currRequest);
		  if (deleteAttributeRequest){
			   deleteAttributeRequest->GetOnDeleteAttributeComplete(getter_AddRefs(callback));
		  } else {
			   replaceAttributeRequest = do_QueryInterface(currRequest);
			   if (replaceAttributeRequest){
					replaceAttributeRequest->GetOnReplaceAttributeComplete(getter_AddRefs(callback));
			   } else {
					return false;
			   }
		  }
	 }
	 callback->Callback(currRequest, success, desc.c_str());

	 return true;
}


NS_IMETHODIMP sashLDAPConnection::ProcessMessages(){
	 DEBUGMSG(ldap, "sashLDAPConnection::ProcessMessages()\n");
	 sashLDAPService *service = GetSashLDAPService();
	 messages_vector messagesVec;

	 service->GetMessages(m_ldap, messagesVec);
	 DEBUGMSG(ldap, "got %d messages\n", messagesVec.size());
	 vector<sashLDAPMessage>::iterator a = messagesVec.begin(),
		  b = messagesVec.end();
	 while (a != b){
		  // Use the message ID to figure out which request it's 
		  // associated with.
		  switch ((*a).msgType){
		  case LDAP_RES_SEARCH_ENTRY:
			   // A search has been completed, so return the 
			   // array of search objects that satisfied the
			   // search request.
			   // The request should be removed from the 
			   // open requests hashtable.
			   if (! CallSearchResultEvent(*a)){
					DEBUGMSG(ldap, "CallSearchResultEvent unsuccessful\n");
			   }
			   break;
		  case LDAP_RES_SEARCH_RESULT:
			   // A single search result is returned. 
			   // The search is still in progress, so do not
			   // remove it from the open requests hashtable.
			   if (! CallSearchCompleteEvent(*a)){
					DEBUGMSG(ldap, "CallSearchCompleteEvent unsuccessful\n");
			   }
			   break;
		  case LDAP_RES_COMPARE:
			   CallCompareCompleteEvent(*a);
			   break;
		  case LDAP_RES_ADD:
			   CallAddEntryCompleteEvent(*a);
			   break;
		  case LDAP_RES_DELETE:
			   CallDeleteEntryCompleteEvent(*a);
			   break;
		  case LDAP_RES_MODRDN:
			   CallRenameEntryCompleteEvent(*a);
			   break;
		  case LDAP_RES_MODIFY:
			   CallModifyAttributeCompleteEvent(*a);
			   break;
		  case LDAP_RES_BIND:
		  default:
			   // some sort of error.
			   DEBUGMSG(ldap, "Unrecognized message type.\n");
			   break;
		  }
		  ++a;
	 }
	 // Clear messagesVec.
	 return NS_OK;

}

/* readonly attribute string server; */
NS_IMETHODIMP sashLDAPConnection::GetServer(char * *aServer)
{
	 char buf[1024];
	 snprintf(buf, 1023, "%s:%d", m_host.c_str(), m_port);
	 XP_COPY_STRING(buf, aServer);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor SearchRequest; */
NS_IMETHODIMP sashLDAPConnection::GetSearchRequest(sashIGenericConstructor * *aSearchRequest)
{
	 DEBUGMSG(ldap, "sashLDAPConnection::GetSearchRequest()\n");

	 *aSearchRequest = m_searchConstructor;
	 NS_ADDREF(*aSearchRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor CompareRequest; */
NS_IMETHODIMP sashLDAPConnection::GetCompareRequest(sashIGenericConstructor * *aCompareRequest)
{
	 *aCompareRequest = m_compareConstructor;
	 NS_ADDREF(*aCompareRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor AddEntryRequest; */
NS_IMETHODIMP sashLDAPConnection::GetAddEntryRequest(sashIGenericConstructor * *aAddEntryRequest)
{
	 *aAddEntryRequest = m_addEntryConstructor;
	 NS_ADDREF(*aAddEntryRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor RenameEntryRequest; */
NS_IMETHODIMP sashLDAPConnection::GetRenameEntryRequest(sashIGenericConstructor * *aRenameEntryRequest)
{
	 *aRenameEntryRequest = m_renameEntryConstructor;
	 NS_ADDREF(*aRenameEntryRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor DeleteEntryRequest; */
NS_IMETHODIMP sashLDAPConnection::GetDeleteEntryRequest(sashIGenericConstructor * *aDeleteEntryRequest)
{
	 *aDeleteEntryRequest = m_deleteEntryConstructor;
	 NS_ADDREF(*aDeleteEntryRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor AddAttributeRequest; */
NS_IMETHODIMP sashLDAPConnection::GetAddAttributeRequest(sashIGenericConstructor * *aAddAttributeRequest)
{
	 *aAddAttributeRequest = m_addAttributeConstructor;
	 NS_ADDREF(*aAddAttributeRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor ReplaceattributeRequest; */
NS_IMETHODIMP sashLDAPConnection::GetReplaceAttributeRequest(sashIGenericConstructor * *aReplaceattributeRequest)
{
	 *aReplaceattributeRequest = m_replaceAttributeConstructor;
	 NS_ADDREF(*aReplaceattributeRequest);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor DeleteAttributeRequest; */
NS_IMETHODIMP sashLDAPConnection::GetDeleteAttributeRequest(sashIGenericConstructor * *aDeleteAttributeRequest)
{
	 *aDeleteAttributeRequest = m_deleteAttributeConstructor;
	 NS_ADDREF(*aDeleteAttributeRequest);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPConnection::GetLDAP(LDAP ** ldap) {
	 DEBUGMSG(ldap, "sashLDAPConnection::GetLDAP()\n");
	 *ldap = m_ldap;
	 return m_ldap ? NS_OK : NS_ERROR_FAILURE;
}

/* void Disconnect (); */
NS_IMETHODIMP sashLDAPConnection::Disconnect()
{
	 if (m_ldap){
		  sashLDAPService *service = GetSashLDAPService();		  
		  service->DisconnectFromServer(m_ldap);
		  clearRequests();

		  nsCOMPtr<sashILDAP> ldap = do_QueryInterface(m_ext);
		  if (ldap) {
			   ldap->UnregisterConnection(this);
		  }
		  else {
			   OutputError("The LDAP extension does not support the sashILDAP interface!");
		  }		  
	 } 
	 return NS_OK;
}

/* [noscript] boolean MatchLDAPServer (in nativeLDAP LDAP); */
NS_IMETHODIMP sashLDAPConnection::MatchLDAPServer(LDAP * LDAP, PRBool *_retval)
{
	 *_retval = (m_ldap == LDAP);
	 return NS_OK;
}

/* attribute string OnError; */
NS_IMETHODIMP sashLDAPConnection::GetOnError(sashILDAPErrorCallback * *aOnError)
{
	 *aOnError = m_onError;
	 NS_IF_ADDREF(*aOnError);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPConnection::SetOnError(sashILDAPErrorCallback * aOnError)
{
	 m_onError = aOnError;
	 return NS_OK;
}


/* [noscript] void CallMessageError (in nativeLDAPMessage message); */
NS_IMETHODIMP sashLDAPConnection::CallMessageError(LDAPMessage *message)
{
	 DEBUGMSG(ldap, "sashLDAPConnection::CallMessageError()\n");
	 int msgID = ldap_msgid(message);
	 int msgResult = ldap_result2error(m_ldap, message, 0);
	 string desc = ldap_err2string(msgResult);

	 if (msgID == -1){
		  // Error with the connection.
		  DEBUGMSG(ldap, "Calling a connection error.\n");
		  if (m_onError) 
			   m_onError->Callback(desc.c_str());
	 } else {
		  // Error with a particular request.
		  sashILDAPRequest *request = removeRequest(msgID);

		  if (request){
			   nsCOMPtr<sashILDAPErrorCallback> callback;
			   request->GetOnError(getter_AddRefs(callback));
			   DEBUGMSG(ldap, "Calling a request error.\n");
			   if (callback)
					callback->Callback(desc.c_str());
		  } else {
			   DEBUGMSG(ldap, "Calling a connection error (request could not be found).\n");
			   if (m_onError)
					m_onError->Callback(desc.c_str());
		  }
	 }
	 // WWW: clean up the message?
	 return NS_OK;
}

