
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung, John Corwin

implementation file for sashLDAPService, which makes 
calls directly to the openldap library.

*****************************************************************/

#include "sashLDAPService.h"
#include "sash_error.h"
#include "debugmsg.h"
#include <unistd.h>

static sashLDAPService * s_service = NULL;
pthread_mutex_t sashLDAPService::m_mutex;
sashILDAP *sashLDAPService::m_LDAPExtension;

void InitLDAPService(sashILDAP *ldapext) {
	 pthread_mutex_init(&sashLDAPService::m_mutex, 0);	 
	 sashLDAPService::m_LDAPExtension = ldapext;
}

sashLDAPService * GetSashLDAPService() {
	 sashLDAPService::LockMutex();
	 {
		  if (s_service == NULL){
			   DEBUGMSG(ldap, "new sashLDAPService\n");
			   s_service = new sashLDAPService;
		  }
	 }
	 sashLDAPService::UnlockMutex();
	 return s_service;
}

char ** StrVecToCharArray(const vector<string> & strings) {
	 char ** array = new char * [strings.size() + 1];
	 for (unsigned int i = 0; i < strings.size(); i++)
		  array[i] = strdup(strings[i].c_str());
	 array[strings.size()] = NULL;
	 return array;
}

void FreeCharArray(char ** array) {
	 int cur=0;
	 while (array[cur]) {
		  free(array[cur]);
		  cur++;
	 }
	 delete [] array;
}

void sashLDAPAttribute::printLDAPAttribute(){
	 string valuesStr = "";
	 vector<string>::iterator a = values.begin(),
		  b = values.end();
	 while (a != b){
		  valuesStr += (*a).c_str();
		  ++a;
		  if (a != b) valuesStr += ", ";
	 }
	 DEBUGMSG(ldap, "Attribute: name: %s, value(s): %s\n",
			  name.c_str(), valuesStr.c_str());
}

void sashLDAPEntry::printLDAPEntry(){
	 vector<sashLDAPAttribute>::iterator a = attributes.begin(),
		  b = attributes.end();
	 DEBUGMSG(ldap, "Entry: dn: %s (%d attributes)\n", dn.c_str(),
			  attributes.size());
	 while (a != b){
		  (*a).printLDAPAttribute();
		  ++a;
	 }
}

sashLDAPMessage::sashLDAPMessage(const sashLDAPMessage & m) : 
	 msgType(m.msgType),
	 msgID(m.msgID),
	 msgResult(m.msgResult),
	 entries(m.entries)
{
}

sashLDAPMessage::sashLDAPMessage(LDAP * ld, LDAPMessage * message) {
	 DEBUGMSG(ldap, "sashLDAPMessage::sashLDAPMessage()\n");
	 msgType = ldap_msgtype(message);
	 msgID = ldap_msgid(message);
	 msgResult = ldap_result2error(ld, message, 0);

	 LDAPMessage * entry = ldap_first_entry(ld, message);
	 while (entry != NULL) {
		  sashLDAPEntry e;

		  // get the entry's distinguished name
		  char * dn_cstr = ldap_get_dn(ld, entry);
		  e.dn = dn_cstr;
		  free(dn_cstr);

		  // get the entry's attributes
		  BerElement * berptr;
		  char * attr_name;
		  attr_name = ldap_first_attribute(ld, entry, &berptr);
		  while (attr_name != NULL) {
			   sashLDAPAttribute attr;
			   attr.name = attr_name;

			   // get the attribute values
			   char ** values = ldap_get_values(ld, entry, attr_name);
			   int num_values = ldap_count_values(values);

			   DEBUGMSG(ldap, "adding new attribute, name: %s, numvalues: %d\n",
						attr_name, num_values);

			   for (int i = 0; i < num_values; i++){
					DEBUGMSG(ldap, "adding value: %s\n", values[i]);
					attr.values.push_back(values[i]);
			   }
			   e.attributes.push_back(attr);
			   ldap_value_free(values);

			   // next attribute
			   attr_name = ldap_next_attribute(ld, entry, berptr);
		  }
		  ber_free(berptr, 0);  // free the attribute iterator

		  // next entry
		  entries.push_back(e);
		  entry = ldap_next_entry(ld, entry);
	 }
}

void sashLDAPMessage::printLDAPMessage()
{
	 string messagetype;
	 switch(msgType){
	 case LDAP_RES_SEARCH_ENTRY:
		  messagetype = "SEARCH_ENTRY";
		  break;
	 case LDAP_RES_SEARCH_RESULT:
		  messagetype = "SEARCH_RESULT";			   
		  break;
	 case LDAP_RES_COMPARE:
		  messagetype = "COMPARE";			   
		  break;
	 case LDAP_RES_ADD:
		  messagetype = "ADD";
		  break;
	 case LDAP_RES_DELETE:
		  messagetype = "DELETE";
		  break;
	 case LDAP_RES_MODRDN:
		  messagetype = "MODRDN";
		  break;
	 case LDAP_RES_MODIFY:
		  messagetype = "MODIFY";
		  break;
	 case LDAP_RES_BIND:
		  messagetype = "BIND";
		  break;
	 default:
		  messagetype = "UNKNOWN";
		  break;
	 }
	 DEBUGMSG(ldap, "Message: ID: %d, type: %s, result: %s (%d entries)\n",
			  msgID, messagetype.c_str(), ldap_err2string(msgResult),
			  entries.size());
	 vector<sashLDAPEntry>::iterator a = entries.begin(),
		  b=entries.end();
	 while (a != b){
		  (*a).printLDAPEntry();
		  ++a;
	 }
}

sashLDAPService::sashLDAPService()
{
	 if (pthread_create(&m_threadID, 0, ldap_start, this) != 0)
		  OutputError("The SashXB LDAP extension was unable to create a thread");
}

sashLDAPService::~sashLDAPService() {
	 pthread_mutex_destroy(&m_mutex);
}

void sashLDAPService::SetNotificationCallback(sashIActionRuntime * rt, 
											  sashIExtension * ext)
{
	 DEBUGMSG(ldap, "sashLDAPService::SetNotificationCallback\n");
	 LockMutex();
	 {
		  NS_IF_ADDREF(rt);
		  NS_IF_ADDREF(ext);
		  m_listeners.push_back(make_pair(rt, ext));
	 }
	 UnlockMutex();
}

void * sashLDAPService::ldap_start(void * data) {
	 DEBUGMSG(ldap, "sashLDAPService::ldap_start\n");
	 GetSashLDAPService()->ProcessEvents();
	 return NULL;
}

void sashLDAPService::ProcessEvents() {
	 DEBUGMSG(ldap, "sashLDAPService::ProcessEvents()\n");
	 while (true) {
		  LockMutex();
		  {
			   ServerMessageMap::iterator a = m_serverMessages.begin(), 
					b = m_serverMessages.end();
			   while (a != b) {
					DEBUGMSG(ldap, "listening for messages...\n");
					LDAPMessage * message = NULL;
					struct timeval timeout = {0, 0};
					
					assert(a->first != NULL);
					int rv = ldap_result(a->first, LDAP_RES_ANY, 0, 
										 &timeout, &message);

					if (message){
						 DEBUGMSG(ldap, 
								  "received ldap_result, rv: %d, id: %d\n", 
								  rv, ldap_msgid(message));
					}

					switch (rv) {
					case -1: // OnError
						 DEBUGMSG(ldap, "There was an LDAP error while polling for responses.\n");
						 m_LDAPExtension->CallMessageError(a->first, message);
						 break;
					case 0:
						 break;
					default:
						 DEBUGMSG(ldap, 
								  "adding new message to be processed\n");
						 sashLDAPMessage currmessage (a->first, message);
						 currmessage.printLDAPMessage();
						 a->second.push_back(currmessage);
						 DEBUGMSG(ldap, "%d messages in queue\n",
								  a->second.size());

						 // WWW: free the message?

						 // WWW: Why not call ProcessEvent every time 
						 // there's a new message?
//						 if (a->second.size() == 1) {
							  DEBUGMSG(ldap, "tell extension about new message...\n");
							  for (unsigned int i = 0; i < m_listeners.size(); i++)
								   m_listeners[i].first->ProcessEvent(m_listeners[i].second);
//						 }							  
					}
					++a;
			   }
		  }
		  UnlockMutex();
		  // WWW: change this back to 1
		  usleep(1);
//		  usleep(500000);
	 }
}

LDAP * sashLDAPService::ConnectToServer(const string & host, int port,
									 const string & user, const string & pw)
{
	 DEBUGMSG(ldap, "sashLDAPService::ConnectToServer()\n");
	 
	 LDAP * ldap = ldap_init(host.c_str(), port);

	 int result = ldap_simple_bind_s(ldap, user == "" ? NULL : user.c_str(), 
						pw == "" ? NULL : pw.c_str());

	 if (result == LDAP_SUCCESS){
		  DEBUGMSG(ldap, "ldap_simple_bind was successful.\n");
		  LockMutex();
		  {
			   DEBUGMSG(ldap, "Adding to m_serverMessages\n");
			   m_serverMessages.push_back(make_pair(ldap, vector<sashLDAPMessage>()));
		  }
		  UnlockMutex();
		  return ldap;
	 } else {
		  DEBUGMSG(ldap, "ldap_simple_bind was unsuccessful.\n");
		  DEBUGMSG(ldap, "error: %s\n", ldap_err2string(result));
		  // WWW: free ldap?
	 }

	 return NULL;

}

LDAP * look_for = NULL;
//bool match_server_pred(SingleServer server, LDAP *ldap) {
bool match_server_pred(SingleServer& server) {
	 // see if the program is alive
	 return (server.first == look_for);
}

void sashLDAPService::DisconnectFromServer(LDAP *ldap){
	 int result;
	 LockMutex();
	 {
		  // Remove it from the server messages...
		  look_for = ldap;
 		  m_serverMessages.erase(remove_if(m_serverMessages.begin(), 
										m_serverMessages.end(),
										ptr_fun(match_server_pred)), 
								 m_serverMessages.end());

		  result = ldap_unbind_s(ldap);
		  if (result == LDAP_SUCCESS){
			   DEBUGMSG(ldap, "Disconnected succesfully\n");
		  } else {
			   DEBUGMSG(ldap, "Couldn't disconnect\n");
		  }
	 }
	 UnlockMutex();
}

void sashLDAPService::GetMessages(LDAP * ldap, vector<sashLDAPMessage> & messages) {
	 DEBUGMSG(ldap, "sashLDAPService::GetMessages()\n");
	 LockMutex();
	 {
		  ServerMessageMap::iterator a = m_serverMessages.begin(), b = m_serverMessages.end();
		  while (a != b) {
			   if (a->first == ldap) {
					DEBUGMSG(ldap, "getting %d messages off the queue\n",
							 a->second.size());
					for (unsigned int i = 0; i < a->second.size(); i++)
						 messages.push_back(a->second[i]);
					a->second.clear();
			   }
			   ++a;
		  }
	 }
	 UnlockMutex();
}

LDAPMod ** sashLDAPService::CreateModArray(int op, const property_map & attributes) {
	 LDAPMod ** mods = new LDAPMod *[attributes.size() + 1];
	 mods[attributes.size()] = NULL;

	 property_map::const_iterator a = attributes.begin(), b = attributes.end();
	 int index = 0;
	 while (a != b) {
		  mods[index] = new LDAPMod;
		  mods[index]->mod_op = op;
		  mods[index]->mod_type = strdup(a->first.c_str());
		  mods[index]->mod_values = StrVecToCharArray(a->second);
		  index++;
		  a++;
	 }

	 DEBUGMSG(ldap, "Created mod array, size: %d:\n", attributes.size());
	 printModArray(mods);

	 return mods;
}

void sashLDAPService::printModArray(LDAPMod **mods){
	 int i=0;
	 LDAPMod *tempMod;
	 while ((tempMod = mods[i]) != NULL){
		  char *temp;
		  switch ((tempMod)->mod_op){
		  case LDAP_MOD_ADD:
			   DEBUGMSG(ldap, "Add: name: %s, first val: %s\n",
						tempMod->mod_type, 
						((temp = tempMod->mod_values[0]) == NULL) ? 
						"NULL" : temp);
			   break;
		  case LDAP_MOD_DELETE:
			   DEBUGMSG(ldap, "Delete: name: %s, first val: %s\n",
						tempMod->mod_type, 
						((temp = tempMod->mod_values[0]) == NULL) ? 
						"NULL" : temp);
			   break;
		  case LDAP_MOD_REPLACE:
			   DEBUGMSG(ldap, "Replace: name: %s, first val: %s\n",
						tempMod->mod_type, 
						((temp = tempMod->mod_values[0]) == NULL) ? 
						"NULL" : temp);
			   break;
		  }
		  i++;
	 }

}


int sashLDAPService::AddEntry(LDAP * ldap, const string & dn, 
							  const property_map & attributes) {
	 LDAPMod ** mods = CreateModArray(LDAP_MOD_ADD, attributes);

	 int rv;
	 LockMutex();
	 {
		  rv = ldap_add(ldap, dn.c_str(), mods);
		  // free mods
	 }
	 UnlockMutex();
	 return rv;
}

int sashLDAPService::CancelRequest(LDAP * ldap, int request) {
	 int rv = -1;
	 LockMutex();
	 {
		  if (request != -1) rv = ldap_abandon(ldap, request);
	 }
	 UnlockMutex();
	 return rv;
}

int sashLDAPService::Compare(LDAP * ldap, const string & dn, 
							 const string & attribute, const string & value) 
{
	 int rv;
	 LockMutex();
	 {
		  rv = ldap_compare(ldap, dn.c_str(), attribute.c_str(), value.c_str());
	 }
	 UnlockMutex();

	 return rv;
}

int sashLDAPService::DeleteEntry(LDAP * ldap, const string & dn) {
	 int rv;

	 LockMutex();
	 {
		  rv = ldap_delete(ldap, dn.c_str());
	 }
	 UnlockMutex();
	 return rv;
}

int sashLDAPService::RenameEntry(LDAP * ldap, const string & dn,
								 const string & newRDN, bool deleteOldRDN) 
{
	 int rv;

	 LockMutex();
	 {
		  rv = ldap_modrdn2(ldap, dn.c_str(), newRDN.c_str(), deleteOldRDN);
	 }
	 UnlockMutex();

	 return rv;
}

int sashLDAPService::Modify(LDAP * ldap, const string & dn,
							const property_map & attributes, int op)
{
	 DEBUGMSG(ldap, "sashLDAPService::Modify()\n");
	 LDAPMod ** mods = CreateModArray(op, attributes);

	 DEBUGMSG(ldap, "CreateModArray successful.\n");

	 int rv;

	 LockMutex();
	 {
		  DEBUGMSG(ldap, "Calling ldap_modify.\n");
		  rv = ldap_modify(ldap, dn.c_str(), mods);
		  DEBUGMSG(ldap, "Done callling ldap_modify.\n");
		  // free mods
	 }
	 UnlockMutex();

	 return rv;
}

int sashLDAPService::AddAttributes(LDAP * ldap, const string & dn,
									  const property_map & add_attrs)
{
	 return Modify(ldap, dn, add_attrs, LDAP_MOD_ADD);
}

int sashLDAPService::ReplaceAttributes(LDAP * ldap, const string & dn,
									  const property_map & replace_attrs)
{
	 DEBUGMSG(ldap, "sashLDAPService::ReplaceAttributes()\n");
	 return Modify(ldap, dn, replace_attrs, LDAP_MOD_REPLACE);
}

int sashLDAPService::DeleteAttributes(LDAP * ldap, const string & dn,
								   const property_map & delete_attrs)
{
	 return Modify(ldap, dn, delete_attrs, LDAP_MOD_DELETE);
}

int sashLDAPService::Search(LDAP * ldap, const string & baseDN,
							int scope, const string & filter,
							const vector<string> & attributes) 
{
	 DEBUGMSG(ldap, "sashLDAPService::Search()\n");
	 char ** attrs = StrVecToCharArray(attributes);
	 int rv;

	 LockMutex();
	 {
		  rv = ldap_search(ldap, baseDN.c_str(), scope, filter.c_str(), attrs, 0);
	 }
	 UnlockMutex();
	 FreeCharArray(attrs);
	 return rv;
}

int sashLDAPService::SetSearchMaxTime(LDAP *ldap, int *val){
	 return SetOption(ldap, LDAP_OPT_TIMELIMIT, val);
}

int sashLDAPService::SetSearchMaxResults(LDAP *ldap, int *val){
	 return SetOption(ldap, LDAP_OPT_SIZELIMIT, val);
}

int sashLDAPService::SetAliasDerefOption(LDAP *ldap, int *val){
	 return SetOption(ldap, LDAP_OPT_DEREF, val);
}

int sashLDAPService::SetOption(LDAP *ldap, int opt, int *value){
	 int rv;
	 LockMutex();
	 {
		  rv = ldap_set_option(ldap, opt, (void *)value);
	 }
	 UnlockMutex();
	 return rv;
}

// For some strange reason, setting the referral option is slightly 
// different from setting any other option. You pass a constant directly
// in to ldap_set_option rather than passing the address of a value.
int sashLDAPService::SetReferralOption(LDAP *ldap, bool referralsOn){
	 int rv;
	 LockMutex();
	 {
		  if (referralsOn)
			   rv = ldap_set_option(ldap, LDAP_OPT_REFERRALS, LDAP_OPT_ON);
		  else
			   rv = ldap_set_option(ldap, LDAP_OPT_REFERRALS, LDAP_OPT_OFF);
	 }
	 UnlockMutex();
	 return rv;
}
