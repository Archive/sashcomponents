
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung, John Corwin

*****************************************************************/

#ifndef SASHLDAPREQUEST_H
#define SASHLDAPREQUEST_H

#include "sashILDAP.h"
#include "sashIConstructor.h"
#include "sashICollection.h"
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include "sashLDAPService.h"
#include "ldap.h"
#include <string>
#include <vector>

class sashLDAPRequest : public sashIConstructor {
  public:
	NS_DECL_SASHICONSTRUCTOR
	NS_DECL_SASHILDAPREQUEST

    sashLDAPRequest();
    virtual ~sashLDAPRequest();
    
  protected:
	virtual void Initialize(PRUint32 argc, nsIVariant ** argv) {}
	void getConnectionLDAP(LDAP **ldap);
 	nsCOMPtr<sashILDAPErrorCallback> m_onError;
	LDAP * m_ldap;
	int m_ID;
	
	nsCOMPtr<sashILDAPConnection> m_connection;
	nsCOMPtr<sashIActionRuntime> m_rt;

	void collectionToPropertyMap(sashICollection *attributeCollection,
							property_map &attributeMap);
	void vectorToPropertyMap(const vector<string> &attributesVec,
							 property_map &attributeMap);


	friend class sashLDAPSearchRequest;
	friend class sashLDAPCompareRequest;
	friend class sashLDAPAddEntryRequest;
	friend class sashLDAPDeleteEntryRequest;
	friend class sashLDAPRenameEntryRequest;
	friend class sashLDAPAddAttributeRequest;
	friend class sashLDAPDeleteAttributeRequest;
	friend class sashLDAPReplaceAttributeRequest;
};

#define SASHLDAPSEARCHREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPSearchRequest;1"
#define SASHLDAPSEARCHREQUEST_CID {0xac4007f5, 0xc293, 0x47cd, {0x9e, 0xb8, 0xb6, 0x4f, 0x32, 0xf3, 0x96, 0x9d}}
NS_DEFINE_CID(ksashLDAPSEARCHREQUESTCID, SASHLDAPSEARCHREQUEST_CID);

class sashLDAPSearchRequest : public sashILDAPSearchRequest, 
							  public sashLDAPRequest{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPSEARCHREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPSearchRequest();
	 ~sashLDAPSearchRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_baseDN;
	 int m_scope;
	 string m_filter;
	 vector<string> m_attributes;

     // events
 	nsCOMPtr<sashILDAPSearchResultCallback> m_onSearchResult;
	nsCOMPtr<sashILDAPSearchCompleteCallback> m_onSearchComplete;
};


#define SASHLDAPCOMPAREREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPCompareRequest;1"
#define SASHLDAPCOMPAREREQUEST_CID {0x3d0e1eb3, 0x8ab7, 0x4608, {0x9f, 0x01, 0x75, 0xa1, 0x3d, 0x14, 0x36, 0x3b}}
NS_DEFINE_CID(ksashLDAPCOMPAREREQUESTCID, SASHLDAPCOMPAREREQUEST_CID);

class sashLDAPCompareRequest : public sashILDAPCompareRequest,
							   public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPCOMPAREREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPCompareRequest();
	 ~sashLDAPCompareRequest();
protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;
	 string m_attribute;
	 string m_value;

	 // events
	 nsCOMPtr<sashILDAPCompareCompleteCallback> m_onCompareComplete;
};


#define SASHLDAPADDENTRYREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPAddEntryRequest;1"
#define SASHLDAPADDENTRYREQUEST_CID {0x09b76b8f, 0x14d6, 0x42de, {0xab, 0x7b, 0x82, 0x1d, 0xc1, 0x3b, 0x83, 0xbc}}
NS_DEFINE_CID(ksashLDAPADDENTRYREQUESTCID, SASHLDAPADDENTRYREQUEST_CID);

class sashLDAPAddEntryRequest : public sashILDAPAddEntryRequest,
								public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPADDENTRYREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPAddEntryRequest();
	 ~sashLDAPAddEntryRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;
	 nsCOMPtr<sashICollection> m_attributes;

	 // events
	 nsCOMPtr<sashILDAPAddEntryCompleteCallback> m_onAddEntryComplete;
};



#define SASHLDAPDELETEENTRYREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPDeleteEntryRequest;1"
#define SASHLDAPDELETEENTRYREQUEST_CID {0x39546e38, 0x3336, 0x49e8, {0x84, 0xeb, 0xc3, 0x81, 0xaa, 0xae, 0x94, 0x9c}}
NS_DEFINE_CID(ksashLDAPDELETEENTRYREQUESTCID, SASHLDAPDELETEENTRYREQUEST_CID);

class sashLDAPDeleteEntryRequest : public sashILDAPDeleteEntryRequest,
								   public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPDELETEENTRYREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPDeleteEntryRequest();
	 ~sashLDAPDeleteEntryRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;

	 // events
	 nsCOMPtr<sashILDAPOperationCompleteCallback> m_onDeleteEntryComplete;
};

#define SASHLDAPRENAMEENTRYREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPRenameEntryRequest;1"
#define SASHLDAPRENAMEENTRYREQUEST_CID {0xde6e484d, 0xf519, 0x4d25, {0x8a, 0x90, 0xf5, 0x29, 0xab, 0xd4, 0x93, 0x8b}}
NS_DEFINE_CID(ksashLDAPRENAMEENTRYREQUESTCID, SASHLDAPRENAMEENTRYREQUEST_CID);

class sashLDAPRenameEntryRequest : public sashILDAPRenameEntryRequest,
								   public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPRENAMEENTRYREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPRenameEntryRequest();
	 ~sashLDAPRenameEntryRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;
	 string m_newRDN;
	 string m_newParent;
	 bool m_deleteOldRDN;

	 // events 
	 nsCOMPtr<sashILDAPOperationCompleteCallback> m_onRenameEntryComplete;
};



#define SASHLDAPADDATTRIBUTEREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPAddAttributeRequest;1"
#define SASHLDAPADDATTRIBUTEREQUEST_CID {0x279d9da8, 0x567c, 0x481f, {0x8e, 0xec, 0x0b, 0x6e, 0x9d, 0x34, 0x4c, 0x23}}
NS_DEFINE_CID(ksashLDAPADDATTRIBUTEREQUESTCID, SASHLDAPADDATTRIBUTEREQUEST_CID);

class sashLDAPAddAttributeRequest : public sashILDAPAddAttributeRequest,
									public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPADDATTRIBUTEREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPAddAttributeRequest();
	 ~sashLDAPAddAttributeRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;
	 nsCOMPtr<sashICollection> m_attributes;

	 // events
	 nsCOMPtr<sashILDAPOperationCompleteCallback> m_onAddAttributeComplete;
};


#define SASHLDAPDELETEATTRIBUTEREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPDeleteAttributeRequest;1"
#define SASHLDAPDELETEATTRIBUTEREQUEST_CID {0x47cb9016, 0x5230, 0x4f92, {0xa4, 0x37, 0x04, 0x3d, 0x6e, 0x6d, 0xcc, 0x92}}
NS_DEFINE_CID(ksashLDAPDELETEATTRIBUTEREQUESTCID, SASHLDAPDELETEATTRIBUTEREQUEST_CID);

class sashLDAPDeleteAttributeRequest : public sashILDAPDeleteAttributeRequest,
									   public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPDELETEATTRIBUTEREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPDeleteAttributeRequest();
	 ~sashLDAPDeleteAttributeRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;
	 vector<string> m_attributes;

	 // events
	 nsCOMPtr<sashILDAPOperationCompleteCallback> m_onDeleteAttributeComplete;
};

#define SASHLDAPREPLACEATTRIBUTEREQUEST_CONTRACT_ID "@gnome.org/SashXB/sashLDAPReplaceAttributeRequest;1"
#define SASHLDAPREPLACEATTRIBUTEREQUEST_CID {0xf94036f7, 0x6b61, 0x47b0, {0xb2, 0x42, 0xa0, 0x30, 0x3f, 0xe0, 0xe1, 0xb1}}

NS_DEFINE_CID(ksashLDAPREPLACEATTRIBUTEREQUESTCID, SASHLDAPREPLACEATTRIBUTEREQUEST_CID);

class sashLDAPReplaceAttributeRequest : public sashILDAPReplaceAttributeRequest,
										public sashLDAPRequest
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHILDAPREPLACEATTRIBUTEREQUEST
	 NS_FORWARD_SASHILDAPREQUEST(sashLDAPRequest::)
	 sashLDAPReplaceAttributeRequest();
	 ~sashLDAPReplaceAttributeRequest();

protected:
	 void Initialize(PRUint32 argc, nsIVariant ** argv);

	 string m_distinguishedName;
	 nsCOMPtr<sashICollection> m_attributes;

	 // events
	 nsCOMPtr<sashILDAPOperationCompleteCallback> m_onReplaceAttributeComplete;
};



#endif
