
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung, John Corwin

*****************************************************************/

#include "sashLDAPRequest.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"

using namespace std;

sashLDAPRequest::sashLDAPRequest()
{
}

sashLDAPRequest::~sashLDAPRequest()
{
}

NS_IMETHODIMP sashLDAPRequest::InitializeNewObject(sashIActionRuntime * actionRuntime, 
												   sashIExtension * ext, 
												   JSContext * cx, PRUint32 argc,
												   nsIVariant **argv, PRBool *ret)
{
	 DEBUGMSG(ldap, "sashLDAPRequest::InitializeNewObject()\n");
	 m_rt = actionRuntime;
	 m_ID = -1;

	 char *name;
	 ext->GetSashName(&name);

	 DEBUGMSG(ldap, "extension name: %s\n", name);
	 
	 m_connection = do_QueryInterface(ext);
	 if (m_connection) {
		  Initialize(argc, argv);
		  *ret = true;
		  return NS_OK;
	 }
	 else {
		  DEBUGMSG(ldap, "Unable to get the connection object!\n");
		  *ret = false;
		  return NS_ERROR_FAILURE;
	 }
}

/* attribute string OnError; */
NS_IMETHODIMP sashLDAPRequest::GetOnError(sashILDAPErrorCallback ** aOnError)
{
	 *aOnError = m_onError;
	 NS_IF_ADDREF(*aOnError);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPRequest::SetOnError(sashILDAPErrorCallback * aOnError)
{
	 m_onError = aOnError;
	 return NS_OK;
}

/* readonly attribute long ID; */
NS_IMETHODIMP sashLDAPRequest::GetID(PRInt32 *aID)
{
	 *aID = m_ID;
	 return NS_OK;
}

/* boolean Cancel (); */
NS_IMETHODIMP sashLDAPRequest::Cancel(PRBool *_retval)
{
	 DEBUGMSG(ldap, "sashLDAPRequest::Cancel()\n");
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 int retval;
	 if (ldap && LDAPService){
		  if (m_ID != -1){
			   retval = LDAPService->CancelRequest(ldap, m_ID);
			   if (retval == LDAP_SUCCESS){
					DEBUGMSG(ldap, "Request %d successfully abandoned\n",
							 m_ID);
					m_connection->RemoveOpenRequest(m_ID);
					m_ID = -1;
					*_retval = true;
			   } else {
					DEBUGMSG(ldap, "Couldn't abandon request\n");
					*_retval = false;
			   }
		  } else {
			   DEBUGMSG(ldap, "requestID was -1, didn't abandon\n");
			   *_retval = false;
		  }
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}

void sashLDAPRequest::getConnectionLDAP(LDAP **ldap){
	 nsCOMPtr <sashINativeLDAPConnection> connection 
		  = do_QueryInterface (m_connection);
	 connection->GetLDAP(ldap);
}

void sashLDAPRequest::collectionToPropertyMap(sashICollection *attributeCollection,
											  property_map &attributeMap){

	 nsCOMPtr<nsIVariant> keysVar;
	 nsCOMPtr<nsIVariant> itemVar;
	 vector<string> keysVec;
	 string itemStr;
	 vector<string> itemVec;

	 attributeCollection->GetKeys(getter_AddRefs(keysVar));
	 VariantGetArray(keysVar, keysVec);
	 
	 vector<string>::iterator a = keysVec.begin(),
		  b = keysVec.end();
	 while (a != b){
		  attributeCollection->Item((*a).c_str(), getter_AddRefs(itemVar));
		  VariantGetArray(itemVar, itemVec);
		  attributeMap[(*a)] = itemVec;
		  ++a;
	 }
} 

void sashLDAPRequest::vectorToPropertyMap(const vector<string> &attributesVec,
										  property_map &attributeMap){
	 vector<string>::const_iterator a = attributesVec.begin(),
		  b = attributesVec.end();
	 vector<string> dummy;
	 while (a != b){
		  attributeMap[(*a)] = dummy;
		  ++a;
	 }
}


/*****
 * sashLDAPSearchRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPSearchRequest, sashILDAPSearchRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPSearchRequest::sashLDAPSearchRequest()
{
	 NS_INIT_ISUPPORTS();

}

sashLDAPSearchRequest::~sashLDAPSearchRequest()
{
}

void sashLDAPSearchRequest::Initialize(PRUint32 argc, nsIVariant ** argv) {
	 DEBUGMSG(ldap, "sashLDAPSearchRequest::Initialize()\n");
	 DEBUGMSG(ldap, "%d arguments\n", argc);
	 if (argc > 0) {
		  m_baseDN = VariantGetString(argv[0]);
		  if (argc > 1) {
			   m_scope = VariantGetInteger(argv[1]);
			   if (argc > 2) {
					m_filter = VariantGetString(argv[2]);
					if (argc > 3) {
						 VariantGetArray(argv[3], m_attributes);
					}
			   }
		  }
	 }
	 DEBUGMSG(ldap, "baseDN: %s, filter: %s\n", m_baseDN.c_str(),
			  m_filter.c_str());
}

/* attribute string BaseDN; */
NS_IMETHODIMP sashLDAPSearchRequest::GetBaseDN(char * *aBaseDN)
{
	 XP_COPY_STRING(m_baseDN.c_str(), aBaseDN);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::SetBaseDN(const char * aBaseDN)
{
	 m_baseDN = aBaseDN;
	 return NS_OK;
}

/* attribute long Scope; */
NS_IMETHODIMP sashLDAPSearchRequest::GetScope(PRInt32 *aScope)
{
	 *aScope = m_scope;
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::SetScope(PRInt32 aScope)
{
	 m_scope = aScope;
	 return NS_OK;
}

/* attribute string Filter; */
NS_IMETHODIMP sashLDAPSearchRequest::GetFilter(char * *aFilter)
{
	 XP_COPY_STRING(m_filter.c_str(), aFilter);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::SetFilter(const char * aFilter)
{
	 m_filter = aFilter;
	 return NS_OK;
}

/* attribute nsIVariant Attributes; */
NS_IMETHODIMP sashLDAPSearchRequest::GetAttributes(nsIVariant * *aAttributes)
{
	 NewVariant(aAttributes);
	 if (m_attributes.size() > 0) {
		  VariantSetArray(*aAttributes, m_attributes);
	 } else {
		  VariantSetEmpty(*aAttributes);
	 }
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPSearchRequest::SetAttributes(nsIVariant * aAttributes)
{
	 m_attributes.clear();
	 VariantGetArray(aAttributes, m_attributes);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::GetOnSearchResult(sashILDAPSearchResultCallback ** aOnSearchResult)
{
	 *aOnSearchResult = m_onSearchResult;
	 NS_IF_ADDREF(*aOnSearchResult);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::SetOnSearchResult(sashILDAPSearchResultCallback * aOnSearchResult)
{
	 m_onSearchResult = aOnSearchResult;
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::GetOnSearchComplete(sashILDAPSearchCompleteCallback ** aOnSearchComplete)
{
	 *aOnSearchComplete = m_onSearchComplete;
	 NS_IF_ADDREF(*aOnSearchComplete);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPSearchRequest::SetOnSearchComplete(sashILDAPSearchCompleteCallback * aOnSearchComplete)
{
	 m_onSearchComplete = aOnSearchComplete;
	 return NS_OK;
}


/* long Invoke (); */
NS_IMETHODIMP sashLDAPSearchRequest::Invoke(PRInt32 *_retval)
{
	 DEBUGMSG(ldap, "sashLDAPSearchRequest::Invoke()\n");
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  *_retval = m_ID = 
			   LDAPService->Search(ldap, m_baseDN, m_scope, m_filter,
								   m_attributes);

		  if (m_connection){
			   DEBUGMSG(ldap, "Tracking open request through connection\n");
			   m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);
		  }else{
			   DEBUGMSG(ldap, "LDAP connection is NULL\n");
		  }
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}


/*****
 * sashLDAPCompareRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPCompareRequest, sashILDAPCompareRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPCompareRequest::sashLDAPCompareRequest()
{
	 NS_INIT_ISUPPORTS();
}

sashLDAPCompareRequest::~sashLDAPCompareRequest()
{
}

void sashLDAPCompareRequest::Initialize(PRUint32 argc, nsIVariant ** argv) 
{
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
		  if (argc > 1) {
			   m_attribute = VariantGetString(argv[1]);
			   if (argc > 2) {
					m_value = VariantGetString(argv[2]);
			   }
		  }
	 }
}

/* attribute string DistinguishedName; */
NS_IMETHODIMP sashLDAPCompareRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPCompareRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute string Attribute; */
NS_IMETHODIMP sashLDAPCompareRequest::GetAttribute(char * *aAttribute)
{
	 XP_COPY_STRING(m_attribute.c_str(), aAttribute);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPCompareRequest::SetAttribute(const char * aAttribute)
{
	 m_attribute = aAttribute;
	 return NS_OK;
}

/* attribute string Value; */
NS_IMETHODIMP sashLDAPCompareRequest::GetValue(char * *aValue)
{
	 XP_COPY_STRING(m_value.c_str(), aValue);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPCompareRequest::SetValue(const char * aValue)
{
	 m_value = aValue;
	 return NS_OK;
}

/* attribute sashILDAPCompareCompleteCallback OnCompareComplete; */
NS_IMETHODIMP sashLDAPCompareRequest::GetOnCompareComplete(sashILDAPCompareCompleteCallback * *aOnCompareComplete)
{
	 *aOnCompareComplete = m_onCompareComplete;
	 NS_IF_ADDREF(*aOnCompareComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPCompareRequest::SetOnCompareComplete(sashILDAPCompareCompleteCallback * aOnCompareComplete)
{
	 m_onCompareComplete = aOnCompareComplete;
	 return NS_OK;
}

/* long Invoke (); */
NS_IMETHODIMP sashLDAPCompareRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  *_retval = m_ID = LDAPService->Compare(ldap, m_distinguishedName,
												 m_attribute, m_value);
		  m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}


/*****
 * sashLDAPAddEntryRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPAddEntryRequest, sashILDAPAddEntryRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPAddEntryRequest::sashLDAPAddEntryRequest()
{
	 NS_INIT_ISUPPORTS();
	 NewSashCollection(getter_AddRefs(m_attributes));
}

sashLDAPAddEntryRequest::~sashLDAPAddEntryRequest()
{
}

void sashLDAPAddEntryRequest::Initialize(PRUint32 argc, nsIVariant ** argv)
{
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
		  if (argc > 1){
			   nsISupports *temp = VariantGetInterface(argv[1]);
			   m_attributes = do_QueryInterface(temp);
		  }
	 }
}

/* attribute string DistinguishedName; */
NS_IMETHODIMP sashLDAPAddEntryRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPAddEntryRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute sashICollection Attributes; */
NS_IMETHODIMP sashLDAPAddEntryRequest::GetAttributes(sashICollection * *aAttributes)
{
	 *aAttributes = m_attributes;
	 NS_ADDREF(*aAttributes);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPAddEntryRequest::SetAttributes(sashICollection * aAttributes)
{
	 m_attributes = aAttributes;
	 return NS_OK;
}

/* attribute sashILDAPAddEntryCompleteCallback OnAddEntryComplete; */
NS_IMETHODIMP sashLDAPAddEntryRequest::GetOnAddEntryComplete(sashILDAPAddEntryCompleteCallback * *aOnAddEntryComplete)
{
	 *aOnAddEntryComplete = m_onAddEntryComplete;
	 NS_ADDREF(*aOnAddEntryComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPAddEntryRequest::SetOnAddEntryComplete(sashILDAPAddEntryCompleteCallback * aOnAddEntryComplete)
{
	 m_onAddEntryComplete = aOnAddEntryComplete;
	 return NS_OK;
}

/* long Invoke (); */
NS_IMETHODIMP sashLDAPAddEntryRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  property_map attributes;
		  collectionToPropertyMap(m_attributes, attributes);
		  *_retval = m_ID = LDAPService->AddEntry(ldap, m_distinguishedName,
												  attributes);
		  m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);

	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}

/*****
 * sashLDAPDeleteEntryRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPDeleteEntryRequest, sashILDAPDeleteEntryRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPDeleteEntryRequest::sashLDAPDeleteEntryRequest()
{
	 NS_INIT_ISUPPORTS();
}

sashLDAPDeleteEntryRequest::~sashLDAPDeleteEntryRequest()
{
}

void sashLDAPDeleteEntryRequest::Initialize(PRUint32 argc, nsIVariant ** argv) 
{
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
	 }
}

/* attribute string DistinguishedName; */
NS_IMETHODIMP sashLDAPDeleteEntryRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPDeleteEntryRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute sashILDAPOperationCompleteCallback OnDeleteEntryComplete; */
NS_IMETHODIMP sashLDAPDeleteEntryRequest::GetOnDeleteEntryComplete(sashILDAPOperationCompleteCallback * *aOnDeleteEntryComplete)
{
	 *aOnDeleteEntryComplete = m_onDeleteEntryComplete;
	 NS_ADDREF(*aOnDeleteEntryComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPDeleteEntryRequest::SetOnDeleteEntryComplete(sashILDAPOperationCompleteCallback * aOnDeleteEntryComplete)
{
	 m_onDeleteEntryComplete = aOnDeleteEntryComplete;
	 return NS_OK;
}

/* long Invoke (); */
NS_IMETHODIMP sashLDAPDeleteEntryRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  *_retval = m_ID = 
			   LDAPService->DeleteEntry(ldap, m_distinguishedName);
		  m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}


/*****
 * sashLDAPRenameEntryRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPRenameEntryRequest, sashILDAPRenameEntryRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPRenameEntryRequest::sashLDAPRenameEntryRequest()
{
	 NS_INIT_ISUPPORTS();
}

sashLDAPRenameEntryRequest::~sashLDAPRenameEntryRequest()
{
}

void sashLDAPRenameEntryRequest::Initialize(PRUint32 argc, nsIVariant ** argv) {
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
		  if (argc > 1) {
			   m_newRDN = VariantGetString(argv[1]);
			   if (argc > 2) {
					m_newParent = VariantGetString(argv[2]);
					if (argc > 3) {
						 m_deleteOldRDN = VariantGetBoolean(argv[3]);
					}
			   }
		  }
	 }
}

/* attribute string DistinguishedName; */
NS_IMETHODIMP sashLDAPRenameEntryRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPRenameEntryRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute string NewRDN; */
NS_IMETHODIMP sashLDAPRenameEntryRequest::GetNewRDN(char * *aNewRDN)
{
	 XP_COPY_STRING(m_newRDN.c_str(), aNewRDN);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPRenameEntryRequest::SetNewRDN(const char * aNewRDN)
{
	 m_newRDN = aNewRDN;
	 return NS_OK;
}

/* attribute string NewParent; */
NS_IMETHODIMP sashLDAPRenameEntryRequest::GetNewParent(char * *aNewParent)
{
	 XP_COPY_STRING(m_newParent.c_str(), aNewParent);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPRenameEntryRequest::SetNewParent(const char * aNewParent)
{
	 m_newParent = aNewParent;
	 return NS_OK;
}

/* attribute boolean DeleteOldRDN; */
NS_IMETHODIMP sashLDAPRenameEntryRequest::GetDeleteOldRDN(PRBool *aDeleteOldRDN)
{
	 *aDeleteOldRDN = m_deleteOldRDN;
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPRenameEntryRequest::SetDeleteOldRDN(PRBool aDeleteOldRDN)
{
	 m_deleteOldRDN = aDeleteOldRDN;
	 return NS_OK;
}

/* attribute sashILDAPOperationCompleteCallback OnRenameEntryComplete; */
NS_IMETHODIMP sashLDAPRenameEntryRequest::GetOnRenameEntryComplete(sashILDAPOperationCompleteCallback * *aOnRenameEntryComplete)
{
	 *aOnRenameEntryComplete = m_onRenameEntryComplete;
	 NS_ADDREF(*aOnRenameEntryComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPRenameEntryRequest::SetOnRenameEntryComplete(sashILDAPOperationCompleteCallback * aOnRenameEntryComplete)
{
	 m_onRenameEntryComplete = aOnRenameEntryComplete;
	 return NS_OK;
}

/* long Invoke (); */
NS_IMETHODIMP sashLDAPRenameEntryRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  *_retval = m_ID = 
			   LDAPService->RenameEntry(ldap, m_distinguishedName,
										m_newRDN, m_deleteOldRDN);
		  m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}



/*****
 * sashLDAPAddAttributeRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPAddAttributeRequest, 
					  sashILDAPAddAttributeRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPAddAttributeRequest::sashLDAPAddAttributeRequest()
{
	 NS_INIT_ISUPPORTS();
	 NewSashCollection(getter_AddRefs(m_attributes));
}

sashLDAPAddAttributeRequest::~sashLDAPAddAttributeRequest()
{
}

void sashLDAPAddAttributeRequest::Initialize(PRUint32 argc, nsIVariant ** argv) {
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
		  if (argc > 1){
			   nsISupports *temp = VariantGetInterface(argv[1]);
			   m_attributes = do_QueryInterface(temp);
		  }
	 }
}

NS_IMETHODIMP sashLDAPAddAttributeRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPAddAttributeRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute sashICollection Attributes; */
NS_IMETHODIMP sashLDAPAddAttributeRequest::GetAttributes(sashICollection * *aAttributes)
{
	 *aAttributes = m_attributes;
	 NS_ADDREF(*aAttributes);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPAddAttributeRequest::SetAttributes(sashICollection * aAttributes)
{
	 m_attributes = aAttributes;
	 return NS_OK;
}

/* attribute sashILDAPOperationCompleteCallback OnAddAttributeComplete; */
NS_IMETHODIMP sashLDAPAddAttributeRequest::GetOnAddAttributeComplete(sashILDAPOperationCompleteCallback * *aOnAddAttributeComplete)
{
	 *aOnAddAttributeComplete = m_onAddAttributeComplete;
	 NS_ADDREF(*aOnAddAttributeComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPAddAttributeRequest::SetOnAddAttributeComplete(sashILDAPOperationCompleteCallback * aOnAddAttributeComplete)
{
	 m_onAddAttributeComplete = aOnAddAttributeComplete;
	 return NS_OK;
}


/* long Invoke (); */
NS_IMETHODIMP sashLDAPAddAttributeRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  property_map attributes;
		  collectionToPropertyMap(m_attributes, attributes);
		  *_retval = m_ID = 
			   LDAPService->AddAttributes(ldap, m_distinguishedName,
										  attributes);
		  m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}


/*****
 * sashLDAPDeleteAttributeRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPDeleteAttributeRequest, 
					  sashILDAPDeleteAttributeRequest,
					  sashILDAPRequest, sashIConstructor);

sashLDAPDeleteAttributeRequest::sashLDAPDeleteAttributeRequest()
{
	 NS_INIT_ISUPPORTS();
}

sashLDAPDeleteAttributeRequest::~sashLDAPDeleteAttributeRequest()
{
}

void sashLDAPDeleteAttributeRequest::Initialize(PRUint32 argc, nsIVariant ** argv) {
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
		  if (argc > 1) {
			   VariantGetArray(argv[1], m_attributes);
		  }
	 }
}

NS_IMETHODIMP sashLDAPDeleteAttributeRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPDeleteAttributeRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute nsIVariant Attributes; */
NS_IMETHODIMP sashLDAPDeleteAttributeRequest::GetAttributes(nsIVariant * *aAttributes)
{
	 NewVariant(aAttributes);
	 if (m_attributes.size() > 0) {
		  VariantSetArray(*aAttributes, m_attributes);
	 } else {
		  VariantSetEmpty(*aAttributes);
	 }
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPDeleteAttributeRequest::SetAttributes(nsIVariant * aAttributes)
{
	 m_attributes.clear();
	 VariantGetArray(aAttributes, m_attributes);
	 return NS_OK;
}


/* attribute sashILDAPOperationCompleteCallback OnDeleteAttributeComplete; */
NS_IMETHODIMP sashLDAPDeleteAttributeRequest::GetOnDeleteAttributeComplete(sashILDAPOperationCompleteCallback * *aOnDeleteAttributeComplete)
{
	 *aOnDeleteAttributeComplete = m_onDeleteAttributeComplete;
	 NS_ADDREF(*aOnDeleteAttributeComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAPDeleteAttributeRequest::SetOnDeleteAttributeComplete(sashILDAPOperationCompleteCallback * aOnDeleteAttributeComplete)
{
	 m_onDeleteAttributeComplete = aOnDeleteAttributeComplete;
	 return NS_OK;
}

/* long Invoke (); */
NS_IMETHODIMP sashLDAPDeleteAttributeRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  property_map attributes;
		  vectorToPropertyMap(m_attributes, attributes);
		  *_retval = m_ID = 
			   LDAPService->DeleteAttributes(ldap, m_distinguishedName,
											 attributes);
		  m_connection->AddOpenRequest(m_ID, (sashILDAPRequest *)this);
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}


/*****
 * sashLDAPReplaceAttributeRequest
 *****/

NS_IMPL_ISUPPORTS3_CI(sashLDAPReplaceAttributeRequest, 
					  sashILDAPReplaceAttributeRequest,
					  sashILDAPRequest, sashIConstructor)

sashLDAPReplaceAttributeRequest::sashLDAPReplaceAttributeRequest()
{
	 NS_INIT_ISUPPORTS();
	 NewSashCollection(getter_AddRefs(m_attributes));
}

sashLDAPReplaceAttributeRequest::~sashLDAPReplaceAttributeRequest()
{
}

void sashLDAPReplaceAttributeRequest::Initialize(PRUint32 argc, nsIVariant ** argv) {
	 if (argc > 0) {
		  m_distinguishedName = VariantGetString(argv[0]);
		  if (argc > 1){
			   nsISupports *temp = VariantGetInterface(argv[1]);
			   m_attributes = do_QueryInterface(temp);
		  }
	 }
}

NS_IMETHODIMP sashLDAPReplaceAttributeRequest::GetDistinguishedName(char * *aDistinguishedName)
{
	 XP_COPY_STRING(m_distinguishedName.c_str(), aDistinguishedName);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPReplaceAttributeRequest::SetDistinguishedName(const char * aDistinguishedName)
{
	 m_distinguishedName = aDistinguishedName;
	 return NS_OK;
}

/* attribute sashICollection Attributes; */
NS_IMETHODIMP sashLDAPReplaceAttributeRequest::GetAttributes(sashICollection * *aAttributes)
{
	 *aAttributes = m_attributes;
	 NS_ADDREF(*aAttributes);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPReplaceAttributeRequest::SetAttributes(sashICollection * aAttributes)
{
	 m_attributes = aAttributes;
	 return NS_OK;
}

/* attribute sashILDAPOperationCompleteCallback OnReplaceAttributeComplete; */
NS_IMETHODIMP sashLDAPReplaceAttributeRequest::GetOnReplaceAttributeComplete(sashILDAPOperationCompleteCallback * *aOnReplaceAttributeComplete)
{
	 *aOnReplaceAttributeComplete = m_onReplaceAttributeComplete;
	 NS_ADDREF(*aOnReplaceAttributeComplete);
	 return NS_OK;
}

NS_IMETHODIMP sashLDAPReplaceAttributeRequest::SetOnReplaceAttributeComplete(sashILDAPOperationCompleteCallback * aOnReplaceAttributeComplete)
{
	 m_onReplaceAttributeComplete = aOnReplaceAttributeComplete;
	 return NS_OK;
}

/* long Invoke (); */
NS_IMETHODIMP sashLDAPReplaceAttributeRequest::Invoke(PRInt32 *_retval)
{
	 LDAP *ldap; getConnectionLDAP(&ldap);
	 sashLDAPService *LDAPService = GetSashLDAPService();
	 if (ldap && LDAPService){
		  property_map attributes;
		  collectionToPropertyMap(m_attributes, attributes);
		  *_retval = m_ID = 
			   LDAPService->ReplaceAttributes(ldap, m_distinguishedName,
											  attributes);
		  m_connection->AddOpenRequest((PRInt32)m_ID, 
									   (sashILDAPRequest *)this);
	 } else {
		  DEBUGMSG(ldap, "No LDAP object or no LDAP service\n");
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}

