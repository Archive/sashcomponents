
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):

*****************************************************************/

#include "sashIGenericConstructor.h"
#include "sashLDAP.h"
#include "extensiontools.h"
#include "sashLDAPConnection.h"

#include <gmodule.h>

using namespace std;

SASH_EXTENSION_IMPL_NO_NSGET(sashLDAP, sashILDAP, "LDAP");

sashLDAP::sashLDAP() : m_rt(NULL)
{
  NS_INIT_ISUPPORTS();
  pthread_mutex_init(&m_connectionsLock, 0);
}

sashLDAP::~sashLDAP() 
{
	 pthread_mutex_destroy(&m_connectionsLock);
}

NS_IMETHODIMP sashLDAP::ProcessEvent() {
	 DEBUGMSG(ldap, "sashLDAP::ProcessEvent()\n");
	 LockConnections();
	 for (unsigned int i = 0; i < m_connections.size(); i++) {
		  m_connections[i]->ProcessMessages();
	 }
	 UnlockConnections();
	 return NS_OK;
}

NS_IMETHODIMP sashLDAP::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP sashLDAP::Initialize(sashIActionRuntime * act,
								   const char *registrationXml, const char *webGuid,
								   JSContext *cx, JSObject *obj) 
{
	 DEBUGMSG(ldap, "sashLDAP::Initialize()\n");
	 m_rt = act;
	 m_cx = cx;

	 string sashPath;
	 XP_GET_STRING(act->GetSashDirectory, sashPath);

	 sashPath += "/";
	 sashPath += ToUpper(ksashLDAPCID.ToString());
	 sashPath += "/data";

	 char * module_path = g_module_build_path(sashPath.c_str(), "ldap");
	 GModule * ldapModule = g_module_open(module_path, (GModuleFlags)0);
	 if (ldapModule)
		  g_module_make_resident(ldapModule);
	 else
		  OutputError("%s", g_module_error());

	 InitLDAPService(this);

	 NewSashConstructor(m_rt, this, SASHLDAPCONNECTION_CONTRACT_ID,
						SASHILDAPCONNECTION_IID_STR, 
						getter_AddRefs(m_ConnectionConstructor));

	 sashLDAPService * service = GetSashLDAPService();
	 if (service){
		  DEBUGMSG(ldap, "Got sashLDAPService.\n");
		  service->SetNotificationCallback(m_rt, this);
	 } else {
		  DEBUGMSG(ldap, "Couldn't get sashLDAPService.\n");
	 }

	 return NS_OK;
}

NS_IMETHODIMP sashLDAP::RegisterConnection(sashILDAPConnection * con) {
	 DEBUGMSG(ldap, "sashLDAP::RegisterConnection()\n");
	 if (con) {
		  NS_ADDREF(con);
		  LockConnections();
		  m_connections.push_back(con);
		  DEBUGMSG(ldap, "%d connections registered\n", m_connections.size());
		  UnlockConnections();
		  return NS_OK;
	 }
	 return NS_ERROR_NULL_POINTER;
}

NS_IMETHODIMP sashLDAP::UnregisterConnection(sashILDAPConnection * con) {
	 DEBUGMSG(ldap, "sashLDAP::UnregisterConnection()\n");
	 if (con) {
		  LockConnections();
		  vector<sashILDAPConnection *>::iterator a = remove(m_connections.begin(), m_connections.end(), con);
		  m_connections.erase(a, m_connections.end());
		  DEBUGMSG(ldap, "%d connections registered\n", m_connections.size());
		  UnlockConnections();
		  NS_IF_RELEASE(con);
		  return NS_OK;
	 }
	 return NS_ERROR_NULL_POINTER;
}

/* readonly attribute long SCOPE_BASE; */
NS_IMETHODIMP sashLDAP::GetSCOPE_BASE(PRInt32 *aSCOPE_BASE)
{
	 *aSCOPE_BASE = LDAP_SCOPE_BASE;
	 return NS_OK;
}

/* readonly attribute long SCOPE_ONELEVEL; */
NS_IMETHODIMP sashLDAP::GetSCOPE_ONELEVEL(PRInt32 *aSCOPE_ONELEVEL)
{
	 *aSCOPE_ONELEVEL = LDAP_SCOPE_ONELEVEL;
	 return NS_OK;
}

/* readonly attribute long SCOPE_SUBTREE; */
NS_IMETHODIMP sashLDAP::GetSCOPE_SUBTREE(PRInt32 *aSCOPE_SUBTREE)
{
	 *aSCOPE_SUBTREE = LDAP_SCOPE_SUBTREE;
	 return NS_OK;
}

/* readonly attribute long ALIAS_DEREF_NEVER; */
NS_IMETHODIMP sashLDAP::GetALIAS_DEREF_NEVER(PRInt32 *aALIAS_DEREF_NEVER)
{
	 *aALIAS_DEREF_NEVER = LDAP_DEREF_NEVER;
	 return NS_OK;
}

/* readonly attribute long ALIAS_DEREF_FINDING; */
NS_IMETHODIMP sashLDAP::GetALIAS_DEREF_FINDING(PRInt32 *aALIAS_DEREF_FINDING)
{
	 *aALIAS_DEREF_FINDING = LDAP_DEREF_FINDING;
	 return NS_OK;
}

/* readonly attribute long ALIAS_DEREF_SEARCHING; */
NS_IMETHODIMP sashLDAP::GetALIAS_DEREF_SEARCHING(PRInt32 *aALIAS_DEREF_SEARCHING)
{
	 *aALIAS_DEREF_SEARCHING = LDAP_DEREF_SEARCHING;
	 return NS_OK;
}

/* readonly attribute long ALIAS_DEREF_ALWAYS; */
NS_IMETHODIMP sashLDAP::GetALIAS_DEREF_ALWAYS(PRInt32 *aALIAS_DEREF_ALWAYS)
{
	 *aALIAS_DEREF_ALWAYS = LDAP_DEREF_ALWAYS;
	 return NS_OK;
}


NS_IMETHODIMP sashLDAP::GetConnection(sashIGenericConstructor ** connection) {
	 DEBUGMSG(ldap, "sashLDAP::GetConnection()\n");
	 *connection = m_ConnectionConstructor;
	 NS_ADDREF(*connection);
	 return NS_OK;
}

/* attribute string OnError; */
NS_IMETHODIMP sashLDAP::GetOnError(sashILDAPErrorCallback ** onError)
{
	 *onError = m_onError;
	 NS_IF_ADDREF(*onError);
	 return NS_OK;
}
NS_IMETHODIMP sashLDAP::SetOnError(sashILDAPErrorCallback * aOnError)
{
	 m_onError = aOnError;
	 return NS_OK;
}

/* [noscript] void CallMessageError (in nativeLDAP LDAP, in nativeLDAPMessage message); */
NS_IMETHODIMP sashLDAP::CallMessageError(LDAP * LDAP, LDAPMessage *message)
{
	 DEBUGMSG(ldap, "sashLDAP::CallMessageError()\n");
	 PRBool match;
	 sashILDAPConnection *conn = NULL;
	 LockConnections();
	 {
		  vector<sashILDAPConnection *>::iterator a = m_connections.begin(),
			   b = m_connections.end();
		  while ((a != b) && (conn == NULL)){
			   // match LDAP server.
			   (*a)->MatchLDAPServer(LDAP, &match);
			   if (match) {
					conn = (*a);
			   }
			   ++a;
		  }
	 }
	 UnlockConnections();
	 if (conn){
		  conn->CallMessageError(message);
	 } else {
		  DEBUGMSG(ldap, "Error called, but not associated with a particular connection\n");
		  
		  int msgResult = ldap_result2error(LDAP, message, 0);
		  string desc = ldap_err2string(msgResult);

		  if (m_onError)
			   m_onError->Callback(desc.c_str());

		  // WWW: clean up the message?
	 }
	 return NS_OK;
}
