
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHLDAP_H
#define SASHLDAP_H

#include "sashILDAP.h"
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include <string>
#include <vector>

class sashIGenericConstructor;

#define SASHLDAP_CONTRACT_ID "@gnome.org/SashXB/sashLDAP;1"
#define SASHLDAP_CID {0x98BF4159, 0xB9D7, 0x48D7, {0x96, 0x25, 0x92, 0xE3, 0x3F, 0xF1, 0x42, 0x06}}

NS_DEFINE_CID(ksashLDAPCID, SASHLDAP_CID);

class sashLDAP: public sashILDAP, public sashIExtension {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHIEXTENSION
    NS_DECL_SASHILDAP

    sashLDAP();
    virtual ~sashLDAP();
    
  protected:
    nsCOMPtr<sashIActionRuntime> m_rt;
	JSContext * m_cx;
	nsCOMPtr<sashIGenericConstructor> m_ConnectionConstructor;
	nsCOMPtr<sashIGenericConstructor> m_EntryConstructor;

	pthread_mutex_t m_connectionsLock;
	void LockConnections() { pthread_mutex_lock(&m_connectionsLock); }
	void UnlockConnections() { pthread_mutex_unlock(&m_connectionsLock); }
	vector<sashILDAPConnection *> m_connections;

	nsCOMPtr<sashILDAPErrorCallback> m_onError;
};

#endif
