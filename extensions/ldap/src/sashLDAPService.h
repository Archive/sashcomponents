
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung, John Corwin

implementation file for sashLDAPService, which makes 
calls directly to the openldap library.

*****************************************************************/

#ifndef SASH_LDAP_SERVICE
#define SASH_LDAP_SERVICE

#include "sashIActionRuntime.h"
#include "sashILDAP.h"
#include <vector>
#include <string>
#include <hash_map>
#include <pthread.h>
#include <ldap.h>

struct sashLDAPAttribute {
	 string name;
	 vector<string> values;
	 void printLDAPAttribute();
};

struct sashLDAPEntry {
	 string dn;
	 vector<sashLDAPAttribute> attributes;
	 void printLDAPEntry();
};

struct sashLDAPMessage {
	 int msgType;
	 int msgID;
	 int msgResult;
	 vector<sashLDAPEntry> entries;

	 sashLDAPMessage(const sashLDAPMessage & m);
	 sashLDAPMessage(LDAP * ld, LDAPMessage * message);
	 sashLDAPMessage(){}
	 void printLDAPMessage();
};

// code to make a hashtable with string keys
struct strhash {
	 static hash<const char*> h;
	 size_t operator()(const string& s) const {
		  return h(s.c_str());
	 }
};
hash<const char*> strhash::h;
typedef hash_map<string, vector<string>, strhash> property_map;

//typedef hash_map<LDAP *, vector<sashLDAPMessage> > ServerMessageMap;
typedef pair<LDAP *, vector<sashLDAPMessage> > SingleServer;
typedef vector<SingleServer> ServerMessageMap;

class sashLDAPService;

sashLDAPService * GetSashLDAPService();
void InitLDAPService(sashILDAP *ldapext);

class sashLDAPService {
protected:
	 static pthread_mutex_t m_mutex;
	 static sashILDAP *m_LDAPExtension;

	 ServerMessageMap m_serverMessages;
	 vector<pair<sashIActionRuntime *, sashIExtension *> > m_listeners;

	 pthread_t m_threadID;

	 static void * ldap_start(void * data);
	 void ProcessEvents();

	 // Modify: used for AddAttributes, ReplaceAttributes, and DeleteAttributes
	 int Modify(LDAP * ldap, const string & dn,
				const property_map & attributes, int op);

	 LDAPMod ** CreateModArray(int op, const property_map & attributes);
	 void printModArray(LDAPMod **mods);
	 int SetOption(LDAP *ldap, int optType, int *value);
	 
public:
	 sashLDAPService();
	 ~sashLDAPService();

	 static void LockMutex() { pthread_mutex_lock(&m_mutex); }
	 static void UnlockMutex() { pthread_mutex_unlock(&m_mutex); }

	 LDAP * ConnectToServer(const string & host, int port,
						 const string & user, const string & pw);
	 void DisconnectFromServer(LDAP *ldap);

	 void GetMessages(LDAP * ldap, vector<sashLDAPMessage> & messages);

	 int AddEntry(LDAP * ldap, const string & dn, 
				  const property_map & attributes);
	 int CancelRequest(LDAP * ldap, int request);
	 int Compare(LDAP * ldap, const string & dn, 
				 const string & attribute, const string & value);
	 int DeleteEntry(LDAP * ldap, const string & dn);

	 int AddAttributes(LDAP * ldap, const string & dn,
					   const property_map & add_attrs);

	 int ReplaceAttributes(LDAP * ldap, const string & dn,
						  const property_map & replace_attrs);

	 int DeleteAttributes(LDAP * ldap, const string & dn,
						  const property_map & delete_attrs);

	 int RenameEntry(LDAP * ldap, const string & dn,
					 const string & newRDN, bool deleteOldRDN);
	 int Search(LDAP * ldap, const string & baseDN,
				int scope, const string & filter,
				const vector<string> & attributes);
	 int SetSearchMaxTime(LDAP *ldap, int *val);
	 int SetSearchMaxResults(LDAP *ldap, int *val);
	 int SetAliasDerefOption(LDAP *ldap, int *val);
	 int SetReferralOption(LDAP *ldap, bool referralsOn);

	 void SetNotificationCallback(sashIActionRuntime * rt, sashIExtension * ext);

	 friend void InitLDAPService(sashILDAP *ldapext);
	 friend sashLDAPService * GetSashLDAPService();
};

#endif
