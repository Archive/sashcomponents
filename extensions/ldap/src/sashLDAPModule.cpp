
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142

    USA

*****************************************************************

Contributor(s): Wing Yung

module for the sashLDAP
******************************************************************/

#include "sashLDAP.h"
#include "sashLDAPConnection.h"
#include "sashLDAPRequest.h"
#include "extensiontools.h"

NS_DECL_CLASSINFO(sashLDAP);
NS_DECL_CLASSINFO(sashLDAPConnection);
NS_DECL_CLASSINFO(sashLDAPSearchRequest);
NS_DECL_CLASSINFO(sashLDAPCompareRequest);
NS_DECL_CLASSINFO(sashLDAPAddEntryRequest);
NS_DECL_CLASSINFO(sashLDAPDeleteEntryRequest);
NS_DECL_CLASSINFO(sashLDAPRenameEntryRequest);
NS_DECL_CLASSINFO(sashLDAPAddAttributeRequest);
NS_DECL_CLASSINFO(sashLDAPDeleteAttributeRequest);
NS_DECL_CLASSINFO(sashLDAPReplaceAttributeRequest);

NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAP);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPConnection);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPSearchRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPCompareRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPAddEntryRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPDeleteEntryRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPRenameEntryRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPAddAttributeRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPDeleteAttributeRequest);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashLDAPReplaceAttributeRequest);

static nsModuleComponentInfo sashLDAP_components[] = {
  MODULE_COMPONENT_ENTRY(sashLDAP, SASHLDAP, "SashXB LDAP Extension"),
  MODULE_COMPONENT_ENTRY(sashLDAPConnection, SASHLDAPCONNECTION, "LDAP Connection object"),
  MODULE_COMPONENT_ENTRY(sashLDAPSearchRequest, SASHLDAPSEARCHREQUEST, "LDAP SearchRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPCompareRequest, SASHLDAPCOMPAREREQUEST, "LDAP CompareRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPAddEntryRequest, SASHLDAPADDENTRYREQUEST, "LDAP AddEntryRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPDeleteEntryRequest, SASHLDAPDELETEENTRYREQUEST, "LDAP DeleteEntryRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPRenameEntryRequest, SASHLDAPRENAMEENTRYREQUEST, "LDAP RenameEntryRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPAddAttributeRequest, SASHLDAPADDATTRIBUTEREQUEST, "LDAP AddAttributeRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPDeleteAttributeRequest, SASHLDAPDELETEATTRIBUTEREQUEST, "LDAP DeleteAttributeRequest object"),
  MODULE_COMPONENT_ENTRY(sashLDAPReplaceAttributeRequest, SASHLDAPREPLACEATTRIBUTEREQUEST, "LDAP ReplaceAttributeRequest object"),
};

NS_IMPL_NSGETMODULE(sashLDAP, sashLDAP_components);
