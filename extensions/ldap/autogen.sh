#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="SashXB-LDAP"

. ../../prepare-build.sh
. $srcdir/macros/autogen.sh
