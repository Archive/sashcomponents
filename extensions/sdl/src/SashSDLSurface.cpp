/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the SDL Surface object

*****************************************************************/

#include "extensiontools.h"
#include "SashSDLSurface.h"
#include "sashVariantUtils.h"
#include "SDL.h"

NS_IMPL_ISUPPORTS1_CI(SashSDLSurface, sashISDLSurface);

SashSDLSurface::SashSDLSurface() {
	 NS_INIT_ISUPPORTS();
	 m_Surface = NULL;
	 m_IsScreen = true;
	 m_TransparencyColor = 0;
}

SashSDLSurface::~SashSDLSurface() {
	 if (m_Surface && ! m_IsScreen)
		  SDL_FreeSurface(m_Surface);
}

NS_IMETHODIMP SashSDLSurface::SetSurface(SDL_Surface* surface, PRBool isScreen) {
	 m_Surface = surface;
	 m_IsScreen = isScreen;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetSurface(SDL_Surface** surface) {
	 *surface = m_Surface;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetWidth(PRInt32* _retval) {
	 *_retval = m_Surface->w;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetHeight(PRInt32* _retval) {
	 *_retval = m_Surface->h;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetBpp(PRInt32* _retval) {
	 *_retval = m_Surface->format->BitsPerPixel;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetAlpha(PRUint8* _retval) {
	 *_retval = m_Surface->format->alpha;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::SetAlpha(PRUint8 alpha) {
	 m_Surface->format->alpha = alpha;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::Draw(sashISDLSurface* dest, PRInt32 x, PRInt32 y) {
	 SDL_Surface* destination;
	 SDL_Rect rect;
	 rect.x = x;
	 rect.y = y;
	 dest->GetSurface(&destination);
	 SDL_BlitSurface(m_Surface, NULL, destination, &rect);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::DrawRect(PRInt32 x, PRInt32 y, PRInt32 w, PRInt32 h, 
								   sashISDLSurface* dest, PRInt32 destx, PRInt32 desty) {
	 SDL_Surface* destination;
	 SDL_Rect destrect, srcrect;
	 srcrect.x = x;
	 srcrect.y = y;
	 srcrect.w = w;
	 srcrect.h = h;
	 destrect.x = destx;
	 destrect.y = desty;
	 dest->GetSurface(&destination);
	 SDL_BlitSurface(m_Surface, &srcrect, destination, &destrect);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::UpdateRect(PRInt32 x, PRInt32 y, PRInt32 w, PRInt32 h) {
	 SDL_UpdateRect(m_Surface, x, y, w, h);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::Flip() {
	 SDL_Flip(m_Surface);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::SaveBMP(const char* filename) {
	 SDL_SaveBMP(m_Surface, filename);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::FillRect(PRInt32 x, PRInt32 y, PRInt32 w, 
										 PRInt32 h, PRUint32 color) {
	 SDL_Rect rect;
	 rect.x = x;
	 rect.y = y;
	 rect.w = w;
	 rect.h = h;
	 SDL_FillRect(m_Surface, &rect, color);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetColor(PRUint8 r, PRUint8 g, PRUint8 b, 
									   PRUint8 a, PRUint32* _retval) {
	 *_retval = SDL_MapRGBA(m_Surface->format, r, g, b, a);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetTransparencyColor(PRUint32* _retval) {
	 *_retval = m_TransparencyColor;
	 return NS_OK;
}


NS_IMETHODIMP SashSDLSurface::SetTransparencyColor(PRUint32 color) {
	 m_TransparencyColor = color;
	 SetUseTransparency(m_UseTransparency);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::SetUseTransparency(PRBool use) {
	 m_UseTransparency = use;
	 if (use) {
		  SDL_SetColorKey(m_Surface, SDL_SRCCOLORKEY, m_TransparencyColor);
	 } else {
		  SDL_SetColorKey(m_Surface, 0, 0);
	 }
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetUseTransparency(PRBool* _retval) {
	 *_retval = m_UseTransparency;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::SetUseAlpha(PRBool use) {
	 m_UseAlpha = use;
	 if (use) {
		  m_Surface->flags |= SDL_SRCALPHA;
	 } else {
		  m_Surface->flags &= ~SDL_SRCALPHA;
	 }
	 return NS_OK;
}

NS_IMETHODIMP SashSDLSurface::GetUseAlpha(PRBool* _retval) {
	 *_retval = (m_Surface->flags & SDL_SRCALPHA);
	 return NS_OK;
}
