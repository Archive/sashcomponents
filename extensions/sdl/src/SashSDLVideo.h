/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the SDL Video object

*****************************************************************/

#ifndef SASHSDLVIDEO_H
#define SASHSDLVIDEO_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashISDL.h"
#include "sashIExtension.h"
#include "secman.h"

#include <string>

#define SASHSDLVIDEO_CID {0xf63634a1, 0x5c91, 0x4250, {0x86, 0x14, 0x5c, 0x56, 0x97, 0xce, 0x7d, 0x3c}}
    
NS_DEFINE_CID(kSashSDLVideoCID, SASHSDLVIDEO_CID);

#define SASHSDLVIDEO_CONTRACT_ID "@gnome.org/SashXB/SDLVideo;1"

class SashSDLVideo : public sashISDLVideo {
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHISDLVIDEO
  
  SashSDLVideo();
  virtual ~SashSDLVideo();
 private:

  sashIGenericConstructor* m_AnimationConstructor;
};

#endif
