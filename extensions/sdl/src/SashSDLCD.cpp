/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the SDL CD object

*****************************************************************/

#include "extensiontools.h"
#include "SashSDLCD.h"
#include "SashSDLCDROM.h"
#include "sashVariantUtils.h"
#include "SDL.h"

NS_IMPL_ISUPPORTS1_CI(SashSDLCD, sashISDLCD);

SashSDLCD::SashSDLCD() {
	 NS_INIT_ISUPPORTS();
	 // make sure that the CD subsystem has been initialized
	 Uint32 was_init = SDL_WasInit(SDL_INIT_CDROM);
	 if (! was_init && SDL_Init(SDL_INIT_CDROM) == -1)
		  OutputError("Unable to initialize SDL CDROM!");
}

SashSDLCD::~SashSDLCD() {
}

NS_IMETHODIMP SashSDLCD::GetNumDrives(PRInt32 *_retval) {
	 *_retval = SDL_CDNumDrives();
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCD::DriveName(PRInt32 drive, char** _retval) {
	 XP_COPY_STRING(SDL_CDName(drive), _retval);
	 return NS_OK;
}


/* sashISDLCDROM Open (in long drive); */
NS_IMETHODIMP SashSDLCD::Open(PRInt32 drive, sashISDLCDROM **_retval) {
	 if (drive < 0 || drive >= SDL_CDNumDrives()) {
		  OutputError("Trying to open nonexistent CDROM drive!");
	 }
	 nsresult res = nsComponentManager::CreateInstance(kSashSDLCDROMCID, NULL, 
												NS_GET_IID(sashISDLCDROM),
												(void **) _retval);
	 assert(NS_SUCCEEDED(res));
	 NS_ADDREF(*_retval);
	 (*_retval)->SetDrive(drive);
	 return NS_OK;

}
