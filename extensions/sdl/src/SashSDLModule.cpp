// include all of your implementation header files here
#include "SashSDL.h"
#include "SashSDLCD.h"
#include "SashSDLCDROM.h"
#include "SashSDLVideo.h"
#include "SashSDLSurface.h"
#include "SashSDLAnimation.h"

#include "extensiontools.h"

// we need one each of these for each object we're exposing -- 
// including the base extension itself
NS_DECL_CLASSINFO(SashSDL);
NS_DECL_CLASSINFO(SashSDLCD);
NS_DECL_CLASSINFO(SashSDLCDROM);
NS_DECL_CLASSINFO(SashSDLVideo);
NS_DECL_CLASSINFO(SashSDLSurface);
NS_DECL_CLASSINFO(SashSDLAnimation);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashSDL);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashSDLCD);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashSDLCDROM);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashSDLVideo);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashSDLSurface);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashSDLAnimation);

static nsModuleComponentInfo components[] = {
	 MODULE_COMPONENT_ENTRY(SashSDL, SASHSDL, "Simple DirectMedia Layer Extension"),
	 MODULE_COMPONENT_ENTRY(SashSDLCD, SASHSDLCD, "SDL CD Object"),
	 MODULE_COMPONENT_ENTRY(SashSDLCDROM, SASHSDLCDROM, "SDL CDROM Object"), 
	 MODULE_COMPONENT_ENTRY(SashSDLVideo, SASHSDLVIDEO, "SDL Video Object"),
	 MODULE_COMPONENT_ENTRY(SashSDLSurface, SASHSDLSURFACE, "SDL Surface Object"),
	 MODULE_COMPONENT_ENTRY(SashSDLAnimation, SASHSDLANIMATION, "SDL Animation Object")
};

NS_IMPL_NSGETMODULE(SashSDL, components);


