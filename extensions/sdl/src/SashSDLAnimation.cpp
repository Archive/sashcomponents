/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the SDL Animation object

*****************************************************************/

#include "extensiontools.h"
#include "SashSDLAnimation.h"
#include "sashVariantUtils.h"
#include "SDL.h"
#include "SashSDLSurface.h"
#include "SDL_image/SDL_image.h"
#include <vector>
#include <string>
#include <numeric>

NS_IMPL_ISUPPORTS2_CI(SashSDLAnimation, sashISDLAnimation, sashIConstructor);

SashSDLAnimation::SashSDLAnimation() {
	 NS_INIT_ISUPPORTS();
	 m_Screen = NULL;
	 m_TicksPerFrame = 10;
	 m_IsAnimating = false;
	 m_X = 0; m_Y = 0;
	 m_StartAnimation = 0;
	 m_CurrentFrameIndex = 0;
	 m_DelayTotal = 0;
	 m_LastUpdate = 0;
}

SashSDLAnimation::~SashSDLAnimation() {
	 for(unsigned int i = 0 ; i < m_Frames.size() ; i++) {
		  NS_IF_RELEASE(m_Frames[i]);
	 }
}

NS_IMETHODIMP SashSDLAnimation::InitializeNewObject(
	 sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
	 PRUint32 argc, nsIVariant **argv, PRBool *ret) {
	 // only want an array of filenames, or nothing
	 if (argc == 1) {
		  InitFramesFromFilenames(argv[0]);
	 }
	 *ret = true;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::InitFramesFromFilenames(nsIVariant* filenames) {
	 vector<sashISDLSurface*>::iterator ai = m_Frames.begin(), bi = m_Frames.end();
	 while (ai != bi) {
		  NS_IF_RELEASE(*ai);
		  ++ai;
	 }
	 m_Frames.clear();
	 vector<string> f;
	 VariantGetArray(filenames, f);
	 vector<string>::iterator ais = f.begin(), bis = f.end();
	 while (ais != bis) {
		  sashISDLSurface* surface;
		  SDL_Surface* s_init = IMG_Load(ais->c_str());
		  SDL_Surface* s = SDL_DisplayFormat(s_init);
		  SDL_FreeSurface(s_init);
		  if (s) {
			   nsresult res = 
					nsComponentManager::CreateInstance(kSashSDLSurfaceCID, NULL, 
													   NS_GET_IID(sashISDLSurface),
													   (void **) &surface);
			   assert(NS_SUCCEEDED(res));
			   surface->SetSurface(s, false);
			   m_Frames.push_back(surface);
		  }
		  ++ais;
	 }
	 if (m_Delays.size() < m_Frames.size()) {
		  m_Delays.insert(m_Delays.end(), m_Frames.size() - m_Delays.size(), 1.);
	 }
	 m_DelayTotal = accumulate(m_Delays.begin(), m_Delays.begin()+m_Frames.size(), 0.);

	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::GetFrames(nsIVariant** _retval) {
	 NewVariant(_retval);
	 vector<nsISupports*> v;
	 for(unsigned int i = 0 ; i < m_Frames.size() ; i++) {
		  NS_ADDREF(m_Frames[i]);
		  v.push_back((nsISupports*) m_Frames[i]);
	 }
	 
	 VariantSetArray(*_retval, v);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::SetFrames(nsIVariant* _retval) {
	 for(unsigned int i = 0 ; i < m_Frames.size() ; i++) {
		  NS_IF_RELEASE(m_Frames[i]);
	 }
	 m_Frames.clear();
	 vector<nsISupports*> v;
	 VariantGetArray(_retval, v);
	 vector<nsISupports*>::iterator ai = v.begin(), bi = v.end();
	 while (ai != bi) {
		  nsresult rv;
		  sashISDLSurface* s;
		  rv = (*ai)->QueryInterface(NS_GET_IID(sashISDLSurface), (void**) &s);
		  if (NS_SUCCEEDED(rv)) {
			   NS_ADDREF(s);
			   m_Frames.push_back(s);
		  }
		  ++ai;
	 }
	 if (m_Delays.size() < m_Frames.size()) {
		  m_Delays.insert(m_Delays.end(), m_Frames.size() - m_Delays.size(), 1);
	 }
	 m_DelayTotal = accumulate(m_Delays.begin(), m_Delays.begin()+m_Frames.size(), 0.);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::GetCurrentFrameIndex(PRUint32* _retval) {
	 *_retval = m_CurrentFrameIndex;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::SetCurrentFrameIndex(PRUint32 _retval) {
	 _retval %= m_Frames.size();
	 m_CurrentFrameIndex = _retval;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::GetCurrentFrame(sashISDLSurface** _retval) {
	 *_retval = m_Frames[m_CurrentFrameIndex];
	 NS_ADDREF(*_retval);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::GetTicksPerFrame(PRUint32* _retval) {
	 *_retval = m_TicksPerFrame;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::SetTicksPerFrame(PRUint32 ticksperframe) {
	 m_TicksPerFrame = ticksperframe;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::GetIsAnimating(PRBool* _retval) {
	 *_retval = m_IsAnimating;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::SetIsAnimating(PRBool is_animating) {
	 m_IsAnimating = is_animating;
	 if (m_IsAnimating) {
		  // retroactively set it
		  m_StartAnimation = SDL_GetTicks() - (unsigned int)
			   accumulate(m_Delays.begin(), m_Delays.begin() + m_CurrentFrameIndex, 0.) 
			   * m_TicksPerFrame;
	 }
	 return NS_OK;
}


NS_IMETHODIMP SashSDLAnimation::Update() {
	 if (! m_IsAnimating)
		  return NS_OK;
	 // seems complicated but produces decent results across many situations

	 long ticks = SDL_GetTicks();
	 unsigned long left = (long) m_Delays[m_CurrentFrameIndex] * m_TicksPerFrame;
	 // make sure there's something to do
	 if (ticks - m_LastUpdate > left){
		  m_LastUpdate = ticks;
		  int nf = (m_CurrentFrameIndex + 1) % m_Frames.size();
		  if (ticks - m_LastUpdate < left + m_Delays[nf] * m_TicksPerFrame) {
			   m_CurrentFrameIndex = nf;
		  } else {
			   m_CurrentFrameIndex = 0;
			   float diff = (ticks - m_StartAnimation) % 
					(int) ((float) m_TicksPerFrame * m_DelayTotal);
			   vector<float>::iterator a = m_Delays.begin();
			   while ((diff -= *a * m_TicksPerFrame) > 0.) {
					++a;
					++m_CurrentFrameIndex;
			   }
		  }
	 }
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::Draw(sashISDLSurface* dest, PRInt32 x, PRInt32 y) {
	 SDL_Surface* destination;
	 SDL_Rect rect;
	 rect.x = x;
	 rect.y = y;
	 dest->GetSurface(&destination);
	 SDL_Surface* src;
	 m_Frames[m_CurrentFrameIndex]->GetSurface(&src);
	 SDL_BlitSurface(src, NULL, destination, &rect);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::GetDelay(PRUint32 frame, float* _retval) {
	 *_retval = m_Delays[frame];
	 return NS_OK;
}

NS_IMETHODIMP SashSDLAnimation::SetDelay(PRUint32 frame, float delay) {
	 m_Delays[frame] = delay;
	 m_DelayTotal = accumulate(m_Delays.begin(), m_Delays.begin()+m_Frames.size(), 0.);
	 return NS_OK;
}
