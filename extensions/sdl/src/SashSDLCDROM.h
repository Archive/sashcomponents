/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the SDL CDROM object

*****************************************************************/

#ifndef SASHSDLCDROM_H
#define SASHSDLCDROM_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashISDL.h"
#include "sashIExtension.h"
#include "secman.h"
#include "SDL.h"
#include <string>

#define SASHSDLCDROM_CID {0xafeef68c, 0x34aa, 0x448b, {0xad, 0xc0, 0xca, 0x5b, 0x50, 0x49, 0xf2, 0xce}}
    
NS_DEFINE_CID(kSashSDLCDROMCID, SASHSDLCDROM_CID);

#define SASHSDLCDROM_CONTRACT_ID "@gnome.org/SashXB/SDLCDROM;1"

class SashSDLCDROM : public sashISDLCDROM {
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHISDLCDROM
  
  SashSDLCDROM();
  virtual ~SashSDLCDROM();
 private:
  
  SDL_CD* m_Drive;
  signed long m_EndPosition;
};

#endif
