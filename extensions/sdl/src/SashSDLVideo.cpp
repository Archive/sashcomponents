/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the SDL.Video object

*****************************************************************/

#include "extensiontools.h"
#include "SashSDLVideo.h"
#include "SashSDLSurface.h"
#include "SashSDLAnimation.h"
#include "sashVariantUtils.h"
#include "sashIGenericConstructor.h"
#include "SDL.h"
#include "SDL_image/SDL_image.h"

NS_IMPL_ISUPPORTS1_CI(SashSDLVideo, sashISDLVideo);

SashSDLVideo::SashSDLVideo() {
	 NS_INIT_ISUPPORTS();
	 // make sure that the CD subsystem has been initialized
	 Uint32 was_init = SDL_WasInit(SDL_INIT_VIDEO);
	 if (! was_init && SDL_Init(SDL_INIT_VIDEO) == -1)
		  OutputError("Unable to initialize SDL Video!");
	 NewSashConstructor(NULL, NULL, SASHSDLANIMATION_CONTRACT_ID, 
						SASHISDLANIMATION_IID_STR, &m_AnimationConstructor);

}

SashSDLVideo::~SashSDLVideo() {
  NS_RELEASE(m_AnimationConstructor);

}

NS_IMETHODIMP SashSDLVideo::GetShowCursor(PRBool* _retval) {
	 *_retval = (SDL_ShowCursor(SDL_QUERY) == SDL_ENABLE);
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::SetShowCursor(PRBool cursor) {
	 SDL_ShowCursor(cursor ? SDL_ENABLE : SDL_DISABLE);
	 return NS_OK;
};


NS_IMETHODIMP SashSDLVideo::GetTicks(PRUint32* _retval) {
	 *_retval = SDL_GetTicks();
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetSWSURFACE(PRUint32* _retval) {
	 *_retval = SDL_SWSURFACE;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetHWSURFACE(PRUint32* _retval) {
	 *_retval = SDL_HWSURFACE;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetDOUBLEBUF(PRUint32* _retval) {
	 *_retval = SDL_DOUBLEBUF;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetFULLSCREEN(PRUint32* _retval) {
	 *_retval = SDL_FULLSCREEN;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetRESIZABLE(PRUint32* _retval) {
	 *_retval = SDL_RESIZABLE;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetNOFRAME(PRUint32* _retval) {
	 *_retval = SDL_NOFRAME;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::VideoModeOK(PRInt32 width, PRInt32 height, PRInt32 bpp, PRUint32 flags, PRBool* _retval) {
	 *_retval = SDL_VideoModeOK(width, height, bpp, flags) != 0;
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::SetVideoMode(PRInt32 width, PRInt32 height, PRInt32 bpp, PRUint32 flags, PRBool* _retval) {
	 *_retval = (SDL_SetVideoMode(width, height, bpp, flags) != NULL);
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::GetVideoSurface(sashISDLSurface** _retval) {
	 nsresult res = nsComponentManager::CreateInstance(kSashSDLSurfaceCID, NULL, 
												NS_GET_IID(sashISDLSurface),
												(void **) _retval);
	 assert(NS_SUCCEEDED(res));
	 (*_retval)->SetSurface(SDL_GetVideoSurface(), true);
	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::CreateRGBSurface(PRInt32 width, PRInt32 height, PRInt32 bpp, 
											 PRUint32 flags, sashISDLSurface** _retval) {
	 SDL_Surface* s_init = SDL_CreateRGBSurface(flags, width, height, bpp, 0, 0, 0, 0);
	 SDL_Surface* s = SDL_DisplayFormat(s_init);
	 SDL_FreeSurface(s_init);

	 nsresult res = nsComponentManager::CreateInstance(kSashSDLSurfaceCID, NULL, 
												NS_GET_IID(sashISDLSurface),
												(void **) _retval);
	 assert(NS_SUCCEEDED(res));
	 (*_retval)->SetSurface(s, false);

	 return NS_OK;
};

NS_IMETHODIMP SashSDLVideo::LoadImage(const char* file, sashISDLSurface** _retval) {
	 SDL_Surface* s_init = IMG_Load(file);
	 SDL_Surface* s = SDL_DisplayFormat(s_init);
	 SDL_FreeSurface(s_init);
	 if (s) {
		  nsresult res = nsComponentManager::CreateInstance(kSashSDLSurfaceCID, NULL, 
															NS_GET_IID(sashISDLSurface),
															(void **) _retval);
		  assert(NS_SUCCEEDED(res));
		  (*_retval)->SetSurface(s, false);
	 } else {
		  *_retval = NULL;
	 }
	 return NS_OK;
};


NS_IMETHODIMP
SashSDLVideo::GetAnimation(sashIGenericConstructor **ret)
{
  NS_ADDREF(m_AnimationConstructor);
	 *ret = m_AnimationConstructor;
	 return NS_OK;
}
