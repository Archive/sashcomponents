/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the SDL Surface object

*****************************************************************/

#ifndef SASHSDLSURFACE_H
#define SASHSDLSURFACE_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashISDL.h"
#include "secman.h"
#include "SDL.h"
#include <string>

#define SASHSDLSURFACE_CID {0xbec5c371, 0xe226, 0x4b7a, {0x92, 0x38, 0x87, 0x96, 0x85, 0xc8, 0xe0, 0x71}}
    
NS_DEFINE_CID(kSashSDLSurfaceCID, SASHSDLSURFACE_CID);

#define SASHSDLSURFACE_CONTRACT_ID "@gnome.org/SashXB/SDLSurface;1"

class SashSDLSurface : public sashISDLSurface {
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHISDLSURFACE
  
  SashSDLSurface();
  virtual ~SashSDLSurface();
  
 private:
  SDL_Surface* m_Surface;
  bool m_IsScreen;
  PRUint32 m_TransparencyColor;
  bool m_UseTransparency, m_UseAlpha;
};

#endif
