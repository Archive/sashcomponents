#include <string>
#include <vector>
#include <iostream>
#include "xpcomtools.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "security.h"
#include "sash_constants.h"
#include "sashIActionRuntime.h"
#include "SashSDL.h"
#include "SashSDLCD.h"
#include "SashSDLVideo.h"
#include "sashIGenericConstructor.h"
#include "SDL.h"

SASH_EXTENSION_IMPL_NO_NSGET(SashSDL, sashISDL, "SDL");

SashSDL::SashSDL() {
  NS_INIT_ISUPPORTS();
  m_Video = NULL;
//  m_Audio = NULL;
//  m_WM = NULL;
  m_CD = NULL;
//  m_Joystick = NULL;
}

SashSDL::~SashSDL() {
//  NS_IF_RELEASE(m_Video);
//  NS_IF_RELEASE(m_Audio);
//  NS_IF_RELEASE(m_WM);
  NS_IF_RELEASE(m_CD);
//  NS_IF_RELEASE(m_Joystick);
}

NS_IMETHODIMP SashSDL::Initialize(sashIActionRuntime *act,
                                   const char *xml,
                                   const char *instanceGuid, 
                                   JSContext *context, 
                                   JSObject *object) {
  act->GetSecurityManager(&m_pSecurityManager);
  m_pActionRuntime = act;
  m_pContext = context;
  m_pObject = object;
  m_Guid = instanceGuid;
  m_Registration = xml;
  atexit(SDL_Quit);
  return NS_OK;
}

NS_IMETHODIMP SashSDL::ProcessEvent() {
  return NS_OK;
}

NS_IMETHODIMP SashSDL::Cleanup() {
	 return NS_OK;
}


NS_IMETHODIMP SashSDL::GetCD(sashISDLCD ** cd) {
  nsresult retval;

  if (m_CD == NULL) {
	retval = nsComponentManager::CreateInstance(kSashSDLCDCID, NULL, 
												NS_GET_IID(sashISDLCD),
												(void **) &m_CD);
	assert(NS_SUCCEEDED(retval));
  }

  NS_ADDREF(m_CD);
  *cd = m_CD;
  return NS_OK;
}

NS_IMETHODIMP SashSDL::GetVideo(sashISDLVideo ** video) {
  nsresult retval;

  if (m_Video == NULL) {
	retval = nsComponentManager::CreateInstance(kSashSDLVideoCID, NULL, 
												NS_GET_IID(sashISDLVideo),
												(void **) &m_Video);
	assert(NS_SUCCEEDED(retval));
  }
  NS_ADDREF(m_Video);
  *video = m_Video;
  return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnActive(char * *aOnActive) {
	 XP_COPY_STRING(m_OnActive.c_str(), aOnActive);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnActive(const char * aOnActive) {
	 m_OnActive = aOnActive;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnKey(char * *aOnKey) {
	 XP_COPY_STRING(m_OnKey.c_str(), aOnKey);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnKey(const char * aOnKey) {
	 m_OnKey = aOnKey;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnMotion(char * *aOnMotion) {
	 XP_COPY_STRING(m_OnMotion.c_str(), aOnMotion);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnMotion(const char * aOnMotion) {
	 m_OnMotion = aOnMotion;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnButton(char * *aOnButton) {
	 XP_COPY_STRING(m_OnButton.c_str(), aOnButton);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnButton(const char * aOnButton) {
	 m_OnButton = aOnButton;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnResize(char * *aOnResize) {
	 XP_COPY_STRING(m_OnResize.c_str(), aOnResize);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnResize(const char * aOnResize) {
	 m_OnResize = aOnResize;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnExpose(char * *aOnExpose) {
	 XP_COPY_STRING(m_OnExpose.c_str(), aOnExpose);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnExpose(const char * aOnExpose) {
	 m_OnExpose = aOnExpose;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnUserEvent(char * *aOnUserEvent) {
	 XP_COPY_STRING(m_OnUserEvent.c_str(), aOnUserEvent);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnUserEvent(const char * aOnUserEvent) {
	 m_OnUserEvent = aOnUserEvent;
	 return NS_OK;
}

NS_IMETHODIMP SashSDL::GetOnSysWM(char * *aOnSysWM) {
	 XP_COPY_STRING(m_OnSysWM.c_str(), aOnSysWM);
	 return NS_OK;
}
NS_IMETHODIMP SashSDL::SetOnSysWM(const char * aOnSysWM) {
	 m_OnSysWM = aOnSysWM;
	 return NS_OK;
}
