#ifndef SASHSDL_H
#define SASHSDL_H

#include "nsID.h"
#include "sashISDL.h"
#include "sashIExtension.h"
#include "secman.h"
#include <string>

//{5C3523CD-D82C-412C-BFAD-E47D58D0039E}
#define SASHSDL_CID \
{0x5C3523CD, 0xD82C, 0x412C, {0xBF, 0xAD, 0xE4, 0x7D, 0x58, 0xD0, 0x03, 0x9E}}

NS_DEFINE_CID(kSashSDLCID, SASHSDL_CID);

#define SASHSDL_CONTRACT_ID "@gnome.org/SashXB/SDL;1"

class SashSDL : public sashISDL,
				public sashIExtension {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHISDL

  SashSDL();
  virtual ~SashSDL();
  
private:
  // the runtime security manager -- we'll need this later
  sashISecurityManager* m_pSecurityManager;

  // the JSContext, needed for callbacks
  JSContext* m_pContext;

  // also needed for callbacks
  sashIActionRuntime* m_pActionRuntime;

  string m_Registration, m_Guid;
  JSObject* m_pObject;

  sashISDLVideo* m_Video;
//  sashISDLAudio* m_Audio;
//  sashISDLWM* m_WM;
  sashISDLCD* m_CD;
//  sashISDLJoystick* m_Joystick;

  string m_OnActive;
  string m_OnKey;
  string m_OnMotion;
  string m_OnButton;
  string m_OnResize;
  string m_OnExpose;
  string m_OnUserEvent;
  string m_OnSysWM;

};

#endif // SASHSDL_H


