/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the SDL CDROM object

*****************************************************************/

#include "extensiontools.h"
#include "SashSDLCDROM.h"
#include "sashVariantUtils.h"
#include "SDL.h"

NS_IMPL_ISUPPORTS1_CI(SashSDLCDROM, sashISDLCDROM);

SashSDLCDROM::SashSDLCDROM() {
	 NS_INIT_ISUPPORTS();
	 m_Drive = NULL;
	 m_EndPosition = 0;
}

SashSDLCDROM::~SashSDLCDROM() {
	 SDL_CDClose(m_Drive);
}

NS_IMETHODIMP SashSDLCDROM::GetCD_TRAYEMPTY(PRInt32 *_retval) {
	 *_retval =  CD_TRAYEMPTY;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetCD_STOPPED(PRInt32 *_retval) {
	 *_retval =  CD_STOPPED;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetCD_PLAYING(PRInt32 *_retval) {
	 *_retval =  CD_PLAYING;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetCD_PAUSED(PRInt32 *_retval) {
	 *_retval =  CD_PAUSED;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetCD_ERROR(PRInt32 *_retval) {
	 *_retval =  CD_ERROR;
	 return NS_OK;
}


NS_IMETHODIMP SashSDLCDROM::GetStatus(PRInt32 *_retval) {
	 *_retval =  SDL_CDStatus(m_Drive);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetNumTracks(PRInt32 *_retval) {
	 SDL_CDStatus(m_Drive);
	 *_retval =  m_Drive->numtracks;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetCurTrack(PRInt32 *_retval) {
	 SDL_CDStatus(m_Drive);
	 *_retval =  m_Drive->cur_track;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetCurTrackPosition(PRInt32 *_retval) {
	 SDL_CDStatus(m_Drive);
	 *_retval =  m_Drive->cur_frame / CD_FPS;
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::TrackIsAudio(PRInt32 track, PRBool *_retval) {
	 
	 *_retval =  (track >= 0 && track < m_Drive->numtracks ? 
				  m_Drive->track[track].type == SDL_AUDIO_TRACK :
				  false);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::TrackLength(PRInt32 track, PRInt32 *_retval) {
	 
	 *_retval =  (track >= 0 && track < m_Drive->numtracks ? 
				  m_Drive->track[track].length / CD_FPS :
				  0);
	 return NS_OK;
}


NS_IMETHODIMP SashSDLCDROM::TrackOffset(PRInt32 track, PRInt32 *_retval) {
	 *_retval =  (track >= 0 && track < m_Drive->numtracks ? 
				  m_Drive->track[track].offset / CD_FPS :
				  0);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::SetDrive(PRInt32 drive) {
	 m_Drive = SDL_CDOpen(drive);
	 if (m_Drive == NULL) {
		  OutputError("Could not open drive %d!", drive);
	 }
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::Pause() {
	 SDL_CDPause(m_Drive);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::Resume() {
	 SDL_CDResume(m_Drive);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::Stop() {
	 SDL_CDStop(m_Drive);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::Eject() {
	 SDL_CDEject(m_Drive);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::GetDiscID(char** _retval) {
	 SDL_CDStatus(m_Drive);
	 unsigned int trackhash = 0;
	 for (int i = 0 ; i < m_Drive->numtracks ; i++) {
		  unsigned int offset = m_Drive->track[i].offset;
		  int min, sec, frame;
		  FRAMES_TO_MSF(offset, &min, &sec, &frame);
		  offset = min*60 + sec;
		  while (offset > 0) {
			   trackhash += (offset % 10);
			   offset /= 10;
		  }
	 }
	 unsigned int total_seconds = (m_Drive->track[m_Drive->numtracks].offset - m_Drive->track[0].offset) / CD_FPS;
	 unsigned int final = ((trackhash % 0xff) << 24 | total_seconds << 8 | m_Drive->numtracks);
	 char out[16];
	 snprintf(out, 15, "%08x", final);
	 XP_COPY_STRING(out, _retval);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::Advance(PRInt32 seconds) {
	 SDL_CDStatus(m_Drive);
	 if (m_EndPosition == 0) 
		  m_EndPosition = m_Drive->track[m_Drive->numtracks-1].offset + m_Drive->track[m_Drive->numtracks-1].length;

	 SDL_CDPlay(m_Drive, m_Drive->track[m_Drive->cur_track].offset + m_Drive->cur_frame + seconds * CD_FPS, 
				m_EndPosition);
	 return NS_OK;
}

NS_IMETHODIMP SashSDLCDROM::Play(PRInt32 start_track, PRInt32 start_seconds,
			   PRInt32 num_tracks_to_play, PRInt32 end_seconds, PRBool* _retval) {
	 if (start_track < 0 || num_tracks_to_play > m_Drive->numtracks - start_track) {
		  *_retval = false;
	 } else {
		  *_retval = SDL_CDPlayTracks(m_Drive, start_track, start_seconds * CD_FPS, 
									  num_tracks_to_play, end_seconds * CD_FPS) == 0;
		  // compute finish
		  m_EndPosition = (num_tracks_to_play == 0 && end_seconds == 0) ? 0 : 
			   m_Drive->track[start_track + num_tracks_to_play].offset + end_seconds * CD_FPS;
	 }

	 return NS_OK;
}
