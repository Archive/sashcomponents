/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the SDL Animation object

*****************************************************************/

#ifndef SASHSDLANIMATION_H
#define SASHSDLANIMATION_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashISDL.h"
#include "secman.h"
#include "SDL.h"
#include "sashIConstructor.h"
#include <vector>

#define SASHSDLANIMATION_CID {0xfe22c545, 0x26e9, 0x4852, {0x8a, 0xd3, 0x2c, 0x8f, 0x11, 0x9d, 0xaf, 0x2d}}
    
NS_DEFINE_CID(kSashSDLAnimationCID, SASHSDLANIMATION_CID);

#define SASHSDLANIMATION_CONTRACT_ID "@gnome.org/SashXB/SDLAnimation;1"

class SashSDLAnimation : public sashISDLAnimation,
						 public sashIConstructor {
 public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHICONSTRUCTOR;
	 NS_DECL_SASHISDLANIMATION;
  
  SashSDLAnimation();
  virtual ~SashSDLAnimation();
  
 private:
  vector<sashISDLSurface*> m_Frames;
  vector<float> m_Delays;
  SDL_Surface* m_Screen;
  
  PRUint32 m_CurrentFrameIndex;
  PRUint32 m_TicksPerFrame;
  bool m_IsAnimating;
  PRInt32 m_X, m_Y;
  PRUint32 m_StartAnimation, m_LastUpdate;
  float m_DelayTotal;
};

#endif
