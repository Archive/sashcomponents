
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashXMLDocument.h"
#include "sashXMLNode.h"
#include "extensiontools.h"
#include <string>
#include <vector>
#include "sashVariantUtils.h"
#include "sashIXMLExtension.h"

NS_IMPL_ISUPPORTS2_CI(sashXMLDocument, sashIXPXMLDocument, sashIConstructor)

sashXMLDocument::sashXMLDocument() {
  NS_INIT_ISUPPORTS();
}

NS_IMETHODIMP sashXMLDocument::InitializeNewObject(sashIActionRuntime * actionRuntime, 
												   sashIExtension *parent, 
												   JSContext *cx, 
												   PRUint32 argc, nsIVariant **argv, 
												   PRBool *_retval) 
{
  DEBUGMSG(xmlext, "sashxmldocument initializenewobject\n");
  m_rt = actionRuntime;
  m_rt->GetSecurityManager(&m_pSecurityManager);

  if (argc!= 0)
    return NS_OK;
  nsCOMPtr<sashIXMLExtension> dad = do_QueryInterface(parent);
  if (!dad)
	   OutputError("sashXMLDocument must be created by the XML extension!\n");
  XP_GET_STRING(dad->GetBasePath, m_base_path);

  m_xd = NewSashXPXMLDocument();
  *_retval= true;
  return NS_OK;
}

/* void loadFromFile (in string file_name); */
NS_IMETHODIMP 
sashXMLDocument::LoadFromFile(const char *file_name, PRBool *_retval) 
{
	 if (file_name == NULL) 
		  *_retval = false;
	 else {
		  string path;
		  if (! g_path_is_absolute(file_name)) {
			   path = m_base_path + file_name;
		  } else {
			   path = file_name;
		  }
		  m_rt->AssertFSAccess(path.c_str());

		  return m_xd->LoadFromFile(path.c_str(), _retval);
	 }
	 return NS_OK;
}

/* void saveToFile (in string file_name); */
NS_IMETHODIMP 
sashXMLDocument::SaveToFile(const char *file_name, PRBool *_retval) 
{
	 if (file_name == NULL) 
		  *_retval = false;
	 else {
		  string path;
		  if (! g_path_is_absolute(file_name)) {
			   path = m_base_path + file_name;
		  } else {
			   path = file_name;
		  }
		  m_rt->AssertFSAccess(path.c_str());

		  return m_xd->SaveToFile(path.c_str(), _retval);
	 }
	 return NS_OK;
}
