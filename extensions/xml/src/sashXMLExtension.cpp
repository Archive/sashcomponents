
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashIGenericConstructor.h"
#include "sashXMLExtension.h"
#include "sashXMLNode.h"
#include "sashXMLDocument.h"
#include "sashXMLIterator.h"
#include "extensiontools.h"

using namespace std;

SASH_EXTENSION_IMPL_NO_NSGET(sashXMLExtension, sashIXMLExtension, "XML");

sashXMLExtension::sashXMLExtension() : m_act(NULL),
									   DocumentConstructor(NULL),
									   IteratorConstructor(NULL)
{
  NS_INIT_ISUPPORTS();
}

sashXMLExtension::~sashXMLExtension() {
  NS_IF_RELEASE(DocumentConstructor);
  NS_IF_RELEASE(IteratorConstructor);
  NS_IF_RELEASE(m_act);
}

NS_IMETHODIMP sashXMLExtension::ProcessEvent() {
  return NS_OK;
}
NS_IMETHODIMP 
sashXMLExtension::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP sashXMLExtension::Initialize(sashIActionRuntime * act,
					   const char *registrationXml, const char *webGuid,
					   JSContext *cx, JSObject *obj) {
  m_act= act;
  NS_ADDREF(m_act);
  char *str= NULL;
  if (NS_FAILED(act->GetDataDirectory(&str)))
    return NS_OK;
  if (str) {
    base_dir= str;
    nsCRT::free(str);
  }
  else
    base_dir= "";
  if ((base_dir.length()> 0) && (base_dir[base_dir.length()- 1]!= '/'))
    base_dir+= "/";

  NewSashConstructor(m_act, this, SASHXMLDOCUMENT_CONTRACT_ID, SASHIXPXMLDOCUMENT_IID_STR, &DocumentConstructor);
  NewSashConstructor(m_act, this, SASHXMLITERATOR_CONTRACT_ID, SASHIXPXMLITERATOR_IID_STR, &IteratorConstructor);

  return NS_OK;
}

NS_IMETHODIMP sashXMLExtension::GetDocument(sashIGenericConstructor **aDocument) {
  *aDocument= DocumentConstructor;
  NS_ADDREF(*aDocument);
  return NS_OK;
}

NS_IMETHODIMP sashXMLExtension::GetIterator(sashIGenericConstructor **aIterator) {
  *aIterator= IteratorConstructor;
  NS_ADDREF(*aIterator);
  return NS_OK;
}

NS_IMETHODIMP sashXMLExtension::GetBasePath(char **_retval) {
  XP_COPY_STRING(base_dir, _retval);
  return NS_OK;
}

NS_DECL_CLASSINFO(sashXMLExtension);
NS_DECL_CLASSINFO(sashXMLDocument);
NS_DECL_CLASSINFO(sashXMLNode);
NS_DECL_CLASSINFO(sashXMLIterator);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashXMLExtension);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashXMLNode);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashXMLIterator);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashXMLDocument);

static nsModuleComponentInfo sashXMLExtension_components[] = {
  MODULE_COMPONENT_ENTRY(sashXMLExtension, SASHXMLEXTENSION, "EZ XML toplevel object"),
  MODULE_COMPONENT_ENTRY(sashXMLDocument, SASHXMLDOCUMENT, "sashXMLDocument"),
  MODULE_COMPONENT_ENTRY(sashXMLNode, SASHXMLNODE, "sashXMLNode"),
  MODULE_COMPONENT_ENTRY(sashXMLIterator, SASHXMLITERATOR, "sashXMLIterator")
};
  
NS_IMPL_NSGETMODULE(sashXMLExtension, sashXMLExtension_components);
