
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHXMLDOCUMENT_H
#define SASHXMLDOCUMENT_H

#include "sashIXPXMLDocument.h"
#include "sashIActionRuntime.h"
#include "sashIConstructor.h"
#include "nsCOMPtr.h"
#include <string>


class AbstractXMLDocument : public sashIXPXMLDocument {
  public:
	 NS_FORWARD_SAFE_SASHIXPXMLDOCUMENT(m_xd);
	
	AbstractXMLDocument() : m_xd(NULL) {

	}
    virtual ~AbstractXMLDocument() {}
    
  protected:
//    nsCOMPtr<sashIXPXMLDocument> m_xd;
    sashIXPXMLDocument * m_xd;
};


// CID: b655feba-3265-4dd9-a590-c104d241b61b
#define SASHXMLDOCUMENT_CONTRACT_ID "@gnome.org/SashXB/sashXMLDocument;1"
#define SASHXMLDOCUMENT_CID {0xb655feba, 0x3265, 0x4dd9, {0xa5, 0x90, 0xc1, 0x04, 0xd2, 0x41, 0xb6, 0x1b}}
NS_DEFINE_CID(ksashXMLDocumentCID, SASHXMLDOCUMENT_CID);

class sashXMLDocument : public AbstractXMLDocument, public sashIConstructor
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHICONSTRUCTOR

 	 NS_IMETHOD SaveToFile(const char *file_name, PRBool *_retval);
	 NS_IMETHOD LoadFromFile(const char *file_name, PRBool *_retval);

	 sashXMLDocument();
	 virtual ~sashXMLDocument() { }

protected:
	nsCOMPtr<sashIActionRuntime> m_rt;
	sashISecurityManager* m_pSecurityManager;
    string m_base_path;
};

#endif
