
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHXMLITERATOR_H
#define SASHXMLITERATOR_H

#include "sashIXPXMLIterator.h"
#include "sashIConstructor.h"

// CID: 51462b98-e872-4c29-808c-ace5716a7e38
#define SASHXMLITERATOR_CONTRACT_ID "@gnome.org/SashXB/sashXMLIterator;1"
#define SASHXMLITERATOR_CID {0x51462b98, 0xe872, 0x4c29, {0x80, 0x8c, 0xac, 0xe5, 0x71, 0x6a, 0x7e, 0x38}}
NS_DEFINE_CID(ksashXMLIteratorCID, SASHXMLITERATOR_CID);

class sashXMLIterator: public sashIXPXMLIterator, public sashIConstructor {
protected:
//	 nsCOMPtr<sashIXPXMLIterator> m_xi;
	 sashIXPXMLIterator * m_xi;

  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHICONSTRUCTOR

    NS_FORWARD_SAFE_SASHIXPXMLITERATOR(m_xi);

    sashXMLIterator();
    virtual ~sashXMLIterator() {}
};

#endif
