
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHXMLNODE_H
#define SASHXMLNODE_H

#include "sashIXPXMLNode.h"

// CID: 6f4d4191-1468-4efe-a2e2-bcf0dbd36058

#define SASHXMLNODE_CONTRACT_ID "@gnome.org/SashXB/sashXMLNode;1"
#define SASHXMLNODE_CID {0x6f4d4191, 0x1468, 0x4efe, {0xa2, 0xe2, 0xbc, 0xf0, 0xdb, 0xd3, 0x60, 0x58}}
NS_DEFINE_CID(ksashXMLNodeCID, SASHXMLNODE_CID);

class sashXMLNode: public sashIXPXMLNode {
  public:
    NS_DECL_ISUPPORTS
	NS_FORWARD_SAFE_SASHIXPXMLNODE(m_xn);

    sashXMLNode();
    virtual ~sashXMLNode();
    
  protected:
    sashIXPXMLNode * m_xn;
};

#endif // SASHXMLNODE_H
