
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHXMLEXTENSION_H
#define SASHXMLEXTENSION_H

#include "sashIXMLExtension.h"
#include "sashIXPXMLDocument.h"
#include "sashIXPXMLIterator.h"
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include <string>

// CID: 0ea3c872-2d74-486a-81e8-295261d1c1c4
#define SASHXMLEXTENSION_CONTRACT_ID "@gnome.org/SashXB/sashXMLExtension;1"
#define SASHXMLEXTENSION_CID {0x0ea3c872, 0x2d74, 0x486a, {0x81, 0xe8, 0x29, 0x52, 0x61, 0xd1, 0xc1, 0xc4}}
NS_DEFINE_CID(ksashXMLExtensionCID, SASHXMLEXTENSION_CID);

class sashXMLExtension: public sashIXMLExtension, public sashIExtension {
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_SASHIEXTENSION
    NS_DECL_SASHIXMLEXTENSION

    sashXMLExtension();
    virtual ~sashXMLExtension();
    
  private:
  
    sashIActionRuntime * m_act;
    std::string base_dir;
    sashIGenericConstructor *DocumentConstructor, *IteratorConstructor;
    
};

#endif
