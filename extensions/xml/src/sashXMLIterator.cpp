
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashXMLIterator.h"
#include "sashXMLNode.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS2_CI(sashXMLIterator, sashIXPXMLIterator, sashIConstructor)

sashXMLIterator::sashXMLIterator() {
	 NS_INIT_ISUPPORTS();
}

NS_IMETHODIMP sashXMLIterator::InitializeNewObject(sashIActionRuntime * actionRuntime,
												   sashIExtension *parent, 
												   JSContext *cx, 
												   PRUint32 argc, nsIVariant **argv, 
												   PRBool *_retval) 
{
  if (argc< 1 || argc> 2)
    return NS_OK;

  if (!VariantIsInterface(argv[0]))
    return NS_OK;
  if (argc== 2 && !VariantIsString(argv[1]))
    return NS_OK;

  string pattern;
  if (argc== 2)
    pattern= VariantGetString(argv[1], "");

  sashIXPXMLNode *tmp_node;
  nsISupports *tmp;
  tmp= VariantGetInterface(argv[0]);
  if (!tmp)
    return NS_OK;
  if (NS_FAILED(tmp->QueryInterface(NS_GET_IID(sashIXPXMLNode), (void **) &tmp_node))) {
    NS_IF_RELEASE(tmp);
    return NS_OK;
  }

  m_xi = NewSashXPXMLIterator(tmp_node, pattern.c_str());
  *_retval= true;
  return NS_OK;
}
