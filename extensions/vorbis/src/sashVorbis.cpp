/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Implementation of the Ogg/Vorbis extension

*****************************************************************/

#include "extensiontools.h"
#include "sashVorbis.h"
#include "sashVorbisFile.h"
#include "sashIGenericConstructor.h"
#include "sashIRuntime.h"
#include "vorbisWorker.h"

SASH_EXTENSION_IMPL_NO_NSGET(sashVorbis, sashIVorbis, "Vorbis");

sashVorbis::sashVorbis()
{
  NS_INIT_ISUPPORTS();
  m_worker = new VorbisWorker();
  DEBUGMSG(vorbis, "Making vorbis extension\n");
}

sashVorbis::~sashVorbis()
{
  delete m_worker;
  NS_RELEASE(m_vfConstructor);
  DEBUGMSG(vorbis, "Destroying vorbis extension\n");
}

NS_IMETHODIMP 
sashVorbis::Cleanup(){
	 return NS_OK;
}

NS_IMETHODIMP
sashVorbis::ProcessEvent()
{
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Initialize(sashIActionRuntime *act,
		       const char *regsrationXML,
		       const char *webGUID,
		       JSContext *cx, JSObject *obj)
{
  //rt->GetSecurityManager(&m_securityManager);
  NewSashConstructor(act, this, SASHVORBISFILE_CONTRACT_ID, SASHIVORBISFILE_IID_STR, &m_vfConstructor);

  DEBUGMSG(vorbis, "initializing vorbis\n");
  if (!m_worker->start())
    return NS_COMFALSE;
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::AssertFileOK(const char *filename)
{
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::GetVorbisFile(sashIGenericConstructor **ret)
{
  DEBUGMSG(vorbis, "Returning VorbisFile constructor\n");
  NS_ADDREF(m_vfConstructor);
  *ret = m_vfConstructor;
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::SetCurrent(sashIVorbisFile *file)
{
  char *name;
  file->GetName(&name);
  DEBUGMSG(vorbis, "Setting current\n");
  m_worker->setCurrent(name);
  nsMemory::Free(name);
  Reset();
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Play()
{
  DEBUGMSG(vorbis, "Playing\n");
  m_worker->resume();
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Pause()
{
  m_worker->pause();
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Reset()
{
  m_worker->seek(0);
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Seek(double sec)
{
  m_worker->seek(sec);
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Tell(double *sec)
{
  *sec = m_worker->tell();
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Forward(double sec)
{
  double pos = m_worker->tell();
  double newPos = pos + sec;
  m_worker->seek(newPos);
  return NS_OK;
}

NS_IMETHODIMP
sashVorbis::Back(double sec)
{
  double pos = m_worker->tell();
  double newPos = pos - sec;
  if (newPos < 0) newPos = 0;
  m_worker->seek(newPos);
  return NS_OK;
}
