/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Worker thread class for the vorbis extension

*****************************************************************/

#include <esd.h>
#include <unistd.h>
#include <assert.h>

#include "vorbisWorker.h"
#include "debugmsg.h"

static void *
start_run(void *worker)
{
  VorbisWorker *worker2 = (VorbisWorker *) worker;
  DEBUGMSG(vorbis, "About to run\n");
  worker2->run();
  return NULL;
}

VorbisWorker::VorbisWorker()
  : m_running(false),
    m_has_request(false),
    m_file(NULL),
    m_eof(false),
    m_state(VW_IDLE)
{
}

VorbisWorker::~VorbisWorker()
{
  closeCurrent();
  pthread_mutex_destroy(&m_state_lock);
  pthread_mutex_destroy(&m_ov_lock);
  pthread_mutex_destroy(&m_idle_lock);
  pthread_cond_destroy(&m_idle_cond);
}

worker_state
VorbisWorker::getState()
{
  worker_state temp;
  pthread_mutex_lock(&m_state_lock);
  temp = m_state;
  pthread_mutex_unlock(&m_state_lock);
  return temp;
}

void
VorbisWorker::setState(worker_state state)
{
  pthread_mutex_lock(&m_state_lock);
  m_state = state;
  pthread_mutex_unlock(&m_state_lock);

  switch(state) {

  case VW_PLAY:
    setRequest(true);
    break;

  case VW_IDLE:
    setRequest(false);
    break;

  case VW_SEEK:
    seekTo(m_seek);
    setRequest(true);

    break;
  }
}

bool
VorbisWorker::getEOF()
{
  bool eof;
  pthread_mutex_lock(&m_state_lock);
  eof = m_eof;
  pthread_mutex_unlock(&m_state_lock);
  return eof;
}

void
VorbisWorker::setEOF(bool eof)
{
  pthread_mutex_lock(&m_state_lock);
  m_eof = eof;
  pthread_mutex_unlock(&m_state_lock);
}

void
VorbisWorker::initESD()
{
  int rate = ESD_DEFAULT_RATE;
  int esd_fmt = ESD_BITS16 | ESD_STEREO | ESD_STREAM | ESD_PLAY;
  m_sock = esd_play_stream(esd_fmt, rate, NULL, "test output");
	if (m_sock < 0) {
		m_sock = esd_play_stream_fallback(esd_fmt, rate, 
																			NULL, "test output");
	}
  assert(m_sock >= 0);
	DEBUGMSG(vorbis, "Got socket %d\n", m_sock);
}

void
VorbisWorker::stopESD()
{
  close(m_sock);
}

bool
VorbisWorker::start()
{
  initESD();

  pthread_mutex_init(&m_state_lock, NULL);
  pthread_mutex_init(&m_ov_lock, NULL);
  pthread_mutex_init(&m_idle_lock, NULL);
  pthread_cond_init(&m_idle_cond, NULL);

  pthread_attr_t attr;
  pthread_attr_init(&attr);

  // do not allow the thread to enter the loop until m_running is set
  pthread_mutex_lock(&m_state_lock);
  int res = pthread_create(&m_thread, &attr, start_run, (void *) this);  
  m_running = (res == 0);
  pthread_mutex_unlock(&m_state_lock);

  return m_running;
}

bool
VorbisWorker::setCurrent(const string & filename)
{
  assert(m_running);
  closeCurrent();
  pthread_mutex_lock(&m_ov_lock);
  DEBUGMSG(vorbis, "Setting current to %s\n", filename.c_str());
  m_file = fopen(filename.c_str(), "r");
  if (m_file == NULL) {
    fprintf(stderr, "Could not open file\n");
    pthread_mutex_unlock(&m_ov_lock);
    return false;
  }
  int res = ov_open(m_file, &m_vf, NULL, 0);
  if (res < 0) {
    fprintf(stderr, "Could not open as a Vorbis file: %s\n", 
	    filename.c_str());
    pthread_mutex_unlock(&m_ov_lock);
    return false;
  }
  pthread_mutex_unlock(&m_ov_lock);
  return true;
}

void
VorbisWorker::closeCurrent()
{
  pthread_mutex_lock(&m_ov_lock);
  if (m_file) {
    ov_clear(&m_vf);
  }
  pthread_mutex_unlock(&m_ov_lock);
}

bool
VorbisWorker::keepRunning()
{
  return true;
}

void
VorbisWorker::waitForRequest()
{
  pthread_mutex_lock(&m_idle_lock);
  while (!m_has_request) {
    pthread_cond_wait(&m_idle_cond, &m_idle_lock);
  }
  pthread_mutex_unlock(&m_idle_lock);
}

void
VorbisWorker::setRequest(bool val)
{
  pthread_mutex_lock(&m_idle_lock);
  m_has_request = val;
  if (m_has_request) {
    pthread_cond_signal(&m_idle_cond);
  }
  pthread_mutex_unlock(&m_idle_lock);
}

void
VorbisWorker::run()
{
  // cannot enter the loop until Init has set m_running
  DEBUGMSG(vorbis, "Starting vorbis worker\n");
  pthread_mutex_lock(&m_state_lock);
  pthread_mutex_unlock(&m_state_lock);

  while (m_running) {

    waitForRequest();

    switch (getState()) {

    case VW_IDLE:
      break;

    case VW_PLAY:
      playLoop();
      break;

    case VW_SEEK:
      seekTo(m_seek);
      setRequest(false);
      break;

    default:
      break;
    }
  }
  DEBUGMSG(vorbis, "Done running\n");
}

void
VorbisWorker::play()
{
  assert(m_running);
  setState(VW_PLAY);
}

void 
VorbisWorker::pause()
{
  assert(m_running);
  setState(VW_IDLE);
}

void
VorbisWorker::resume()
{
  assert(m_running);
  setState(VW_PLAY);
}

void 
VorbisWorker::seekTo(double pos)
{
  pthread_mutex_lock(&m_state_lock);
  ov_time_seek(&m_vf, pos);
  pthread_mutex_unlock(&m_state_lock);
}

void
VorbisWorker::seek(double sec)
{
  pthread_mutex_lock(&m_state_lock);
  m_seek = sec;
  pthread_mutex_unlock(&m_state_lock);
  setState(VW_SEEK);
}

double
VorbisWorker::tell()
{
  double temp;
  pthread_mutex_lock(&m_ov_lock);
  temp = ov_time_tell(&m_vf);
  pthread_mutex_unlock(&m_ov_lock);
  return temp;
}

void
VorbisWorker::playLoop()
{
  int current_section;
  char pcmout[4096];

  pthread_mutex_lock(&m_ov_lock);
  long ret = ov_read(&m_vf, pcmout, sizeof(pcmout), 0, 2, 1, &current_section);
  pthread_mutex_unlock(&m_ov_lock);

  if (ret == 0) {
    m_eof = true;
  } else if (ret < 0) {
  } else {
    write(m_sock, pcmout, ret);
  }
}
