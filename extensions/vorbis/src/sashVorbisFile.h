
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

header file for the VorbisFile object
******************************************************************/

#ifndef SASHVORBISFILE_H
#define SASHVORBISFILE_H

#include "nsID.h"
#include "sashIVorbisFile.h"
#include "sashIConstructor.h"
#include <string>

//A34E463E-41F9-42AC-AAA1-7B142FDD1C07
#define SASHVORBISFILE_CID {0xA34E463E, 0x41F9, 0x42AC, {0xAA, 0xA1, 0x7B, 0x14, 0x2F, 0xDD, 0x1C, 0x07}}

NS_DEFINE_CID(ksashVorbisFileCID, SASHVORBISFILE_CID);

#define SASHVORBISFILE_CONTRACT_ID "@gnome.org/SashXB/vorbis/vorbisfile;1"

class sashVorbisFile : public sashIVorbisFile,
		       public sashIConstructor
{
 public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHICONSTRUCTOR;
  NS_DECL_SASHIVORBISFILE;

  sashVorbisFile();
  virtual ~sashVorbisFile();

 private:
  string m_filename;
  int m_sock;
};

#endif // SASHVORBISFILE_H
