
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

implementation file for the VorbisFile object
******************************************************************/

#include "sashVorbisFile.h"
#include "sashIVorbis.h"
#include "sashIExtension.h"
#include "sashVariantUtils.h"

#include "debugmsg.h"
#include <nsCRT.h>

#include <vorbis/vorbisfile.h>
#include <vorbis/codec.h>
#include <stdio.h>
#include <esd.h>
#include <xpcomtools.h>
#include <glib.h>
#include "secman.h"

NS_IMPL_ISUPPORTS2_CI(sashVorbisFile, sashIVorbisFile, sashIConstructor);

sashVorbisFile::sashVorbisFile()
{
  DEBUGMSG(vorbis, "Creating vorbisfile\n");
  NS_INIT_ISUPPORTS();
}

sashVorbisFile::~sashVorbisFile()
{
  DEBUGMSG(vorbis, "Destroying vorbisfile\n");
}

NS_IMETHODIMP
sashVorbisFile::InitializeNewObject(sashIActionRuntime * act, sashIExtension *ext, JSContext *cx, 
									PRUint32 argc,nsIVariant **argv, 
									PRBool *ret)
{
  if (argc != 1) {
    *ret = false;
    return NS_COMFALSE;
  }

  if (! VariantIsString(argv[0])){
    *ret = false;
    return NS_COMFALSE;
  }

  m_filename = VariantGetString(argv[0]);
  if (m_filename[0] != '/') {
	   string path;
	   XP_GET_STRING(act->GetDataDirectory, path);
	   m_filename = path + '/' + m_filename;
  }

  DEBUGMSG(vorbis, "Creating sashVorbisFile with filename %s\n", 
	   m_filename.c_str());

  act->AssertFSAccess(m_filename.c_str());

  nsCOMPtr<sashIVorbis> vorbisExt(do_QueryInterface(ext));
  //vorbisExt->AssertFileOK(m_filename);
  
  *ret = true;
  return NS_OK;
}

NS_IMETHODIMP
sashVorbisFile::GetName(char **name)
{
  XP_COPY_STRING(m_filename.c_str(), name);
  return NS_OK;
}

NS_IMETHODIMP
sashVorbisFile::GetTag(const char *tagName, char **ret)
{
  assert(ret != NULL);
  DEBUGMSG(vorbis, "Getting tag %s from filename %s\n", 
	   tagName, m_filename.c_str());
  FILE *file = fopen(m_filename.c_str(), "r");
  if (file == NULL) {
    // Throw an exception
    return NS_ERROR_FILE_NOT_FOUND;
  }

  OggVorbis_File vf;
  int res = ov_open(file, &vf, NULL, 0);

  if (res != 0) {
    //throw an exception
    fclose(file); // anywhere after this, ov_clear closes for us
    return NS_COMFALSE;
  }

  vorbis_comment *comments = ov_comment(&vf, -1);
  if (!comments) {
    // throw an exception
    ov_clear(&vf);
    return NS_COMFALSE;
  }

  char *tagDup = g_strdup(tagName);
  int pos = 0;
  *ret = vorbis_comment_query(comments, tagDup, pos);
  DEBUGMSG(vorbis, "Got comment %s\n", *ret);
  g_free(tagDup);
  if (*ret == NULL) {
    *ret = "";
    DEBUGMSG(vorbis, "Null\n");
  }
  *ret = XP_STRDUP(*ret); // we are returned a reference, not a copy
  ov_clear(&vf);
  return NS_OK;
}
