/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Header file for the vorbis extension

*****************************************************************/

#ifndef SASHVORBIS_H
#define SASHVORBIS_H

#include "nsID.h"
#include "sashIExtension.h"
#include "sashIVorbis.h"

class VorbisWorker;
class sashISecurityManager;

// {76A30DCE-C69C-4DCE-A7BF-342E6FE7F044}

#define SASHVORBIS_CID {0x76A30DCE, 0xC69C, 0x4DCE, {0xA7, 0xBF, 0x34, 0x2E, 0x6F, 0xE7, 0xF0, 0x44}}

NS_DEFINE_CID(ksashVorbisCID, SASHVORBIS_CID);

#define SASHVORBIS_CONTRACT_ID "@gnome.org/SashXB/vorbis;1"

class sashVorbis : public sashIExtension,
                   public sashIVorbis
{
 public:
  NS_DECL_ISUPPORTS;
  NS_DECL_SASHIEXTENSION;
  NS_DECL_SASHIVORBIS;

  sashVorbis();
  virtual ~sashVorbis();

 private:
  sashIGenericConstructor *m_vfConstructor;
  sashISecurityManager *m_securityManager;
  VorbisWorker *m_worker;
};

#endif // SASHVORBIS_H
