
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef VORBISWORKER_H
#define VORBISWORKER_H

#include <pthread.h>
#include <vorbis/vorbisfile.h>
#include <string>
#include <stdio.h>

typedef enum {VW_IDLE, VW_PLAY, VW_SEEK} worker_state;

/**
   Class which operates in a separate thread and accepts and executes 
   vorbis commands. It is thread-safe.
*/
class VorbisWorker 
{
 public:
  VorbisWorker();
  ~VorbisWorker();

  //! Start the thread running; returns false on failure
  bool start();

  //! Kill the thread
  void kill();

  //! Set the current vorbis file to play
  bool setCurrent(const string & filename);

  //! Start playing the current song
  void play();

  //! Pause but do not reset the position in the file
  void pause();

  //! Unpause
  void resume();

  //! Seek to the given number of seconds
  void seek(double sec);

  //! Return the position in the file in seconds
  double tell();



  //! Contains the main loop for the object. Don't call directly
  void run();

 private:

  int m_sock;
  bool m_running;
  bool m_has_request;
  double m_seek;

  pthread_t m_thread;

  // locks which control access to internal variables, 
  // the vorbisfile, and the idle condition
  pthread_mutex_t m_state_lock, m_ov_lock, m_idle_lock;

  // condition variable which lets the thread block when idle
  pthread_cond_t m_idle_cond;

  OggVorbis_File m_vf;
  FILE *m_file;

  bool m_eof;
  worker_state m_state;
  
  //! set whether a request is pending so the thread can block/wake-up
  void setRequest(bool);
  //! Block until a request is made
  void waitForRequest();

  void closeCurrent();

  worker_state getState();
  void setState(worker_state state);

  bool getEOF();
  void setEOF(bool);

  bool keepRunning();
  
  void initESD();
  void stopESD();

  void playLoop();
  void seekTo(double pos);
};

#endif // VORBISWORKER_H
