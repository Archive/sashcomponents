/* sashJabberJID.h
 * JID functions for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBERJID_H
#define SASHJABBERJID_H

#include "nsID.h"
#include "sashIJabberJID.h"
#include "sashIConstructor.h"

#include <jabberoo.hh>
#include <jabberlue.hh>

// CID: 92720e60-2fbb-46ec-b5fb-cb59330657f8

#define SASHJABBERJID_CONTRACT_ID "@gnome.org/SashXB/sashJabberJID;1"
#define SASHJABBERJID_CID {0x92720e60, 0x2fbb, 0x46ec, {0xb5, 0xfb, 0xcb, 0x59, 0x33, 0x06, 0x57, 0xf8}}
NS_DEFINE_CID(ksashJabberJIDCID, SASHJABBERJID_CID);

class sashJabberJID : public sashIJabberJID
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHIJABBERJID;

     sashJabberJID();
     virtual ~sashJabberJID();
};

#endif
