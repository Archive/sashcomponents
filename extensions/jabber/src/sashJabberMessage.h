/* sashJabberMessage.h
 * Message object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBERMESSAGE_H
#define SASHJABBERMESSAGE_H

#include "nsID.h"
#include "sashIJabberMessage.h"
#include "sashIConstructor.h"

#include <jabberoo.hh>
#include <jabberlue.hh>

// CID: 476066dc-158c-4754-b2b1-05c40eb05637

#define SASHJABBERMESSAGE_CONTRACT_ID "@gnome.org/SashXB/sashJabberMessage;1"
#define SASHJABBERMESSAGE_CID {0x476066dc, 0x158c, 0x4754, {0xb2, 0xb1, 0x05, 0xc4, 0x0e, 0xb0, 0x56, 0x37}}
NS_DEFINE_CID(ksashJabberMessageCID, SASHJABBERMESSAGE_CID);

class sashJabberMessage : public sashIJabberMessage,
			  public sashIConstructor
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHICONSTRUCTOR;
     NS_DECL_SASHIJABBERMESSAGE;

     sashJabberMessage();
     virtual ~sashJabberMessage();
     
 private:
     Jabberlue::Message* _message;
};

#endif
