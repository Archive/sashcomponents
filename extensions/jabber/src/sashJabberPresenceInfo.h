/* sashJabberPresenceInfo.h
 * Presence Info for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBERPRESENCEINFO_H
#define SASHJABBERPRESENCEINFO_H

#include "nsID.h"
#include "sashIJabberPresenceInfo.h"
#include "sashIConstructor.h"

#include <jabberoo.hh>
#include <jabberlue.hh>

// CID: 69013959-f63d-4c37-940a-f09cce8d6d1f

#define SASHJABBERPRESENCEINFO_CONTRACT_ID "@gnome.org/SashXB/sashJabberPresenceInfo;1"
#define SASHJABBERPRESENCEINFO_CID {0x69013959, 0xf63d, 0x4c37, {0x94, 0x0a, 0xf0, 0x9c, 0xce, 0x8d, 0x6d, 0x1f}}
NS_DEFINE_CID(ksashJabberPresenceInfoCID, SASHJABBERPRESENCEINFO_CID);

class sashJabberPresenceInfo : public sashIJabberPresenceInfo,
			       public sashIConstructor
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHICONSTRUCTOR;
     NS_DECL_SASHIJABBERPRESENCEINFO;

     sashJabberPresenceInfo();
     virtual ~sashJabberPresenceInfo();

 private:
     int _status;
     string _statusmsg;
};

#endif
