/* sashJabber.h
 * Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBER_H
#define SASHJABBER_H

#include "nsID.h"
#include "sashIJabber.h"
#include "sashIExtension.h"
#include "sashIJabberJID.h"

// CID: 5a8b96f2-2415-4c7c-bc46-7bb4e23653b5

#define SASHJABBER_CONTRACT_ID "@gnome.org/SashXB/sashJabber;1"
#define SASHJABBER_CID {0x5a8b96f2, 0x2415, 0x4c7c, {0xbc, 0x46, 0x7b, 0xb4, 0xe2, 0x36, 0x53, 0xb5}}
NS_DEFINE_CID(ksashJabberCID, SASHJABBER_CID);

class sashJabber : public sashIExtension,
		   public sashIJabber
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHIEXTENSION;
     NS_DECL_SASHIJABBER;

     sashJabber();
     virtual ~sashJabber();
     
 private:
     sashIGenericConstructor *m_sessionConstructor;
     sashIGenericConstructor *m_messageConstructor;
     sashIJabberJID *m_jid;
     sashIGenericConstructor *m_rosteritemConstructor;
     sashISecurityManager* m_SecurityManager;
};

#endif
