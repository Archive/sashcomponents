/* sashJabberModule.cc
 * Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabber.h"
#include "sashJabberSession.h"
#include "sashJabberMessage.h"
#include "sashJabberRoster.h"
#include "sashJabberRosterItem.h"
#include "sashJabberJID.h"
#include "extensiontools.h"

NS_DECL_CLASSINFO(sashJabber);
NS_DECL_CLASSINFO(sashJabberSession);
NS_DECL_CLASSINFO(sashJabberMessage);
NS_DECL_CLASSINFO(sashJabberRoster);
NS_DECL_CLASSINFO(sashJabberRosterItem);
NS_DECL_CLASSINFO(sashJabberJID);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashJabber);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashJabberSession);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashJabberMessage);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashJabberRoster);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashJabberRosterItem);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashJabberJID);

static nsModuleComponentInfo components[] = {
  MODULE_COMPONENT_ENTRY(sashJabber, SASHJABBER, "Jabber extension"),
  MODULE_COMPONENT_ENTRY(sashJabberSession, SASHJABBERSESSION, "Jabber Session"),
  MODULE_COMPONENT_ENTRY(sashJabberMessage, SASHJABBERMESSAGE, "Jabber Message"),
  MODULE_COMPONENT_ENTRY(sashJabberRoster, SASHJABBERROSTER, "Jabber Roster"),
  MODULE_COMPONENT_ENTRY(sashJabberRosterItem, SASHJABBERROSTERITEM, "Jabber Roster Item"),
  MODULE_COMPONENT_ENTRY(sashJabberJID, SASHJABBERJID, "Jabber JID")
};

NS_IMPL_NSGETMODULE(sashJabber, components)
