/* sashJabberSession.cpp
 * Session object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabberSession.h"
#include "extensiontools.h"
#include "nsIVariant.h"
#include "sashVariantUtils.h"
#include <string>
#include <vector>

#include <jabberoo.hh>
#include <jabberlue.hh>
#include <jabberlue-conf-write.hh> // for setting username, host, etc
#include <sigc++/bind.h>
#include <sigc++/signal_system.h>

#include "sashJabberMessage.h"
#include "sashJabberRoster.h"

NS_IMPL_ISUPPORTS2_CI(sashJabberSession, sashIJabberSession, sashIConstructor)

sashJabberSession::sashJabberSession() : _connected(false) {
  NS_INIT_ISUPPORTS();
  nsresult rv = nsComponentManager::CreateInstance(ksashJabberRosterCID,
						   NULL, NS_GET_IID(sashIJabberRoster), (void **) &m_roster);
  if (NS_FAILED(rv))
       DEBUGMSG(sashJabber, "CreateInstance m_roster failed.");
}

sashJabberSession::~sashJabberSession()
{
  /* destructor code */
     if (_session)
     {
	  _session->disconnect();
	  delete _session;
     }
     NS_IF_RELEASE(m_roster);
}

NS_IMETHODIMP sashJabberSession::InitializeNewObject(sashIActionRuntime * act, sashIExtension* ext, 
						     JSContext* cx, 
						     PRUint32 argc, nsIVariant **argv, 
						     PRBool *ret)
{
     // Store a pointer to the JSContext
     _jscontext = cx;
	 _act = act;

     DEBUGMSG(sashJabber, "Session initialize\n");
     // Create a new session
     char* temp = "";
     _session = new Jabberlue::Session(0, &temp);

     DEBUGMSG(sashJabber, "Session created.\n");

     _session->evtConnected.connect(SigC::slot(this, &sashJabberSession::on_evtConnected));
     _session->evtDisconnected.connect(SigC::slot(this, &sashJabberSession::on_evtDisconnected));
     _session->evtMessage.connect(SigC::slot(this, &sashJabberSession::on_evtMessage));
     _session->evtIQ.connect(SigC::slot(this, &sashJabberSession::on_evtIQ));
     _session->evtPresence.connect(SigC::slot(this, &sashJabberSession::on_evtPresence));

     _session->errMessage.connect(SigC::slot(this, &sashJabberSession::on_errMessage));
     _session->errNoConfig.connect(SigC::slot(this, &sashJabberSession::on_errNoConfig));
     _session->errNoPassword.connect(SigC::slot(this, &sashJabberSession::on_errNoPassword));
     _session->errNoResource.connect(SigC::slot(this, &sashJabberSession::on_errNoResource));
     _session->errResourceDuplicate.connect(SigC::slot(this, &sashJabberSession::on_errResourceDuplicate));

     *ret = true;
     return NS_OK;
}


/* readonly attribute sashIJabberRoster Roster; */
NS_IMETHODIMP sashJabberSession::GetRoster(sashIJabberRoster * *aRoster)
{
     NS_IF_ADDREF(m_roster);
     *aRoster = m_roster;
     (*aRoster)->Initialize(_jscontext, &_session->getSession());
     return NS_OK;
}

/* attribute string RemoteHost; */
NS_IMETHODIMP sashJabberSession::GetRemoteHost(char * *aRemoteHost)
{
     XP_COPY_STRING(_session->getRemoteHost().c_str(), aRemoteHost);
     return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetRemoteHost(const char * aRemoteHost)
{
     JabberlueConfWrite jc(0, NULL);
     jc.set_server(aRemoteHost);
     jc.sync();
     return NS_OK;
}

/* attribute short RemotePort; */
NS_IMETHODIMP sashJabberSession::GetRemotePort(PRInt16 *aRemotePort)
{
     *aRemotePort = _session->getRemotePort();
     return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetRemotePort(PRInt16 aRemotePort)
{
     JabberlueConfWrite jc(0, NULL);
     jc.set_port(aRemotePort);
     jc.sync();
     return NS_OK;
}

/* attribute string UserName; */
NS_IMETHODIMP sashJabberSession::GetUserName(char * *aUserName)
{
     XP_COPY_STRING(_session->getUsername().c_str(), aUserName);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetUserName(const char * aUserName)
{
     JabberlueConfWrite jc(0, NULL);
     jc.set_username(aUserName);
     jc.sync();
     return NS_OK;
}


/* attribute string Password; */
NS_IMETHODIMP sashJabberSession::GetPassword(char * *aPassword)
{
     XP_COPY_STRING(_session->getPassword().c_str(), aPassword);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetPassword(const char * aPassword)
{
     _session->setPassword(aPassword);
     return NS_OK;
}


/* attribute string Resource; */
NS_IMETHODIMP sashJabberSession::GetResource(char * *aResource)
{
     XP_COPY_STRING(_session->getResource().c_str(), aResource);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetResource(const char * aResource)
{
     _session->setResource(aResource);
     return NS_OK;
}

/* readonly attribute string JID; */
NS_IMETHODIMP sashJabberSession::GetJID(char * *aJID)
{
     XP_COPY_STRING(_session->getJID().c_str(), aJID);
     return NS_OK;
}

/* attribute boolean NewUser; */
NS_IMETHODIMP sashJabberSession::GetNewUser(PRBool * aNewUser)
{
     *aNewUser = _newuser;
     return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetNewUser(PRBool aNewUser)
{
     _newuser = aNewUser;
     _session->setNewUser(_newuser);
     return NS_OK;
}

/* void Begin (); */
NS_IMETHODIMP sashJabberSession::Begin()
{
     DEBUGMSG(sashJabber, "Connecting.\n");
     _session->connect();
     return NS_OK;
}

/* void End (); */
NS_IMETHODIMP sashJabberSession::End()
{
     _session->disconnect();
     return NS_OK;
}

/* void SetPresence (in short status, in string message); */
NS_IMETHODIMP sashJabberSession::SetPresence(PRInt16 status, const char *message)
{
     _session->setPresence((jabberoo::Presence::Show)(status), string(message));
     return NS_OK;
}

/* void SendMessage (in string jid, in string body); */
NS_IMETHODIMP sashJabberSession::SendMessage(const char *jid, const char *body)
{
     if (_connected)
	  _session->send(Jabberlue::Message(string(jid), string(body)));
     return NS_OK;
}

/* void SendMessageObject (in sashIJabberMessage message); */
NS_IMETHODIMP sashJabberSession::SendMessageObject(sashIJabberMessage *message)
{
     Jabberlue::Message* msg;
     message->GetMessage(&msg);
     if (_connected) 
		  _session->send(*msg);
     return NS_OK;
}

/* void SendXML (in string xml); */
NS_IMETHODIMP sashJabberSession::SendXML(const char *xml)
{
     _session->send(xml);
     return NS_OK;
}

/* attribute string OnBegin; */
NS_IMETHODIMP sashJabberSession::GetOnBegin(char * *aOnBegin)
{
     XP_COPY_STRING(_OnBegin.c_str(), aOnBegin);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnBegin(const char * aOnBegin)
{
     _OnBegin = aOnBegin;
     return NS_OK;
}

/* attribute string OnEnd; */
NS_IMETHODIMP sashJabberSession::GetOnEnd(char * *aOnEnd)
{
     XP_COPY_STRING(_OnEnd.c_str(), aOnEnd);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnEnd(const char * aOnEnd)
{
     _OnEnd = aOnEnd;
     return NS_OK;
}

/* attribute string OnError; */
NS_IMETHODIMP sashJabberSession::GetOnError(char * *aOnError)
{
     XP_COPY_STRING(_OnError.c_str(), aOnError);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnError(const char * aOnError)
{
     _OnError = aOnError;
     return NS_OK;
}

/* attribute string OnErrorNoConfig; */
NS_IMETHODIMP sashJabberSession::GetOnErrorNoConfig(char * *aOnErrorNoConfig)
{
     XP_COPY_STRING(_OnErrorNoConfig.c_str(), aOnErrorNoConfig);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnErrorNoConfig(const char * aOnErrorNoConfig)
{
     _OnErrorNoConfig = aOnErrorNoConfig;
     return NS_OK;
}

/* attribute string OnErrorNoPassword; */
NS_IMETHODIMP sashJabberSession::GetOnErrorNoPassword(char * *aOnErrorNoPassword)
{
     XP_COPY_STRING(_OnErrorNoPassword.c_str(), aOnErrorNoPassword);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnErrorNoPassword(const char * aOnErrorNoPassword)
{
     _OnErrorNoPassword = aOnErrorNoPassword;
     return NS_OK;
}

/* attribute string OnErrorNoResource; */
NS_IMETHODIMP sashJabberSession::GetOnErrorNoResource(char * *aOnErrorNoResource)
{
     XP_COPY_STRING(_OnErrorNoResource.c_str(), aOnErrorNoResource);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnErrorNoResource(const char * aOnErrorNoResource)
{
     _OnErrorNoResource = aOnErrorNoResource;
     return NS_OK;
}

/* attribute string OnErrorResourceDuplicate; */
NS_IMETHODIMP sashJabberSession::GetOnErrorResourceDuplicate(char * *aOnErrorResourceDuplicate)
{
     XP_COPY_STRING(_OnErrorResourceDuplicate.c_str(), aOnErrorResourceDuplicate);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnErrorResourceDuplicate(const char * aOnErrorResourceDuplicate)
{
     _OnErrorResourceDuplicate = aOnErrorResourceDuplicate;
     return NS_OK;
}

/* attribute string OnMessage; */
NS_IMETHODIMP sashJabberSession::GetOnMessage(char * *aOnMessage)
{
     XP_COPY_STRING(_OnMessage.c_str(), aOnMessage);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnMessage(const char * aOnMessage)
{
     _OnMessage = aOnMessage;
     return NS_OK;
}

/* attribute string OnMessageObject; */
NS_IMETHODIMP sashJabberSession::GetOnMessageObject(char * *aOnMessageObject)
{
     XP_COPY_STRING(_OnMessageObject.c_str(), aOnMessageObject);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnMessageObject(const char * aOnMessageObject)
{
     _OnMessageObject = aOnMessageObject;
     return NS_OK;
}

/* attribute string OnIQ; */
NS_IMETHODIMP sashJabberSession::GetOnIQ(char * *aOnIQ)
{
     XP_COPY_STRING(_OnIQ.c_str(), aOnIQ);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnIQ(const char * aOnIQ)
{
     _OnIQ = aOnIQ;
     return NS_OK;
}

/* attribute string OnPresence; */
NS_IMETHODIMP sashJabberSession::GetOnPresence(char * *aOnPresence)
{
     XP_COPY_STRING(_OnPresence.c_str(), aOnPresence);
  return NS_OK;
}
NS_IMETHODIMP sashJabberSession::SetOnPresence(const char * aOnPresence)
{
     _OnPresence = aOnPresence;
     return NS_OK;
}

void sashJabberSession::on_evtConnected()
{
     _connected = true;
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(1);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);
     
     if (!_OnBegin.empty()) 
	  CallEvent(_act, _jscontext, _OnBegin, args);
}

void sashJabberSession::on_evtDisconnected()
{
     _connected = false;
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(1);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);
     
     if (!_OnEnd.empty()) 
	  CallEvent(_act, _jscontext, _OnEnd, args);
}

void sashJabberSession::on_evtMessage(const Jabberlue::Message& msg)
{
     nsresult rv;
     nsISupports *ret;
     sashIJabberMessage* smsg;
     rv = nsComponentManager::CreateInstance(ksashJabberMessageCID,
					     NULL, NS_GET_IID(sashIJabberMessage), (void **) &smsg);
     if (NS_FAILED(rv))
	  return;  
     smsg->Initialize(_jscontext, &msg);
     std::vector<nsIVariant *> args(2);
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     
     NewVariant(&(args[1]));
     
     VariantSetInterface(args[0], ret);
     VariantSetInterface(args[1], NS_STATIC_CAST(nsISupports*, smsg));
     
     if (!_OnMessageObject.empty()) 
	  CallEvent(_act, _jscontext, _OnMessageObject, args);
     
     // Now for the normal OnMessage:
     std::vector<nsIVariant *> argsn;

     nsIVariant *sessVariant;
     NewVariant(&sessVariant);
     VariantSetInterface(sessVariant, ret);
     argsn.push_back(sessVariant);

     nsIVariant *jidVariant;
     NewVariant(&jidVariant);
     VariantSetString(jidVariant, msg.getFrom());
     argsn.push_back(jidVariant);

     nsIVariant *bodyVariant;
     NewVariant(&bodyVariant);
     VariantSetString(bodyVariant, msg.getBody());
     argsn.push_back(bodyVariant);

     nsIVariant *xmlVariant;
     NewVariant(&xmlVariant);
     VariantSetString(xmlVariant, msg.toString());
     argsn.push_back(xmlVariant);

     if (!_OnMessage.empty())
	  CallEvent(_act, _jscontext, _OnMessage, argsn);
}

void sashJabberSession::on_evtIQ(const jabberoo::Packet& iq)
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(2);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);
     
     NewVariant(&(args[1]));
     VariantSetString(args[1], iq.toString());

     if (!_OnIQ.empty()) 
	  CallEvent(_act, _jscontext, _OnIQ, args);
}

void sashJabberSession::on_evtPresence(const Jabberlue::Presence& p, bool changed)
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(2);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);
     
     NewVariant(&(args[1]));
     VariantSetString(args[1], p.toString());

     if (!_OnPresence.empty()) 
	  CallEvent(_act, _jscontext, _OnPresence, args);
}

void sashJabberSession::on_errNoConfig()
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(1);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);

     if (!_OnErrorNoConfig.empty()) 
	  CallEvent(_act, _jscontext, _OnErrorNoConfig, args);
}

void sashJabberSession::on_errNoPassword()
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(1);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);

     if (!_OnErrorNoConfig.empty()) 
	  CallEvent(_act, _jscontext, _OnErrorNoPassword, args);
}

void sashJabberSession::on_errNoResource()
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(1);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);

     if (!_OnErrorNoConfig.empty()) 
	  CallEvent(_act, _jscontext, _OnErrorNoResource, args);
}

void sashJabberSession::on_errResourceDuplicate()
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(1);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);

     if (!_OnErrorNoConfig.empty()) 
	  CallEvent(_act, _jscontext, _OnErrorResourceDuplicate, args);
}

void sashJabberSession::on_errMessage(const Jabberlue::Message& m)
{
     nsresult rv;
     nsISupports *ret;
     std::vector<nsIVariant *> args(2);
     
     NewVariant(&(args[0]));
     rv = QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
     VariantSetInterface(args[0], ret);

     NewVariant(&(args[1]));
     VariantSetString(args[1], m.getError());

     if (!_OnErrorNoConfig.empty()) 
	  CallEvent(_act, _jscontext, _OnError, args);
}
