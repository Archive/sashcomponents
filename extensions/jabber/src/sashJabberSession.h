/* sashJabberSession.h
 * Session object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBERSESSION_H
#define SASHJABBERSESSION_H

#include "nsID.h"
#include "sashIJabberSession.h"
#include "sashIJabberMessage.h"
#include "sashIJabberRoster.h"
#include "sashIConstructor.h"
#include "sashIActionRuntime.h"

#include <jabberoo.hh>
#include <jabberlue.hh>
#include <sigc++/object.h>

// CID: faa6cfcd-ab13-48f6-b2a3-98ee88155778

#define SASHJABBERSESSION_CONTRACT_ID "@gnome.org/SashXB/sashJabberSession;1"
#define SASHJABBERSESSION_CID {0xfaa6cfcd, 0xab13, 0x48f6, {0xb2, 0xa3, 0x98, 0xee, 0x88, 0x15, 0x57, 0x78}}
NS_DEFINE_CID(ksashJabberSessionCID, SASHJABBERSESSION_CID);

class sashJabberSession : public sashIJabberSession,
			  public sashIConstructor,
			  public SigC::Object
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHICONSTRUCTOR;
     NS_DECL_SASHIJABBERSESSION;

     sashJabberSession();
     virtual ~sashJabberSession();

 protected:
     void on_evtConnected();
     void on_evtDisconnected();
     void on_evtMessage(const Jabberlue::Message& msg);
     void on_evtIQ(const jabberoo::Packet& iq);
     void on_evtPresence(const Jabberlue::Presence& p, bool changed);
     void on_errNoConfig();
     void on_errNoPassword();
     void on_errNoResource();
     void on_errResourceDuplicate();
     void on_errMessage(const Jabberlue::Message& m);

 private:
     JSContext* _jscontext;
     Jabberlue::Session* _session;
     string _OnBegin;
     string _OnEnd;
     string _OnError;
     string _OnErrorNoConfig;
     string _OnErrorNoPassword;
     string _OnErrorNoResource;
     string _OnErrorResourceDuplicate;
     string _OnMessage;
     string _OnMessageObject;
     string _OnIQ;
     string _OnPresence;
     bool _connected;
     nsCOMPtr<sashIActionRuntime> _act;
     bool _newuser;
     sashIJabberRoster *m_roster;
};

#endif
