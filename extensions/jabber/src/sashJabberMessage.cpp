/* sashJabberMessage.cpp
 * Message object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabberMessage.h"
#include "extensiontools.h"
#include <string>

#include <jabberoo.hh>
#include <jabberlue.hh>

NS_IMPL_ISUPPORTS2_CI(sashJabberMessage, sashIJabberMessage, sashIConstructor)

sashJabberMessage::sashJabberMessage()
{
  NS_INIT_ISUPPORTS();
}

sashJabberMessage::~sashJabberMessage()
{
  /* destructor code */
     delete _message;
}

NS_IMETHODIMP sashJabberMessage::InitializeNewObject(sashIActionRuntime * act,
													 sashIExtension* ext, JSContext* cx, 
													 PRUint32 argc, nsIVariant **argv, 
													 PRBool *ret)
{
     // Create a new blank message
     // TODO: accept arguments in the nsIVariant and create a new message based on them
     _message = new Jabberlue::Message("", "", jabberoo::Message::mtChat);

     *ret = true;
     return NS_OK;
}

NS_IMETHODIMP sashJabberMessage::Initialize(JSContext* cx, const Jabberlue::Message* msg)
{
     // Create a new blank message
     _message = new Jabberlue::Message(*msg);

     return NS_OK;
}


/* attribute string ID; */
NS_IMETHODIMP sashJabberMessage::GetID(char * *aID)
{
     XP_COPY_STRING(_message->getID().c_str(), aID);
  return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetID(const char * aID)
{
     _message->setID(aID);
     return NS_OK;
}

/* attribute string To; */
NS_IMETHODIMP sashJabberMessage::GetTo(char * *aTo)
{
     XP_COPY_STRING(_message->getTo().c_str(), aTo);
  return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetTo(const char * aTo)
{
     _message->setTo(aTo);
     return NS_OK;
}

/* attribute string From; */
NS_IMETHODIMP sashJabberMessage::GetFrom(char * *aFrom)
{
     XP_COPY_STRING(_message->getFrom().c_str(), aFrom);
  return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetFrom(const char * aFrom)
{
     _message->setFrom(aFrom);
     return NS_OK;
}

/* attribute string Subject; */
NS_IMETHODIMP sashJabberMessage::GetSubject(char * *aSubject)
{
     XP_COPY_STRING(_message->getSubject().c_str(), aSubject);
  return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetSubject(const char * aSubject)
{
     _message->setSubject(aSubject);
     return NS_OK;
}

/* attribute string Body; */
NS_IMETHODIMP sashJabberMessage::GetBody(char * *aBody)
{
     XP_COPY_STRING(_message->getBody().c_str(), aBody);
  return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetBody(const char * aBody)
{
     _message->setBody(aBody);
     return NS_OK;
}

/* attribute string Thread; */
NS_IMETHODIMP sashJabberMessage::GetThread(char * *aThread)
{
     XP_COPY_STRING(_message->getThread().c_str(), aThread);
  return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetThread(const char * aThread)
{
     _message->setThread(aThread);
     return NS_OK;
}

/* attribute short Type; */
NS_IMETHODIMP sashJabberMessage::GetType(PRInt16 *aType)
{
     if (!aType) return NS_ERROR_NULL_POINTER;
     *aType = _message->getType();
     return NS_OK;
}
NS_IMETHODIMP sashJabberMessage::SetType(PRInt16 aType)
{
     _message->setType((jabberoo::Message::Type)(aType));
     return NS_OK;
}

/* void AddExtension (in string xmlns, in string xmlelement, in string xml); */
NS_IMETHODIMP sashJabberMessage::AddExtension(const char *xmlns, const char* xmlelement, const char *xml)
{
     judo::Element* x = _message->addX(xmlns);
     x->addElement(xmlelement, xml);
     return NS_OK;
}

/* string GetExtension (in string xmlns, in string xmlelement); */
NS_IMETHODIMP sashJabberMessage::GetExtension(const char *xmlns, const char *xmlelement, char **_retval)
{
     judo::Element* x = _message->findX(xmlns);
     string ext_data = x->findElement(xmlelement)->toString();
     ext_data.erase(0, ext_data.find('>') + 1);
     ext_data.erase(ext_data.find("</" + string(xmlelement)));
     XP_COPY_STRING(ext_data.c_str(), _retval);
  return NS_OK;
}

/* [noscript] nativeJabberlueMessage getMessage (); */
NS_IMETHODIMP sashJabberMessage::GetMessage(Jabberlue::Message * *_retval)
{
     *_retval = _message;
     return NS_OK;
}
