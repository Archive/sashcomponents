/* sashJabberPresenceInfo.cpp
 * Presence Info for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabberPresenceInfo.h"
#include "extensiontools.h"
#include <string>

#include <jabberoo.hh>
#include <jabberlue.hh>

NS_IMPL_ISUPPORTS2_CI(sashJabberPresenceInfo, sashIJabberPresenceInfo, sashIConstructor)

sashJabberPresenceInfo::sashJabberPresenceInfo()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

sashJabberPresenceInfo::~sashJabberPresenceInfo()
{
  /* destructor code */
}

NS_IMETHODIMP sashJabberPresenceInfo::InitializeNewObject(sashIActionRuntime * act,
														  sashIExtension* ext, JSContext* cx, 
														  PRUint32 argc, nsIVariant **argv, 
														  PRBool *ret)
{
     *ret = true;
     return NS_OK;
}

/* attribute short Status; */
NS_IMETHODIMP sashJabberPresenceInfo::GetStatus(PRInt16 *aStatus)
{
     if (!aStatus) return NS_ERROR_NULL_POINTER;
     *aStatus = _status;
     return NS_OK;
}
NS_IMETHODIMP sashJabberPresenceInfo::SetStatus(PRInt16 aStatus)
{
     _status = aStatus;
     return NS_OK;
     return NS_OK;
}

/* attribute string StatusMsg; */
NS_IMETHODIMP sashJabberPresenceInfo::GetStatusMsg(char * *aStatusMsg)
{
     XP_COPY_STRING(_statusmsg.c_str(), aStatusMsg);
     return NS_OK;
}
NS_IMETHODIMP sashJabberPresenceInfo::SetStatusMsg(const char * aStatusMsg)
{
     _statusmsg = aStatusMsg;
     return NS_OK;
}
