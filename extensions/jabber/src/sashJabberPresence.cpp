/* sashJabberPresence.cpp
 * Presence object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabberPresence.h"
#include "extensiontools.h"
#include <string>

#include <jabberoo.hh>
#include <jabberlue.hh>

NS_IMPL_ISUPPORTS2_CI(sashJabberPresence, sashIJabberPresence, sashIConstructor)

sashJabberPresence::sashJabberPresence()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

sashJabberPresence::~sashJabberPresence()
{
  /* destructor code */
     delete _presence;
}

NS_IMETHODIMP sashJabberPresence::InitializeNewObject(sashIActionRuntime * act,
													  sashIExtension* ext, JSContext* cx, 
													  PRUint32 argc, nsIVariant **argv, 
													  PRBool *ret)
{
     // Create a new blank presence
     // TODO: accept arguments in the nsIVariant and create a new message based on them
     _presence = new Jabberlue::Presence("", 
					 jabberoo::Presence::ptAvailable, 
					 jabberoo::Presence::stOnline);

     *ret = true;
     return NS_OK;
}

NS_IMETHODIMP sashJabberPresence::Initialize(JSContext* cx, const Jabberlue::Presence* p)
{
     // Create a new blank presence
     _presence = new Jabberlue::Presence(*p);

     return NS_OK;
}


/* attribute string ID; */
NS_IMETHODIMP sashJabberPresence::GetID(char * *aID)
{
     XP_COPY_STRING(_presence->getID().c_str(), aID);
  return NS_OK;
}
NS_IMETHODIMP sashJabberPresence::SetID(const char * aID)
{
     _presence->setID(aID);
     return NS_OK;
}

/* attribute string To; */
NS_IMETHODIMP sashJabberPresence::GetTo(char * *aTo)
{
     XP_COPY_STRING(_presence->getTo().c_str(), aTo);
  return NS_OK;
}
NS_IMETHODIMP sashJabberPresence::SetTo(const char * aTo)
{
     _presence->setTo(aTo);
     return NS_OK;
}

/* attribute string From; */
NS_IMETHODIMP sashJabberPresence::GetFrom(char * *aFrom)
{
     XP_COPY_STRING(_presence->getFrom().c_str(), aFrom);
  return NS_OK;
}
NS_IMETHODIMP sashJabberPresence::SetFrom(const char * aFrom)
{
     _presence->setFrom(aFrom);
     return NS_OK;
}

/* attribute short Status; */
NS_IMETHODIMP sashJabberPresence::GetStatus(PRInt16 *aStatus)
{
     if (!aStatus) return NS_ERROR_NULL_POINTER;
     *aStatus = _presence->getShow();
     return NS_OK;
}
NS_IMETHODIMP sashJabberPresence::SetStatus(PRInt16 aStatus)
{
     _presence->setShow((jabberoo::Presence::Show)(aStatus));
     return NS_OK;
     return NS_OK;
}

/* attribute string StatusMsg; */
NS_IMETHODIMP sashJabberPresence::GetStatusMsg(char * *aStatusMsg)
{
     XP_COPY_STRING(_presence->getStatus().c_str(), aStatusMsg);
  return NS_OK;
}
NS_IMETHODIMP sashJabberPresence::SetStatusMsg(const char * aStatusMsg)
{
     _presence->setStatus(aStatusMsg);
     return NS_OK;
}

/* attribute string Priority; */
NS_IMETHODIMP sashJabberPresence::GetPriority(char * *aPriority)
{
//     XP_COPY_STRING(_presence->getPriority().c_str(), aFrom);
//  return NS_OK;
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashJabberPresence::SetPriority(const char * aPriority)
{
     _presence->setPriority(aPriority);
     return NS_OK;
}

/* attribute short Type; */
NS_IMETHODIMP sashJabberPresence::GetType(PRInt16 *aType)
{
     if (!aType) return NS_ERROR_NULL_POINTER;
     *aType = _presence->getType();
     return NS_OK;
}
NS_IMETHODIMP sashJabberPresence::SetType(PRInt16 aType)
{
     _presence->setType((jabberoo::Presence::Type)(aType));
     return NS_OK;
}

/* void AddExtension (in string xmlns, in string xmlelement, in string xml); */
NS_IMETHODIMP sashJabberPresence::AddExtension(const char *xmlns, const char* xmlelement, const char *xml)
{
     judo::Element* x = _presence->addX(xmlns);
     x->addElement(xmlelement, xml);
     return NS_OK;
}

/* string GetExtension (in string xmlns, in string xmlelement); */
NS_IMETHODIMP sashJabberPresence::GetExtension(const char *xmlns, const char *xmlelement, char **_retval)
{
     judo::Element* x = _presence->findX(xmlns);
     XP_COPY_STRING(x->findElement(xmlelement)->toString().c_str(), _retval);
  return NS_OK;
}


/* [noscript] nativeJabberluePresence getPresence (); */
NS_IMETHODIMP sashJabberPresence::GetPresence(Jabberlue::Presence * *_retval)
{
     *_retval = _presence;
     return NS_OK;
}
