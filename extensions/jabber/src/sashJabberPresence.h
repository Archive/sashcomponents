/* sashJabberPresence.h
 * Presence object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBERPRESENCE_H
#define SASHJABBERPRESENCE_H

#include "nsID.h"
#include "sashIJabberPresence.h"
#include "sashIConstructor.h"

#include <jabberoo.hh>
#include <jabberlue.hh>

// CID: 202a36e0-c0c7-4f4a-8f3b-594314428303

#define SASHJABBERPRESENCE_CONTRACT_ID "@gnome.org/SashXB/sashJabberPresence;1"
#define SASHJABBERPRESENCE_CID {0x202a36e0, 0xc0c7, 0x4f4a, {0x8f, 0x3b, 0x59, 0x43, 0x14, 0x42, 0x83, 0x03}}
NS_DEFINE_CID(ksashJabberPresenceCID, SASHJABBERPRESENCE_CID);

class sashJabberPresence : public sashIJabberPresence,
			   public sashIConstructor
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHICONSTRUCTOR;
     NS_DECL_SASHIJABBERPRESENCE;

     sashJabberPresence();
     virtual ~sashJabberPresence();
     
 private:
     Jabberlue::Presence* _presence;
};

#endif
