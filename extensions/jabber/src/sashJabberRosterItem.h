/* sashJabberRosterItem.h
 * Roster Item object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#ifndef SASHJABBERROSTERITEM_H
#define SASHJABBERROSTERITEM_H

#include "nsID.h"
#include "sashIJabberRosterItem.h"
#include "sashIConstructor.h"

#include <jabberoo.hh>
#include <jabberlue.hh>

// CID: 6701c03c-9620-4e9d-b9e5-7ce1dccf3849

#define SASHJABBERROSTERITEM_CONTRACT_ID "@gnome.org/SashXB/sashJabberRosterItem;1"
#define SASHJABBERROSTERITEM_CID {0x6701c03c, 0x9620, 0x4e9d, {0xb9, 0xe5, 0x7c, 0xe1, 0xdc, 0xcf, 0x38, 0x49}}
NS_DEFINE_CID(ksashJabberRosterItemCID, SASHJABBERROSTERITEM_CID);

class sashJabberRosterItem : public sashIJabberRosterItem,
			   public sashIConstructor
{
 public:
     NS_DECL_ISUPPORTS;
     NS_DECL_SASHICONSTRUCTOR;
     NS_DECL_SASHIJABBERROSTERITEM;

     sashJabberRosterItem();
     virtual ~sashJabberRosterItem();
     
 private:
     jabberoo::Session* _session;
     jabberoo::Roster::Item* _rosteritem;
};

#endif
