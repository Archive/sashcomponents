/* sashJabberRosterItem.cpp
 * Roster Item object for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabberRosterItem.h"
#include "extensiontools.h"
#include <string>

#include <jabberoo.hh>
#include <jabberlue.hh>

NS_IMPL_ISUPPORTS2_CI(sashJabberRosterItem, sashIJabberRosterItem, sashIConstructor)

sashJabberRosterItem::sashJabberRosterItem()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

sashJabberRosterItem::~sashJabberRosterItem()
{
  /* destructor code */
     delete _rosteritem;
}

NS_IMETHODIMP sashJabberRosterItem::InitializeNewObject(sashIActionRuntime * act,
														sashIExtension* ext, JSContext* cx, 
														PRUint32 argc, nsIVariant **argv, 
														PRBool *ret)
{
     // Create a new blank presence
     // TODO: accept arguments in the nsIVariant and create a new message based on them
     _rosteritem = new jabberoo::Roster::Item("", "");

     *ret = true;
     return NS_OK;
}

/* [noscript] void Initialize (in nativeJSContext cx, in nativeJabberooSession js, in nativeJabberooRosterItem ri); */
NS_IMETHODIMP sashJabberRosterItem::Initialize(JSContext * cx, jabberoo::Session * s, jabberoo::Roster::Item * ri)
{
     _session = s;
     _rosteritem = new jabberoo::Roster::Item(_session->roster(), ri->getBaseElement());
     return NS_OK;
}

/* readonly attribute string JID; */
NS_IMETHODIMP sashJabberRosterItem::GetJID(char * *aJID)
{
     XP_COPY_STRING(_rosteritem->getJID().c_str(), aJID);
     return NS_OK;
}

/* attribute string NickName; */
NS_IMETHODIMP sashJabberRosterItem::GetNickName(char * *aNickName)
{
     XP_COPY_STRING(_rosteritem->getNickname().c_str(), aNickName);
     return NS_OK;
}
NS_IMETHODIMP sashJabberRosterItem::SetNickName(const char * aNickName)
{
     _rosteritem->setNickname(aNickName);
     return NS_OK;
}

/* attribute nsIVariant Groups; */
NS_IMETHODIMP sashJabberRosterItem::GetGroups(nsIVariant * *aGroups)
{
     return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashJabberRosterItem::SetGroups(nsIVariant * aGroups)
{
     return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute short Status; */
NS_IMETHODIMP sashJabberRosterItem::GetStatus(PRInt16 * aStatus)
{
     try {
//     	  const jabberoo::Presence& p = G_App->getSession().presenceDB().findExact(_rosteritem->JID());
//	  *aStatus = p.getShow();
     } catch (jabberoo::PresenceDB::XCP_InvalidJID& e) {
     }
     return NS_OK;
}

/* readonly attribute string StatusMessage; */
NS_IMETHODIMP sashJabberRosterItem::GetStatusMessage(char * *aStatusMessage)
{
     try {
//     	  const jabberoo::Presence& p = G_App->getSession().presenceDB().findExact(_rosteritem->JID());
//        XP_COPY_STRING(p.getStatus().c_str(), aStatusMessage);
     } catch (jabberoo::PresenceDB::XCP_InvalidJID& e) {
     }
     return NS_OK;
}

/* readonly attribute bool SubscriptionTo; */
NS_IMETHODIMP sashJabberRosterItem::GetSubscriptionTo(PRBool *SubscriptionTo)
{
     try {
//     	  const jabberoo::Presence& p = G_App->getSession().presenceDB().findExact(_rosteritem->JID());
//      getSubsType() rsNone, rsTo, rsFrom, rsBoth
     } catch (jabberoo::PresenceDB::XCP_InvalidJID& e) {
     }
     return NS_OK;
}

/* readonly attribute bool PendingSubscriptionTo; */
NS_IMETHODIMP sashJabberRosterItem::GetPendingSubscriptionTo(PRBool *PendingSubscriptionTo)
{
     try {
//     	  const jabberoo::Presence& p = G_App->getSession().presenceDB().findExact(_rosteritem->JID());
//      getSubsType() rsNone, rsTo, rsFrom, rsBoth
     } catch (jabberoo::PresenceDB::XCP_InvalidJID& e) {
     }
     return NS_OK;
}

/* readonly attribute bool SubscriptionFrom; */
NS_IMETHODIMP sashJabberRosterItem::GetSubscriptionFrom(PRBool *SubscriptionFrom)
{
     try {
//     	  const jabberoo::Presence& p = G_App->getSession().presenceDB().findExact(_rosteritem->JID());
//      getSubsType() rsNone, rsTo, rsFrom, rsBoth
     } catch (jabberoo::PresenceDB::XCP_InvalidJID& e) {
     }
     return NS_OK;
}

/* void Synchronize (); */
NS_IMETHODIMP sashJabberRosterItem::Synchronize()
{
     return NS_OK;
}

/* [noscript] nativeJabberooRosterItem getRosterItem (); */
NS_IMETHODIMP sashJabberRosterItem::GetRosterItem(jabberoo::Roster::Item * *_retval)
{
     *_retval = _rosteritem;
     return NS_OK;
}
