/* sashJabber.cc
 * Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabber.h"
#include "sashJabberSession.h"
#include "sashJabberMessage.h"
#include "sashJabberJID.h"
#include "sashJabberRosterItem.h"
#include "extensiontools.h"
#include "sashIGenericConstructor.h"
#include <string>
#include "secman.h"
#include <jabberlue-conf-write.hh>
#include <jabberlue-conf-read.hh>
  
SASH_EXTENSION_IMPL_NO_NSGET(sashJabber, sashIJabber, "Jabber");

enum JabberSecurity {
	 JS_CREATE_SESSION = 0
};

sashJabber::sashJabber()
{
     NS_INIT_ISUPPORTS();
     nsresult rv = nsComponentManager::CreateInstance(ksashJabberJIDCID,
						      NULL, NS_GET_IID(sashIJabberJID), (void **) &m_jid);
     if (NS_FAILED(rv))
	  DEBUGMSG(sashJabber, "CreateInstance failed. Death and destruction shall fall upon thee!");
}

sashJabber::~sashJabber()
{
  /* destructor code */
     NS_IF_RELEASE(m_sessionConstructor);
     NS_IF_RELEASE(m_messageConstructor);
     NS_IF_RELEASE(m_jid);
     NS_IF_RELEASE(m_rosteritemConstructor);
}

NS_IMETHODIMP sashJabber::ProcessEvent()
{
     return NS_OK;
}

NS_IMETHODIMP sashJabber::Cleanup()
{
     return NS_OK;
}

NS_IMETHODIMP sashJabber::Initialize(sashIActionRuntime *act,
				     const char* registrationXML,
				     const char* webGUID,
				     JSContext *cx, JSObject *obj)
{
     act->GetSecurityManager(&m_SecurityManager);
     
     NewSashConstructor(act, this, SASHJABBERSESSION_CONTRACT_ID, 
			SASHIJABBERSESSION_IID_STR, &m_sessionConstructor);
     NewSashConstructor(act, this, SASHJABBERMESSAGE_CONTRACT_ID, 
			SASHIJABBERMESSAGE_IID_STR, &m_messageConstructor);
     NewSashConstructor(act, this, SASHJABBERROSTERITEM_CONTRACT_ID,
			SASHIJABBERROSTERITEM_IID_STR, &m_rosteritemConstructor);

     DEBUGMSG(sashJabber, "Initialize");
     return NS_OK;
}

/* readonly attribute sashIGenericConstructor Session; */
NS_IMETHODIMP sashJabber::GetSession(sashIGenericConstructor * *aSession)
{
     AssertSecurity(m_SecurityManager, ToUpper(ksashJabberCID.ToString()), JS_CREATE_SESSION, true);
     NS_IF_ADDREF(m_sessionConstructor);
     *aSession = m_sessionConstructor;
     return NS_OK;
}

/* readonly attribute sashIGenericConstructor Message; */
NS_IMETHODIMP sashJabber::GetMessage(sashIGenericConstructor * *aMessage)
{
     NS_IF_ADDREF(m_messageConstructor);
     *aMessage = m_messageConstructor;
     return NS_OK;
}

/* readonly attribute sashIJabberJID JID; */
NS_IMETHODIMP sashJabber::GetJID(sashIJabberJID * *aJID)
{
     NS_IF_ADDREF(m_jid);
     *aJID = m_jid;
     return NS_OK;
}

/* readonly attribute sashIGenericConstructor RosterItem; */
NS_IMETHODIMP sashJabber::GetRosterItem(sashIGenericConstructor * *aRosterItem)
{
     NS_IF_ADDREF(m_rosteritemConstructor);
     *aRosterItem = m_rosteritemConstructor;
     return NS_OK;
}

/* readonly attribute short MESSAGE_NORMAL; */
NS_IMETHODIMP sashJabber::GetMESSAGE_NORMAL(PRInt16 *aMESSAGE_NORMAL)
{
     if (!aMESSAGE_NORMAL) return NS_ERROR_NULL_POINTER;
     *aMESSAGE_NORMAL = jabberoo::Message::mtNormal;
     return NS_OK;
}

/* readonly attribute short MESSAGE_ERROR; */
NS_IMETHODIMP sashJabber::GetMESSAGE_ERROR(PRInt16 *aMESSAGE_ERROR)
{
     if (!aMESSAGE_ERROR) return NS_ERROR_NULL_POINTER;
     *aMESSAGE_ERROR = jabberoo::Message::mtError;
     return NS_OK;
}

/* readonly attribute short MESSAGE_CHAT; */
NS_IMETHODIMP sashJabber::GetMESSAGE_CHAT(PRInt16 *aMESSAGE_CHAT)
{
     if (!aMESSAGE_CHAT) return NS_ERROR_NULL_POINTER;
     *aMESSAGE_CHAT = jabberoo::Message::mtChat;
     return NS_OK;
}

/* readonly attribute short MESSAGE_GROUPCHAT; */
NS_IMETHODIMP sashJabber::GetMESSAGE_GROUPCHAT(PRInt16 *aMESSAGE_GROUPCHAT)
{
     if (!aMESSAGE_GROUPCHAT) return NS_ERROR_NULL_POINTER;
     *aMESSAGE_GROUPCHAT = jabberoo::Message::mtGroupchat;
     return NS_OK;
}

/* readonly attribute short MESSAGE_HEADLINE; */
NS_IMETHODIMP sashJabber::GetMESSAGE_HEADLINE(PRInt16 *aMESSAGE_HEADLINE)
{
     if (!aMESSAGE_HEADLINE) return NS_ERROR_NULL_POINTER;
     *aMESSAGE_HEADLINE = jabberoo::Message::mtHeadline;
     return NS_OK;
}

/* readonly attribute short STATUS_UNKNOWN; */
NS_IMETHODIMP sashJabber::GetSTATUS_UNKNOWN(PRInt16 *aSTATUS_UNKNOWN)
{
     if (!aSTATUS_UNKNOWN) return NS_ERROR_NULL_POINTER;
     *aSTATUS_UNKNOWN = jabberoo::Presence::stInvalid;
     return NS_OK;
}

/* readonly attribute short STATUS_OFFLINE; */
NS_IMETHODIMP sashJabber::GetSTATUS_OFFLINE(PRInt16 *aSTATUS_OFFLINE)
{
     if (!aSTATUS_OFFLINE) return NS_ERROR_NULL_POINTER;
     *aSTATUS_OFFLINE = jabberoo::Presence::stOffline;
     return NS_OK;
}

/* readonly attribute short STATUS_ONLINE; */
NS_IMETHODIMP sashJabber::GetSTATUS_ONLINE(PRInt16 *aSTATUS_ONLINE)
{
     DEBUGMSG(sashJabber, "GetSTATUS_ONLINE called.\n");
     if (!aSTATUS_ONLINE) return NS_ERROR_NULL_POINTER;
     *aSTATUS_ONLINE = jabberoo::Presence::stOnline;
     return NS_OK;
}

/* readonly attribute short STATUS_ONLINE_CHAT; */
NS_IMETHODIMP sashJabber::GetSTATUS_ONLINE_CHAT(PRInt16 *aSTATUS_ONLINE_CHAT)
{
     if (!aSTATUS_ONLINE_CHAT) return NS_ERROR_NULL_POINTER;
     *aSTATUS_ONLINE_CHAT = jabberoo::Presence::stChat;
     return NS_OK;
}

/* readonly attribute short STATUS_AWAY; */
NS_IMETHODIMP sashJabber::GetSTATUS_AWAY(PRInt16 *aSTATUS_AWAY)
{
     if (!aSTATUS_AWAY) return NS_ERROR_NULL_POINTER;
     *aSTATUS_AWAY = jabberoo::Presence::stAway;
     return NS_OK;
}

/* readonly attribute short STATUS_EXT_AWAY; */
NS_IMETHODIMP sashJabber::GetSTATUS_EXT_AWAY(PRInt16 *aSTATUS_EXT_AWAY)
{
     if (!aSTATUS_EXT_AWAY) return NS_ERROR_NULL_POINTER;
     *aSTATUS_EXT_AWAY = jabberoo::Presence::stXA;
     return NS_OK;
}

/* readonly attribute short STATUS_DND; */
NS_IMETHODIMP sashJabber::GetSTATUS_DND(PRInt16 *aSTATUS_DND)
{
     if (!aSTATUS_DND) return NS_ERROR_NULL_POINTER;
     *aSTATUS_DND = jabberoo::Presence::stDND;
     return NS_OK;
}

/* readonly attribute short PRESENCE_SUBREQUEST; */
NS_IMETHODIMP sashJabber::GetPRESENCE_SUBREQUEST(PRInt16 *aPRESENCE_SUBREQUEST)
{
     if (!aPRESENCE_SUBREQUEST) return NS_ERROR_NULL_POINTER;
     *aPRESENCE_SUBREQUEST = jabberoo::Presence::ptSubRequest;
     return NS_OK;
}

/* readonly attribute short PRESENCE_UNSUBREQUEST; */
NS_IMETHODIMP sashJabber::GetPRESENCE_UNSUBREQUEST(PRInt16 *aPRESENCE_UNSUBREQUEST)
{
     if (!aPRESENCE_UNSUBREQUEST) return NS_ERROR_NULL_POINTER;
     *aPRESENCE_UNSUBREQUEST = jabberoo::Presence::ptUnsubRequest;
     return NS_OK;
}

/* readonly attribute short PRESENCE_SUBSCRIBED; */
NS_IMETHODIMP sashJabber::GetPRESENCE_SUBSCRIBED(PRInt16 *aPRESENCE_SUBSCRIBED)
{
     if (!aPRESENCE_SUBSCRIBED) return NS_ERROR_NULL_POINTER;
     *aPRESENCE_SUBSCRIBED = jabberoo::Presence::ptSubscribed;
     return NS_OK;
}

/* readonly attribute short PRESENCE_UNSUBSCRIBED; */
NS_IMETHODIMP sashJabber::GetPRESENCE_UNSUBSCRIBED(PRInt16 *aPRESENCE_UNSUBSCRIBED)
{
     if (!aPRESENCE_UNSUBSCRIBED) return NS_ERROR_NULL_POINTER;
     *aPRESENCE_UNSUBSCRIBED = jabberoo::Presence::ptUnsubscribed;
     return NS_OK;
}

/* readonly attribute short PRESENCE_AVAILABLE; */
NS_IMETHODIMP sashJabber::GetPRESENCE_AVAILABLE(PRInt16 *aPRESENCE_AVAILABLE)
{
     if (!aPRESENCE_AVAILABLE) return NS_ERROR_NULL_POINTER;
     *aPRESENCE_AVAILABLE = jabberoo::Presence::ptAvailable;
     return NS_OK;
}

/* readonly attribute short PRESENCE_UNAVAILABLE; */
NS_IMETHODIMP sashJabber::GetPRESENCE_UNAVAILABLE(PRInt16 *aPRESENCE_UNAVAILABLE)
{
     if (!aPRESENCE_UNAVAILABLE) return NS_ERROR_NULL_POINTER;
     *aPRESENCE_UNAVAILABLE = jabberoo::Presence::ptUnavailable;
     return NS_OK;
}

NS_IMETHODIMP sashJabber::SetConfiguration(const char* username, const char* password,
										   const char* server) {
	 char *v[] = { NULL };
	 JabberlueConfWrite w(0, v);
	 w.set_username(username);
	 w.set_password(password);
	 w.set_port(5222);
	 w.set_server(server);
	 w.sync();

	 return NS_OK;
}

NS_IMETHODIMP sashJabber::GetIsConfigured(PRBool* _retval) {
	 char *v[] = { NULL };
	 JabberlueConfRead r(0, v);
	 *_retval = ! (r.get_server().empty() || r.get_username().empty());
	 return NS_OK;
}
