/* sashJabberJID.cpp
 * JID functions for the Jabber extension for Sash XB
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Pubic License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "sashJabberJID.h"
#include "extensiontools.h"
#include <string>

#include <jabberoo.hh>
#include <jabberlue.hh>

NS_IMPL_ISUPPORTS1_CI(sashJabberJID, sashIJabberJID)

sashJabberJID::sashJabberJID()
{
  NS_INIT_ISUPPORTS();
}

sashJabberJID::~sashJabberJID()
{
  /* destructor code */
}

/* string GetResource (in string jid); */
NS_IMETHODIMP sashJabberJID::GetResource(const char *jid, char **_retval)
{
     XP_COPY_STRING(jabberoo::JID::getResource(jid).c_str(), _retval);
	 return NS_OK;
}

/* string GetUserHost (in string jid); */
NS_IMETHODIMP sashJabberJID::GetUserHost(const char *jid, char **_retval)
{
     XP_COPY_STRING(jabberoo::JID::getUserHost(jid).c_str(), _retval);
	 return NS_OK;
}

/* string GetHost (in string jid); */
NS_IMETHODIMP sashJabberJID::GetHost(const char *jid, char **_retval)
{
     XP_COPY_STRING(jabberoo::JID::getHost(jid).c_str(), _retval);
	 return NS_OK;
}

/* string GetUser (in string jid); */
NS_IMETHODIMP sashJabberJID::GetUser(const char *jid, char **_retval)
{
     XP_COPY_STRING(jabberoo::JID::getUser(jid).c_str(), _retval);
	 return NS_OK;
}
