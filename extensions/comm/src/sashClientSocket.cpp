
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the ClientSocket object 
******************************************************************/
#include <strstream>
#include <sys/utsname.h>
#include "sashSockets.h"
#include "sashClientSocket.h"
#include "sashnet.h"

#include "sashVariantUtils.h"
#include "extensiontools.h"
#include "nsCRT.h"
#include "sashIExtension.h"
#include <string>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>

NS_IMPL_ISUPPORTS2_CI(sashClientSocket, sashIClientSocket, sashIConstructor);

sashClientSocket::sashClientSocket()
{
	 DEBUGMSG(comm, "sashClientSocket constructor\n");
	 NS_INIT_ISUPPORTS();
	 m_connected = false;
	 m_toBeClosed = false;
	 m_sockfd = -1;
	 m_receivedByteCount = 0;
	 m_sentByteCount = 0;
	 pthread_mutex_init(&m_clientLock, NULL);
}

sashClientSocket::~sashClientSocket()
{
	 if (m_sendBuffer) delete m_sendBuffer;
	 if (m_receiveBuffer) delete m_receiveBuffer;
	 pthread_mutex_destroy(&m_clientLock);
}

NS_IMETHODIMP
sashClientSocket::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
									  PRUint32 argc,nsIVariant **argv, PRBool *ret)
{
	 m_sendBuffer = new streamBuffer();
	 m_receiveBuffer = new streamBuffer();

	 *ret = true;
	 if ((m_sendBuffer == NULL) || (m_receiveBuffer == NULL)){
		  DEBUGMSG(comm, "not enough memory to create send and receive buffers\n");
		  *ret = false;
	 }

	 m_cx = cx;
	 return NS_OK;
}

/* void Close (in nsIVariant bForce); */
NS_IMETHODIMP sashClientSocket::Close(nsIVariant *bForce)
{
	 bool force = VariantIsEmpty(bForce) ? false : VariantGetBoolean(bForce);

	 DEBUGMSG(comm, "close request from sashDatagramSocket\n");

	 lockClient();
	 bool sendBufferEmpty = m_sendBuffer->empty();
	 bool receiveBufferEmpty = m_receiveBuffer->empty();
	 bool connected = m_connected;
	 unlockClient();
	 
	 if (connected){
		  if ((force)
			  || ((sendBufferEmpty)
				  && (receiveBufferEmpty)))
		  {
			   sashSockets::m_scm->addCSocketToCloseQueue(this);
		  } else {
			   // can't close just yet.
			   lockClient();
			   m_toBeClosed = true;
			   unlockClient();
		  }
	 }
	 return NS_OK;
}

/* PRBool Connect (in string remoteHost, in PRInt32 remotePort, in string socksHost, in nsIVariant socksPort); */
NS_IMETHODIMP sashClientSocket::Connect(const char *remoteHost, PRInt32 remotePort, nsIVariant *socksHost, nsIVariant *socksPort, PRBool *_retval)
{
	 // *** ignore socks stuff for now.
	 lockClient();
	 bool connected = m_connected;
	 unlockClient();

	 if (! connected){
		  *_retval = true;

		  lockClient();
		  m_toBeClosed = false;
		  setRemoteAddr(&m_remoteAddr, remoteHost, (unsigned short)remotePort);
		  unlockClient();

		  sashSockets::m_scm->addClientToConnectionQueue(this);
	 }
	 return NS_OK;
}

/* string Receive (in nsIVariant bReceiveAsBinary, in nsIVariant nBytesToReceive); */
NS_IMETHODIMP sashClientSocket::Receive(nsIVariant *bReceiveAsBinary, nsIVariant *nBytesToReceive, char **_retval)
{
	 // don't need to lock anything; the buffer is thread-safe.
	 DEBUGMSG(comm, "sashClientSocket::Receive\n");

	 int bytesToReceive = (VariantIsEmpty(nBytesToReceive) ? 
						   m_receiveBuffer->getSize() 
						   : (int)VariantGetNumber(nBytesToReceive));

	 char *buf = new char[bytesToReceive+1];
	 if (buf == NULL){
		  DEBUGMSG(comm, "couldn't allocate memory for buffer to receive client data\n");
		  return NS_ERROR_FAILURE;
	 } else {
		  int bytesReceived = m_receiveBuffer->getData(bytesToReceive, buf);
		  buf[bytesReceived] = '\0';

		  DEBUGMSG(comm, "sashClientSocket::Receive: received %d / %d bytes\n",
				   bytesReceived, bytesToReceive);

		  // check to see whether this is supposed to be closed.
		  lockClient();
		  bool receiveBufferEmpty = m_receiveBuffer->empty();
		  bool sendBufferEmpty = m_sendBuffer->empty();
		  bool toBeClosed = m_toBeClosed;
		  unlockClient();

		  if (receiveBufferEmpty && sendBufferEmpty && toBeClosed){
			   sashSockets::m_scm->addCSocketToCloseQueue(this);
		  }

	 }
	 XP_COPY_STRING(buf, _retval);
	 return NS_OK;
}

/* PRInt32 SearchPendingReceiveDat (in string searchFor); */
NS_IMETHODIMP sashClientSocket::SearchPendingReceiveDat(const char *searchFor, PRInt32 *_retval)
{ 
	 // pass it to the receive stream buffer.
	 int startIndex = m_receiveBuffer->findInBuffer(searchFor);

	 // if found, skip to end of buffer.
	 int temp;
	 if (startIndex == -1)
		  temp = -1;
	 else
		  temp = (startIndex + strlen(searchFor));


	 *_retval = temp;
	 
	 return NS_OK;
}

/* PRBool Send (in nsIVariant vData); */
NS_IMETHODIMP sashClientSocket::Send(nsIVariant *vData, PRBool *_retval)
{
	 string strdata = VariantGetString(vData);
	 const char *buf = strdata.c_str();
	 int bufsize = strdata.length();
	 bool sendDataReady = false;

	 // don't bother if we're not connected or this is meant to be closed.
	 lockClient();
	 if (m_connected && (! m_toBeClosed) && (bufsize != 0)){
		  m_sendBuffer->appendData(bufsize, (char *)buf);
		  DEBUGMSG(comm, "sashClientSocket::Send (fd%d): appending %d bytes to buffer: %s\n", m_sockfd, bufsize, buf);
		  sendDataReady = true;
	 } else {
		  DEBUGMSG(comm, "sashClientSocket::Send: didn't send. not connected or about to be closed\n");

	 }
	 unlockClient();
	 
	 if (sendDataReady) dataToSend();
	 return NS_OK;
}

/* attribute nsIVariant Context; */
NS_IMETHODIMP sashClientSocket::GetContext(nsIVariant * *aContext)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashClientSocket::SetContext(nsIVariant * aContext)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute PRBool Connected; */
NS_IMETHODIMP sashClientSocket::GetConnected(PRBool *aConnected)
{
	 *aConnected = isConnected();
	 return NS_OK;
}

bool sashClientSocket::isConnected(){
	 lockClient();
	 bool retval = m_connected;
	 unlockClient();
	 return retval;
}

/* readonly attribute string RemoteHost; */
NS_IMETHODIMP sashClientSocket::GetRemoteHost(char * *aRemoteHost)
{
	 string str;
	 lockClient();
	 if (m_connected){
		  // assume that struct sockaddr_in has been filled in, either
		  // by ClientSocket's Connect method or ServerSocket's
		  // AcceptConnection.
		  str = inet_ntoa(m_remoteAddr.sin_addr); 
	 }
	 else{
		  str = "";
	 }
	 unlockClient();
	 XP_COPY_STRING(str.c_str(), aRemoteHost);
	 return NS_OK;
	 
}

NS_IMETHODIMP sashClientSocket::GetRemotePort(PRInt32 *aLocalPort)
{
	 lockClient();
	 *aLocalPort =  ntohs(m_remoteAddr.sin_port);
	 unlockClient();	 
	 return NS_OK;
}

/* readonly attribute string LastErrorDescription; */
NS_IMETHODIMP sashClientSocket::GetLastErrorDescription(char * *aLastErrorDescription)
{
	 lockClient();
	 string errorStr = m_lastErrorDescription;
	 unlockClient();
	 XP_COPY_STRING(errorStr.c_str(), aLastErrorDescription);
	 return NS_OK;
}

/* readonly attribute string LocalIPAddress; */
NS_IMETHODIMP sashClientSocket::GetLocalIPAddress(char * *aLocalIPAddress)
{
	 int size = HOSTNAME_MAX;
	 char *hostname = new char[size];
	 if (hostname == NULL){
		  DEBUGMSG(comm, "couldn't allocate memory for hostname\n");
		  return NS_ERROR_FAILURE;
	 }

	 hostname[0] = '\0';
	 ostrstream o;

	 if (gethostname(hostname, HOSTNAME_MAX) == -1){
		  DEBUGMSG(comm, "couldn't get local ip address\n");
		  o << '\0';
	 } else {
		  struct hostent *h;
		  if ((h=gethostbyname(hostname)) == NULL){
			   o << '\0';
		  }
		  else{
			   if (h->h_addr != NULL){
					o << inet_ntoa(*((struct in_addr *)h->h_addr)) << '\0';
			   } else {
					// not sure if this is covered by the first if condition.
					o << '\0';
			   }
		  }
	 }
	 delete hostname;
	 XP_COPY_STRING(o.str(), aLocalIPAddress);
	 return NS_OK;
}

/* readonly attribute PRInt32 PendingReceiveByteCount; */
NS_IMETHODIMP sashClientSocket::GetPendingReceiveByteCount(PRInt32 *aPendingReceiveByteCount)
{
	 lockClient();
	 *aPendingReceiveByteCount = m_receiveBuffer->getSize();
	 unlockClient();
	 return NS_OK;
}

/* readonly attribute PRInt32 PendingSendByteCount; */
NS_IMETHODIMP sashClientSocket::GetPendingSendByteCount(PRInt32 *aPendingSendByteCount)
{
	 lockClient();
	 *aPendingSendByteCount = m_sendBuffer->getSize();
	 unlockClient();
	 return NS_OK;
}

/* attribute PRInt32 SentByteCount; */
NS_IMETHODIMP sashClientSocket::GetSentByteCount(PRInt32 *aSentByteCount)
{
	 lockClient();
	 *aSentByteCount = m_sentByteCount;
	 unlockClient();
	 return NS_OK;
}

NS_IMETHODIMP sashClientSocket::SetSentByteCount(PRInt32 aSentByteCount)
{
	 lockClient();
	 m_sentByteCount = aSentByteCount;
	 unlockClient();
	 return NS_OK;
}

/* attribute PRInt32 ReceivedByteCount; */
NS_IMETHODIMP sashClientSocket::GetReceivedByteCount(PRInt32 *aReceivedByteCount)
{
	 lockClient();
	 *aReceivedByteCount = m_receivedByteCount;
	 unlockClient();
	 return NS_OK;
}

NS_IMETHODIMP sashClientSocket::SetReceivedByteCount(PRInt32 aReceivedByteCount)
{
	 lockClient();
	 m_receivedByteCount = aReceivedByteCount;
	 unlockClient();
	 return NS_OK;
}

/* attribute string OnClosed; */
NS_IMETHODIMP sashClientSocket::GetOnClosed(char * *aOnClosed)
{
	 XP_COPY_STRING(m_onClosed.c_str(), aOnClosed);
	 return NS_OK;
}
NS_IMETHODIMP sashClientSocket::SetOnClosed(const char * aOnClosed)
{
	 m_onClosed = aOnClosed;
	 return NS_OK;
}

/* attribute string OnConnect; */
NS_IMETHODIMP sashClientSocket::GetOnConnect(char * *aOnConnect)
{
	 XP_COPY_STRING(m_onConnect.c_str(), aOnConnect);
	 return NS_OK;
}
NS_IMETHODIMP sashClientSocket::SetOnConnect(const char * aOnConnect)
{
	 m_onConnect = aOnConnect;
	 return NS_OK;
}

/* attribute string OnError; */
NS_IMETHODIMP sashClientSocket::GetOnError(char * *aOnError)
{
	 XP_COPY_STRING(m_onError.c_str(), aOnError);
	 return NS_OK;
}
NS_IMETHODIMP sashClientSocket::SetOnError(const char * aOnError)
{
	 m_onError = aOnError;
	 return NS_OK;
}

/* attribute string OnReceived; */
NS_IMETHODIMP sashClientSocket::GetOnReceived(char * *aOnReceived)
{
	 XP_COPY_STRING(m_onReceived.c_str(), aOnReceived);
	 return NS_OK;
}

NS_IMETHODIMP sashClientSocket::SetOnReceived(const char * aOnReceived)
{
	 m_onReceived = aOnReceived;
	 return NS_OK;
}


bool sashClientSocket::hasDataToSend(){
	 return (m_sendBuffer->getSize() > 0);
}

int sashClientSocket::dataSendSize(){
	 return (m_sendBuffer->getSize());
}

int sashClientSocket::getSendData(int size, char * buf){
	 return (m_sendBuffer->getData(size, buf));
}

int sashClientSocket::putSendDataBack(int size, char * buf){
	 return (m_sendBuffer->prependData(size, buf));
}

int sashClientSocket::getSockFD(){
	 int retval;
	 lockClient();
	 retval = m_sockfd;
	 unlockClient();
	 return retval;
}

int sashClientSocket::setSockFD(int sockfd){
	 lockClient();
	 DEBUGMSG(comm, "changing sockfd from %d to %d\n",
			  m_sockfd, sockfd);

	 m_sockfd = sockfd;
	 unlockClient();
	 return sockfd;
}

void sashClientSocket::setConnected(bool connected){
	 lockClient();
	 m_connected = connected;
	 unlockClient();
}

void sashClientSocket::getRemoteAddr(struct sockaddr_in *remoteAddr){
	 lockClient();
	 memcpy (remoteAddr, &m_remoteAddr, sizeof(struct sockaddr_in));
	 unlockClient();
}

void sashClientSocket::copyRemoteAddr(struct sockaddr_in *remoteAddr){
	 lockClient();
	 memcpy (&m_remoteAddr, remoteAddr, sizeof(struct sockaddr_in));
	 unlockClient();
}

void sashClientSocket::addToSentByteCount(int bytes){
	 lockClient();
	 m_sentByteCount += bytes;
	 unlockClient();
}

void sashClientSocket::addToReceivedByteCount(int bytes){
	 lockClient();
	 m_receivedByteCount += bytes;
	 unlockClient();
}

void sashClientSocket::addToPendingSendByteCount(int bytes){
	 lockClient();
	 m_pendingSendByteCount += bytes;
	 unlockClient();
}

void sashClientSocket::addToPendingReceiveByteCount(int bytes){
	 lockClient();
	 m_pendingReceiveByteCount += bytes; 
	 unlockClient();
}


bool sashClientSocket::getToBeClosed(){
	 bool retval;
	 lockClient();
	 retval = m_toBeClosed;
	 unlockClient();
	 return retval;
}

void sashClientSocket::setErrorMessage(char *message, bool lock){
	 if (lock) lockClient();
	 m_lastErrorDescription=message;
	 if (lock) unlockClient();
}

void sashClientSocket::dataToSend()
{
	 sashSockets::m_scm->addToWriteSet(getSockFD());
}
