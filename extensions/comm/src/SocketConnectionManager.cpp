
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the SocketConnectionManager
******************************************************************/
#include "sashComm.h"
#include "sashnet.h"
#include "SocketConnectionManager.h"
#include "sashDatagramSocket.h"
#include "sashServerSocket.h"
#include "sashClientSocket.h"
#include "debugmsg.h"
#include "sash_error.h"
#include "sashVariantUtils.h"

#include "xpcomtools.h"
#include "extensiontools.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string>

#include <fcntl.h>

SocketConnectionManager::SocketConnectionManager()
{
	 DEBUGMSG(comm, "socket connection manager constructor\n");

	 m_running = false;
	 m_numSockets = 0;
	 m_highestSockfd = 0;

	 FD_ZERO(&m_readmaster);	 
	 FD_ZERO(&m_readfds);	 
	 FD_ZERO(&m_writemaster);	 
	 FD_ZERO(&m_writefds);
	 FD_ZERO(&m_exceptfds);
	 setTimeout(TIMEOUT_SECS, TIMEOUT_MICROSECS);
}

SocketConnectionManager::~SocketConnectionManager()
{
  pthread_mutex_destroy(&m_listener_lock);
  pthread_mutex_destroy(&m_sender_lock);
  pthread_mutex_destroy(&m_clientConnection_lock);
  pthread_mutex_destroy(&m_closeQueue_lock);
}

bool SocketConnectionManager::Initialize(sashIActionRuntime * actionRuntime, 
										 JSContext *cx, 
										 sashIExtension *ext)
{
	 DEBUGMSG(comm, "initializing SocketConnectionManager\n");
	 m_cx = cx;
	 m_ext = ext;
	 m_act = actionRuntime;

	 return true;
}

bool SocketConnectionManager::addToSocketSet(int newsockfd,
											 socketObjectType sotype,
											 void *socketObj,
											 bool lock){
	 if (lock) lockListener();

	 m_numSockets ++;
	 if (newsockfd > m_highestSockfd) m_highestSockfd = newsockfd;

	 DEBUGMSG(comm, "adding fd%d to listen set\n", newsockfd);
	 setReadBit(newsockfd, false);

	 // associate the sockfd with the socket object
	 openSocketObject currObj;
	 currObj.type = sotype;
	 currObj.sockObj = socketObj;
	 m_socketObjectsHash[newsockfd] = currObj;

	 if (lock) unlockListener();
	 return true;
}

void SocketConnectionManager::removeFromSocketSet(int currfd,
												  bool lock){
	 if (lock) lockListener();
	 m_numSockets --;

	 // remove from readset.
	 DEBUGMSG(comm, "removing fd%d from select set\n", currfd);

	 if (currfd != -1){
		  FD_CLR(currfd, &m_readmaster);
		  
		  // Get rid of the entry in the hash table.
		  DEBUGMSG(comm, "going to delete object from hash...");
		  m_socketObjectsHash.erase(currfd);
	 } else {
		  DEBUGMSG(comm, "-1 file descriptor can't be closed...\n");
	 }
	 if (lock) unlockListener();
}

static void * start_run(void *manager){
	 DEBUGMSG(comm, "about to run...\n");
	 SocketConnectionManager *m = (SocketConnectionManager *) manager;
	 m->run();
	 return NULL;
}

bool SocketConnectionManager::start(){
	 DEBUGMSG(comm, "starting connection manager...\n");

	 pthread_mutex_init(&m_listener_lock, NULL);
	 pthread_mutex_init(&m_sender_lock, NULL);
	 pthread_mutex_init(&m_closeQueue_lock, NULL);
	 pthread_mutex_init(&m_clientConnection_lock, NULL);

	 pthread_attr_t attr;
	 pthread_attr_init(&attr);

	 lockListener();
	 int res = pthread_create(&m_selectOnSockets, &attr, 
							  start_run, (void *) this);  
	 m_running = (res == 0);
	 unlockListener();
	 
	 return m_running;
}

void SocketConnectionManager::run()
{
	 DEBUGMSG(comm, "running...\n");
	 m_running = true;

	 lockListener();
	 unlockListener();

	 openSocketObject currSocketObj;
	 sashServerSocket *ssocket=NULL;
	 sashDatagramSocket *dsocket=NULL;
	 sashClientSocket *csocket=NULL;

	 while(m_running){
		  lockSender();
		  m_writefds = m_writemaster;
		  unlockSender();

		  lockListener();
		  m_readfds = m_readmaster;
		  setTimeout(TIMEOUT_SECS, TIMEOUT_MICROSECS);

		  DEBUGMSG(commrunning, "calling select\n");
		  if (select(m_highestSockfd+1, &m_readfds, &m_writefds, NULL, 
					 &m_timeout) == -1){
			   perror("select");
			   return;
		  } 
		  
		  // iterate through all keys
		  socketHash::iterator a = m_socketObjectsHash.begin(), 
			   b = m_socketObjectsHash.end();
		  int currfd;
 
		  while (a != b) {
			   ssocket = NULL;
			   csocket = NULL;
			   dsocket = NULL;
			   
			   currfd = a->first;
			   currSocketObj = a->second;

			   switch (currSocketObj.type){
			   case DATAGRAM:
					// could be a datagram...
					dsocket = (sashDatagramSocket *)currSocketObj.sockObj;
					assert(dsocket != NULL);

					if (FD_ISSET(currfd, &m_readfds)){
						 DEBUGMSG(comm, "adding datagram socket fd%d to receive queue\n", dsocket->getSockFD());
						 m_datagramReceiveQueue.push_back(dsocket);
					}

					if ((FD_ISSET(currfd, &m_writefds))
						&& (dsocket->hasDataToSend())){
						 DEBUGMSG(comm, "adding datagram socket fd%d to send queue\n", dsocket->getSockFD());
						 m_datagramSendQueue.push_back(dsocket);
					}
					break;
			   case CLIENT:
					csocket = (sashClientSocket *)currSocketObj.sockObj;
					assert(csocket != NULL);

					if (FD_ISSET(currfd, &m_readfds)){
						 // add csocket to the client queue to be checked.
						 DEBUGMSG(comm, "adding client socket fd%d ready to receive queue\n", csocket->getSockFD());
						 m_clientReceiveQueue.push_back(csocket);
					}

					if ((FD_ISSET(currfd, &m_writefds)) 
						&& (csocket->hasDataToSend())){
						 DEBUGMSG(comm, "adding client socket fd%d ready to send queue\n", csocket->getSockFD());
						 m_clientSendQueue.push_back(csocket);
					}
					break;
			   case SERVER:
					// this is a special case because this will trigger
					// an OnConnection event in ServerSocket, which will
					// call AcceptConnection, which needs to have 
					// the lock. 
					ssocket = (sashServerSocket *)currSocketObj.sockObj;
					assert (ssocket != NULL);
					
					if (FD_ISSET(currfd, &m_readfds)){
						 m_pendingServerQueue.push_back(ssocket);

						 // unlike the other socket actions, the
						 // accept does not happen until the 
						 // server socket AcceptConnection method 
						 // is called. select will continue to see 
						 // data on this socket until that happens.
						 // to avoid this, we clear the appropriate 
						 // socket and add it back when the 
						 // connection has been accepted.
						 DEBUGMSG(comm, "stop listening to server sockfd%d\n",
								  ssocket->getSockFD());
						 FD_CLR(ssocket->getSockFD(), &m_readmaster);
					}

					if (FD_ISSET(currfd, &m_writefds)){
						 DEBUGMSG(comm, "server socket ready to send(?)\n");
					}
					break;
			   default:
					break;
			   }
			   ++a;
		  }
		  unlockListener();

		  // see whether there are any TCP Client connections waiting to be 
		  // connected.
		  DEBUGMSG(commrunning, "checking for new tcp clients\n");
		  checkClientQueue();

		  DEBUGMSG(commrunning, "checking for datagrams\n");
		  processDatagramReceiveQueue();
		  processDatagramSendQueue();

		  // see whether any data for a tcp client arrived
		  DEBUGMSG(commrunning, "checking for new client data\n");
		  processClientReceiveQueue();
		  processClientSendQueue();

		  // see whether there are any new client connections
		  DEBUGMSG(commrunning, "checking for new clients for the server\n");
		  processServerQueue();

		  // close any connections that are ready to be closed.
		  DEBUGMSG(commrunning, "checking close queue\n");
 		  checkCloseQueue();
	 }
}

void SocketConnectionManager::processDatagramReceiveQueue(){
	 sashDatagramSocket *dsocket = NULL;
	 // see whether any datagrams arrived
	 while (! m_datagramReceiveQueue.empty()){
		  dsocket = m_datagramReceiveQueue.front();
		  m_datagramReceiveQueue.pop_front();

		  assert (dsocket != NULL);
					
		  int datagramSize = receiveDatagram(dsocket);
		  if (datagramSize == -1){
			   DEBUGMSG(comm, "error receiving datagrams\n");
		  } else {
			   while(dsocket->receivedData()){
					int dgramSize 
						 = dsocket->getSizeOfNextReceivedDatagram();
					char *buf = new char[dgramSize +1];
					if (buf == NULL){
						 DEBUGMSG(comm, "couldn't new buffer to process received data\n");
					}else{
						 dsocket->getNextReceivedDatagram(buf);
						 buf[dgramSize] = '\0';
						 string strbuf = buf;
						 ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(
							  m_cx,
							  dsocket->m_onReceived,
							  (nsISupports *)(sashIDatagramSocket *)dsocket,
							  strbuf);
						 delete [] buf;
					}
			   }
		  }
	 }
}

void SocketConnectionManager::processDatagramSendQueue(){
	 sashDatagramSocket *dsocket = NULL;
	 while (!m_datagramSendQueue.empty()){
		  dsocket = m_datagramSendQueue.front();
		  m_datagramSendQueue.pop_front();
		  assert (dsocket != NULL);
			   
		  DEBUGMSG(comm, "datagram socket ready to send\n");
		  char * sendErrorBuf;
		  if (! dsocket->sendAllDatagrams(&sendErrorBuf)){
			   DEBUGMSG(comm, "error sending datagrams\n");
			   string strbuf = sendErrorBuf;
			   ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx,
					dsocket->m_onSendError,
					(nsISupports *)(sashIDatagramSocket *)dsocket,
					strbuf);
		  }
		  removeFromWriteSet(dsocket->getSockFD());
	 }
}

void SocketConnectionManager::processClientReceiveQueue(){
	 sashClientSocket *csocket = NULL;
	 while(! m_clientReceiveQueue.empty()){
		  csocket = m_clientReceiveQueue.front();
		  m_clientReceiveQueue.pop_front();
		  assert (csocket != NULL);
		  
		  int bytesOfClientData = receiveSomeData(csocket);
		  if (bytesOfClientData > 0){
			   DEBUGMSG(comm, "adding csocket onreceive event to queue...");
			   ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx,
					csocket->m_onReceived,
					(nsISupports *)(sashIClientSocket *)csocket);
		  } else if (bytesOfClientData == 0){
			   DEBUGMSG(comm, "0 bytes of client data received on fd%d; other side closed connection\n", csocket->getSockFD());
			   // put it on the close queue
			   addCSocketToCloseQueue(csocket);
		  } else if (bytesOfClientData == -1){
			   ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(
					m_cx,
					csocket->m_onError,
					(nsISupports *)(sashIClientSocket *)csocket);
		  } else if (bytesOfClientData == -2){
			   // this means that the recv would have blocked (if
			   // the socket was non blocking). no need to signal 
			   // an error.
		  }
	 }
}

void SocketConnectionManager::processClientSendQueue(){
	 sashClientSocket *csocket = NULL;	 
	 while (!m_clientSendQueue.empty()){
		  csocket = m_clientSendQueue.front();
		  m_clientSendQueue.pop_front();
		  assert (csocket != NULL);
			   
		  DEBUGMSG(comm, "client socket ready to send\n");
		  int amtToSend = csocket->dataSendSize();
		  int amtSent = sendSomeData(csocket);
		  if (amtSent == -1){
			   DEBUGMSG(comm, "error sending client data\n");
			   ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
																  csocket->m_onError,
																  (nsISupports *)(sashIClientSocket *)csocket);
		  } else if (amtSent == -2) {
			   // there was some error, but it was possible
			   // to recover from it.
		  } else if (amtSent == -3) {
			   // close this connection, call an error event.
			   addCSocketToCloseQueue(csocket);
			   ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
																  csocket->m_onError,
																  (nsISupports *)(sashIClientSocket *)csocket);
		  } else if (amtSent == amtToSend) {
			   DEBUGMSG(comm, "all data from this socket sent\n");
			   removeFromWriteSet(csocket->getSockFD());
		  } else if (amtSent < amtToSend) {
			   DEBUGMSG(comm, "more data to send for this client\n");
			   m_clientSendQueue.push_back(csocket);
		  }
	 }
}

void SocketConnectionManager::processServerQueue(){
	 sashServerSocket *ssocket = NULL;
	 while (!m_pendingServerQueue.empty()){
		  ssocket = m_pendingServerQueue.front();
		  m_pendingServerQueue.pop_front();
		  assert (ssocket != NULL);

		  // Fire an on connection event.
		  DEBUGMSG(comm, 
				   "adding ssocket connection event to queue\n");
		  ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
															 ssocket->m_onConnection,
															 (nsISupports *)(sashIServerSocket *)ssocket);
	 }
}

void SocketConnectionManager::checkClientQueue(){
	 sashClientSocket *csocket = NULL;

	 bool connectError = false;
	 lockClientConnQueue();
	 while((!m_clientConnectionQueue.empty()) && (! connectError)){
		  DEBUGMSG(comm, "client found on queue, trying to create connection\n");
		  csocket = m_clientConnectionQueue.front();
		  m_clientConnectionQueue.pop_front();
		  unlockClientConnQueue();

		  assert (csocket != NULL);

		  bool connectedAlready = false; //csocket->isConnected();
		  if (connectedAlready){
			   DEBUGMSG(comm, "This socket is already connected!\n");
		  } else {
			   if ((csocket->setSockFD(socket(AF_INET, SOCK_STREAM,0)) == -1)){
					perror("socket");
					csocket->setErrorMessage("socket call failed\n");
					connectError = true;
			   }else{
					struct sockaddr_in remoteAddr;
					csocket->getRemoteAddr(&remoteAddr);

					DEBUGMSG(comm, "trying to connect to %s, port %d\n", 
							 inet_ntoa(*(struct in_addr *)
									   &(remoteAddr.sin_addr.s_addr)), 
							 ntohs(remoteAddr.sin_port));
			   
					if ((connect(csocket->getSockFD(), 
								 (struct sockaddr*)&remoteAddr,
								 sizeof(struct sockaddr))) == -1){
						 perror("connect");

						 csocket->setErrorMessage("connect failed\n");
						 connectError = true;

						 // if it couldn't connect, release the socket.
						 close (csocket->getSockFD());
					}
					else
					{
//						 fcntl(csocket->getSockFD(), F_SETFL, O_NONBLOCK);

						 // add it to the listeners.
						 addToSocketSet(csocket->getSockFD(), 
										CLIENT, (void *)csocket);

						 csocket->setConnected(true);

						 // if connected successfully, 
						 // we should call an onconnection
						 // event.
						 ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
												   csocket->m_onConnect,
												   (nsISupports *)(sashIClientSocket *)csocket);
					}
			   }
		  }
		  lockClientConnQueue();
	 }
	 unlockClientConnQueue();
	 if ((connectError) && csocket != NULL) {
		  ((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
												   csocket->m_onError,
												   (nsISupports *)(sashIClientSocket *)csocket);
	 }
}

void SocketConnectionManager::checkCloseQueue(){
	 openSocketObject *osObj;
	 
	 int sockfd = -1;
	 
	 sashDatagramSocket *dsocket = NULL;
	 sashClientSocket *csocket = NULL;
	 sashServerSocket *ssocket = NULL;

	 lockCloseQueue();
	 while(! m_closeQueue.empty()){
		  osObj = m_closeQueue.front();
		  m_closeQueue.pop_front();

		  assert (osObj!= NULL);
		  unlockCloseQueue();

		  // ugly, but this seems to be the most readable way to do this.
		  // which sockfd?
		  switch (osObj->type)
		  {
		  case DATAGRAM:
			   sockfd = ((sashDatagramSocket *)(osObj->sockObj))->getSockFD();
			   break;
		  case CLIENT:
			   sockfd = ((sashClientSocket *)(osObj->sockObj))->getSockFD();
			   break;
		  case SERVER:
			   sockfd = ((sashServerSocket *)(osObj->sockObj))->getSockFD();
			   break;
		  default:
			   break;
		  }
		  if (sockfd != -1){
			   DEBUGMSG(comm, "close queue not empty, trying to close fd%d\n",
						sockfd);
		  
			   // stop lisening to that sockfd.
			   removeFromSocketSet(sockfd);
			   removeFromWriteSet(sockfd);

			   // close that sockfd.
			   close (sockfd);
			   DEBUGMSG(comm, "fd%d closed\n", sockfd);

			   // call the right event.
			   dsocket = NULL;
			   csocket = NULL;
			   ssocket = NULL;

			   switch (osObj->type)
			   {
			   case DATAGRAM:
					dsocket = (sashDatagramSocket *)(osObj->sockObj);
					assert (dsocket != NULL);
					dsocket->setIsOpen(false);
					dsocket->setSockFD(-1);
					((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
												   dsocket->m_onClosed,
												   (nsISupports *)(sashIDatagramSocket *)dsocket);
					break;
			   case CLIENT:
					csocket = (sashClientSocket *)(osObj->sockObj);
					assert (csocket != NULL);
					csocket->setConnected(false);
					csocket->setSockFD(-1);
					((sashComm *)m_ext)->m_eventQueue->addGenericEvent(m_cx,
												   csocket->m_onClosed,
												   (nsISupports *)(sashIClientSocket *)csocket);
					break;
					break;
			   case SERVER:{
					ssocket = (sashServerSocket *)(osObj->sockObj);
					assert (ssocket != NULL);
					ssocket->setSockFD(-1);
					ssocket->setStarted(false);
					break;
			   }
			   default:
					DEBUGMSG (comm, "unknown socket type.\n");
					break;
			   }
		  }
		  delete (osObj);
		  lockCloseQueue();
	 }
	 unlockCloseQueue();
}

int SocketConnectionManager::sendSomeData(sashClientSocket *csocket){
	 int dataSize = csocket->getSendData(MAX_CLIENT_SEND_BUFSIZE, 
										 m_TCPSendBuffer);
	 int currAmtSent = send(csocket->getSockFD(), 
							m_TCPSendBuffer, 
							dataSize, 0);

	 if (currAmtSent == -1){
		  perror("send");
		  switch (errno){
		  case EWOULDBLOCK:
			   DEBUGMSG(comm, "would have blocked on send, put data back\n");
			   csocket->putSendDataBack(dataSize, m_TCPSendBuffer);
			   currAmtSent = -2;
			   break;
		  case ENOBUFS:
			   // don't send now. put it back.
			   DEBUGMSG(comm, "no internal buffers for send, put data back\n");
			   csocket->putSendDataBack(dataSize, m_TCPSendBuffer);
			   currAmtSent = -2;
			   break;
		  case EPIPE:
			   // broken pipe. close the connection?
			   DEBUGMSG(comm, "broken pipe, connection should be closed\n");
			   csocket->setErrorMessage("Broken pipe.\n");
			   currAmtSent = -3;
			   break;
		  case ECONNRESET:
			   // should close the connection since it has been
			   // reset on the other side.
			   DEBUGMSG(comm, "connection reset by peer, connection should be closed\n");
			   csocket->setErrorMessage("connection reset by peer.\n");
			   currAmtSent = -3;
			   break;
		  default:
			   csocket->setErrorMessage(
					"Error sending data on client socket\n");
			   break;
		  }
	 } else {
		  DEBUGMSG(comm, "%d of %d bytes sent on fd%d\n", currAmtSent, 
				   dataSize, csocket->getSockFD());
		  if (dataSize > currAmtSent){
			   // put some of the data back.
			   csocket->putSendDataBack((dataSize - currAmtSent), 
										&(m_TCPSendBuffer[currAmtSent]));
		  }
	 }
	 return currAmtSent;
}

int SocketConnectionManager::receiveSomeData(sashClientSocket *csocket){
	 int bytesReceived = recv(csocket->getSockFD(), m_TCPReceiveBuffer, 
							  MAX_CLIENT_RECEIVE_BUFSIZE, 0);

	 if (bytesReceived > 0){
		  m_TCPReceiveBuffer[bytesReceived] = '\0';
		  DEBUGMSG(comm, "%d bytes received on fd%d\n", bytesReceived,
				   csocket->getSockFD());
		  // add the bytes to the receive streambuffer.
		  csocket->m_receiveBuffer->appendData(bytesReceived, 
											   m_TCPReceiveBuffer);
		  csocket->addToReceivedByteCount(bytesReceived);
	 } else if (bytesReceived == -1) {
		  perror("recv");
		  switch (errno){
		  case EWOULDBLOCK:
			   // would this ever come up?
			   // this might not be reason enough to call an error.
			   bytesReceived = -2;
			   break;
		  default:
			   csocket->setErrorMessage("Error receiving data on client socket\n");
			   break;
		  }
	 }
	 return bytesReceived;
}

int SocketConnectionManager::receiveDatagram(sashDatagramSocket *dsocket){
	 int currfd = dsocket->getSockFD();

	 struct sockaddr_in their_addr;
	 socklen_t flen = sizeof(struct sockaddr);

	 int bytesReceived = recvfrom(currfd, m_UDPReceiveBuffer, 
								  MAX_DATAGRAM_BUFSIZE, 0, 
								  (struct sockaddr *)&their_addr,
								  &flen);
	 DEBUGMSG(comm, "received %d bytes on fd%d\n", bytesReceived,
			  currfd);

	 if (bytesReceived == -1){
		  perror("recvfrom");
		  switch (errno){
		  case EWOULDBLOCK:
			   // would this ever come up?
			   // this might not be reason enough to call an error.
			   bytesReceived = -2;
			   break;
		  default:
			   break;
		  }
	 }else{
		  dsocket->appendReceivedData(bytesReceived, m_UDPReceiveBuffer);
	 }
	 return bytesReceived;
}

void SocketConnectionManager::addToWriteSet(int sockfd, bool lock){
	 DEBUGMSG(comm, "adding fd%d to write set\n",
			  sockfd);
	 setWriteBit(sockfd, lock);
}

void SocketConnectionManager::removeFromWriteSet(int sockfd, bool lock){
	 if (lock) lockSender();
	 DEBUGMSG(comm, "removing fd%d from write set\n",
			  sockfd);
	 FD_CLR(sockfd, &m_writemaster);
	 if (lock) unlockSender();
}

bool SocketConnectionManager::addSSocketToCloseQueue(sashServerSocket *ssocket){
	 openSocketObject *sockobj = new openSocketObject;
	 if (sockobj == NULL) return false;

	 sockobj->type = SERVER;
	 sockobj->sockObj = ssocket;

	 lockCloseQueue();
	 m_closeQueue.push_back(sockobj);
	 unlockCloseQueue();

	 return true;
}

void SocketConnectionManager::addClientToConnectionQueue
(sashClientSocket *csocket){
	 DEBUGMSG(comm, "adding to client connection queue.\n");

	 lockClientConnQueue();
	 m_clientConnectionQueue.push_back(csocket);
	 unlockClientConnQueue();
}

bool SocketConnectionManager::addDSocketToCloseQueue(sashDatagramSocket *dsocket){
	 openSocketObject *sockobj = new openSocketObject;
	 if (sockobj == NULL) return false;

	 sockobj->type = DATAGRAM;
	 sockobj->sockObj = dsocket;

	 lockCloseQueue();
	 m_closeQueue.push_back(sockobj);
	 unlockCloseQueue();
	 return true;
}

bool SocketConnectionManager::addCSocketToCloseQueue(sashClientSocket *csocket){
	 openSocketObject *sockobj = new openSocketObject;
	 if (sockobj == NULL) return false;

	 sockobj->type = CLIENT;
	 sockobj->sockObj = csocket;

	 lockCloseQueue();
	 m_closeQueue.push_back(sockobj);
	 unlockCloseQueue();
	 return true;
}

void SocketConnectionManager::callSocketEvent (void *socket, const string& callback){
	 vector <nsIVariant *> args;
	 DEBUGMSG(comm, "about to call client error event %s\n",
			  callback.c_str());

	 if (callback != "") {
		  nsIVariant * sockVariant;

		  NewVariant(&sockVariant);
		  VariantSetInterface(sockVariant, 
							  (nsISupports *)socket);
		  args.push_back(sockVariant);
								   
		  CallEvent(m_act, m_cx, 
					callback.c_str(), args);
	 }
}

bool SocketConnectionManager::alreadyListening(int sockfd, bool lock){
	 if (lock) lockListener();
	 bool retval = ((sockfd <= m_highestSockfd)
					&& (FD_ISSET(sockfd, &m_readmaster)));
	 if (lock) unlockListener();
	 return retval;
}

void SocketConnectionManager::setReadBit(int sockfd, bool lock){
	 if (lock) lockListener();
	 FD_SET(sockfd, &m_readmaster);
	 if (lock) unlockListener();
}

void SocketConnectionManager::setWriteBit(int sockfd, bool lock){
	 if (lock) lockSender();
	 FD_SET(sockfd, &m_writemaster);
	 if (lock) unlockSender();
}
