
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the Comm object (which only supports sockets)
******************************************************************/

#ifndef SASHCOMM_H
#define SASHCOMM_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIComm.h"
#include "sashIExtension.h"
#include "sashISecurityManager.h"
#include <string>
#include "event.h"

class sashISockets;
class sashIGenericConstructor;

#define SASHCOMM_CID \
{0xA70ED32F, 0x93C0, 0x47AB, {0x8D, 0xDA, 0x11, 0x34, 0x0C, 0x3D, 0x34, 0xC6}}

NS_DEFINE_CID(ksashCommCID, SASHCOMM_CID);

#define SASHCOMM_CONTRACT_ID "@gnome.org/SashXB/comm;1"

class sashComm : public sashIComm,
				 public sashIExtension
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHICOMM

  sashComm();
  virtual ~sashComm();

  JSContext *m_cx;

private:
  sashISockets *m_sockets;
  sashISecurityManager * m_secMan;
  eventQueue *m_eventQueue;
  nsCOMPtr<sashIActionRuntime> m_act;
  friend class SocketConnectionManager;
};

#endif
