
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the sockets object 
******************************************************************/
#ifndef SASHSOCKETS_H
#define SASHSOCKETS_H

#include "nsID.h"
#include "sashIComm.h"
#include "sashISockets.h"
#include "sashIConstructor.h"
#include "SocketConnectionManager.h"
#include "sashVariantUtils.h"
#include <string>
#include <arpa/inet.h>
#include <strstream>

class sashIGenericConstructor;

// (aa66fd78-e6a0-40e9-9d8a-ff4a450638f4)
#define SASHSOCKETS_CID \
{0x112701e5, 0xce5c, 0x41da, {0xa0, 0x60, 0xb0, 0x1c, 0x51, 0x4c, 0x92, 0x63}}

NS_DEFINE_CID(ksashSocketsCID, SASHSOCKETS_CID);

#define SASHSOCKETS_CONTRACT_ID "@gnome.org/SashXB/communications/sockets;1"

const int HOSTNAME_MAX =100;


class sashSockets : public sashISockets,
					public sashIConstructor

{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHISOCKETS;
	 NS_DECL_SASHICONSTRUCTOR;

	 sashSockets();
	 virtual ~sashSockets();
	 
private:
	 static SocketConnectionManager *m_scm;

	 sashIExtension *m_sashComm;
	 sashIGenericConstructor *m_datagramSocketConstructor,
		  *m_clientSocketConstructor,
		  *m_serverSocketConstructor;

	 JSContext *m_cx;
	 nsCOMPtr<sashIActionRuntime> m_act;
	 sashISecurityManager* m_secMan;
	 friend class sashDatagramSocket;
	 friend class sashServerSocket;	 
	 friend class sashClientSocket;
};


inline void VariantSetStringArrayC(nsIVariant * v, char **arr) {
	 int strnum = 0;

	 vector <string> strs;

	 if (arr == NULL) {
		  VariantSetArray(v, strs);
		  return;
	 }
	 
	 while (arr[strnum] != NULL){
		  ostrstream o;
		  o << inet_ntoa(*((struct in_addr *)arr[strnum])) << '\0';
		  strs.push_back(o.str());

		  strnum++;
	 }

	 VariantSetArray(v, strs);
	 return;
}

#endif //SASHSOCKETS_H
