
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation of the communications extension
******************************************************************/

#include "sashComm.h"
#include "sashSockets.h"

#include "sash_constants.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIGenericConstructor.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "extensiontools.h"
#include <unistd.h>
#include "secman.h"

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>

SASH_EXTENSION_IMPL_NO_NSGET(sashComm, sashIComm, 
							 "Comm");

sashComm::sashComm()
{
	 NS_INIT_ISUPPORTS();
	 DEBUGMSG(comm, "sashComm: Creating communications extension\n");
	 m_sockets = NULL;
}

sashComm::~sashComm()
{
	 NS_IF_RELEASE(m_sockets);
}

// *** what else?
NS_IMETHODIMP
sashComm::Initialize(sashIActionRuntime *act, 
			   const char *registrationXml, 
			   const char *webGuid,
			   JSContext *cx, JSObject *obj)
{

	 DEBUGMSG(comm, "sashComm::Initialize\n");
	 act->GetSecurityManager(&m_secMan);
	 m_act = act;
	 m_cx = cx;
	 m_eventQueue = new eventQueue (act, this);
	 return NS_OK;
}

NS_IMETHODIMP
sashComm::ProcessEvent()
{
	 DEBUGMSG(comm, "sashComm::ProcessEvent\n");
	 m_eventQueue->callAllEvents();
	 return NS_OK;
}

NS_IMETHODIMP
sashComm::Cleanup(){
	 DEBUGMSG(comm, "sashComm:: cleaning up sashcomm\n");
	 return NS_OK;
}

NS_IMETHODIMP sashComm::GetSockets(sashISockets * *aSockets)
{
	 nsresult retval;
	 if (m_sockets == NULL){
		  retval = nsComponentManager::CreateInstance(ksashSocketsCID, NULL, 
													  NS_GET_IID(sashISockets),
													  (void **) &m_sockets);
		  assert(NS_SUCCEEDED(retval));
		  m_sockets->Initialize(m_act, this, m_cx);
	 }
	 NS_ADDREF(m_sockets);
	 *aSockets = m_sockets;
	 return NS_OK;
}

