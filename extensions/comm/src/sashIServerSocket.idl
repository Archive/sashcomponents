
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

interface file for Server Socket

*****************************************************************/

#include "nsISupports.idl"
#include "nsIVariant.idl"

interface sashIClientSocket;

[scriptable, uuid(a9ab47bb-eac0-4093-9861-4f0febdd3c83)]
interface sashIServerSocket: nsISupports
{
// --------------------- Methods -----------------------
	 boolean AcceptConnection(in sashIClientSocket clientObj);
	 boolean Start(in nsIVariant Port);
	 void Stop();

// ----------- Sash Server Socket properties --------------
	 attribute nsIVariant Context;
	 readonly attribute long ServerPort;
	 readonly attribute boolean Started;
	 readonly attribute string LastErrorDescription;
	 
// ----------- Sash Server Socket event handlers -----------
	 attribute string OnConnection;
	 attribute string OnError;
};

