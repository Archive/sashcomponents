
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the ServerSocket object 
******************************************************************/



#ifndef SASHSERVERSOCKET_H
#define SASHSERVERSOCKET_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashIComm.h"
#include "sashISockets.h"
#include "sashIServerSocket.h"
#include <string>


// (5be4c286-888e-4eff-8741-33560faa46a9)
#define SASHSERVERSOCKET_CID \
{0x5be4c286, 0x888e, 0x4eff, {0x87, 0x41, 0x33, 0x56, 0x0f, 0xaa, 0x46, 0xa9}}

NS_DEFINE_CID(ksashServerSocketCID, SASHSERVERSOCKET_CID);

#define SASHSERVERSOCKET_CONTRACT_ID "@gnome.org/SashXB/communications/sockets/sashserversocket;1"

/* Header file */
class sashServerSocket : public sashIServerSocket,
						 public sashIConstructor

{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHISERVERSOCKET;
	 NS_DECL_SASHICONSTRUCTOR;

  sashServerSocket();
  virtual ~sashServerSocket();

protected:
  int getSockFD();
  void setSockFD(int sockfd);
  void setStarted(bool started);
  void setErrorMessage(char *message, bool lock = true);

private:
  pthread_mutex_t m_serverLock; 

  int m_serverPort;
  int m_sockfd;

  sashIExtension *m_sashComm;
  JSContext *m_cx;
  bool m_started;

  string m_lastErrorDescription;

  string m_onConnection;
  string m_onError;

  friend class SocketConnectionManager;
};
#endif
