#include "streamBuffer.h"
#include <stdio.h>

main(){
	 char buf[] = "123";
	 char buf2[] = "456789";
	 char buf3[] = "01234";
	 char buf4[] = "5678901234";
	 char buf5[] = "56";
	 char buf6[] = "7";



/*
	 int size = strlen(buf);
	 int size2 = strlen(buf2);

	 int size3 = strlen(buf3);
	 int size4 = strlen(buf4);

	 int size5 = strlen(buf5);
	 int size6 = strlen(buf6);
*/
	 
	 streamBuffer *sb = new streamBuffer();

	 printf("=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
	 printf("test appendData\n");
	 printf("FTF\n");
	 sb->appendData(strlen(buf), buf);
	 sb->appendData(strlen(buf2), buf2, true);
	 sb->appendData(strlen(buf3), buf3, false);
	 sb->appendData(strlen(buf4), buf4);
	 sb->appendData(strlen(buf5), buf5);
	 sb->appendData(strlen(buf6), buf6);

	 sb->printStreamBuffer();

	 printf("=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
	 printf("test prependData\n");
	 sb->prependData(strlen(buf5), buf5);
	 sb->prependData(strlen(buf3), buf3);

	 sb->printStreamBuffer();


	 sb->checkTotalSize();


	 int buf7size = 13;
	 char *buf7 = new char[buf7size+1];

	 int buf8size = 2;
	 char *buf8 = new char[buf8size+1];

	 int buf9size = 9;
	 char *buf9 = new char[buf9size+1];

	 printf("----------------------------\n");
	 printf("test getting data\n");
	 assert(sb->getData(buf7size, buf7) == buf7size);
	 buf7[buf7size]='\0';
	 printf("first %d chars: %s\n", buf7size, buf7);

	 printf("current state of stream buffer:\n");
	 sb->printStreamBuffer();

/*
	 sb->getData(buf8size, buf8);
	 buf8[buf8size]='\0';
	 printf("----------------------------\n");
	 printf("next %d chars: %s\n", buf8size, buf8);

	 printf("current state of stream buffer:\n");
	 sb->printStreamBuffer();

	 sb->getData(buf9size, buf9);
	 buf9[buf9size]='\0';
	 printf("----------------------------\n");
	 printf("next %d chars: %s\n", buf9size, buf9);

	 printf("current state of stream buffer:\n");
	 sb->printStreamBuffer();
*/

	 int sizefirst = sb->getSizeOfFirstItem();
	 char *buf0 = new char [sizefirst +1];

	 printf("----------------------------\n");
	 printf("getting next item\n");
	 sb->getDataFromFirstItem(buf0);
	 buf0[sizefirst] = '\0';
	 printf("next item: %s\n",  buf0);

	 printf("current state of stream buffer:\n");
	 sb->printStreamBuffer();

	 string findstr = "1234";
	 int index = sb->findInBuffer(findstr);
	 printf("----------------------------\n");
	 printf("trying to find %s: ", findstr.c_str());
	 printf("found at %d\n", index);

	 string findstr2 = "9012";
	 index = sb->findInBuffer(findstr2);
	 printf("trying to find %s: ", findstr2.c_str());
	 printf("found at %d\n", index);

	 index = sb->findInBuffer('1');
	 printf("trying to find 1: ");
	 printf("found at %d\n", index);

	 index = sb->findInBuffer('5');
	 printf("trying to find 5: ");
	 printf("found at %d\n", index);


	 string notfoundstr = "9876";
	 index = sb->findInBuffer(notfoundstr);
	 printf("trying to find %s: ", notfoundstr.c_str());
	 printf("found at %d\n", index);

	 printf("current state of stream buffer:\n");
	 sb->printStreamBuffer();


	 printf("----------------------------\n");
	 int sizerest = sb->getSize();
	 char *buff = new char [sizerest +1];
	 int restofdata = sb->getData(-1, buff);
	 assert (restofdata == sizerest);
	 buff[sizerest] = '\0';
	 printf("rest of data: %s\n", buff);

	 printf("current state of stream buffer:\n");
	 sb->printStreamBuffer();


	 delete [] buf7;
	 delete [] buf8;
	 delete [] buf9;
	 delete [] buf0;
	 delete [] buff;
}
