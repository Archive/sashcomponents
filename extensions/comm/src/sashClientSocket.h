
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the ClientSocket object 
******************************************************************/

#ifndef SASHCLIENTSOCKET_H
#define SASHCLIENTSOCKET_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashIComm.h"
#include "sashISockets.h"
#include "sashIClientSocket.h"
#include "streamBuffer.h"
#include "debugmsg.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>

// (e4fe0d89-24d7-489c-800e-4f9de7872473)
#define SASHCLIENTSOCKET_CID \
{0xe4fe0d89, 0x24d7, 0x489c, {0x80, 0x0e, 0x4f, 0x9d, 0xe7, 0x87, 0x24, 0x73}}

NS_DEFINE_CID(ksashClientSocketCID, SASHCLIENTSOCKET_CID);

#define SASHCLIENTSOCKET_CONTRACT_ID "@gnome.org/SashXB/communications/sockets/sashclientsocket;1"

class sashClientSocket : public sashIClientSocket,
						 public sashIConstructor
{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHICLIENTSOCKET;
	 NS_DECL_SASHICONSTRUCTOR;

	 sashClientSocket();
	 virtual ~sashClientSocket();

protected:

	 bool hasDataToSend();
	 int dataSendSize();
	 int getSendData(int size, char *buf);
	 int putSendDataBack(int size, char *buf);

	 void setLastErrorDescription (char *errdesc);
	 int setSockFD(int sockfd);
	 int getSockFD();
	 void setConnected(bool connected);
	 bool isConnected();

	 bool getToBeClosed();

	 void getRemoteAddr(struct sockaddr_in *remoteAddr);
	 void copyRemoteAddr(struct sockaddr_in *remoteAddr);
	 void addToSentByteCount(int bytes);
	 void addToPendingSendByteCount(int bytes);
	 void addToReceivedByteCount(int bytes);
	 void addToPendingReceiveByteCount(int bytes);

	 void setErrorMessage(char *message, bool lock = true);

	 void dataToSend();

private:
	 void lockClient(){
		  DEBUGMSG(socklock, "locking client...\n");
		  pthread_mutex_lock(&m_clientLock);
		  DEBUGMSG(socklock, "locked client...\n");
	 }

	 void unlockClient(){
		  DEBUGMSG(socklock, "unlocking client...\n");
		  pthread_mutex_unlock(&m_clientLock);
		  DEBUGMSG(socklock, "unlocked client...\n");
	 }

	 pthread_mutex_t m_clientLock; 

	 sashIExtension *m_sashComm;
	 JSContext *m_cx;

	 string m_onReceived;
	 string m_onError;
	 string m_onConnect;
	 string m_onClosed;

	 bool m_connected;
	 bool m_toBeClosed;

	 struct sockaddr_in m_remoteAddr;
	 int m_sockfd;
	 streamBuffer *m_sendBuffer;
	 streamBuffer *m_receiveBuffer;

	 string m_lastErrorDescription;

	 int m_sentByteCount;
	 int m_receivedByteCount;
	 int m_pendingReceiveByteCount;
	 int m_pendingSendByteCount;

	 friend class SocketConnectionManager;
	 friend class sashServerSocket;
};

#endif
