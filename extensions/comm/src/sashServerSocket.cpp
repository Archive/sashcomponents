
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the ServerSocket object 
******************************************************************/

#include "sashServerSocket.h"
#include "sashClientSocket.h"
#include "sashVariantUtils.h"
#include "extensiontools.h"
#include "nsCRT.h"
#include "sashIExtension.h"
#include "sashSockets.h"
#include "sashnet.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

NS_IMPL_ISUPPORTS2_CI(sashServerSocket, sashIServerSocket, sashIConstructor);

sashServerSocket::sashServerSocket()
{
	 NS_INIT_ISUPPORTS();
	 m_started = false;
	 pthread_mutex_init(&m_serverLock, NULL);
}

sashServerSocket::~sashServerSocket()
{
	 pthread_mutex_destroy(&m_serverLock);
}

NS_IMETHODIMP
sashServerSocket::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, 
									  JSContext * cx, PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	 m_cx = cx;
	 *ret = true;
	 return NS_OK;
}

/* PRBool AcceptConnection (in sashIClientSocket clientObj); */
NS_IMETHODIMP sashServerSocket::AcceptConnection
(sashIClientSocket *clientObj, PRBool *_retval)
{
	 sashClientSocket *csocket = (sashClientSocket *)clientObj;

	 DEBUGMSG(comm, "sashServerSocket::AcceptConnection\n");
	 int sin_size = sizeof(struct sockaddr_in);
	 assert(csocket != NULL);

	 int csockfd = csocket->getSockFD();

	 // it's possible that this has already been assigned.
	 if ((csockfd != -1) && sashSockets::m_scm->alreadyListening(csockfd)){
		  DEBUGMSG(comm,"already been accepted, don't accept again.\n");
	 } else {
		  struct sockaddr_in remoteAddr;

		  DEBUGMSG(comm, "calling accept\n");
		  csockfd = csocket->setSockFD(accept(getSockFD(),
									(struct sockaddr *)&remoteAddr, 
									(socklen_t*)&sin_size));
		  csocket->copyRemoteAddr(&remoteAddr);
		  DEBUGMSG(comm, "returned from accept\n");

		  if (csockfd == -1){
			   perror("accept");
			   DEBUGMSG(comm, "error running accept\n");
			   setErrorMessage("error accepting new connection");
			   sashSockets::m_scm->callSocketEvent((void *)this, m_onError);
		  } else {
			   DEBUGMSG(comm, "accept run successfully, newfd%d\n", 
						csockfd);
			   DEBUGMSG(comm, "connected to %s\n",
						inet_ntoa(remoteAddr.sin_addr)); 

			   // need to check csocket->m_sockfd for data now. add it
			   // to the select set.
			   sashSockets::m_scm->addToSocketSet(csockfd, CLIENT, 
												  (void *)csocket);
			   csocket->setConnected(true);

			   DEBUGMSG(comm, "new client socket added fd%d\n", 
						csockfd);

			   // init numbers to 0
			   csocket->m_sendBuffer->clear();
			   csocket->m_receiveBuffer->clear();
		  }
	 }

	 // before returning, make sure that the server can still listen.
	 DEBUGMSG(comm, "start listening to server sockfd%d again.\n", 
			  getSockFD());

	 // note: because of the way that events are queued up, we took the server
	 // socket out of the listen set. now that we've handled the connection,
	 // we can start listening to the server again.
	 sashSockets::m_scm->setReadBit(getSockFD());

	 return NS_OK;
}

/* PRBool Start (in nsIVariant Port); */
NS_IMETHODIMP sashServerSocket::Start(nsIVariant *Port, PRBool *_retval)
{
	 *_retval = true;

	 // if it has started already, don't start again
	 if (! m_started){
		  DEBUGMSG(comm, "sashServerSocket::Start\n");

		  m_serverPort = (VariantIsEmpty(Port))? 
			   0 : (int)(VariantGetNumber(Port));

		  sockaddr_in myAddr;
		  if ((m_sockfd = bindLocal(SOCK_STREAM, 
									m_serverPort, &myAddr)) == -1){
			   setErrorMessage("Error calling socket or bind, couldn't start server socket\n");
			   sashSockets::m_scm->callSocketEvent((void *)this, m_onError);
		  } else {
			   // start listening
			   if (listen(m_sockfd, BACKLOG) == -1){
					perror("listen");
					setErrorMessage("Error calling listen.\n");
					sashSockets::m_scm->callSocketEvent((void *)this, 
														m_onError);
					m_sockfd = -1;
			   } else {
					DEBUGMSG(comm,"listening...\n");

					sashSockets::m_scm->addToSocketSet(m_sockfd, SERVER, this);
					DEBUGMSG(comm, 
							 "new server socket (fd%d) added to select set\n", 
							 m_sockfd);
			   }
		  }
		  *_retval = m_started = (m_sockfd == -1) ? false : true;
	 }
	 return NS_OK;
}

/* void Stop (); */
NS_IMETHODIMP sashServerSocket::Stop()
{
	 if (m_started){
		  sashSockets::m_scm->addSSocketToCloseQueue(this);
	 }
	 return NS_OK;
}

/* attribute nsIVariant Context; */
NS_IMETHODIMP sashServerSocket::GetContext(nsIVariant * *aContext)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashServerSocket::SetContext(nsIVariant * aContext)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute PRInt32 ServerPort; */
NS_IMETHODIMP sashServerSocket::GetServerPort(PRInt32 *aServerPort)
{
	 pthread_mutex_lock(&m_serverLock);	 
	 *aServerPort = m_serverPort;
	 pthread_mutex_unlock(&m_serverLock);	 
	 return NS_OK;
}

/* readonly attribute PRBool Started; */
NS_IMETHODIMP sashServerSocket::GetStarted(PRBool *aStarted)
{
	 pthread_mutex_lock(&m_serverLock);	 
	 *aStarted = m_started;
	 pthread_mutex_unlock(&m_serverLock);	 
	 return NS_OK;
}

/* attribute string OnConnection; */
NS_IMETHODIMP sashServerSocket::GetOnConnection(char * *aOnConnection)
{
	 XP_COPY_STRING(m_onConnection.c_str(), aOnConnection);
	 return NS_OK;
}
NS_IMETHODIMP sashServerSocket::SetOnConnection(const char * aOnConnection)
{
	 m_onConnection = aOnConnection;
	 return NS_OK;
}

int sashServerSocket::getSockFD(){
	 int retval;
	 pthread_mutex_lock(&m_serverLock);	 
	 retval = m_sockfd;
	 pthread_mutex_unlock(&m_serverLock);
	 return retval;
}

void sashServerSocket::setSockFD(int sockfd){
	 pthread_mutex_lock(&m_serverLock);
	 m_sockfd = sockfd;
	 pthread_mutex_unlock(&m_serverLock);

}

void sashServerSocket::setStarted(bool started){
	 pthread_mutex_lock(&m_serverLock);
	 m_started = started;
	 pthread_mutex_unlock(&m_serverLock);
}

/* readonly attribute string LastErrorDescription; */
NS_IMETHODIMP sashServerSocket::GetLastErrorDescription(char * *aLastErrorDescription)
{
	 pthread_mutex_lock(&m_serverLock);	 
	 string errorStr = m_lastErrorDescription;
	 pthread_mutex_unlock(&m_serverLock);	 
	 XP_COPY_STRING(errorStr.c_str(), aLastErrorDescription);
	 return NS_OK;
}

void sashServerSocket::setErrorMessage(char *message, bool lock){
	 if(lock) pthread_mutex_lock(&m_serverLock);	 
	 m_lastErrorDescription=message;
	 if(lock) pthread_mutex_unlock(&m_serverLock);	 
}

/* attribute string OnError; */
NS_IMETHODIMP sashServerSocket::GetOnError(char * *aOnError)
{
	 XP_COPY_STRING(m_onError.c_str(), aOnError);
	 return NS_OK;
}
NS_IMETHODIMP sashServerSocket::SetOnError(const char * aOnError)
{
	 m_onError = aOnError;
	 return NS_OK;
}
