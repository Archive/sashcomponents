
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the connection manager object 
******************************************************************/
#ifndef SOCKETCONNECTIONMANAGER_H
#define SOCKETCONNECTIONMANAGER_H

#include <hash_map>
#include <deque>
#include "extensiontools.h"
#include "sashIExtension.h"

class sashDatagramSocket;
class sashServerSocket;
class sashClientSocket;

const int TIMEOUT_SECS = 0;
const int TIMEOUT_MICROSECS = 100000;

typedef enum  {DATAGRAM, CLIENT, SERVER}socketObjectType;

typedef struct {
	 socketObjectType type;
	 void *sockObj;
} openSocketObject;

typedef  hash_map<int, openSocketObject> socketHash;

const int  BACKLOG =20;
const int  MAX_DATAGRAM_BUFSIZE = 512;
const int  MAX_CLIENT_RECEIVE_BUFSIZE = 10000;
const int  MAX_CLIENT_SEND_BUFSIZE = 10000;

class SocketConnectionManager
{
public:
	 SocketConnectionManager();
	 ~SocketConnectionManager();
	 bool Initialize(sashIActionRuntime * actionRuntime, JSContext *cx, sashIExtension *ext);

	 // (called from sashClientSocket)
	 void addClientToConnectionQueue (sashClientSocket *clientObj);

	 bool start();
	 void run();

	 // closing sockets.
	 void addToCloseQueue (openSocketObject *socketObj);
	 bool addDSocketToCloseQueue (sashDatagramSocket *dsocket);	 
	 bool addCSocketToCloseQueue (sashClientSocket *csocket);
	 bool addSSocketToCloseQueue (sashServerSocket *ssocket);
	 bool closeDSocket(sashDatagramSocket *dsocket,
					   bool force);
	 bool closeSSocket(sashServerSocket *dsocket);

	 // managing all of the read/write sets
	 void addToWriteSet(int sockfd, bool lock = true);
	 void removeFromWriteSet(int sockfd, bool lock = true);

	 void callSocketEvent (void *socket, const string& callback);
	 bool addToSocketSet(int newsockfd, socketObjectType type,
						 void *socketObj, bool lock = true);
	 void removeFromSocketSet(int currfd, bool lock = true);

	 bool alreadyListening(int sockfd, bool lock = true);
	 void setReadBit(int sockfd, bool lock = true);
	 void setWriteBit(int sockfd, bool lock = true);

private:
	 sashIActionRuntime * m_act;
	 JSContext *m_cx;
	 sashIExtension *m_ext;

	 // thread to select on connections
	 bool m_running;
	 pthread_t m_selectOnSockets;

	 // keeps track of all of the sockets.
	 // hash table (keys = fds, values = open sockets 
	 socketHash m_socketObjectsHash;

	 int m_numSockets;
	 int m_highestSockfd;

	 // master keeps track of all sockets that need to be listened to
	 // it must be copied into m_readfds before calling select. 
	 // select changes m_readfds when it runs.
	 fd_set m_readmaster;
	 fd_set m_readfds;	 
	 fd_set m_writemaster;
	 fd_set m_writefds;
	 fd_set m_exceptfds;



	 // lock for the hash table and 
	 // read select set
	 pthread_mutex_t m_listener_lock; 
	 void lockListener(){
		  DEBUGMSG(socklock, "locking listener...\n");
		  pthread_mutex_lock(&m_listener_lock);
		  DEBUGMSG(socklock, "locked listener...\n");
	 }
	 void unlockListener(){
		  DEBUGMSG(socklock, "unlocking listener...\n");
		  pthread_mutex_unlock(&m_listener_lock);
		  DEBUGMSG(socklock, "unlocked listener...\n");
	 }

	 // lock for the write select set
	 pthread_mutex_t m_sender_lock; 
	 void lockSender(){
		  DEBUGMSG(socklock, "locking sender...\n");
		  pthread_mutex_lock(&m_sender_lock);
		  DEBUGMSG(socklock, "locked sender...\n");
	 }
	 void unlockSender(){
		  DEBUGMSG(socklock, "unlocking sender...\n");
		  pthread_mutex_unlock(&m_sender_lock);
		  DEBUGMSG(socklock, "unlocked sender...\n");
	 }

	 // timeout value for select.
	 struct timeval m_timeout;
	 void setTimeout(int secs, int microsecs){
		  m_timeout.tv_sec = secs;
		  m_timeout.tv_usec = microsecs;
	 }

	 // pending client connections. protected by a lock.
	 deque<sashClientSocket*> m_clientConnectionQueue;
	 pthread_mutex_t m_clientConnection_lock;
	 void lockClientConnQueue(){
		  DEBUGMSG(socklock, "locking client connection queue...\n");
		  pthread_mutex_lock(&m_clientConnection_lock);
		  DEBUGMSG(socklock, "locked client connection queue...\n");
	 }
	 void unlockClientConnQueue(){
		  DEBUGMSG(socklock, "unlocking client connection queue...\n");
		  pthread_mutex_unlock(&m_clientConnection_lock);
		  DEBUGMSG(socklock, "unlocked client connection queue...\n");
	 }

	 // queue containing sockets to be closed. protected by a lock.
	 deque<openSocketObject*> m_closeQueue;
	 pthread_mutex_t m_closeQueue_lock;
	 void lockCloseQueue(){
		  DEBUGMSG(socklock, "locking close queue...\n");
		  pthread_mutex_lock(&m_closeQueue_lock);
		  DEBUGMSG(socklock, "locked close queue...\n");
	 }
	 void unlockCloseQueue(){
		  DEBUGMSG(socklock, "unlocking close queue...\n");
		  pthread_mutex_unlock(&m_closeQueue_lock);
		  DEBUGMSG(socklock, "unlocked close queue...\n");
	 }

	 // since there's only one instance of socket connection manager
	 // running, don't need to lock these.
	 deque<sashDatagramSocket*> m_datagramReceiveQueue;
	 deque<sashDatagramSocket*> m_datagramSendQueue;
	 deque<sashClientSocket*> m_clientReceiveQueue;	 
	 deque<sashClientSocket*> m_clientSendQueue;	 
	 deque<sashServerSocket*> m_pendingServerQueue;
	 
	 // buffers for send and recv.
	 char m_TCPReceiveBuffer[MAX_CLIENT_RECEIVE_BUFSIZE+1];
	 char m_TCPSendBuffer[MAX_CLIENT_SEND_BUFSIZE+1];

	 // buffer for recvfrom.
	 char m_UDPReceiveBuffer[MAX_DATAGRAM_BUFSIZE+1];

	 // datagram methods.
	 int receiveDatagram(sashDatagramSocket *dsocket);
	 int processDatagram(sashDatagramSocket *dsocket, char *buf);

	 // client methods.
	 int receiveSomeData(sashClientSocket *csocket);
	 int sendSomeData(sashClientSocket *csocket);
	 int processClientData(sashClientSocket *csocket);

	 // processing methods.
	 void checkClientQueue();
	 void checkCloseQueue();
	 void processDatagramReceiveQueue();
	 void processDatagramSendQueue();
	 void processClientReceiveQueue();
	 void processClientSendQueue();
	 void processServerQueue();
	 void checkClientSendBuffers();

};


#endif
