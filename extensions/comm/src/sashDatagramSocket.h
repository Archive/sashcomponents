
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the datagram socket object
******************************************************************/


#ifndef SASHDATAGRAMSOCKET_H
#define SASHDATAGRAMSOCKET_H

#include "nsID.h"
#include "sashIConstructor.h"
#include "sashIComm.h"
#include "sashISockets.h"
#include "sashIDatagramSocket.h"
#include <string>
#include <deque>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "streamBuffer.h"

// (c975470e-8865-4a9d-aa7c-fc7bfae3104b)
#define SASHDATAGRAMSOCKET_CID \
{0xc975470e, 0x8865, 0x4a9d, {0xaa, 0x7c, 0xfc, 0x7b, 0xfa, 0xe3, 0x10, 0x4b}}

NS_DEFINE_CID(ksashDatagramSocketCID, SASHDATAGRAMSOCKET_CID);

#define SASHDATAGRAMSOCKET_CONTRACT_ID "@gnome.org/SashXB/communications/sockets/sashdatagramsocket;1"

typedef struct {
	 int dataSize;
	 char *data;
	 struct sockaddr_in dest_addr;
} datagram;


class sashDatagramSocket : public sashIDatagramSocket,
						   public sashIConstructor
{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHIDATAGRAMSOCKET;
	 NS_DECL_SASHICONSTRUCTOR;

	 sashDatagramSocket();
	 virtual ~sashDatagramSocket();

protected:
	 bool receivedData();
	 void appendReceivedData(int size, char *buf);
	 int getSizeOfNextReceivedDatagram();
	 void getNextReceivedDatagram(char *buf);
	 int getSockFD();
	 void setSockFD(int sockfd);
	 void setIsOpen(bool isopen);
	 bool hasDataToSend();
	 bool sendAllDatagrams(char **sendErrorBuf);
	 void waitForData();
	 void dataToSend();

private:
	 pthread_mutex_t m_datagramLock; 

	 pthread_mutex_t m_sendBufferLock; 
	 deque<datagram*> m_sendBuffer;

	 pthread_mutex_t m_receiveBufferLock;
	 streamBuffer *m_receiveBuffer;

	 JSContext *m_cx;
	 string m_lastErrorDescription;

	 int m_maxDatagramSize;
	 int m_receivePort;
	 int m_sockfd;

	 bool m_isOpen;
	 
	 sashIExtension *m_sashComm;
	 sashISockets *m_sashSockets;
	 
	 string m_onClosed;
	 string m_onReceived;
	 string m_onSendError;

	 bool sendBufferEmpty();

	 friend class SocketConnectionManager;
	 void callSendErrorEvent(const char *buf);
	 nsCOMPtr<sashIActionRuntime> m_act;
};

#endif
