
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142

    USA

*****************************************************************

Contributor(s): Wing Yung

module for the SashComm
******************************************************************/

#include "sashComm.h"
#include "sashSockets.h"
#include "sashDatagramSocket.h"
#include "sashClientSocket.h"
#include "sashServerSocket.h"
#include "extensiontools.h"

NS_DECL_CLASSINFO(sashComm);
NS_DECL_CLASSINFO(sashSockets);
NS_DECL_CLASSINFO(sashDatagramSocket);
NS_DECL_CLASSINFO(sashClientSocket);
NS_DECL_CLASSINFO(sashServerSocket);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashComm);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashSockets);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashDatagramSocket);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashClientSocket);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashServerSocket);

static nsModuleComponentInfo components[] = {
	 MODULE_COMPONENT_ENTRY(sashComm, SASHCOMM, "Communications extension"),
	 MODULE_COMPONENT_ENTRY(sashSockets, SASHSOCKETS, "Sockets"),
	 MODULE_COMPONENT_ENTRY(sashDatagramSocket, SASHDATAGRAMSOCKET, "Datagram socket"),
	 MODULE_COMPONENT_ENTRY(sashClientSocket, SASHCLIENTSOCKET, "Client socket"),
	 MODULE_COMPONENT_ENTRY(sashServerSocket, SASHSERVERSOCKET, "Server socket")
};

NS_IMPL_NSGETMODULE(sashComm, components)
