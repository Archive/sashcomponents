
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the datagram socket object
******************************************************************/

#include "sashDatagramSocket.h"
#include "sashVariantUtils.h"
#include "extensiontools.h"
#include "nsCRT.h"
#include "sashIExtension.h"
#include "sashSockets.h"
#include "SocketConnectionManager.h"
#include "sashnet.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>

NS_IMPL_ISUPPORTS2_CI(sashDatagramSocket, sashIDatagramSocket, sashIConstructor);

sashDatagramSocket::sashDatagramSocket()
{
	 NS_INIT_ISUPPORTS();
	 m_isOpen = false;
	 m_maxDatagramSize = MAX_DATAGRAM_BUFSIZE;
	 pthread_mutex_init(&m_datagramLock, NULL);
	 pthread_mutex_init(&m_sendBufferLock, NULL);
	 pthread_mutex_init(&m_receiveBufferLock, NULL);
}

sashDatagramSocket::~sashDatagramSocket()
{
	 pthread_mutex_destroy(&m_datagramLock);
	 pthread_mutex_destroy(&m_sendBufferLock);
	 pthread_mutex_destroy(&m_receiveBufferLock);
	 if (m_receiveBuffer) delete m_receiveBuffer;
}

NS_IMETHODIMP
sashDatagramSocket::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, 
										JSContext * cx, PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	 m_receiveBuffer = new streamBuffer();
	 m_act = actionRuntime;

	 *ret = true;
	 if (m_receiveBuffer == NULL){
		  DEBUGMSG(comm, "not enough memory to create receive buffer\n");
		  *ret = false;
	 }

	 m_cx = cx;
	 return NS_OK;
}

/* void Close (in nsIVariant bForce); */
NS_IMETHODIMP sashDatagramSocket::Close(nsIVariant *bForce)
{
// 	 bool force = (VariantIsEmpty(bForce) ? false : VariantGetBoolean(bForce));
	 bool force = true;
	 DEBUGMSG(comm, "sashDatagramSocket: Close.\n");

	 pthread_mutex_lock(&m_datagramLock);	 
	 // Do something. 
	 if (m_isOpen){
		  if (force){
			   sashSockets::m_scm->addDSocketToCloseQueue(this);
		  } else {
			   // don't want to force it to close. let data finish sending
			   // first.
		  }
	 }
	 pthread_mutex_unlock(&m_datagramLock);	 
	 return NS_OK;
}

/* PRBool Create (in nsIVariant Port); */
NS_IMETHODIMP sashDatagramSocket::Create(nsIVariant *Port, PRBool *_retval)
{
	 DEBUGMSG(comm, "sashDatagramSocket: Create.\n");
	 *_retval = true;
	 pthread_mutex_lock(&m_datagramLock);	 
	 if (! m_isOpen){
		  m_receivePort 
			   = (VariantIsEmpty(Port))? 0 : (int)(VariantGetNumber(Port));

		  sockaddr_in myAddr;
		  if ((m_sockfd = bindLocal(SOCK_DGRAM, m_receivePort, &myAddr)) 
			  == -1){
			   // if it didn't work, then signal an error.
		  } else {
			   m_isOpen = true;
		  
			   DEBUGMSG(comm, "new datagram socket created, fd%d\n", m_sockfd);
			   sashSockets::m_scm->addToSocketSet(m_sockfd, 
												  DATAGRAM, (void *)this);
			   DEBUGMSG(comm, "fd%d added to listen set\n", m_sockfd);
		  }
		  
		  *_retval = (m_sockfd != -1);
	 }
	 pthread_mutex_unlock(&m_datagramLock);	 

	 return NS_OK;
}

/* void sendConnectionlessDatagram (in nsIVariant vData, in string strDestAddress, in PRInt32 nDestPort); */
NS_IMETHODIMP sashDatagramSocket::SendConnectionlessDatagram
(nsIVariant *vData, const char *strDestAddress, PRInt32 nDestPort)
{
	 int sockfd;
	 struct sockaddr_in dest_addr; 
	 struct hostent *he;
	 short destPort = (short)nDestPort;
	 int numbytes;
	 
	 // no support for binary objects.
	 string datastr = VariantGetString(vData);
	 const char * datastrc = datastr.c_str();

	 bool setupFailed = false;
	 
	 pthread_mutex_lock(&m_datagramLock);	 
	 if ((int)datastr.size() > m_maxDatagramSize){
		  DEBUGMSG(comm, "sashDatagramSocket: size of data exceeded maximum datagram size\n");
		  m_lastErrorDescription = "data size exceeded maximum datagram size";
		  setupFailed = true;
	 }

	 if ((he=gethostbyname(strDestAddress)) == NULL) { 
		  perror("gethostbyname");
		  DEBUGMSG(comm, "sashDatagramSocket: gethostbyname failed\n");
		  m_lastErrorDescription = "gethostbyname call failed";
		  setupFailed = true;
	 }

	 if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		  perror("socket");
		  DEBUGMSG(comm, "sashDatagramSocket: socket call failed\n");
		  m_lastErrorDescription = "socket call failed";
		  setupFailed = true;
	 }

	 pthread_mutex_unlock(&m_datagramLock);	 

	 if (setupFailed){
		  callSendErrorEvent(datastrc);
		  return NS_OK;
	 }

	 // set up destination address.
	 dest_addr.sin_family = AF_INET;   
	 dest_addr.sin_port = htons(destPort); 
	 dest_addr.sin_addr = *((struct in_addr *)he->h_addr);
	 memset(&(dest_addr.sin_zero), '\0', 8);

	 bool sendFailed = false;
	 if ((numbytes=sendto(sockfd, datastrc, strlen(datastrc), 0,
						  (struct sockaddr *)&dest_addr, 
						  sizeof(struct sockaddr))) == -1) {
		  perror("sendto");
		  DEBUGMSG(comm, "sashDatagramSocket: sendconnectionlessdatagram: sendto call failed\n");

		  pthread_mutex_lock(&m_datagramLock);	 
		  m_lastErrorDescription = "sendconnectionlessdatagram: sendto call failed";
		  pthread_mutex_unlock(&m_datagramLock);	 
		  sendFailed = true;
	 }

	 if (sendFailed){
		  callSendErrorEvent(datastrc);
	 }

	 close(sockfd);
	 return NS_OK;
}

/* void SendDatagram (in nsIVariant vData, in string strDestAddress, in PRInt32 nDestPort); */
NS_IMETHODIMP sashDatagramSocket::SendDatagram(nsIVariant *vData, const char *strDestAddress, PRInt32 nDestPort)
{
	 struct hostent *he = NULL;
	 short destPort = (short)nDestPort;

	 DEBUGMSG(comm, "sashDatagramSocket: sendDatagram, fd%d.\n",
			  getSockFD());

	 // no support for binary objects.
	 string datastr = VariantGetString(vData);

	 bool setupFailed = false;
	 
	 // Figure out where the datagram is going.
	 pthread_mutex_lock(&m_datagramLock);	 

	 if (m_sockfd == -1){
		  DEBUGMSG(comm, "sashDatagramSocket: sockfd == -1\n");
		  m_lastErrorDescription = "Datagram socket not created properly!\n";
		  setupFailed = true;
	 } else {
		  if ((int)datastr.size() > m_maxDatagramSize){
			   DEBUGMSG(comm, "size of data exceeded maximum datagram size\n");
			   m_lastErrorDescription = "data size exceeded maximum datagram size";
			   setupFailed = true;
		  } else {
			   if ((he=gethostbyname(strDestAddress)) == NULL) { 
					perror("gethostbyname");
					DEBUGMSG(comm, "sashDatagramSocket: gethostbyname failed\n");
					m_lastErrorDescription = "gethostbyname call failed";
					setupFailed = true;
			   }
		  }
	 }

	 pthread_mutex_unlock(&m_datagramLock);	 

	 datagram *dgram = new datagram;
	 if (dgram == NULL){
		  m_lastErrorDescription = "not enough memory!";
		  return NS_ERROR_FAILURE;
	 }

	 const char * datastrc = datastr.c_str();
	 if (setupFailed){
		  DEBUGMSG(comm, "sashDatagramSocket: failed to create datagram\n");
		  callSendErrorEvent(datastrc);
		  return NS_OK;
	 }

	 int size = dgram->dataSize = datastr.size();

	 // copy the data in.
	 if (size > 0){
		  dgram->data = new char [size+1];
		  if (dgram->data == NULL){
			   DEBUGMSG(comm, "couldn't allocate memory for datagram object\n");
			   return NS_ERROR_FAILURE;
		  }

		  memcpy (dgram->data, datastrc, size);
		  dgram->data[size] = '\0';
	 }

	 // copy the destination address in.
	 dgram->dest_addr.sin_family = AF_INET;   
	 dgram->dest_addr.sin_port = htons(destPort); 
	 dgram->dest_addr.sin_addr = *((struct in_addr *)he->h_addr);
	 memset(&(dgram->dest_addr.sin_zero), '\0', 8);

	 // add it to the queue.
	 DEBUGMSG(comm, 
			  "sashDatagramSocket: appending datagram to send buffer, size %d containing %s\n",
			  dgram->dataSize, dgram->data);

	 pthread_mutex_lock(&m_sendBufferLock);	 
	 m_sendBuffer.push_back(dgram);
	 pthread_mutex_unlock(&m_sendBufferLock);

	 dataToSend();
	 return NS_OK;
}

/* readonly attribute PRInt32 ReceivePort; */
NS_IMETHODIMP sashDatagramSocket::GetReceivePort(PRInt32 *aReceivePort)
{
	 pthread_mutex_lock(&m_datagramLock);	 
	 *aReceivePort = m_receivePort;
	 pthread_mutex_unlock(&m_datagramLock);	 
	 return NS_OK;
}

/* attribute nsIVariant Context; */
NS_IMETHODIMP sashDatagramSocket::GetContext(nsIVariant * *aContext)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashDatagramSocket::SetContext(nsIVariant * aContext)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute string LastErrorDescription; */
NS_IMETHODIMP sashDatagramSocket::GetLastErrorDescription(char * *aLastErrorDescription)
{
	 pthread_mutex_lock(&m_datagramLock);	 
	 string errorStr = m_lastErrorDescription;
	 pthread_mutex_unlock(&m_datagramLock);	 
	 XP_COPY_STRING(errorStr.c_str(), aLastErrorDescription);
	 return NS_OK;
}

/* readonly attribute PRInt32 MaxDatagramSize; */
NS_IMETHODIMP sashDatagramSocket::GetMaxDatagramSize(PRInt32 *aMaxDatagramSize)
{
	 pthread_mutex_lock(&m_datagramLock);	 
	 *aMaxDatagramSize = m_maxDatagramSize;
	 pthread_mutex_unlock(&m_datagramLock);	 
	 return NS_OK;
}

/* attribute string OnClosed; */
NS_IMETHODIMP sashDatagramSocket::GetOnClosed(char * *aOnClosed)
{
	 XP_COPY_STRING(m_onClosed.c_str(), aOnClosed);
	 return NS_OK;
}
NS_IMETHODIMP sashDatagramSocket::SetOnClosed(const char * aOnClosed)
{
	 m_onClosed = aOnClosed;
	 return NS_OK;
}

/* attribute string OnReceived; */
NS_IMETHODIMP sashDatagramSocket::GetOnReceived(char * *aOnReceived)
{
	 XP_COPY_STRING(m_onReceived.c_str(), aOnReceived);
	 return NS_OK;
}
NS_IMETHODIMP sashDatagramSocket::SetOnReceived(const char * aOnReceived)
{
	 m_onReceived = aOnReceived;
	 return NS_OK;
}

/* attribute string OnSendError; */
NS_IMETHODIMP sashDatagramSocket::GetOnSendError(char * *aOnSendError)
{
	 XP_COPY_STRING(m_onSendError.c_str(), aOnSendError); 
	 return NS_OK;
}
NS_IMETHODIMP sashDatagramSocket::SetOnSendError(const char * aOnSendError)
{
	 m_onSendError = aOnSendError;
	 return NS_OK;
}

int sashDatagramSocket::getSockFD(){
	 int retval;
	 pthread_mutex_lock(&m_datagramLock);	 
	 retval = m_sockfd;
	 pthread_mutex_unlock(&m_datagramLock);	 
	 return retval;
}
void sashDatagramSocket::setSockFD(int sockfd){
	 pthread_mutex_lock(&m_datagramLock);	 
	 m_sockfd = sockfd;
	 pthread_mutex_unlock(&m_datagramLock);	 
}
void sashDatagramSocket::setIsOpen(bool isopen){
	 pthread_mutex_lock(&m_datagramLock);	 
	 m_isOpen = isopen;
	 pthread_mutex_unlock(&m_datagramLock);	 
}

bool sashDatagramSocket::hasDataToSend(){
	 bool empty;
	 pthread_mutex_lock(&m_sendBufferLock);	 
	 empty = m_sendBuffer.empty();
	 pthread_mutex_unlock(&m_sendBufferLock);	 

	 if (empty) return false;
	 return true;
}

bool sashDatagramSocket::sendAllDatagrams(char **sendErrorBuf){
	 datagram *dgram;
	 int sockfd;

	 DEBUGMSG(comm, "sashDatagramSocket: sendAllDatagrams.\n");

	 bool isEmpty;

	 // check to see whether there are any datagrams to send.
	 pthread_mutex_lock(&m_sendBufferLock);	 
	 isEmpty = m_sendBuffer.empty();
	 pthread_mutex_unlock(&m_sendBufferLock);	 
	 if (isEmpty){
		  DEBUGMSG(comm, "sashDatagramSocket: no datagrams to send...\n");
		  return true;
	 }else{
		  DEBUGMSG(comm, "sashDatagramSocket: %d datagrams to send...\n", 
				   m_sendBuffer.size());
	 }

	 // check to make sure that there is already a socket to send on.
	 sockfd = getSockFD();
	 if (sockfd == -1){
		  pthread_mutex_lock(&m_datagramLock);	 
		  m_lastErrorDescription = "socket not ready for sending";
		  pthread_mutex_unlock(&m_datagramLock);	 
		  return false;
	 }

	 pthread_mutex_lock(&m_sendBufferLock);	 
	 bool sendFailed = false;
	 int numbytes;

	 while ((! m_sendBuffer.empty()) && (! sendFailed)){
		  dgram = m_sendBuffer.front();
		  m_sendBuffer.pop_front();
		  
		  DEBUGMSG(comm, "sashDatagramSocket: dgram size = %d\n", dgram->dataSize);

		  if ((numbytes=sendto(sockfd, dgram->data, dgram->dataSize, 0,
							   (struct sockaddr *)&(dgram->dest_addr), 
							   sizeof(struct sockaddr))) == -1) {
			   perror("sendto");
			   DEBUGMSG(comm, "sashDatagramSocket: sendAllDatagrams: sendto call failed\n");

			   pthread_mutex_lock(&m_datagramLock);	 
			   m_lastErrorDescription = "sendAllDatagrams: sendto call failed";
			   sendFailed = true;

			   *sendErrorBuf = dgram->data;
			   pthread_mutex_unlock(&m_datagramLock);	 

			   DEBUGMSG(comm, "sashDatagramSocket: senderrorbuf: %s\n", 
						*sendErrorBuf);

		  }else{
			   DEBUGMSG(comm, "sashDatagramSocket: sendto sent %d bytes\n", 
						numbytes);
			   delete(dgram->data);
		  }
		  delete(dgram);

	 }
	 pthread_mutex_unlock(&m_sendBufferLock);	 

	 if (sendFailed){
		  return false;
	 }else{
		  return true;
	 }
}

bool sashDatagramSocket::sendBufferEmpty(){
	 bool retval;
	 pthread_mutex_lock(&m_datagramLock);
	 retval = m_sendBuffer.empty();
	 pthread_mutex_unlock(&m_datagramLock);
	 return retval;
}

void sashDatagramSocket::dataToSend()
{
	 sashSockets::m_scm->addToWriteSet(getSockFD());
}

bool sashDatagramSocket::receivedData(){
	 bool retval;
	 pthread_mutex_lock(&m_receiveBufferLock);	 
	 retval = (! m_receiveBuffer->empty());
	 pthread_mutex_unlock(&m_receiveBufferLock);
	 return retval;
}

void sashDatagramSocket::appendReceivedData(int size, char *buf){
	 pthread_mutex_lock(&m_receiveBufferLock);	 
	 m_receiveBuffer->appendData(size, (char *)buf);
	 pthread_mutex_unlock(&m_receiveBufferLock);
}

int sashDatagramSocket::getSizeOfNextReceivedDatagram(){
	 pthread_mutex_lock(&m_receiveBufferLock);	 
	 int size = m_receiveBuffer->getSizeOfFirstItem();
	 pthread_mutex_unlock(&m_receiveBufferLock);
	 return size;
}

void sashDatagramSocket::getNextReceivedDatagram(char *buf){
	 pthread_mutex_lock(&m_receiveBufferLock);	 
	 m_receiveBuffer->getDataFromFirstItem(buf);
	 pthread_mutex_unlock(&m_receiveBufferLock);
}

void sashDatagramSocket::callSendErrorEvent
(const char *buf){
	 string bufstr = buf;
	 string callback = m_onSendError;

	 vector <nsIVariant *> args;
	 DEBUGMSG(comm, "about to call datagram send error event %s\n",
			  callback.c_str());

	 if (callback != "") {
		  nsIVariant * sockVariant;
		  nsIVariant * sockData;

		  NewVariant(&sockVariant);
		  VariantSetInterface(sockVariant, 
							  (nsISupports *)(sashIDatagramSocket *)this);
		  args.push_back(sockVariant);
								   
		  NewVariant(&sockData);
		  VariantSetString(sockData, bufstr);
								   
		  args.push_back(sockData);
		  CallEvent(m_act, m_cx, 
					callback.c_str(), args);
	 }
}

