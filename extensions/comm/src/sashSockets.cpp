
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the sockets object
******************************************************************/

#include <iostream.h>
#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "sashComm.h"
#include "sashSockets.h"
#include "sashDatagramSocket.h"
#include "sashClientSocket.h"
#include "sashServerSocket.h"

#include "sash_constants.h"
#include "sashIGenericConstructor.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "extensiontools.h"
#include <unistd.h>
#include "secman.h"

NS_IMPL_ISUPPORTS2_CI(sashSockets, sashISockets,
				   sashIConstructor);

SocketConnectionManager *sashSockets::m_scm
= new SocketConnectionManager();

enum SocketSecurity {
	 SS_DATAGRAM = 0,
	 SS_CLIENT,
	 SS_SERVER
};

sashSockets::sashSockets()
{
	 NS_INIT_ISUPPORTS();
	 m_datagramSocketConstructor = NULL;
	 m_serverSocketConstructor = NULL;
	 m_clientSocketConstructor = NULL;
}

sashSockets::~sashSockets()
{
	 NS_RELEASE (m_datagramSocketConstructor);
	 NS_RELEASE (m_serverSocketConstructor);
	 NS_RELEASE (m_clientSocketConstructor);

	 if (m_scm){
		  delete m_scm;
	 }
}

NS_IMETHODIMP sashSockets::Initialize(sashIActionRuntime * actionRuntime, sashIExtension *sComm,
									  JSContext *cx)
{
	 DEBUGMSG(comm, "sashSockets::initialize\n");
	 m_sashComm = sComm;
	 m_cx = cx;
	 m_act = actionRuntime;
	 m_act->GetSecurityManager(&m_secMan);

	 NewSashConstructor(m_act, m_sashComm, SASHDATAGRAMSOCKET_CONTRACT_ID, 
						SASHIDATAGRAMSOCKET_IID_STR, &m_datagramSocketConstructor);
	 NewSashConstructor(m_act, m_sashComm, SASHSERVERSOCKET_CONTRACT_ID, 
						SASHISERVERSOCKET_IID_STR, &m_serverSocketConstructor);
	 NewSashConstructor(m_act, m_sashComm, SASHCLIENTSOCKET_CONTRACT_ID, 
						SASHICLIENTSOCKET_IID_STR, &m_clientSocketConstructor);
/*
	 nsComponentManager::CreateInstance
		  (ksashGenericConstructorCID, 
		   NULL, 
		   NS_GET_IID(sashIGenericConstructor), 
		   (void **) &m_datagramSocketConstructor); 

	 m_datagramSocketConstructor->SetConstructorProperties
		  (m_sashComm, SASHDATAGRAMSOCKET_CONTRACT_ID, 
		   SASHIDATAGRAMSOCKET_IID_STR);

	 nsComponentManager::CreateInstance
		  (ksashGenericConstructorCID, 
		   NULL, 
		   NS_GET_IID(sashIGenericConstructor), 
		   (void **) &m_serverSocketConstructor); 
	 m_serverSocketConstructor->SetConstructorProperties
		  (m_sashComm, SASHSERVERSOCKET_CONTRACT_ID, 
		   SASHISERVERSOCKET_IID_STR);

	 nsComponentManager::CreateInstance
		  (ksashGenericConstructorCID, 
		   NULL, 
		   NS_GET_IID(sashIGenericConstructor), 
		   (void **) &m_clientSocketConstructor); 
	 m_clientSocketConstructor->SetConstructorProperties
		  (m_sashComm, SASHCLIENTSOCKET_CONTRACT_ID, 
		   SASHICLIENTSOCKET_IID_STR);
*/
	 // *** check return 
	 if(! m_scm->Initialize(m_act,
							m_cx, 
							m_sashComm)){
		  return NS_ERROR_FAILURE;
	 }

	 m_scm->start();

	 return NS_OK;
}

NS_IMETHODIMP
sashSockets::InitializeNewObject(sashIActionRuntime * actionRuntime, sashIExtension * ext, JSContext * cx, 
								 PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
	 m_act = actionRuntime;
	 return NS_OK;
}


/* readonly attribute sashIGenericConstructor DatagramSocket; */
NS_IMETHODIMP sashSockets::GetDatagramSocket(sashIGenericConstructor * *aDatagramSocket)
{
	 DEBUGMSG(comm, "sockets::getdatagramsocket\n");
	 AssertSecurity(m_secMan, ToUpper(ksashCommCID.ToString()), SS_DATAGRAM, true);
	 NS_ADDREF(m_datagramSocketConstructor);
	 *aDatagramSocket = m_datagramSocketConstructor;
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor ClientSocket; */
NS_IMETHODIMP sashSockets::GetClientSocket(sashIGenericConstructor * *aClientSocket)
{
	 DEBUGMSG(comm, "sockets::getclientsocket\n");
	 AssertSecurity(m_secMan, ToUpper(ksashCommCID.ToString()), SS_CLIENT, true);
	 NS_ADDREF(m_clientSocketConstructor);
	 *aClientSocket = m_clientSocketConstructor;
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor ServerSocket; */
NS_IMETHODIMP sashSockets::GetServerSocket(sashIGenericConstructor * *aServerSocket)
{
	 DEBUGMSG(comm, "sockets::getserversocket\n");
	 AssertSecurity(m_secMan, ToUpper(ksashCommCID.ToString()), SS_SERVER, true);
	 NS_ADDREF(m_serverSocketConstructor);
	 *aServerSocket = m_serverSocketConstructor;
	 return NS_OK;
}

/* nsIVariant ResolveHostName (inas string strHostName); */
NS_IMETHODIMP sashSockets::ResolveHostName(const char *strHostName, nsIVariant **_retval)
{
	 nsIVariant *v;
	 if (NewVariant (_retval) == NS_OK){
		  v = *_retval;
	 }else{
		  return NS_ERROR_FAILURE;
	 }

	 struct hostent *h;
	 if ((h=gethostbyname(strHostName)) == NULL){
		  vector <string> strs;
		  VariantSetArray(v, strs); 
	 }else{
		  VariantSetStringArrayC(v, h->h_addr_list);
	 }
	 return NS_OK;
}

/* string ResolveHostNameFirst (in string strHostName); */
NS_IMETHODIMP sashSockets::ResolveHostNameFirst(const char *strHostName, char **_retval)
{
	 struct hostent *h;
	 ostrstream o;
	 if ((h=gethostbyname(strHostName)) == NULL){
		  o << '\0';
	 }
	 else{
		  if (h->h_addr != NULL){
			   o << inet_ntoa(*((struct in_addr *)h->h_addr)) << '\0';
		  } else {
			   // not sure if this is covered by the first if condition.
			   o << '\0';
		  }
	 }
	 XP_COPY_STRING(o.str(), _retval);
	 return NS_OK;
}

/* readonly attribute string LocalHostName; */
NS_IMETHODIMP sashSockets::GetLocalHostName(char * *aLocalHostName)
{
	 char buf[HOSTNAME_MAX];
	 
	 if (gethostname(buf, HOSTNAME_MAX) == -1){
		  buf[0] = '\0';
	 }
	 XP_COPY_STRING(buf, aLocalHostName);
	 return NS_OK;
}

