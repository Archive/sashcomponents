
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the event class
use this to call events safely from threads that you create yourself
(see sashFTPConnection and SocketConnectionManager for examples).

Just declare a pointer to an eventQueue in the extension's header file
Initialize the pointer in the Initialize() method (pass it a pointer to
a sashIActionRuntime object and a pointer to the extension).
(Remember to delete in the destructor).
Just have ProcessEvent call	the eventQueue's callAllEvents() method.

******************************************************************/


#ifndef EVENT_H
#define EVENT_H

#include <deque>
#include "sashIExtension.h"
#include <string>
#include "nsIVariant.h"
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"

typedef struct{
	 JSContext *cx;
	 string callback;
	 vector <nsIVariant *> args;
} genericEvent;

typedef enum {
TYPE_STRING,
TYPE_NUM,
TYPE_BOOL,
TYPE_OBJ
} vType;

typedef struct {
	 vType type;
     struct {
        bool b;
        double d;
        void* o;
        string s;
     } arg;
} argElement;

class eventQueue{
public:
	 eventQueue(sashIActionRuntime *rt, sashIExtension *ext);
	 ~eventQueue();

	 // all of these take an object and one or more other values.
	 bool addGenericEvent(JSContext *cx,
						  const string& callback,
						  const nsISupports *obj);
	 bool addGenericEvent(JSContext *cx,
						  const string& callback,
						  const nsISupports *obj,
						  const string& message);
	 bool addGenericEvent(JSContext *cx,
						  const string& callback,
						  const nsISupports *obj,
						  const string& message1, 
const string& message2);

	 bool addGenericEvent(JSContext *cx,
						  const string& callback,
						  const nsISupports *obj,
						  const string& message,
						  double numval);
	 bool addGenericEvent(JSContext *cx,
						  const string& callback,
						  const nsISupports *obj,
						  const string& message,
						  double numval,
						  double numval2);
	 bool addGenericEvent(JSContext *cx,
						  const string& callback,
						  const nsISupports *obj,
						  bool bval);

	 void addEvent(genericEvent *e, bool lock = true);
	 void callAllEvents (bool lock = true);

private:
	 sashIActionRuntime *m_rt;
	 sashIExtension *m_ext;

	 pthread_mutex_t m_queueLock;
	 deque <genericEvent *> m_queue;
	 
	 void lockEventQueue(){
		  pthread_mutex_lock(&m_queueLock);
	 }
	 void unlockEventQueue(){
		  pthread_mutex_unlock(&m_queueLock);
	 }
	 
	 genericEvent *nextEvent(bool lock = true);
	 void callNextEvent (bool lock = true);
	 bool emptyEventQueue(bool lock = true);

	 // pass this a deque of argElements (which specify a type and a 
	 // pointer to an argument).
	 bool createGenericEvent
		  (genericEvent *event, JSContext *cx, const string& callback, 
		   deque <argElement>& args);



};

#endif
